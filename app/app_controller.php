<?php

    if (!function_exists('array_column')) {
        function array_column(array $input, $columnKey, $indexKey = null)
        {
            $array = array();
            foreach ($input as $value) {
                if (!isset($value[$columnKey])) {
                    trigger_error("Key \"$columnKey\" does not exist in array");
                    return false;
                }
                if (is_null($indexKey)) {
                    $array[] = $value[$columnKey];
                } else {
                    if (!isset($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not exist in array");
                        return false;
                    }
                    if (!is_scalar($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not contain scalar value");
                        return false;
                    }
                    $array[$value[$indexKey]] = $value[$columnKey];
                }
            }
            return $array;
        }
    }

    class AppController extends Controller
    {

        var $uses = array(
            'Setting',
            'Pagina',
            'PagamentoCondicao',
            'Categoria',
            'Usuario'
        );

        public function __construct()
        {
            parent::__construct();
            $this->components = array(
                'Acl', 'Auth', 'Ssl', 'Acl.AclFilter', 'Session', 'Carrinho.Carrinho', 'Session', 'RequestHandler'
            );
            $this->helpers = array(
                'Html', 'Form', 'Session', 'Crypt', 'Estados', 'String', 'Layout', 'Javascript', 'Parcelamento'
            );
        }

        public function beforeFilter()
        {
            parent::beforeFilter();
            $this->Setting->writeConfiguration();
            $this->AclFilter->auth();
            if (isset($this->params['admin']) && $this->params['admin'] == true) {
                $this->layout = 'admin';
            } else {

            }
            $this->set('auth', $this->Auth->user());
        }

        public function beforeRender()
        {
            $this->loadCategories();
            $this->loadPlots();
            $this->loadCart();
            $this->loadRulePayFast();
            $this->set('pages', $this->Pagina->getPages());
        }

        /**
         * Carrega o carrinho valor total e quantidade
         */
        private function loadCart()
        {
            $quantity = 0;
            $total_cart_value = 0.00;
            $items = $this->Session->read('Carrinho.itens');
            if ($items) {
                foreach ($items as $item) {
                    $quantity += $item['Produto']['quantidade'];
                    $total_cart_value += ($item['Produto']['preco_promocao'] > 0) ? $item['Produto']['preco_promocao'] : $item['Produto']['preco'] * $item['Produto']['quantidade'];
                }
            }
            $this->set(compact('items', 'total_cart_value', 'quantity'));
        }

        /**
         * Carrega os parcelamentos disponíveis
         */
        public function loadPlots()
        {
            $parcelas = $this->PagamentoCondicao->find('first', array('recursive' => -1, 'conditions' => array('PagamentoCondicao.status' => true)));
            $this->set('parcelamento', $parcelas['PagamentoCondicao']);
        }

        /**
         * Carregas as categorias com status ativo e com destaque na home
         */
        public function loadCategories()
        {
            $categorias = $this->Categoria->getCategories();
            $this->set(compact('categorias'));
        }

        /**
         * Regra para compra rápida do cliente
         * Verifica se o usuário tem um endereço completo cadastrado.
         */
        public function loadRulePayFast()
        {
            if ($this->Auth->user() && $this->Auth->user('id')) {
                $usuario = $this->Usuario->getRealTimeUser($this->Auth->user('id'));
                if ($usuario) {
                    $usuarioEndereco = $usuario['UsuarioEndereco'];
                    if (count($usuarioEndereco)) {
                        foreach ($usuarioEndereco as $e) {
                            if (
                                empty($e['rua']) || empty($e['cep']) || empty($e['numero']) || empty($e['cidade'])
                                || empty($e['bairro']) || empty($e['uf'])
                            ) {
                                $this->Session->write('Finalizacao', false);
                            } else {
                                $this->Session->write('Finalizacao', true);
                            }
                        }
                    } else {
                        $this->Session->write('Finalizacao', false);
                    }
                } else {
                    $this->Session->write('Finalizacao', false);
                }
            }
            $this->set('fastBuy', $this->Session->read('Finalizacao'));
        }



    }
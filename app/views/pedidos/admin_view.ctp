<?php echo $javascript->link('common/jquery-ui-1.8.16.custom.min', false); ?>
<?php echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css', false); ?>
<?php echo $this->Html->css('common/nyro_modal/nyroModal.css', false); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom', false); ?>
<?php echo $javascript->link('admin/pedidos/crud.js', false); ?>
<div class="index">
    <h2><?php __('Pedido'); ?></h2>
    <dl><?php $i = 0;
            $class = ' class="altrow"'; ?>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Id'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['id']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Usuário'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $this->Html->link($pedido['Usuario']['nome'], array(
                'controller' => 'usuarios',
                'action'     => 'edit',
                $pedido['Usuario']['id']
            )); ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Valor Frete'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            R$ <?php echo $pedido['Pedido']['valor_frete']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Valor Pedido'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            R$ <?php echo $pedido['Pedido']['valor_pedido']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Valor Desconto Meio Pagamento'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            R$ <?php echo $pedido['Pedido']['valor_desconto_meio_pagamento']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Valor Desconto Cupom'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            R$ <?php echo $pedido['Pedido']['valor_desconto_cupom']; ?>
            &nbsp;
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Valor Juros Pagamento'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            R$ <?php echo $pedido['Pedido']['valor_juros_meio_pagamento']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Cliente Nome'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['usuario_nome']; ?>
            &nbsp;
        </dd>

        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Cliente Email'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['usuario_email']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Cliente Telefone'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['usuario_telefone']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Cliente Celular'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['usuario_celular']; ?>
            &nbsp;
        </dd>

        <?php if ($pedido['Pedido']['usuario_tipo_pessoa'] == 'F') { ?>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente CPF'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_cpf']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente Sexo'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_sexo']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente RG'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_rg']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente Data Nascimento'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_data_nascimento']; ?>
                &nbsp;
            </dd>

        <?php } else { ?>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente Cnpj'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_cnpj']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente Razão Social'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_razao_social']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente Nome Fantasia'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_nome_fantasia']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Cliente Inscrição Estadual'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['usuario_inscricao_estadual']; ?>
                &nbsp;
            </dd>

        <?php } ?>

        <?php if (!empty($pedido['Pedido']['lista_id'])) : ?>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Lista de Presentes'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <a href="<?php echo $this->Html->url(array(
                    'plugin'     => 'lista_de_presente',
                    'controller' => 'listas',
                    'action'     => 'admin_edit',
                    $pedido['Pedido']['lista_id']
                )) ?>"><?php echo $pedido['Pedido']['lista_id']; ?></a>
                &nbsp;
            </dd>
        <?php endif ?>


        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Entrega Tipo'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['entrega_tipo']; ?>
            &nbsp;
        </dd>

        <?php if ($pedido['Pedido']['entrega_tipo'] == "LOJA" || $pedido['Pedido']['entrega_tipo'] == "RETIRAR_LOJA"): ?>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('Loja'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Loja']['nome']; ?>: <?php echo $pedido['Loja']['rua']; ?>
                , <?php echo $pedido['Loja']['numero']; ?>, <?php echo $pedido['Loja']['bairro']; ?>
                - <?php echo $pedido['Loja']['cidade']; ?>/<?php echo $pedido['Loja']['uf']; ?>
                &nbsp;
            </dd>
        <?php endIf; ?>

        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Entrega Prazo'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['entrega_prazo']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Parcelas'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['parcelas']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Cep'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_cep']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Rua'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_rua']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Numero'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_numero']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Complemento'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_complemento']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Bairro'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_bairro']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Cidade'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_cidade']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Endereco Estado'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['Pedido']['endereco_estado']; ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Data Criação'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $pedido['Pedido']['created']); ?>
            &nbsp;
        </dd>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Pagamento Condição'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $pedido['PagamentoCondicao']['nome']; ?>
            &nbsp;
        </dd>
        <?php if ($pedido['PagamentoCondicao']['pagamento_tipo_id'] == '2'): ?>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('CÓDIGO DE RETORNO'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['redecard_retorno']; ?>
                &nbsp;
            </dd>
            <dt<?php if ($i % 2 == 0)
                echo $class; ?>><?php __('MENSAGEM DE RETORNO'); ?></dt>
            <dd<?php if ($i++ % 2 == 0)
                echo $class; ?>>
                <?php echo $pedido['Pedido']['redecard_comprovante']; ?>
                &nbsp;
            </dd>
        <?php endIf; ?>
        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Tracking'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php
                $link_modal = $this->Html->Url("/admin/pedidos/tracking/{$pedido['Pedido']['id']}");
                $link_tracking = '<a target="_blank" class="nyroModal" href="' . $link_modal . '">Adicionar</a>';
                echo !empty($pedido['Pedido']['tracking']) ? $pedido['Pedido']['tracking'] : $link_tracking; ?>
            &nbsp;
        </dd>

        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><?php __('Emitir Pedido Oryon'); ?></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <a target="_self" href="<?php echo $html->Url('/admin/pedidos/downloadOrder/' . $pedido['Pedido']['id']); ?>">Baixar Pedido</a>
            &nbsp;
        </dd>

        <dt<?php if ($i % 2 == 0)
            echo $class; ?>><span class="value-total">Total Pedido</span></dt>
        <dd<?php if ($i++ % 2 == 0)
            echo $class; ?>>
            <?php echo $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_frete']) + $this->String->moedaToBco($pedido['Pedido']['valor_pedido'])); ?>
            &nbsp;
        </dd>
    </dl>


</div>
<div class="clear"></div>
<div class="PedidoItens index">
    <h2><?php __('Pedido Itens'); ?></h2>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th>Sku.</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Característica</th>
            <th>Quantidade</th>
            <th>Preço</th>
        </tr>
        <?php

            $i = 0;
            foreach ($pedido['PedidoItem'] as $item):
                $class = null;
                if ($i++ % 2 == 0) {
                    $class = ' class="altrow"';
                }

                ?>
                <tr<?php echo $class; ?>>
                    <td align="center"><a
                            href="<?php echo $this->Html->Url('/admin/produtos/edit/' . $item['produto_id']); ?>"><?php echo $item['sku']; ?></a>
                    </td>
                    <td align="center"><?php echo $item['nome']; ?>&nbsp;</td>
                    <td align="center"><?php echo $item['descricao']; ?>&nbsp;</td>
                    <td align="left" style="padding-top: 7px;">
                        <?php
                            if (count(json_decode($item['variacoes'])) > 0):
                                foreach (json_decode($item['variacoes']) as $key => $atributo) {
                                    echo $key . ": " . $atributo . "<br />";
                                }
                            else:
                                foreach (json_decode($item['atributos']) as $key => $atributo) {
                                    echo $key . ": " . $atributo . "<br />";
                                }
                            endIf;
                        ?>&nbsp;
                    </td>
                    <td align="center"><?php echo $item['quantidade']; ?>&nbsp;</td>
                    <td align="center">
                        R$ <?php echo $this->String->bcoToMoeda($item['quantidade'] * ($item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco'])); ?>
                        &nbsp;</td>
                </tr>
            <?php endforeach; ?>
    </table>
</div>
<?php if ($pedido['PedidoHistoricoTracking']): ?>
    <div class="PedidoItens index">
        <h2><?php __('Histórico de Tracking'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th>Email</th>
                <th>Data de Criação</th>
            </tr>
            <?php
                $i = 0;
                foreach ($pedido['PedidoHistoricoTracking'] as $item):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr<?php echo $class; ?>>
                        <td><?php echo $item['tracking']; ?>&nbsp;</td>
                        <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $item['created']); ?>&nbsp;</td>
                    </tr>
                <?php endforeach; ?>
        </table>
    </div>
<?php endif; ?>
<?php if ($pedido['PagamentoCondicao']['pagamento_tipo_id'] == '1'): ?>
    <div class="PedidoItens index">
        <h2><?php __('Clear Sale'); ?></h2>

        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?php echo $clearsale; ?></th>
            </tr>
        </table>
    </div>
<?php endIf; ?>
<div class="clear"></div>
<?php if ($pedido['PedidoHistoricoStatus']): ?>
    <div class="PedidoItens index">
        <h2><?php __('Histórico de Status'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th>Status</th>
                <th>Data de Modificação</th>
            </tr>
            <?php
                $i = 0;
                foreach ($pedido['PedidoHistoricoStatus'] as $item):
                    $class = null;
                    if ($i++ % 2 == 0) {
                        $class = ' class="altrow"';
                    }
                    ?>
                    <tr<?php echo $class; ?>>
                        <td><?php echo $item['status']; ?>&nbsp;</td>
                        <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $item['created']); ?>&nbsp;</td>
                    </tr>
                <?php endforeach; ?>
        </table>
    </div>
<?php endif; ?>
<?php if (isset($pedido['CupomQueimado']['Cupom'])): ?>
    <div class="PedidoItens index">
        <h2><?php __('Cupom de desconto'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th>Código</th>
                <th>Tipo de Cupom</th>
            </tr>
            <?php if (isset($pedido['CupomQueimado']['Cupom'])) { ?>
                <tr<?php echo $class; ?>>
                    <td><?php echo $this->Html->link($pedido['CupomQueimado']['Cupom']['cupom'], array(
                            'controller' => 'cupons',
                            'action'     => 'edit',
                            $pedido['CupomQueimado']['Cupom']['id']
                        )); ?>&nbsp;</td>
                    <td><?php echo $pedido['CupomQueimado']['CupomTipo']['nome']; ?>&nbsp;</td>
                </tr>
            <?php } ?>
        </table>
    </div>
<?php endif; ?>

<div class="pedidos form">
    <?php echo $this->Form->create('Pedido', array('action' => 'edit/' . $pedido['Pedido']['id'])); ?>
    <fieldset>
        <legend><?php __('Alterar Status do Pedido'); ?></legend>
        <?php
            echo $this->Form->input('id', array('value' => $pedido['Pedido']['id']));
            echo $this->Form->input('pedido_status_id', array('options' => $pedido_status));
            //força somente a exibição de UM status, por causa do pagseguro
            //echo $this->Form->input('pedido_status_id',array('options'=>array($pedido['Pedido']['id']=>$pedido_status[$pedido['Pedido']['pedido_status_id']])));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Enviar', true)); ?>
</div>


<?php
echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.js', false);
echo $this->Html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false));
echo $javascript->link('admin/pedidos/crud.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
?>
<div class="index">
	<h2><?php __('Pedidos a serem Exportados'); ?></h2>
	<table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Usuário','usuario_id'); ?></th>
            <th><?php echo $this->Paginator->sort('valor_frete'); ?></th>
            <th><?php echo $this->Paginator->sort('valor_pedido'); ?></th>
            <th><?php echo $this->Paginator->sort('valor_desconto_meio_pagamento'); ?></th>
            <th><?php echo $this->Paginator->sort('entrega_tipo'); ?></th>
            <th><?php echo $this->Paginator->sort('entrega_prazo'); ?></th>
            <th><?php echo $this->Paginator->sort('parcelas'); ?></th>
            <th><?php echo $this->Paginator->sort('Status do Pedido','pedido_status_id'); ?></th>
             <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Condição de Pagamento','pagamento_condicoes_id'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $corStatus = array('','classAguardando','classAprovado','classCancelado','classNegado','classCanceladoUsuario','classEntregue');
        $i = 0;
        foreach ($pedidos as $pedido):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $pedido['Pedido']['id']; ?>&nbsp;</td>
                <td>
                <?php echo $this->Html->link($pedido['Usuario']['nome'], array('controller' => 'usuarios', 'action' => 'edit', $pedido['Usuario']['id'])); ?>
            </td>
            <td><?php echo $pedido['Pedido']['valor_frete']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['valor_pedido']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['valor_desconto_meio_pagamento']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['entrega_tipo']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['entrega_prazo']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['parcelas']; ?>&nbsp;</td>
            <td><div class="<?php echo $corStatus[$pedido['Pedido']['pedido_status_id']]; ?>"><?php echo $pedido['PedidoStatus']['nome']; ?></div></td>
            <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$pedido['Pedido']['created']); ?>&nbsp;</td>
            <td><?php echo $pedido['PagamentoCondicao']['nome']; ?>&nbsp;</td>
            <td class="actions">
				<?php echo $this->Html->link(__('Exportar', true), array('action' => 'export_datasul', $pedido['Pedido']['id']), null, sprintf(__('Deseja mesmo exportar o Pedido # %s?', true), $pedido['Pedido']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>

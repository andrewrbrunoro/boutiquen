<?php
echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.js', false);
echo $javascript->link('common/jsapi', false);
echo $this->Html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false));
echo $javascript->link('admin/pedidos/crud.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
?>
<div class="index">
	<h2><?php __('Pedidos'); ?></h2>
    <?php echo $form->create('Pedido', array('action'=>'/index','class'=>'formBusca'));?>
		<fieldset>
			<div class="w312 left">
			<?php
            echo $form->input('Filter.pedido_status_id', array('options' => array('' => 'Selecione') + $pedido_status));
            echo $form->input('Filter.pagamento_condicao_id', array('label' => 'Condição de Pgto', 'options' => array('' => 'Selecione') + $condicoes));
            echo $form->input('Filter.produto_id', array('type' => 'text', 'class' => 'w312', 'label' => 'Produto:'));
            echo $form->input('Filter.sku', array('type' => 'text', 'class' => 'w312', 'label' => 'SKU:'));
            echo $form->input('Filter.cupom', array('type' => 'text', 'label' => 'Cupom:', 'class' => 'w312'));
            echo $form->input('Filter.midia', array('type' => 'text', 'label' => 'Midia:', 'class' => 'w312'));
            echo $form->input('Filter.data_ini', array('class' => 'datePicker w147', 'label' => 'Data de:'));
            echo $form->input('Filter.data_fim', array('class' => 'datePicker w147', 'label' => 'Data até:'));
            echo $form->input('Filter.pedido', array('type' => 'text', 'label' => 'Número do Pedido:', 'class' => 'w312'));
            echo $form->input('Filter.midia', array('type' => 'text', 'label' => 'Midia:', 'class' => 'w312'));
            echo $form->input('Filter.nome', array('type' => 'text', 'label' => 'Nome ou Razão social:', 'class' => 'w312'));
            echo $form->input('Filter.email', array('type' => 'text', 'label' => 'E-mail:', 'class' => 'w312'));
            echo $form->input('Filter.cpf', array('type' => 'text', 'label' => 'CPF:', 'class' => 'w312'));
            echo $form->input('Filter.cnpj', array('type' => 'text', 'label' => 'CNPJ:', 'class' => 'w312'));

            ?>
            <div class="submit" style="clear: both;">
                <input name="submit" type="submit" class="button1" value="Busca" />
            </div>
            <div class="submit">
                <input name="submit" type="submit" class="button1" value="Exportar" />
            </div>
            <div class="submit">
                <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
            </div>
            <div class="submit">
                <input name="submit" type="submit" class="button" value="Relatório" />
            </div>
            <?php
            echo $form->end();
		
			$totalt		= 0;
			$cores		= array();
			
			foreach ($pedidostotais as $pedido){
				if(isset($cores[$pedido['Pedido']['pedido_status_id']])){
						$cores[$pedido['Pedido']['pedido_status_id']]['count']++; 
				}else{
					$cores[$pedido['Pedido']['pedido_status_id']]['count'] = 0;
				}
				$totalt += str_replace(',','.',str_replace('.','',$pedido['Pedido']['valor_pedido']));
			}	


			$status_arr = array();
			$cores_arr = array();
			foreach($pedidostatus as $v){
				$status_arr[] = "['".up(Inflector::slug( $v['PedidoStatus']['nome'],'_'))."', ".(isset($cores[$v['PedidoStatus']['id']]['count'])?$cores[$v['PedidoStatus']['id']]['count']:0)." ]";
				$cores_arr[] = "'#{$v['PedidoStatus']['cor']}'";
			}
		?>
		
		</div>
		<div style="width: 650px; float: right;">
			<!--Load the AJAX API-->
			
			<!--Div that will hold the pie chart-->
			<div id="chart_div" style="width:350; height:250"></div>
			<div class="totais" style="width:350px; height:100px;text-align:right;">
				<b>Total</b>: R$ <?php echo $this->String->bcoToMoeda($totalt); ?><br/>
				<b>Ticket Médio</b>: R$ <?php echo ((count($pedidostotais))>0)?$this->String->bcoToMoeda(($totalt/(count($pedidostotais)))):'0,00'; ?>
			</div>
		</div>
		</fieldset>
	
	<table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Usuário','usuario_id'); ?></th>
            <th><?php echo $this->Paginator->sort('Frete','valor_frete'); ?></th>
            <th><?php echo $this->Paginator->sort('Pedido','valor_pedido'); ?></th>
            <th><?php echo $this->Paginator->sort('Entrega','entrega_tipo'); ?></th>
            <th><?php echo $this->Paginator->sort('Prazo','entrega_prazo'); ?></th>
            <th><?php echo $this->Paginator->sort('Status do Pedido','pedido_status_id'); ?></th>
            <th><?php echo $this->Paginator->sort('Condição de Pagamento','pagamento_condicoes_id'); ?></th>   
			<th><?php echo $this->Paginator->sort('Midia', 'campanha'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($pedidos as $pedido):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $pedido['Pedido']['id']; ?>&nbsp;</td>
                <td>
                <?php echo $this->Html->link($pedido['Usuario']['nome'], array('controller' => 'usuarios', 'action' => 'edit', $pedido['Usuario']['id'])); ?>
            </td>
            <td><?php echo $pedido['Pedido']['valor_frete']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['valor_pedido']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['entrega_tipo']; ?>&nbsp;</td>
            <td><?php echo $pedido['Pedido']['entrega_prazo']; ?>&nbsp;</td>
            <td style="background:#<?php echo $pedido['PedidoStatus']['cor']; ?>"><?php echo $pedido['PedidoStatus']['nome']; ?></td>
            <td><?php echo $pedido['PagamentoCondicao']['nome']; ?>&nbsp;</td>
			<td><?php
                    $campanha = json_decode($pedido['Pedido']['campanha'], true);
                    echo!empty($campanha['midia']) ? $campanha['midia'] : '---';
                    ?>&nbsp;</td> 			
            <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$pedido['Pedido']['created']); ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Html->link(__('Visualizar', true), array('action' => 'view', $pedido['Pedido']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
		<script type="text/javascript">

				// Load the Visualization API and the piechart package.
				google.load('visualization', '1.0', {'packages':['corechart']});

				// Set a callback to run when the Google Visualization API is loaded.
				google.setOnLoadCallback(drawChart);


				// Callback that creates and populates a data table, 
				// instantiates the pie chart, passes in the data and
				// draws it.
				function drawChart(){
					// Create the data table.
					var data = new google.visualization.DataTable();
					data.addColumn('string', 'Topping');
					data.addColumn('number', 'Slices');
					
					data.addRows([
					<?php echo implode(",",$status_arr); ?>
					]);
					
					// Set chart options
					var options = {
						'colors':<?php echo "[".implode(",",$cores_arr)."]"; ?>,
						'title':'Transações concluidas',
						'width':550,
						'height':250
					};

					// Instantiate and draw our chart, passing in some options.
					var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
					chart.draw(data, options);
				}
			</script>
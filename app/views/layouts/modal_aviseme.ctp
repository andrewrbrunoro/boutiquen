
 <style type="text/css">

    .header {
        background: url('../../img/site/bg_header.jpg') repeat-x scroll left top #FFF;
        min-height: 150px !important;
        margin-bottom: 1px;
    }

    .logo {
        width: 280px;
        float: none !important;
    }

     .nyroModalCont {
        width: 450px;
        height: 300px;
        overflow: hidden !important;
    }

    body { overflow-x: hidden !important; }
    body { overflow-y: hidden !important; }

   /* html{
        overflow-x: hidden !important; 
        overflow-y: hidden !important;
    }*/

    .flash_success {
        width: 445px !important;
        margin-left: 0px !important;
    }

    .flash_error {
        width: 445px !important;
        margin-left: 0px !important;
    }    

    .container {
        /*padding: 120px 8px 85px !important;*/
    }    
 </style>




<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php
            if (isset($seo_title)):
                $title_site = $seo_title;
                echo $seo_title;
            else:
                ?>
                <?php
                if (isset($title_for_layout_produto)) {
                    $title_site = $title_for_layout_produto;
                    echo $title_for_layout_produto;
                } else {
                    if ($this->params['controller'] != "home") {
                        $title_site = $title_for_layout . ' - ' . Configure::read('Loja.titulo');
                        echo $title_for_layout . ' - ' . Configure::read('Loja.titulo');
                    } else {
                        $title_site = Configure::read('Loja.titulo');
                        echo Configure::read('Loja.titulo');
                    }
                }
                ?>
            <?php endIf; ?>
        </title>
        <?php if (isset($seo_meta_description)): ?>
            <meta name="description" content="<?php echo $seo_meta_description; ?>" />
            <meta property="og:description" content="<?php echo $seo_meta_description; ?>"/>
        <?php else: ?>
            <meta name="description" content="<?php echo Configure::read('Loja.seo_meta_description'); ?>" />
            <meta property="og:description" content="<?php echo Configure::read('Loja.seo_meta_description'); ?>"/>
        <?php endIf; ?>

        <meta property="og:image" content="<?php echo $this->Html->Url("/img/site/logo_face.jpg", true); ?>"/>
        <meta property="og:site_name" content="<?php echo $title_site; ?>"/>
        <meta property="og:title" content="<?php echo $title_site; ?>"/>
        <meta property="og:url" content="<?php echo $this->Html->Url("/", true); ?>"/>
        
        <link href='http://fonts.googleapis.com/css?family=Nunito:300' rel='stylesheet' type='text/css'>

        <?php if (Configure::read('Reweb.webmaster_id') != ""): ?>
            <meta name="google-site-verification" content="<?php echo Configure::read('Reweb.webmaster_id'); ?>" />
            <?php endIf; ?>

        <?php if (isset($seo_meta_keywords)): ?>
            <meta name="keywords" content="<?php echo $seo_meta_keywords; ?>" />
        <?php else: ?>
            <meta name="keywords" content="<?php echo Configure::read('Loja.seo_meta_keywords'); ?>" />
        <?php endIf; ?>

        <?php echo $this->element('common/js_path'); ?>
        <?php
        echo $this->Html->meta('icon');
        echo $this->Javascript->link('common/jquery.js');
        echo $this->Javascript->link('common/jquery-ui-1.8.16.custom.min.js');
        echo $this->Javascript->link('common/use_default.js');
        echo $this->Javascript->link('site/common/jquery.cycle.all.js');
        echo $this->Javascript->link('common/jcarousel/jquery.jcarousel.min.js');
        echo $this->Javascript->link('site/common/index.js');
        echo $this->Javascript->link('site/common/https');
        echo $this->Html->css('site/style.css');
        ?>
        <!--[if IE 7]>
            <?php echo $this->Html->css('site/ie7.css'); ?>
        <![endif]-->
         <!--[if IE 8]>
            <?php echo $this->Html->css('site/ie8.css'); ?>
        <![endif]-->
        <!--[if IE 9]>
            <?php echo $this->Html->css('site/ie.css'); ?>
        <![endif]-->
        <?php
            echo $scripts_for_layout;
        ?>
    </head>
    
    <body class="<?php echo ($this->params["controller"] == "home") ? "homepage" : ""; ?>">
        <noscript>
            <div class="flash_error">
                <p>Detectamos que você está com o recurso "Javascript" desabilitado no seu navegador.</p>
                <p>Para acessar corretamente o site, é necessário usar um navegador que ofereça suporte ao JavaScript. Para obter instruções detalhadas sobre como ativar o JavaScript, consulte a Ajuda em seu navegador.</p>
            </div>
        </noscript>
        <div id="message_top">
            <?php
            echo $this->Session->flash();
            //if (isset($referer) && $referer == '/login') {
            echo $this->Session->flash('auth');
            //}
            ?>
            <div class="box">
                
            </div>
        </div>
        <!-- start container -->
        <div class="container">
            <!-- start container-in -->
            <div class="container-in">
                <!-- start box -->
                <div class="box">                    
                    <?php echo $content_for_layout; ?>
                
                </div>
                <!-- end box -->
            </div>
            <!-- end container-in -->
        </div>       
        
    </body>
</html>
<?php
    /**
     *
     * PHP versions 4 and 5
     *
     * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
     * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
     * @link          http://cakephp.org CakePHP(tm) Project
     * @package       cake
     * @subpackage    cake.cake.libs.view.templates.layouts
     * @since         CakePHP(tm) v 0.10.0.1076
     * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
     */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php
        echo $this->Html->charset();
    ?>
    <title>
        <?php
            if (isset($title_for_layout_produto)) {
                echo $title_for_layout_produto;
            } else {
                echo Configure::read('Loja.nome') . '-' . $title_for_layout;
            }
        ?>
    </title>
    <?php echo $this->element('common/js_path'); ?>
    <?php
        echo $this->Html->meta('icon');
        echo $this->Javascript->link('common/jquery.js');
        echo $this->Javascript->link('common/jquery-ui-1.8.16.custom.min.js');
        echo $this->Javascript->link('common/use_default.js');
        echo $this->Javascript->link('common/jquery.slideshow.js');
        echo $this->Javascript->link('common/jcarousel/jquery.jcarousel.min.js');
        echo $this->Javascript->link('site/common/index.js');
        echo $this->Html->css('site/style.css');
        echo $scripts_for_layout;
    ?>
    <!--[if lte IE 6]>
    <?php
        echo $this->Javascript->link('common/pngfix.js');
    echo $this->Javascript->link('common/ie6.js');
    echo $this->Html->css('site/ie6.css');
    ?>
    <![endif]-->
</head>
<body>
<!-- Layout Carrinho -->
<?php if (($this->params['controller'] == "carrinho" && $this->params['action'] == "index") || $this->params['controller'] == "usuarios"): ?>
    <!-- start box -->
    <div id="box">
        <?php echo $this->element('site/header'); ?>
    </div>
    <!-- end box -->
<?php endIf; ?>

<!-- start container -->
<div id="container_main">
    <div id="container_wrap">
        <div id="container_hp">
            <?php
                $areas_sem_menu = array(
                    'paginas',
                    'contato',
                    'usuarios',
                    'carrinho',
                    'usuario_enderecos'
                );
                $areas_menu_institucional = array(
                    'paginas',
                    'contato'
                );
                if (!in_array($this->params['controller'], $areas_sem_menu)) {
                    echo $this->element('site/left');
                } else if (in_array($this->params['controller'], $areas_menu_institucional)) {
                    echo $this->element('site/left-institucional');
                }
            ?>
            <!-- start leftcol -->
            <div id="leftcol_hp">
                <?php
                    $areas_sem_menu_conta = array(
                        'meus_pedidos',
                        'edit',
                        'add'
                    );
                    $areas_sem_menu_conta_c = array(
                        'carrinho',
                        'usuarios',
                        'usuario_enderecos'
                    );
                    if (!in_array($this->params['action'], $areas_sem_menu_conta) || in_array($this->params['controller'], $areas_sem_menu_conta_c)) {
                        ?>

                        <?php if ($this->params['controller'] != "usuarios" && $this->params['controller'] != "carrinho") { ?>

                            <!-- start bt_leftcol_top -->
                            <div class="bt_leftcol_top">
                                <a href="<?php e($this->Html->url("/carrinho")) ?>" title="CARRINHO DE COMPRA"><strong>CARRINHO
                                        DE COMPRA</strong></a>
                            </div>
                            <!-- end bt_leftcol_top -->

                            <?php
                        }

                    } else {
                        ?>
                        <!-- start sub navigation -->
                        <div id="sub_navigation">
                            <div class="gray_box">
                                <div class="gray_box_top">
                                    <div class="gray_box_bottom">
                                        <ul>
                                            <li>MINHA CONTA</li>
                                            <li><a href="<?php e($this->Html->url("/usuarios/edit")) ?>"
                                                   title="MEUS DADOS">MEUS DADOS</a></li>
                                            <li><a href="<?php e($this->Html->url("/carrinho/meus_pedidos")) ?>"
                                                   title="MEUS PEDIDOS">MEUS PEDIDOS</a></li>
                                            <?php if (isset($auth['Usuario']['nome'])) { ?>
                                                <li class="last"><a href="<?php e($this->Html->url("/logout")) ?>"
                                                                    title="SAIR">SAIR</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end sub navigation -->
                        <!-- start receba_offer -->
                        <div id="receba_offer">
                            <div class="gray_box">
                                <div class="gray_box_top">
                                    <div class="gray_box_bottom">
                                        <!-- row -->
                                        <div class="row">
                                            <h3>RECEBA OFERTAS</h3>
                                            <?php echo $this->Form->create(null, array(
                                                'id'  => 'newsbox',
                                                'url' => '/newsletter'
                                            )); ?>
                                            <?php echo $this->Form->input('Newsletter.nome', array(
                                                'rel'   => 'Nome',
                                                'class' => 'input useDefault',
                                                'div'   => false,
                                                'label' => false
                                            )); ?>
                                            <?php echo $this->Form->input('Newsletter.email', array(
                                                'rel'   => 'Email',
                                                'div'   => false,
                                                'class' => 'input useDefault',
                                                'label' => false
                                            )); ?>
                                            <input name="" type="submit" value="" class="link"/>

                                            <div class="clear"></div>
                                            <?php echo $this->Form->end() ?>
                                        </div>
                                        <!-- row -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end receba_offer -->
                        <?php
                    }
                ?>
            </div>
            <!-- end leftcol -->
            <?php
                if (!in_array($this->params['action'], $areas_sem_menu_conta) || in_array($this->params['controller'], $areas_sem_menu_conta_c)) {

                    if ($this->params['controller'] != "usuarios" && $this->params['controller'] != "carrinho") {
                        ?>
                        <!-- start rightcol -->
                        <div id="rightcol_hp">
                            <?php // echo $this->element('site/search');?>
                        </div>
                        <!-- end rightcol -->
                        <?php
                    }

                }
            ?>
            <?php echo $content_for_layout; ?>
        </div>
    </div>
</div>
<!-- end container -->

<?php

    if ($this->params['action'] != "forma_pagamento" && $this->params['action'] != "entrega" && $this->params['action'] != "finalizacao")
        echo $this->element('site/footer');

?>
</body>
</html>
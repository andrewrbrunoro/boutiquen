<?php echo $this->Html->script('common/jquery.meio_mask.js', array('inline' => false)); ?>
<?php echo $this->Html->script('site/usuarios/crud.js', array('inline' => false)); ?>

<div class="fillform-1st">
    <div class="fillform3 outer-border">
        <h3>CADASTRAR NOVO ENDEREÇO</h3>
        <!-- start inner border -->
        <div class="form-in">
            <?php echo $this->Form->create('UsuarioEndereco', array('class' => 'form-content Identification')); ?>
            <div class="inner-box">
                <div class="row1">
                    <label style="display:block;">Tipo de endereço *:</label>
                    <?php echo $this->Form->input('UsuarioEndereco.cobranca', array(
                        'div'     => false,
                        'legend'  => false,
                        'id'      => 'UsuarioEnderecoCobranca',
                        'class'   => 'radio left',
                        'type'    => 'radio',
                        'options' => array(
                            0 => 'Endereço de entrega'
                        ),
                        'default' => 0
                    )); ?>
                    <div class="clear"></div>
                </div>
                <!-- end row1 -->
                <div class="row">
                    <div class="col">
                    <?php echo $this->Form->input('UsuarioEndereco.id', array(
                        'id'    => 'UsuarioEnderecoId',
                        'label' => false
                    )); ?>
                    </div>
                </div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.cep', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoCep',
                        'class' => 'form-control input input1 width last left mask-cep',
                        'rel'   => 'Cep * (somente números)',
                        'label' => 'Cep * (somente números)'
                    )); ?>
                    <div class="clear"></div>
						<span class="sub-input">
							Não sabe seu CEP? <a href="http://www.buscacep.correios.com.br/" target="_blank">Clique
                                aqui.</a>
                            <?php echo $this->Html->image('/img/site/zoomloader.gif', array(
                                'id'    => 'loading',
                                'style' => 'visibility: hidden; float: right; margin-right: 345px; margin-top: 10px; position: absolute;',
                                'alt'   => 'Carregando',
                                'title' => 'Carregando'
                            )); ?>
						</span>
                </div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.nome', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoNome',
                        'class' => 'form-control input input1',
                        'label' => 'Identificação do Endereço *'
                    )); ?>
                    <div class="clear"></div>
                    <span class="sub-input">(Ex: Residencial, Comercial, etc.)</span>
                </div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.rua', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoRua',
                        'class' => 'form-control input input1',
                        'label' => 'Endereço *'
                    )); ?>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.numero', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoNumero',
                        'class' => 'form-control input input1 width',
                        'label' => 'Número *'
                    )); ?>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.complemento', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoComplemento',
                        'class' => 'form-control input input1 width',
                        'label' => 'Complemento'
                    )); ?>
                    <div class="clear"></div>
                    <span class="sub-input">(OPCIONAL)</span>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.bairro', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoBairro',
                        'class' => 'form-control input input1',
                        'label' => 'Bairro *'
                    )); ?>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.uf', array(
                        'div'     => false,
                        'id'      => 'UsuarioEnderecoUf',
                        'class'   => 'form-control input input1 width select3',
                        'style'   => 'height: 30px;',
                        'label'   => 'Estado *',
                        'options' => $this->Estados->estadosBrasileiros()
                    )); ?>
                </div>
                <div class="clear"></div>
                <div class="row">
                    <?php echo $this->Form->input('UsuarioEndereco.cidade', array(
                        'div'   => false,
                        'id'    => 'UsuarioEnderecoCidade',
                        'class' => 'form-control input input1 width',
                        'label' => 'Cidade *'
                    )); ?>
                </div>
                <div class="clear"></div>
            </div>
            <br /><br />
            <button type="submit" class="button">Salvar</button>

            <?php echo $this->Form->end(); ?>
        </div>
        <!-- end inner border -->
    </div>
    <!-- end form1 -->
</div>




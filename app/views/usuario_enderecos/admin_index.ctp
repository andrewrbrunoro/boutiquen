<div class="index">
    <h2 class="left"><?php __('Usuário Enderecos'); ?></h2>
    <div class="btAddProduto">
        <?php echo $this->Html->link(__('[+] Adicionar Endereco', true), array('action' => 'add', 'user_id:' . $this->params['named']['user_id'])); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('usuario_id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('cep'); ?></th>
            <th><?php echo $this->Paginator->sort('cobranca'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($usuarioEnderecos as $usuarioEndereco):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?> 
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $usuarioEndereco['UsuarioEndereco']['id']; ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($usuarioEndereco['Usuario']['nome'], array('controller' => 'usuarios', 'action' => 'edit', $usuarioEndereco['Usuario']['id'])); ?>
                </td>
                <td><?php echo $usuarioEndereco['UsuarioEndereco']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo $usuarioEndereco['UsuarioEndereco']['cep']; ?>&nbsp;</td>
                <td align="center"><?php echo ($usuarioEndereco['UsuarioEndereco']['cobranca']) ? 'Sim' : 'Não'; ?>&nbsp;</td>
                <td align="center"><?php echo ($usuarioEndereco['UsuarioEndereco']['status']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $usuarioEndereco['UsuarioEndereco']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $usuarioEndereco['UsuarioEndereco']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $usuarioEndereco['UsuarioEndereco']['id'], 'user_id:' . $usuarioEndereco['UsuarioEndereco']['usuario_id'])); ?>
                    <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $usuarioEndereco['UsuarioEndereco']['id'], 'user_id:' . $usuarioEndereco['UsuarioEndereco']['usuario_id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $usuarioEndereco['UsuarioEndereco']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>


    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>

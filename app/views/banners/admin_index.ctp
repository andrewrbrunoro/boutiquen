
<?php echo $javascript->link('admin/common/swfobject.js', false); ?>
<div class="index">
    <h2 class="left"><?php __('Banners'); ?></h2>
    <div class="btAddProduto">
        <?php echo $this->Html->link(__('[+] Adicionar Banner', true), array('action' => 'add')); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('banner_tipo_id'); ?></th>
            <th><?php echo $this->Paginator->sort('Arquivo', 'filename'); ?></th>
            <th><?php echo $this->Paginator->sort('Tamanho', 'filesize'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th><?php echo $this->Paginator->sort('Exibir de', 'data_inicio'); ?></th>
            <th><?php echo $this->Paginator->sort('Exibir até', 'data_fim'); ?></th>
            <th><?php echo $this->Paginator->sort('Status', 'status'); ?></th>
            <th><?php echo $this->Paginator->sort('capa'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($banners as $banners):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $banners['Banner']['id']; ?>&nbsp;</td>
                <td align="center"><?php echo $banners['BannerTipo']['nome'] ?>&nbsp;</td>
                <td align="center" style="padding:5px"><?php
                    if ($banners['Banner']['filename'] != "" && $banners['Banner']['mimetype'] == 'application/x-shockwave-flash') {
                        echo $flash->renderSwf($banners['Banner']['dir'] . DS . $banners['Banner']['filename'], 100, 100, false);
                    } elseif ($banners['Banner']['filename'] != "") {
                        echo $image->resize($banners['Banner']['dir'] . DS . $banners['Banner']['filename'], 100, 100, true);
                    }
                    ?>&nbsp;</td>
                <td align="center"><?php echo $banners['Banner']['filesize']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $banners['Banner']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $banners['Banner']['modified']); ?>&nbsp;</td>
                <td align="center">
                    <?php if (null != $banners['Banner']['data_inicio']) { ?>
                        <?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $banners['Banner']['data_inicio']); ?>&nbsp;
                    <?php } else { ?>
                        <?php echo 'Sem Data Inicial' ?>&nbsp;
                    <?php } ?>
                </td>
                <td align="center">
                    <?php if (null != $banners['Banner']['data_fim']) { ?>
                        <?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $banners['Banner']['data_fim']); ?>&nbsp;
                    <?php } else { ?>
                        <?php echo 'Sem Data final' ?>&nbsp;
                    <?php } ?>
                </td>
                <td align="center"><?php echo ($banners['Banner']['status']) ? 'Sim' : 'Não'; ?>&nbsp;</td>
                <td align="center"><?php echo ($banners['Banner']['capa']) ? 'Sim' : 'Não'; ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $banners['Banner']['id'])); ?>
                    <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $banners['Banner']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $banners['Banner']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>

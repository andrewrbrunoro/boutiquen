<?php echo $javascript->link('admin/banners/crud.js', false); ?>
<?php echo $javascript->link('common/jquery-ui-1.8.16.custom.min', false); ?>
<?php echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css'); ?>
<div class="index">
    <?php echo $this->Form->create('Banner', array('url' => '/admin/banners/add', 'type' => 'file')); ?>
    <fieldset>
        <legend><?php __('Adicionar Banners'); ?></legend>
        <?php
        echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo "<br class='clear' />";
			echo $this->Form->input('banner_tipo_id', array('class' => 'produtosSel bannerSel'));
        echo $this->Form->input('link', array('class' => 'w312 inputs'));
        echo $this->Form->input('ordem', array('class' => 'w312 inputs'));
        echo $this->Form->input('data_inicio', array('type'=>'text', 'class' => 'datePicker inputs w147', 'label' => 'Ativo de:'));
        echo $this->Form->input('data_fim', array('type'=>'text',  'class' => 'datePicker inputs w147', 'label' => 'Ativo até:'));
        echo $this->Form->input('capa', array('class' => 'banner-tipo', 'default' => true, 'type' => 'radio', 'options' => array(true => 'Sim', false => 'Não')));
        echo $this->Form->input('Categoria', array('class' => 'h400', 'label' => 'Categorias'));
        echo $this->Form->input('filename', array('label' => 'Arquivo', 'type' => 'file'));
        echo $this->Form->input('dir', array('type' => 'hidden'));
        echo $this->Form->input('mimetype', array('type' => 'hidden'));
        echo $this->Form->input('filesize', array('type' => 'hidden'));
        echo $this->Form->end(__('Inserir', true));
        ?>
    </fieldset>
</div> 

<style type="text/css">
    .i-check {
        border: none;
        margin-left: -19px;
    }
</style>


<div class="container">
    <header class="page-header">
        <h1 class="page-title"><?php echo $category['Categoria']['nome'] ?></h1>
        <ol class="breadcrumb page-breadcrumb">
            <li>
                <a href="<?php echo $this->Html->Url('/') ?>">
                    Página inicial
                </a>
            </li>

            <?php if (isset($category['FatherCategory']['nome'])) { ?>
                <li>
                    <a href="<?php echo $this->Html->Url('/categorias/' . $category['FatherCategory']['seo_url']) ?>">
                        <?php echo $category['FatherCategory']['nome'] ?>
                    </a>
                </li>
            <?php } ?>

            <li class="active">
                <?php echo $category['Categoria']['nome'] ?>
            </li>
        </ol>
        <ul class="category-selections clearfix">
            <li>
                <a class="fa fa-th-large category-selections-icon active"
                   href="#"></a>
            </li>
            <li>
                <a class="fa fa-th-list category-selections-icon"
                   href="#"></a>
            </li>
            <li><span class="category-selections-sign">Sort by :</span>
                <select class="category-selections-select">
                    <option selected>Newest First</option>
                    <option>Best Sellers</option>
                    <option>Trending Now</option>
                    <option>Best Raited</option>
                    <option>Price : Lowest First</option>
                    <option>Price : Highest First</option>
                    <option>Title : A - Z</option>
                    <option>Title : Z - A</option>
                </select>
            </li>
            <li><span class="category-selections-sign">Items :</span>
                <select class="category-selections-select">
                    <option>9 / page</option>
                    <option selected>12 / page</option>
                    <option>18 / page</option>
                    <option>All</option>
                </select>
            </li>
        </ul>
    </header>
    <div class="row">
        <div class="col-md-3">
            <aside class="category-filters category-filters-inverse">
                <div class="category-filters-section">
                    <h3 class="widget-title-sm">Preço</h3>
                    <input type="text"
                           id="price-slider"
                           data-min="<?php echo $min ?>"
                           data-max="<?php echo $max ?>"/>
                </div>
                <div class="category-filters-section">
                    <h3 class="widget-title-sm">Categorias</h3>

                    <?php foreach ($categorias as $categoria) { ?>
                        <?php if (isset($categoria['SubCategory']) && count($categoria['SubCategory'])) { ?>
                            <?php foreach ($categoria['SubCategory'] as $subcategory) { ?>
                                <div class="checkbox"
                                     style="margin-left: -20px">
                                        <a href="<?php echo $this->Html->Url('/' . $subcategory['seo_url']) ?>"
                                           style="color: #CCC !important;">
                                            <?php echo $subcategory['nome'] ?>
                                        </a>
                                    </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>

                </div>
<!--                <div class="category-filters-section">-->
<!--                    <h3 class="widget-title-sm">Cor</h3>-->
<!--                    <div class="checkbox">-->
<!--                        <label>-->
<!--                            <input class="i-check"-->
<!--                                   type="checkbox"/>10% Off or More<span class="category-filters-amount">(86)</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="category-filters-section">-->
<!--                    <h3 class="widget-title-sm">Tamanho</h3>-->
<!--                    <div class="checkbox">-->
<!--                        <label>-->
<!--                            <input class="i-check"-->
<!--                                   type="checkbox"/>New<span class="category-filters-amount">(49)</span>-->
<!--                        </label>-->
<!--                    </div>-->
<!--                </div>-->
            </aside>
        </div>
        <div class="col-md-9">
            <div class="row"
                 data-gutter="15">
                <?php
                    $i = 1;
                    foreach ($products as $product) {
                        $product['FreeShipping'] = isset($product[0]) ? $product[0]['free_shipping'] : 0;
                        echo $this->element('site/product/big', $product);
                        echo $i % 3 == 0 ? '</div><div class="clearfix"></div><div class="row" data-gutter="15">' : '';
                        $i++;
                    }
                ?>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <ul class="pagination category-pagination pull-right">
                            <?php for ($i = 1; $i < $this->Paginator->counter(array('format' => '%pages%')); $i++) { ?>
                                <?php
                                if (isset($this->params['page']) && $this->params['page'] == $i || isset($this->params['page']) && $this->params['page'] == 1 && $i == 1) {
                                    $class = ' class="active"';
                                } else {
                                    $class = '';
                                }
                                ?>
                                <li<?php echo $class; ?>>
                                    <a href="<?php echo $this->Html->Url('/categoria/' . $category['Categoria']['seo_url'] . '/filtros/' . $i) ?>">
                                        <?php echo $i ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="gap"></div>

<?php echo $this->element('site/manufacturers'); ?>

<?php echo $this->addScript($this->Javascript->link('category')) ?>
<div class="index">
<?php
echo $form->create('Categoria',array('type' => 'file', 'action'=>'add'));
echo $this->Form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
echo $this->Form->input('destaque_home',array('type'=>'radio','options'=>array(true=>'Sim',false=>'Não')));
?>
    <fieldset>
        <legend><?php __('Editar Categorias'); ?></legend>
        <?php 
            echo $this->Form->input('nome',array('class'=>'w312'));
            echo $this->Form->input('parent_id',array('label'=>'Categoria Pai'));
            echo $this->Form->input('sort',array('label'=>'Ordem','class'=>'w147'));
            echo $this->Form->input('descricao',array('label'=>'Descrição','class'=>'w312'));
        ?>
        <legend>SEO</legend>
        <?php echo $this->Form->input('seo_title', array('label' => 'Seo Title', 'class' => 'inputs w312')); ?>
        <?php echo $this->Form->input('seo_institucional', array('label' => 'Seo Institucional', 'class' => 'inputs w312')); ?>
        <?php echo $this->Form->input('seo_meta_description', array('label' => 'Seo Meta Description', 'class' => 'inputs w312')); ?>
        <?php echo $this->Form->input('seo_meta_keywords', array('label' => 'Seo Meta Keywords', 'class' => 'inputs w312')); ?>
        <legend>Imagem</legend>
        <?php
            echo $this->Form->input('Categoria.thumb_filename', array('type' => 'file'));
            echo $this->Form->input('Categoria.thumb_dir', array('type' => 'hidden'));
            echo $this->Form->input('Categoria.thumb_mimetype', array('type' => 'hidden'));
            echo $this->Form->input('Categoria.thumb_filesize', array('type' => 'hidden'));
            //echo $this->Form->input('CategoriaAtributo', array( 'options' => $atributos));
        echo $form->end('Inserir');
        ?>
    </fieldset>
</div>

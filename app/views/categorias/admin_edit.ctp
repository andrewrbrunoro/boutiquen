<div class="index">
    <?php
    echo $form->create('Categoria', array('type' => 'file', 'action' => 'edit/' . $this->params['pass'][0]));
    ?>
    <fieldset>
        <legend><?php __('Editar Categoria'); ?></legend>
        <?php
            echo $this->Form->input('id');
            echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
            echo $this->Form->input('destaque_home', array('type' => 'radio', 'options' => array(true => 'Sim', false => 'Não')));
            echo $this->Form->input('nome', array('class' => 'w312'));
            echo $this->Form->input('descricao', array('label' => 'Descrição', 'class' => 'w312'));
            echo $this->Form->input('parent_id', array('label' => 'Categoria Pai'));
            echo $this->Form->input('sort', array('label' => 'Ordem', 'class' => 'w147'));
        ?>
        <legend>SEO</legend>
        <?php echo $this->Form->input('seo_title', array('label' => 'Seo Title', 'class' => 'inputs')); ?>
        <?php echo $this->Form->input('seo_institucional', array('label' => 'Seo Institucional', 'class' => 'inputs')); ?>
        <?php echo $this->Form->input('seo_meta_description', array('label' => 'Seo Meta Description', 'class' => 'inputs')); ?>
        <?php echo $this->Form->input('seo_meta_keywords', array('label' => 'Seo Meta Keywords', 'class' => 'inputs')); ?>
        <legend>Thumb</legend>
        <?php
            $img = ( isset($this->data['Categoria']['thumb_filename']) ) ? $this->data['Categoria']['thumb_dir'] . '/' . $this->data['Categoria']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
            echo $this->Form->input('Categoria.thumb_filename', array('type' => 'file'));
            echo $image->resize($img, 80, 80);
            echo $this->Form->input('Categoria.thumb_dir', array('type' => 'hidden'));
            echo $this->Form->input('Categoria.thumb_mimetype', array('type' => 'hidden'));
            echo $this->Form->input('Categoria.thumb_filesize', array('type' => 'hidden'));
            if (isset($this->data['Categoria']['thumb_filename'])) {
                echo $form->input('Categoria.thumb_remove', array('type' => 'checkbox'));
            }
        ?>
        <?php echo $form->end('Salvar'); ?>
    </fieldset>
</div>

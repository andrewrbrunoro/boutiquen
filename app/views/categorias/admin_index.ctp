<?php echo $javascript->link('common/tree_view/jquery.treeview.js', false); ?>
<?php echo $javascript->link('admin/categorias/index.js', false); ?>

<?php echo $this->Html->css('common/tree_view/jquery.tree_view', null, array('inline' => false)); ?>
<div class="index">
    <h2><?php __('Categorias'); ?></h2>

    <div class="btAddProduto clear">
        <?php echo $this->Html->link(__('[+] Adicionar Categoria', true), array('action' => 'add')); ?>
    </div>
    <br class="clear"/>
    <?php echo $tree->show('Categoria/nome', $this->requestAction('/admin/categorias/menu'), ' id="catlist" class="tree"', '/admin/categorias/edit/', false, false, 2); ?>
</div>

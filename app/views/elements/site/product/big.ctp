<?php if (isset($Produto) && isset($Grade)) { ?>
    <?php $url = low(Inflector::slug($Produto['nome'], '-')) . '-prod-' . $Produto['grade_id'] . '.html'; ?>
    <div class="col-md-4">
        <div class="product">
            <ul class="product-labels">
                <?php if (isset($FreeShipping) && $FreeShipping == 1) { ?>
                    <li>Frete Grátis</li>
                <?php } ?>
                <?php
                    if (strstr($Produto['tag'], ',')) {
                        $tags = explode(',', $Produto['tag']);
                        foreach ($tags as $tag) {
                            echo !empty($tag) ? '<li>' . $tag . '</li>' : '';
                        }
                    }
                ?>
                <?php if (Configure::read('Loja.desconto_geral') != '') { ?>
                    <li>desconto de <?php echo Configure::read('Loja.desconto_geral'); ?>%</li>
                <?php } ?>
            </ul>
            <div class="product-img-wrap">
                <?php
                    $i = 0;
                    foreach ($this->String->getImages($Produto['grade_id']) as $image) {
//                        if (file_exists('uploads/produto_imagem/filename/' . $image['GradeImagem']['filename'])) {
//                            echo $this->Html->image('/uploads/produto_imagem/filename/' . $image['GradeImagem']['filename'],
//                                array(
//                                    'alt'   => $Produto['nome'],
//                                    'title' => $Produto['nome'],
//                                    'class' => $i % 2 == 1 ? 'product-img-primary' : 'product-img-alt'
//                                )
//                            );
//                        } else {
//                            echo $this->Html->image('/img/500x500.png', array('alt' => $Produto['nome'], 'title' => $Produto['nome'], 'class' => $i % 2 == 1 ? 'product-img-primary' : 'product-img-alt'));
//                        }
                        echo $this->Html->image('http://boutiqueno.com.br/uploads/produto_imagem/filename/' . $image['GradeImagem']['filename'],
                            array(
                                'alt'   => $Produto['nome'],
                                'title' => $Produto['nome'],
                                'class' => $i % 2 == 1 ? 'product-img-primary' : 'product-img-alt'
                            )
                        );
                        $i++;
                    }
                ?>
            </div>
            <a class="product-link"
               href="<?php echo $this->Html->Url('/'.$url); ?>"></a>
            <div class="product-caption">
                <h5 class="product-caption-title">
                    <?php echo $Produto['nome']; ?>
                </h5>
                <div class="product-caption-price">
                    <?php if ($Grade['preco_promocao'] != '' && $Grade['preco_promocao'] > 0.00) { ?>
                        <span class="product-caption-price-old">
                            R$ <?php echo $this->String->bcoToMoeda($Grade['preco']); ?>
                        </span>
                        <span class="product-caption-price-new">
                            R$ <?php echo $this->String->bcoToMoeda($Grade['preco_promocao']); ?>
                        </span>
                    <?php } ?>
                    <span class="product-caption-price-new">
                        R$ <?php echo $this->String->bcoToMoeda($Grade['preco']); ?>
                    </span>

                    <?php
                        if (isset($parcelamento)) {
                            $parcela_valida = $this->Parcelamento->parcelaValida($Grade['preco_promocao'] != '' && $Grade['preco_promocao'] > 0 ? $Grade['preco_promocao'] : $Grade['preco'], $parcelamento);
                            if (isset($parcela_valida) && $parcela_valida['parcela'] > 1) {
                                ?>
                                <p>
                                    ou <strong class="quantity-plots"><?php echo $parcela_valida['parcela']; ?>x</strong> de
                                    <strong class="product-price-plots">
                                        R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor']) ?>
                                    </strong>
                                </p>
                                <?php
                            }
                        }
                    ?>
                </div>
                <ul class="product-caption-feature-list">
                    <li>
                        <?php echo isset($FreeShipping) && $FreeShipping == 1 ? 'Frete Grátis' : ''; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
<?php } ?>
<div class="copyright-area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="copyright-text">
                    Proibida a reprodução total ou parcial. Preços e estoques sujeito a alterações sem aviso prévio. <br />
                    ©Todos os direitos reservados. Boutique Nô - CNPJ 09.044.817/0001-36
                </p>
            </div>
            <div class="col-md-6">
                <ul class="payment-icons-list">
                    <li>
                        <img src="<?php echo $this->Html->Url('/img/payment/visa-straight-32px.png') ?>" alt="Image Alternative text" title="Pague com Visa" />
                    </li>
                    <li>
                        <img src="<?php echo $this->Html->Url('/img/payment/mastercard-straight-32px.png') ?>" alt="Image Alternative text" title="Pague com MasterCard" />
                    </li>
                    <li>
                        <img src="<?php echo $this->Html->Url('/img/payment/maestro-straight-32px.png') ?>" alt="Image Alternative text" title="Pague com Maestro" />
                    </li>
                    <li>
                        <img src="<?php echo $this->Html->Url('/img/payment/discover-straight-32px.png') ?>" alt="Image Alternative text" title="Pague com Discover" />
                    </li>
                    <li>
                        <img src="<?php echo $this->Html->Url('/img/payment/american-express-straight-32px.png') ?>" alt="Image Alternative text" title="Pague com American Express" />
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
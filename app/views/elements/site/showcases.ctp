<?php if (isset($Vitrine) && isset($ProdutoVitrine)) { ?>
    <div class="container">
        <h3 class="widget-title-lg"><?php echo $Vitrine['nome']; ?></h3>
        <div class="row" data-gutter="15">
            <?php
                $i = 1;
                foreach ($this->String->getProducts(array_column($ProdutoVitrine, 'produto_id')) as $product) {
                    $product['FreeShipping'] = isset($product[0]) ? $product[0]['free_shipping'] : 0;
                    echo $this->element('site/product/big', $product);
                    echo $i % 3 == 0 ? '</div><div class="clearfix"></div><div class="row" data-gutter="15">' : '';
                    $i++;
                }
            ?>
        </div>
    </div>
<?php } ?>
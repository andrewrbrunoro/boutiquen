<?php if (isset($banners)) { ?>
    <div class="owl-carousel owl-loaded owl-nav-dots-inner"
         data-options='{"items":1,"loop":true}'>
        <?php foreach ($banners as $banner) { ?>
            <div class="owl-item">
                <?php $json = isset($banner['Banner']['json_texto']) ? json_decode($banner['Banner']['json_texto'], true) : array(); ?>
                <div class="slider-item"
                     style="background: #FFF;">
                    <div class="container">
                        <div class="slider-item-inner">
                            <div class="slider-item-caption-left slider-item-caption-white">
                                <?php if (isset($json['texto1']) && !empty($json['texto1'])) { ?>
                                    <h4 class="slider-item-caption-title" <?php echo isset($json['style1']) && !empty($json['style1']) ? 'style="' . $json['style1'] . '"' : '' ?>>
                                        <?php echo $json['texto1']; ?>
                                    </h4>
                                <?php } ?>
                                <?php if (isset($json['texto2']) && !empty($json['texto2'])) { ?>
                                    <p class="slider-item-caption-desc" <?php echo isset($json['style2']) && !empty($json['style2']) ? 'style="' . $json['style2'] . '"' : '' ?>>
                                        <?php echo $json['texto2']; ?>
                                    </p>
                                <?php } ?>
                                <?php if (isset($banner['Banner']['link']) && !empty($banner['Banner']['link'])) { ?>
                                    <a class="btn btn-lg btn-primary"
                                       href="<?php echo $banner['Banner']['link']; ?>">
                                        <?php echo (isset($json['button1']) && !empty($json['button1'])) ? $json['button1'] : 'Visualizar' ?>
                                    </a>
                                <?php } ?>
                            </div>
                            <?php
                                if (isset($json) && !empty($json)) {
                                    echo $this->Html->image('http://boutiqueno.com.br/uploads/banner/filename/'.$banner['Banner']['filename'],
                                        array(
                                            'style' => 'top: 60%; width: 56%; -webkit-filter: none;',
                                            'class' => 'slider-item-img-right'
                                        )
                                    );
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>
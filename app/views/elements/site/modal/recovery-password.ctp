<div class="mfp-with-anim mfp-hide mfp-dialog clearfix" id="nav-pwd-dialog">
    <h3 class="widget-title">Recuperar Senha</h3>
    <p>Preencha os campos abaixo, você irá receber um e-mail com as instruções.</p>
    <hr />
    <form>
        <div class="form-group">
            <label>Seu e-mail</label>
            <input class="form-control" type="text" />
        </div>
        <button type="submit" class="btn btn-success pull-right">
            Recuperar Senha
        </button>
    </form>
</div>
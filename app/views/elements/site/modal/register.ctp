<div class="mfp-with-anim mfp-hide mfp-dialog clearfix"
     id="nav-account-dialog">
    <h3 class="widget-title">Criar uma conta BoutiqueNô</h3>
    <p>Preencha todos os campos abaixo e fique por dentro de todas as ofertas.</p>
    <hr/>
    <?php echo $this->Form->create('Usuario', array('url' => '/usuarios/add')); ?>
    <div class="form-group">
            <label for="email">
                Email
            </label>
        <?php echo $this->Form->text('email', array('class' => 'form-control', 'placeholder' => 'Digite seu e-mail aqui')); ?>
        </div>
        <div class="form-group">
            <label for="senha">
                Senha
            </label>
            <?php echo $this->Form->text('senha_nova', array('class' => 'form-control', 'type' => 'password', 'placeholder' => 'Digite sua senha aqui')); ?>
        </div>
        <div class="form-group">
            <label for="confirm">Repetir Senha</label>
            <?php echo $this->Form->text('confirm', array('class' => 'form-control', 'type' => 'password', 'placeholder' => 'Repetir a senha')); ?>
        </div>
        <div class="form-group">
            <label for="telefone">
                Telefone
            </label>
            <?php echo $this->Form->text('telefone', array('class' => 'form-control telefone')); ?>
        </div>
        <div class="checkbox">
            <label>
                <?php echo $this->Form->checkbox('receber_newsletter', array('class' => 'i-check')) ?>Receber promoções e desconto por e-mail
            </label>
        </div>
        <button type="submit"
                class="btn btn-success pull-right">
            Criar Conta
        </button>
    <?php echo $this->Form->end(); ?>
    <div class="gap gap-small"></div>
    <ul class="list-inline">
        <li>
            <a href="#nav-login-dialog"
               class="popup-text">Já tenho uma conta.</a>
        </li>
    </ul>
</div>
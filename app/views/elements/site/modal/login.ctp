<div class="mfp-with-anim mfp-hide mfp-dialog clearfix"
     id="nav-login-dialog">
    <h3 class="widget-title">Identificação</h3>
    <p>
        Bem-vindo a Boutique Nô.
    </p>
    <p class="text-info">Preencha os campos abaixo e clique em logar.</p>
    <hr/>
    <?php echo $this->Form->create('Usuario', array('url' => '/login')); ?>
    <div class="form-group">
        <label for="email">E-mail (*)</label>
        <?php
            echo $this->Form->input('email', array(
                'div'         => false,
                'required'    => 'required',
                'class'       => 'form-control ',
                'label'       => false,
                'placeholder' => 'Digite seu e-mail'
            ));
        ?>
    </div>
    <div class="form-group">
        <label for="senha">Senha (*)</label>
        <?php
            echo $this->Form->input('senha', array(
                'type'        => 'password',
                'div'         => false,
                'required'    => 'required',
                'class'       => 'form-control',
                'label'       => false,
                'placeholder' => 'Digite sua senha'
            ));
        ?>
    </div>
    <button type="submit"
            class="btn btn-success pull-right">
        Logar
    </button>
    <?php echo $this->Form->end(); ?>
    <div class="gap gap-small"></div>
    <ul class="list-inline">
        <li>
            <a href="#nav-account-dialog"
               class="popup-text">
                Esqueceu a senha?
            </a>
        </li>
    </ul>
</div>
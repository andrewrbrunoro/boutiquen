<div class="container">
    <a class="navbar-brand align-logo"
       href="<?php echo $this->Html->Url('/'); ?>">
        <?php
            echo $this->Html->image('/img/boutiqueno.png',
                array(
                    'alt'   => 'Boutique Nô',
                    'title' => 'Boutique Nô',
                    'class' => 'img-responsive center-block'
                )
            );
        ?>
    </a>
</div>

<nav class="navbar navbar-inverse navbar-main yamm">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle collapsed"
                    type="button"
                    data-toggle="collapse"
                    data-target="#main-nav-collapse"
                    area_expanded="false"><span class="sr-only">Main Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse"
             id="main-nav-collapse">
            <?php if (isset($categorias) && count($categorias)) { ?>
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#">
                            <i class="fa fa-reorder"></i>&nbsp; Categorias<i class="drop-caret"
                                                                             data-toggle="dropdown"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-category">
                            <?php foreach ($categorias as $categoria) { ?>
                                <li>
                                    <a href="<?php echo $this->Html->Url('/' . $categoria['Categoria']['seo_url']) ?>">
                                        <i class="fa fa-cubes dropdown-menu-category-icon"></i> <?php echo $categoria['Categoria']['nome'] ?>
                                    </a>
                                    <?php if (isset($categoria['SubCategory']) && count($categoria['SubCategory'])) { ?>
                                        <div class="dropdown-menu-category-section">
                                            <div class="dropdown-menu-category-section-inner">
                                                <div class="dropdown-menu-category-section-content">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <h5 class="dropdown-menu-category-title">
                                                                <?php echo $categoria['Categoria']['nome'] ?>
                                                            </h5>
                                                            <ul class="dropdown-menu-category-list">
                                                                <?php foreach ($categoria['SubCategory'] as $subcategory) { ?>
                                                                    <li>
                                                                        <a href="<?php echo $this->Html->Url('/categoria/' . $subcategory['seo_url']) ?>"><?php echo $subcategory['nome'] ?></a>
                                                                        <p><?php echo $subcategory['seo_title'] ?></p>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            <?php } ?>
            <form class="navbar-form navbar-left navbar-main-search"
                  role="search">
                <div class="form-group">
                    <input class="form-control"
                           type="text"
                           placeholder="Digite aqui o que você procura..."/>
                </div>
                <a class="fa fa-search navbar-main-search-submit"
                   href="#"></a>
            </form>
            <ul class="nav navbar-nav navbar-right">
                <?php if (!$auth['Usuario']) { ?>
                    <li>
                        <a href="#nav-login-dialog"
                           data-effect="mfp-move-from-top"
                           class="popup-text">
                            Entrar
                        </a>
                    </li>
                    <li>
                        <a href="#nav-account-dialog"
                           data-effect="mfp-move-from-top"
                           class="popup-text">
                            Criar Conta
                        </a>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo $this->Html->Url('/logout'); ?>"
                           title="Sair">
                            Sair
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $this->Html->Url("/usuarios/edit"); ?>"
                           title="Sair">
                            Minha Conta
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo $this->Html->Url("/meus-pedidos"); ?>"
                           title="Meus Pedidos">
                            Meus Pedidos
                        </a>
                    </li>
                <?php } ?>
                <li class="dropdown">
                    <a class="fa fa-shopping-cart"
                       href="<?php echo $this->Html->Url('/carrinho') ?>"></a>
                    <ul class="dropdown-menu dropdown-menu-shipping-cart">
                        <?php if (isset($items) && count($items)) { ?>
                            <?php foreach ($items as $key => $item) { ?>
                                <li>
                                    <a class="dropdown-menu-shipping-cart-img"
                                       href="<?php echo $item['Produto']['url']; ?>">
                                        <div class="img-circle btn btn-xs btn-info"
                                             style="position: absolute;"
                                             title="Quantidade deste produto">
                                            <?php echo $item['Produto']['quantidade']; ?>
                                        </div>
                                        <img src="http://boutiqueno.com.br/<?php echo $item['Produto']['imagem']; ?>"
                                             alt="<?php echo $item['Produto']['nome']; ?>"
                                             title="<?php echo $item['Produto']['nome']; ?>"/>
                                    </a>
                                    <div class="dropdown-menu-shipping-cart-inner">
                                        <p class="dropdown-menu-shipping-cart-price">
                                            R$ <?php echo $this->String->bcoToMoeda(($item['Produto']['preco_promocao'] > 0) ? $item['Produto']['preco_promocao'] : $item['Produto']['preco']); ?>
                                        </p>
                                        <p class="dropdown-menu-shipping-cart-item">
                                            <a href="<?php echo $item['Produto']['url']; ?>"><?php echo $item['Produto']['nome']; ?></a>
                                        </p>
                                    </div>
                                </li>
                            <?php } ?>
                        <?php } else { ?>
                            <li>
                                <div class="dropdown-menu-shipping-cart-inner">
                                    <p class="dropdown-menu-shipping-cart-price">
                                        Nenhum produto no carrinho.
                                    </p>
                                </div>
                            </li>
                        <?php } ?>
                        <li>
                            <p class="dropdown-menu-shipping-cart-total">Total: R$ <?php echo $this->String->bcoToMoeda(isset($total_cart_value) ? $total_cart_value : 0.00); ?></p>
                            <?php if ($auth['Usuario']) { ?>
                                <?php echo $this->Form->create('', array('url' => '/carrinho')); ?>
                                <button type="submit"
                                        class="dropdown-menu-shipping-cart-checkout btn btn-success">
                                    FINALIZAR
                                </button>
                                <?php echo $this->Form->end(); ?>
                            <?php } else { ?>
                                <button data-effect="mfp-move-from-top"
                                        href="#nav-login-dialog"
                                        class="popup-text dropdown-menu-shipping-cart-checkout btn btn-success">
                                    FAÇA O LOGIN
                                </button>
                            <?php } ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
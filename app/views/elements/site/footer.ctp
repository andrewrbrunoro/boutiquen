<footer class="main-footer">
    <div class="container">
        <div class="row row-col-gap" data-gutter="60">
            <div class="col-md-3">
                <h4 class="widget-title-sm">SOCIAL</h4>
                <ul class="main-footer-social-list">
                    <li>
                        <a class="fa fa-facebook" href="https://www.facebook.com/boutiqueno" target="_blank"></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4 class="widget-title-sm">Tags mais procuradas</h4>
                <ul class="main-footer-tag-list">
                    <li>
                        <a href="#">Botas</a>
                    </li>
                    <li>
                        <a href="#">Sapatilhas</a>
                    </li>
                    <li>
                        <a href="#">Bota Country</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h4 class="widget-title-sm">Newsletter</h4>
                <form>
                    <div class="form-group">
                        <label for="email">
                            Fique por dentro das promoções
                            <small>Ganhe 10% de desconto</small>
                        </label>
                        <input class="newsletter-input form-control" placeholder="Digite seu e-mail" type="text" />
                    </div>
                    <input class="btn btn-success pull-right" type="submit" value="Cadastrar" />
                </form>
            </div>
        </div>
        <?php if (isset($pages) && count($pages)) { ?>
            <ul class="main-footer-links-list">
                <?php foreach($pages as $page) { ?>
                    <li>
                        <a href="<?php echo $page['Pagina']['url'] ? $this->Html->Url($page['Pagina']['url']) : 'javascript:void(0);' ?>">
                            <?php echo $page['Pagina']['nome']; ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
        <img class="main-footer-img" src="<?php echo $this->Html->Url('/img/test_footer2-i.png') ?>" alt="Image Alternative text" title="Image Title" />
    </div>
</footer>
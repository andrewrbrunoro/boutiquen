<?php if (isset($manufacturers)) { ?>
    <div class="container">
    <h3 class="widget-title-lg">Comprar por Marca</h3>
    <div class="row row-sm-gap manufacturers"
         data-gutter="15">
        <?php foreach ($manufacturers as $manufacturer) { ?>
            <div class="col-md-2">
                <a class="banner-category"
                   href="<?php echo $this->Html->Url('/marca/index/'.$manufacturer['Fabricante']['id'].'/'.$manufacturer['Fabricante']['seo_url']); ?>">
                    <?php
                        echo $this->Html->image('/uploads/fabricante_imagem/filename/' . $manufacturer['Imagem']['filename'], array(
                                'class' => 'img-responsive',
                                'alt'   => $manufacturer['Fabricante']['nome'],
                                'title' => $manufacturer['Fabricante']['nome']
                            )
                        );
                    ?>
                    <h5 class="banner-category-title"><?php echo $manufacturer['Fabricante']['nome']; ?></h5>
                </a>
            </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
<?php echo $this->Session->flash(); ?>

<div class="navbar-before mobile-hidden">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="navbar-before-sign">
                    <?php echo $this->String->saudacao(isset($auth) ? $auth['Usuario']['nome'] : 'tudo bem?'); ?>
                </p>
            </div>
            <div class="col-md-6">
                <?php if (isset($pages) && count($pages)) { ?>
                <ul class="nav navbar-nav navbar-right navbar-right-no-mar">
                    <?php foreach($pages as $page) { ?>
                        <li>
                            <a href="<?php echo $page['Pagina']['url'] ? $this->Html->Url($page['Pagina']['url'])  : 'javascript:void(0);' ?>">
                                <?php echo $page['Pagina']['nome']; ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php if (!$auth['Usuario']) {  ?>
    <?php echo $this->element('site/modal/login'); ?>
    <?php echo $this->element('site/modal/register'); ?>
    <?php echo $this->element('site/modal/recovery-password'); ?>
<?php } ?>

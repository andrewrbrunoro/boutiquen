<img src="<?php echo $this->Html->url('/img/site/email/logo_email.jpg',true) ?>" /><br/>
<font size="4">Foram comprados os seguintes itens de sua <?php echo $emailData['lista']['ListaTipo']['nome'] ?>:</font><br/>
<table>
	<tr>
		<th colspan="2">Produto</th>
		<th>Quantidade Comprada</th>
	</tr>
<?php foreach ($emailData['itens'] as $item) : ?>
	<tr>
		<td><?php echo $this->Image->resize($item['PedidoItem']['imagem'],100,100) ?></td>
		<td><?php echo $item['PedidoItem']['nome'] ?></td>
		<td><center><?php echo $item['PedidoItem']['quantidade'] ?></center></td>
	</tr>
<?php endforeach ?>
</table>
Para acessar a sua lista <a href="<?php echo $this->Html->url(array('plugin' => 'lista_de_presente','controller' => 'listas','action' => 'editar',$emailData['lista']['Lista']['id']),true) ?>">clique aqui</a>.
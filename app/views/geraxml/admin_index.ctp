<?php
    echo $javascript->link('admin/geraxml/index.js',false);
?>

<div class="index">
    <?php echo $this->Form->create('Geraxml', array('url'=>'/admin/geraxml/index/' . $this->params['pass'][0])); ?>
		<fieldset>
			<legend><?php __('Seleciona Produtos - ' . $titulo); ?></legend>
			<?php
			echo $this->Form->input('Buscar', array('class'=>'w312','after' => $this->Form->Button('OK', array('id' => 'buscar-produtos'))));
			$botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add'));
			$botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm'));
			echo $this->Form->input('Selecionar', array('type' => 'select', 'multiple' => true, 'after' => $botoes));
			echo $this->Form->input('Produtos', array('options'=>$produtossel,'type' => 'select', 'multiple' => true));
			echo $this->Form->end(__('Atualizar XML', true));
			?>
		</fieldset>
	</form>
</div>

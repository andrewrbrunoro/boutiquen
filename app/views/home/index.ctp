
<?php echo isset($banners_home_top) ? $this->element('site/banners/home-top', array('banners' => $banners_home_top)) : ''; ?>

<div class="gap"></div>

<?php
    if (isset($showcases) && count($showcases)) {
        foreach($showcases as $showcase) {
            echo $this->element('site/showcases', $showcase);
        }
    }
?>

<div class="gap"></div>

<?php echo $this->element('site/manufacturers'); ?>
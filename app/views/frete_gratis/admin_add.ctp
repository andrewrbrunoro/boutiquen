<?php
echo $this->Html->script(array(
    'common/jquery.meio_mask.js',
    'common/jquery-ui-1.8.16.custom.min',
    'admin/frete_gratis/crud.js',
));
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
?>
<div class="index">
    <?php echo $this->Form->create('FreteGratis', array('url' => '/admin/frete_gratis/add')); ?>
    <fieldset>
        <legend>Adicionar Regra</legend>
        <?php
        echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->input('codigo', array('label' => 'Tipo', 'options' => array('CATEGORIA' => 'Categoria', 'PRODUTO' => 'Produto', 'CARRINHO' => 'Carrinho', 'GRADE' => 'Grade')));
        echo $this->Form->input('nome', array('label' => 'Descrição'));
        echo $this->Form->input('label', array('label' => 'Label'));
        echo $this->Form->input('cep_inicial', array('label' => 'Cep Inicial', 'class' => 'mask-cep'));
        echo $this->Form->input('cep_final', array('label' => 'Cep Final', 'class' => 'mask-cep'));
        echo $this->Form->input('apartir_de', array('label' => 'Frete válido apartir de R$', 'class' => 'mask-moeda'));
        echo $this->Form->input('data_inicio', array('type' => 'text', 'label' => 'Data Início', 'class' => 'w312 datePicker'));
        echo $this->Form->input('data_fim', array('type' => 'text', 'label' => 'Data Fim', 'class' => 'w312 datePicker'));
        ?>
        <br/>
        <br/>
        <span class="box-produtos">
            <?php
            echo $this->Form->input('Buscar', array('class' => 'w312', 'after' => $this->Form->Button('OK', array('id' => 'buscar-produtos'))));
            echo $this->Form->input('produto_categoria_id', array('options' => $produtos_categorias, 'label' => 'Categorias'));
            $botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add', 'class' => 'add-bt'));
            $botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm', 'class' => 'rm-bt'));
            echo $this->Form->input('Selecionar', array('type' => 'select', 'multiple' => true, 'after' => $botoes, 'label' => 'Selecionar Produtos'));
            echo $this->Form->input('produto_id', array('options' => $produtos_produtos, 'type' => 'select', 'multiple' => true, 'label' => 'Produtos selecionados'));
            ?>
        </span>
        <span class="box-categorias">
            <?php
            echo $this->Form->input('Buscar2', array('label' => 'Buscar', 'class' => 'w312', 'after' => $this->Form->Button('OK', array('id' => 'buscar-categorias'))));
            $botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add2', 'class' => 'add-bt'));
            $botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm2', 'class' => 'rm-bt'));
            echo $this->Form->input('Selecionar2', array('type' => 'select', 'multiple' => true, 'after' => $botoes, 'label' => 'Selecionar Categorias'));
            echo $this->Form->input('categoria_id', array('options' => $categorias_categorias, 'type' => 'select', 'multiple' => true, 'label' => 'Categorias selecionadas'));
            ?>
        </span>
        <span class="box-grades">
            <?php
            echo $this->Form->input('Buscar3', array('label' => 'Buscar', 'class' => 'w312', 'after' => $this->Form->Button('OK', array('id' => 'buscar-grades'))));
            echo $this->Form->input('produto_categoria_id2', array('options' => $produtos_categorias, 'label' => 'Categorias'));
            $botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add3', 'class' => 'add-bt'));
            $botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm3', 'class' => 'rm-bt'));
            echo $this->Form->input('Selecionar3', array('type' => 'select', 'multiple' => true, 'after' => $botoes, 'label' => 'Selecionar Produtos'));
            echo $this->Form->input('grade_id', array('options' => $grades_ids, 'type' => 'select', 'multiple' => true, 'label' => 'Produtos selecionados'));
            ?>
        </span>
        <?php echo $this->Form->end(__('Inserir', true)); ?>
    </fieldset>
</div>
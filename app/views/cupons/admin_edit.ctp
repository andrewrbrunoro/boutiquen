<?php
   echo $this->Javascript->link('common/jquery-ui-1.8.16.custom.min',false);
   echo $this->Javascript->link('common/jquery.meio_mask.js',false);
	echo $this->Javascript->link('admin/cupons/crud.js',false);
   echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
?>

<div class="index">
<?php echo $this->Form->create('Cupom'); ?>
	<fieldset>
		<legend><?php __('Editar Cupom '); ?></legend>
	<?php echo $this->Form->input('id'); ?>
	<div class="clear left">
		<?php echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo'))); ?>
	</div>
	<div class="clear left">
		<?php echo $this->Form->input('usuario_unico',array('type'=>'radio','legend'=>'Usuário Único','options'=>array(true=>'Sim',false=>'Não'))); ?>
	</div>
	<div class="clear left">
		<?php echo $this->Form->input('cupom_tipo_id',array('label'=>'Tipo de Cupom','class'=>'w312')); ?>
	</div>
	<div class="clear left">
		<?php echo $this->Form->input('nome',array('class'=>'w312'));  ?>
	</div>
	<div class="left">	
		<?php echo $this->Form->input('cupom',array('type'=>'text','label'=>'Cupom','class'=>'w312')); ?>
	</div>
	<div class="clear left">
		<?php echo $this->Form->input('vlr_desconto',array('label'=>'Valor de Desconto R$','class'=>'mask-moeda w312')); ?>
	</div>
	<div class="left">
		<?php echo $this->Form->input('pct_desconto',array('label'=>'Perc. de Desconto %','class'=>'mask-moeda w312')); ?>
	</div>
	<div class="clear left">
		<?php echo $this->Form->input('data_fim',array('type'=>'text','label'=>'Expira em:','class'=>'datePicker w312')); ?>
	</div>
	<div class="left">
		<?php echo $this->Form->input('max_usos',array('label'=>'Número máximo de usos:','class'=>'mask-numerico w312')); ?>
	</div>
	<div class="clear left">
		<?php echo $this->Form->input('compra_minima',array('label'=>'Válido apartir de R$:','class'=>'mask-moeda  w312')); ?>
	</div>

	<div class="clear left">
		<?php echo $this->Form->input('categoria_id',array('label'=>'Categoria','class'=>'w312')); ?>
	</div>
		<div class="input select required">
	<?php
		echo $this->Form->input('Buscar',array('class'=>'w312','id'=>'query','after'=>$this->Html->link('Adicionar','javascript:;',array('id'=>'buscar-produtos'))));
		echo $this->Form->input('grade_id',array('label'=>'Produto','class'=>'w312'));	
		echo $this->Form->end(__('Salvar', true));	
	?>
	</div>
	</fieldset>
</div>

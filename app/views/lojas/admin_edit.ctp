<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/lojas/index.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Loja',array('type' => 'file')); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Loja', true)); ?></legend>
        <div class="left clear">
        	<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
        </div>
        <div class="left clear">
			<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('cep', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('rua', array('id' => 'LojaRua', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('numero', array('id' => 'LojaNumero', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('bairro', array('id' => 'LojaBairro', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('cidade', array('id' => 'LojaCidade', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('uf', array('id' => 'LojaUf', 'class' => 'w312', 'options' => $this->Estados->estadosBrasileiros())); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('telefone', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('email_contato', array('class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('mapa', array('label' => 'Google Maps (Formato: 350px x 300px) ou URL', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('coordenadas', array('label' => 'Coordenadas (separadas por Ponto e vírgula ( ; )', 'class'=>'w312')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('ordem', array('label' => 'Ordem', 'class'=>'w312')); ?>
		</div>>
		<div class="left clear">
			<?php echo $this->Form->input('observacoes', array('class' => 'mceEditor wCEM h400', 'type' => 'textarea')); ?>
		</div>
		<div class="left clear">
			<legend>Thumb</legend>
	        <?php 
	        	$img = ( isset($this->data['Loja']['thumb_filename']) ) ? $this->data['Loja']['thumb_dir'] . '/' . $this->data['Loja']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
				echo $this->Form->input('Loja.thumb_filename', array('type' => 'file'));
				echo $image->resize($img, 80, 80);		
				echo $this->Form->input('Loja.thumb_dir', array('type' => 'hidden'));
				echo $this->Form->input('Loja.thumb_mimetype', array('type' => 'hidden'));
				echo $this->Form->input('Loja.thumb_filesize', array('type' => 'hidden'));
	       		echo $this->Form->end(__('Salvar', true));
	        ?>
	    </div>
    </fieldset>
</div>
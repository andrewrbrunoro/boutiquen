<?php
echo $javascript->link('common/swfobject.js', false);
echo $javascript->link('common/jquery.uploadify.v2.1.4.min', false);
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce.js', false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min', false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/produtos/crud.js', false);
echo $javascript->link('admin/carrinho_abandonado/crud.js',false);
echo $javascript->link('common/asmselect/jquery.asmselect', false);
echo $this->Html->css('common/asmselect/jquery.asmselect.css');
?>

<div class="index ">
    <h2><?php __('Carrinho Abandonados'); ?></h2>
    <?php echo $form->create('carrinho_abandonado', array('url' => '/admin/carrinho_abandonado/index', 'class' => 'formBusca')); ?>
    <fieldset>
        <div class="w312 left">
            <?php
            echo $this->Form->input('Filter.cupom_enviado', array('type' => 'select', 'options' => array('' => 'Selecione', 'Enviado' => 'Enviado', 'Não' => 'Não')));
            echo $form->input('Filter.nome', array('type' => 'text', 'label' => 'Usuário:', 'class' => 'w312'));
            echo $form->input('Filter.email', array('type' => 'text', 'label' => 'E-mail:', 'class' => 'w312'));
            echo $form->input('Filter.sku', array('type' => 'text', 'label' => 'SKU:', 'class' => 'w312'));
            echo $this->Form->input('Filter.finalizado', array('type' => 'select', 'options' => array('' => 'Selecione', 'Não' => 'Não', 'Sim' => 'Sim')));
            echo $form->input('Filter.data_ini', array('class' => 'datePicker w147', 'label' => 'Data de:'));
            echo $form->input('Filter.data_fim', array('class' => 'datePicker w147', 'label' => 'Data até:'));
            echo $this->Form->input('Filter.preco', array('div' => false, 'label' => 'Preço', 'class' => 'mask-moeda inputs w147'));
            echo $this->Form->input('Filter.faixa_preco_de', array('div' => false, 'label' => 'Preço De', 'class' => 'mask-moeda inputs w147'));
            echo $this->Form->input('Filter.faixa_preco_ate', array('div' => false, 'label' => 'Preço Até', 'class' => 'mask-moeda inputs w147'));
            echo $form->input('Filter.cupom', array('type' => 'text', 'label' => 'Cupom:', 'class' => 'w312'));
            ?>
            <div style="clear: both;"></div>
            <div class="submit">
                <input name="submit" type="submit" class="button1" value="Busca" />
            </div>
            <div class="submit">
                <input name="submit" type="submit" class="button1" value="Exportar" />
            </div>
            <div class="submit">
                <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
            </div>
            <div style="clear: both;"></div>

            <div class="combo_legend" style="display: none;">
                <fieldset>
                    <legend><?php __('Enviar cupom aos itens marcados'); ?></legend>
                    <?php echo $this->Form->input('cupom',array('options'=>$cupons,'class'=>'w147','label'=>'Cupons')); ?>
                    <div class="submit">
                        <input name="submit" type="submit" class="button1" value="Enviar Cupom" />
                    </div>
                </fieldset>
            </div>
        </div>
        <div style="width: 650px; float: right;">
            <div id="chart_div" style="width:350; height:250"></div>
            <div class="totais" style="width:350px; height:100px;text-align:right;"></div>
        </div>
    </fieldset>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><input type="checkbox" value="" name="data[Filter][ids_carrinho][]" onclick="toggleChecked(this.checked)" id="check_all" /></th>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('usuario_id'); ?></th>
            <th><?php echo $this->Paginator->sort('E-mail', 'Usuario.email'); ?></th>
            <th><?php echo 'Carrinho'; ?></th>
            <th><?php echo 'Total'; ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th><?php echo $this->Paginator->sort('Cupom Enviado', 'cupom_enviado'); ?></th>
            <th><?php echo $this->Paginator->sort('Cupom', 'cupom'); ?></th>
            <th><?php echo $this->Paginator->sort('Finalizado', 'finalizado'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($carrinhos_abandonados as $pedido_abandonado):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <?php if ($pedido_abandonado['CarrinhoAbandonado']['produtos'] && $pedido_abandonado['CarrinhoAbandonado']['valor_total'] > 0 && !$pedido_abandonado['CarrinhoAbandonado']['cupom_enviado'] && null != $pedido_abandonado['Usuario']['nome']) {?>
                <td>
                    <input type="checkbox" value="<?php echo $pedido_abandonado['CarrinhoAbandonado']['id'] ?>" name="data[Filter][ids_carrinho][]" class="ids_carrinhos_abandonados" <?php if($pedido_abandonado['CarrinhoAbandonado']['checked'] == 'checked') { echo 'checked="checked"'; } ?> onclick="toggleChecked2(this.checked)" />
                </td>
                <?php } else { ?>
                <td>
                    <input disabled="disabled" readonly="readonly" type="checkbox" value="<?php echo $pedido_abandonado['CarrinhoAbandonado']['id'] ?>" name="data[Filter][ids_carrinho][]" class="ids_carrinhos_abandonados" <?php if($pedido_abandonado['CarrinhoAbandonado']['checked'] == 'checked') { echo 'checked="checked"'; } ?> />
                </td>
                <?php } ?>
                <td align="center"><?php echo $pedido_abandonado['CarrinhoAbandonado']['id']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Html->link($pedido_abandonado['Usuario']['nome'], array('controller' => 'usuarios', 'action' => 'edit', $pedido_abandonado['Usuario']['id'])); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Html->link($pedido_abandonado['Usuario']['email'], array('controller' => 'usuarios', 'action' => 'edit', $pedido_abandonado['Usuario']['email'])); ?>&nbsp;</td>
                <td align="left"><?php
                    if ($pedido_abandonado['CarrinhoAbandonado']['produtos']) {
                        //$total = 0;
                        $carrinho = '';
                        foreach ($pedido_abandonado['CarrinhoAbandonado']['produtos'] as $chave => $produto) {
                            $preco = $produto['Grade']['preco_promocao'] > 0 ? $produto['Grade']['preco_promocao'] : $produto['Grade']['preco'];
                            //$total += $preco * $chave;
                            $variacoes_no_nome = '';
                            if($produto['Produto']['grades'] != null){
                                $variacoes = json_decode($produto['Produto']['grades'], true);
                                if(is_array($variacoes) && count($variacoes) > 0){
                                    foreach($variacoes as $k => $var){
                                        if($var['Grade']['id'] == $produto['Grade']['id']){
                                            if(isset($variacoes[$k]['Variacoes']) && is_array($variacoes[$k]['Variacoes'])){
                                                foreach($variacoes[$k]['Variacoes'] as $vr){
                                                    $variacoes_no_nome .= ' - '.$vr['VariacaoTipo']['nome'] .': '. $vr['Variacao']['valor'];
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            $carrinho .= $produto['Grade']['sku'] . ' - ' . $produto['Produto']['nome'] . $variacoes_no_nome . '<br/>';
                        }
                        echo $carrinho;
                    } else {
                        echo 'Carrinho Vazio';
                        //$total = 0;
                    }
                    ?>&nbsp;</td>
                <td style="width: 100px; text-align: center;">R$ <?php echo $pedido_abandonado['CarrinhoAbandonado']['valor_total']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $pedido_abandonado['CarrinhoAbandonado']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $pedido_abandonado['CarrinhoAbandonado']['modified']); ?>&nbsp;</td>
                <td align="center"><?php
                    if (isset($pedido_abandonado['CarrinhoAbandonado']['cupom_enviado']) && null != $pedido_abandonado['CarrinhoAbandonado']['cupom_enviado']) {
                        if ($pedido_abandonado['CarrinhoAbandonado']['cupom_enviado'] == true) {
                            echo 'Sim';
                        } else {
                            echo 'Não';
                        }
                    } else {
                        echo 'Não';
                    }
                ?>&nbsp;
                </td>
                <td align="center">
                    <?php
                        if ($pedido_abandonado['CarrinhoAbandonado']['cupom'] != "") {
                            echo $pedido_abandonado['CarrinhoAbandonado']['cupom'];
                        } else {
                            echo "---";
                        }
                    ?>&nbsp;
                </td>
                <td align="center"><?php
                    if (isset($pedido_abandonado['CarrinhoAbandonado']['finalizado']) && null != $pedido_abandonado['CarrinhoAbandonado']['finalizado']) {
                        if ($pedido_abandonado['CarrinhoAbandonado']['finalizado'] == true) {
                            echo 'Sim';
                        } else {
                            echo 'Não';
                        }
                    } else {
                        echo 'Não';
                    }
                ?>&nbsp;
               </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php echo $form->end(); ?>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>  </p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
        |   <?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>

</div>

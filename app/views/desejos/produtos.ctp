
<div class="pro_category_hp desejos-lista ">
<h3>Lista de Desejos: <?php echo $desejo["Desejo"]["nome"]; ?></h3>
<div class="clear"></div>
<!-- inner box -->
<?php
        foreach ($produtos as $produto):
            $img = ( isset($produto['Produto']['ProdutoImagem'][0]['filename']) && file_exists($produto['Produto']['ProdutoImagem'][0]['dir'] . '/' . $produto['Produto']['ProdutoImagem'][0]['filename']) ) ? $produto['Produto']['ProdutoImagem'][0]['dir'] . '/' . $produto['Produto']['ProdutoImagem'][0]['filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg";
			?>
	<div class="pro_category_hp change2">
	<label><a href="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['id'])) ?>.html">
						<?php echo $image->resize($img, 129, 121); ?>
					</a></label>
	<p>
		<span><a title="<?php e($this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['id'])) ?>.html"><?php echo $produto['Produto']['nome'] ?></a></span>
		<strong>
		<?php
					$preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
					$parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
					if ($produto['Produto']['preco_promocao'] > 0) {
						?>
							<em>de: R$<?php echo $this->String->bcoToMoeda($produto['Produto']['preco']) ?> </em>
						<?php
					}
					?>
					<em>por: R$<?php echo $this->String->bcoToMoeda($preco) ?></em>
		</strong>
		<?php if( $parcela_valida['parcela'] > 1 ):?>
			<em>ou em <?php e($parcela_valida['parcela']) ?>x de R$<?php echo $this->String->bcoToMoeda($parcela_valida['valor'])?></em>
			<em>sem juros</em>
		<?php endIf;?>
		<a href="<?php e($this->Html->Url("/carrinho/add/" . $produto['Produto']['id']))?>" title="Colocar no carrinho o produto <?php echo $produto['Produto']['nome']?>" class="link">comprar</a>
	</p>
	</div>
		<?php endforeach; ?>
</div>
 <div class="clear"></div>
   <div id="pagination">
        <ul>
            <?php 
            if( $this->Paginator->current() > 1 ){
            ?>
                <li><?php echo $this->Paginator->prev('<img src="' . $this->Html->Url("/img/site/") . 'pagination_left.gif" alt="left" />', array('escape' => false, 'title' => 'Anterior'), null, array('class' => 'disabled')); ?></li>
            <?php
            }
            
            echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => '<li>-</li>'));
 
            if( $this->Paginator->current() < $this->Paginator->params['paging']['DesejoProduto']['pageCount'] ){
            ?>
                <li><?php echo $this->Paginator->next('<img src="' . $this->Html->Url("/img/site/") . 'pagination_right.gif" alt="right" />', array('escape' => false, 'title' => 'Próximo'), null, array('class' => 'disabled')); ?></li>
            <?php
            }
            ?>
            <li><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages% página(s), exibindo %current% no total de %count% produtos.', true))); ?></li>
        </ul>
    </div>

<div id="rightcol">
    <h4>Editar Lista de Desejos</h4>
    <?php echo $this->Form->create('Desejo', array('class' => 'clear', 'action' => 'edit/' . $this->params['pass'][0])); ?>
		
		<fieldset class="container-listas">
			<div class="add">
				<?php echo $this->Form->input("Desejo.nome", array('div'=>false,'class' => 'input input3', 'label' => 'Nome Lista:')); ?>
				<div class="clear"></div>
				<?php echo $this->Form->button('Enviar', array('class' => 'link6')); ?>
			</div>
		</fieldset>
		
    <?php echo $this->Form->end(); ?>
</div>
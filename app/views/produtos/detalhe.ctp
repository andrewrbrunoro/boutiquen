
<div class="container">
	<header class="page-header">
		<h1 class="page-title">
			<?php echo $product['Produto']['nome']; ?>
		</h1>
		<ol class="breadcrumb page-breadcrumb">
			<li>
				<a href="<?php echo $this->Html->Url('/'); ?>">
					Página inicial
				</a>
			</li>
            <!--			<li>-->
            <!--				<a href="-->
            <?php //echo $this->Html->Url('/categorias/'.$category['Categoria']['seo_url']); ?><!--">-->
            <!--					--><?php //echo $category['Categoria']['nome']; ?>
            <!--				</a>-->
            <!--			</li>-->
			<li class="active">
				<?php echo $product['Produto']['nome']; ?>
			</li>
		</ol>
	</header>
	<div class="row">
		<div class="col-md-5">
			<div class="product-page-product-wrap jqzoom-stage">
				<div class="clearfix">
<!--					--><?php //if (file_exists('uploads/produto_imagem/filename/' . $images[0]['GradeImagem']['filename'])) { ?>
<!--						<a href="--><?php //echo $this->Html->Url('/uploads/produto_imagem/filename/' . $images[0]['GradeImagem']['filename']); ?><!--"-->
<!--						   id="jqzoom"-->
<!--						   data-rel="gal-1">-->
<!--							<img src="--><?php //echo $this->Html->Url('/uploads/produto_imagem/filename/' . $images[0]['GradeImagem']['filename']); ?><!--"-->
<!--								 alt="--><?php //echo $product['Produto']['nome']; ?><!--"-->
<!--								 width="500"-->
<!--								 title="--><?php //echo $product['Produto']['nome']; ?><!--"/>-->
<!--						</a>-->
<!--					--><?php //} ?>

						<a href="<?php echo $this->Html->Url('http://boutiqueno.com.br/uploads/produto_imagem/filename/' . $images[0]['GradeImagem']['filename']); ?>"
						   id="jqzoom"
						   data-rel="gal-1">
							<img src="<?php echo $this->Html->Url('http://boutiqueno.com.br/uploads/produto_imagem/filename/' . $images[0]['GradeImagem']['filename']); ?>"
								 alt="<?php echo $product['Produto']['nome']; ?>"
								 width="500"
								 title="<?php echo $product['Produto']['nome']; ?>"/>
						</a>
				</div>
			</div>
            <?php if (isset($images) && count($images)) { ?>
                <ul class="jqzoom-list">
					<?php foreach ($images as $key => $image) { ?>
                        <li>
							<a class="<?php echo ($key == 0) ? 'zoomThumbActive' : '' ?>"
                               href="javascript:void(0);"
                               data-rel="{gallery:'gal-1', smallimage: '<?php echo $this->Html->Url('http://boutiqueno.com.br/uploads/produto_imagem/filename/' . $image['GradeImagem']['filename']) ?>', largeimage: '<?php echo $this->Html->Url('http://boutiqueno.com.br/uploads/produto_imagem/filename/' . $image['GradeImagem']['filename']) ?>'}">
								<img src="<?php echo $this->Html->Url('http://boutiqueno.com.br/uploads/produto_imagem/filename/' . $image['GradeImagem']['filename']) ?>"
                                     alt="Image Alternative text"
                                     title="Image Title"/>
							</a>
						</li>
                    <?php } ?>
				</ul>
            <?php } ?>
		</div>
		<div class="col-md-7">
			<div class="row"
                 data-gutter="10">
				<div class="col-md-8">
					<div class="box">
						<p class="product-page-desc">
							<?php echo ($product['Produto']['descricao_resumida']) ? $product['Produto']['descricao_resumida'] : $product['Produto']['descricao']; ?>
						</p>
						<strong>Tags</strong> <br/>
						<ul class="main-footer-tag-list">
							<?php
                                if ($product['Produto']['tag'] != '' && strstr($product['Produto']['tag'], ',')) {
                                    foreach (explode(',', $product['Produto']['tag']) as $tag) {
                                        if (!empty($tag)) {
                                            echo '<li><a href="#">' . $tag . '</a></li>';
                                        }
                                    }
                                }
                            ?>
                            <?php if ($product[0]['free_shipping']) { ?>
                                <li>
									<a href="#">
										Frete grátis
									</a>
								</li>
                            <?php } ?>
						</ul>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box-highlight">
						<?php if ($product['Grade']['preco_promocao'] != '' && $product['Grade']['preco_promocao'] > 0) { ?>
                            <p class="product-page-price-list">
								R$ <?php echo $this->String->bcoToMoeda($product['Grade']['preco']); ?>
							</p>
                            <p class="product-page-price">
								R$ <?php echo $this->String->bcoToMoeda($product['Grade']['preco_promocao']); ?>
							</p>
                        <?php } else { ?>
                            <p class="product-page-price">
								R$ <?php echo $this->String->bcoToMoeda($product['Grade']['preco']); ?>
							</p>
                        <?php } ?>
                        <?php if ($product[0]['free_shipping']) { ?>
                            <p class="text-muted text-sm text-uppercase">
								Frete Grátis
							</p>
                        <?php } ?>
						<div class="product-page-side-section">
							<?php if (isset($colors) && count($colors)) { ?>
								<div class="form-group">
									<label for="variation">
										<?php count($colors) > 1 ? 'Cores' : 'Cor' ?>
									</label>
									<select class="form-control variations">
										<?php foreach ($colors as $color) { ?>
											<?php
											$url = $this->Html->Url('/' . low(Inflector::slug($product['Produto']['nome'], '-')) . '-prod-' . $color['VariacaoProduto']['grade_id'] . '.html');
											?>
											<?php $selected = in_array($color['Variacao']['id'], $current_variation) ? 'selected' : ''; ?>
											<option <?php echo $selected; ?> value="<?php echo $url; ?>">
												<?php echo $color['Variacao']['valor']; ?>
											</option>
										<?php } ?>
									</select>
								</div>
							<?php } ?>

							<?php if (isset($sizes) && count($sizes)) { ?>
							<div class="form-group">
								<label for="variation">
									<?php count($sizes) > 1 ? 'Tamanhos' : 'Tamanho' ?>
								</label>
								<select class="form-control variations">
									<?php foreach ($sizes as $size) { ?>
										<?php
										$url = $this->Html->Url('/' . low(Inflector::slug($product['Produto']['nome'], '-')) . '-prod-' . $size['VariacaoProduto']['grade_id'] . '.html');
										?>
										<?php $selected = in_array($size['VariacaoProduto']['variacao_id'], $current_variation) ? 'selected' : ''; ?>
										<option <?php echo $selected; ?> value="<?php echo $url; ?>">
											<?php echo $size['Variacao']['valor']; ?>
										</option>
									<?php } ?>
								</select>
							</div>
							<?php } ?>
						</div>

						<?php echo $this->Form->create('Produto', array('url' => '/carrinho/carrinho/add', 'class' => 'form-group')); ?>
							<?php echo $this->Form->hidden("Compra.produto_id", array("value" => $product['Produto']['id'])); ?>
							<?php echo $this->Form->hidden("Compra.grade_id", array("value" => $grade_id)); ?>
							<button class="btn btn-block btn-success" type="submit">
								<i class="fa fa-shopping-cart"></i> Comprar
							</button>
						<?php echo $this->Form->end(); ?>

						<a class="btn btn-block btn-default"
                           href="#">
							<i class="fa fa-star"></i> Favorito
						</a>
						<div class="product-page-side-section">
							<h5 class="product-page-side-title">Compartilhe</h5>
							<ul class="product-page-share-item">
								<li>
									<a class="fa fa-facebook"
                                       href="#"></a>
								</li>
							</ul>
						</div>
						<div class="product-page-side-section">
							<h5 class="product-page-side-title">Entrega e Devolução</h5>
							<p class="product-page-side-text">Média do tempo de entrega é de <br/> 5 à 7 dias úteis.</p>
							<p class="product-page-side-text">A primeira troca é totalmente gratuita.</p>
							<p class="product-page-side-text">Produtos com valor superior a <br/> R$200,00 o frete é grátis para todos o Brasil.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="gap"></div>
	<div class="tabbable product-tabs">
		<ul class="nav nav-tabs"
            id="myTab">
			<li class="active">
				<a href="#tab-1"
                   data-toggle="tab">
					<i class="fa fa-list nav-tab-icon"></i>Descrição
				</a>
			</li>
			<li>
				<a href="#tab-2"
                   data-toggle="tab">
					<i class="fa fa-wechat nav-tab-icon"></i>Comentários
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade in active"
                 id="tab-1">

			</div>
			<div class="tab-pane fade"
                 id="tab-2">

			</div>
		</div>
	</div>
	<div class="gap"></div>
<!--	<h3 class="widget-title">Produto que talvez você goste</h3>-->
<!---->
<!--	<div class="row"-->
<!--         data-gutter="15">-->
<!--		<div class="col-md-3">-->
<!--			<div class="product ">-->
<!--				<ul class="product-labels"></ul>-->
<!--				<div class="product-img-wrap">-->
<!--					<img class="product-img-primary"-->
<!--                         src="img/500x500.png"-->
<!--                         alt="Image Alternative text"-->
<!--                         title="Image Title"/>-->
<!--					<img class="product-img-alt"-->
<!--                         src="img/490x500.png"-->
<!--                         alt="Image Alternative text"-->
<!--                         title="Image Title"/>-->
<!--				</div>-->
<!--				<a class="product-link"-->
<!--                   href="#"></a>-->
<!--				<div class="product-caption">-->
<!--					<h5 class="product-caption-title">Motorola XT1096 Moto X 2nd Generation 16GB Verizon Wireless gsm unlocked</h5>-->
<!--					<div class="product-caption-price"><span class="product-caption-price-new">$88</span>-->
<!--					</div>-->
<!--					<ul class="product-caption-feature-list">-->
<!--						<li>4 left</li>-->
<!--						<li>Free Shipping</li>-->
<!--					</ul>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
</div>
<?php
echo $javascript->link('common/jquery.uploadify.v2.1.4.min' ,false);
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('common/tiny_mce/tiny_mce.js',false);
echo $javascript->link('common/jquery.cookie.js',false);
echo $javascript->link('admin/produtos/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Faq');?>
	<fieldset>
		<legend><?php __('Editar Faq'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
		echo $this->Form->input('texto');
		echo $this->Form->input('url');
		echo $this->Form->input('seo_title');
        echo $this->Form->input('seo_meta_description');
        echo $this->Form->input('seo_meta_keywords');
        echo $this->Form->input('seo_institucional');
	?>
	</fieldset>
</div>

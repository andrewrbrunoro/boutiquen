<?php echo $javascript->link('site/faq/index.js',false);?>
<div class="faq" id="form-faq">
	<div class="busca">
		 <h4><?php echo $faqs['Faq']['texto']?></h4>            
	</div>

<?php foreach ($perguntas as $valor): ?>
        <ul class="faq-list">
            <?php if(count($valor['FaqResposta'])>0):?>
            <li><strong><?php echo $html->link($valor['FaqPergunta']['texto'],'javascript:;',array('class'=>'pergunta','rel'=>$valor['FaqPergunta']['id'])); ?></strong>
                <ul style="display:none">
                    <?php foreach ($valor['FaqResposta'] as $valor): ?>
                        <li><?php echo $valor['texto'] ?></li>
                    <?php endForeach; ?>
                </ul>
            </li>
            <?php endIf;?>
        </ul>
    <?php endForeach; ?>
</div>

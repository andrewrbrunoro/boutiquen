<div class="index">
    <h2><?php __('Faqs'); ?></h2>
	  <div class="btAddProduto">
    	<?php echo $this->Html->link(__('Adicionar Faq', true), array('action' => 'add')); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('texto'); ?></th>
            <th><?php echo $this->Paginator->sort('url'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($faqs as $faq):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $faq['Faq']['id']; ?>&nbsp;</td>
                <td><?php echo $faq['Faq']['texto']; ?>&nbsp;</td>
                <td><?php echo $faq['Faq']['url']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $faq['Faq']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $faq['Faq']['modified']); ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $faq['Faq']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $faq['Faq']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $faq['Faq']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
       
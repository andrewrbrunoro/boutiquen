<div class="index">
<?php 
//debug($this->data);die;
echo $this->Form->create('Flag',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php __('Editar Flag'); ?></legend>
	<?php
		echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
		echo $this->Form->input('id');
		echo $this->Form->input('nome',array('class'=>'w312'));
	?>
	</fieldset>
        <fieldset>
            <legend>Imagem</legend>
            <?php if(isset($this->data['Flag']['thumb_filename'])){?>
                <div class="img">
                    <?php
                    echo $image->resize($this->data['Flag']['thumb_dir'].DS.$this->data['Flag']['thumb_filename'], 100, 100,false);
                    if ($this->data['Flag']['thumb_filename']=="") {
                        echo $form->input('Flag.thumb_remove', array('type' => 'checkbox','label' => 'Remover Imagem'));
                    }
                    ?>
                </div>
            <?php } ?>
            <?php
                echo $this->Form->input('Flag.thumb_filename', array('type' => 'file'));
                echo $this->Form->input('Flag.thumb_dir', array('type' => 'hidden'));
                echo $this->Form->input('Flag.thumb_mimetype', array('type' => 'hidden'));
                echo $this->Form->input('Flag.thumb_filesize', array('type' => 'hidden'));
                
                echo $this->Form->end(__('Salvar', true));
            ?>
        </fieldset>
</div>

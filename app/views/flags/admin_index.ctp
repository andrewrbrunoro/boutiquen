<div class="index">
    <h2 class="left"><?php __('Flags'); ?></h2>
    <div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Flag', true), array('controller' => 'flags','action' => 'add')); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        //debug($flags);die;
        foreach ($flags as $flag):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $flag['Flag']['id']; ?>&nbsp;</td>
                <td><?php echo $flag['Flag']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo ($flag['Flag']['status']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$flag['Flag']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$flag['Flag']['modified']); ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $flag['Flag']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $flag['Flag']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $flag['Flag']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
      
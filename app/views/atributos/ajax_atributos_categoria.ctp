<!-- start block1 -->
<div class="block1">	
	<?php if(isset($filtros_selecionados)): ?> 
		<!-- start filter -->
		<div class="filter" style="padding: 15px 0px 0px 0px; margin: 0px 0px -10px 0px">
			<div id="filtros-selecionados">
				<h4>Filtros Selecionados</h4>
				<?php foreach($filtros_selecionados as $tipo_filtro):?>
				<a title="Remove filtro: <?php echo $tipo_filtro['nome_filtro']; ?> <?php echo $tipo_filtro['valor_filtro']; ?>" class="gray filtros_selecionados" href="javascript:void(0);" rel="<?php echo $tipo_filtro['id_filtro']; ?>" ><strong>X</strong> <span><?php echo $tipo_filtro['nome_filtro']; ?> : <?php echo $tipo_filtro['valor_filtro']; ?></span></a>
				<?php endforeach;?>
				<div class="clear"></div>
			</div>
		</div>
		<!-- end filter -->
	<?php endIf;?>

	<?php if($menu_filtro): ?>

		<!-- start filter -->
		<div class="filter">
		
			<h3>Filtrar por:</h3>
			<!-- filter search -->
			<form action="#" class="filter-search" style="display: none;">
				<input type="text" value="Marca" name="field" class="input" onfocus="if(this.value == 'Marca') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Marca'; }" />
				<input type="submit" value="" name="button" class="button" />
			</form>
				
		</div>
		<!-- end filter -->
			
		<?php foreach($menu_filtro as $tipo_filtro):?>
		
			<?php 
				//inicio provisorio cores
				if(isset($tipo_filtro['codigo_filtro']) && $tipo_filtro['codigo_filtro'] == 'cor'):
			?>	
				<!-- start color bar -->
				<div class="color-bar">
					<h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
					<!-- start filtroContent -->
					<div class="filtroContent" style="background: none repeat scroll 0px 0px rgb(250, 250, 250); display: block; float: left; width: 100%; padding-top: 5px; padding-bottom: 5px;">
						<!-- start customSelectContent -->
						<div class="customSelectContent" style="float: left; position: relative; width: 140px;">						
							<select class="atributos-cores select">
								<option value="">Selecione...</option>
								<?php foreach($tipo_filtro['valor_filtro'] as $filtro):?>
									<option value="<?php e($filtro['id']); ?>"><?php echo $this->String->title_case($filtro['valor']); ?></option>
								<?php endforeach;?>
							</select>
						</div>
						<!-- end customSelectContent -->
					</div>
					<!-- end filtroContent -->
				</div>
				<!-- end color bar -->
			<?php
				endIf; 
				//fim provisorio cores
			?>	
			
			<br /><br /><br />
		
			<?php 
				if(isset($tipo_filtro['codigo_filtro']) && $tipo_filtro['codigo_filtro'] == 'tamanho'):
			?>	
				<!-- start size bar -->
				<div class="size-bar">
					<h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
					<ul class="atributos-tamanhos">
					<?php foreach($tipo_filtro['valor_filtro'] as $filtro):?>
						<li>
							<!--<a href="javascript:void(0);" rel="<?php echo $filtro['id']; ?>" title="<?php echo $this->String->title_case($filtro['valor']); ?>">
								<?php //echo $this->String->title_case($filtro['valor']); ?>
							</a>-->
							<input type="checkbox" class="checkbox" id="<?php echo $filtro['id']; ?>" rel="<?php echo low(Inflector::slug($tipo_filtro['nome_filtro'], '-')); ?>" value="<?php e($filtro['id']); ?>" name="data[Filtro][id]" />
							<label for="<?php echo $filtro['id']; ?>"><?php echo $this->String->title_case($filtro['valor']); ?></label>
						</li>
					<?php endforeach;?>
					</ul>
				</div>
				<!-- end size bar -->
			<?php endIf; ?>	
			
			<?php 
				//inicio backuo thumb de cores
				if(1 == 2):
					if(isset($tipo_filtro['codigo_filtro']) && $tipo_filtro['codigo_filtro'] == 'cor'):
			?>	
				<!-- start color bar -->
				<div class="color-bar">
					<h3><?php echo $this->String->title_case($tipo_filtro['nome_filtro']); ?></h3>
					<ul class="atributos-cores">
					<?php foreach($tipo_filtro['valor_filtro'] as $filtro):?>
						<li>
							<?php $img = ( isset($filtro['thumb_filename']) ) ? 'uploads/atributo/thumb/' . $filtro['thumb_filename'] : "uploads/atributo/thumb/sem_imagem.jpg"; ?>
							<input type="checkbox" class="checkbox" id="<?php echo $filtro['id']; ?>" rel="<?php echo low(Inflector::slug($tipo_filtro['nome_filtro'], '-')); ?>" value="<?php e($filtro['id']); ?>" name="data[Filtro][id]" />
							<label for="<?php echo $filtro['id']; ?>" style="background-image: url('<?php echo $this->Html->url("/", true).$img; ?>');" title="<?php echo $this->String->title_case($filtro['valor']); ?>"></label>
						</li>
					<?php endforeach;?>
					</ul>
				</div>
				<!-- end color bar -->
			<?php 	
					endIf;
				endIf;
				//fim backuo thumb de cores
			?>
				
			
		<?php endforeach;?>

	<?php endIf;?>	
</div>
<!-- end block1 -->
<div class="index">
    <?php
    echo $form->create('Atributo', array('type' => 'file', 'action' => 'edit/' . $this->params['pass'][0]));
    ?>
    <fieldset>
        <legend><?php __('Editar Atributo'); ?></legend>
       <div class="left clear">
            <?php echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('filtro', array('type' => 'radio', 'options' => array(true => 'Sim', false => 'Não'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('atributo_tipo_id', array('label' => 'Tipo de Atributo', 'options' => $atributo_tipos)); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('valor', array('class' => 'w312')); ?>
        </div>
        <br class="clear" />
        <legend>Thumb</legend>
        <?php
        $img = ( isset($this->data['Atributo']['thumb_filename']) ) ? $this->data['Atributo']['thumb_dir'] . '/' . $this->data['Atributo']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
        echo $this->Form->input('Atributo.thumb_filename', array('type' => 'file'));
        echo $image->resize($img, 80, 80);
        echo $this->Form->input('Atributo.thumb_dir', array('type' => 'hidden'));
        echo $this->Form->input('Atributo.thumb_mimetype', array('type' => 'hidden'));
        echo $this->Form->input('Atributo.thumb_filesize', array('type' => 'hidden'));

        if (isset($this->data['Atributo']['thumb_filename'])) {
            echo $form->input('Atributo.thumb_remove', array('type' => 'checkbox'));
        }
        ?>
        <br class="clear" />
        <?php
        echo $form->end('Salvar');
        ?>
    </fieldset>
</div>

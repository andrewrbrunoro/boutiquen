<div class="index">
    <?php
    echo $form->create('Atributo', array('type' => 'file', 'action' => 'add'));
    ?>
    <fieldset>
        <legend><?php __('Adicionar Atributos'); ?></legend>
        <div class="left clear">
            <?php echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('filtro', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Sim', false => 'Não'))); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('atributo_tipo_id', array('label' => 'Tipo de Atributo', 'options' => $atributo_tipos)); ?>
        </div>
        <div class="left clear">
            <?php echo $this->Form->input('valor', array('class' => 'w312')); ?>
        </div>
        <br class="clear" />
        <legend>Thumb</legend>
        <?php
        echo $this->Form->input('Atributo.thumb_filename', array('type' => 'file'));
        echo $this->Form->input('Atributo.thumb_dir', array('type' => 'hidden'));
        echo $this->Form->input('Atributo.thumb_mimetype', array('type' => 'hidden'));
        echo $this->Form->input('Atributo.thumb_filesize', array('type' => 'hidden'));
        ?>
        <br class="clear" />
        <?php
        echo $form->end('Inserir');
        ?>
    </fieldset>
</div>
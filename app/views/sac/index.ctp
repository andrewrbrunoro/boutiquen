<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('site/contato/index.js', false); ?>



<div class="contact">
	<!-- start contact-form -->
	<div class="contact-form">
		<p>Entre em contato conosco. Informe suas dúvidas, reclamações e elogios, além do seu telefone de contato.</p>
		<?php echo $form->create('Sac', array('id' => 'comente', 'class' => 'grid_11 grid_center Identification', 'url' => array('controller' => 'sac', 'action' => 'index'))); ?>
			<div class="col1 labelblocks" style="width: 280px;">
				<div>
					<?php echo $this->Form->input('Sac.sac_tipo_id', array('div' => false, 'label'=>'Tipo:','class' => 'input2 select2 select22')); ?>
				</div>
				<div class="clear"></div>
				<div>
					<?php echo $form->input('Sac.nome', array('class' => 'input2', 'div' => 'false',  'label' => 'Nome:')); ?>
				</div>
				<div class="clear"></div>
				<div>
					<?php echo $form->input('Sac.email', array('class' => 'input2', 'div' => 'false', 'label' => 'Email:')); ?>
				</div>
				<div class="clear"></div>
				<div>
					<?php echo $this->Form->input('Sac.telefone', array('class' => 'input2 mask-telefone', 'div' => 'false', 'label' => 'Telefone:', 'maxlength' => '15')); ?>
				</div>
				<div class="clear"></div>
				<div>
					<?php echo $this->Form->input('Sac.estado', array('div' => false, 'label'=>'Estado:','class' => 'input2 select2 select22',  'options' => $this->Estados->estadosBrasileiros())); ?>
				</div>
				<div class="clear"></div>
				<div>
					<?php echo $form->input('Sac.cidade', array('class' => 'input2', 'div' => 'false', 'label' => 'Cidade:')); ?>
				</div>
				<div class="clear"></div>
				<div>
					<?php echo $form->input('Sac.mensagem', array('value'=>'', 'div' => 'false', 'label' => 'Mensagem:', 'type' => 'textarea', 'class'=>'textarea')); ?>
				</div>
				<div class="clear"></div>
				<div class="clearl">
				   <?php echo $form->submit('Enviar', array( 'class' => 'button bt-submit-left', 'value' => 'OK')); ?>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			
		<?php echo $form->end(); ?>
	</div>
	<!-- end contact-form -->
	<!-- start map -->
	<div class="map">
		
	</div>
	<!-- end map -->
	<div class="clear"></div>
</div>




<div class="clear"></div>
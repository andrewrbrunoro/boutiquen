<?php 
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('admin/frete_tipos/crud.js',false);
?>
<div class="index">
    <h2 class="left"><?php __('Fretes'); ?></h2>
    <div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Frete', true), array('action' => 'add')); ?>
    </div>       
    <?php echo $form->create('Frete', array('class'=>'formBusca','action'=>'/index'));?>
		<fieldset>
			<div class="left">
				<?php echo $form->input('Filter.cep_inicial',array('div'=>false,'label'=>'Cep Inicial:','class'=>'mask-cep produtosFiltro'));  ?>
				<?php echo $form->input('Filter.cep_final',array('div'=>false,'label'=>'Cep Final:','class'=>'mask-cep produtosFiltro'));  ?>
			</div>
			<div class="left">
				<?php echo $form->input('Filter.frete_tipo_id',array('div'=>false,'options'=>$frete_tipos,'class'=>'produtosSel')); ?>
			</div>			
			<?php echo $form->end('Busca'); ?>
		</fieldset>	
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Tipo de Frete','frete_tipo_id'); ?></th>
            <th><?php echo $this->Paginator->sort('cep_inicial'); ?></th>
            <th><?php echo $this->Paginator->sort('cep_final'); ?></th>
            <th><?php echo $this->Paginator->sort('peso'); ?></th>
            <th><?php echo $this->Paginator->sort('Preço','preco'); ?></th>
            <th><?php echo $this->Paginator->sort('prazo'); ?></th>    
            <th><?php echo $this->Paginator->sort('Descrição','descricao'); ?></th>        
            <th><?php echo $this->Paginator->sort('Status','status'); ?></th>        
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($fretes as $frete):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $frete['Frete']['id']; ?>&nbsp;</td>
                <td><?php echo $frete['FreteTipo']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo $frete['Frete']['cep_inicial']; ?>&nbsp;</td>
                <td align="center"><?php echo $frete['Frete']['cep_final']; ?>&nbsp;</td>
                <td><?php echo $frete['Frete']['peso']; ?>&nbsp;</td>
                <td><?php echo $this->String->bcoToMoeda($frete['Frete']['preco']); ?>&nbsp;</td>
                <td><?php echo $frete['Frete']['prazo']; ?>&nbsp;</td>
                <td align="center"><?php echo $frete['Frete']['descricao']?$frete['Frete']['descricao']:'---'; ?>&nbsp;</td>                
                <td align="center"><?php echo $frete['Frete']['status']?'Ativo':'Inativo'; ?>&nbsp;</td>                
                
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $frete['Frete']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $frete['Frete']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $frete['Frete']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $frete['Frete']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>

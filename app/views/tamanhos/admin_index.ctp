<div class="index">
    <h2 class="left"><?php __('Tamanho'); ?></h2>
    <div class="btAddProduto">
    	<?php echo $this->Html->link(__('Adicionar Tamanho', true), array('action' => 'add')); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($tamanhos as $tamanho):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $tamanho['Tamanho']['id']; ?>&nbsp;</td>
                <td><?php echo $tamanho['Tamanho']['nome']; ?>&nbsp;</td>
                <td><?php echo ($tamanho['Tamanho']['status']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$tamanho['Tamanho']['created']); ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$tamanho['Tamanho']['modified']); ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $tamanho['Tamanho']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $tamanho['Tamanho']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $tamanho['Tamanho']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
      
<?php
echo $this->Javascript->link('site/categorias/index', false);
echo $this->Javascript->link('site/home/index.js');
?>
<?php echo $this->Javascript->link('common/nyro_modal/jquery.nyroModal.custom.js', false); ?>
<?php echo $this->Html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false)); ?>
<?php echo $javascript->link('site/produtos/variacao', false); ?>

<div class="sub-menu">
    <?php echo $this->element('site/left'); ?>
</div>



<div class="products_search">
    <!-- start entry -->
    <div class="entry">
        <?php
        $ordenacao = array(
            'sort:Produto.preco/direction:asc' => 'Menor preço',
            'sort:Produto.preco/direction:desc' => 'Maior preço',
            'sort:Produto.nome/direction:asc' => 'Ordenar A-Z',
            'sort:Produto.nome/direction:desc' => 'Ordenar Z-A',
            'sort:Produto.quantidade_acessos/direction:desc' => 'Mais acessados',
            'sort:Produto.quantidade_vendidos/direction:desc' => 'Mais vendidos',
        );
        ?>
        <?php if(isset($categoria_data)): ?>
            <h2 class="title" style="font-weight:bold;">
				<span class="sort" style="float:right;">
					<?php echo $this->Form->input('order', array(
                            'label' => false,
                            'div' => false,
                            'class' => 'input selectw',
                            'id' => 'ordenacao',
                            'rel' => $this->Html->url(),
                            'value' => (isset($this->params['named']['sort']) && isset($this->params['named']['direction'])) ? 'sort:' . $this->params['named']['sort'] . '/direction:' . $this->params['named']['direction'] : false,
                            'options' => array('' => 'Ordenar por') + $ordenacao)
                    ); ?>
				</span>
            </h2>
        <?php endIf; ?>
        <div class="clear"></div>
        <!-- start product-list -->
        <div class="product-list">
            <?php if($produtos && is_array($produtos) && count($produtos) > 0): ?>
                <ul>
                <?php
                $cont = 1;
                foreach($produtos as $produto): ?>
                    <li>
                        <?php echo $this->element('site/produto', array("produto" => $produto)); ?>
                    </li>
                    <?php if(($cont%4) == 0): ?>
                        </ul><div class="clear"></div><ul>
                    <?php endIf; ?>
                    <?php
                    $cont++;
                endForeach; ?>
                </ul>
            <?php else: ?>
                <p style="width: 100%; text-align: center; padding: 50px">Nenhum produto encontrado</p>
            <?php endIf; ?>
            <div class="clear"></div>
        </div>
        <!-- end product-list -->
        <!-- start pagination -->
        <div class="pagination1">
            <?php
            $pageCount = 1;
            if(isset($this->Paginator->params['paging']['ProdutoCategoria']['pageCount'])){
                $pageCount = $this->Paginator->params['paging']['ProdutoCategoria']['pageCount'];
            }else if(isset($this->Paginator->params['paging']['Produto']['pageCount'])){
                $pageCount = $this->Paginator->params['paging']['Produto']['pageCount'];
            }
            if($pageCount > 1){
                ?>
                <ul>
                    <li class="prev"><?php echo $this->Paginator->prev(__('<<', true), array(), null, array('class' => 'prev')); ?></li>
                    <?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => null)); ?>
                    <li class="next"><?php echo $this->Paginator->next(__('>>', true), array(), null, array('class' => 'next')); ?></li>
                </ul>
            <?php
            }
            ?>
            <div class="clear"></div>
            <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
        </div>
        <div class="clear"></div>

    </div>
    <!-- end entry -->
</div>
<!-- end rightcol -->

<div class="clear"></div>
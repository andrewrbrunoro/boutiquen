<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="page-header">
                <h1 class="page-title">Identificação</h1>
            </header>
            <div class="box-lg">
                <div class="row" data-gutter="60">
                    <div class="col-md-6">
                        <h3 class="widget-title">Minha conta</h3>
                        <?php echo $this->Form->create('Usuario', array('url' => '/login')); ?>
                            <div class="form-group">
                                <label for="email">
                                    E-mail <strong class="text-danger">(*)</strong>
                                </label>
                                <?php echo $this->Form->text('email', array('class' => 'form-control')); ?>
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Senha <strong class="text-danger">(*)</strong>
                                </label>
                                <?php echo $this->Form->text('senha', array('class' => 'form-control', 'type' => 'password')); ?>
                            </div>
                            <button class="btn btn-success" type="submit">
                                Logar
                            </button>
                        <?php echo $this->Form->end(); ?>
                        <br />
                        <a href="#">Esqueceu a senha?</a>
                    </div>
                    <div class="col-md-6">
                        <h3 class="widget-title">Não tenho cadastro</h3>
                        <?php echo $this->Form->create('Usuario', array('url' => '/usuarios/add')); ?>
                            <div class="form-group">
                                <label for="email">
                                    E-mail <strong class="text-danger">(*)</strong>
                                </label>
                                <?php echo $this->Form->text('email', array('class' => 'form-control')); ?>
                            </div>
                            <div class="form-group">
                                <label for="cep">
                                    CEP
                                </label>
                                <?php echo $this->Form->text('cep', array('class' => 'form-control')); ?>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="newsletter" value="true" class="i-check" type="checkbox" /> Receber promoções e desconto por e-mail
                                </label>
                            </div>
                            <button class="btn btn-success" type="submit">
                                Criar conta
                            </button>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
	<header class="page-header">
		<h1 class="page-title">Cadastro</h1>
		<span class="text-danger">
			Todos os campos seguidos de (*) são obrigatórios.
		</span>
	</header>
    <?php echo $this->Form->create('Usuario', array('url' => '/usuarios/edit')) ?>
    <div class="row">
        <div class="col-lg-6">
			<div class="form-group">
				<?php echo $this->Form->input('nome', array(
                    'label' => 'Nome (*)', 'class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'required')) ?>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="data[Usuario][dia]">
						Data de nascimento (*)
					</label>
					<div class="form-group">
						<?php echo $this->Form->input('dia', array('maxlength' => 2, 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'DD', 'label' => false)) ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<?php echo $this->Form->input('mes', array('maxlength' => 2, 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'MM', 'label' => '&nbsp;')) ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<?php echo $this->Form->input('ano', array('maxlength' => 4, 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'YYYY', 'label' => '&nbsp;')) ?>
					</div>
				</div>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['cpf'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('cpf', array(
                    'label' => 'CPF [Cadastro de pessoa física] (*)', 'required' => 'required', 'class' => 'form-control cpf', 'placeholder' => 'CPF')) ?>
			</div
				<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['telefone'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('telefone', array(
                    'label' => 'Telefone [Residencial ou Celular] (*)', 'required' => 'required', 'class' => 'form-control telefone')) ?>
			</div
		</div>
		<div class="col-lg-6">
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['email'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('email', array(
                    'label' => 'E-mail (*)', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'E-mail')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['senha_nova'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('senha_nova', array(
                    'label' => 'Senha', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Senha')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['confirm'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('confirm', array(
                    'label' => 'Repetir senha', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Repetir Senha')) ?>
			</div>
			<div class="form-group">
				<label for="data[Usuario][newsletter]">
					<small>
						<span class="text-info">
							Assine a newsletter e receba 10% de desconto.
						</span>
					</small>
				</label>
				<div class="checkbox">
					<label>
						<?php echo $this->Form->checkbox('receber_newsletter', array('class' => 'i-check')) ?> Receber promoções e desconto por e-mail
					</label>
				</div>
			</div>
		</div>
	</div>

	<hr/>

	<header class="page-header">
		<h1 class="page-title">Adicionar endereço</h1>
		<span class="text-danger">
			Para adicionar um endereço, preencha todos os campos do formulário abaixo.
		</span>
	</header>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['cep'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.cep', array(
                    'label' => 'CEP', 'class' => 'form-control cep', 'placeholder' => 'CEP')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['rua'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.rua', array(
                    'label' => 'Rua/Logradouro', 'class' => 'form-control', 'placeholder' => 'Rua')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['numero'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.numero', array(
                    'label' => 'Número', 'class' => 'form-control', 'placeholder' => 'Número')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['complemento'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.complemento', array(
                    'label' => 'Complemento (Opcional)', 'class' => 'form-control', 'placeholder' => 'Complemento')) ?>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['uf'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.uf', array(
                    'label' => 'Estado', 'class' => 'form-control', 'options' => $this->Estados->estadosBrasileiros(), 'placeholder' => 'Estado')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['cidade'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.cidade', array(
                    'label' => 'Cidade', 'class' => 'form-control', 'placeholder' => 'Cidade')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['bairro'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.bairro', array(
                    'label' => 'Bairro', 'class' => 'form-control', 'placeholder' => 'Bairro')) ?>
			</div>
		</div>
	</div>


	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="row">
				<div class="col-md-12">
					<div class="widget-box">
						<div class="widget-header bordered-bottom bordered-themesecondary">
							<i class="widget-icon fa fa-tags themesecondary"></i>
							<h5 class="widget-caption themesecondary">
								Endereços cadastrados
							</h5>
							<div class="img-circle btn btn-xs btn-info pull-left"
                                 style="margin: 5px 0 0 5px;"
                                 title="Quantidad de endereços">
								<?php echo (isset($this->data['UsuarioEndereco']) && count($this->data['UsuarioEndereco'])) ? count($this->data['UsuarioEndereco']) : 0 ?>
							</div>
						</div><!--Widget Header-->
						<div class="widget-body">
							<div class="widget-main no-padding">
								<div class="tickets-container">
									<ul class="tickets-list">
										<?php if (isset($this->data['UsuarioEndereco']) && count($this->data['UsuarioEndereco'])) { ?>
                                            <?php foreach ($this->data['UsuarioEndereco'] as $endereco) { ?>
                                                <li class="ticket-item">
													<div class="row">
														<div class="ticket-user col-md-10 col-sm-12">
															<img src="https://cdn2.iconfinder.com/data/icons/dellipack/48/home.png"
                                                                 class="user-avatar">
															<span class="user-name"><?php echo $endereco['nome'] ?></span>
															<span class="user-at">Nº</span>
															<span class="user-company"><?php echo $endereco['numero'] ?></span>
															<span class="user-name"><?php echo $endereco['bairro'] . ', ' . $endereco['cidade'] . ' - ' . $endereco['uf'] ?></span>
														</div>
														<div class="ticket-type col-md-2 col-sm-6 col-xs-12">
															<span class="divider hidden-xs"></span>
															<span class="type">
																<?php if ($endereco['status']) { ?>
                                                                    Entrega/cobrança
                                                                <?php } else { ?>
                                                                    Entrega
                                                                <?php } ?>
															</span>
														</div>
														<div class="ticket-state <?php echo ($endereco['status']) ? 'bg-palegreen' : 'bg-yellow' ?>">
															<a href="<?php echo $this->Html->Url('/usuarios/ativaEndereco/' . $endereco['id'] . '/' . $endereco['status']); ?>">
																<?php if ($endereco['status']) { ?>
                                                                    <i class="fa fa-check"
                                                                       title="Ativo"></i>
                                                                <?php } else { ?>
                                                                    <i class="fa fa-times"
                                                                       title="Inativado"></i>
                                                                <?php } ?>
															</a>
														</div>
													</div>
												</li>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <li class="ticket-item">
												<div class="row">
													<div class="ticket-user col-md-6 col-sm-12">
														<img src="https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Desert-48.png"
                                                             class="user-avatar">
														<span class="user-name">Nenhum endereço cadastrado no momento</span>
													</div>
												</div>
											</li>
                                        <?php } ?>
									</ul>
								</div>
							</div>
						</div>
						<div class="widget-header">
							<i class="widget-icon fa fa-info themesecondary"></i>
							<h5 class="widget-caption themesecondary">
								Para editar o endereço, clique sobre o ícone <img src="https://cdn2.iconfinder.com/data/icons/dellipack/32/home.png"
                                                                                  class="user-avatar">
							</h5>
						</div>
						<div class="widget-header bordered-bottom text-danger bordered-themesecondary">
							<i class="widget-icon fa fa-info themesecondary"></i>
							<h5 class="widget-caption themesecondary">
								Para adicionar um novo endereço, preencha o formulário de "Adicionar endereço" e clique em <button type="button"
                                                                                                                                   class="btn btn-xs btn-success">SALVAR REGISTROS</button>
							</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<button type="submit"
                        class="btn btn-success">
					Salvar registros
				</button>
			</div>
		</div>
	</div>
    <?php echo $this->Form->end() ?>
</div>
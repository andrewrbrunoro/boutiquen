
<!-- start identification -->
<div class="fillform2 identificacao7">
	<!-- start fillform -->
	<div class="fillform fillformcustom">
		<h3 class="title">RESTAURAÇÃO DE SENHA</h3>
		<!-- start col2 -->
		<div class="col2">
			<div class="graycol">
				<h3>ALTERAR SENHA</h3>
				<div class="whitecol" style="width: 400px;height: initial;margin-bottom: 20px;">

					<div class="col1" style="padding:20px;width:355px;">
						<?php echo $form->create('Usuario', array('class' => '', 'url' => array('controller' => 'usuarios', 'action' => 'reset', $email, $key)));?>
							<?php echo $this->Form->input('senha', array('div'=>false,'type'=>'password','class' => 'input1', 'label' => 'Nova senha')); ?>
							<div class="clear"></div>
							<input name="submit" type="submit" value="Salvar" class="button1 button15s" />
						<?php echo $this->Form->end(); ?>
					</div>

					<div class="clear"></div>
				</div>
			</div>
		</div>
		<!-- end col2 -->
	</div>
	<!-- end fillform -->
</div>
<!-- end identification -->

<div class="clear"></div>
<div class="container">
	<header class="page-header">
		<h1 class="page-title">Cadastro</h1>
		<span class="text-danger">
			Todos os campos seguidos de (*) são obrigatórios.
		</span>
	</header>
    <?php echo $this->Form->create('Usuario') ?>
    <div class="row">
        <div class="col-lg-6">
			<div class="form-group">
				<?php echo $this->Form->input('nome', array(
                    'label' => 'Nome (*)', 'class' => 'form-control', 'placeholder' => 'Nome', 'required' => 'required')) ?>
			</div>
			<div class="row">
				<div class="col-md-4">
					<label for="data[Usuario][dia]">
						Data de nascimento (*)
					</label>
					<div class="form-group">
						<?php echo $this->Form->input('dia', array('maxlength' => 2, 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'DD', 'label' => false)) ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<?php echo $this->Form->input('mes', array('maxlength' => 2, 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'MM', 'label' => '&nbsp;')) ?>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<?php echo $this->Form->input('ano', array('maxlength' => 4, 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'YYYY', 'label' => '&nbsp;')) ?>
					</div>
				</div>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['cpf'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('cpf', array(
                    'label' => 'CPF [Cadastro de pessoa física] (*)', 'required' => 'required', 'class' => 'form-control cpf', 'placeholder' => 'CPF')) ?>
			</div
				<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['telefone'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('telefone', array(
                    'label' => 'Telefone [Residencial ou Celular] (*)', 'required' => 'required', 'class' => 'form-control telefone')) ?>
			</div
		</div>
		<div class="col-lg-6">
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['email'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('email', array(
                    'label' => 'E-mail (*)', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'E-mail')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['senha_nova'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('senha_nova', array(
                    'label' => 'Senha (*)', 'required' => 'required', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Senha')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['Usuario']['confirm'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('confirm', array(
                    'label' => 'Repetir senha (*)', 'required' => 'required', 'type' => 'password', 'class' => 'form-control', 'placeholder' => 'Repetir Senha')) ?>
			</div>
			<div class="form-group">
				<label for="data[Usuario][newsletter]">
					<small>
						<span class="text-info">
							Assine a newsletter e receba 10% de desconto.
						</span>
					</small>
				</label>
				<div class="checkbox">
					<label>
						<?php echo $this->Form->checkbox('receber_newsletter', array('class' => 'i-check')) ?> Receber promoções e desconto por e-mail
					</label>
				</div>
			</div>
		</div>
	</div>

	<hr />

	<header class="page-header">
		<h1 class="page-title">Adicionar endereço para entrega</h1>
		<span class="text-danger">
			Todos os campos seguidos de (*) são obrigatórios.
		</span>
	</header>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['cep'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.cep', array(
                    'label' => 'CEP (*)', 'required' => 'required', 'class' => 'form-control cep', 'placeholder' => 'CEP')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['rua'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.rua', array(
                    'label' => 'Rua/Logradouro (*)', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'Rua')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['numero'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.numero', array(
                    'label' => 'Número (*)', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'Número')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['complemento'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.complemento', array(
                    'label' => 'Complemento (Opcional)', 'class' => 'form-control', 'placeholder' => 'Complemento')) ?>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['uf'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.uf', array(
                    'label' => 'Estado (*)', 'required' => 'required', 'class' => 'form-control', 'options' => $this->Estados->estadosBrasileiros(), 'placeholder' => 'Estado')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['cidade'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.cidade', array(
                    'label' => 'Cidade (*)', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'Cidade')) ?>
			</div>
			<div class="form-group<?php echo (isset($this->validationErrors['UsuarioEndereco']['bairro'])) ? ' has-error' : '' ?>">
				<?php echo $this->Form->input('UsuarioEndereco.bairro', array(
                    'label' => 'Bairro (*)', 'required' => 'required', 'class' => 'form-control', 'placeholder' => 'Bairro')) ?>
			</div>
		</div>
	</div>




	<div class="row">
		<div class="col-lg-12">
			<div class="form-group">
				<button type="submit"
                        class="btn btn-success">
					Salvar registros
				</button>
			</div>
		</div>
	</div>
    <?php echo $this->Form->end() ?>
</div>
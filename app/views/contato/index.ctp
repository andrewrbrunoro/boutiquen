<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('site/contato/index.js', false); ?>

<!-- start rightside -->
<div class="rightside">
<h2 class="page-title">Fale conosco</h2>
	<p>Aguardamos o seu contato!</p>
	<br /><br />
	<!-- start entry box -->
	<div class="entry">
		<?php echo $form->create('Contato', array('id' => 'comente', 'class' => 'form3', 'url' => array('controller' => 'contato', 'action' => 'index'))); ?>
			<ul class="contato">
				<li>
					<?php echo $this->Session->flash(); ?>
				</li>
				<li>
					<?php echo $form->input('Contato.nome', array('class' => 'input1 inputfield ', 'div' => 'false',  'label' => 'Nome:')); ?>
				</li>
				<li>
					<?php echo $form->input('Contato.email', array('class' => 'input1 inputfield ', 'div' => 'false', 'label' => 'Email:')); ?>
				</li>
				<li>
					<?php echo $this->Form->input('Contato.telefone', array('class' => 'input1 inputfield mask-telefone', 'div' => 'false', 'label' => 'Telefone:', 'maxlength' => '15')); ?>
				</li>
				<li>
					<?php echo $this->Form->input('Contato.estado', array('label'=>'Estado:','class' => 'input1 selectfield',  'options' => $this->Estados->estadosBrasileiros())); ?>
				</li>
				<li>
					<?php echo $form->input('Contato.cidade', array('class' => 'input1 inputfield ', 'div' => 'false', 'label' => 'Cidade:')); ?>
				</li>
				<li>
					<?php echo $form->input('Contato.mensagem', array('value'=>'', 'div' => 'false', 'label' => 'Mensagem:','type' => 'textarea','class'=>'textarea ')); ?>
				</li>
				<li class="clearl">
				   <?php echo $form->submit('Enviar', array( 'class' => 'button1 button', 'value' => '')); ?>
				</li>
			</ul>
		<?php e($form->end())?>
	</div>
	<div class="clear"></div>
	<!-- end entry box -->
</div>
<!-- end rightside -->
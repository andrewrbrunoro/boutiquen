<div class="pedidos index">
	<h2>
		<?php __('Importação'); ?>
	</h2>
	
	<?php echo $form->create( NULL , array( 'type' => 'file')); ?>
		
		<fieldset>
			
			<span style="color:red;">
				ATENÇÃO
			</span>
			<br />
			<span style="color:red;">
				OS ARQUIVOS PRECISAM TER O MESMO NOME, PORÉM, COM SUAS DEVIDAS EXTENSÕES
			</span>
			
			
			<?php echo $form->input('arquivo' , array( 'label' => 'Arquivo' , 'type' => 'file' ) ); ?>
			<span style="color:blue; margin-left:5px;">Extensões aceitas :. xlsx , csv e xml</span>
			<?php echo $form->input('zip' , array( 'label' => 'Arquivo para imagens' , 'type' => 'file' ) ); ?>
			<span style="color:blue; margin-left:5px;">Obrigatório a extensão do arquivo ser .zip</span>

			<!-- <?php echo $form->input('agenda' , array( 'label' => 'Agendar Importação' , 'style' => 'width:100px;' , 'placeholder' => date('d/m/Y') )); ?>
			<div class="clear"></div>
			<span style="color:blue; margin-left:5px;">Este campo não é obrigatório, utilize caso queira agendar a importação</span> -->
		</fieldset>
	
	<?php echo $form->end('Enviar'); ?>
	
	<?php 
		foreach($importacoes as $data){ 
			
			echo $form->create(NULL , array( 'url' => array( 'controller' => 'Importacao' , 'action' => 'importa' ) ));
	?>
	
			<div style="margin-bottom:10px;">
				<div style="float:left;">
					<?php echo $data['Importacao']['arquivo']; ?> 
					<?php if(empty($data['Importacao']['log'])){ echo 'Ainda <strong style="color:red;">não importado</strong>'; }else{ echo '<strong style="color:green;">Importado</strong>'; } ?> 
					<input type="hidden" name="data[Importacao][id]" value="<?php echo $data['Importacao']['id']; ?>" />
				</div>
				
				
				
				<div style="float:left; margin-top:-25px;">
					<?php echo $form->end('Iniciar importação');  ?>
				</div>
				
				
				<div style="float:left; margin-top:-25px;">
					<div class="submit">
						<input type="button" value="Visualizar" />
					</div>
				</div>
				
				
			</div>	

	
	<?php
		} 
	?>
	
</div>
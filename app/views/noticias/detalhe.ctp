<?php echo $html->css('common/nyro_modal/nyroModal.css', null, array('inline' => false)); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom', false); ?>
<?php echo $javascript->link('site/noticias/index', false); ?>
<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "b5c8b4f8-9290-467f-9537-078d59eb7697"});</script>

<div class="borderline"></div>
<!-- start leftcol -->
<div id="leftcol">
    <?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start rightcol -->
<div id="rightcol">
    <!-- bread crumb -->
    <div class="bread_crumb">
        <ul>
            <li><a href="<?php echo $this->Html->Url("/"); ?>" title="Home">Inicial</a></li>
            <?php if (isset($breadcrumb)): ?>
            <?php 
                $cont = 1;
                foreach ($breadcrumb as $bread): 
                    if($cont < count($breadcrumb)):
             ?>
                <?php if ($bread['url'] != "javascript:void(0);"): ?>
                    <li><a href="<?php echo $this->Html->Url("/") . $bread['url']; ?>" title="<?php echo $bread['nome']; ?>"> <?php echo $bread['nome']; ?></a></li>
                <?php else: ?>
                    <li><a href="javascript:void(0);" title="<?php echo $bread['nome']; ?>"> <?php echo $bread['nome']; ?></a></li>
                <?php endif; ?>
            <?php 
                    else:
            ?>
                    <li><?php echo $bread['nome']; ?></li>
            <?php
                    endif;
                $cont++;
                endforeach; ?>
            <?php endIf; ?>
        </ul>
    </div>
    <!-- bread crumb -->
    <?php if (isset($noticia) && !empty($noticia)): ?>
        <!-- start content_noticia -->
        <div class="content_noticia">
            <!-- end entry -->
            <div class="entry">
                <h1><?php echo $noticia['Noticia']['titulo']; ?></h1>
                <?php echo $noticia['Noticia']['conteudo']; ?>
            </div>
            <!-- end entry -->
            <!-- start social2 -->
            <div class="social2">
                <h3><?php echo __("Compartilhe"); ?>:</h3>
                <div class="clear"></div>
                <ul>
                    <li><span class='st_orkut' displayText=''></span></li>
                    <li><span class='st_twitter' displayText=''></span></li>
                    <li><span class='st_facebook' displayText=''></span></li>
                    <li><span class='st_email' displayText=''></span></li>
                    <li><span class='st_googleplus' displayText=''></span></li>
                </ul>
                <div class="clear"></div>
            </div>
            <!-- end social2 -->
        </div>
        <!-- end content_noticia -->
        <?php endIf; ?>
    <?php if (isset($mais_noticias) && count($mais_noticias) > 0): ?>
        <!-- start productcol -->
        <div class="product-list notice-list mais-noticias">
            <h2><?php echo "MAIS NOTÍCIAS"; ?></h2>
            <ul>
                <?php foreach ($mais_noticias as $noticia_lista): ?>
                    <li>
                        <h3><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia_lista['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia_lista['Noticia']['id']); ?>.html" title="<?php echo $noticia_lista['Noticia']['titulo']; ?>"><?php echo $noticia_lista['Noticia']['titulo']; ?></a></h3>
                        <span><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia_lista['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia_lista['Noticia']['id']); ?>.html" title="<?php echo $noticia_lista['Noticia']['titulo']; ?>"><?php echo $this->String->formata_intervalo_data($noticia_lista['Noticia']['created']); ?></a></span>
                        <p><a href="<?php echo $this->Html->Url("/noticias/" . low(Inflector::slug($noticia_lista['Noticia']['titulo'], '-')) . '-' . 'news-' . $noticia_lista['Noticia']['id']); ?>.html" title="<?php echo $noticia_lista['Noticia']['titulo']; ?>"><?php echo $noticia_lista['Noticia']['descricao']; ?></a></p>
                    </li>
                    <?php endForeach; ?>
            </ul>
            <div class="clear"></div>
        </div>
        <!-- end productcol -->
        <?php endIf; ?>
    <div class="clear"></div>
</div>	
<!-- end rightcol -->
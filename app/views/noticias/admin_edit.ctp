<?php
	echo $javascript->link('common/jquery.meio_mask.js', false);
	echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
	echo $javascript->link('admin/noticias/index.js',false);
?>
<div class="index">
<?php echo $this->Form->create('Noticia', array('type' => 'file', 'action'=>'add'));?>
	<fieldset>
		<legend><?php printf(__('Editar %s', true), __('Notícia', true)); ?></legend>
		<div class="left">
			<?php echo $this->Form->input('id'); ?>
			<?php echo $this->Form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo'))); ?>
		</div>
                <div class="left clear">
			<?php echo $this->Form->input('destaque_home',array('type'=>'radio','options'=>array(true=>'Sim',false=>'Não'))); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('titulo',array('label'=>'Título','class'=>'w312 w700 inputs')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('descricao',array('label'=>'Descrição','type'=>'textArea','class'=>'w312 h100 w700 inputs')); ?>
		</div>
		<div class="left clear">
			<?php echo $this->Form->input('conteudo',array('label'=>'Conteúdo','type'=>'textArea','class'=>'mceEditor wCEM h300 w700')); ?>
		</div>
		<br class="clear" />
		<legend>Thumb</legend>
        <?php
			$img = ( isset($this->data['Noticia']['thumb_filename']) ) ? $this->data['Noticia']['thumb_dir'] . '/' . $this->data['Noticia']['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
			echo $this->Form->input('Noticia.thumb_filename', array('type' => 'file'));
			echo $image->resize($img, 80, 80);		
			echo $this->Form->input('Noticia.thumb_dir', array('type' => 'hidden'));
			echo $this->Form->input('Noticia.thumb_mimetype', array('type' => 'hidden'));
			echo $this->Form->input('Noticia.thumb_filesize', array('type' => 'hidden'));
			
			if( isset($this->data['Noticia']['thumb_filename']) ){
				echo $form->input('Noticia.thumb_remove', array('type' => 'checkbox')); 
			}
		?>
		<br class="clear" />
		<?php
			echo $form->end('Salvar');
        ?>
    </fieldset>
</div>
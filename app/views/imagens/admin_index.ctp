    <div class="index">
        <h2><?php __('Imagens'); ?></h2>
        <table cellpadding="0" cellspacing="0">
            <tr>

                <th>id</th>
                <th>Imagem</th>
                <th>filename</th>
                <th>Data Criação</th>
                <th>Data Modificação</th>
            </tr>
        <?php
        $i = 0;
        foreach ($imagens as $imagem):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $imagem['Imagem']['id']; ?>&nbsp;</td>
                <td><?php echo $image->resize($imagem['Imagem']['dir'].DS.$imagem['Imagem']['filename'], 100, 100,true);?>&nbsp;</td>
                <td><?php echo $imagem['Imagem']['filename']; ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$imagem['Imagem']['created']); ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$imagem['Imagem']['modified']); ?>&nbsp;</td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
</div>

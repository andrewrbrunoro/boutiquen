<?php

/**
 * @version 1.1
 * @author Luan Garcia 
 */
class UploadifyHelper extends Helper {

    var $helpers = array('Html', 'Image');

    public function build($imgs, $model = "ProdutoImagem", $destaque = false, $path_default = '../../../../', $order = true) {
        App::import("helper", "Image");
        $this->Image = new ImageHelper();
        $string = '';
        if ($imgs):
            foreach ($imgs as $img):
                
                if (isset($img['tmp_file'])) {
                    $imagem = $path_default . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
                } else {
                    $imagem = $path_default . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
                }

                $id = isset($img['tmp_file']) ? $img['tmp_file'] : $img['id'];
                $name = isset($img['tmp_file']) ? $img['tmp_file'] : $img['filename'];

                $string .= '<div class="uploadifyQueue" id="file_uploadQueue">
								<div class="uploadifyQueueItem">
									<div class="cancel">' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '</div>										
									<span class="fileName">';
									
				if($order == true){
					'<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[' . $model . '][ordem]" /></label>';
				}
										
                if ($destaque == true) {
                    $checked = "";
                    if ($img['destaque'] == 1) {
                        $checked = "checked='checked'";
                    }
                    $string .= '<label>Destaque <input type="checkbox" class="destaque" value="' . $img['destaque'] . '" rel="' . $id . '" name="data[' . $model . '][destaque]" ' . $checked . ' /></label>';
                }
                $string .= '<img src="' . $imagem . '" alt="Foto" /> ' . $name . '
									</span>
									<span class="percentage"> - 100%</span>											
									</div>
								</div>';
            endForeach;
        endIf;
        return $string;
    }

    public function imagens_news($imgs, $destaque = false, $order = true, $data = null, $temp = false, $recursive = false) {
       
	    App::import("helper", "Image");
        $this->Image = new ImageHelper();
		
		App::import("helper", "Html");
        $this->Html = new HtmlHelper();
       
		$model 			= "GeranewsImagem";
		
		$string 		= '';
		if ($imgs):
			$string 	.= '<table><tr>';
			
			foreach ($imgs as $i => $img):
				
				$string		.= '<td>';
				
				$path_default = $this->Html->Url('/', true);
                
                if (isset($img['tmp_file'])) {
                    $imagem = $path_default . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
                } else {
                    $imagem = $path_default . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
                }

                $id = isset($img['tmp_file']) ? $img['tmp_file'] : $img['id'];
                $name = isset($img['tmp_file']) ? $img['tmp_file'] : $img['filename'];
				
				// não é cadastrado temporario? então classe de exclusao de banco, senão, exclusao da sessao
				if($temp == false){
					$class_rm = "rm-img-old";
				}else{
					$class_rm = "rm-img-tmp";
				}
				
				if($recursive == true){
					$data_recursive = 1;
				}else{
					$data_recursive = 0;
				}
				
				$string .= '<div class="uploadifyQueue" id="file_uploadQueue">
								<div class="uploadifyQueueItem">
									<div class="cancel"><a href="javascript:;" rel="' . $id . '" class="rm '. $class_rm .'" data-grade-id="'.$img['geranew_id'].'" data-recursive="'.$data_recursive.'">remover</a></div>										
									<span class="fileName">';
				
                if ($destaque == true) {
                    $checked = "";
                    if ($img['destaque'] == 1) {
                        $checked = "checked='checked'";
                    }
                    $string .= '<label>Destaque <input type="checkbox" class="destaque" value="' . $img['destaque'] . '" rel="' . $id . '" name="data[' . $model . '][destaque]" ' . $checked . ' /></label>';
                }

                $string .= '<img src="' . $imagem . '" alt="Foto" /> ' . $name . '
									</span>
									<span class="percentage"> - 100%</span>											
									</div>';
				
				if($order == true){
					$string .= '<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[' . $model . ']['.$id.'][ordem]" /></label>';
					$string .= '<label>Link <input type="text" class="link" value="' . $img['link'] . '" rel="' . $id . '" name="data[' . $model . ']['.$id.'][link]" /></label>';
				}
				
				
				$string .= '</div>';
				$string	.= '</td>';
				
				if(($i+1)%4 == 0 && isset($imgs[$i+1])){
					$string 	.= '</tr><tr>';
				}
				
            endForeach;
			$string 	.= '</tr></table>';
        endIf;
        return $string;
    }

	public function imagens_grades($imgs, $destaque = false, $order = true, $data = null, $temp = false, $recursive = false) {
       
	    App::import("helper", "Image");
        $this->Image = new ImageHelper();
		
		App::import("helper", "Html");
        $this->Html = new HtmlHelper();
       
		$model 			= "GradeImagem";
		
		$string 		= '';
		if ($imgs):
			$string 	.= '<table><tr>';
			
			foreach ($imgs as $i => $img):
				
				$string		.= '<td>';
				
				$path_default = $this->Html->Url('/', true);
                
                if (isset($img['tmp_file'])) {
                    $imagem = $path_default . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
                } else {
                    $imagem = $path_default . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
                }

                $id = isset($img['tmp_file']) ? $img['tmp_file'] : $img['id'];
                $name = isset($img['tmp_file']) ? $img['tmp_file'] : $img['filename'];
				
				// não é cadastrado temporario? então classe de exclusao de banco, senão, exclusao da sessao
				if($temp == false){
					$class_rm = "rm-img-old";
				}else{
					$class_rm = "rm-img-tmp";
				}
				
				if($recursive == true){
					$data_recursive = 1;
				}else{
					$data_recursive = 0;
				}
				
				$string .= '<div class="uploadifyQueue" id="file_uploadQueue">
								<div class="uploadifyQueueItem">
									<div class="cancel"><a href="javascript:;" rel="' . $id . '" class="rm '. $class_rm .'" data-grade-id="'.$img['grade_id'].'" data-recursive="'.$data_recursive.'">remover</a></div>										
									<span class="fileName">';
									
				if($order == true){
					'<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[' . $model . '][ordem]" /></label>';
				}
										
                if ($destaque == true) {
                    $checked = "";
                    if ($img['destaque'] == 1) {
                        $checked = "checked='checked'";
                    }
                    $string .= '<label>Destaque <input type="checkbox" class="destaque" value="' . $img['destaque'] . '" rel="' . $id . '" name="data[' . $model . '][destaque]" ' . $checked . ' /></label>';
                }

                $string .= '<img src="' . $imagem . '" alt="Foto" /> ' . $name . '
									</span>
									<span class="percentage"> - 100%</span>											
									</div>';
				
				if($data != null && is_array($data) && count($data) > 0){
					$string .= '<div class="box_imagens_grades"><table><tr><td>';
					//$string .= '<a href="javascript:void(0);" class="btn-exibir-grades">Exibir grades</a>';
					foreach($data as $y => $dt){
						$string .= '<table class="lista-grades" style="display: block;">';
						$string .= '<tr>';						
							//defino se a input virá 'checkada' ou não
							$checked = '';
							$disabled = '';
							$grades_filhas = false;
							
							$img['id_img'] = $img['id'];
							//se houver o index de grade, eh porque a imagem possui filhas (imagens clonadas dela mesma)
							if(isset($img['grades']) && $img['grades'] != ""){
								$json = json_decode($img['grades'], true);
								//se a imagem atual tive o index da grade atual no array, check a mesma, e atualiza o id da imagem
								if(is_array($json) && array_key_exists($y, $json)){
									$grades_filhas = true;
									$img['id_img'] = $json[$y];
								}
							}
							//debug($img);
							//checked
							if($y == $img['grade_id'] || $grades_filhas == true){
								$checked = 'checked="checked"';
							}
							if($checked!="" && $y == $img['grade_id']){
								$disabled = 'disabled="disabled"';
							}
							
							// não é cadastrado temporario? então classe de exclusao de banco, senão, exclusao da sessao
							if($temp == false){
								$check_grade_imagem = "check_grade_imagem";
							}else{
								$check_grade_imagem = "check_grade_imagem_tmp";
							}
							
							//monto a input
							$string .= '<td colspan="2">
											<input 
												class='.$check_grade_imagem.'
												type="checkbox" 
												value="'.$y.'" '.$checked.' ' . $disabled . 'rel="'.$img['id_img'].'" 
												name="grade-'.$y.'-'.$img['id_img'].'" 
												id="grade-'.$y.'-'.$img['id_img'].'"
                                                                                                produto_id="'.$img['produto_id'].'"
                                                                                                parent_id="'.$img['parent_id'].'" />
												<label for="grade-'.$y.'-'.$img['id_img'].'">Grade: ' .$y.'</label>
										</td>';
						
						$string .= '</tr>';
						$string .= '<tr>';						
							//listo os tipos e valores de variacoes da grade
							foreach($dt as $vl){
								$string .= '<td>';	
									$string .= $vl['variacao_tipo'] .'-' . $vl['variacao_valor'];
								$string .= '</td>';
							}
						$string .= '</tr>';
						$string .= '</table>';						
					}					
					$string .= '</td></tr></table></div>';
				}
				
				$string .= '</div>';
				$string	.= '</td>';
				
				if(($i+1)%2 == 0 && isset($imgs[$i+1])){
					$string 	.= '</tr><tr>';
				}
				
            endForeach;
			$string 	.= '</tr></table>';
        endIf;
        return $string;
    }

}
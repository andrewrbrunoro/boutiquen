<?php


    class StringHelper extends AppHelper
    {

        /**
         * @param $ids
         * @return array
         */
        public function getProducts($ids)
        {
            App::import('Model', 'Produto');
            $this->Produto = new Produto();
            return $this->Produto->getProducts($ids);
        }

        /**
         * @param     $grade_id
         * @param int $limit
         * @return array
         */
        public function getImages($grade_id, $limit = 2)
        {
            App::import('Model', 'GradeImagem');
            $this->GradeImagem = new GradeImagem();
            return $this->GradeImagem->getShowcaseImages($grade_id, $limit);
        }

        public function getTypeMethod($code)
        {
            App::import('Model', 'PagseguroCode');
            $this->PagseguroCode = new PagseguroCode();

            $query = $this->PagseguroCode->find('first',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'PagseguroCode.codigo' => $code
                    )
                )
            );

            if ($query)
                return $query['PagseguroCode']['nome'];
            else {
                return 'Não foi possível finalizar a compra';
            }
        }

        public function cropSentence($str, $size = 50, $endOfStr = "...", $cutchars = null, $htmlencode = false, $htmldecode = false)
        {
            $str = strip_tags($str);
            $tamanho_inicial = strlen($str);
            if (is_null($cutchars))
                $cutchars = str_split(" ,.;!/\"'?(){}[]\t\n\r+-@#$%&*\\`");
            elseif (!is_string($cutchars))
                return $str;
            else
                $cutchars = str_split($cutchars);
            if ($htmldecode)
                $str = html_entity_decode($str);
            if (strlen($str) > $size) {
                for ($i = $size; $i > 0; $i--) {
                    if (in_array($str[$i], $cutchars))
                        break;
                }
                $str = substr($str, 0, $i);
            }
            $str = rtrim($str, implode('', $cutchars));
            $tamanho_final = strlen($str);
            if ($tamanho_inicial == $tamanho_final)
                $endOfStr = "";

            if ($htmlencode)
                return htmlentities($str);

            return $str . $endOfStr;
        }

        public function getImgVariacao($name)
        {
            App::import('Model', 'Variacao');
            $this->Variacao = new Variacao();

            $query = $this->Variacao->find("first",
                array(
                    'fields'     => array('thumb_filename'),
                    'conditions' => array(
                        'Variacao.valor' => $name
                    )
                )
            );

            if ($query) {
                return $query['Variacao']['thumb_filename'];
            } else {
                return 'sem_imagem.png';
            }
        }

        public function changeNullTo($string, $changeStr)
        {
            return empty($string) ? $changeStr : $string;
        }

        public function decimalFormat($to, $valor, $prec = 2)
        {
            if ($to == "db") {
                return (float)$this->moedaToBco($valor);
            } elseif ($to == "human") {
                return $this->bcoToMoeda($valor, $prec);
            }
        }

        public function moedaToBco($valor)
        {
            return (float)str_replace(",", ".", str_replace(".", "", $valor));
        }

        public function bcoToMoeda($valor, $prec = 2)
        {
            return number_format($valor, $prec, ",", ".");
        }

        /**
         * Remover os acentos de uma string
         *
         * @param string $str
         * @return string
         */
        public function removerAcentos($str)
        {
            $from = 'ÀÁÃÂÉÊÍÓÕÔÚÜÇàáãâéêíóõôúüç';
            $to = 'AAAAEEIOOOUUCaaaaeeiooouuc';
            return strtr($str, $from, $to);
        }

        public function remove_accent($texto)
        {
            $oque = array("/(?i)á|ã|â|Á|Ã|Â/", "/(?i)é|ê|É|Ê/", "/(?i)í|î|Í|Î/", "/(?i)ó|õ|ô|Ó|Ô|Õ/", "/(?i)ú|û|Ú|Û/", "/(?i)ç|Ç/", "/(?i)º|ª/");
            $peloque = array("a", "e", "i", "o", "u", "c", "");
            return preg_replace($oque, $peloque, $texto);
        }

        //metodo que deixa a primeira letra de cada palavra em maiuscula, com tratamento de exceção
        function title_case($title)
        {
            //return $title;
            //termos que não serão deixados em maiusculo
            $smallwordsarray = array(
                'da', 'de', 'do', 'e', 'das', 'dos', 'para', 'p/'
            );

            //caracteres especiais
            $trans = array('á' => 'Á', 'é' => 'É', 'í' => 'Í', 'ó' => 'Ó', 'ç' => 'Ç');
            $trans2 = array('Á' => 'á', 'Ã' => 'ã', 'É' => 'é', 'Í' => 'í', 'Ó' => 'ó', 'Ç' => 'ç');

            //separo as palavras
            $words = explode(' ', $title);
            foreach ($words as $key => $word) {
                if ($key == 0 or !in_array($word, $smallwordsarray))
                    $words[$key] = ucwords(strtolower($word));
            }

            // volto a juntar as palavras e retorno o termo
            $newtitle = implode(' ', $words);
            //die;

            $primeira_letra = mb_substr($newtitle, 0, 1);
            $primeira_letra = strtr($primeira_letra, $trans);
            return strtr($primeira_letra . mb_substr($newtitle, 1), $trans2);
            return $primeira_letra . mb_substr($newtitle, 1);
            //return $newtitle;
        }

        //script que pega o intervalo da data
        //script encontrado na internet
        public function formata_intervalo_data($time)
        {

            $time = strtotime($time);
            $diff = time() - $time;

            $seconds = $diff;
            $minutes = round($diff / 60);
            $hours = round($diff / 3600);
            $days = round($diff / 86400);
            $weeks = round($diff / 604800);
            $months = round($diff / 2419200);
            $years = round($diff / 29030400);

            if ($seconds <= 60)
                return "$seconds segundos atras";
            else if ($minutes <= 60)
                return $minutes == 1 ? 'um minuto atrás' : $minutes . ' minutos atrás';
            else if ($hours <= 24)
                return $hours == 1 ? 'uma hora atrás' : $hours . ' horas atrás';
            else if ($days <= 7)
                return $days == 1 ? 'um dia atrás' : $days . ' dias atrás';
            else if ($weeks <= 4)
                return $weeks == 1 ? 'uma semana atrás' : $weeks . ' semanas atrás';
            else if ($months <= 12)
                return $months == 1 ? 'um mês atrás' : $months . ' meses atrás';
            else
                return $years == 1 ? 'um ano atrás' : $years . ' anos atrás';
        }

        /*
         * retorna a letra do alfabeto referente a posição passa por parametro
         * (metodo usado na tela de 'localizacao', com intuito de formatação)
         */

        public function get_letra_por_numero($numero)
        {

            switch ($numero) {
                case '1':
                    return 'A';

                case '2':
                    return 'B';

                case '3':
                    return 'C';

                case '4':
                    return 'D';

                case '5':
                    return 'E';

                case '6':
                    return 'F';

                case '7':
                    return 'G';

                case '8':
                    return 'H';

                case '9':
                    return 'I';

                case '10':
                    return 'J';

                case '11':
                    return 'K';

                case '12':
                    return 'L';

                case '13':
                    return 'M';

                case '14':
                    return 'N';

                case '15':
                    return 'O';

                case '16':
                    return 'P';

                case '17':
                    return 'Q';

                case '18':
                    return 'R';

                case '19':
                    return 'S';

                case '20':
                    return 'T';

                case '21':
                    return 'U';

                case '22':
                    return 'V';

                case '23':
                    return 'W';

                case '24':
                    return 'X';

                case '25':
                    return 'Y';

                case '26':
                    return 'Z';
            }

        }

        public function saudacao($nome = '')
        {
            date_default_timezone_set('America/Sao_Paulo');
            $hora = date('H');
            if ($hora >= 6 && $hora <= 12)
                return 'Bom dia' . (empty($nome) ? '' : ', ' . $nome);
            else if ($hora > 12 && $hora <= 18)
                return 'Boa tarde' . (empty($nome) ? '' : ', ' . $nome);
            else
                return 'Boa noite' . (empty($nome) ? '' : ', ' . $nome);
        }

        public function getFrete($tipo, $cep_destino, $formato = 'caixa')
        {
            $data = array(
                'tipo'        => $tipo, // opções: `sedex`, `sedex_a_cobrar`, `sedex_10`, `sedex_hoje`, `pac`, 'pac_contrato', 'sedex_contrato' , 'esedex'
                'formato'     => $formato, // opções: `caixa`, `rolo`, `envelope`
                'cep_destino' => preg_replace("/[^0-9]/", "", $cep_destino), // Obrigatório
                'cep_origem'  => Configure::read('Correios.cep_origem'), // Obrigatorio
                //'empresa'         => '', // Código da empresa junto aos correios, não obrigatório.
                //'senha'           => '', // Senha da empresa junto aos correios, não obrigatório.
                'peso'        => '1', // Peso em kilos
                'comprimento' => '34', // Em centímetros
                'altura'      => '14', // Em centímetros
                'largura'     => '12', // Em centímetrosss
                'diametro'    => '0', // Em centímetros, no caso de rolo
                // 'mao_propria'       => '1', // Não obrigatórios
                // 'valor_declarado'   => '1', // Não obrigatórios
                // 'aviso_recebimento' => '1', // Não obrigatórios
            );
            return $GLOBALS['Freight']->frete($data);
        }

    }
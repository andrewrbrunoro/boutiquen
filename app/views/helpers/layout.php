<?php

class LayoutHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    var $helpers = array(
        'Html'
    );

/**
 * Constructor
 *
 * @param array $options options
 * @access public
 */
    function __construct($options = array()) {
        $this->View =& ClassRegistry::getObject('view');
        return parent::__construct($options);
    }

/**
 * Show Menu by Alias
 *
 * @param string $menuAlias Menu alias
 * @param array $options (optional)
 * @return string
 */
    function menu($menuAlias, $options = array()) {
        $_options = array(
            'findOptions' => array(),
            'tag' => 'ul',
            'tagAttributes' => array(),
            'containerTag' => 'div',
            'containerTagAttr' => array(
                'class' => 'menu',
            ),
            'selected' => 'selected',
            'dropdown' => false,
            'dropdownClass' => 'sf-menu',
        );
        $options = array_merge($_options, $options);
		if (!isset($this->View->viewVars['menus_for_layout'][$menuAlias])){
            return false;
        }
		
        $menu = $this->View->viewVars['menus_for_layout'][$menuAlias];

        $options['containerTagAttr']['id'] = 'menu-' . $this->View->viewVars['menus_for_layout'][$menuAlias]['Menu']['id'];
        $options['containerTagAttr']['class'] .= '';

        $links = $this->View->viewVars['menus_for_layout'][$menuAlias]['threaded'];
        $linksList = $this->nestedLinks($links, $options);
		
		$output = $this->Html->tag($options['containerTag'], $linksList, $options['containerTagAttr']);

        return $output;
    }
/**
 * Nested Links
 *
 * @param array $links model output (threaded)
 * @param array $options (optional)
 * @param integer $depth depth level
 * @return string
 */
    function nestedLinks($links, $options = array(), $depth = 1) {
        $_options = array();
        $options = array_merge($_options, $options);

		if(isset($options['registro_coluna']) && $options['registro_coluna'] != ""){
			$options['registro_linha'] = ceil(count($links)/$options['registro_coluna']);
		}
		
        $output = '';
		$cont = 1;
	
        foreach ($links AS $link) {
            $linkAttr = array(
                'id' => 'link-' . $link['Link']['id'],
                'rel' => $link['Link']['rel'],
                'target' => $link['Link']['target'],
                'title' => $link['Link']['description'],
            );

            foreach ($linkAttr AS $attrKey => $attrValue) {
                if ($attrValue == null) {
                    unset($linkAttr[$attrKey]);
                }
            }

            if (Router::url($link['Link']['link']) == Router::url('/' . $this->params['url']['url'])) {
                $linkAttr['class'] = $options['selected'];
            }

            if (isset($link['Link']['thumb_filename']) && null != $link['Link']['thumb_filename']) {
            	$linkOutput = $this->Html->image('/uploads/links/thumb/'.$link['Link']['thumb_filename'], 
            			array('alt'=> $link['Link']['title'], 'height' => 15, 'url' => $link['Link']['link'])
            	);
            } else {
            	$linkOutput = $this->Html->link($link['Link']['title'], $link['Link']['link'], $linkAttr);
            }

            if (isset($link['children']) && count($link['children']) > 0) {
                $linkOutput .= $this->nestedLinks($link['children'], $options, $depth + 1);
            }
            $linkOutput = $this->Html->tag('li', $linkOutput);
			
			$output .= $linkOutput;
			
			if(isset($options['registro_linha']) && $options['registro_linha'] != ""){
				if($cont%$options['registro_linha'] == 0){
					 $output .= "</ul><ul>";
				}
			}
			
			$cont++;
        }
        if ($output != null) {
            $tagAttr = $options['tagAttributes'];
            if ($options['dropdown'] && $depth == 1) {
                $tagAttr['class'] = $options['dropdownClass'];
            }
            $output = $this->Html->tag($options['tag'], $output, $tagAttr);
        }

        return $output;
    }
}
?>
<?php

	class CalendarioHelper extends AppHelper {
	
		/**
			Converte tempo para timestamp (contagem de segundos) absoluta, n�o suporta anos
			
		*/
		public function DataToTimestamp($data){
			$acumulador = 0;
			if (preg_match('/([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+)/',$data,$m)) {
				$acumulador += $m[6];
				$acumulador += $m[5]*60;
				$acumulador += $m[4]*60*60;
				$acumulador += $m[3]*60*60*24;
				$acumulador += $m[2]*60*60*24*30.4375;
				$acumulador += $m[1]*60*60*24*365.25;
				$acumulador = (int) $acumulador;
			}elseif(preg_match('/([0-9]+):([0-9]+):([0-9]+)/',$data,$m)){
				$acumulador += $m[3];
				$acumulador += $m[2]*60;
				$acumulador += $m[1]*60*60;
			}
			return $acumulador;
		}

			// Faz ao contr�rio da datatotimestamp
		public function TimestampToData($segundos){
			$segundos = abs($segundos);
			$anos = floor($segundos/(60*60*24*365.25));
			$segundos -= $anos*60*60*24*365.25;
			$meses = floor($segundos/(60*60*24*30.4375));
			$segundos -= $meses*60*60*24*30.4375;
			$dias = floor($segundos/(60*60*24));
			$segundos -= $dias*60*60*24;
			$horas = floor($segundos/(60*60));
			$segundos -= $horas*60*60;
			$minutos = floor($segundos/60);
			$segundos -= $minutos*60;
			return sprintf("%04s-%02s-%02s %02s:%02s:%02s",$anos,$meses,$dias,$horas,$minutos,$segundos);
		}

		/**
			Realiza a previsão de uma data para um tempo a partir de horários semanais e exceções
			O formato dos horários é
				array(
						1=>array(array('00:00:00','02:00:00')), // Segunda
						2=>array(array('00:00:00','02:00:00')),
						3=>array(array('00:00:00','02:00:00')),
						4=>array(array('00:00:00','02:00:00')),
						5=>array(array('00:00:00','02:00:00')),
						6=>array(array('00:00:00','02:00:00')),
						7=>array(array('00:00:00','02:00:00')),
					)
		*/
		public function FlowTime($datainicial,$tempo,$horarios=null,$excecoes=null){
			$datainicial_segundos = $this->DataToTime($datainicial);
			$tempo_segundos = Calendario::DataToTimestamp($tempo);
			$fim_segundos = $datainicial_segundos+$tempo_segundos;
			$fim = $this->DataFormatada('Y-m-d H:i:s',$fim_segundos);
			$turnos_n = array();
			$utime = 0;
			foreach($horarios as $dia=>$turns){
				$turnos_n[$dia] = array();
				$ts = '00:00:00';
				foreach($turns as $k=>$v){
					$turnos_n[$dia][] = array($ts,$v[0],1);
					$turnos_n[$dia][] = array($v[0],$v[1],-1);
					$utime += $this->DataToTime($v[1])-$this->DataToTime($v[0]);
					$ts = $v[1];
				}
				$turnos_n[$dia][] = array($ts,'tomorrow',1);
			}
			if(is_array($excecoes)){
				foreach($excecoes as $dia=>$turns){
					$excecoes_n[$dia] = array();
					$ts = '00:00:00';
					foreach($turns as $k=>$v){
						$excecoes_n[$dia][] = array($ts,$v[0],1);
						$excecoes_n[$dia][] = array($v[0],$v[1],-1);
						$ts = $v[1];
					}
					$excecoes_n[$dia][] = array($ts,'tomorrow',1);
				}
			}
			if($utime<10){
				return $fim_segundos;
			}

			$verificado = $datainicial_segundos;

			do{
			
				$dia = date('Y-m-d',$verificado);
				if(isset($excecoes_n[$dia]))
					$turno = $excecoes_n[$dia];
				else 
					$turno = $turnos_n[date('N',$verificado)];

				foreach($turno as $day=>$turn){
					if($verificado>=$fim_segundos)
						break 2;
					
					$start_tempo = $this->DataToTime($turn[0],$verificado);
					$end_tempo = $this->DataToTime($turn[1],$verificado);

					if($verificado>=$end_tempo)
						continue;
					elseif($verificado>=$start_tempo && $verificado<$end_tempo){
						if($turn[2]>0)
							$fim_segundos += $end_tempo-$verificado;
						$verificado = $end_tempo;
					}elseif($end_tempo<=$fim_segundos){
						if($turn[2]>0)
							$fim_segundos += $end_tempo-$start_tempo;
						$verificado = $end_tempo;
					}else{
						if($turn[2]>0)
							$fim_segundos += $end_tempo+($fim_segundos-$start_tempo);
						$verificado = $end_tempo;
					}
				}
				
			}while($verificado<$fim_segundos);
			
			$fim = $this->DataFormatada('Y-m-d H:i:s',$fim_segundos);
			
			return $fim;
		}
		// Undertime realiza a ação contrária a flowtime
		function UnderTime($datainicial,$tempo,$horarios=null,$excecoes=null){
			$datainicial_segundos = $this->DataToTime($datainicial);
			$tempo_segundos = Calendario::DataToTimestamp($tempo);
			$fim_segundos = $datainicial_segundos+$tempo_segundos;
			$fim = $this->DataFormatada('Y-m-d H:i:s',$fim_segundos);
			$turnos_n = array();
			$utime = 0;
			foreach($horarios as $dia=>$turns){
				$turnos_n[$dia] = array();
				$ts = '00:00:00';
				foreach($turns as $k=>$v){
					$turnos_n[$dia][] = array($ts,$v[0],+1);
					$turnos_n[$dia][] = array($v[0],$v[1],-1);
					$utime += $this->DataToTime($v[1])-$this->DataToTime($v[0]);
					$ts = $v[1];
				}
				$turnos_n[$dia][] = array($ts,'tomorrow',1);
			}
			if(is_array($excecoes)){
				foreach($excecoes as $dia=>$turns){
					$excecoes_n[$dia] = array();
					$ts = '00:00:00';
					foreach($turns as $k=>$v){
						$excecoes_n[$dia][] = array($ts,$v[0],1);
						$excecoes_n[$dia][] = array($v[0],$v[1],-1);
						$ts = $v[1];
					}
					$excecoes_n[$dia][] = array($ts,'tomorrow',1);
				}
			}
			if($utime<10){
				return $this->DataFormatada('Y-m-d H:i:s',$fim_segundos);
			}

			$verificado = $datainicial_segundos;
			$final_fake = $fim_segundos;

			do{

				$dia = date('Y-m-d',$verificado);
				if(isset($excecoes_n[$dia]))
					$turno = $excecoes_n[$dia];
				else
					$turno = $turnos_n[date('N',$verificado)];

				foreach($turno as $day=>$turn){
					if($verificado>=$fim_segundos)
						break 2;

					$start_tempo = $this->DataToTime($turn[0],$verificado);
					$end_tempo = $this->DataToTime($turn[1],$verificado);

					if($verificado>=$end_tempo){
						continue;
					}elseif($verificado>=$start_tempo && $verificado<$end_tempo){
						if($turn[2]>0)
							$final_fake -= $end_tempo-$verificado;
						//else
							//$final_fake += $verificado-$start_tempo;
						$verificado = $end_tempo;
					}elseif($end_tempo<=$fim_segundos){
						if($turn[2]>0)
							$final_fake -= $end_tempo-$start_tempo;
						$verificado = $end_tempo;
					}else{
						if($turn[2]>0)
							$final_fake -= $end_tempo+($fim_segundos-$start_tempo);
						$verificado = $end_tempo;
					}
				}

			}while($verificado<$fim_segundos);

			$fim = $this->DataFormatada('Y-m-d H:i:s',$final_fake);

			return $fim;
		}

			
		/**
		* Retorna o timestamp da data informada, é uma versão otimizada do strtotime
		*
		* @param string Data
		* @param string Data relativa (quando necessário para cálculo, como "próxima segunda")
		*/
		public function DataToTime($data,$relative=null){
			if(is_numeric($data)) return $data;
			if(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)[ \t\n\r]*([0-9]{2})/i',$data,$m)){
				$hora = $m[4]; $minuto = $m[6]; $segundo = $m[8]; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
			}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|m|min|mins|minutos|minuto|mn)/i',$data,$m)){
				$hora = $m[4]; $minuto = $m[6]; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
			}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})[ \t\n\r]*([0-9]{2})[ \t\n\r]*(:|h|hour|hours|horas|hr)/i',$data,$m)){
				$hora = $m[4]; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
			}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4}|[0-9]{2})/',$data,$m)){
				$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = $m[3];
			}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{1,2})/',$data,$m)){
				$hora = 0; $minuto = 0; $segundo = 0; $dia = $m[1]; $mes = $m[2]; $ano = date('Y',is_null($relative)?time():$relative);
			}elseif(preg_match('/([0-9]{1,2})[ \t\n\r]*\/[ \t\n\r]*([0-9]{4})/',$data,$m)){
				$hora = 0; $minuto = 0; $segundo = 0; $dia = 1; $mes = $m[1]; $ano = $m[2];
			}else{
				$tr = array(
					'hoje'=>'today','dia'=>'day', 'semana'=>'week', 'hora'=>'hour', 'minuto'=>'minute', 'segundo'=>'second',
					'meses'=>'months','mês'=>'month','mes'=>'month', 'ano'=>'year',
					'proxima'=>'next', 'próxima'=>'next',  'próximo'=>'next',  'próximo'=>'next', 
					'Última'=>'last', 'ultima'=>'last','ultimo'=>'last','Último'=>'last',
					'segunda-feira'=>'monday','segunda'=>'monday','terça-feira'=>'tuesday','terça'=>'tuesday',
					'quarta-feira'=>'wednesday','quarta'=>'wednesday', 'quinta-feira'=>'thursday','quinta'=>'thursday', 
					'sexta-feira'=>'friday','sexta'=>'friday', 'sábado'=>'saturday','sabado'=>'saturday', 'domingo'=>'sunday',
					'janeiro'=>'january', 'jan'=>'january', 'fevereiro'=>'february','fev'=>'february', 
					'março'=>'march','mar'=>'march', 'abril'=>'april', 'abr'=>'april',
					'maio'=>'may','mai'=>'may', 'junho'=>'june', 'jun'=>'june', 
					'julho'=>'july','jul'=>'july', 'agosto'=>'august', 'ago'=>'august', 
					'setembro'=>'september','set'=>'september', 'outubro'=>'october', 'out'=>'october', 
					'novembro'=>'november','nov'=>'november', 'dezembro'=>'december','dez'=>'december',
					'depois de amanhã'=>'+2 day','depois de amanha'=>'+2 day',
					'anteontem'=>'-2 day',
					'amanhã'=>'tomorrow','amanha'=>'tomorrow','ontem'=>'yesterday',' de '=>''
				
				);
				return strtotime(str_ireplace(array_keys($tr),array_values($tr),$data),is_null($relative)?time():($this->DataToTime($relative)));		
			}
			return mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
		}
		
		/**
		* Formata data para a string date passada
		*
		* @param string Formato (utilizar nota��o do PHP), se n�o informada gera DateTime Sem Timezone
		* @param string Data
		* @param string Data relativa (quando necess�rio para c�lculo, como "pr�xima segunda")
		*/
		public function DataFormatada($formato=null,$data=null,$relative=null){
			if(is_null($data)) $data = time();
			if(is_null($formato)) $formato = 'Y-m-d H:i:s';
			return date($formato,$this->DataToTime($data,$relative));
		}
	
		
		
		/**
		* Retorna a diferença em segundos da primeira data para a segunda data
		* Aceita datas em vários formatos (string, timestamp, data pt_BR, data ISO)
		*
		* @param string Primeira data
		* @param string Segunda data
		*/
		public function DataDiff($data1,$data2){
			return $this->DataToTime($data2)-$this->DataToTime($data1);
		}
		
		
		
		/**
			Converte um timestamp absoluto para uma string 'user-friendly'
		*/
		public function timestamp2String($tm){ if(!is_numeric($tm)) return "N/D";
			$days = floor($tm/86400); $tms = $tm%86400;
			$hours = floor($tms/3600); $tms = $tms%3600;
			$minutes = floor($tms/60); $seconds = $tms%60;
			return ($days>0?$days."d ":'').($hours>0?$hours."h ":'').($minutes>0?$minutes."m ":'').$seconds."s";
		}

		/**
		 * Função relativeDate
		 * @param string $date Formato Date('d/m/Y H:i:s');
		 * @static
		 * @access public
		 * @return Dia da semana em (Segunda) dia relativo (Ontem)
		 */
		public function relativeDate($date){
	 
			$dias = array(
						'Sunday'	=>'Domingo',
						'Monday'	=>'Segunda',
						'Tuesday'	=>'Terça',
						'Wednesday'	=>'Quarta',
						'Thursday'	=>'Quinta',
						'Friday'	=>'Sexta',
						'Saturday'	=>'Sábado'
			);
			$meses = array(
						'January'	=>'Janeiro',
						'February'	=>'Fevereiro',
						'March'		=>'Março',
						'April'		=>'Abril',
						'May'		=>'Maio',
						'June'		=>'Junho',
						'July'		=>'Julho',
						'August'	=>'Agosto',
						'September'	=>'Setembro',
						'October'	=>'Outubro',
						'November'	=>'Novembro',
						'December'	=>'Dezembro'
			);
	 
			$timestamp_date = $this->DataToTime($date);
			$hoje 			= $this->DataToTime('hoje');
			$amanha 		=  $this->DataToTime('amanha');
			$anteontem 		=  $this->DataToTime('anteontem');
			$ontem 			=  $this->DataToTime('ontem');
			$depoisdeamanha =  strtotime('+2 day 00:00');
			$umasemana 		=  strtotime('+6 day');
	 
			switch(true){
				case($timestamp_date < $hoje && $timestamp_date > $anteontem):
					return "Ontem";
					break;
				case($timestamp_date >= $hoje && $timestamp_date < $amanha):
					return "Hoje";
					break;
				case($timestamp_date >= $amanha && $timestamp_date < $depoisdeamanha ):
					return "Amanhã";
					break;
				case( $timestamp_date > $depoisdeamanha && $timestamp_date < $umasemana ):
					return $dias[date('l',$timestamp_date)];
					break;
				case($timestamp_date > $umasemana  ):
					return Date('d',$timestamp_date).'-'.$meses[date('F',$timestamp_date)];
					break;
			}
		}
		
		public function nomeDoDia($data){
			$dias = array("Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado");
			return $dias[$this->DataFormatada("w", $data)];
		}
		public function nomeDoMes($data){
			$meses = array(
						'January'	=>'Jan',
						'February'	=>'Fev',
						'March'		=>'Mar',
						'April'		=>'Abr',
						'May'		=>'Maio',
						'June'		=>'Jun',
						'July'		=>'Jul',
						'August'	=>'Agos',
						'September'	=>'Set',
						'October'	=>'Out',
						'November'	=>'Nov',
						'December'	=>'Dez'
			);
			return $meses[$this->DataFormatada("F", $data)];
		}

	}

?>

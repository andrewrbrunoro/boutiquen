<?php

/**
 * SEO helper to create new, fine URLs that Google loves.
 *
 * @author Matti Putkonen,  matti.putkonen@fi3.fi
 * @copyright Copyright (c); 2006-2009, Matti Putkonen, Helsinki, Finland
 * @package BakeSale
 * @version $Id: seo.php 512 2007-10-05 07:12:41Z matti $
 */

    class SeoHelper extends Helper
    {
	public $helpers = array('Html');

/**
 * generate SEO link
 *
 * @param $data
 * @param $controller
 * @param $action
 */

	public function link($data, $controller = 'produtos', $action = 'show', $plugin = '') {
		$url = $this->url($data, $controller, $action, $plugin);
		$link = '<a href="' . $url . '"><span>' . $data['name'] . '</span></a>';		
		return $link;
	}

/**
 * generate SEO url
 *
 * @param $data
 * @param $controller
 * @param $action
 */

	public function url($data, $controller = 'categorias', $action = 'show', $plugin = '') {
		if($data['parent_id']==0){
			$tipo = '-depto-'.$data['id'];
		}else{
			$tipo = '-cat-'.$data['id'];
		}
		app::import('model','Categoria');
		$this->Categoria = new Categoria();
		$pai = $this->Categoria->getpath($data['id']);
		$slug = null;
		foreach($pai as $valor):
			$slug[] = low(Inflector::slug($valor['Categoria']['nome'],'-'));
		endForeach;		
		$slug = implode('-',$slug);
		$link = Router::url('/').$slug.$tipo  ;
		return $link;
	}
}
?>
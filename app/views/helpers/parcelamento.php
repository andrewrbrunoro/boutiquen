<?php

class ParcelamentoHelper extends AppHelper {

    //var $helpers = array('String');
    var $coef = array(4 => 1.02490, 5 => 1.03750, 6 => 1.05019, 7 => 1.06300, 8 => 1.07589, 9 => 1.08889, 10 => 1.10200, 11 => 1.11530, 12 => 1.12860);

    public function parcelaValida($valor, $parcelamento) {
        App::import('Helper', 'String');
        $this->String = new StringHelper();
        $retorno['valor'] = null;
        $retorno['parcela'] = null;
        $retorno['juros'] = null;
        if (is_string($valor)) {
            $valor = (str_replace(',', '.', $valor));
        }
        $juros_apartir = $parcelamento['juros_apartir'];
        $juros = false;

        for ($i = 1; $i <= (int) $parcelamento['parcelas']; $i++):
            $parcela = $valor / $i;
            if ($i > $juros_apartir && up($parcelamento['sigla']) == 'PAGSEGURO') {
                $parcela = $valor * $this->coef[$i] / $i;
                $juros = true;
            }
            if ($i == 1 || $parcela > $this->String->moedaToBco(Configure::read('Loja.valor_minimo_parcelamento'))):
                $retorno['valor'] = $parcela;
                $retorno['parcela'] = $i;
                $retorno['juros'] = $juros;
            endIf;
        endFor;
		
        return $retorno;
    }

    public function descontoAVista($valor, $parcelamento) {
		if (is_string($valor)) {
			$valor = (str_replace(',', '.', $valor));
		}
		
		$retorno['valor'] = $valor*((100-$parcelamento['desconto_de'])/100);
		$retorno['parcela'] = 1;
		$retorno['juros'] = false;
		
		return $retorno;
    }

    public function listaParcelamentos($valor, $parcelamento) {
        App::import('Helper', 'String');
        $this->String = new StringHelper();
        $content = '';
        $retorno['valor'] = null;
        $retorno['parcela'] = null;
        $retorno['juros'] = null;
        if (is_string($valor)) {
            $valor = (str_replace(',', '.', $valor));
        }
        $juros_apartir = $parcelamento['juros_apartir'];
        $juros = false;

		
        $div1 = '';
        $div2 = '';
		
        //abro a div de divisao de coluna
        $content .= '<ul>';
                        //'<span class="blue_color left"><strong>Número de Parcelas</strong></span> <strong>Valor parcela</strong>';

        for ($i = 1; $i <= (int) $parcelamento['parcelas']; $i++):
            $parcela = $valor / $i;
            if ($i > $juros_apartir && up($parcelamento['sigla']) == 'PAGSEGURO') {
                $parcela = $valor * $this->coef[$i] / $i;
                $juros = true;
            }
            if ($i == 1 || $parcela > $this->String->moedaToBco(Configure::read('Loja.valor_minimo_parcelamento'))) {
                $retorno['valor'] = $parcela;
                $retorno['parcela'] = $i;
                $retorno['juros'] = $juros;

                $content .= '<p><span>' . $retorno['parcela'] . 'x '. ($retorno["juros"] == true ? "com juros" : "sem juros") .'</span> R$ ' . $this->String->bcoToMoeda($retorno['valor']) . '</p>';
				
				$div1 .= '<li>' . $retorno['parcela'] . 'x '. ($retorno["juros"] == true ? "com juros" : "sem juros") .'</li>';
				$div2 .= '<li><strong>R$ ' . $this->String->bcoToMoeda($retorno['valor']) . '</strong></li>';
				

                //se o index for igual a metade de valor da parcela, corto a lista ao meio com uma coluna
                //if ($i % (int) ((int) $parcelamento['parcelas'] / 2) == 0 && $i != $parcelamento['parcelas']) {
                    //$content .= '</ul><ul>';
                                //'<span class="row"><span class="blue_color left"><strong>Número de Parcelas</strong></span> <strong>Valor parcela</strong></span>';
                //}
            }

        endFor;

		$content2 = '<div class="number-plots"><h4>NÚMERO DE PARCELAS</h4><ul>'.$div1.'</ul></div><div class="number-plots particle-plots"><h4>VALOR PARCELA</h4><ul>'.$div2.'</ul></div>';
		
		
        //fecho a div de divisao de coluna
        //$content .= '</div>';

        //return $content;
        return $content2;
    }

}

?>
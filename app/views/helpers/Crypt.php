<?php

    if ( !function_exists( 'hex2bin' ) ) {
        function hex2bin( $str ) {
            $sbin = "";
            $len = strlen( $str );
            for ( $i = 0; $i < $len; $i += 2 ) {
                $sbin .= pack( "H*", substr( $str, $i, 2 ) );
            }

            return $sbin;
        }
    }

    /**
     * Class CryptHelper
     */
    class CryptHelper extends AppHelper
    {

        /**
         * @var string
         */
        private $pass = 'UbADprU7';

        /**
         * @param $jsonString
         * @return mixed|null
         *
         * Decrypt
         */
        public function decJson($jsonString)
        {
            $jsondata = json_decode($jsonString, true);
            try {
                $salt = hex2bin($jsondata["s"]);
                $iv = hex2bin($jsondata["iv"]);
            } catch (Exception $e) {
                return null;
            }
            $ct = base64_decode($jsondata["ct"]);
            $concatedPassphrase = $this->pass . $salt;
            $md5 = array();
            $md5[0] = md5($concatedPassphrase, true);
            $result = $md5[0];
            for ($i = 1; $i < 3; $i++) {
                $md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
                $result .= $md5[$i];
            }
            $key = substr($result, 0, 32);
            $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
            return json_decode($data, true);
        }

        /**
         * @param $value
         * @return string
         */
        public function encJson($value)
        {
            $salt = openssl_random_pseudo_bytes(8);
            $salted = '';
            $dx = '';
            while (strlen($salted) < 48) {
                $dx = md5($dx . $this->pass . $salt, true);
                $salted .= $dx;
            }
            $key = substr($salted, 0, 32);
            $iv = substr($salted, 32, 16);
            $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
            $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
            return json_encode($data);
        }

    }
<?php

class ImageHelper extends Helper {

    var $helpers = array('Html');
    var $cacheDir = 'imagecache'; // relative to 'img'.DS
   
    /**
     * Automatically resizes an image and returns formatted IMG tag
     *
     * @param string $path Path to the image file, relative to the webroot/img/ directory.
     * @param integer $width Image of returned image
     * @param integer $height Height of returned image
     * @param boolean $aspect Maintain aspect ratio (default: true)
     * @param array    $htmlAttributes Array of HTML attributes.
     * @param boolean $return Wheter this method should return a value or output it. This overrides AUTO_OUTPUT.
     * @return mixed    Either string or echos the value, depends on AUTO_OUTPUT and $return.
     * @access public
     */

    function resize($path, $width, $height, $aspect = true, $htmlAttributes = array(), $src = false) {
        if(!isset($this->Html)){
            App::import("helper", "Html");
            $this->Html = new HtmlHelper();
        }
        try {
            $types = array(1 => "gif", "jpeg", "png", "swf", "psd", "wbmp"); // used to determine image type
            //if (empty($htmlAttributes['alt']))
                //$htmlAttributes['alt'] = 'thumb';  // Ponemos alt default

                $fullpath = ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS;
            $path = str_replace("\\", "/", $path);
            $url = $fullpath . $path;

            if (!($size = @getimagesize($url))) {
                $path = 'img' . DS . 'site' . DS . 'sem_imagem.jpg';
                $url = $fullpath . $path;
                $size = @getimagesize($url);
            }
            if ($aspect) { // adjust to aspect.
                if (($size[1] / $height) > ($size[0] / $width))  // $size[0]:width, [1]:height, [2]:type
                    $width = ceil(($size[0] / $size[1]) * $height);
                else
                    $height = ceil($width / ($size[0] / $size[1]));
            }

            $relfile = $this->webroot . $this->cacheDir . '/' . $width . 'x' . $height . '_' . basename($path); // relative file
            $cachefile = $fullpath . $this->cacheDir . DS . $width . 'x' . $height . '_' . basename($path);  // location on server
            $cached = true;
			
            if (file_exists($cachefile)) {
                //$csize = getimagesize($cachefile);
				// check if up to date
				//var_dump(filemtime($cachefile),@filemtime($url));die;
                //if (@filemtime($cachefile) < @filemtime($url)){
					 if ($src) {
                        return $this->cacheDir . '/'. $width . 'x' . $height . '_' . basename($path);
                    } else {
                        return $this->output(sprintf($this->Html->tags['image'],$this->Html->Url('/',true) . $this->cacheDir . '/'. $width . 'x' . $height . '_' . basename($path), $this->Html->_parseAttributes($htmlAttributes, null, '', ' ')), false);
                    
                    }

                //}
				$cached = false;
            } else {
                $cached = false;
            }

            if (!$cached) {
                $resize = ($size[0] > $width || $size[1] > $height) || ($size[0] < $width || $size[1] < $height);
            } else {
                $resize = true;
            }

            if ($resize) {
                
				$image = call_user_func('imagecreatefrom' . $types[$size[2]], $url);
					
						
                if (function_exists("imagecreatetruecolor") && ($temp = imagecreatetruecolor($width, $height))) {
					imagealphablending($temp, false);
					imagesavealpha  ( $temp  , true );
                    imagecopyresampled($temp, $image, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                } else {
                 
					$temp  = imagecreate( $width, $height );
					imagealphablending( $temp, false );
					$transparent = imagecolorallocatealpha( $temp, 0, 0, 0, 127 );
					imagefill( $temp, 0, 0, $transparent );
					imagesavealpha( $temp,true );
					imagealphablending( $temp, true );
                    imagecopyresized($temp, $image, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                }
       
                call_user_func("image" . $types[$size[2]], $temp, $cachefile);
                imagedestroy($image);
                imagedestroy($temp);
            } else {
                copy($url, $cachefile);
            }
            if ($src) {
                return $relfile;
            } else {
                return $this->output(sprintf($this->Html->tags['image'], $relfile, $this->Html->_parseAttributes($htmlAttributes, null, '', ' ')), false);
            }
        } catch (Exception $exc) {
            //echo $exc->getTraceAsString();
        }
    }

}

?>
<div class="faqs index">
    <h2><?php __('CupomCampanha'); ?></h2>
    <div class="btAddProduto">
		<?php echo $this->Html->link(__('Adicionar Campanha', true), array('action' => 'add')); ?>
    </div>       
	
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('Status', 'status'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;

        foreach ($cupom_campanha as $grupo):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $grupo['CupomCampanha']['id']; ?>&nbsp;</td>
                <td><?php echo $grupo['CupomCampanha']['nome']; ?>&nbsp;</td>
                <td><?php echo $grupo['CupomCampanha']['status']==1?'Ativo':'Inativo'; ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Visualizar cadastros da Campanha', true), array('controller'=>'cupons','action' => 'view', $grupo['CupomCampanha']['id'])); ?>
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $grupo['CupomCampanha']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $grupo['CupomCampanha']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $grupo['CupomCampanha']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <p>
    <?php
            echo $this->Paginator->counter(array(
                'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
            ));
    ?>
    </p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>
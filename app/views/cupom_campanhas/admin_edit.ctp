<div class="faqs form">
    <?php echo $this->Form->create('CupomCampanha',array('controller'=>'cupom_campanha','action'=>'edit')); ?>
    <fieldset>
        <legend><?php __('Editar Campanha'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('nome');
        echo $this->Form->input('status', array('default'=>true,'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Enviar', true)); ?>
</div>
<div class="actions">
    <h3>Ações</h3>
    <ul>
        <li><?php echo $this->Html->link(__('Listar Campanhas', true), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('Adicionar Campanha', true), array('action' => 'add')); ?> </li>
    </ul>
</div>
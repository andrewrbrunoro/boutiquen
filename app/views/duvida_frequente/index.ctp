<?php echo $javascript->link('site/faq/index.js',false);?>
<!-- start leftcol -->
<div class="leftcol gap" id="leftcol">
    <?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->
<!-- start rightcol -->
<div class="column-right">
    <!-- start contact -->
    <div class="contact">
        <h2 class="heading">Dúvidas Frequentes</h2>
        
     <br><br>   
<div id="faq_accordion" class="accordion">
    
        <?php
        //debug($duvidas_frequentes);die;
        foreach ($duvidas_frequentes as $duvidas_frequente):

            ?>
            
                <a><h3><span></span><?php echo $duvidas_frequente['DuvidaFrequente']['pergunta']; ?></h3></a>
                <div class="accordion-content">
                    <p><?php echo $duvidas_frequente['DuvidaFrequente']['resposta']; ?></p>
                </div>
                </hr>
        <?php endforeach; ?>
    
</div>
        
        <!-- end col2 -->
        <div class="clear"></div>
    </div>
    <!-- end contact -->
</div>
<!-- end rightcol -->

<div class="clear"></div>
<?php
	$q_colunas = 1;
	if(count($news_imagens)>1){
		if($dadosnews['Geranew']['template']==1){
			$q_colunas =2;
		}else if($dadosnews['Geranew']['template']==2){
			$q_colunas =2;
		}else if($dadosnews['Geranew']['template']==3){
			$q_colunas =3;
		}
	}
	
?>
<textarea style="width: 840px;height: 510px;" disabled="disabled">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<?php echo $this->Html->charset(); ?>
        <title><?php echo Configure::read('Loja.titulo'); ?></title>
    </head>
    <body style="color:#848484;">
    <!-- Container Table -->
    <table cellpadding="0" cellspacing="0" border="0" width="100%" >
        <tr>
            <td align="center">
     
    <!-- Email Wrapper Table -->
    <table cellpadding="0" cellspacing="10" border="0" width="700" align="center">
        <tr>
            <td colspan="<?php echo $q_colunas; ?>">
				<div style="font-size: 11px;color: #999;line-height: 25px;float:right;">Central de Atendimento <span style="color: #c5ab41;"><?php echo Configure::read('Loja.televendas'); ?></span> Faça seu <a href="<?php echo $this->Html->Url("/login",true) ?>" style="color: #c5ab41;">cadastro</a> no site, ou <a href="<?php echo $this->Html->Url("/login",true) ?>" style="color: #c5ab41;">identifique-se aqui</a>.</div>
				
				<div style="clear:both;"></div>
				
				<a href="<?php echo $this->Html->Url('/',true); ?>" title="<?php echo Configure::read('Loja.nome'); ?>" style="float:left;">
					<img src="<?php echo $this->Html->Url('/img/site/',true); ?>logo.png" alt="<?php echo Configure::read('Loja.nome'); ?>" />
				</a>
				
				<div style="float:right;margin-top:12px;">
					SEDEX Grátis<br/>
					Envio e Retorno
				</div>
				
				<div style="clear:both;"></div>
				
				<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 12px 0;background: #f7f7f7;margin: 20px 0 0;font-family: arial;" >
					<tr>
						<?php foreach($lista_menu_newsletter AS $itemmenu){ ?>
							<td align="center" style="border-right: 1px solid #eaeaea;">
								<?php
									if($itemmenu['Link']['tipo']=='CATEGORIA' || $itemmenu['Link']['tipo']=='PAGINA' ){
										$linkfinal = $this->Html->Url('/',true);
										$linkfinal .= $itemmenu['Link']['link'];
									}else{
										$linkfinal = $itemmenu['Link']['link'];
									}
									
								?>
								<a href="<?php echo $linkfinal; ?>" target="_blank" style="text-decoration: none;color:#848484;">
									<?php echo $itemmenu['Link']['title']; ?>
								</a>
							</td>
						<?php } ?>
					</tr>
				</table>
				
			</td>
        </tr>
		
		
		
		
		<?php if($dadosnews['Geranew']['template']==1){ ?>
			
			<?php
				//$linhamedia=(int)ceil((((count($news_imagens)-1)/$q_colunas)+1)/2);
				$linhamedia=3;
				$contalinha=0;
			?>
			<?php foreach($news_imagens AS $k => $imagem){ ?>
				<?php
					if(($k%$q_colunas)==1 && $contalinha==$linhamedia && !empty($dadosnews['Geranew']['mensagem'])){
						echo '<tr><td style="padding:10px;background:red;color:white;font-weight:bold;" colspan="'.($q_colunas).'">'.$dadosnews['Geranew']['mensagem'].'</td></tr>';
					}
				?>
				
				<?php if($k==0 || ($k%$q_colunas)==1){echo '<tr>';$contalinha++;} ?>
				<td align="center" <?php if($k==0){echo 'colspan="'.($q_colunas).'" ';} ?>>
					<?php if(!empty($imagem['GeranewsImagem']['link'])){echo '<a href="'.$imagem['GeranewsImagem']['link'].'" target="_blank">';} ?>
					<img src="<?php echo ($this->Html->Url('/',true)).$imagem['GeranewsImagem']['dir'].'/'.$imagem['GeranewsImagem']['filename']; ?>"/>
					<?php if(!empty($imagem['GeranewsImagem']['link'])){echo '</a>';} ?>
				</td>
				<?php if($k==0 || ($k%$q_colunas)==0 || $k==(count($news_imagens)-1)){echo '</tr>';} ?>
				
			<?php } ?>
			
		<?php }else if($dadosnews['Geranew']['template']==2){ ?>
			<tr>
				<td align="center" colspan="1" rowspan='3'>
					<?php if(!empty($news_imagens[0]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[0]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[0]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[0]['GeranewsImagem']['dir'].'/'.$news_imagens[0]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[0]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
				<td align="center" colspan="1">
					<?php if(!empty($news_imagens[1]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[1]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[1]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[1]['GeranewsImagem']['dir'].'/'.$news_imagens[1]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[1]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="1">
					<?php if(!empty($news_imagens[2]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[2]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[2]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[2]['GeranewsImagem']['dir'].'/'.$news_imagens[2]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[2]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="1">
					<?php if(!empty($news_imagens[3]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[3]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[3]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[3]['GeranewsImagem']['dir'].'/'.$news_imagens[3]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[3]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
			</tr>
			<?php if(!empty($dadosnews['Geranew']['mensagem'])){ ?>
				<tr>
					<td style="padding:10px;background:red;color:white;font-weight:bold;" colspan="2">
						<?php echo $dadosnews['Geranew']['mensagem']; ?>
					</td>
				</tr>
			<?php } ?>
			<tr>
				<td align="center" colspan="2">
					<?php if(!empty($news_imagens[4]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[4]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[4]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[4]['GeranewsImagem']['dir'].'/'.$news_imagens[4]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[4]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="1">
					<?php if(!empty($news_imagens[5]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[5]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[5]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[5]['GeranewsImagem']['dir'].'/'.$news_imagens[5]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[5]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
				<td align="center" colspan="1">
					<?php if(!empty($news_imagens[6]['GeranewsImagem']['filename'])){ ?>
						<?php if(!empty($news_imagens[6]['GeranewsImagem']['link'])){echo '<a href="'.$news_imagens[6]['GeranewsImagem']['link'].'" target="_blank">';} ?>
						<img src="<?php echo ($this->Html->Url('/',true)).$news_imagens[6]['GeranewsImagem']['dir'].'/'.$news_imagens[6]['GeranewsImagem']['filename']; ?>"/>
						<?php if(!empty($news_imagens[6]['GeranewsImagem']['link'])){echo '</a>';} ?>
					<?php } ?>
				</td>
			</tr>
		<?php }else if($dadosnews['Geranew']['template']==3){ ?>
			
			<?php
				//$linhamedia=(int)ceil((((count($news_imagens)-1)/$q_colunas)+1)/2);
				$linhamedia=3;
				$contalinha=0;
			?>
			<?php foreach($news_imagens AS $k => $imagem){ ?>
				<?php
					if(($k%$q_colunas)==1 && $contalinha==$linhamedia && !empty($dadosnews['Geranew']['mensagem'])){
						echo '<tr><td style="padding:10px;background:red;color:white;font-weight:bold;" colspan="'.($q_colunas).'">'.$dadosnews['Geranew']['mensagem'].'</td></tr>';
					}
				?>
				
				<?php if($k==0 || ($k%$q_colunas)==1){echo '<tr>';$contalinha++;} ?>
				<td align="center" <?php if($k==0){echo 'colspan="'.($q_colunas).'" ';} ?>>
					<?php if(!empty($imagem['GeranewsImagem']['link'])){echo '<a href="'.$imagem['GeranewsImagem']['link'].'" target="_blank">';} ?>
					<img src="<?php echo ($this->Html->Url('/',true)).$imagem['GeranewsImagem']['dir'].'/'.$imagem['GeranewsImagem']['filename']; ?>"/>
					<?php if(!empty($imagem['GeranewsImagem']['link'])){echo '</a>';} ?>
				</td>
				<?php if($k==0 || ($k%$q_colunas)==0 || $k==(count($news_imagens)-1)){echo '</tr>';} ?>
				
			<?php } ?>
			
		<?php } ?>
		
		
        <tr>
            <td colspan="<?php echo $q_colunas; ?>" >
				
				<table cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 12px;background: #f7f7f7;margin: 20px 0 0;" >
					<tr>
						<td style="width:150px;border-bottom: 1px solid #eaeaea;padding:0 0 10px;margin-right:10px;">
							INSTITUCIONAL
						</td>
						<td style="width:30px;"></td>
						<td style="border-bottom: 1px solid #eaeaea;padding:0 0 10px;">
							PAGAMENTO
						</td>
					</tr>
					<tr>
						<td style="padding:10px 0;font-size:11px;" rowspan="3" valign="top">
							Blog Coliseu<br/>
							Conceito Coliseu<br/>
							Catálogos<br/>
							Lojas<br/>
							Dúvidas Frequentes<br/>
							Contato<br/>
							Making Of<br/>
							Marcas
						</td>
						<td style=""></td>
						<td style="padding:10px 0;">
							<img src="<?php echo $this->Html->Url('/img/site/img_visa.png',true); ?>" alt="visa1"/>​ 
							<img src="<?php echo $this->Html->Url('/img/site/img_mastercard.png',true); ?>" alt="mastercard1"/>
							<img src="<?php echo $this->Html->Url('/img/site/img_boleto.png',true); ?>" alt="boleto1"/>​
							<img src="<?php echo $this->Html->Url('/img/site/img_payment1.png',true); ?>" alt="bradesco1"/>
						</td>
					</tr>
					<tr>
						<td style=""></td>
						<td style="border-bottom: 1px solid #eaeaea;padding:0 0 10px;">
							REDES SOCIAIS
						</td>
					</tr>
					<tr>
						<td style=""></td>
						<td style="padding:10px 0;">
							<a href="<?php echo Configure::read('Loja.facebook'); ?>"><img src="<?php echo $this->Html->Url('/img/site/sprite_social1.jpg',true); ?>" target="_blank" alt="facebook"/></a>
							<a href="<?php echo Configure::read('Loja.twitter'); ?>"><img src="<?php echo $this->Html->Url('/img/site/sprite_social2.jpg',true); ?>" target="_blank" alt="twitter"/></a>
						</td>
					</tr>
				</table>
				
				
			</td>
        </tr>
		
    </table>
    <!-- End Email Wrapper Table -->
     
            </td>
        </tr>
    </table>
    <!-- End Container Table -->
    </body>
</html>
</textarea>
<?php
echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom',false);
echo $this->Html->css('common/nyro_modal/nyroModal.css');
echo $javascript->link('admin/geranews/index',false);
?>
<div class="index ">
    <h2><?php __('Gera News'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Newsletter', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($geranews as $pagina):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $pagina['Geranew']['id']; ?>&nbsp;</td>
                <td><?php echo $pagina['Geranew']['nome']; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $pagina['Geranew']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $pagina['Geranew']['modified']); ?>&nbsp;</td>

                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $pagina['Geranew']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $pagina['Geranew']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $pagina['Geranew']['id'])); ?>
                    <?php echo $this->Html->link(__('Visualizar', true), array('action' => 'ver', $pagina['Geranew']['id']), array('target' => '_blank', 'escape' => false)); ?>
					
					<span>
						<?php echo $this->Html->link(__('Código', true), array('action' => 'gera', $pagina['Geranew']['id']), array('class' => 'gerar_codigo', 'escape' => false)); ?>
					</span>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>

	
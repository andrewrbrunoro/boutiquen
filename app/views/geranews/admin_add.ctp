<?php
//echo $javascript->link('common/tiny_mce/tiny_mce_src.js', false);
//echo $javascript->link('admin/paginas/crud.js', false);
?>
<div class="index">
    <?php echo $this->Form->create('Geranew'); ?>
    <fieldset>
        <legend><?php printf(__('Adidionar %s', true), __('Geranew', true)); ?></legend>
		
		<div style="position: absolute;right: 45px;">
			<img src="<?php echo $this->Html->Url('/img/site/'); ?>template1.png" width="200" />
			<img src="<?php echo $this->Html->Url('/img/site/'); ?>template2.png" width="200" />
			<img src="<?php echo $this->Html->Url('/img/site/'); ?>template3.png" width="200" />
		</div>
        
        <?php
		
		$templates=array(
				1=>'template 1',
				2=>'template 2',
				3=>'template 3'
			);
		
        echo $this->Form->input('nome', array('class' => 'w312'));
        echo $this->Form->input('template', array('class' => 'w312', 'options' => $templates));
        echo $this->Form->input('mensagem', array('class' => 'w312'));
        echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
        echo $this->Form->end(__('Inserir', true));
        ?>
    </fieldset>
</div>
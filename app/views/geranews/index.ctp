<?php
echo $javascript->link('common/jquery.meio.mask.js', false);
echo $javascript->link('site/paginas/index.js', false);

if ($pagina['Pagina']['dinamico'] == 1):
    $pagina_element = $this->element('paginas/' . $pagina['Pagina']['element'] . '', array('pagina_element_content' => $pagina_element_content));
    $pagina_content = $pagina['Pagina']['texto'];
    $pagina_content = str_replace('{VAR_CONTENT}', $pagina_element, $pagina_content);
else:
    $pagina_content = $pagina['Pagina']['texto'];
endIf;
?>

<!-- start leftcol -->
<div class="leftcol">
    <?php echo $this->element('site/left'); ?>
</div>
<!-- end leftcol -->

<!-- start colright -->
<div class="rightcol">

	<?php echo $pagina_content; ?>
	
</div>
<!-- end colright -->

<div class="clear"></div>
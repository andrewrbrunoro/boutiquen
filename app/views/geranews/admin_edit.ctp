<?php
//echo $javascript->link('common/tiny_mce/tiny_mce_src.js', false);
//echo $javascript->link('admin/paginas/crud.js', false);

echo $javascript->link('common/swfobject.js', false);
echo $javascript->link('common/jquery.uploadify.v2.1.4.min' ,false);
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('common/tiny_mce/tiny_mce.js',false);
echo $javascript->link('common/jquery.cookie.js',false);
echo $javascript->link('common/jquery-ui-1.8.16.custom.min',false);
echo $this->Html->css('common/ui-lightness/jquery-ui-1.8.16.custom.css');
echo $javascript->link('admin/geranews/crud.js',false);
?>
<div class="index">
    <?php echo $this->Form->create('Geranew'); ?>
    <fieldset>
        <legend><?php printf(__('Editar %s', true), __('Geranew', true)); ?></legend>
		
		<div style="position: absolute;right: 505px;text-align:center;">
			<img src="<?php echo $this->Html->Url('/img/site/'); ?>template1.png" width="200" /><br/>
			Template 1
		</div>
		<div style="position: absolute;right: 275px;text-align:center;">
			<img src="<?php echo $this->Html->Url('/img/site/'); ?>template2.png" width="200" /><br/>
			Template 2
		</div>
		<div style="position: absolute;right: 45px;text-align:center;">
			<img src="<?php echo $this->Html->Url('/img/site/'); ?>template3.png" width="200" /><br/>
			Template 3
		</div>
        
		<?php
		
		$templates=array(
				1=>'template 1',
				2=>'template 2',
				3=>'template 3'
			);
		
        echo $this->Form->input('nome', array('class' => 'w312'));
        echo $this->Form->input('template', array('class' => 'w312', 'options' => $templates));
        echo $this->Form->input('mensagem', array('class' => 'w312'));
		echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo')));
		?>
		<?php echo $this->Form->input('id'); ?>
				<legend>Adicionar Imagens</legend>
				<input id="edit-produto" name="file_upload" type="file" />
				
				<div class="container-edit">
        		<?php
        			if(isset($imagens_news)){
        				echo $imagens_news;
        			}
        		 ?>	
        		 </div>
				 
				 <div style="clear:both;"></div>
		<?php echo $this->Form->end(__('Salvar', true)); ?>
    </fieldset>
	
</div>

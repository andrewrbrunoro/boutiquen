<?php echo $this->Html->script('site/busca/index', array('inline' => false)); ?>
<?php echo  $this->Html->script('site/produtos/variacao', array('inline' => false)); ?>

<div class="sub-menu">
    <?php echo $this->element('site/left'); ?>
</div>

<div class="products">
	<!-- start entry -->
	<div class="entry">

		<div class="product-list">
			<?php if($produtos && is_array($produtos) && count($produtos) > 0): ?>
			<ul>
				<?php 
					$cont = 1;
					foreach($produtos as $produto): ?>
					<li>
						<?php echo $this->element('site/produto', array("produto" => $produto)); ?>
					</li>
					<?php if(($cont%3) == 0): ?>
						</ul><div class="clear"></div><ul>
					<?php endIf; ?>
				<?php 
					$cont++;
					endForeach; ?>
			</ul>
			<?php else: ?>
                <p style="width: 100%; text-align: center; padding: 50px">Nenhum produto encontrado</p>
            <?php endIf; ?>
			<div class="clear"></div>
		</div>
		<!-- end product-list -->
		<!-- start pagination -->
		<div class="pagination1">
			<?php 
				$pageCount = 1;
				if(isset($this->Paginator->params['paging']['ProdutoCategoria']['pageCount'])){
					$pageCount = $this->Paginator->params['paging']['ProdutoCategoria']['pageCount'];
				}else if(isset($this->Paginator->params['paging']['Produto']['pageCount'])){
					$pageCount = $this->Paginator->params['paging']['Produto']['pageCount'];
				}
				if($pageCount > 1){
					?>
					<ul>
						<li class="prev"><?php echo $this->Paginator->prev(__('<<', true), array(), null, array('class' => 'prev')); ?></li>
						<?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => null)); ?>
						<li class="next"><?php echo $this->Paginator->next(__('>>', true), array(), null, array('class' => 'next')); ?></li>
					</ul>
					<?php
				}
			?>
			<div class="clear"></div>
			<p><?php echo $this->Paginator->counter(array('format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true))); ?></p>
		</div>
		<div class="clear"></div>
		
	</div>
	<!-- end entry -->
</div>


<div class="clear"></div>
<div class="index">
<?php echo $this->Form->create('Busca');?>
	<fieldset>
		<legend><?php __('Adicionar Busca'); ?></legend>
	<?php
		echo $this->Form->input('palavra_chave');
		echo $this->Form->input('palavra_relacionada');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Enviar', true));?>
</div>

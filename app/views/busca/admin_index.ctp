<div class="index">
    <h2 class="left"><?php __('Buscas'); ?></h2>
    <?php echo $form->create('Busca', array('url'=>'/admin/busca/index','class'=>'formBusca'));?>
		<fieldset>
			<div class="w312 left">
				<?php
					echo $form->input('Filter.palavra_chave',array('type'=>'text','label'=>'Palavra Chave:','class'=>'w312'));
					echo $form->input('Filter.palavra_relacionada',array('type'=>'text','label'=>'Palavra Relacionada:','class'=>'w312'));
				?>
				<div class="submit" style="clear: both;">
					<input name="submit" type="submit" class="button1" value="Busca" />
				</div>
				<div class="submit">
					<a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
				</div>
			</div>
			<div style="width: 650px; float: right;">
				<div id="chart_div" style="width:350; height:250"></div>
				<div class="totais" style="width:350px; height:100px;text-align:right;"></div>
			</div>
		</fieldset>
	<?php echo $form->end(); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('palavra_chave'); ?></th>
            <th><?php echo $this->Paginator->sort('quantidade_itens'); ?></th>
            <th><?php echo $this->Paginator->sort('quantidade_busca'); ?></th>
            <th><?php echo $this->Paginator->sort('palavra_relacionada'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($buscas as $busca):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $busca['Busca']['id']; ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo $busca['Busca']['palavra_chave']; ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo $busca['Busca']['quantidade_itens']; ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo isset($busca['Busca']['quantidade_busca']) ? $busca['Busca']['quantidade_busca'] : "---"; ?>&nbsp;</td>
                <td style="text-align: center;"><?php echo isset($busca['Busca']['palavra_relacionada']) ? $busca['Busca']['palavra_relacionada'] : "---"; ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$busca['Busca']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$busca['Busca']['modified']); ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $busca['Busca']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $busca['Busca']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $busca['Busca']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
      
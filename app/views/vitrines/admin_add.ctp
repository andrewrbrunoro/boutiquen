<?php
echo $this->Html->css('admin/vitrines/index.css', null, array('inline' => false));
echo $javascript->link('common/jquery-ui-1.8.16.custom.min', false);
echo $javascript->link('admin/vitrines/crud.js', false);
echo $javascript->link('common/asmselect/jquery.asmselect', false);
echo $this->Html->css('common/asmselect/jquery.asmselect.css');
?>
<div class="index">
    <?php echo $this->Form->create('Vitrine'); ?>
    <fieldset>
        <legend><?php __('Adicionar Vitrine'); ?></legend>
        <?php echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
		<?php echo $this->Form->input('capa', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Sim', false => 'Não'))); ?>
		<?php echo $this->Form->input('nome', array('class' => 'w312')); ?>
        <?php echo $this->Form->input('ordem', array('class' => 'w312')); ?>
        <?php echo $this->Form->input('tipo', array('options' => array('NORMAL' => 'Normal', 'DESTAQUE' => 'Destaque', 'COLECOES' => 'Coleções'))); ?>
        
        <div id="vitrines-produtos-campos">
            <?php echo $this->Form->input('categoria_id', array('options' => $categorias, 'label' => 'Categoria')); ?>
            <?php echo $this->Form->input('Buscar', array('class' => 'w312', 'after' => $this->Form->Button('OK', array('id' => 'buscar-produtos')))); ?>
            <?php $botoes = $this->Html->link('Adicionar', 'javascript:;', array('id' => 'add'));
                $botoes .= $this->Html->link('Remover', 'javascript:;', array('id' => 'rm'));
                echo $this->Form->input('Selecionar', array('type' => 'select', 'multiple' => true, 'after' => $botoes)); ?>
            <?php echo $this->Form->input('Produto', array('class' => 'categoria_produtos_sel')); ?>
            <?php echo $this->Form->input('quantidade_exibida', array('class' => 'w312')); ?>
        </div>
        
        <?php echo $this->Form->end(__('Inserir', true)); ?>
    </fieldset>
</div>  

<div class="index">
<?php echo $this->Form->create('Fabricante',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php __('Editar Fabricante'); ?></legend>
	<?php
		echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
		echo $this->Form->input('id');
		echo $this->Form->input('nome',array('class'=>'w312'));
	?>
	</fieldset>
        <fieldset>
            <legend>Imagem</legend>
            <?php if(isset($this->data['FabricanteImagem'][0]['dir'])){?>
                <div class="img">
                    <?php
                    echo $image->resize($this->data['FabricanteImagem'][0]['dir'].DS.$this->data['FabricanteImagem'][0]['filename'], 100, 100,false);
                    
                    echo $this->Form->input('FabricanteImagem.id', array('type' => 'hidden','value'=>$this->data['FabricanteImagem'][0]['id']));
                    ?>
                </div>
            <?php } ?>
            <?php
                echo $this->Form->input('FabricanteImagem.filename', array('type' => 'file'));
                echo $this->Form->input('FabricanteImagem.fabricante_id', array('type' => 'hidden','value'=>$this->Form->value('Fabricante.id')));
                echo $this->Form->input('FabricanteImagem.dir', array('type' => 'hidden'));
                echo $this->Form->input('FabricanteImagem.mimetype', array('type' => 'hidden'));
                echo $this->Form->input('FabricanteImagem.filesize', array('type' => 'hidden'));
                echo $this->Form->end(__('Salvar', true));
            ?>
        </fieldset>
</div>

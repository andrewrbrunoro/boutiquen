<div class="container">
    <header class="page-header">
        <h1 class="page-title"><i class="fa fa-question"></i> Dúvidas Frequentes</h1>
        <span class="text-info">
            Verifique abaixo e tire suas dúvidas. Caso ainda tenha alguma dúvida, entre em contato conosco pelo telefone (51) 3228-5036 ou pelo e-mail contato@boutiqueno.com.br
        </span>
    </header>
    <div class="row">
        <ul>
            <li>
                <h4>COMO COMPRAR</h4>
                <p>
                    Para comprar na Boutique Nô é fácil, seguro e rápido. Além da praticidade de comprar em qualquer lugar, você ainda recebe sua compra sem precisar sair de casa. Tudo funciona como uma loja física: você pode visualizar a vitrine, lançamentos e promoções e solicitar a compra pelo site. <br />
                    Para efetivar sua compra é simples: basta colocar cada produto no carrinho e, para concluir, clique em comprar. Se ainda não for cadastrado no site, basta preencher os dados solicitados e finalizar a compra. <br />
                    Caso tenha alguma dúvida, ligue para (51) 3228-5036, que um de nossos colaboradores irá lhe atender ou entre em contato com nossa Central de Relacionamento (de segunda a sexta, das 09:00 às 18:00).
                </p>
            </li>
            <li>
                <h4>PRAZO DE ENTREGA</h4>
                <p>
                    O prazo varia de acordo com o local e o método de entrega escolhido no ato da compra, lembrando que o prazo será contado após a data de liberação do seu pedido. Após preencher o seu endereço e definir o método de entrega, será calculado o prazo de entrega do seu pedido. <br />
                    Você pode acompanhar o Status do seu pedido através do seu cadastro no site, no item “Meus Pedidos” e também receberá um e-mail confirmando sua compra e prazo de entrega. Alguns meios de logística oferecem a opção de rastreamento, o qual será informado após a liberação do pedido (quando disponível).
                </p>
            </li>
            <li>
                <h4>AUSÊNCIA NA ENTREGA</h4>
                <p>
                    As nossas entregas são realizadas de segunda a sexta em horário comercial (09:00 às 18:00). Após três tentativas de entrega sem êxito, o pedido fica por um período de cinco dias úteis na agência de sua cidade aguardando retirada. Para retirada junto à agência, é indispensável RG e código de rastreamento. Se após esse período você não coletar o seu produto, ele retornará ao Centro de Distribuição Boutique Nô e você receberá uma notificação. <br />
                    Temos a possibilidade de realizar a entrega para terceiros, desde que aceitem o recebimento e assinem o comprovante de entrega, lembrando que para isso o cliente deve informar a Boutique Nô no ato da compra ou através de e-mail ou telefone, garantindo assim a segurança do seu pedido. Em caso do cliente residir em um apartamento, deixar a autorização de entrega ao zelador do prédio, quando necessário.
                </p>
            </li>
            <li>
                <h4>MEIOS DE PAGAMENTO</h4>
                <p>
                    A forma de pagamento que permite o pagamento parcelado é o cartão de crédito. Aceitamos compras em até seis vezes sem juros. <br />
                    Optando pelo pagamento por cartão de crédito, entraremos em contato com sua administradora para obter autorização. Em seguida, seu pedido já estará confirmado e aprovado. <br />
                    A outra opção de pagamento disponível é por boleto bancário via PagSeguro, sendo somente para pagamento à vista.
                </p>
            </li>
            <li>
                <h4>ACOMPANHE O SEU PEDIDO</h4>
                <p>
                    Para acompanhar o status do seu pedido, basta que acesse em nosso site o link “Meus Pedidos”. Lá estarão todas as informações sobre o andamento do seu pedido. Vale lembrar que o seu pedido também pode ser acompanhado pelos e-mails que enviamos. <br />
                    Alguns de nossos parceiros logísticos disponibilizam a comodidade do rastreamento dos produtos nos respectivos sites, mas nem todos os transportadores oferecem este serviço.
                </p>
            </li>
            <li>
                <h4>TROCA DE DEVOLUÇÃO</h4>
                <p>
                    Para dúvidas com relação a troca, devoluções, cancelamentos e restituições, favor entrar na nossa Política de Troca e Devolução.
                </p>
            </li>
        </ul>
    </div>
</div>
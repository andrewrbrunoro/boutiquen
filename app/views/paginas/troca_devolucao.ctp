<div class="container">
    <header class="page-header">
        <h1 class="page-title"><img src="https://cdn1.iconfinder.com/data/icons/arrows-38/512/Exchange_Swap_Change_Direction_Arrows-48.png" /> POLÍTICA DE TROCA E DEVOLUÇÃO</h1>
        <span class="text-info">
            Verifique abaixo e tire suas dúvidas. Caso ainda tenha alguma dúvida, entre em contato conosco pelo telefone (51) 3228-5036 ou pelo e-mail contato@boutiqueno.com.br
        </span>
    </header>
    <div class="row">
        <div class="col-lg-12">
            <ul>
                <li>
                    <h4>Para os casos de troca de produtos por modelos ou desistência da compra.</h4>
                    <p>
                        - Entre em contato conosco através do site, e-mail ou telefone para solicitar a troca ou a desistência da compra. <br />
                        - Esta solicitação deverá ser feita em até 10 dias corridos a contar da data do recebimento do produto adquirido em nosso site. <br />
                        - O custo de devolução do produto, neste caso, será exclusivamente do cliente.
                    </p>
                </li>
                <li>
                    <h4>Procedimento para os casos de troca por defeito</h4>
                    <p>
                        - Entre em contato conosco através do site, e-mail ou telefone e relate o defeito apresentado, se possível enviar foto do problema. A solicitação para troca por defeito deverá ser feita no prazo máximo de 60 dias do recebimento do produto. <br />
                        - O custo de devolução do produto será por conta da Boutique Nô. <br />
                        - A avaliação de defeito será realizada no prazo máximo de 30 dias úteis após o produto chegar à nossa loja, conforme norma do Código de Defesa do Consumidor. <br />
                        - OBSERVAÇÃO: Caso a mercadoria se encontre com lacre violado, o cliente deverá recusar o recebimento.
                    </p>
                </li>
                <li>
                    <h4>A troca ou a desistência não serão aceitas quando</h4>
                    <p>
                        - Não for constatado o defeito relatado pelo cliente; <br />
                        - Não houver a devolução do produto ou se estiver desacompanhado da nota fiscal; <br />
                        - Houver indícios de uso inadequado do produto; <br />
                        - Houver indícios de dano acidental; <br />
                        - For constatado desgaste natural pelo tempo de uso. <br />
                        Nessas hipóteses, o produto será devolvido nas mesmas condições para o cliente.
                    </p>
                </li>
                <li>
                    <h4>Cancelamento do pedido de compra</h4>
                    <p>
                        O cancelamento do pedido será automático nas seguintes situações: <br />
                        - Impossibilidade de execução do débito correspondente à compra no cartão de crédito; <br />
                        - Inconsistência de dados no preenchimento do pedido; <br />
                        - Não envio de confirmação por e-mail ou fax, quando solicitado; <br />
                        - Não pagamento do boleto bancário.
                    </p>
                </li>
                <li>
                    <h4>Quanto a restituição de valores</h4>
                    <p>
                        Caso a compra tenha sido feita no cartão no cartão de crédito, o estorno ocorrerá conforme o procedimento adotado pela administradora do cartão do cliente. Para as demais formas de pagamento (boleto bancário), ocorrerá em 10 dias úteis, após o recebimento e análise das condições do produto por nossa empresa. <br /><br />
                        Visando a segurança de nossos clientes, se fará necessário a apresentação da nota fiscal da compra, não sendo possível o procedimento de troca sem a nota fiscal, pois ela identifica o produto e o pedido realizado no site.<br /><br />
                        Para realizar um excelente atendimento e garantir uma ótima experiência de compra aos nossos clientes, nos preocupamos com todos os detalhes que envolvem a venda. Por isso, temos procedimentos e ferramentas que garantem sua privacidade e segurança enquanto faz suas compras.<br /><br />
                        Aqui, seus dados pessoais - de pagamento e de compra - estão totalmente protegidos e EM HIPÓTESE ALGUMA* serão divulgados.
                        *Esses dados só serão liberados em caso de requisição judicial.
                    </p>
                </li>
            </ul>
        </div>
    </div>
</div>
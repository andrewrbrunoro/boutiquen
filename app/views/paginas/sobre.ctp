<div class="container">
<header class="page-header">
    <h1 class="page-title">Boutique Nô - E-commerce</h1>
</header>
<div class="row">
    <div class="col-md-9">
        <p class="lead">
            Agora você pode fazer sua compra online escolhendo seu calçado favorito com toda segurança, e com a conveniência de receber o produto na sua casa.
            A cada estação serão lançadas novas coleções, as quais estarão sempre conectadas com o que há de novo no mundo da moda.
            Faça sua compra também online, nosso principal objetivo é oferecer conforto e beleza para o seu dia a dia.
        </p>
    </div>
</div>
<div class="gap gap-small"></div>
<div class="row row-col-gap">
    <div class="col-md-8">
        <img class="img-responsive"
             src="<?php echo $this->Html->Url('/img/b-1.jpg'); ?>"
             alt="Boutique Nô - Barra Shopping Sul"
             title="Boutique Nô - Barra Shopping Sul"/>
    </div>
    <div class="col-md-4">
        <h3 class="widget-title">Nossa História</h3>
        <p>
            A rede de lojas Boutique Nô trabalha há mais de 30 anos no mercado de calçados e tem um histórico de satisfação e fidelidade com seus clientes. O sucesso das lojas que estão presentes nos principais shoppings centers de Porto Alegre levou a criar o ecommerce da Boutique Nô, visando proporcionar aos seus clientes produtos exclusivos, de alta qualidade e com o mesmo atendimento que você encontra nas lojas físicas.
        </p>
    </div>
</div>
</div>
<div class="container">
    <header class="page-header">
        <h1 class="page-title"><i class="fa fa-shield"></i> Segurança</h1>
        <span class="text-info">
            Verifique abaixo e tire suas dúvidas. Caso ainda tenha alguma dúvida, entre em contato conosco pelo telefone (51) 3228-5036 ou pelo e-mail contato@boutiqueno.com.br
        </span>
    </header>
    <div class="row">
        <div class="col-lg-12">
        Para realizar um excelente atendimento e garantir uma ótima experiência de compra aos nossos clientes, nos preocupamos com todos os detalhes que envolvem a venda. Por isso, temos procedimentos e ferramentas que garantem sua privacidade e segurança enquanto faz suas compras. <br /> <br />
        Aqui, seus dados pessoais - de pagamento e de compra - estão totalmente protegidos e EM HIPÓTESE ALGUMA* serão divulgados. <br /><br />
        *Esses dados só serão liberados em caso de requisição judicial.
        </div>
    </div>
</div>
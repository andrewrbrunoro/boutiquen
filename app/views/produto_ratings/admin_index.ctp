<div class="index">
    <h2 class="left"><?php __('Classificações'); ?></h2>
    <?php echo $form->create('ProdutoRating', array('action' => '/index', 'class' => 'formBusca')); ?>
    <fieldset>
        <div class="left">
            <?php echo $form->input('Filter.filtro', array('div' => false, 'label' => 'Usuário / Produto / Comentário:', 'class' => 'produtosFiltro')); ?>
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Busca" />
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Exportar" />
        </div>
        <div class="submit">
            <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
        </div>
    </fieldset>
    <?php echo $form->end(); ?>

    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('usuario_id'); ?></th>
            <th><?php echo $this->Paginator->sort('produto_id'); ?></th>
            <th><?php echo $this->Paginator->sort('avaliacao_geral'); ?></th>
            <th><?php echo $this->Paginator->sort('comentario'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($classificacoes as $classificacoes):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr <?php echo $class; ?>>
                <td><?php echo $classificacoes['ProdutoRating']['id']; ?>&nbsp;</td>
                <td><?php echo $classificacoes['Usuario']['nome']; ?>&nbsp;</td>
                <td width="30%"><?php echo $classificacoes['Produto']['nome']; ?>&nbsp;</td>
                <td width="30%"><?php echo ($classificacoes['ProdutoRating']['avaliacao_geral']) ? 'Recomenda' : 'Não recomenda'; ?>&nbsp;</td>
                <td width="30%"><div style="margin:10px 0;"><?php echo $classificacoes['ProdutoRating']['comentario']; ?></div>&nbsp;</td>
                <td><?php echo ($classificacoes['ProdutoRating']['status']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $classificacoes['ProdutoRating']['created']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Muda Status', true), array('action' => 'mudastatus', $classificacoes['ProdutoRating']['id'])); ?>
                    <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $classificacoes['ProdutoRating']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $classificacoes['ProdutoRating']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
        ));
        ?>
    </p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>

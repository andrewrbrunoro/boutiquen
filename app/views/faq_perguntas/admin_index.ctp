<div class="index">
	<h2><?php __('Faq Perguntas');?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Perguntas', true), array('action' => 'add')); ?>
    </div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
                        <th><?php echo $this->Paginator->sort('faq_id');?></th>
			<th><?php echo $this->Paginator->sort('texto');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
                       <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
                        <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
			<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($FaqPerguntas as $FaqPergunta):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $FaqPergunta['FaqPergunta']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($FaqPergunta['Faq']['texto'], array('controller' => 'faqs', 'action' => 'edit', $FaqPergunta['Faq']['id'])); ?>
		</td>
		<td><?php echo $FaqPergunta['FaqPergunta']['texto']; ?>&nbsp;</td>
		<td align="center"><?php echo ($FaqPergunta['FaqPergunta']['status'])?'Ativo':'Inativo'; ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$FaqPergunta['FaqPergunta']['created']); ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $FaqPergunta['FaqPergunta']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $FaqPergunta['FaqPergunta']['id'])); ?>
			<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $FaqPergunta['FaqPergunta']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $FaqPergunta['FaqPergunta']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>

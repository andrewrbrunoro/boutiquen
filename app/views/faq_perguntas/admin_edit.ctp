<?php
echo $javascript->link('common/jquery.uploadify.v2.1.4.min' ,false);
echo $javascript->link('common/jquery.meio_mask.js',false);
echo $javascript->link('common/tiny_mce/tiny_mce.js',false);
echo $javascript->link('common/jquery.cookie.js',false);
echo $javascript->link('admin/produtos/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('FaqPergunta');?>
	<fieldset>
		<legend><?php __('Editar Faq Pergunta'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
		echo $this->Form->input('faq_id');
		echo $this->Form->input('texto',array('type'=>'textArea','class'=>'mceEditor wCEM h400'));
		echo $this->Form->end(__('Salvar', true));
	?>
	</fieldset>
</div>

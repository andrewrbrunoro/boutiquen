<div class="index">
	<h2><?php __('Faq Respostas');?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Resposta', true), array('action' => 'add')); ?>
    </div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
                        <th><?php echo $this->Paginator->sort('faq_pergunta_id');?></th>
			<th><?php echo $this->Paginator->sort('texto');?></th>
			<th><?php echo $this->Paginator->sort('status');?></th>
                       <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
                        <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
			
			<th class="actions">Ações</th>
	</tr>
	<?php
	$i = 0;
	foreach ($faqRespostas as $faqResposta):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td align="center"><?php echo $faqResposta['FaqResposta']['id']; ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($faqResposta['FaqPergunta']['texto'], array('controller' => 'faq_perguntas', 'action' => 'edit', $faqResposta['FaqPergunta']['id'])); ?>
		</td>
		<td><?php echo $faqResposta['FaqResposta']['texto']; ?>&nbsp;</td>
		<td align="center"><?php echo ($faqResposta['FaqResposta']['status'])?'Ativo':'Inativo'; ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$faqResposta['FaqResposta']['created']); ?>&nbsp;</td>
		<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $faqResposta['FaqResposta']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $faqResposta['FaqResposta']['id'])); ?>
			<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $faqResposta['FaqResposta']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $faqResposta['FaqResposta']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>

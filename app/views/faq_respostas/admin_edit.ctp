<?php
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/faq_respostas/crud.js',false);
?>
<div class="index">
<?php echo $this->Form->create('FaqResposta');?>
	<fieldset>
		<legend><?php __('Editar Faq Resposta'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('status',array('type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
		echo $this->Form->input('faq_pergunta_id',array('options'=>$faqPergunta));
		echo $this->Form->input('texto',array('type'=>'textarea','class'=>'mceEditor wCEM h400'));
		echo $this->Form->end(__('Salvar', true));
	?>
	</fieldset>
</div>

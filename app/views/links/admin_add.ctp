<?php echo $javascript->link('admin/links/crud.js', false); ?>
<div class="index">
    <h2>Adicionar Link</h2>
    <?php echo $form->create('Link', array('type' => 'file', 'url' => array('controller' => 'links', 'action' => 'add', 'menu' => $menu)));?>
        <fieldset>
            <div class="tabs">
                <div id="link-basic">
                    <?php
						echo $form->input('status',array('default'=>true,'type'=>'radio','options'=>array(true=>'Ativo',false=>'Inativo')));
                        echo $form->input('menu_id', array('selected' => $menu));
                        echo $form->input('parent_id', array(
                            'label' => __('Parent', true),
                            'options' => $parentLinks,
                            'empty' => true,
                        ));
                        echo $form->input('title');
					?>
					
					<?php
						echo $form->input('tipo',array('default'=>'NORMAL','type'=>'radio','options'=>array('NORMAL'=>'NORMAL','VITRINE'=>'VITRINE','PAGINA'=> 'PAGINA','CATEGORIA'=>'CATEGORIA')));
					?>
					
					<?php echo $form->input('vitrine_id', array('type' => 'select', 'options' => $VitrineId, 'empty' => true)); ?>
					<?php echo $form->input('pagina_id', array('type' => 'select', 'options' => $PaginaId, 'empty' => true)); ?>
					<?php echo $form->input('categoria_id', array('type' => 'select', 'options' => $CategoriaId, 'empty' => true)); ?>
					<?php echo $form->input('link'); ?>
					
					<?php echo $this->Form->input('target', array('default' => '_self', 'type' => 'radio', 'options' => array('_self' => '_self', '_blank' => '_blank'))); ?>
		
                </div>

                <div id="link-access">
                    <?php echo $form->input('Grupo.Grupo'); ?>
                </div>

                <div id="link-misc">
                    <?php
                        echo $form->input('description',array('label'=>'Descrição'));
                        echo $form->input('rel');
                        echo $form->input('params',array('label'=>'Parâmetros'));
                    ?>
                </div>
				
				
				<legend>Imagem</legend>
				<?php
					echo $this->Form->input('Categoria.thumb_filename', array('type' => 'file'));
					echo $this->Form->input('Categoria.thumb_dir', array('type' => 'hidden'));
					echo $this->Form->input('Categoria.thumb_mimetype', array('type' => 'hidden'));
					echo $this->Form->input('Categoria.thumb_filesize', array('type' => 'hidden'));
				?>

            </div>
        </fieldset>
    <?php echo $form->end('Enviar');?>
</div>
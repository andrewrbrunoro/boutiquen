<div class="index">
    <h2><?php echo $title_for_layout; ?></h2>
	  <div class="btAddProduto">
    	<?php echo $this->Html->link(__('Adicionar Link', true), array('action' => 'add', 'menu' => $this->params['named']['menu'])); ?>
    </div>    
  
    <?php
    	if (isset($this->params['named'])) {
            foreach ($this->params['named'] AS $nn => $nv) {
                $paginator->options['url'][] = $nn . ':' . $nv;
            }
        }
    ?>

    <table cellpadding="0" cellspacing="0">
    <?php
        $tableHeaders =  $html->tableHeaders(array(
            __('Id', true),
            __('Título', true),
            __('Status', true),
            __('Ações', true),
        ));
        echo $tableHeaders;

        $rows = array();
        foreach ($linksTree AS $linkId => $linkTitle) {
            $actions  = $html->link(__('Mover para cima', true), array('controller' => 'links', 'action' => 'moveup', 'menu' => $menu, $linkId));
            $actions .= ' ' . $html->link(__('Mover para baixo', true), array('controller' => 'links', 'action' => 'movedown', 'menu' => $menu, $linkId));
            $actions .= ' ' . $html->link(__('Editar', true), array('controller' => 'links', 'action' => 'edit', 'menu' => $menu, $linkId));
            $actions .= ' ' . $html->link(__('Deletar', true), array(
                'controller' => 'links',
                'action' => 'delete',
                'menu' => $menu,
                $linkId,
            ), null, __('Deseja mesmo remover o registro?', true));

            $rows[] = array(
                $linkId,
                $linkTitle,
                ($linksStatus[$linkId])?'Ativo':'Inativo',
                $actions,
            );
        }

        echo $html->tableCells($rows);
    ?>
    </table>
   
</div>

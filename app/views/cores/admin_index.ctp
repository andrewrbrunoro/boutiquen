<div class="index">
    <h2 class="left"><?php __('Cor'); ?></h2>
    <div class="btAddProduto">
    	<?php echo $this->Html->link(__('Adicionar Cor', true), array('action' => 'add')); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>
            <th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
        <?php
        $i = 0;
        foreach ($cores as $cor):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
        ?>
            <tr<?php echo $class; ?>>
                <td><?php echo $cor['Cor']['id']; ?>&nbsp;</td>
                <td><?php echo $cor['Cor']['nome']; ?>&nbsp;</td>
                <td><?php echo ($cor['Cor']['status']) ? 'Ativo' : 'Inativo'; ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$cor['Cor']['created']); ?>&nbsp;</td>
                <td><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s',$cor['Cor']['modified']); ?>&nbsp;</td>
                <td class="actions">
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $cor['Cor']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $cor['Cor']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $cor['Cor']['id'])); ?>
            </td>
        </tr>
        <?php endforeach; ?>
            </table>
            <p>
        <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
                ));
        ?>	</p>

            <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
                	 | 	<?php echo $this->Paginator->numbers(); ?>
                |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
            </div>
        </div>
      
<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/variacoes/index.js',false);
?>
<div class="index">
<?php 
    echo $this->Form->create('Variacao',array('type'=>'file'));
    if(isset($validationErrors))
        $this->Form->validationErrors = $validationErrors;
?>
    
    <fieldset class="fieldset_variacoes">	
        <legend>Grupo de Variações</legend>
        
        <?php echo $this->Form->input('VariacaoGrupo.id',array('label'=>'Grupo de Variação','options' => $variacao_grupos)); ?>
        <div class="container-grupos-variacoes container">
            <div class="container-grupos-variacao-tipo container-content-variacao-tipo">
                <div class="left">
                    <?php echo $this->Form->input("VariacaoTipo.0.id", array('hiddenField' => false, 'type' => 'hidden')); ?>
                    <?php echo $this->Form->input("VariacaoTipo.0.nome", array('label'=>'Tipo de Variação', 'class' => 'inputs')); ?>
                </div>
                <div class="left">
                    <?php echo $this->Form->input("VariacaoTipo.0.ordem", array('label'=>'Ordem', 'class' => 'inputs')); ?>
                </div>
                <a href="#" class="add add-tipo">add</a>
                <a href="#" class="rm rm-tipo" style="display:none">rm</a>
                <div class="left clear">
                    <div class="container-grupos-variacao-valor container-content-variacao-valor">
                        <div class="left">
                            <?php echo $this->Form->input("Variacao.0.0.id", array('hiddenField' => false, 'type' => 'hidden')); ?>
                            <?php echo $this->Form->input("Variacao.0.0.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
							<?php echo $this->Form->input("Variacao.0.0.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                            <div class="content-thumb">
                                <?php echo $this->Form->input("Variacao.0.0.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                <div class="content-thumb-file">
                                    <?php 
                                        echo $this->Form->input("Variacao.0.0.thumb_filename", array('type' => 'file'));
                                        echo $this->Form->input("Variacao.0.0.thumb_dir", array('type' => 'hidden'));
                                        echo $this->Form->input("Variacao.0.0.thumb_mimetype", array('type' => 'hidden'));
                                        echo $this->Form->input("Variacao.0.0.thumb_filesize", array('type' => 'hidden'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="add add-valor">add</a>
                        <a href="#" class="rm rm-valor" style="display:none">rm</a>
                    </div>
                    <div class="container-grupos-variacao-valor-tmp">
                        <?php if(count($this->data['Variacao'][0]) > 0):
                                $first = true; 
                                foreach($this->data['Variacao'][0] as $i => $var_vlr):
                                    if($first){
                                        $first = false;
                                        continue;
                                    }?>
                                    <div class="container-grupos-variacao-valor">
                                          <div class="left">  
                                                <?php echo $this->Form->input("Variacao.0.{$i}.id", array('hiddenField' => false, 'type' => 'hidden')); ?>
                                                <?php echo $this->Form->input("Variacao.0.{$i}.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
												<?php echo $this->Form->input("Variacao.0.{$i}.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                                                <div class="content-thumb">
                                                    <?php echo $this->Form->input("Variacao.0.{$i}.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                                    <div class="content-thumb-file">
                                                        <?php 
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_filename", array('type' => 'file'));
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_dir", array('type' => 'hidden'));
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_mimetype", array('type' => 'hidden'));
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_filesize", array('type' => 'hidden'));
                                                        ?>
                                                    </div>
                                                </div>
                                          </div>
                                          <a href="#" class="rm rm-valor">rm</a>
                                    </div>
                          <?php 
                                endforeach;
                              endIf;
                         ?>
                    </div>
                </div>
            </div>
            <div class="container-grupos-variacao-tipo-tmp">
                
                <?php if(count($this->data['VariacaoTipo']) > 1): 
                        $first2 = true;
                        
                        foreach($this->data['VariacaoTipo'] as $y => $vp):
                            if($first2){
                                $first2 = false;
                                continue;
                            }
                            ?>
                
                            <div class="container-grupos-variacao-tipo">
                                <div class="left">
                                    <?php echo $this->Form->input("VariacaoTipo.{$y}.id", array('hiddenField' => false, 'type' => 'hidden')); ?>
                                    <?php echo $this->Form->input("VariacaoTipo.{$y}.nome", array('label'=>'Tipo de Variação', 'class' => 'inputs')); ?>
                                </div>
                                <div class="left">
                                    <?php echo $this->Form->input("VariacaoTipo.{$y}.ordem", array('label'=>'Ordem', 'class' => 'inputs')); ?>
                                </div>
                                <a href="#" class="rm rm-tipo">rm</a>
                                <div class="left clear">
                                    <div class="container-grupos-variacao-valor">
                                        <div class="left">
                                            <?php echo $this->Form->input("Variacao.{$y}.0.id", array('hiddenField' => false, 'type' => 'hidden')); ?>
                                            <?php echo $this->Form->input("Variacao.{$y}.0.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
											<?php echo $this->Form->input("Variacao.{$y}.0.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                                            <div class="content-thumb">
                                                <?php echo $this->Form->input("Variacao.{$y}.0.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                                <div class="content-thumb-file">
                                                    <?php 
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_filename", array('type' => 'file'));
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_dir", array('type' => 'hidden'));
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_mimetype", array('type' => 'hidden'));
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_filesize", array('type' => 'hidden'));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="add add-valor">add</a>
                                        <a href="#" class="rm rm-valor" style="display:none">rm</a>
                                    </div>
                                    <div class="container-grupos-variacao-valor-tmp">
                                        <?php if(count($this->data['Variacao'][$y]) > 0): 
                                                $first3 = true; 
                                                foreach($this->data['Variacao'][$y] as $f => $var_vlr):
                                                    if($first3){
                                                        $first3 = false;
                                                        continue;
                                                    }?>
                                                    <div class="container-grupos-variacao-valor">
                                                          <div class="left">  
                                                                <?php echo $this->Form->input("Variacao.{$y}.{$f}.id", array('hiddenField' => false, 'type' => 'hidden')); ?>
                                                                <?php echo $this->Form->input("Variacao.{$y}.{$f}.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
																<?php echo $this->Form->input("Variacao.{$y}.{$f}.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                                                                <div class="content-thumb">
                                                                    <?php echo $this->Form->input("Variacao.{$y}.{$f}.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                                                    <div class="content-thumb-file">
                                                                        <?php 
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_filename", array('type' => 'file'));
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_dir", array('type' => 'hidden'));
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_mimetype", array('type' => 'hidden'));
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_filesize", array('type' => 'hidden'));
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                          </div>
                                                          <a href="#" class="rm rm-valor">rm</a>
                                                    </div>
                                          <?php 
                                                endforeach;
                                              endIf;
                                         ?>
                                    </div>
                                </div>
                            </div>
                    <?php   
                        endforeach;
                       endif;
                    ?>
            </div>
        </div>
    </fieldset>
    
    <?php
	echo $this->Form->end(__('Inserir', true));
    ?>
</div>
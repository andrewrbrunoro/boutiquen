<div class="index ">
    <h2><?php __('Variacões'); ?></h2>
    <div class="btAddProduto">
        <?php echo $this->Html->link(__('[+] Adicionar Variação', true), array('action' => 'add')); ?>
    </div>
    <?php echo $form->create('Variacao', array('action' => '/index', 'class' => 'formBusca')); ?>
    <fieldset>
        <div class="left">
            <?php echo $form->input('Filter.variacao_grupo_id', array('div' => false, 'label' => 'Grupo de Variação:', 'class' => 'produtosFiltro', 'options' => $variacao_grupos)); ?>
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Busca" />
        </div>
        <div class="submit">
            <input name="submit" type="submit" class="button1" value="Exportar" />
        </div>
        <div class="submit">
            <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
        </div>
    </fieldset>
    <?php echo $form->end(); ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th>Grupo</th>
            <th>Variações</th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($variacoes as $variacao):
            $class = null;
            if(count($variacao['VariacaoTipo']) == 0) continue;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $variacao['VariacaoGrupo']['id']; ?>&nbsp;</td>
                <td align="center"><?php echo $variacao['VariacaoGrupo']['nome']; ?>&nbsp;</td>
                <td>
                    <table border="0" class="table_variacoes_grade">
                        <?php foreach($variacao['VariacaoTipo'] as $vt): ?>
                        <tr>
                            <td>
                                <?php echo $vt['nome']; ?>
                            </td>
                            <td>
                                <table border="0">
								<tr>
									<td>Valor</td>
									<td>Ordem</td>
								</tr>
                                <?php foreach($vt['Variacao'] as $v): ?>
                                <tr>
                                    <td><?php echo $v['valor']; ?></td>
									<td><?php echo $v['ordem']; ?></td>
                                </tr>    
                                <?php endForeach; ?>
                                </table>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $variacao['VariacaoGrupo']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $variacao['VariacaoGrupo']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $variacao['VariacaoGrupo']['id'])); ?>
                    <?php //echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $variacao['VariacaoGrupo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $variacao['VariacaoGrupo']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>

</div>

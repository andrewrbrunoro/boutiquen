<?php
echo $javascript->link('common/jquery.meio_mask.js', false);
echo $javascript->link('common/tiny_mce/tiny_mce_src.js',false);
echo $javascript->link('admin/variacoes/index.js',false);

//Configure::write('debug',2);
//$foo = "baka";
//debug($foo);

//Configure::write('debug',0);
//die;

?>
<div class="index">
    <?php echo $this->Form->create('Variacao',array('type'=>'file')); 
        if(isset($validationErrors)){
            $this->Form->validationErrors = $validationErrors;
        }
    ?>
    <fieldset class="fieldset_variacoes">	
        <legend>Grupo de Variações</legend>
        
        <?php if(isset($produtos_editados) && count($produtos_editados) > 0): ?>
            <table>
                <tr>
                    <th colspan="3">Produtos que foram desativados</th>
                </tr>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Editar</th>
                </tr>
                <?php foreach($produtos_editados as $pe): ?>
                    <tr>
                        <td><?php echo $pe['Produto']['id']; ?></td>
                        <td><?php echo $pe['Produto']['nome']; ?></td>
                        <td><a href="<?php echo $this->Html->Url('/admin/produtos/edit/'.$pe['Produto']['id']); ?>">Editar</a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endIf; ?>
        
        <?php echo $this->Form->input('VariacaoGrupo.id',array('label'=>'Grupo de Variação','options' => $variacao_grupos)); ?>
        <div class="container-grupos-variacoes container">
            <div class="container-grupos-variacao-tipo container-content-variacao-tipo">
                <div class="left">
                    <?php echo $this->Form->input("VariacaoTipo.0.id", array('hiddenField' => false, 'type' => 'hidden', 'class' => 'inputs')); ?>
                    <div class="input_remover">
                        <?php echo $this->Form->input("VariacaoTipo.0.remover", array('label'=>'Remover', 'type' => 'checkbox')); ?>
                    </div>
                    <?php echo $this->Form->input("VariacaoTipo.0.nome", array('label'=>'Tipo de Variação', 'class' => 'inputs')); ?>
                </div>
                <div class="left">
                    <?php echo $this->Form->input("VariacaoTipo.0.ordem", array('label'=>'Ordem', 'class' => 'inputs')); ?>
                </div>
                
                <?php 
                    if(isset($this->params['pass'][0])){
                        $grupo_id = $this->params['pass'][0];
                    }else if($this->data['VariacaoGrupo']['id']){
                        //$grupo_id = $grupo_id;
			$grupo_id = $this->data['VariacaoGrupo']['id'];
                    }
                ?>
                <a href="#" class="add add-tipo add-tipo-edit" rel="<?php echo $grupo_id; ?>" title="Adicionar Tipo de Variação">add</a>
                <a href="#" class="rm-tipo-ajax" rel="<?php echo $this->data['VariacaoTipo'][0]['id']; ?>" title="Remover Tipo de Variação">rm</a>
                <div class="left clear">
                    <div class="container-grupos-variacao-valor container-content-variacao-valor">
                        <div class="left">
                            <?php echo $this->Form->input("Variacao.0.0.id", array('hiddenField' => false, 'type' => 'hidden', 'class' => 'inputs')); ?>
                            <div class="input_remover">
                                <?php echo $this->Form->input("Variacao.0.0.remover", array('label'=>'Remover', 'type' => 'checkbox')); ?>
                            </div>
                            <?php echo $this->Form->input("Variacao.0.0.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
							 <?php echo $this->Form->input("Variacao.0.0.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                            <div class="content-thumb">
                                <?php echo $this->Form->input("Variacao.0.0.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                <div class="content-thumb-file">
                                    <?php 
                                        echo $this->Form->input("Variacao.0.0.thumb_filename", array('type' => 'file'));
                                        echo $this->Form->input("Variacao.0.0.thumb_dir", array('type' => 'hidden'));
                                        echo $this->Form->input("Variacao.0.0.thumb_mimetype", array('type' => 'hidden'));
                                        echo $this->Form->input("Variacao.0.0.thumb_filesize", array('type' => 'hidden'));
                                    ?>
                                    <span class="content-thumb-preview" style="margin-left: 5px;">
                                    <?php 
                                        $img = '';
                                        if($this->data['Variacao'][0][0]['thumb']){
                                            $img = ($this->data['Variacao'][0][0]['thumb_filename']) ? $this->data['Variacao'][0][0]['thumb_dir'] . '/' . $this->data['Variacao'][0][0]['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
                                            echo $image->resize($img, 80, 80);
                                            echo $this->Form->input("Variacao.0.0.thumb_remove", array('type' => 'checkbox'));
                                        }
                                    ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="add add-valor" title="Adicionar Variação">add</a>
                        <a href="#" class="rm-valor-ajax" rel="<?php echo $this->data['Variacao'][0][0]['id']; ?>" title="Remover Variação">rm</a>
                    </div>
                    <div class="container-grupos-variacao-valor-tmp">
                        <?php if(count($this->data['Variacao'][0]) > 0):
                                $first = true; 
                                foreach($this->data['Variacao'][0] as $i => $var_vlr):
                                    if($first){
                                        $first = false;
                                        continue;
                                    }?>
                                    <div class="container-grupos-variacao-valor">
                                          <div class="left">  
                                                <?php echo $this->Form->input("Variacao.0.{$i}.id", array('hiddenField' => false, 'type' => 'hidden', 'class' => 'inputs')); ?>
                                                <div class="input_remover">
                                                     <?php echo $this->Form->input("Variacao.0.{$i}.remover", array('label'=>'Remover', 'type' => 'checkbox')); ?>
                                                </div>
                                                <?php echo $this->Form->input("Variacao.0.{$i}.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
												<?php echo $this->Form->input("Variacao.0.{$i}.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                                                <div class="content-thumb">
                                                    <?php echo $this->Form->input("Variacao.0.{$i}.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                                    <div class="content-thumb-file">
                                                        <?php 
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_filename", array('type' => 'file'));
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_dir", array('type' => 'hidden'));
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_mimetype", array('type' => 'hidden'));
                                                            echo $this->Form->input("Variacao.0.{$i}.thumb_filesize", array('type' => 'hidden'));
                                                        ?>
                                                        <span class="content-thumb-preview" style="margin-left: 5px;">
                                                        <?php 
                                                            $img = '';
                                                            if($this->data['Variacao'][0][$i]['thumb']){
                                                                $img = ($this->data['Variacao'][0][$i]['thumb_filename']) ? $this->data['Variacao'][0][$i]['thumb_dir'] . '/' . $this->data['Variacao'][0][$i]['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
                                                                echo $image->resize($img, 80, 80);
                                                                echo $this->Form->input("Variacao.0.$i.thumb_remove", array('type' => 'checkbox'));
                                                            }
                                                        ?>
                                                        </span>
                                                    </div>
                                                </div>
                                          </div>
                                          <a href="#" class="rm-valor-ajax" rel="<?php echo $this->data['Variacao'][0][$i]['id']; ?>" title="Remover Variação">rm</a>
                                    </div>
                          <?php 
                                endforeach;
                              endIf;
                         ?>
                    </div>
                </div>
            </div>
            <div class="container-grupos-variacao-tipo-tmp">
                
                <?php if(count($this->data['VariacaoTipo']) > 1): 
                        $first2 = true;
                        
                        foreach($this->data['VariacaoTipo'] as $y => $vp):
                            if($first2){
                                $first2 = false;
                                continue;
                            }
                            ?>
                
                            <div class="container-grupos-variacao-tipo">
                                <div class="left">
                                    <?php echo $this->Form->input("VariacaoTipo.{$y}.id", array('hiddenField' => false, 'type' => 'hidden', 'class' => 'inputs')); ?>
                                    <div class="input_remover">
                                        <?php echo $this->Form->input("VariacaoTipo.{$y}.remover", array('label'=>'Remover', 'type' => 'checkbox')); ?>
                                    </div>
                                    <?php echo $this->Form->input("VariacaoTipo.{$y}.nome", array('label'=>'Tipo de Variação', 'class' => 'inputs')); ?>
                                </div>
                                <div class="left">
                                    <?php echo $this->Form->input("VariacaoTipo.{$y}.ordem", array('label'=>'Ordem', 'class' => 'inputs')); ?>
                                </div>
                                <a href="#" class="rm-tipo-ajax" rel="<?php echo $this->data['VariacaoTipo'][$y]['id']; ?>" title="Remover Tipo de Variação">rm</a>
                                <div class="left clear">
                                    <div class="container-grupos-variacao-valor">
                                        <div class="left">
                                            <?php echo $this->Form->input("Variacao.{$y}.0.id", array('hiddenField' => false, 'type' => 'hidden', 'class' => 'inputs')); ?>
                                            <div class="input_remover">
                                                <?php echo $this->Form->input("Variacao.{$y}.0.remover", array('label'=>'Remover', 'type' => 'checkbox')); ?>
                                            </div>
                                            <?php echo $this->Form->input("Variacao.{$y}.0.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
											<?php echo $this->Form->input("Variacao.{$y}.0.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                                            <div class="content-thumb">
                                                <?php echo $this->Form->input("Variacao.{$y}.0.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                                <div class="content-thumb-file">
                                                    <?php 
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_filename", array('type' => 'file'));
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_dir", array('type' => 'hidden'));
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_mimetype", array('type' => 'hidden'));
                                                        echo $this->Form->input("Variacao.{$y}.0.thumb_filesize", array('type' => 'hidden'));
                                                    ?>
                                                    <span class="content-thumb-preview" style="margin-left: 5px;">
                                                    <?php 
                                                        $img = '';
                                                        if($this->data['Variacao'][$y][0]['thumb']){
                                                            $img = ($this->data['Variacao'][$y][0]['thumb_filename']) ? $this->data['Variacao'][$y][0]['thumb_dir'] . '/' . $this->data['Variacao'][$y][0]['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
                                                            echo $image->resize($img, 80, 80);
                                                            echo $this->Form->input("Variacao.$y.0.thumb_remove", array('type' => 'checkbox'));
                                                        }
                                                    ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="add add-valor" title="Adicionar Variação">add</a>
                                        <?php if(isset($this->data['Variacao'][$y][0])): ?>
                                            <a href="#" class="rm-valor-ajax" title="Remover Variação" rel="<?php echo $this->data['Variacao'][$y][0]['id']; ?>">rm</a>
                                        <?php endIf; ?>
                                    </div>
                                    <div class="container-grupos-variacao-valor-tmp">
                                        <?php if(isset($this->data['Variacao'][$y]) && count($this->data['Variacao'][$y]) > 0): 
                                                $first3 = true; 
                                                foreach($this->data['Variacao'][$y] as $f => $var_vlr):
                                                    if($first3){
                                                        $first3 = false;
                                                        continue;
                                                    }?>
                                                    <div class="container-grupos-variacao-valor">
                                                          <div class="left">  
                                                                <?php echo $this->Form->input("Variacao.{$y}.{$f}.id", array('hiddenField' => false, 'type' => 'hidden', 'class' => 'inputs')); ?>
                                                                <div class="input_remover">
                                                                    <?php echo $this->Form->input("Variacao.{$y}.{$f}.remover", array('label'=>'Remover', 'type' => 'checkbox')); ?>
                                                                </div>
                                                                <?php echo $this->Form->input("Variacao.{$y}.{$f}.valor", array('label' => 'Valor', 'class' => 'inputs variacoes_valor')); ?>
																 <?php echo $this->Form->input("Variacao.{$y}.{$f}.ordem", array('label' => 'Ordem', 'class' => 'inputs variacoes_valor')); ?>
                                                                <div class="content-thumb">
                                                                    <?php echo $this->Form->input("Variacao.{$y}.{$f}.thumb", array('type' => 'checkbox', 'class' => 'flag-thumb')); ?>
                                                                    <div class="content-thumb-file">
                                                                        <?php 
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_filename", array('type' => 'file'));
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_dir", array('type' => 'hidden'));
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_mimetype", array('type' => 'hidden'));
                                                                            echo $this->Form->input("Variacao.{$y}.{$f}.thumb_filesize", array('type' => 'hidden'));
                                                                        ?>
                                                                        <span class="content-thumb-preview" style="margin-left: 5px;">
                                                                        <?php 
                                                                            $img = '';
                                                                            if($this->data['Variacao'][$y][$f]['thumb']){
                                                                                $img = ($this->data['Variacao'][$y][$f]['thumb_filename']) ? $this->data['Variacao'][$y][$f]['thumb_dir'] . '/' . $this->data['Variacao'][$y][$f]['thumb_filename'] : "uploads/produto_imagem/thumb/sem_imagem.jpg";
                                                                                echo $image->resize($img, 80, 80);
                                                                                echo $this->Form->input("Variacao.$y.$f.thumb_remove", array('type' => 'checkbox'));
                                                                            }
                                                                        ?>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                          </div>
                                                          <a href="#" class="rm-valor-ajax" title="Remover Variação" rel="<?php echo $this->data['Variacao'][$y][$f]['id']; ?>">rm</a>
                                                    </div>
                                          <?php 
                                                endforeach;
                                              endIf;
                                         ?>
                                    </div>
                                </div>
                            </div>
                    <?php   
                        endforeach;
                       endif;
                    ?>
            </div>
        </div>
    </fieldset>
    <?php
	echo $this->Form->end(__('Salvar', true));
    ?>
</div>
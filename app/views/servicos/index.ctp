<?php echo $javascript->link('site/paginas/index.js', false); ?>
<!-- start whitecol -->
<div class="whitecol">
    <div class="whitecol-in">
        <!-- start entry -->
        <div class="entry">
            <!-- txtb -->
            <div class="txtb">
                <h2><?php echo $servico['Servico']['nome']; ?></h2>
                <p><?php echo $servico['Servico']['descricao']; ?></p>
            </div>
            <!-- txtb -->
            <!-- column-right -->
            <div class="column-right2">
                <?php if(count($servicos) > 0): ?>
                <!-- bath-col -->
                <div class="bath-col">
                    <ul>
                        <?php foreach($servicos as $serv): ?>
                            <li><a href="<?php echo $this->Html->Url("/servicos/".$serv['Servico']['seo_url']); ?>" title="<?php echo $serv['Servico']['nome'] ?>"><?php echo $serv['Servico']['nome'] ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <!-- bath-col -->
                <?php endIf; ?>
                <!-- imgb -->
                <div class="imgb">
                    <div class="imgb-in" style="text-align: center;">
                        <?php
                        if (count($servico['ServicoImagem']) > 0):
                            foreach ($servico['ServicoImagem'] as $indi => $img):
                                ?>
                                <!-- start seleto -->
                                <div class="seleto sel_foto<?php echo $indi; ?>" <?php if ($indi > 0) { echo ' style="display:none;" '; } ?> >
                                    <?php echo $image->resize(isset($img['filename']) ? $img['dir'] . '/' . $img['filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 447, 293, true, array('alt' => Inflector::slug($servico['Servico']['nome'], ' '))); ?>
                                </div>
                                <!-- end seleto -->
                                <?php
                            endForeach;
                        endIf; ?>
                    </div>
                </div>
                <!-- imgb -->
                 <?php
                    if (count($servico['ServicoImagem']) > 1) {
                        ?>
                        <!-- start slide-nav -->
                        <div class="slider-imagens-servicos-content" style="float: left;">
                            <a href="javascript:void(0);" title="Previous" class="prev btn-prev">Previous</a>
                            <a href="javascript:void(0);" title="Next" class="next btn-next">Next</a>
                            <ul class="slider-imagens-servicos">
                                <?php foreach ($servico['ServicoImagem'] as $indi => $img): ?>
                                    <li>
                                        <a class="clique_sel" rel="foto<?php echo $indi; ?>" href="javascript:;">
                                            <?php echo $image->resize(isset($img['filename']) ? $img['dir'] . '/' . $img['filename'] : 'uploads/produto_imagem/filename/sem_imagem.jpg', 73, 126, true, array('alt' => Inflector::slug($servico['Servico']['nome'], ' '))); ?>
                                        </a>
                                    </li>
                                <?php endForeach ?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <!-- end slide-nav -->
                        <?php
                    } ?>
            </div>
            <!-- column-right -->
            <div class="clear"></div>
            <a href="<?php echo $this->Html->Url("/servicos"); ?>" title="Voltar" class="voltar">Voltar</a>
        </div>
        <!-- end entry -->
        <div class="clear"></div>
    </div>
</div>
<!-- end whitecol -->
<div class="clear"></div>
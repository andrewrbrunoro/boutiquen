<?php echo $javascript->link('common/swfobject.js', false); ?>
<?php echo $javascript->link('common/jquery.uploadify.v2.1.4.min', false);  ?>
<?php echo $javascript->link('admin/servicos/crud.js', false);  ?>
<?php echo $this->Html->css('admin/style');  ?>
<div class="index">
    <?php echo $this->Form->create('Servico',array('type' => 'file')); ?>
        <fieldset>
        	<legend><?php __('Editar Serviço'); ?></legend>
            <div class="left clear">
                <?php 
                    echo $this->Form->input('id');
                    echo $this->Form->input('status',array('type'=>'radio','default'=>true,'options'=>array(true=>'Ativo',false=>'Inativo'))); 
                ?>
            </div>
            <div class="left clear">
                <?php echo $this->Form->input('nome',array('class'=>'w312'));  ?>
            </div>
            <div class="left clear">
                <?php echo $this->Form->input('descricao',array('class'=>'w312')); ?>
            </div>
        </fieldset>
        <fieldset>
            <legend>Adicionar Imagens</legend>
            <input id="edit-imagem" name="file_upload" type="file" />
            <div class="container-edit">
                <?php 
                    if(isset($imgs)){
                        echo $imgs;
                    }
                ?>     
            </div>
        </fieldset>
    <?php echo $this->Form->end(__('Inserir', true)); ?>
</div>
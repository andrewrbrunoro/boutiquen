<?php

    /**
     * Application model for Cake.
     *
     * This file is application-wide model file. You can put all
     * application-wide model-related methods here.
     *
     * PHP versions 4 and 5
     *
     * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
     * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
     * @link          http://cakephp.org CakePHP(tm) Project
     * @package       cake
     * @subpackage    cake.app
     * @since         CakePHP(tm) v 0.2.9
     * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
     */

    /**
     * Application model for Cake.
     *
     * Add your application-wide methods in the class below, your models
     * will inherit them.
     *
     * @package       cake
     * @subpackage    cake.app
     */
    class AppModel extends Model
    {

        private $blackListStatus = array(
            'Usuario',
            'UsuarioEndereco'
        );

        public function beforeFind($queryData)
        {
            parent::beforeFind($queryData);
//            if (parent::hasField('status')) {
//                foreach ($queryData as $key => $value) {
//                    if ($key == 'conditions') {
//                        if (!in_array($this->alias, $this->blackListStatus)) {
//                            $queryData[$key][$this->name . '.status'] = true;
//                        }
//                    }
//                }
//            }
            return $queryData;
        }

        public function getLastQuery()
        {
            $dbo = $this->getDatasource();
            $logs = $dbo->_queriesLog;
            return end($logs);
        }

    }

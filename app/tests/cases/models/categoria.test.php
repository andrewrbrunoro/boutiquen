<?php
/* Categoria Test cases generated on: 2011-04-07 16:16:35 : 1302203795*/
App::import('Model', 'Categoria');

class CategoriaTestCase extends CakeTestCase {
	var $fixtures = array('app.categoria', 'app.departamento', 'app.produto', 'app.subcategoria');

	function startTest() {
		$this->Categoria =& ClassRegistry::init('Categoria');
	}

	function endTest() {
		unset($this->Categoria);
		ClassRegistry::flush();
	}

}
?>
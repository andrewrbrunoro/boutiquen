<?php
/* ProdutosVitrine Fixture generated on: 2011-04-07 16:09:18 : 1302203358 */
class ProdutosVitrineFixture extends CakeTestFixture {
	var $name = 'ProdutosVitrine';

	var $fields = array(
		'produto_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'vitrine_nome_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'indexes' => array('PRIMARY' => array('column' => array('produto_id', 'vitrine_nome_id'), 'unique' => 1), 'fk_produto_has_vitrine_nome_vitrine_nome1' => array('column' => 'vitrine_nome_id', 'unique' => 0), 'fk_produto_has_vitrine_nome_produto1' => array('column' => 'produto_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'produto_id' => 1,
			'vitrine_nome_id' => 1
		),
	);
}
?>
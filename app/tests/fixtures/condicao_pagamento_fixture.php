<?php
/* CondicaoPagamento Fixture generated on: 2011-04-07 16:16:38 : 1302203798 */
class CondicaoPagamentoFixture extends CakeTestFixture {
	var $name = 'CondicaoPagamento';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'pagamento_tipo_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'desconto' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'descricao' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'nome' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'sigla' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'parcelas' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'apartir_de' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'juros_de' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'frete_gratis' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'status' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'data_criacao' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'data_modificacao' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'usuario_criacao' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'usuario_modificacao' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_pagamento_pagamento_tipo1' => array('column' => 'pagamento_tipo_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'pagamento_tipo_id' => 1,
			'desconto' => 1,
			'descricao' => 'Lorem ipsum dolor sit amet',
			'nome' => 'Lorem ipsum dolor sit amet',
			'sigla' => 'Lorem ipsum dolor sit amet',
			'parcelas' => 1,
			'apartir_de' => 1,
			'juros_de' => 1,
			'frete_gratis' => 1,
			'status' => 1,
			'data_criacao' => '2011-04-07 16:16:38',
			'data_modificacao' => '2011-04-07 16:16:38',
			'usuario_criacao' => 1,
			'usuario_modificacao' => 1
		),
	);
}
?>
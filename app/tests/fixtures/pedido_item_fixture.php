<?php
/* PedidoItem Fixture generated on: 2011-04-07 16:36:23 : 1302204983 */
class PedidoItemFixture extends CakeTestFixture {
	var $name = 'PedidoItem';

	var $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'primary'),
		'pedido_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'key' => 'index'),
		'nome' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'descricao' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'preco' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'preco_promocao' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2'),
		'codigo' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'codigo_externo' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 45, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'quantidade' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'peso' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 45, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'garantia' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'agrupador' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'agrupador_variacao' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'tag' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'departamento_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'subcategoria_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'categoria_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'fabricante_id' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'status' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 4),
		'data_criacao' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'data_modificacao' => array('type' => 'datetime', 'null' => true, 'default' => NULL),
		'usuario_criacao' => array('type' => 'integer', 'null' => true, 'default' => NULL),
		'usuario_modificacao' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 45, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'fk_pedido_item_pedido1' => array('column' => 'pedido_id', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	var $records = array(
		array(
			'id' => 1,
			'pedido_id' => 1,
			'nome' => 'Lorem ipsum dolor sit amet',
			'descricao' => 'Lorem ipsum dolor sit amet',
			'preco' => 1,
			'preco_promocao' => 1,
			'codigo' => 'Lorem ipsum dolor sit amet',
			'codigo_externo' => 'Lorem ipsum dolor sit amet',
			'quantidade' => 1,
			'peso' => 'Lorem ipsum dolor sit amet',
			'garantia' => 'Lorem ipsum dolor sit amet',
			'agrupador' => 'Lorem ipsum dolor sit amet',
			'agrupador_variacao' => 'Lorem ipsum dolor sit amet',
			'tag' => 'Lorem ipsum dolor sit amet',
			'departamento_id' => 1,
			'subcategoria_id' => 1,
			'categoria_id' => 1,
			'fabricante_id' => 1,
			'status' => 1,
			'data_criacao' => '2011-04-07 16:36:23',
			'data_modificacao' => '2011-04-07 16:36:23',
			'usuario_criacao' => 1,
			'usuario_modificacao' => 'Lorem ipsum dolor sit amet'
		),
	);
}
?>
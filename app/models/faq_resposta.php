<?php
class FaqResposta extends AppModel {
	var $name = 'FaqResposta';
         public $actsAs =  array('Containable');
	var $validate = array(
		'texto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'faq_pergunta_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'FaqPergunta' => array(
			'className' => 'FaqPergunta',
			'foreignKey' => 'faq_pergunta_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>
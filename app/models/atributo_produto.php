<?php

class AtributoProduto extends AppModel {

    public $actsAs = array('Cached', 'Containable');
    var $name = 'AtributoProduto';
    var $useTable = 'atributos_produtos';
    var $primaryKey = 'atributo_id';
    var $belongsTo = array(
        'Atributo' => array(
            'className' => 'Atributo',
            'foreignKey' => 'atributo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function salvar($data, $add = false) {
        if(isset($data['Produto']['id']) && $data['Produto']['id'] != null || $add == false){
            $this->deleteAll(array('produto_id' => $data['Produto']['id']), false);
        }
        $this->primaryKey = '';
        $array_atributos['Atributos'] = array();
        foreach ($data[$this->alias] as $xx => $valor):
            $valor['id'] = '';
            if (isset($valor['delete']) && $valor['delete'] == true) {
                $array_atributos['Atributos'][$xx]['id'] = $valor['atributo_id'];
                $array_atributos['Atributos'][$xx]['save'] = false;
                continue;
            } elseif (isset($valor['atributo_id']) && $valor['atributo_id'] != "") {
                if ($this->find('count', array('recursive' => -1, 'conditions' => array('AND' => array('AtributoProduto.atributo_id' => $valor['atributo_id'], 'AtributoProduto.produto_id' => $data['Produto']['id'])))) > 0) {
                    continue;
                } else {
                    $this->query("INSERT INTO atributos_produtos (atributo_id, produto_id) VALUES ('".(int)$valor['atributo_id']."', '".(int)$valor['produto_id']."')");
                    //$this->save($valor);
                    $array_atributos['Atributos'][$xx]['id'] = $valor['atributo_id'];
                    $array_atributos['Atributos'][$xx]['save'] = true;
                }
            }
        endForeach;
        return $array_atributos;
    }

}
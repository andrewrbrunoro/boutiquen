<?php

    class Grade extends AppModel
    {

        var $name = 'Grade';

        var $useTable = 'grades';

        var $virtualFields = array(
            'free_shipping' => 'SELECT FreteGratis.apartir_de > Grade.preco FROM frete_gratis AS FreteGratis'
        );

        var $hasMany = array(
            'GradeImagem' => array(
                'className'  => 'GradeImagem',
                'foreignKey' => 'grade_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            )
        );


        public function alteraEstoque($id, $quantidade)
        {
            $this->recursive = -1;
            $grid = $this->findById($id);
            $this->save(array('Grade' => array('id' => $id, 'quantidade_disponivel' => $grid['Grade']['quantidade_disponivel'] - $quantidade)));
        }

        function getLastInsertId($db_connection)
        {
            $result = 0;

            if ($query_result = mysql_query("SELECT LAST_INSERT_ID();", $db_connection)) {
                $temp = mysql_fetch_row($query_result);
                $result = $temp[0];
            }

            return $result;
        }

    }
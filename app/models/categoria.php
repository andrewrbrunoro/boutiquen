<?php

    class Categoria extends AppModel
    {

        public $hasMany = array(
            'ProdutoCategoria' => array(
                'className'  => 'ProdutoCategoria',
                'foreignKey' => 'produto_id'
            ),
            'SubCategory'      => array(
                'className'  => 'Categoria',
                'order'      => 'lft',
                'foreignKey' => 'parent_id',
                'dependent'  => true,
                'conditions' => array(
                    'SubCategory.status'     => true,
                    'SubCategory.seo_url !=' => ''
                )
            )
        );

        public $belongsTo = array(
            'FatherCategory' => array(
                'className'  => 'Categoria',
                'foreignKey' => 'parent_id',
                'dependent'  => true
            )
        );

        public function getCategories()
        {
            $query = $this->find('all',
                array(
                    'contain'    => array(
                        'SubCategory'
                    ),
                    'conditions' => array(
                        'Categoria.status'    => true,
                        'Categoria.parent_id' => 0
                    )
                )
            );
            return $query;
        }

    }
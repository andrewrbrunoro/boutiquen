<?php

class FreteGratis extends AppModel {

    var $name = 'FreteGratis';
    var $useTable = 'frete_gratis';
    var $actsAs = array('Cached', 'Containable');
    var $validate = array(
        'status' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'nome' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            ),
            'unique' => array(
                'rule' => array('checkUnique', 'nome'),
                'message' => 'Regra já adicionada, tente outro nome.'
            ),
        ), 'codigo' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'label' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'data_inicio' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'data_fim' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'categoria_id' => array(
            'noempty' => array(
                'rule' => array('validaCategoria'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'produto_id' => array(
            'noempty' => array(
                'rule' => array('validaProduto'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'grade_id' => array(
            'noempty' => array(
                'rule' => array('validaGrade'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'apartir_de' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'allowEmpty' => false,
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'cep_inicial' => array(
            'valid1' => array(
                'rule' => array('validaCep'),
                'message' => 'O cep deve conter 8 números.'
            ), 'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'cep_final' => array(
            'valid2' => array(
                'rule' => array('validaCepFinal'),
                'message' => 'O cep final deve ser maior que o inicial.'
            ), 'valid1' => array(
                'rule' => array('validaCep'),
                'message' => 'O cep deve conter 8 números.'
            ), 'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        )
    );

    public function afterFind($results, $primary = false) {
        if ($this->isCount($results))
            return parent::afterFind($results, $primary);

        App::import("helper", "String");
        $string = new StringHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        if (!empty($results)) {
            foreach ($results as $k => &$r) {

                if (isset($r[$this->alias]) && isset($r[$this->alias]['data_inicio'])) {
                    $r[$this->alias]['data_inicio'] = $this->Calendario->dataFormatada("d/m/Y", $r[$this->alias]['data_inicio']);
                }
                if (isset($r[$this->alias]) && isset($r[$this->alias]['data_fim'])) {
                    $r[$this->alias]['data_fim'] = $this->Calendario->dataFormatada("d/m/Y", $r[$this->alias]['data_fim']);
                }
            }
        }

        return parent::afterFind($results, $primary);
    }

    public function beforeSave() {


        App::import("helper", "String");
        $string = new StringHelper();
        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        if (isset($this->data[$this->alias]['apartir_de'])) {
            $this->data[$this->alias]['apartir_de'] = $string->moedaToBco($this->data[$this->alias]['apartir_de']);
        }
        if (isset($this->data[$this->alias]['produto_id']) && !empty($this->data[$this->alias]['produto_id'])) {
            $this->data[$this->alias]['produto_id'] = json_encode($this->data[$this->alias]['produto_id']);
        } else {
            $this->data[$this->alias]['produto_id'] = '';
        }
        if (isset($this->data[$this->alias]['grade_id']) && !empty($this->data[$this->alias]['grade_id'])) {
            $this->data[$this->alias]['grade_id'] = json_encode($this->data[$this->alias]['grade_id']);
        } else {
            $this->data[$this->alias]['grade_id'] = '';
        }
        if (isset($this->data[$this->alias]['cep_inicial']) && !empty($this->data[$this->alias]['cep_inicial'])) {
            $this->data[$this->alias]['cep_inicial'] = preg_replace('/[^0-9]+/', '', $this->data[$this->alias]['cep_inicial']);
        }
        if (isset($this->data[$this->alias]['cep_final']) && !empty($this->data[$this->alias]['cep_final'])) {
            $this->data[$this->alias]['cep_final'] = preg_replace('/[^0-9]+/', '', $this->data[$this->alias]['cep_final']);
        }

        if (isset($this->data[$this->alias]['data_inicio']) && $this->data[$this->alias]['data_inicio'] != "") {
            $this->data[$this->alias]['data_inicio'] = $this->Calendario->dataFormatada("Y-m-d 00:00:00", $this->data[$this->alias]['data_inicio']);
        }
        if (isset($this->data[$this->alias]['data_fim']) && $this->data[$this->alias]['data_fim'] != "") {
            $this->data[$this->alias]['data_fim'] = $this->Calendario->dataFormatada("Y-m-d 23:59:59", $this->data[$this->alias]['data_fim']);
        }
        if (isset($this->data[$this->alias]['categoria_id']) && !empty($this->data[$this->alias]['categoria_id'])) {
            foreach ($this->data[$this->alias]['categoria_id'] as $categoria_id):
                App::import("model", "Categoria");
                $this->Categoria = new Categoria();
                $filhos = $this->Categoria->children($categoria_id);
                $ids[] = am(set::extract('/Categoria/id', $filhos), $categoria_id);
            endForeach;

            $this->data[$this->alias]['categoria_id'] = json_encode($ids);
        }else {
            $this->data[$this->alias]['categoria_id'] = '';
        }
        return parent::beforeSave();
    }

    function validaCepFinal() {

        if ((int) preg_replace('/[^0-9]+/', '', $this->data[$this->alias]['cep_final']) < (int) preg_replace('/[^0-9]+/', '', $this->data[$this->alias]['cep_inicial'])) {
            return false;
        }
        return true;
    }

    function validaCep($cep) {
        if (strlen(current(preg_replace('/[^0-9]+/', '', $cep))) == 8) {
            return true;
        }
        return false;
    }

    function validaPreco($cep) {
        App::import("helper", "String");
        $string = new StringHelper();

        if ($string->moedaToBco($this->data[$this->alias]['preco']) > 0) {
            return true;
        }
        return false;
    }

    function validaCategoria() {
        if (in_array($this->data[$this->alias]['codigo'], array("PRODUTO", "CARRINHO", "GRADE")))
            return true;
        if ($this->data[$this->alias]['produto_id'])
            return true;
        if (is_array($this->data[$this->alias]['categoria_id']) && count($this->data[$this->alias]['categoria_id']) > 0) {
            return true;
        }
        return false;
    }

    function validaProduto() {
        if (in_array($this->data[$this->alias]['codigo'], array("CARRINHO", "CARRINHO", "GRADE")))
            return true;
        if ($this->data[$this->alias]['categoria_id'])
            return true;
        if (is_array($this->data[$this->alias]['produto_id']) && count($this->data[$this->alias]['produto_id']) > 0) {
            return true;
        }
        return false;
    }
    
    function validaGrade() {
        if (in_array($this->data[$this->alias]['codigo'], array("CATEGORIA", "CARRINHO", "PRODUTO", "GRADE")))
            return true;
        if ($this->data[$this->alias]['grade_id'])
            return true;
        if (is_array($this->data[$this->alias]['grade_id']) && count($this->data[$this->alias]['grade_id']) > 0) {
            return true;
        }
        return false;
    }
}

?>
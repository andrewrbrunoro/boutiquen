<?php

    class ProdutoCategoria extends AppModel
    {

        var $primaryKey = 'categoria_id';
        var $name = 'ProdutoCategoria';
        var $useTable = 'produtos_categorias';

        var $belongsTo = array(
            'Categoria' => array(
                'className'  => 'Categoria',
                'foreignKey' => 'categoria_id'
            ),
            'Produto'   => array(
                'className'  => 'Produto',
                'conditions' => array(
                    'ProdutoCategoria.produto_id = Produto.id'
                )
            )
        );

        public function getProductCategory($product_id)
        {
            $query = $this->find('first',
                array(
                    'conditions' => array(
                        'ProdutoCategoria.produto_id' => $product_id
                    )
                )
            );
            return $query;
        }
    }
<?php

    class CategoriaAtributo extends AppModel
    {

        var $name = 'CategoriaAtributo';

        var $useTable = 'categorias_atributos';

        var $belongsTo = array(
            'Categoria' => array(
                'fields'     => array('Categoria.id', 'Categoria.nome'),
                'className'  => 'Categoria',
                'foreignKey' => 'categoria_id'
            )
        );

    }
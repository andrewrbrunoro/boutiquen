<?php
class FaqPergunta extends AppModel {
	var $name = 'FaqPergunta';
         public $actsAs =  array('Containable');
	var $validate = array(
		'texto' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'faq_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Faq' => array(
			'className' => 'Faq',
			'foreignKey' => 'faq_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        var $hasMany = array(
		'FaqResposta' => array(
			'className' => 'FaqResposta',
			'foreignKey' => 'faq_pergunta_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
?>
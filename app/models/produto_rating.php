<?php
    class ProdutoRating extends AppModel
    {
		var $name = 'ProdutoRating';
        public $useTable = 'produto_ratings';
        public $actAs = array("Containable");
		var $validate = array(
			'titulo' => array(
				'noempty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Preenchimento obrigatório.'
				)
			),
			'avaliacao_geral' => array(
				'noempty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Preenchimento obrigatório.'
				)
			),
			'nota' => array(
				'noempty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Você deve definir uma nota para o produto.'
				)
			),
			'localizacao' => array(
				'noempty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Preenchimento obrigatório.'
				)
			),
			'comentario' => array(
				'noempty' => array(
					'rule' => array('notEmpty'),
					'message' => 'Preenchimento obrigatório.'
				)
			)
		);
        public $belongsTo = array(
            'Produto' => array(
                'className' => 'Produto',
                'foreignKey' => 'produto_id'
            ),
			'Usuario' => array(
                'className' => 'Usuario',
                'foreignKey' => 'usuario_id'
            )
        );
		
		public function returnaRatingsProduto( $produto_id ){
			$rating = $this->find('all', array('fields' => 'count(*) as quantidade_notas, sum(nota) as total_notas', 'conditions' => array('produto_id' => $produto_id,'status' => 1), 'recursive' => '-1'));
			
			$quantidade_notas 	= (int)$rating[0][0]['quantidade_notas'];
			$total_notas 		= (int)$rating[0][0]['total_notas'];
			
			if( $quantidade_notas > 0 ):
				$media_notas = (int)( $total_notas / $quantidade_notas );
				return $media_notas;
			else:
				return 0;
			endIf;
				
			
			
		}
    }
?>

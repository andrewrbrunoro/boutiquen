<?php

class ProdutoPreco extends AppModel {

    var $useTable = 'produtos_precos';
   
    var $actsAs = array('Cached', 'Containable');
	
    var $validate = array(
        'status' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
		// 'grupo_id' => array(
            // 'noempty' => array(
                // 'rule' => array('notEmpty'),
                // 'message' => 'Preenchimento obrigatório.'
            // )
        // ),
        'quantidade' => array(
            'noempty' => array(
                'rule' => array('numeric'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        // 'inicio' => array(
            // 'noempty' => array(
                // 'rule' => array('notEmpty'),
                // 'message' => 'Preenchimento obrigatório.'
            // )
        // ),
        // 'fim' => array(
            // 'validaPrecoPromocao' => array(
                // 'rule' => array('notEmpty'),
                // 'message' => 'Preenchimento obrigatório.'

            // )
        // ),
		'preco' => array(
			
            'validaPrecoPromocao' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',

            )
        ),
    );

    var $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	
	 public function beforeSave() {

        App::import("helper", "String");
        $this->String = new StringHelper();
		
        if (isset($this->data[$this->alias]['preco'])) {
            $this->data[$this->alias]['preco'] = $this->String->moedaToBco($this->data[$this->alias]['preco']);
        }
		
        return parent::beforeSave();
    }
	
	public function salvar($data) {
		//debug($data);die;
	    foreach ($data[$this->alias] as $valor):
			//var_dump($valor);
            $valor['produto_id'] = $data['Produto']['id'];
			$valor['inicio'] = "0000-00-00 00:00:00";
			$valor['fim'] = "0000-00-00 00:00:00";
			$valor['grupo_id'] = 1;
            if (isset($valor['delete']) && $valor['delete'] == true) {
                $this->id = $valor['id'];
                $this->delete($valor);
            } elseif (isset($valor['id']) && $valor['id'] != "") {
                $valor['status'] = '1';
                $this->id = $valor['id'];
                $this->save($valor);
            } else {
                $valor['status'] = '1';
                $this->save($valor);
                $this->id = null;
            }
        endForeach;
    }

}

?>
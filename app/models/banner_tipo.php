<?php
class BannerTipo extends AppModel {
	var $name = 'BannerTipo';
         public $actsAs =  array('Containable');

	var $displayField = 'nome';
	var $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'Banner' => array(
			'className' => 'Banner',
			'foreignKey' => 'banner_tipo_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>
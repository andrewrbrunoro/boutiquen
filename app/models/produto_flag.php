<?php

class ProdutoFlag extends AppModel {
    
    public $actsAs =  array('Cached','Containable');
    var $primaryKey = 'flag_id';
    var $name = 'ProdutoFlag';
    var $useTable = 'produtos_flags';
	
	
	 var $belongsTo = array(
	
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
        ),	
	
        'Flag' => array(
            'className' => 'Flag',
            'foreignKey' => 'flag_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );	
}
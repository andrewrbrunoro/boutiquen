<?php
class VariacaoGrupo extends AppModel {

    var $name = 'VariacaoGrupo';
    public $actsAs = array('Containable');
    var $displayField = 'nome';
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
            'unique' => array(
                'rule' => array('checkUnique', 'nome'),
                'message' => 'Atributo já adicionado, tente outro nome.'
            ),
        ),
    );
    var $hasMany = array(
        'VariacaoTipo' => array(
            'className' => 'VariacaoTipo',
            'foreignKey' => 'variacao_grupo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    
     public function afterDelete(){
        App::import('model', 'VariacaoTipo');
        App::import('model', 'Variacao');
        App::import('model', 'VariacaoProduto');
        App::import('model', 'Grade');
        App::import('model', 'GradeImagem');
        
        
        //busco os tipos de variacoes
        $this->VariacaoTipo = new VariacaoTipo();
        $variacoes_tipos = $this->VariacaoTipo->find('list', array(
                                                                    'recursive' => -1, 
                                                                    'fields' => array('VariacaoTipo.id'),
                                                                    'conditions' => array('VariacaoTipo.variacao_grupo_id' => $this->id)
                                                                )
                                                    );
        //busco as variacoes
        $this->Variacao = new Variacao();
        $variacoes = $this->Variacao->find('list', array(
                                                         'recursive' => -1,
                                                         'fields' => array('Variacao.id'),
                                                         'conditions' => array('Variacao.variacao_tipo_id' => $variacoes_tipos)
                                                        )
                                            );        
        //busco as variacoes_produtos
        $this->VariacaoProduto = new VariacaoProduto();
        $this->Grade = new Grade();
        $this->GradeImagem = new GradeImagem();
        $variacao_produto = $this->VariacaoProduto->find('list', array(
                                                                    'recursive' => -1,
                                                                    'fields' => array('VariacaoProduto.grade_id'),
                                                                    'conditions' => array('VariacaoProduto.variacao_id' => $variacoes)
                                                                )
                                                        );
        // Apaga as VariacaoTipo
        if (count($variacoes_tipos) > 0) {
            $this->VariacaoTipo->deleteAll(array('VariacaoTipo.id' => $variacoes_tipos), false);
        }
        // Apaga as Variacao
        if (count($variacoes) > 0) {
            $this->Variacao->deleteAll(array('Variacao.id' => $variacoes), false);
        }
        // Apaga as Grade, GradeImagem, VariacaoProduto
        if (count($variacao_produto) > 0) {
            $this->Grade->deleteAll(array('Grade.id' => $variacao_produto), false);
            $this->GradeImagem->deleteAll(array('GradeImagem.grade_id' => $variacao_produto), false);
            $this->VariacaoProduto->deleteAll(array('VariacaoProduto.grade_id' => $variacao_produto), false);
        }
    }

}
?>
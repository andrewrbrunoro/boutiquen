<?php

class Variacao extends AppModel {

    public $actsAs = array('MeioUpload' => array(
            'thumb_filename' => array(
                'dir' => 'uploads/variacao',
                'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
                'fields' => array(
                    'filesize' => 'thumb_filesize',
                    'mimetype' => 'thumb_mimetype',
                    'dir' => 'thumb_dir'
                )
            )
        ), 'Cached', 'Containable');
    var $name = 'Variacao';
    var $useTable = 'variacoes';
    public $validate = array(
        'valor' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'variacao_tipo_id' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'thumb_filename' => array(
            'Empty' => array('check' => false)
        ),
    );
    var $belongsTo = array(
        'VariacaoTipo' => array(
            'className' => 'VariacaoTipo',
            'foreignKey' => 'variacao_tipo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function beforeSave() {

        App::import('Model', 'Produto');
        $this->Produto = new Produto();

        if (isset($this->data[$this->alias]['produto_id'])) {
            $produto = $this->Produto->find('first', array('fields' => array('Produto.sku'), 'conditions' => array('Produto.id' => $this->data[$this->alias]['produto_id'])));
            $this->data[$this->alias]['sku'] = $produto['Produto']['sku'];
        }

        if (isset($this->data[$this->alias]['valor'])) {
            $this->data[$this->alias]['valor'] = mb_convert_encoding($this->data[$this->alias]['valor'], "auto");
        }

        return parent::beforeSave();
    }

    public function salvar($data) {
        foreach ($data[$this->alias] as $valor):

            if ($valor['variacao_id'] == "")
                continue;

            $valor['produto_id'] = $data['Produto']['id'];
            if (isset($valor['delete']) && $valor['delete'] == true) {
                $this->id = $valor['id'];
                $this->delete($valor);
            } elseif (isset($valor['id']) && $valor['id'] != "") {
                $this->id = $valor['id'];
                $this->save($valor);
            } else {
                $this->save($valor);
                $this->id = null;
            }
        endForeach;
    }
    
    public function sort_variacoes($array, $field = 'nome', $order = SORT_ASC) {
    	
		
		
        $novo_array = array();
        $tmp_array = array();

        App::import('Helper', 'String');
        $this->String = new StringHelper();

        if (is_array($array) && count($array) > 0) {
            
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $field) {
                            $tmp_array[$k] = $this->String->remove_accent($v2);
                        }
                    }
                } else {
                    if ($k == $field) {
                        $tmp_array[$k] = $this->String->remove_accent($v);
                    }
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($tmp_array);
                    break;
                case SORT_DESC:
                    arsort($tmp_array);
                    break;
            }

            foreach ($tmp_array as $k => $v) {
                $novo_array[] = $array[$k];
            }
        }

        return $novo_array;
    }

    public function remover_thumb($id) {
        //$this->id = $id;
        $variacao['id'] = $id;
        $variacao['thumb'] = 0;
        $variacao['thumb_filename'] = ' ';
        $variacao['thumb_dir'] = null;
        $variacao['thumb_mimetype'] = null;
        $variacao['thumb_filesize'] = null;

        if ($this->save($variacao, false))
            return true;
    }

}
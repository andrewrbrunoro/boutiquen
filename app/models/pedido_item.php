<?php

    class PedidoItem extends AppModel
    {


        var $name = 'PedidoItem';
        var $useTable = 'pedido_itens';
        var $uses = array(
            'Produto',
            'Grade'
        );

        var $belongsTo = array(
            'Pedido' => array(
                'className'  => 'Pedido',
                'foreignKey' => 'pedido_id'
            )
        );

        public function saveItems($items, $pedido_id)
        {
            App::import('Model', 'Produto');
            $this->Produto = new Produto();
            foreach ($items->item as $item) {
                $produto = $this->Produto->find('first', array('contain' => array('Grade'), 'conditions' => array('Produto.grade_id' => (int)$item->id)));
                $this->save(
                    array(
                        'PedidoItem' => array(
                            'id'             => null,
                            'pedido_id'      => $pedido_id,
                            'nome'           => $produto['Produto']['nome'],
                            'preco'          => $produto['Grade']['preco'],
                            'preco_promocao' => $produto['Grade']['preco_promocao'],
                            'codigo'         => $produto['Produto']['codigo'],
                            'quantidade'     => (int)$item->quantity,
                            'fabricante_id'  => $produto['Produto']['fabricante_id'],
                            'produto_id'     => $produto['Produto']['id'],
                            'grade_id'       => (int)$item->id,
                            'peso'           => $produto['Produto']['peso'],
                            'profundidade'   => $produto['Produto']['profundidade'],
                            'largura'        => $produto['Produto']['largura'],
                            'altura'         => $produto['Produto']['altura'],
                            'sku'            => $produto['Grade']['sku']
                        )
                    )
                );
            }
        }

    }

<?php
class Tamanho extends AppModel {
	var $name = 'Tamanho';
	public $actsAs =  array('Containable');

	var $displayField = 'nome';
	var $validate = array(
		'nome' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' 	=> 'Campo de preenchimento obrigatório.'
			),
		),
	);
	var $hasMany = array(
		'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'tamanho_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
?>
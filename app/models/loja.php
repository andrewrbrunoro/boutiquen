<?php

class Loja extends AppModel {
    var $name = 'Loja';
    var $useTable = 'lojas';
    public $actsAs =  array('MeioUpload' => array(
								'thumb_filename' => array(
										'dir' => 'uploads/lojas', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'thumb_filesize',
												'mimetype' => 'thumb_mimetype',
												'dir' => 'thumb_dir'
											)
										)
								),'Cached','Containable');
    public $validate = array(
    	'thumb_filename' => array(
            'Empty' => array('check' => false)
        ),
   		'nome' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'rua' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'cidade' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'uf' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),       
    );
}

?>
<?php

    class VariacaoProduto extends AppModel
    {

        var $actsAs = array('Containable');
        var $name = 'VariacaoProduto';
        var $useTable = 'variacoes_produtos';
        var $primaryKey = 'grade_id';

        public $belongsTo = array(
            'Variacao' => array(
                'className'  => 'Variacao',
                'foreignKey' => 'variacao_id'
            ),
            'Grade'    => array(
                'className'  => 'Grade',
                'foreignKey' => 'grade_id'
            )
        );

        public $detail = array(
            'Variacao.id', 'Variacao.status', 'Variacao.valor',
            'VariacaoProduto.*'
        );

        public function getVariationsByGrid($grid_id)
        {
            $query = $this->find('all',
                array(
                    'fields'     => $this->detail,
                    'contain'    => array('Variacao' => 'VariacaoTipo'),
                    'conditions' => array(
                        'VariacaoProduto.grade_id' => $grid_id
                    )
                )
            );
            return $query;
        }

    }
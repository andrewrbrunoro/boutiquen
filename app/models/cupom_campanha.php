<?php

class CupomCampanha extends AppModel {

    var $name = 'CupomCampanha';
    var $useTable = 'cupom_campanhas';
    public $actsAs = array('Containable'); 
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            )
        ),
        'status' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            )
        )
    );

    var $hasMany= array(
        'Cupom'=>array(
              'className' => 'Cupom',
              'foreignKey' => 'cupom_campanha_id',
              'conditions' => '',
              'fields' => '',
              'order' => ''
        )

    );

}

?>
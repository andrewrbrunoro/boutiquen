<?php

class CarrinhoAbandonado extends AppModel {

    var $name = 'CarrinhoAbandonado';
    var $useTable = 'carrinho_abandonado';
    public $actsAs =  array('Cached','Containable');
	// var $validate = array(
		// 'produtos' => array(
            // 'rule' => array('notEmpty'),
            // 'message' => 'Campo de preenchimento obrigatório.'
        // )
    // );
	var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
	
	public function salvar($data, $id = null){
		//itens carrinho
		App::import("component", "Session");
		$this->Session = new SessionComponent();

		$carrinho_abandonado = '';		
		if(isset($data['produtos'])){
			if(is_array($data['produtos']) && $data['produtos'] != null){
				$total = 0;
				foreach($data['produtos'] as $key => $abandonado){
					if($abandonado['produto_id'] != null && $abandonado['quantidade'] != null){
						$carrinho_abandonado[$key]['produto_id'] =  $abandonado['produto_id'];
                                                $carrinho_abandonado[$key]['grade_id']   =  $abandonado['grade_id'];
						$carrinho_abandonado[$key]['quantidade'] =  $abandonado['quantidade'];
						$carrinho_abandonado[$key]['preco'] = $abandonado['preco'];
						$total += $carrinho_abandonado[$key]['preco'];
					}
				}
				$data['produtos'] = json_encode($carrinho_abandonado);
				$data['valor_total'] = $total;
			}else{
				$data['produtos'] = null;
			}
		}
		//debug($data['produtos']);die;
		//seto id se tiver
		if( $id != null && $id > 0 ) 
			$this->id = $id;
		
		$carrinho_abandonado_id = array();

		if($this->save($data)){
			//id
			$carrinho_abandonado_id = $this->id;
			
			//add o key pedidos aabandonados
			$this->Session->write("Carrinho.carrinho_abandonado", $carrinho_abandonado_id);
		}
	}
	
	public function remover($grade_id, $id){
		
		$produtos_carrinho = $this->find('first',array(
													'recursive' 	=> -1,
													'fields'		=> 'CarrinhoAbandonado.produtos',
													'conditions' 	=> array('CarrinhoAbandonado.id' => $id)
													)
										);
		
		$produtos = array();
		if($produtos_carrinho['CarrinhoAbandonado']['produtos'] != ""){
			$produtos =  json_decode($produtos_carrinho['CarrinhoAbandonado']['produtos'], true);
		}
		$data['produtos'] = "";
		foreach($produtos as $prod){
			if($prod['grade_id'] != $grade_id){
				$data['produtos'][] = $prod;
			}
		}
		$this->salvar($data, $id);
	}
	
	public function destroy(){
		// App::import("component", "CarrinhosAbandonados");
		// $this->CarrinhosAbandonados = new CarrinhosAbandonadosComponent();
		
		// $this->CarrinhosAbandonados->destroy();
	}
}

?>
<?php
class FabricanteImagem extends AppModel {
    var $name = 'FabricanteImagem';
    var $actsAs = array('Containable','MeioUpload' => array('filename'));
    var $useTable = 'fabricante_imagens';
	 // public $validate = array(
        // 'filename' => array(
            // 'Empty' => array('check' => false)
        // ),
    // );
    var $belongsTo = array(
		'Fabricante' => array(
			'className' => 'Fabricante',
			'foreignKey' => 'fabricante_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>
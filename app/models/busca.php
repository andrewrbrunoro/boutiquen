<?php

class Busca extends AppModel {

    var $name = 'Busca';
    var $useTable = 'busca';
    var $actsAs = array('Cached', 'Containable');

    public function salvar($dados_busca) {
        $dados = "";
        if ($dados = $this->find('first', array('conditions' => array('Busca.palavra_chave' => $dados_busca['palavra_chave'])))) {
            $save = $dados;
            $save['Busca']['quantidade_busca']++;
            $this->save($save);
        } else {
            $this->save($dados_busca);
        }
    }

}
<?php

class GeranewsImagem extends AppModel {

    var $name = 'GeranewsImagem';
    var $useTable = 'geranews_imagens';
    public $actsAs = array(
        'MeioUpload' => array(
            'filename' => array(
                'dir' => 'uploads/geranews_imagem/filename',
                'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
                'fields' => array(
                    'filesize' => 'filesize',
                    'mimetype' => 'mimetype',
                    'dir' => 'dir'
                )
            )
        ), 'Containable');
    var $belongsTo = array(
        'Geranew' => array(
            'className' => 'Geranew',
            'foreignKey' => 'geranew_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
	

}

?>
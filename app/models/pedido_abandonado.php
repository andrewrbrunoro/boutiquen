<?php

class PedidoAbandonado extends AppModel {

    var $name = 'PedidoAbandonado';
    var $useTable = 'pedido_abandonado';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'produtos' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	var $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
	
	public function salvar($data, $id = null){
		$pedido_abandonado = array();
		foreach($data['produtos'] as $key => $abandonado)
		{
			$abandonado_temp = unserialize($abandonado);
			$pedido_abandonado[$key]['produto_id'] =  $abandonado_temp['produto_id'];
			$pedido_abandonado[$key]['quantidade'] =  $abandonado_temp['quantidade'];
		}		
		$data['produtos'] = json_encode($pedido_abandonado);
		
		if( $id != null && $id > 0 ) 
			$this->id = $id;
		
		$pedido_abandonado_id = array();
		if($this->save($data)){
			//id
			$pedido_abandonado_id = $this->id;
			
			//itens carrinho
			App::import("component", "Session");
			$this->Session = new SessionComponent();
			
			//add o key pedidos aabandonados
			$this->Session->write("Carrinho.pedido_abandonado", $pedido_abandonado_id);
		}
	}
	
	public function remover($id = null){
		$this->delete($id);
	}
}

?>
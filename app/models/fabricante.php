<?php

    class Fabricante extends AppModel
    {
        var $name = 'Fabricante';
        var $actsAs = array('Cached', 'Containable');
        var $displayField = 'nome';
        var $validate = array(
            'nome' => array(
                'rule'    => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.'
            )
        );

        public $belongsTo = array(
            'Imagem' => array(
                'className'  => 'FabricanteImagem',
                'dependent'  => false,
                'foreignKey' => '',
                'conditions' => array(
                    'Imagem.fabricante_id = Fabricante.id'
                ),
                'fields'     => '',
                'order'      => array('Imagem.created ASC')
            )
        );

        public $hasMany = array(
            'FabricanteImagem' => array(
                'className'  => 'FabricanteImagem',
                'foreignKey' => 'fabricante_id',
                'dependent'  => false,
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            )
        );

        public function beforeSave()
        {
            if (isset($this->data[$this->alias]['nome']) && $this->data[$this->alias]['nome'] != "") {
                $this->data[$this->alias]['seo_url'] = '/' . low(Inflector::slug($this->data[$this->alias]['nome'], "-"));
            }
            return parent::beforeSave();
        }

        public function getManufacturers()
        {
            return $this->find('all',
                array(
                    'fields'     => array('Fabricante.*', 'Imagem.*'),
                    'conditions' => array(
                        'Fabricante.status'     => true,
                        'Fabricante.seo_url !=' => '',
                        'Imagem.filename !='    => ''
                    )
                )
            );
        }


    }
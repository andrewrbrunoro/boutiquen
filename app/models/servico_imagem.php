<?php
class ServicoImagem extends AppModel {
    var $name = 'ServicoImagem';
    var $actsAs = array('Containable','MeioUpload' => array('filename'));
    var $useTable = 'servico_imagens';
    var $belongsTo = array(
		'Servico' => array(
			'className' => 'Servico',
			'foreignKey' => 'servico_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
?>
<?php

class Frete extends AppModel {

	const RETIRAR_NA_LOJA = "RETIRAR_LOJA";    
    var $name = 'Frete';
    var $useTable = 'fretes';
    var $actsAs = array('Cached', 'Containable');
    var $belongsTo = array(
        'FreteTipo' => array(
            'className' => 'FreteTipo',
            'foreignKey' => 'frete_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $validate = array(
        'status' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'peso' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'prazo' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'frete_tipo_id' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'preco' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            ),
            'valor' => array(
                'rule' => array('validaPreco'),
                'message' => 'O Preço deve ser maior que R$ 0,00'
            ),
        ),
        'cep_inicial' => array(
            'valid1' => array(
                'rule' => array('validaCep'),
                'message' => 'O cep deve conter 8 números.'
            ), 'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'cep_final' => array(
            'valid2' => array(
                'rule' => array('validaCepFinal'),
                'message' => 'O cep final deve ser maior que o inicial.'
            ), 'valid1' => array(
                'rule' => array('validaCep'),
                'message' => 'O cep deve conter 8 números.'
            ), 'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        )
    );

    public function beforeSave() {

        App::import("helper", "String");
        $string = new StringHelper();
        if (isset($this->data[$this->alias]['preco'])) {
            $this->data[$this->alias]['preco'] = $string->moedaToBco($this->data[$this->alias]['preco']);
        }
        if (isset($this->data[$this->alias]['apartir_de'])) {
            $this->data[$this->alias]['apartir_de'] = $string->moedaToBco($this->data[$this->alias]['apartir_de']);
        }
        return parent::beforeSave();
    }

    function validaCepFinal() {
        if ($this->data['Frete']['cep_final'] < $this->data['Frete']['cep_inicial']) {
            return false;
        }
        return true;
    }

    function validaCep($cep) {
        if (strlen(current($cep)) == 8) {
            return true;
        }
        return false;
    }

    function validaPreco($cep) {
        App::import("helper", "String");
        $string = new StringHelper();
        App::import("helper", "String");
        $string = new StringHelper();

        if ($string->moedaToBco($this->data[$this->alias]['preco']) > 0) {
            return true;
        }
        return false;
    }
	
	// metodo que identifica se o tipo de frete é passado é o de 'retirar na loja'
	public function retirarNaLoja($id){
		App::import('Model','Frete');
		$this->Frete = new Frete();
		
		$frete = $this->Frete->find('first', array('conditions' => array('Frete.id' => $id)));
		
		if($frete['FreteTipo']['nome'] == self::RETIRAR_NA_LOJA){
			return true;
		}else{
			return false;
		}
	}

}
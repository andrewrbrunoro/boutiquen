<?php

class Geranew extends AppModel {

    var $name = 'Geranew';
    var $useTable = 'geranews';
    public $actsAs =  array('Cached','Containable');
	
	var $validate = array(
        'nome' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            ),
        ),
    );
	

}

?>
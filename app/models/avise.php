<?php

class Avise extends AppModel {

    var $name = 'Avise';
    var $useTable = 'avise';
    var $actsAs = array('Containable');
    var $validate = array(
        'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.',
            'required' => true,
        ),
        'produto_id' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.',
            'required' => true,
        ),
        'email' => array(
            'valido' => array(
                'rule' => array('email'),
                'message' => 'Informe um email válido, ex: example@example.com.br'
            ),
            'obrigatorio' => array(
                'rule' => array('notEmpty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true,
            )
        )
    );
    var $belongsTo = array(
        'Produto' => array(
            'className' => 'Produto',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}

?>
<?php

    class Usuario extends AppModel
    {

        var $actsAs = array('Acl' => 'requester');
        var $name = 'Usuario';
        var $useTable = 'usuarios';

        var $validate = array(
            'cpf'        => array(
                'unico'      => array(
                    'rule'    => array('isUnique'),
                    'message' => 'CPF já está cadastrado.',
                ),
                'preenchido' => array(
                    'rule'    => array('notEmpty'),
                    'message' => 'Campo de preenchimento obrigatório'
                )
            ),
            'telefone'   => array(
                'preenchido1' => array(
                    'rule'    => array('notEmpty'),
                    'message' => 'Campo de preenchimento obrigatório'
                )
            ),
            'nome'       => array(
                'valido'        => array(
                    'rule'    => array('custom', '/[A-Za-z ] [A-Za-z ]/'),
                    'message' => 'O nome deve ser composto'
                ), 'preenchido' => array(
                    'rule'    => array('notEmpty'),
                    'message' => 'Campo de preenchimento obrigatório'
                )
            ),
            'email'      => array(
                'valid'    => array(
                    'rule'    => array('email'),
                    'message' => 'O email informado não é válido, tente o formado teste@teste.com'
                ),
                'notempty' => array(
                    'rule'    => array('notEmpty'),
                    'message' => 'Campo de preenchimento obrigatório.'
                ), 'unico' => array(
                    'rule'    => array('isUnique'),
                    'message' => 'E-mail já está cadastrado.',
                    'on'      => 'create'
                ),
            ),
            'senha_nova' => array(
                'confirm'      => array(
                    'rule'    => array('validaRepetirSenha'),
                    'message' => 'As senhas não conferem'
                ),
                'minimoUpdate' => array(
                    'rule'       => array('between', 6, 20),
                    'message'    => 'A senha deve ter de 6 à  20 caracteres',
                    'allowEmpty' => true,
                    'on'         => 'update'
                ),
                'minimo'       => array(
                    'rule'    => array('between', 6, 20),
                    'message' => 'A senha deve ter 6 à 20 caracteres',
                ),
                'preenchido'   => array(
                    'rule'     => 'notEmpty',
                    'message'  => 'Campo de preenchimento obrigatório',
                    'required' => true,
                    'on'       => 'create'
                )
            )
        );

        var $hasMany = array(
            'UsuarioEndereco' => array(
                'className'  => 'UsuarioEndereco',
                'foreignKey' => 'usuario_id',
                'dependent'  => false,
                'conditions' => ''
            )
        );

        public function changeDataSource($newSource)
        {
            parent::setDataSource($newSource);
            parent::__construct();
        }

        /**
         * @return array|bool|null
         * Obrigatório para o ACL
         */
        public function parentNode()
        {
            if (!isset($data['Usuario']['grupo_id'])
            ) return true;

            if (!$this->id && empty($this->data)) {
                return null;
            }
            $data = $this->data;
            if (empty($this->data)) {
                $data = $this->read();
            }
            if (!$data['Usuario']['grupo_id']) {
                return null;
            } else {
                return array('Grupo' => array('id' => $data['Usuario']['grupo_id']));
            }
        }

        /**
         * @param bool $created
         * @return bool
         * Obrigatório para o ACL
         */
        function afterSave($created)
        {
            if (!isset($this->data['Usuario']['email'])
            ) return true;
            $parent = $this->parentNode();
            $parent = $this->node($parent);
            $node = $this->node();
            $aro = $node[0];
            $aro['Aro']['parent_id'] = $parent[0]['Aro']['id'];
            $aro['Aro']['alias'] = $this->data['Usuario']['email'];
            $this->Aro->save($aro);
        }

        /**
         * @param $user_id
         * @return array
         *
         * Retorna o usuário e os endereços relacionados
         */
        public function getUsuarioEndereco($user_id)
        {
            $query = $this->find('first',
                array(
                    'conditions' => array(
                        'Usuario.id' => $user_id
                    )
                )
            );
            $nascimento = explode('-', $query['Usuario']['data_nascimento']);
            $query['Usuario']['dia'] = $nascimento[2];
            $query['Usuario']['mes'] = $nascimento[1];
            $query['Usuario']['ano'] = $nascimento[0];
            return $query;
        }

        /**
         * Pega os dados do usuário direto pelo banco e não da sessão
         */
        public function getRealTimeUser($user_id)
        {
            return $this->find('first',
                array(
                    'conditions' => array(
                        'Usuario.id' => $user_id
                    )
                )
            );
        }

        /**
         * @return bool
         *
         * Validar a senha feita pelo cadastro
         */
        public function validaRepetirSenha()
        {
            if (isset($this->data[$this->name]['senha_nova']) || isset($this->data[$this->name]['confirm']))
                return (@$this->data[$this->name]['senha_nova'] == @$this->data[$this->name]['confirm']);
            else
                return true;
        }

    }
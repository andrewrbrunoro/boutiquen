<?php

class Flag extends AppModel {

    public $actsAs = array(
        'MeioUpload' => array(
            'thumb_filename' => array(
                'dir' => 'uploads/flag/thumb',
                'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
                'fields' => array(
                    'filesize' => 'thumb_filesize',
                    'mimetype' => 'thumb_mimetype',
                    'dir' => 'thumb_dir'
                )
            )
        ), 'Tree', 'Containable');
    public $validate = array(
        'nome' => array(
            'notEmpty' => array(
                'required' => true,
                'rule' => 'notEmpty',
                'message' => 'Campo de preenchimento obrigatório',
            ),
        ),
        'thumb_filename' => array(
            'Empty' => array('check' => false)
        ),
    );
    public $hasMany = array(
        'ProdutoFlag' => array(
            'className' => 'ProdutoFlag',
            'foreignKey' => 'produto_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
/*
    public function remover_thumb($id) {
        //$this->id = $id;
        $data['id'] = $id;
        $data['thumb'] = 0;
        $data['thumb_filename'] = ' ';
        $data['thumb_dir'] = null;
        $data['thumb_mimetype'] = null;
        $data['thumb_filesize'] = null;

        if ($this->save($data, false))
            return true;
    }
*/
}

?>
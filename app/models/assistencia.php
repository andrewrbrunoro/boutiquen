<?php

class Assistencia extends AppModel {

    var $name = 'Assistencia';
    var $useTable = 'assistencias';
    public $actsAs =  array('Cached','Containable');
	var $validate = array(
		'marca' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'empresa' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'email' => array(
            'valido' => array(
                'rule' => array('email'),
                'message' => 'Informe um email válido, ex: example@example.com.br'
            )
        )
    );
}

?>
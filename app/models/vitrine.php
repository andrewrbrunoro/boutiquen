<?php

    class Vitrine extends AppModel
    {

        var $useTable = 'vitrines';

        var $virtualFields = array(
            'count_products' => 'SELECT COUNT(*) FROM produtos_vitrines AS ProdutoVitrine WHERE ProdutoVitrine.vitrine_id = Vitrine.id'
        );

        public $customFields = array(
            'Vitrine.nome', 'Vitrine.quantidade_exibida', 'Vitrine.status', 'Vitrine.ordem'
        );

        var $hasMany = array(
            'ProdutoVitrine' => array(
                'className'  => 'ProdutoVitrine',
                'foreignKey' => 'vitrine_id',
                'fields'     => array('ProdutoVitrine.produto_id'),
                'order'      => array('ProdutoVitrine.ordem ASC')
            )
        );

        public function getAllShowcases($type = 'NORMAL')
        {
            return $this->find('all',
                array(
                    'fields'     => $this->customFields,
                    'conditions' => array(
                        'Vitrine.tipo' => $type
                    ),
                    'order' => array('Vitrine.ordem ASC')
                )
            );
        }

    }
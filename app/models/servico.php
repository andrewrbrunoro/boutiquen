<?php

class Servico extends AppModel {

    var $name = 'Servico';
    var $actsAs = array('Cached', 'Containable');
    var $displayField = 'nome';
    var $order = 'id ASC';
    var $validate = array(
        'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'descricao' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
    var $hasMany = array(
        'ServicoImagem' => array(
            'className' => 'ServicoImagem',
            'foreignKey' => 'servico_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    public function beforeSave() {
        debug($this->data);
        if (!empty($this->data[$this->alias]['nome'])) {
            $this->data[$this->alias]['seo_url'] = low(Inflector::slug($this->data[$this->alias]['nome'], '-'));;
        }
        return parent::beforeSave();
    }
}

?>
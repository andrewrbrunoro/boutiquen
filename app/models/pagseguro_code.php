<?php

    class PagseguroCode extends AppModel {

        var $name = 'PagseguroCode';

        var $useTable = 'pagseguro_codes';

        var $primaryKey = 'codigo';

    }
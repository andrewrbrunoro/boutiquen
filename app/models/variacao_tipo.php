<?php

class VariacaoTipo extends AppModel {

    var $name = 'VariacaoTipo';
    public $actsAs = array('Containable');
    var $displayField = 'nome';
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        
//            'unique' => array(
//                'rule' => array('checkUnique', 'nome'),
//                'message' => 'Atributo já adicionado, tente outro nome.'
//            ),
        ),
        'ordem' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        ),
        'variacao_grupo_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        )
    );
    var $hasMany = array(
        'Variacao' => array(
            'className' => 'Variacao',
            'foreignKey' => 'variacao_tipo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
//        'Variacao' => array(
//            'className' => 'VariacaoProduto',
//            'foreignKey' => 'variacao_id',
//            'dependent' => false,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
    );
    
    var $belongsTo = array(
        'VariacaoGrupo' => array(
            'className' => 'VariacaoGrupo',
            'foreignKey' => 'variacao_grupo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

}

?>
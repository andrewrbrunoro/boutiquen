<?php

    class Produto extends AppModel
    {
        var $actsAs = array('Containable');
        var $useTable = 'produtos';

        var $uses = array(
            'VariacaoProduto',
            'VariacaoTipo'
        );

        var $virtualFields = array(
            'count_image' => 'SELECT COUNT(*) FROM grade_imagens AS GradeCount WHERE GradeCount.grade_id = Produto.grade_id'
        );

        public $detail = array(
            'Produto.id', 'Produto.variacao_grupo_id', 'Produto.nome', 'Produto.grade_id', 'Produto.sku', 'Produto.referencia', 'Produto.codigo', 'Produto.seo_institucional', 'Produto.seo_meta_description',
            'Produto.seo_meta_keywords', 'Produto.seo_title', 'Produto.marca', 'Produto.grade_id', 'Produto.tag', 'Grade.preco', 'Produto.grades', 'Produto.tag',
            'Grade.preco_promocao', 'Grade.preco_promocao_fim', 'Grade.preco_promocao_inicio', 'Grade.quantidade', 'Produto.descricao', 'Produto.descricao_resumida',
            '(SELECT FreteGratis.apartir_de < Grade.preco FROM frete_gratis AS FreteGratis) AS free_shipping'
        );

        public $showcase = array(
            'Produto.nome', 'Produto.grade_id', 'Produto.sku', 'Produto.referencia', 'Produto.codigo', 'Produto.seo_institucional', 'Produto.seo_meta_description',
            'Produto.seo_meta_keywords', 'Produto.seo_title', 'Produto.marca', 'Produto.count_image', 'Produto.grade_id', 'Produto.tag', 'Grade.preco',
            'Grade.preco_promocao', 'Grade.preco_promocao_fim', 'Grade.preco_promocao_inicio',
            '(SELECT FreteGratis.apartir_de < Grade.preco FROM frete_gratis AS FreteGratis) AS free_shipping'
        );

        var $belongsTo = array(
            'Grade' => array(
                'className'  => 'Grade',
                'foreignKey' => 'grade_id'
            )
        );

        public $hasMany = array(
            'VariacaoProduto' => array(
                'className'  => 'VariacaoProduto',
                'foreignKey' => 'produto_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            ),
        );

        public function getDetail($grade_id)
        {
            $query = '
            SELECT ' . implode(',', $this->detail) . ' FROM grades AS Grade
            LEFT OUTER JOIN variacoes_produtos AS VariacaoProduto ON VariacaoProduto.grade_id = Grade.id
            LEFT OUTER JOIN produtos AS Produto ON Produto.id = VariacaoProduto.produto_id
            WHERE Grade.id = "' . $grade_id . '" LIMIT 1
            ';
            $query = $this->query($query);
            return end($query);
        }

        private function getGridsByColor($produto_id, $variacao_id)
        {
            return $this->VariacaoProduto->find('list',
                array(
                    'fields'     => array('VariacaoProduto.grade_id'),
                    'conditions' => array(
                        'VariacaoProduto.produto_id'  => $produto_id,
                        'VariacaoProduto.variacao_id' => $variacao_id
                    )
                )
            );
        }

        public function getColors($product)
        {
            $query = $this->VariacaoProduto->find('all',
                array(
                    'fields'     => array('Variacao.valor', 'Variacao.id', 'VariacaoProduto.grade_id'),
                    'conditions' => array(
                        'Variacao.variacao_tipo_id'  => 1,
                        'VariacaoProduto.produto_id' => $product['Produto']['id']
                    ),
                    'group'      => array('Variacao.valor')
                )
            );
            return $query;
        }

        public function getCurrentVariation($grade_id)
        {
            $query = $this->VariacaoProduto->find('all',
                array(
                    'fields'     => array('VariacaoProduto.variacao_id'),
                    'recursive'  => -1,
                    'conditions' => array(
                        'VariacaoProduto.grade_id' => $grade_id
                    )
                )
            );
            foreach ($query as $item) {
                $return[] = $item['VariacaoProduto']['variacao_id'];
            }
            if (isset($return))
                return $return;
        }

        public function getSizesByColor($product, $grade_id)
        {
            $cor = $this->VariacaoProduto->find('first',
                array(
                    'conditions' => array(
                        'Variacao.variacao_tipo_id' => 1,
                        'VariacaoProduto.grade_id'  => $grade_id
                    )
                )
            );
            $grids = $this->getGridsByColor($product['Produto']['id'], $cor['VariacaoProduto']['variacao_id']);
            $sizes = $this->VariacaoProduto->find('all',
                array(
                    'fields'     => array('VariacaoProduto.grade_id', 'VariacaoProduto.variacao_id', 'VariacaoProduto.produto_id', 'Variacao.valor', 'Grade.quantidade_disponivel'),
                    'conditions' => array(
                        'VariacaoProduto.variacao_id !=' => $cor['VariacaoProduto']['variacao_id'],
                        'VariacaoProduto.grade_id'       => $grids
                    )
                )
            );
            return $sizes;
        }

        public function getProducts($ids)
        {
            $query = $this->find('all',
                array(
                    'fields'     => $this->showcase,
                    'conditions' => array(
                        'Produto.id'     => $ids
                    )
                )
            );
            return $query;
        }

        public function getMinValue(array $ids = array())
        {
            $this->recursive = -1;
            if (count($ids)) {
                $min = $this->find('first',
                    array(
                        'fields'     => array('MIN(Produto.preco) as MinValue'),
                        'conditions' => array(
                            'Produto.id'      => $ids,
                            'Produto.preco >' => 10,
                            'Produto.status'  => true
                        )
                    )
                );
            } else {
                $min = $this->find('first',
                    array(
                        'fields'     => array('MIN(Produto.preco) as MinValue'),
                        'conditions' => array(
                            'Produto.preco >' => 10,
                            'Produto.status'  => true
                        )
                    )
                );
            }
            return isset($min[0]['MinValue']) ? $min[0]['MinValue'] : 0.00;
        }

        public function getMaxValue(array $ids = array())
        {
            $this->recursive = -1;
            if (count($ids)) {
                $max = $this->find('first',
                    array(
                        'fields'     => array('MAX(Produto.preco) as MinValue'),
                        'conditions' => array(
                            'Produto.id'      => $ids,
                            'Produto.preco >' => 10,
                            'Produto.status'  => true
                        )
                    )
                );
            } else {
                $max = $this->find('first',
                    array(
                        'fields'     => array('MAX(Produto.preco) as MinValue'),
                        'conditions' => array(
                            'Produto.preco >'  => 10,
                            'Produto.preco !=' => 0,
                            'Produto.status'   => true
                        )
                    )
                );
            }
            return isset($max[0]['MinValue']) ? $max[0]['MinValue'] : 0.00;
        }

    }

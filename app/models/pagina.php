<?php

class Pagina extends AppModel {

    var $name = 'Pagina';
    var $useTable = 'paginas';
    public $actsAs =  array('Cached','Containable');
	
	var $validate = array(
        'nome' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigat�rio.'
            ),
        ),
		'categoria' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigat�rio.'
            ),
        ),
		'dinamico' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigat�rio.'
            ),
        ),
		'pagina_externa' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigat�rio.'
            ),
        ),
    );
	
	public function beforeSave() {		
		if (isset($this->data[$this->alias]['pagina_externa_url'])) {
            $this->data[$this->alias]['url'] = $this->data[$this->alias]['pagina_externa_url'];
        }
		return parent::beforeSave();
	}

    /**
     * @return array
     *
     * Retorna todas as páginas do usuários que estiverem com status ativo
     */
    public function getPages()
    {
        return $this->find('all',
            array(
                'recursive' => -1,
                'conditions' => array(
                    'Pagina.status' => true
                )
            )
        );
    }

}
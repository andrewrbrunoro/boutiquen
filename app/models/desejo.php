<?php

class Desejo extends AppModel {

    var $name = 'Desejo';
    public $actsAs = array('Containable');
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        ),
        'usuario_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        )
    );
    var $hasMany = array(
        'DesejoProduto' => array(
            'className' => 'DesejoProduto',
            'foreignKey' => 'desejo_id',
            'dependent' => true,
            'exclusive' => true
        )
    );

}

?>
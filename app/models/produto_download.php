<?php
class ProdutoDownload extends AppModel {
    var $name = 'ProdutoDownload';
    var $useTable = 'produto_downloads';
    var $primaryKey = 'produto_id';
    var $actsAs = array('MeioUpload' => array('filename' => array(
            'createDirectory' => true,
            'allowedMime' => array('image/jpeg', 'image/pjpeg', 'image/png', 'application/pdf', 'application/doc', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/pdf', 'application/docx'),
            'allowedExt' => array('.jpg', '.jpeg', '.png', '.pdf', '.doc', '.docx')
            ) 
        )
    );
}
?>
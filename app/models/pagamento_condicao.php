<?php

class PagamentoCondicao extends AppModel {

    var $name = 'PagamentoCondicao';
    var $useTable = 'pagamento_condicoes';
    var $displayField = 'nome';
    var $actsAs = array('Cached', 'Containable');
    var $validate = array(
        'nome' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        ),
        'pagamento_tipo_id' => array(
            'numeric' => array(
                'rule' => array('notempty'),
                'message' => 'Campo de preenchimento obrigatório.',
                'required' => true
            ),
        ),
        'parcelas' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Campo de preenchimento obrigatório.'
            ),
        ),
            // 'desconto_de' => array(		
            // 'valida' => array(
            // 'rule' => array('validaDescontoDe'),
            // 'message' => 'O campo de Desconto Avista em % é obrigatório.'
            // ),
            // ),
            // 'juros_de' => array(		
            // 'valida' => array(
            // 'rule' => array('validaJurosDe'),
            // 'message' => 'O campo de Juros de % é obrigatório.'
            // ),
            // ),
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $belongsTo = array(
        'PagamentoTipo' => array(
            'className' => 'PagamentoTipo',
            'foreignKey' => 'pagamento_tipo_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    var $hasMany = array(
        'PagamentoCondicaoImagem' => array(
            'className' => 'PagamentoCondicaoImagem',
            'foreignKey' => 'pagamento_condicao_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'dependent' => true
        )
    );

    public function validaDescontoDe() {

        App::import("helper", "String");
        $string = new StringHelper();
        $apartir = $string->moedaToBco($this->data['PagamentoCondicao']['desconto_apartir']);
        $desconto = $string->moedaToBco($this->data['PagamentoCondicao']['desconto_de']);

        if (isset($this->data['PagamentoCondicao']['desconto_apartir']) && $apartir > 0) {
            if (isset($this->data['PagamentoCondicao']['desconto_de']) && $desconto <= 0) {
                return false;
            }
        }
        return true;
    }

    public function validaJurosDe() {

        App::import("helper", "String");
        $string = new StringHelper();
        $apartir = $string->moedaToBco($this->data['PagamentoCondicao']['juros_apartir']);
        $desconto = $string->moedaToBco($this->data['PagamentoCondicao']['juros_de']);

        if (isset($this->data['PagamentoCondicao']['juros_apartir']) && $apartir > 0) {
            if (isset($this->data['PagamentoCondicao']['juros_de']) && $desconto <= 0) {
                return false;
            }
        }
        return true;
    }

    public function afterFind($results, $primary = false) {
//        if ($this->isCount($results))
//            return parent::afterFind($results, $primary);
        App::import("helper", "String");
        $string = new StringHelper();
        if (!empty($results)) {
            foreach ($results as $k => &$r) {
                @$r[$this->alias]['juros_de'] = $string->bcoToMoeda($r[$this->alias]['juros_de']);
                @$r[$this->alias]['desconto'] = $string->bcoToMoeda($r[$this->alias]['desconto']);
            }
        }
        return parent::afterFind($results, $primary);
    }

    public function beforeSave() {

        App::import("helper", "String");
        $string = new StringHelper();
        if (isset($this->data[$this->alias]['juros_de']) && !empty($this->data[$this->alias]['juros_de'])) {
            $this->data[$this->alias]['juros_de'] = $string->moedaToBco($this->data[$this->alias]['juros_de']);
        }
        if (isset($this->data[$this->alias]['desconto']) && !empty($this->data[$this->alias]['desconto'])) {
            $this->data[$this->alias]['desconto'] = $string->moedaToBco($this->data[$this->alias]['desconto']);
        }
        return parent::beforeSave();
    }

}

?>
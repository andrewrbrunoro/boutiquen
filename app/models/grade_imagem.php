<?php

    class GradeImagem extends AppModel
    {

        var $name = 'GradeImagem';
        var $useTable = 'grade_imagens';
        public $actsAs = array(
            'MeioUpload' => array(
                'filename' => array(
                    'dir'          => 'uploads/produto_imagem/filename',
                    'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                    'allowed_ext'  => array('.jpg', '.jpeg', '.png', '.gif'),
                    'fields'       => array(
                        'filesize' => 'filesize',
                        'mimetype' => 'mimetype',
                        'dir'      => 'dir'
                    )
                )
            ), 'Containable');
        var $belongsTo = array(
            'Grade' => array(
                'className'  => 'Grade',
                'foreignKey' => 'grade_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            )
        );

        public function beforeSave()
        {
            if (!isset($this->data[$this->alias]['parent_id'])) {
                $this->data[$this->alias]['parent_id'] = 0;
            }
            return parent::beforeSave();
        }

        public function getShowcaseImages($grade_id, $limit = 2)
        {
            return $this->find('all',
                array(
                    'fields'     => array('GradeImagem.filename', 'GradeImagem.ordem'),
                    'recursive'  => -1,
                    'conditions' => array(
                        'GradeImagem.grade_id' => $grade_id
                    ),
                    'limit'      => $limit
                )
            );
        }

        public function getImages($grade_id)
        {
            return $this->find('all',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'GradeImagem.grade_id' => $grade_id
                    ),
                    'order'      => array('GradeImagem.ordem')
                )
            );
        }

    }

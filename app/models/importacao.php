<?php
class Importacao extends AppModel{
	
	
	/**
	 * @param Atributo de validação da Importação
	 */
	var $validate = array(
        'arquivo' => array(
            'rule' => array('extension',array('xls','xlsx','xml','csv')),
            'message' => 'A extensão do arquivo não corresponde com as que são aceitas.',
            'required' => TRUE,
        ),
        'zip' => array(
			'rule' => array( 'extension' , array('zip','rar') ),
			'message' => 'A extensão do arquivo é apenas .zip ou .rar'
		)
    );
	
	
	
}
?>
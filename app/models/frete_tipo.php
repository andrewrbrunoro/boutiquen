<?php

    class FreteTipo extends AppModel
    {
        var $name = 'FreteTipo';
        public $actsAs = array('Containable');

        var $hasMany = array(
            'Frete' => array(
                'className'  => 'Frete',
                'fields'     => array(
                    'Frete.nome', 'Frete.descricao', 'Frete.dias_adicionais', 'Frete.id'
                ),
                'foreignKey' => 'frete_tipo_id',
                'dependent'  => false,
                'conditions' => array(
                    'Frete.status' => true
                )
            )
        );

        public function getFreights()
        {
            return $this->find('all',
                array(
                    'fields'     => array(
                        'FreteTipo.nome', 'FreteTipo.status'
                    ),
                    'conditions' => array(
                        'FreteTipo.status' => true
                    )
                )
            );
        }

    }
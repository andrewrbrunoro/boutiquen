<?php
class Link extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    var $name = 'Link';
/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
    public $actsAs = array(
		'Containable',
        'Encoder',
        'Tree',
        'Cached' => array(
            'prefix' => array(
                'link_',
                'menu_',
                'croogo_menu_',
            ),
        ),
        'MeioUpload' => array(
            'thumb_filename' => array(
                'dir' => 'uploads/links/thumb',
                'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
                'fields' => array(
                    'filesize' => 'thumb_filesize',
                    'mimetype' => 'thumb_mimetype',
                    'dir' => 'thumb_dir'
                )
            )
        )
	);
/**
 * Validation
 *
 * @var array
 * @access public
 */
    var $validate = array(
        'title' => array(
            'rule' => array('minLength', 1),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'link' => array(
            'rule' => array('validaLink'),
            'message' => 'Campo de preenchimento obrigatório.',
        ),
        'thumb_filename' => array(
            'Empty' => array('check' => false)
        ),
    );
/**
 * Model associations: belongsTo
 *
 * @var array
 * @access public
 */
    var $belongsTo = array(
        'Menu' => array('counterCache' => true)
    );
	
	public function validaLink($data){
		if (
			isset($this->data[$this->alias]['categoria_id']) && 
			isset($this->data[$this->alias]['pagina_id']) && 
			isset($this->data[$this->alias]['vitrine_id'])) {
			return true;
		}else if(strlen(reset($data)) > 0){
			return true;
		}
		return false;
	}
	
	public function beforeSave() {		
		//obtem e seta a url da categoria
		if (isset($this->data[$this->alias]['categoria_id']) && $this->data[$this->alias]['categoria_id'] != "") {
			$categoriaModel = ClassRegistry::init('Categoria');
			$categoria = $categoriaModel->find('first', array('fields' => array('Categoria.seo_url'), 'conditions' => array('Categoria.id' => $this->data[$this->alias]['categoria_id'])));
            
			$this->data[$this->alias]['link'] = '/categorias/'.$categoria['Categoria']['seo_url'];
			
			//null
			$this->data[$this->alias]['pagina_id'] = null;
			$this->data[$this->alias]['vitrine_id'] = null;
        }
		
		//obtem e seta a url da pagina
		if (isset($this->data[$this->alias]['pagina_id']) && $this->data[$this->alias]['pagina_id'] != "") {
			$paginaModel = ClassRegistry::init('Pagina');
			$pagina = $paginaModel->find('first', array('fields' => array('Pagina.url'), 'conditions' => array('Pagina.id' => $this->data[$this->alias]['pagina_id'])));
            
			$this->data[$this->alias]['link'] = '/'.$pagina['Pagina']['url'];
			
			//null
			$this->data[$this->alias]['categoria_id'] = null;
			$this->data[$this->alias]['vitrine_id'] = null;
        }
		
		//obtem e seta a url da vitrine
		if (isset($this->data[$this->alias]['vitrine_id']) && $this->data[$this->alias]['vitrine_id'] != "") {
			$vitrineModel = ClassRegistry::init('Vitrine');
			$vitrine = $vitrineModel->find('first', array('fields' => array('Vitrine.nome', 'Vitrine.tipo'), 'conditions' => array('Vitrine.id' => $this->data[$this->alias]['vitrine_id'])));
			if ($vitrine['Vitrine']['tipo'] == 'PROMOCOES') {
				$this->data[$this->alias]['link'] = "/ofertas";
			} elseif ($vitrine['Vitrine']['tipo'] == 'LANCAMENTOS') {
				$this->data[$this->alias]['link'] = "/lancamentos";
			}elseif ($vitrine['Vitrine']['tipo'] == 'MENU') {
				$this->data[$this->alias]['link'] = "/vitrine/" . low(Inflector::slug($vitrine['Vitrine']['nome'], "-"));
			}else{
				$this->data[$this->alias]['link'] = "/categoria/" . low(Inflector::slug($vitrine['Vitrine']['nome'], "-"));
			}
			
			//null
			$this->data[$this->alias]['categoria_id'] = null;
			$this->data[$this->alias]['pagina_id'] = null;
        }
		return parent::beforeSave();
	}

}
?>
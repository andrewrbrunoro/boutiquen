<?php

class Item extends AppModel {

    var $useDbConfig = 'postgres';
    var $name = 'Item';
    var $useTable = 'item';

   public function atualizaEstoque() {
        App::import("model", "Produto");
        $this->Produto = new Produto();

        $produtos =  $this->Produto->query('select a.id,a.nome,a.quantidade_alocada,a.descricao,a.codigo,c.valor from produtos a,atributos_produtos b ,atributos c where a.id = b.produto_id and c.id = b.atributo_id and c.`atributo_tipo_id` = 2');
        
        foreach($produtos as $produto){
           
           $estoque = $this->query(" 

                            SELECT 
                                
                                tamanhoitem.codtamanho,
                                item.descricao,
                                item.coditem,
                                item.reffornecedor,
                                (SELECT sum(estoque.qtd) FROM estoque WHERE estoque.coditem = item.coditem and estoque.codtamanho = tamanhoitem.codtamanho group by estoque.codtamanho limit 1) AS estoque

                            FROM 
                                item, 
                                tamanhoitem
                            WHERE 
                                
                                item.coditem = tamanhoitem.coditem AND
                                item.codempitem = tamanhoitem.codempitem  AND   
                                item.classeitemsite != '' AND   
                                item.ativo = 'S' AND
                                item.prcvendasite > 0 AND
                                item.coditem = '".($produto['a']['codigo'])."' AND
                                tamanhoitem.codtamanho = '".($produto['c']['valor'])."'

                            GROUP BY 
                                tamanhoitem.codtamanho,
                                item.fretegratissite,
                                item.descricao,
                                item.coditem,
                                item.reffornecedor
                               
                                                             
                        ");

            if (!isset($estoque[0][0]['estoque'])) {
                $estoque[0][0]['estoque'] = 0;
            }
            $this->Produto->save(array('id'=>$produto['a']['id'],'quantidade'=>$estoque[0][0]['estoque'],'quantidade_disponivel'=>$estoque[0][0]['estoque']-$produto['a']['quantidade_alocada']),false);
        }


        $produtos_a = $this->Produto->find('all',array('conditions'=>array('parent_id'=>0)));
        foreach($produtos_a as $produto_b){
            $this->Produto->altera_produto_estoque($produto_b['Produto']['id']);
        }
		die;
    }
}
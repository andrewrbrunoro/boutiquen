<?php

class DuvidaFrequenteTipo extends AppModel {

    public $name = 'DuvidaFrequenteTipo';
    public $useTable = 'duvidas_frequentes_tipos';
    public $actsAs =  array('Cached','Containable');
	public $validate = array(
        'nome' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	
	var $hasMany = array(
		'DuvidaFrequente' => array(
			'className' => 'DuvidaFrequente',
			'foreignKey' => 'duvida_frequente_tipo_id',
			//'dependent' => false,'
		)
	);
	
	/**
	 * Obteção das dúvidas frequentes filtradas por tipo
	 *
	 * @access public
	 * @param string $tipo Tipo de dúvida frequente em slug
	 * @return array Conjunto de informações do(s) registro(s)
	 */
	public function getPorTipo($tipo){
		$duvidas_frequentes = $this->find(
									'first', 
										array(
											'fields' => array('DuvidaFrequenteTipo.nome','DuvidaFrequenteTipo.codigo'),
											'conditions' => array('DuvidaFrequenteTipo.codigo' => $tipo),
											'contain' => array('DuvidaFrequente' => array(
																		'conditions' => array('DuvidaFrequente.status' => true),
																		'fields' => array('DuvidaFrequente.pergunta', 'DuvidaFrequente.resposta')
																	)
															)
											)
									);
		if(!$duvidas_frequentes){
			throw new Exception('Nenhuma dúvida frequente encontrada.');
		}
		return $duvidas_frequentes;
	}
	
	// sobrescrita de gancho para ações antes do salvamento
	public function beforeSave(){
		if(isset($this->data[$this->alias]['nome']) && $this->data[$this->alias]['nome'] != ""){
			$this->data[$this->alias]['codigo'] = low(Inflector::slug($this->data[$this->alias]['nome'], '-'));
		}
		return parent::beforeSave();
	}
}

?>
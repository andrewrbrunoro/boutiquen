<?php

    class Pedido extends AppModel
    {

        var $name = 'Pedido';
        var $useTable = 'pedidos';

        var $belongsTo = array(
            'Usuario'           => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario_id'
            ),
            'PedidoStatus'      => array(
                'className'  => 'PedidoStatus',
                'foreignKey' => 'pedido_status_id'
            ),
            'PagamentoCondicao' => array(
                'className'  => 'PagamentoCondicao',
                'foreignKey' => 'pagamento_condicao_id'
            ),
            'Frete'             => array(
                'className'  => 'Frete',
                'foreignKey' => 'frete_id'
            ),
            'PagseguroCode' => array(
                'className'     => 'PagseguroCode',
                'foreignKey'    => 'pagseguro_method_code'
            )
        );

        var $hasOne = array(
            'CupomQueimado' => array(
                'className'  => 'CupomQueimado',
                'foreignKey' => 'pedido_id',
                'dependent'  => false
            )
        );

        var $hasMany = array(
            'PedidoItem'              => array(
                'className'  => 'PedidoItem',
                'foreignKey' => 'pedido_id',
                'dependent'  => false
            ),
            'PedidoHistoricoStatus'   => array(
                'className'  => 'PedidoHistoricoStatus',
                'foreignKey' => 'pedido_id',
                'dependent'  => false
            ),
            'PedidoHistoricoTracking' => array(
                'className'  => 'PedidoHistoricoTracking',
                'foreignKey' => 'pedido_id',
                'dependent'  => false
            )
        );


    }


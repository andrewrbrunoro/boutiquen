<?php

class Noticia extends AppModel {

    var $name = 'Noticia';
    var $useTable = 'noticias';
    public $actsAs =  array('MeioUpload' => array(
								'thumb_filename' => array(
								
										'dir' => 'uploads/noticia/thumb', 
										'allowed_mime' => array( 'image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
										'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
										 'fields' => array(
												'filesize' => 'thumb_filesize',
												'mimetype' => 'thumb_mimetype',
												'dir' => 'thumb_dir'
											)
										)
								),'Cached','Containable');
	var $validate = array(
		'titulo' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'descricao' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
		'conteudo' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
    );
	
	public function remover_thumb($id){
		$model['id'] = $id;
		$model['thumb_filename'] = ' ';
		$model['thumb_dir'] = null;
		$model['thumb_mimetype'] = null;
		$model['thumb_filesize'] = null;		
		
		if($this->save($model,false))
			return true;
	}
}

?>
<?php

class Atributo extends AppModel {

    public $actsAs = array(
        'MeioUpload' => array(
            'thumb_filename' => array(
                'dir' => 'uploads/atributo/thumb',
                'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
                'fields' => array(
                    'filesize' => 'thumb_filesize',
                    'mimetype' => 'thumb_mimetype',
                    'dir' => 'thumb_dir'
                )
            )
        ),
        'Containable'
    );
    public $validate = array(
        'atributo_tipo_id' => array(
            'noempty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Preenchimento obrigatório.'
            )
        ),
        'thumb_filename' => array(
            'Empty' => array('check' => false)
        ),
    );
    var $belongsTo = array(
        'AtributoTipo' => array(
            'className' => 'AtributoTipo',
            'foreignKey' => 'atributo_tipo_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    var $hasAndBelongsToMany = array(
        'CategoriaAtributo' => array(
            'className' => 'CategoriaAtributo',
            'joinTable' => 'categorias_atributos',
            'foreignKey' => 'atributo_id',
            'associationForeignKey' => 'categoria_id',
            'unique' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        ),
        'AtributoProduto' => array(
            'className' => 'AtributoProduto',
            'joinTable' => 'atributos_produtos',
            'foreignKey' => 'produto_id',
            'associationForeignKey' => 'atributo_id',
            'unique' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => ''
        )
    );
	public function beforeSave() {		
		//obtem e seta a url da vitrine
		if (isset($this->data[$this->alias]['valor']) && $this->data[$this->alias]['valor'] != "") {
			$this->data[$this->alias]['codigo'] = low(Inflector::slug($this->data[$this->alias]['valor'], "-"));
        }
		return parent::beforeSave();
	}

    public function remover_thumb($id) {
        //$this->id = $id;
        $atributo['id'] = $id;
        $atributo['thumb_filename'] = ' ';
        $atributo['thumb_dir'] = null;
        $atributo['thumb_mimetype'] = null;
        $atributo['thumb_filesize'] = null;

        if ($this->save($atributo, false))
            return true;
    }

}

?>
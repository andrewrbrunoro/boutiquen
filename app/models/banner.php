<?php

    class Banner extends AppModel
    {

        var $name = 'Banner';
        var $useTable = 'banners';
        var $displayField = 'filename';
        var $validate = array(
            'banner_tipo_id' => array(
                'noempty' => array(
                    'rule'    => array('notEmpty'),
                    'message' => 'Preenchimento obrigatório.'
                )
            ),
            'link'           => array(
                'rule'       => array('url', true),
                'message'    => 'Informe um URL válida ex:http://exemplo.com.br',
                'allowEmpty' => true,
            )
        );
        var $actsAs = array('Cached', 'Containable', 'MeioUpload' => array('filename'));
        var $belongsTo = array(
            'BannerTipo' => array(
                'className'  => 'BannerTipo',
                'foreignKey' => 'banner_tipo_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            )
        );
        var $hasAndBelongsToMany = array(
            'Categoria' => array(
                'className'             => 'Categoria',
                'joinTable'             => 'banners_categorias',
                'foreignKey'            => 'banner_id',
                'associationForeignKey' => 'categoria_id',
                'unique'                => true,
                'conditions'            => '',
                'fields'                => '',
                'order'                 => '',
                'limit'                 => '',
                'offset'                => '',
                'finderQuery'           => '',
                'deleteQuery'           => '',
                'insertQuery'           => ''
            )
        );

        public function beforeSave()
        {
            App::import("helper", "Calendario");
            $this->Calendario = new CalendarioHelper();

            if (isset($this->data[$this->alias]['data_inicio']) && $this->data[$this->alias]['data_inicio'] != "") {
                $this->data[$this->alias]['data_inicio'] = $this->Calendario->dataFormatada("Y-m-d 00:00:00", $this->data[$this->alias]['data_inicio']);
            } else {
                $this->data[$this->alias]['data_inicio'] = null;
            }
            if (isset($this->data[$this->alias]['data_fim']) && $this->data[$this->alias]['data_fim'] != "") {
                $this->data[$this->alias]['data_fim'] = $this->Calendario->dataFormatada("Y-m-d 23:59:59", $this->data[$this->alias]['data_fim']);
            } else {
                $this->data[$this->alias]['data_fim'] = null;
            }

            return parent::beforeSave();
        }

        public function getBanners($type = 'banner_topo')
        {
            return $this->find('all',
                array(
                    'conditions' => array(
                        'BannerTipo.codigo' => $type,
                        'Banner.capa'       => true
                    ),
                    'order' => array('Banner.ordem ASC')
                )
            );
        }

    }
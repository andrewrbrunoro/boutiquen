<?php

    class UsuarioEndereco extends AppModel
    {
        public $actsAs = array('Validacao', 'Containable');
        var $name = 'UsuarioEndereco';
        var $useTable = 'usuario_enderecos';


        var $belongsTo = array(
            'Usuario' => array(
                'className'  => 'Usuario',
                'foreignKey' => 'usuario_id',
                'conditions' => '',
                'fields'     => '',
                'order'      => ''
            )
        );

        public function isMyAddress($id, $user_id)
        {
            $query = $this->find('first',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'UsuarioEndereco.id'         => $id,
                        'UsuarioEndereco.usuario_id' => $user_id
                    )
                )
            );
            return $query;
        }

        public function getAddressByCep($cep, $user_id)
        {
            return $this->find('first',
                array(
                    'recursive'  => -1,
                    'conditions' => array(
                        'UsuarioEndereco.usuario_id' => $user_id,
                        'UsuarioEndereco.cep'        => preg_replace('/[^0-9]/', '', $cep)
                    )
                )
            );
        }

        public function beforeSave()
        {
            if (isset($this->data[$this->alias]['cep']))
                $this->data[$this->alias]['cep'] = preg_replace("/[^0-9]/", "", $this->data[$this->alias]['cep']);
            return parent::beforeSave();
        }
    }

<?php

class DuvidaFrequente extends AppModel {

    public $name = 'DuvidaFrequente';
    public $useTable = 'duvidas_frequentes';
    public $actsAs =  array('Cached','Containable');
	public $validate = array(
        'pergunta' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        ),
        'resposta' => array(
            'rule' => array('notEmpty'),
            'message' => 'Campo de preenchimento obrigatório.'
        )
    );
	
	/**
	 * Definição de relacionamentos de 1 -> 1
	 *
	 * @var array
	 * @access public
	 */
	public $belongsTo = array(
		'DuvidaFrequenteTipo' => array(
			'className' => 'DuvidaFrequenteTipo',
		),
	);


        public function buscaDuvidas() {
            $duvidas_frequentes = $this->find('all',array('conditions' => array('DuvidaFrequente.status'=>1, 'DuvidaFrequente.duvida_frequente_tipo_id'=>1), 'order' => array('DuvidaFrequente.created ASC')));
            return $duvidas_frequentes;
        }	        
}

?>
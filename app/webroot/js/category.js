$(document).ready(function(){
    var $priceSlider = $('#price-slider');
    $priceSlider.ionRangeSlider({
        min: $priceSlider.data('min'),
        max: $priceSlider.data('max'),
        type: 'double',
        prefix: "R$",
        prettify: false,
        hasGrid: true
    });
});
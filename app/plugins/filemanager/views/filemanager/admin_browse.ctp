<style>
.filemanager .directory-content{
	clear: both;
    margin: 0 auto;
    width: 98%;
}
.filemanager .actions{
	width: auto;
}
.filemanager .actions li{
	float: left;
    margin-right: 10px;
}
.filemanager .breadcrumb{
	clear: both;
    margin-bottom: 10px;
    margin-left: 15px;
}
#flashMessage{
    clear: both;
    color: #fff;
    background: green;
    padding: 5px 5px 5px 25px;   
}
</style>
<div class="index filemanager folder">
	<h2><?php echo $title_for_layout; ?></h2>

    <div class="actions">
        <ul>
            <li><?php echo $filemanager->link(__('Upload nesse diretório', true), array('controller' => 'filemanager', 'action' => 'upload'), $path); ?></li>
            <li><?php echo $filemanager->link(__('Criar diretório', true), array('controller' => 'filemanager', 'action' => 'create_directory'), $path); ?></li>
            <li><?php echo $filemanager->link(__('Criar arquivo', true), array('controller' => 'filemanager', 'action' => 'create_file'), $path); ?></li>
        </ul>
    </div>

	<div class="breadcrumb">
	<?php
        echo __('Você está aqui:', true) . ' ';
        $breadcrumb = $filemanager->breadcrumb($path);
        foreach ($breadcrumb AS $pathname => $p) {
            echo $filemanager->linkDirectory($pathname, $p);
            echo DS;
        }
	?>
	</div>
	
	<div class="directory-content">
        <table cellpadding="0" cellspacing="0">
        <?php
            $tableHeaders =  $html->tableHeaders(array(
                '',
                __('Directory content', true),
                __('Actions', true),
            ));
            echo $tableHeaders;

            // directories
            $rows = array();
            foreach ($content['0'] AS $directory) {
                $actions = $filemanager->linkDirectory(__('Abrir', true), $path.$directory.DS);
                if ($filemanager->inPath($deletablePaths, $path.$directory)) {
                    $actions .= ' ' . $filemanager->link(__('Deletar', true), array(
                        'controller' => 'filemanager',
                        'action' => 'delete_directory',
                        'token' => md5($this->Session->id()),
                    ), $path.$directory);
                }
                $rows[] = array(
                    $html->image('/img/icons/folder.png'),
                    $filemanager->linkDirectory($directory, $path.$directory.DS),
                    $actions,
                );
            }
            echo $html->tableCells($rows, array('class' => 'directory'), array('class' => 'directory'));

            // files
			$rows = array();
            foreach ($content['1'] AS $file) {
				$actions = '';
                if ($filemanager->inPath($deletablePaths, $path.$file)) {
					$actions = $filemanager->link(__('Editar', true), array('controller' => 'filemanager', 'action' => 'editfile'), $path.$file);
                    $actions .= ' ' . $filemanager->link(__('Deletar', true), array(
                        'controller' => 'filemanager',
                        'action' => 'delete_file',
                        'token' => md5($this->Session->id()),
                    ), $path.$file);
                }
                $rows[] = array(
                    $html->image('/img/icons/'.$filemanager->filename2icon($file)),
					$file,
                    //$filemanager->linkFile($file, $path.$file),
                    $actions,
                );
            }
            echo $html->tableCells($rows, array('class' => 'file'), array('class' => 'file'));

            echo $tableHeaders;
        ?>
        </table>
	</div>
</div>
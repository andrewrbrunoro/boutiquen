$(function(){
	//nyrol modal video
	if ($('.nyroModalVideo').length) {
        var width = 520;
        var height = 370;

        $('.nyroModalVideo').nyroModal({
            sizes: {
                initW: width, initH: height,
                minW: width, minH: height,
                w: width, h: height
            },
            callbacks: {
                beforeShowCont: function(nm) {
                    $('.nyroModalCont iframe').css('height', height+'px').css('width', width + 'px');
                    $('.nyroModalCont').css('height', height + 'px');
                }
            }
        });
    };
    
    //nuro modal
    if ($('.nyroModalCompra').length) {
        var width = 600;
        var height = 470;

        $('.nyroModalCompra').nyroModal({
            sizes: {
                initW: width, initH: height,
                minW: width, minH: height,
                w: width, h: height
            },
            callbacks: {
                beforeShowCont: function(nm) {
                    $('.nyroModalCont iframe').css('height', height+'px').css('width', width + 'px');
                    $('.nyroModalCont').css('height', height + 'px').css('width', width + 'px');
                }
            }
        });
    };
    
    //nyro photos
    $('.nyroModalFoto').nyroModal();
    
    //click de confirmao e requisicao da exclusao de foto da lista
    $('.rm-img').live("click", function() {
        img = $(this);
        if (confirm('Deseja mesmo remover a foto?')) {
            img.addClass('loading');
            $.post(PATH.basename + "/admin/lista_de_presente/lista_fotos/remover/" + img.data('id'), function(values) {
                if (values) {
                    img.parents('li').remove();
                } else {
                    alert('Erro ao remover Imagem');
                }
            }, 'json');
        }
        return false;
    });
    
    //click de confirmao e requisicao da exclusao de video da lista
    $(".rm-video").click(function(event){
    	event.preventDefault();
    	var btn = $(this);
    	if(confirm('Deseja realmente remover este vídeo?')){
    		btn.addClass('loading');
    		removeMensagens();
			$.post(PATH.basename+'/admin/lista_de_presente/lista_videos/remover/'+btn.data('lista-id')+"/"+btn.data('video-id'),function(response){
				if(response.status){
					$.each(response.mensagens, function(){
						addMensagem(this);
					})
					btn.closest('li').remove();
				}
			},'json')
    	}
    });
    
    //click de confirmacao de exclusao de um item da lista
    $('.rm-item').click(function(){
		return confirm('Deseja realmente remover este item?');
	});
	
	//begin abas
	var idl = $('#ListaId').val();
	// Abre fecha abas
    $(".clicaaba").live('click', function() {
        var rel = $(this).attr("rel");
        $(".abas").hide();
        $('.abas[rel="' + rel + '"]').show();
        $(".clicaaba").removeClass("active");
        $(this).addClass("active");
        if (typeof $.cookie === "function") {
        	$.cookie("abalista" + idl, rel);
        }
    });
    $('.abas').each(function() {
        if ($('.error-message', this).length > 0) {
            var rel = $(this).attr("rel");
            $('.clicaaba[rel="' + rel + '"]').addClass('borderred');
        }
    });
    if (typeof $.cookie === "function") {
        if ($.cookie("abalista" + idl)) {
            var qaba = $.cookie("abalista" + idl);
            $(".abas").hide();
            $('.abas[rel="' + qaba + '"]').show();
            $(".clicaaba").removeClass("active");
            $('.clicaaba[rel="' + qaba + '"]').addClass("active");
        }else{
	    	$(".abas").hide();
	        $('.abas[rel="0"]').show();
	        $(".clicaaba").removeClass("active");
	        $('.clicaaba[rel="0"]').addClass("active");
	    }
    }
    //end abas
    
    //marcara para o field 'Hora do Evento'
    $('.number').setMask({
        mask: '9999'
    });
    
    //click de confirmao e requisicao da exclusao de video da lista
    $(".rm-compra").click(function(event){
    	event.preventDefault();
    	var btn = $(this);
    	if(confirm('Deseja realmente remover essa venda?')){
    		btn.addClass('loading');
    		
    		var lista_id = btn.data('lista-id');
	    	var tipo = btn.data('tipo');
	    	var pedido_id = btn.data('pedido-id');
	    	var data = {data: {lista_id: lista_id, tipo: tipo, pedido_id: pedido_id}};
    		
			$.post(PATH.basename+'/admin/lista_de_presente/listas/remover_compra/',data,function(response){
				if(response.status){
					btn.closest('tr').remove();
				}else{
					alert('Ocorre um erro ao remover venda.');
				}
			},'json')
    	}
    });
    
    //checkebox que defini se a foto eh capa ou nao
    $(".foto_capa").live('change', function(){
    	//event.preventDefault();
    	var btn = $(this);
    	removeMensagens();
    	
    	var lista_id = btn.data('lista-id');
    	var item_id = btn.data('item-id');
    	
    	var capa = "";
    	if(btn.is(':checked')){
    		capa = 1;
    		$(".foto_capa").attr("checked", '');
    		btn.attr("checked", "checked");
    	}else{
    		capa = 0;
    		btn.attr("checked", "");
    	}
    	var data = {data: {lista_id: lista_id, item_id: item_id, capa: capa}};
    	
		$.post(PATH.basename+'/lista_de_presente/lista_fotos/define_capa/',data,function(response){
			if(response.status){
				$.each(response.mensagens, function(){
					addMensagem(this);
				})
			}
		},'json')
    });
    
    $("select[name='data[Lista][loja_id]']").change(function(){
    	if($(this).val() != ""){
    		$("select[name='data[Lista][usuario_endereco_id]']").val("");
    	}
    });
    
     $("select[name='data[Lista][usuario_endereco_id]']").change(function(){
    	if($(this).val() != ""){
    		$("select[name='data[Lista][loja_id]']").val("");
    	}
    });
})
$(document).ready(function(){
	
	$('#edit-produto').uploadify({
        'uploader': PATH.basename + '/swf/uploadify.swf',
        'script': PATH.basename + '/lista_de_presente/lista_fotos/img_edit/' + $('#lista-id').val() + '/sid:' + PATH.sid,
        'cancelImg': PATH.basename + '/img/icons/delete.png',
        'auto': true,
        'multi': true,
        'debub': true, 
        'fileExt': '*.jpg;*.gif;*.png',
        'fileDesc': 'Image Files (.JPG, .GIF, .PNG)',
        'buttonText': 'Enviar Arquivo',
        'sizeLimit': 104857600,
        'onComplete': function(event, queueID, fileObj, response, data) {
            $('.container-edit').html(response);
        },
        'onError': function(a, b, c, d) {
            if (d.status == 404)
                alert('Erro ao enviar arquivo');
            else if (d.type === "HTTP")
                alert('Erro ' + d.type + ": " + d.status);
            else if (d.type === "File Size")
                alert('Arquivo muito grande, suportado somente até 100MB');
            else
                alert('error ' + d.type + ": " + d.text);
        }
    });
    
    $('.rm-img-old').live("click", function() {
        img = $(this);
        if (confirm('Deseja mesmo remover a foto?')) {
            img.addClass('loading');
            $.post(PATH.basename + "/lista_de_presente/lista_fotos/rm_img/" + img.attr('rel'), function(values) {
                if (values) {
                    img.parent().parent().parent().remove();
                } else {
                    alert('Erro ao remover Imagem');
                }
            }, 'json');
        }
        return false;
    });
    
    $(".foto_capa").live('change', function(){
    	//event.preventDefault();
    	var btn = $(this);
    	removeMensagens();
    	
    	var lista_id = btn.data('lista-id');
    	var item_id = btn.data('item-id');
    	
    	var capa = "";
    	if(btn.is(':checked')){
    		capa = 1;
    		$(".foto_capa").attr("checked", '');
    		btn.attr("checked", "checked");
    	}else{
    		capa = 0;
    		btn.attr("checked", "");
    	}
    	var data = {data: {lista_id: lista_id, item_id: item_id, capa: capa}};
    	
		$.post(PATH.basename+'/lista_de_presente/lista_fotos/define_capa/',data,function(response){
			if(response.status){
				$.each(response.mensagens, function(){
					addMensagem(this);
				})
			}
		},'json')
    });
});

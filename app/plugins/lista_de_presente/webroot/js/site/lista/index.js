$(document).ready(function() {
	//marcara para o field 'Data do Evento'
	$('.date').setMask({
        mask: '39/19/9999'
    });
	//formata a sanfona da tela de duvidas frequentes
	$(".duvida-frequente li a").click(function() {
	 	if ($(this).attr('rel') == 'ativo') {
	 		$(this).parents('li').find(".duvida_frequente_resposta").slideUp();
	 		$(this).attr('rel', 'inativo');
			$(this).removeClass('ativa');
	 	} else{
	     	$(".duvida_frequente_resposta").slideUp();
	 		$(".duvida-frequente li a").attr('rel', 'inativo');
			$(".duvida-frequente li a").removeClass('ativa');
			$(this).parents('li').find(".duvida_frequente_resposta").slideDown();
			$(this).attr('rel', 'ativo');
			$(this).addClass('ativa');
		}
	});
});

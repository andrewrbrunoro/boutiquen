$(function(){
	$('#ordenacao').change(function(){
		var url = window.location.href
		url = url.replace(/\/sort:[A-Za-z0-9_\.\-]+/,'').replace(/\/direction:[A-Za-z0-9_\.\-]+/,'').replace(/\/$/,'')
		url += '/' + $(this).val()
		window.location.href = url
	})

	$('.present-navigation a').click(function(event){
		event.preventDefault()
		$('.active','.present-navigation').removeClass('active')
		$(this).parent().addClass('active')

		$('.box_active').removeClass('box_active')
		$('#'+$(this).data('id')).addClass('box_active')
	})

	$('.product-box .up').click(function(event){
		event.preventDefault()
		alterQuantity(this,'up')
	})

	$('.product-box .down').click(function(event){
		event.preventDefault()
		alterQuantity(this,'down')
	})
	$('.product-box input').keyup(function(event){
		value = $(this).val()
		if (!value.match(/^[0-9]+$/)) {
			value = 0
		}
		setQuantity(this,value)
	})
	
	//nyrol modal video
	if ($('.nyroModalVideo').length) {
        var width = 600;
        var height = 370;

        $('.nyroModalVideo').nyroModal({
            sizes: {
                initW: width, initH: height,
                minW: width, minH: height,
                w: width, h: height
            },
            callbacks: {
                beforeShowCont: function(nm) {
                    $('.nyroModalCont iframe').css('height', height+'px').css('width', width + 'px');
                    $('.nyroModalCont').css('height', height + 'px');
                }
            }
        });
    };
    
    //nyro photos
    if ($('.nyroModalFoto').length) {
    	$('.nyroModalFoto').nyroModal();
    }
    
    //nyrol modal map
	if ($('.nyroModalMap').length) {
        var width = 600;
        var height = 450;

        $('.nyroModalMap').nyroModal({
            sizes: {
                initW: width, initH: height,
                minW: width, minH: height,
                w: width, h: height
            },
            callbacks: {
                beforeShowCont: function(nm) {
                	$('.nyroModalCont iframe').css('height', height+'px').css('width', width + 'px');
                    $('.nyroModalCont').css('height', height + 'px');
                    $('.nyroModalCont .nyroModalDom').append('<div id="map_evento" style="width: 580px; height: 420px;"></div>');
                },
                afterShowCont: function(nm) {
                	googleMapInitialize('map_evento',$("#local").text());
                }
            }
        });
    };
    
    var time = 0;
    $('#ListaLocal').keyup(function(event){
    	var actualTime = new Date().getTime();
    	var pause = 1000;
    	var address = $(this).val();
    	$("#view").html('Carregando...');
    	time = actualTime;
    	setTimeout(function() {
    		actualTime = new Date().getTime()
	    	if (time != 0 && address != '' && actualTime - time >= pause) {
	    		googleMapInitialize('view',address,{'zoom':15});
	    	}
    	},1000)    	
    })
    
    $('#ListaLocal').blur(function(){
    	if($(this).val() == ""){
    		$("#view").empty();
    	}
    });
    
    if($('#ListaLocal').length && $('#ListaLocal').val() != ""){
    	$("#view").html('Carregando...');
    	googleMapInitialize('view',$('#ListaLocal').val(),{'zoom':15});
    }
});

var map;
function googleMapInitialize(id,address,options) {
	var myOptions = {
	    zoom: 17,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	$.extend(myOptions, options);
	
	map = new google.maps.Map(document.getElementById(id), myOptions);
	geocoder0 = new google.maps.Geocoder();
	geocoder0.geocode({
	    'address': address,
	    }, function (results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	        map.setCenter(results[0].geometry.location);
	        var marker0 = new google.maps.Marker({
	            position: results[0].geometry.location,
	            map: map,
	        });
	    }
	});
}

function setQuantity(input,value) {
	var container = $(input).closest('li')
	var max = parseInt($('.quantidade',container).text()) - parseInt($('.quantidade-vendidos',container).text())
	if (value > max) {
		value = max
	} else if (value < 0) {
		value = 0
	}
	value = parseInt(value)
	if (value == NaN) {
		value = 0
	}
	$(input).val(value)
}

function alterQuantity(element,direction) {
	var input = $('input',$(element).parent())
	var value = $(input).val()
	if (direction == 'up') {
		value++
	} else {
		value--
	}
	setQuantity(input,value)

}
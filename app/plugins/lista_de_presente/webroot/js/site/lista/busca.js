var buscaAvancadaElement
$(function(){
	buscaAvancadaElement = $("input[name='data[Busca][avancada]']")
	showBusca()

	$('a.alteraBusca').click(function(event) {
		event.preventDefault()
		if ($(buscaAvancadaElement).val() == 1) {
			$(buscaAvancadaElement).val(0)
		} else {
			$(buscaAvancadaElement).val(1)
		}
		showBusca()
	})
})

function showBusca() {
	console.log($(buscaAvancadaElement).val())
	if ($(buscaAvancadaElement).val() == 1) {
		$('.row2').fadeOut();
		$('.busca-normal').hide()
		$('.busca-normal input').attr('disabled',true)
		$('.busca-avancada input').attr('disabled',false)
		$('.busca-avancada').hide();
		$('.busca-avancada').toggle(1000)
		$('.alteraBusca').text('Busca Normal')
	} else {
		$('.row2').fadeIn();
		$('.busca-avancada').toggle()
		$('.busca-normal input').attr('disabled',false)
		$('.busca-avancada input').attr('disabled',true)
		$('.busca-normal').fadeIn()
		$('.alteraBusca').text('Busca Avançada')
	}
}

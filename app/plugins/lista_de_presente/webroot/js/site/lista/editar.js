$(document).ready(function() {
	$('.form-editar').submit(function(event){
		event.preventDefault();
		removeMensagens();
		$.post(PATH.basename+'/lista_de_presente/listas/adicionar_item/'+$(this).data('lista'),$(this).serialize(),function(response){
			$.each(response.mensagens, function(){
				addMensagem(this);
			})
		},'json')
	})
	
	$('.remover').click(function(){
		return confirm('Deseja realmente remover este item?');
	})
});
$(document).ready(function() {
	//marcara para o field 'Data do Evento'
	$('.date').setMask({
        mask: '39/19/9999'
    });
    
    //marcara para o field 'Hora do Evento'
    $('.hour').setMask({
        mask: '29:69'
    });
    
    //verifica se o campo de definicao de entrega existe
    if($("input[name='data[Lista][endereco_proprio]']").length){
    	//invoca o metodo de lista de endereços no load do element
    	getEnderecos($("input[name='data[Lista][endereco_proprio]']:checked"));
    	
    	//invoca o metodo de lista de endereços no click do element
    	$("input[name='data[Lista][endereco_proprio]']").click(function(){
    		getEnderecos($(this));
    	});	
	}
	
	//efeito na estilizacao do input file customizado
	$(".file-original").live('change', function() {
		$(".file-falso").val($(this).val());
    });	
});

//exibe o gif de loading da requisicao ajax
function showLoading(){
	$('.loading').css('visibility', 'visible');
}

//esconde o gif de loading da requisicao ajax
function hiddenLoading(){
	$('.loading').css('visibility', 'hidden');
}

//metodo que verifica se o usuario setou endereco proprio e carrega os enderecos do mesmo
function getEnderecos( current ){
	if(current.val() == "1"){
		//esconde box aonde ficarão as lojas
		$('.box-lojas').hide();
		//exibe box aonde ficarão os endereços
		$('.box-enderecos-lista').fadeIn();
		//limpa a lista de endereco
		$('.box-enderecos-lista-content').empty();
		showLoading();
		//limpo
		$("#ListaLojaId").val("").attr("checked", false);
		//post
		$.post(PATH.basename + "/usuario_enderecos/get_endereco_usuario/", function(response) {
			if(response != ""){
				//alimento a lista de enderecos
				$(response).each(function(v){            	
	            	var radio = '';
	            	var id = this.UsuarioEndereco.id;
	            	var label = $('<label>').attr('for' , 'ListaUsuarioEnderecoId'+id);
	            	var ckecked = '';
	            	
	            	if($("#ListaUsuarioEnderecoId").val() != "" && $("#ListaUsuarioEnderecoId").val() == id){
	            		ckecked = 'checked';	
	            	}
	            	
	            	radio = $('<input>').attr({
	                    type: 'radio', 
	                    name: 'data[Lista][usuario_endereco_id]', 
	                    value: this.UsuarioEndereco.id, 
	                    id: 'ListaUsuarioEnderecoId'+id,
	                    class: 'radio',
	                    checked: ckecked
	               });
	               
	               $(label).html(
	               		'<strong>'+this.UsuarioEndereco.nome+'</strong>, '+
	               		this.UsuarioEndereco.rua+', '+this.UsuarioEndereco.numero+', '+
	               		this.UsuarioEndereco.bairro+', '+this.UsuarioEndereco.cidade+', '+this.UsuarioEndereco.uf
	               );	                   
	               $('.box-enderecos-lista-content').append(radio.after(label));
	            });
	        }
            hiddenLoading();
		});
	}else if(current.val() == "2"){
		//exibe box aonde ficarão os endereços
		$('.box-lojas').fadeIn();
		//limpa a lista de endereco
		$('.box-lojas-content').empty();
		$('.box-enderecos-lista').hide();
		showLoading();
		//limpo
		$("#ListaUsuarioEnderecoId").val("").attr("checked", false);
		//post
		$.post(PATH.basename + "/lojas/get_enderecos_lojas/", function(response) {
			if(response != ""){
				//alimento a lista de enderecos
				$(response).each(function(v){            	
	            	var radio = '';
	            	var id = this.Loja.id;
	            	var label = $('<label>').attr('for' , 'ListaLojaId'+id);
	            	var ckecked = '';
	            	
	            	if($("#ListaLojaId").val() != "" && $("#ListaLojaId").val() == id){
	            		ckecked = 'checked';	
	            	}
	            	
	            	radio = $('<input>').attr({
	                    type: 'radio', 
	                    name: 'data[Lista][loja_id]', 
	                    value: this.Loja.id, 
	                    id: 'ListaLojaId'+id,
	                    class: 'radio',
	                    checked: ckecked
	               });
	               
	               $(label).html(
	               		'<strong>'+this.Loja.nome+'</strong>, '+
	               		this.Loja.rua+', '+this.Loja.numero+', '+
	               		this.Loja.bairro+', '+this.Loja.cidade+', '+this.Loja.uf
	               );	                   
	               $('.box-lojas-content').append(radio.after(label));
	            });
	        }
            hiddenLoading();
		});
	}else{
		//escondo box aonde ficarão os endereços
		$('.box-enderecos-lista').fadeOut();
		$('.box-lojas').fadeOut();
		//limpa a lista de endereco
		$('.box-enderecos-lista-content').empty();
		//empty hidden
		$("#ListaUsuarioEnderecoId").val("");
	}
}

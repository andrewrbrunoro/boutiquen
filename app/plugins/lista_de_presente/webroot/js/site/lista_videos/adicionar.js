$(document).ready(function(){
	
	//nyrol modal video
	if ($('.nyroModal').length) {
        var width = 520;
        var height = 370;

        $('.nyroModal').nyroModal({
            sizes: {
                initW: width, initH: height,
                minW: width, minH: height,
                w: width, h: height
            },
            callbacks: {
                beforeShowCont: function(nm) {
                    $('.nyroModalCont iframe').css('height', '370px').css('width', width + 'px');
                    $('.nyroModalCont').css('height', height + 'px');
                }
            }
        });
    };
    
    $(".btn-remover-video").click(function(event){
    	event.preventDefault();
    	var btn = $(this);
    	if(confirm('Deseja realmente remover este item?')){
    		removeMensagens();
			$.post(PATH.basename+'/lista_de_presente/lista_videos/remover/'+btn.data('lista-id')+"/"+btn.data('video-id'),function(response){
				if(response.status){
					$.each(response.mensagens, function(){
						addMensagem(this);
					})
					btn.closest('li').remove();
				}
			},'json')
    	}
    });
    
    $("#ListaVideoAdicionarForm").submit(function(){
    	$(".button11").addClass('loading').attr('disabled', true);
    	return true;
    });
    
});
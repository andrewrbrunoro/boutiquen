<?php 
class ListaFoto extends ListaDePresenteAppModel {
	/**
	 * Definições de comportamento
	 * 
	 * @var array
	 * @access public
	 */
	public $actsAs = array(
        'MeioUpload' => array(
            'filename' => array(
                'dir' => 'uploads/listas/filename',
                'allowed_mime' => array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/png'),
                'allowed_ext' => array('.jpg', '.jpeg', '.png', '.gif'),
                'fields' => array(
                    'filesize' => 'filesize',
                    'mimetype' => 'mimetype',
                    'dir' => 'dir'
                )
            )
        ),
        'Containable'
	);
	
	/**
	 * Acao que define a imagem como capa
	 *
	 * @param int $id  Identificador da lista
	 * @param int $item_id Identificar da foto
	 * @param bollean $capa Status de capa 
	 * @access public
	 * @return boolean Resultado da operação
	 */
	public function define_capa($id, $item_id, $capa = true){		
		$result = false;
		if ($this->isUsuarioLista($id)) {
			if($capa == true){
				$this->updateAll(array('capa' => 0), array('ListaFoto.lista_id' => $id));
			}
			if($this->save(array('capa' => $capa, 'id' => $item_id), false)){
				$this->_addMensagem('Item salvo com sucesso',self::MENSAGEM_SUCESSO);
				$result = true;
			} else {
				$this->_addMensagem('Erro ao salvar este item',self::MENSAGEM_ERRO);
			}
		} else {
			$this->_addMensagem('Sem permissão de acesso',self::MENSAGEM_ERRO);
		}
	}
		
	// sobrescrita de gancho para ações antes do delete
	public function beforeDelete($cascade = true){
		
		$foto = $this->find('first', array('conditions' => array('id' => $this->id)));
		//a foto atual pertence a uma lista do usuario logado?
		if($this->isUsuarioLista($foto['ListaFoto']['lista_id'])){
			return true;
		}
		
		return false;
	}
}
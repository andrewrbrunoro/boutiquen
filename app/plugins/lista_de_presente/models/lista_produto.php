<?php
class ListaProduto extends AppModel {
	
	/**
	 * Definições de comportamento
	 *
	 * @var array
	 * @access public
	 */
	public $actsAs = array('Containable');

	/**
	 * Definição de relacionamentos de 1 -> 1
	 *
	 * @var array
	 * @access public
	 */
	public $belongsTo = array(
		'Produto' => array(
			'className' => 'Produto',
		),
		'Grade' => array(
			'className' => 'Grade',
		),
	);
	
	/**
	 * Regras de validação
	 *
	 * @var array
	 * @access public
	 */
	public $validate = array(
		'quantidade' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser preenchido',
				'required' => true,
				'allowEmpty' => false,
			),
			array(
				'rule' => 'validaEstoque',
				'message' => 'Não temos essa quantidade em estoque.',
				'required' => true,
				'allowEmpty' => false,
			),
			array(
				'rule' => 'validaEstoqueMinimo',
				'message' => 'A quantidade não pode ser inferior a quantidade já comprada.',
				'required' => true,
				'allowEmpty' => false,
			),
		),
		'grade_id' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser preenchido',
				'required' => true,
				'allowEmpty' => false,
			)
		),
		'produto_id' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser preenchido',
				'required' => true,
				'allowEmpty' => false,
			)
		),
	);

	/**
	 * Realiza a formatação de um item depois da obtenção
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param array $item Dados do item
	 * @access protected
	 * @return array Item formatado
	 */
	protected function formatItem(array $item) {
		// devem ser decodificados os dados das grades?
		if (array_key_exists('Produto',$item) && !empty($item[$this->alias]['grade_id'])) {
			$grades = json_decode($item['Produto']['grades'],true);
			foreach ($grades as $grade) {
				if ($grade['Grade']['id'] == $item[$this->alias]['grade_id']) {
						
					if(isset($grade['GradeImagem']) && is_array($grade['GradeImagem']) && count($grade['GradeImagem']) > 0){
						$func = function($a,$b){
							if ($a['ordem'] < $b['ordem']) {
								return -1;
							} else {
								return 1;
							}
						};
						usort($grade['GradeImagem'],$func);
					}
					$item += $grade;
					break;
				}
			}
		}
		// devem ser decodificados os dados de pedidos?
		if (!empty($item[$this->alias]['pedidos_metadata'])) {
			$item[$this->alias]['pedidos_metadata'] = json_decode($item[$this->alias]['pedidos_metadata'],true);
		}

		return $item;
	}

	/**
	 * Valida estoque mínimo para a solicitação
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param array $quantidade
	 * @access protected
	 * @return bool
	 */
	protected function validaEstoqueMinimo($quantidade) {
		$result = true;
		$quantidade = array_shift($quantidade);
		if (!empty($this->id)) {
			$vendidos = $this->field('quantidade_vendidos');
			if ($vendidos > $quantidade) {
				$result = false;
			}
		}
		return $result;
	}

	/**
	 * Valida se a grade em questão possui estoque
	 *
	 * @param array $quantidade Quantidade de produtos
	 * @access public
	 * @return boolean Resultado da operação
	 */
	public function validaEstoque($quantidade){
		$result = true;
		$quantidade = array_shift($quantidade);
		if(isset($this->data[$this->alias]['grade_id'])){
			if ($this->id) {
				$quantidade -= $this->field('quantidade_vendidos');
			}
			
			$grade = $this->Grade->find('first', array('conditions' => array('Grade.id' => $this->data[$this->alias]['grade_id'])));
			if(!$grade || ($grade['Grade']['quantidade_disponivel'] < $quantidade && $quantidade != 0)){
				$result = false;
			}
		}
		return $result;
	}

	// sobrescrita de método de callback para depois de encontrar resultados
	public function afterFind($results,$primary = false) {
		if (!empty($results[0][$this->alias])) {
			foreach ($results as &$result) {
				$result = $this->formatItem($result);
			}
		} elseif (!empty($results[$this->alias])) {
			$results = array_shift($this->formatItem(array($results)));
		}
		return parent::afterFind($results,$primary);
	}

	// sobrescrita de gancho para antes do salvamento
	public function beforeSave($created) {
		if (array_key_exists('pedidos_metadata',$this->data[$this->alias])) {
			$this->data[$this->alias]['pedidos_metadata'] = json_encode($this->data[$this->alias]['pedidos_metadata']);
		}

		return parent::beforeSave($created);
	}
}

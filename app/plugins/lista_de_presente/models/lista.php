<?php

class Lista extends ListaDePresenteAppModel {
	/**
	 * Constante de status ativo
	 */
	const STATUS_ATIVO = 1;

	/**
	 * Constante de status inativo
	 */
	const STATUS_INATIVO = 0;

	/**
	 * Constante de status ativo
	 */
	const STATUS_REMOVIDO = -1;

	/**
	 * Definições de comportamento
	 *
	 * @var array
	 * @access public
	 */
	public $actsAs = array('Containable');

	/**
	 * Definição de relacionamentos de 1 -> N
	 *
	 * @var array
	 * @access public
	 */
	public $hasMany = array(
		'ListaVideo' => array(
			'className' => 'ListaVideo',
		),
		'ListaFoto' => array(
			'className' => 'ListaFoto',
			'order'	=> 'capa DESC'
		),
		'ListaProduto' => array(
			'className' => 'ListaProduto',
		),
	);

	/**
	 * Definição de relacionamentos de 1 -> 1
	 *
	 * @var array
	 * @access public
	 */
	public $belongsTo = array(
		'ListaTipo' => array(
			'className' => 'ListaTipo',
		),
		'Usuario' => array(
			'className' => 'Usuario',
		),
	);

	/**
	 * Regras de validação
	 *
	 * @var array
	 * @access public
	 */
	public $validate = array(
		'lista_tipo_id' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser preenchido',
			'required' => true,
			'allowEmpty' => false,
		),
		'nome' => array(
			'rule' => 'notEmpty',
			'message' => 'O campo deve ser preenchido',
			'required' => true,
		),
		'data' => array(
			array(
				'rule' => array('date','dmy'),
				'message' => 'Formato de data inválida',
				'required' => true,
			),
			array(
				'rule' => 'notEmpty',
				'message' => 'O campo deve ser preenchido',
				'required' => true,
			)

		),
		'usuario_endereco_id' => array(
            'noempty' => array(
                'rule' => array('validaUsuarioEnderecoId'),
                'message' => 'Campo de preenchimento obrigatório.',
            )
        ),
        'loja_id' => array(
            'noempty' => array(
                'rule' => array('validaLojaId'),
                'message' => 'Campo de preenchimento obrigatório.',
            )
        ),
	);

	/**
	 * Valida a obrigatoriadade do campo endereço
	 *
	 * @return boolean Resultado da operação
	 */
	protected function validaUsuarioEnderecoId(){
		if(isset($this->data[$this->alias]['endereco_proprio']) && $this->data[$this->alias]['endereco_proprio'] == 1 && $this->data[$this->alias]['usuario_endereco_id'] == ""){
			return false;
		}
		return true;
	}
	
	/**
	 * Valida a obrigatoriadade do campo loja
	 *
	 * @return boolean Resultado da operação
	 */
	protected function validaLojaId(){
		if(isset($this->data[$this->alias]['endereco_proprio']) && $this->data[$this->alias]['endereco_proprio'] == 2 && $this->data[$this->alias]['loja_id'] == ""){
			return false;
		}
		return true;
	}

	/**
	 * Adiciona item na lista atual
	 *
	 * @param int $id Identificador da lista
	 * @param array $item_data Conjunto de informações para o cadastro, como id do produto, quantidade, etc.
	 * @return boolean Resultado da operação
	 */
	public function addItem($id, array $item_data = array()) {
		$result = false;
		if ($this->isUsuarioLista($id)) {
			App::import('Model','ListaDePresente.ListaProduto');
			$model = new ListaProduto();

			$item_data['lista_id'] = $id;

			//verifico se a grade já foi add na lista atual
			$grade = $this->ListaProduto->find('first', array('conditions' => array('grade_id' => $item_data['grade_id'], 'lista_id' => $id)));
			if($grade){
				if(!isset($item_data['id']) && $grade['ListaProduto']['status'] == true){
					$item_data['quantidade'] += $grade['ListaProduto']['quantidade'];
				}
				$item_data['status'] = true;
				$item_data['id'] = $grade['ListaProduto']['id'];
			}

			//salvou?
			if($model->save($item_data)){
				$result = true;
				$this->_addMensagem("Item adicionado com sucesso.", self::MENSAGEM_SUCESSO);
			}else{
				foreach($model->invalidFields() as $message){
					$this->_addMensagem($message, self::MENSAGEM_ALERTA);
				}
			}
		} else {
			$this->_addMensagem("Sem permissão de acesso à lista.", self::MENSAGEM_ERRO);
		}

		return $result;
	}

	/**
	 * Remove uma lista
	 *
	 * @param int $id Identificador da lista
	 * @return boolean Resultado da operação
	 */
	public function remove($id) {
		$result = false;
		if ($this->isUsuarioLista($id)) {
			if ($this->save(array('id' => $id, 'status' => self::STATUS_REMOVIDO),false)) {
				$this->_addMensagem('Lista Removida com sucesso', self::MENSAGEM_SUCESSO);
				$result = true;
			} else {
				$this->_addMensagem('Erro ao remover esta lista',self::MENSAGEM_ERRO);
			}
		} else {
			$this->_addMensagem('Sem permissão de acesso',self::MENSAGEM_ERRO);
		}
		return $result;
	}

	/**
	 * Remove item na lista atual
	 *
	 * @param int $id Identificador da lista
	 * @param int $item_id Identificador do item
	 * @return boolean Resultado da operação
	 */
	public function removeItem($id, $item_id){
		$result = false;
		if ($this->isUsuarioLista($id)) {
			App::import('Model','ListaDePresente.ListaProduto');
			$model = new ListaProduto();
			$item = $model->find('first',array('recursive' => -1, 'conditions' => array('lista_id' => $id, 'ListaProduto.id' => $item_id)));
			if ($item) {
				if ($item[$model->alias]['quantidade_vendidos'] > 0) {
					$item[$model->alias]['status'] = false;
					if ($model->save($item,false)) {
						$this->_addMensagem('Item removido com sucesso',self::MENSAGEM_SUCESSO);
						$result = true;
					} else {
						$this->_addMensagem('Erro ao remover este item',self::MENSAGEM_ERRO);
					}
				} else {
					if ($model->delete($item_id)) {
						$this->_addMensagem('Item removido com sucesso',self::MENSAGEM_SUCESSO);
						$result = true;
					} else {
						$this->_addMensagem('Erro ao remover este item',self::MENSAGEM_ERRO);
					}
				}
			} else {
				$this->_addMensagem('Sem permissão de acesso',self::MENSAGEM_ERRO);
			}
		} else {
			$this->_addMensagem('Sem permissão de acesso',self::MENSAGEM_ERRO);
		}
	}

	/**
	 * Ativa a lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @return boolean Resultado da operação
	 */
	public function ativar($id){
		//@todo
	}

	/**
	 * Adiciona foto na lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @param array $data Conjunto de informações para o cadastro
	 * @return boolean Resultado da operção
	 */
	public function addFoto($id, $data){
		if ($this->isUsuarioLista($id)) {
			//@todo
		} else {
			return false;
		}
	}

	/**
	 * Adiciona video na lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @param array $data Conjunto de informações para o cadastro
	 * @return boolean Resultado da operação
	 */
	public function addVideo($id, $data){
		if ($this->isUsuarioLista($id)) {
			//@todo
		} else {
			return false;
		}
	}

	/**
	 * Retorna a lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @param array $params Conjunto de informações para a busca
	 * @return array Conjunto de informações do(s) registro(s)
	 */
	public function getLista($id, array $params = array()){
		$usuarioId = $this->getUsuarioId();
		if (!empty($usuarioId)) {
			$params['conditions']['usuario_id'] = $usuarioId;
			$params['conditions']['Lista.status !='] = self::STATUS_REMOVIDO;
		}else{
			if(!$this->getAdmin()){
				$params['conditions']['Lista.status'] = self::STATUS_ATIVO;
			}
		}
		$params['conditions']['Lista.id'] = $id;
		$params['recursive'] = 0;

		$lista = $this->find('first', $params);

		return $lista;
	}

	/**
	 * Retorna as informações detalhadas a lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @param array $nao_exibir Detalhes da lista que não devem ser detalhados
	 * @return mixed Conjunto de informações do(s) registro(s) | falso se a lista não for ativa
	 */
	public function getListaDetalhada($id, array $nao_exibir = array()) {
		$lista = $this->getLista($id);

		if ($lista) {
			$result = array('lista' => $lista);

			if(!in_array('videos', $nao_exibir)){
				$result['videos'] = $this->_getVideos($id);
			}

			if(!in_array('fotos', $nao_exibir)){
				$result['fotos'] = $this->_getFotos($id);
			}

			if(!in_array('produtos', $nao_exibir)){
				$result['produtos'] = $this->getProdutos($id);
			}
		} else {
			$result = false;
		}

		return $result;
	}

	/**
	 * Retorna os vídeos da lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @return array Conjunto de informações do(s) registro(s)
	 */
	protected function _getVideos($id) {
		$videos = $this->ListaVideo->find('all',array(
			'conditions' => array('lista_id' => $id),
		));
		return $videos;
	}

	/**
	 * Obtenção de fotos de uma lista
	 *
	 * @param int $id Identificador da lista de presente
	 * @access protected
	 * @return array
	 */
	protected function _getFotos($id) {
		$fotos = $this->ListaFoto->find('all',array(
			'conditions' => array('lista_id' => $id),
			'order' => array('capa DESC', 'id ASC')
		));
		return $fotos;
	}

	/**
	 * Retorna os produtos da lista em questão
	 *
	 * @param int $id        Identificador da lista
	 * @param bool $paginado Flag para paginar o resultado
	 * @return array Conjunto de informações do(s) registro(s)
	 */
	public function getProdutos($id,$paginado = true) {
		App::import('Model','ListaDePresente.ListaProduto');
		$model = new ListaProduto();

		$conditions = array('ListaProduto.lista_id' => $id, 'Grade.status' => 1, 'Produto.status' => 1, 'ListaProduto.status' => true);

		if(!$this->getUsuarioId()){
			$conditions['Grade.quantidade_disponivel > '] = 0;

			//$conditions[] = "ListaProduto.quantidade > ListaProduto.quantidade_vendidos";
		}
		if ($paginado) {
			$produtos = $this->getController()->paginate($model,$conditions);
		} else {
			$produtos = $model->find('all',array('conditions' => $conditions));
		}
		
		//debug($conditions);die;

		return $produtos;
	}

	/**
	 * Retorna as listas condicionadas pelos paramentros
	 *
	 * @param array $params Parametros de condições da busca
	 * @return array Conjunto de informações do(s) registro(s)
	 */
	public function getListas(array $params = array()){
		$usuarioId = $this->getUsuarioId();
		if (!empty($usuarioId)) {
			$params['usuario_id'] = $usuarioId;
			$params['Lista.status <>'] = self::STATUS_REMOVIDO;
		}else{
			if(!$this->getAdmin()){
				$params['Lista.status'] = self::STATUS_ATIVO;
			}
		}
		$this->getController()->paginate = array('limit' => 4);
		//$this->getController()->paginate = array('order' => array('ListaFotos.capa DESC'));
		$result = $this->getController()->paginate($this->name,$params);
		return $result;
	}

	/**
	 * Realização de buscas
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @access public
	 *
	 * @param array $searchData Dados para pesquisa
	 * @return array Conjunto de informações paginadas
	 */
	public function busca(array $searchData) {
		$conditions = array();
		// foi passado a palavra chave?
		if (!empty($searchData['keyword'])) {
			$keyword = "%{$searchData['keyword']}%";
			$conditions['AND'][] = array(
				'OR' => array(
					'Lista.nome LIKE' => $keyword,
					'Lista.nome_secundario LIKE' => $keyword,
					'Lista.id' => $searchData['keyword'],
				)
			);
		}

		// foi passada uma data exata?
		if (!empty($searchData['data'])) {
			$conditions['AND'][] = array('DATE(Lista.data)' => date('Y-m-d',strtotime($searchData['data'])));
		}

		// foi passada uma data inicial?
		if (!empty($searchData['data_inicio'])) {
			$conditions['AND'][] = array('DATE(Lista.data) >=' => date('Y-m-d',strtotime($searchData['data_inicio'])));
		}

		// foi pssada uma data final?
		if (!empty($searchData['data_fim'])) {
			$conditions['AND'][] = array('DATE(Lista.data) <=' => date('Y-m-d',strtotime($searchData['data_fim'])));
		}

		return $this->getListas($conditions);
	}

	/**
	 * Adiciona a venda de um item de lista
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param array $itemData Dados do item
	 * @access public
	 * @return Lista Próprio Objeto para encadeamento
	 */
	public function addItemVendido(array $itemData, $origem = 'ecommerce') {
		// carrega a camada de modelo do itens de lista
		App::import('Model','ListaDePresente.ListaProduto');
		$model = new ListaProduto();
		// carrega os dados do item
		$item = $model->find(
			'first',
			array(
				'recursive' => -1,
				'conditions' => array(
					'lista_id' => $itemData['lista_id'],
					'grade_id' => $itemData['grade_id'],
				),
			)
		);
		// item não existe?
		if (!$item) {
			throw new Exception('Não foi encontrado o item da lista de presente');
		};

		// quantidade é maior que 0?
		if ($itemData['quantidade'] < 1) {
			throw new Exception('A quantidade não pode ser menor ou igual a 0 (zero)');
		};

		// atualiza os dados do item
		$item[$model->alias]['quantidade_vendidos'] += $itemData['quantidade'];
		if(empty($itemData['pedido_id'])){
			$itemData['pedido_id'] = count($item[$model->alias]['pedidos_metadata'][$origem])+1;
		}
		$item[$model->alias]['pedidos_metadata'][$origem]["pedido_".$itemData['pedido_id']] = $itemData;
		// não foi possível o salvamento?
		if (!$model->save($item,false)) {
			// registra erro nos logs
			$message = sprintf('Ao salvar o pedido %d, não foi possível atualizar o item #%d da lista de presente ',$itemData['pedido_id'],$item[$model->alias]['id']);
			$this->log($message);
		}

		return $this;
	}

	/**
	 * Remove itens vendidos
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param int $listaId  Identidicador da lista
	 * @param int $pedidoId Identidicador do pedido
	 * @access public
	 * @return Lista Próprio Objeto para encadeamento
	 */
	public function removeItensVendidos($listaId, $pedidoId, $origem = 'ecommerce') {
		// carrega a camada de modelo do itens de lista
		App::import('Model','ListaDePresente.ListaProduto');
		$model = new ListaProduto();

		// obtenção dos itens da lista
		$itens = $model->find('all',array('conditions' => array('lista_id' => $listaId)));
		// varre os itens a procura dos itens que devem ser desalocados
		foreach ($itens as $item) {
			$item = $item['ListaProduto'];

			// houve pedido deste item?
			if (array_key_exists("pedido_".$pedidoId,$item['pedidos_metadata'][$origem])) {

				$itemPedido = $item['pedidos_metadata'][$origem]["pedido_".$pedidoId];
				// desaloca os itens
				$item['quantidade_vendidos'] -= $itemPedido['quantidade'];
				// remove registro do pedido
				unset($item['pedidos_metadata'][$origem]["pedido_".$pedidoId]);
				// salva as iformações
				$model->save($item, false);
			}
		}

		return $this;
	}


	/**
	 * Envio de email de notificação para o dono da lista
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param int $id       Identificador da lista
	 * @param int $pedidoId Identificador do pedido
	 * @access public
	 * @return Lista Próprio Ovjeto para encadeamento
	 */
	public function enviarEmailDeNotificacao($id,$pedidoId) {
		// carrega lista de presentes
		$lista = $this->find('first',array('conditions' => array('Lista.id' => $id),'contain' => array('Usuario','ListaTipo')));
		// o usuário deve ser notificado?
		if ($lista['Lista']['email_notificacao']) {
			// componente de email
			$email = $this->getController()->Email;
			// configurações
			if (Configure::read('Loja.smtp_host') != 'localhost') {
				$email->smtpOptions = array(
					'port' => (int) Configure::read('Loja.smtp_port'),
					'host' => Configure::read('Loja.smtp_host'),
					'username' => Configure::read('Loja.smtp_user'),
					'password' => Configure::read('Loja.smtp_password')
				);
				$email->delivery = 'smtp';
			}
			$email->lineLength = 120;
			$email->sendAs = 'html';
			$email->layout = 'empty';
			$email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
			$email->template = 'lista_notificacao_compra';
			$email->to = $lista['Usuario']['nome'].'<'.$lista['Usuario']['email'].'>';
			$email->subject = 'Compra de itens de sua '.$lista['ListaTipo']['nome'];

			App::import('Model','PedidoItem');
			$model = new PedidoItem();

			// itens do pedido
			$itens = $model->find('all',array('conditions' => array('pedido_id' => $pedidoId),'recursive' => -1));

			// envia o email
			$this->getController()->set('emailData',array('lista' => $lista,'itens' => $itens));
			$email->send();
		}

		return $this;
	}

	/**
	 * Envio de email de notificação de ativação da lista
	 *
	 * @param int $id       Identificador da lista
	 * @access public
	 * @return Lista Próprio Ovjeto para encadeamento
	 */
	public function enviarEmailDeAtivacao($id) {
		App::import('Helper','Html');
		$this->Html = new HtmlHelper();

		// carrega lista de presentes
		$lista = $this->find('first',array('conditions' => array('Lista.id' => $id),'contain' => array('Usuario', 'ListaTipo')));

		// lista valida?
		if ($lista) {
			// componente de email
			$email = $this->getController()->Email;
			// configurações
			if (Configure::read('Loja.smtp_host') != 'localhost') {
				$email->smtpOptions = array(
					'port' => (int) Configure::read('Loja.smtp_port'),
					'host' => Configure::read('Loja.smtp_host'),
					'username' => Configure::read('Loja.smtp_user'),
					'password' => Configure::read('Loja.smtp_password')
				);
				$email->delivery = 'smtp';
			}
			$email->lineLength = 120;
			$email->sendAs = 'html';
			$email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
			$email->to = $lista['Usuario']['nome'].'<'.$lista['Usuario']['email'].'>';
			$email->subject = Configure::read('Loja.nome') . ' - Lista aprovada: '.$lista['Lista']['nome'];

			$url = $this->Html->Url(array(
							'admin' 		=> false,
							'plugin' 		=> 'lista_de_presente',
							'controller' 	=> 'listas',
							'action' 		=> 'detalhe',
							'slug' 			=> strtolower(
												Inflector::slug($lista['ListaTipo']['nome'].'-'.$lista['Lista']['nome'].(!empty($lista['Lista']['nome_secundario']) ? '-'.$lista['Lista']['nome_secundario'] : ''),'-')) ,
							'id' 			=> $lista['Lista']['id']
						), true);


			$content_email = str_replace(array(
		            			'{USUARIO_NOME}',
		            			'{LISTA_NOME}',
		            			'{LISTA_URL}',
		            			'{LOJA_NOME}',
		            		), array(
					           	$lista['Usuario']['nome'],
					           	$lista['Lista']['nome'],
					           	$url,
					            Configure::read('Loja.smtp_assinatura_email'),
		            		), Configure::read('LojaTemplate.ativacao_lista')
						);

			// envia o email
			$email->send($content_email);
		}

		return $this;
	}
	
	/**
	 * Envio de email de notificação de cadastro da lista
	 *
	 * @param int $id       Identificador da lista
	 * @access public
	 * @return Lista Próprio Ovjeto para encadeamento
	 */
	public function enviarEmailNotificacaoCadastro($id) {
		App::import('Helper','Html');
		$this->Html = new HtmlHelper();

		// carrega lista de presentes
		$lista = $this->find('first',array('conditions' => array('Lista.id' => $id),'contain' => array('Usuario', 'ListaTipo')));

		// lista valida?
		if ($lista) {
			// componente de email
			$email = $this->getController()->Email;
			// configurações
			if (Configure::read('Loja.smtp_host') != 'localhost') {
				$email->smtpOptions = array(
					'port' => (int) Configure::read('Loja.smtp_port'),
					'host' => Configure::read('Loja.smtp_host'),
					'username' => Configure::read('Loja.smtp_user'),
					'password' => Configure::read('Loja.smtp_password')
				);
				$email->delivery = 'smtp';
			}
			$email->lineLength = 120;
			$email->sendAs = 'html';
			$email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
			$email->to = Configure::read('Loja.smtp_remetente_email').'<'.Configure::read('Loja.email_compra').'>';
			$email->subject = Configure::read('Loja.nome') . ' - Nova lista aguardando aprovação: '.$lista['Lista']['nome'];

			$url = $this->Html->Url(array(
							'admin' 		=> true,
							'plugin' 		=> 'lista_de_presente',
							'controller' 	=> 'listas',
							'action' 		=> 'edit',
							'id' 			=> $lista['Lista']['id']
						), true);


			$content_email = str_replace(array(
		            			'{USUARIO_NOME}',
		            			'{LISTA_NOME}',
		            			'{LISTA_URL}',
		            			'{LOJA_NOME}',
		            		), array(
					           	$lista['Usuario']['nome'],
					           	$lista['Lista']['nome'],
					           	$url,
					            Configure::read('Loja.smtp_assinatura_email'),
		            		), Configure::read('LojaTemplate.cadastro_lista')
						);

			// envia o email
			$email->send($content_email);
		}

		return $this;
	}

	// sobrescrita de gancho para ações antes do salvamento
	public function beforeSave(){
		App::import("helper", "Calendario");
		$this->Calendario = new CalendarioHelper();
		
		//formatacao de data e fora
		if (isset($this->data[$this->alias]['data']) && isset($this->data[$this->alias]['hora'])) {
			$data_tmp = $this->data[$this->alias]['data'].$this->data[$this->alias]['hora'];
			$this->data[$this->alias]['data'] = $this->Calendario->dataFormatada("Y-m-d H:m:s", $data_tmp);
		}

		//remove  o id da loja se o id do endereco for definido
		if (isset($this->data[$this->alias]['usuario_endereco_id']) && $this->data[$this->alias]['usuario_endereco_id'] != "") {
			$this->data[$this->alias]['loja_id'] = null;
		}
		
		//remove o id do endereco se o id da loja for definido
		if (isset($this->data[$this->alias]['loja_id']) && $this->data[$this->alias]['loja_id'] != "") {
			$this->data[$this->alias]['usuario_endereco_id'] = null;
		}
		
		//se nem endereco id, nem loja id for passado, forco ambos como null
		if (
			(!isset($this->data[$this->alias]['loja_id']) || $this->data[$this->alias]['loja_id'] == "")
			&&
			(!isset($this->data[$this->alias]['usuario_endereco_id']) || $this->data[$this->alias]['usuario_endereco_id'] == "")
			) {
			$this->data[$this->alias]['loja_id'] = null;
			$this->data[$this->alias]['usuario_endereco_id'] = null;
		}

		//seto o status como inativo quando não for passado
		/*
		if(!isset($this->data[$this->alias]['status'])){
					$this->data[$this->alias]['status'] = self::STATUS_INATIVO;
				}*/

		if(!$this->id){
			//seto o id do usuario no registro da grade (created)
			$this->data[$this->alias]['usuario_id'] = $this->getUsuarioId();
			$this->data[$this->alias]['status'] = self::STATUS_INATIVO;
		}

		return parent::beforeSave();
	}

	// sobrescrita de método de callback para depois de encontrar resultados
	public function afterFind($results,$primary = false) {
		if(is_array($results)){
			foreach ($results as &$result) {
				if (isset($result[$this->alias]['usuario_endereco_id']) && !empty($result[$this->alias]['usuario_endereco_id'])) {
					$result[$this->alias]['endereco_proprio'] = 1;
				}
				if (isset($result[$this->alias]['loja_id']) && !empty($result[$this->alias]['loja_id'])) {
					$result[$this->alias]['endereco_proprio'] = 2;
				}
			}
		}
		return parent::afterFind($results,$primary);
	}
}

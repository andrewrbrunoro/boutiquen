<div class="index">
    <h2 class="left"><?php __('Tipos de Listas'); ?></h2>
    <div class="btAddProduto">
        <?php echo $this->Html->link(__('[+] Adicionar Tipo de Lista', true), array('action' => 'edit')); ?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('Nome','nome'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>
            <th class="actions">Ações</th>
        </tr>
<?php $i = 0; ?>
<?php foreach ($tipos_lista as $tipo_lista) : ?>
<?php
    $class = null;
    if ($i++ % 2 == 0) {
        $class = ' class="altrow"';
    }
?>
        <tr<?php echo $class; ?>>
            <td align="center"><?php echo $tipo_lista['ListaTipo']['id']; ?>&nbsp;</td>
            <td align="center"><?php echo $tipo_lista['ListaTipo']['nome']; ?>&nbsp;</td>
            <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $tipo_lista['ListaTipo']['created']); ?>&nbsp;</td>
            <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $tipo_lista['ListaTipo']['modified']); ?>&nbsp;</td>
            <td class="actions">
                <?php echo $this->Html->link(__('Editar', true), array('action' => 'edit', $tipo_lista['ListaTipo']['id'])); ?>
                <?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $tipo_lista['ListaTipo']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $tipo_lista['ListaTipo']['id'])); ?>
            </td>
        </tr>
<?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, exibindo %current% no total de %count%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('Anterior', true), array(), null, array('class' => 'disabled')); ?>
        | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('Próximo', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
</div>

<div class="index">
<?php echo $this->Form->create('ListaTipo',array('url' => $this->Html->url(null,true))) ?>
    <fieldset>
        <legend>Tipo de Lista</legend>
<?php echo $this->Form->input('id') ?>
<?php echo $this->Form->input('status', array('default' => true, 'type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
<?php echo $this->Form->input('nome') ?>
	</fieldset>
<?php echo $this->Form->end('Salvar') ?>
</div>

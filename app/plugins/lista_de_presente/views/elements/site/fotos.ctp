<!-- start entry -->
<div class="gallery">
	<ul>
		<?php if(isset($lista['fotos']) && is_array($lista['fotos']) && count($lista['fotos']) > 0): ?>
			<?php foreach($lista['fotos'] as $foto): ?>
			<li>
				<div class="video">
					<?php 
						$img = ( isset($foto['ListaFoto']['filename']) && file_exists($foto['ListaFoto']['dir'].'/'.$foto['ListaFoto']['filename']) ) ? $foto['ListaFoto']['dir'].'/'.$foto['ListaFoto']['filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg"; 
						$url = isset($foto['ListaFoto']['filename']) ?  $this->Html->Url('/', true).str_replace('\\', '/',$foto['ListaFoto']['dir']).'/'.$foto['ListaFoto']['filename'] : '';
					?>
					<a href="<?php echo $url ?>" class="nyroModalFoto">
						<?php echo $image->resize($img, 220, 153); ?>
					</a>
					<p>Clique na foto para visualizar.</p>
				</div>
			</li>
			<?php endforeach; ?>
		<?php else: ?>
			<li>Nenhuma foto encontrada.</li>
		<?php endif; ?>
	</ul>
</div>
<!-- end gallery -->

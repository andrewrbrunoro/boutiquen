<?php
	$img = null;
	if (!empty($lista['ListaFoto'])) {
		if (
			!empty($lista['ListaFoto'][0]['dir']) &&
			!empty($lista['ListaFoto'][0]['filename'])
		) {
			$img = $lista['ListaFoto'][0]['dir'] .'/'. $lista['ListaFoto'][0]['filename'];
		}
	}
?>
<!-- start imgb -->
<div class="imgb">
<?php echo $this->Image->resize($img,220,153) ?>
</div>
<!-- start imgb -->
<!-- start txtb -->
<div class="txtb">
    <h3><?php echo $lista['ListaTipo']['nome'] ?>: <?php echo $lista['Lista']['nome'] ?>
<?php if (!empty($lista['Lista']['nome_secundario'])) : ?>
			e <?php echo $lista['Lista']['nome_secundario'] ?>
<?php endif ?></h3>
    <p>
    	Data do Evento: <?php echo $this->Calendario->dataFormatada('d/m/Y',$lista['Lista']['data']) ?><span>Hora do evento: <?php echo $this->Calendario->dataFormatada('H:m',$lista['Lista']['data']) ?></span> Local do evento: <?php echo $lista['Lista']['local'] ?> 
        <?php if($this->params['action'] == "minhas_listas"): ?>
        	<span>Status: <?php echo ($lista['Lista']['status']) ? "Publicado" : "Inativo"; ?></span>
        <?php endIf; ?>
    </p>
</div>
<!-- end txtb -->
<a href="<?php echo $this->Html->Url($url_botao); ?>" class="link" tutle="ver a lista">ver a lista</a>
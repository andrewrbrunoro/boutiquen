<?php if(isset($duvidas_frequentes) && count($duvidas_frequentes) > 0): ?>
<!-- start sub-nav"-->
<div class="sub-nav sub-nav2">
	<h3>DÚVIDAS</h3>
	<ul class="duvida-frequente">
		<?php foreach($duvidas_frequentes['DuvidaFrequente'] as $duvida_frequente): ?>
			<li>
				<a href="javascript:void(0);" title="<?php echo $duvida_frequente['pergunta']; ?>"><?php echo $duvida_frequente['pergunta']; ?></a>
				<p class="duvida_frequente_resposta"><?php echo $duvida_frequente['resposta']; ?></p>
			</li>	
		<?php endforeach; ?>
	</ul>
</div>
<!-- end sub-nav"-->
<?php endif; ?>
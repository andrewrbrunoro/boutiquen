<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/detalhe.js', false) ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/index.js', false); ?>

<?php if (isset($this->Paginator) && $this->Paginator->counter(array('format' => '%pages%')) > 1) : ?>
	<!-- start pagination-->
	<div class="pagination margin_none right">
		<ul>
			<li>
<?php echo $this->Paginator->prev('Anterior',array('class' => 'prev'),null, array('class' => 'prev')) ?>
			</li>
<?php echo $this->Paginator->numbers(array('tag' => 'li','separator' => null)) ?>
			<li>
<?php echo $this->Paginator->next('Próximo',array('class' => 'next'),null, array('class' => 'next')) ?>
			</li>
		</ul>
	</div>
	<!-- end pagination-->
<?php endif ?>

<div class="clear"></div>

<!-- start create-list-->
<div class="banner create-list background" style="margin-top: 20px;">
	<!-- start column-left -->
	<div class="column-left">
		<h2>Lista de Presentes</h2>
<?php echo $this->element('site/duvidas_frequentes') ?>
	</div>
	<!-- end column-left -->
	<!-- start column-right -->
		<?php echo $content_to_element; ?>
	<!-- end column-right -->
	<div class="clear"></div>
</div>
<!-- end create-list -->

<div class="clear"></div>
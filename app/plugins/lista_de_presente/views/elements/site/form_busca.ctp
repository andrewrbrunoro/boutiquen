<?php echo $this->Javascript->link('/lista_de_presente/js/site/lista/busca.js') ?>
<!-- start row1 -->
<div class="row1">
	<h3>Encontre uma lista</h3>
	<p>Nome dos organizadores / Número da lista</p>
<?php echo $this->Form->create(null,array('url' => array('plugin' => 'lista_de_presente','controller' => 'listas','action' => 'busca'))) ?>
<?php echo $this->Form->input('Busca.keyword',array('div' => false,'label' => false,'class' => 'input')); ?>
	<div class="clear"></div>
	<div class="busca-normal">
		<p>Data do evento</p>
<?php echo $this->Form->input('Busca.data',array('div' => false,'label' => false,'class' => 'input date')); ?>
		<div class="clear"></div>
	</div>
<?php echo $this->Form->input('Busca.avancada',array('type' => 'hidden')) ?>
	<div class="busca-avancada">
		<div class="element">
		<p>Período</p>
		<span>De:</span>
<?php echo $this->Form->input('Busca.data_inicio',array('div' => false,'label' => false,'class' => 'input input-data date')); ?>
		</div>
		<div class="element">
		<p>&nbsp;</p>
		<span>até</span>
<?php echo $this->Form->input('Busca.data_fim',array('div' => false,'label' => false,'class' => 'input date input-data')); ?>
		</div>
		<div class="clear"></div>
		<p>Tipo de Lista</p>
<?php echo $this->Form->input('Busca.lista_tipo',array('div' => false,'label' => false,'class' => 'select2','options' => array('' => 'Selecione')+$lista_tipos)) ?>
		<div class="clear"></div>
	</div>
	<a class="alteraBusca" href="">Busca Avançada</a>
	<input type="submit" name="submit" value="Buscar" class="button-lista" />
<?php echo $this->Form->end() ?>
	<div class="clear"></div>
</div>
<!-- end row1 -->

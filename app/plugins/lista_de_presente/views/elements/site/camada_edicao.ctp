<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<script src="https://maps.googleapis.com/maps/api/js?sensor=false" rel="forceload" rev="shown"></script>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/detalhe.js', false) ?>

<!-- start present -->
<div class="present">
    <h2>Minha Lista de Presentes</h2>
    <!-- start imgb -->
    <div class="imgb">
<?php
	$img = null;
	if (!empty($lista['fotos'])) {
		if (
			!empty($lista['fotos'][0]['ListaFoto']['dir']) &&
			!empty($lista['fotos'][0]['ListaFoto']['filename'])
		) {
			$img = $lista['fotos'][0]['ListaFoto']['dir'] .'/'. $lista['fotos'][0]['ListaFoto']['filename'];
		}
	}
?>
<?php echo $this->Image->resize($img,220,153) ?>
    </div>
    <!-- start imgb -->
    <!-- start txtb -->
    <div class="txtb">
    	<h3>
			<?php echo $lista['lista']['ListaTipo']['nome'] ?>: <?php echo $lista['lista']['Lista']['nome'] ?>
<?php if (!empty($lista['lista']['Lista']['nome_secundario'])) : ?>
			e <?php echo $lista['lista']['Lista']['nome_secundario'] ?>
<?php endif ?>
		</h3>
		<p>Data do Evento: <?php echo $this->Calendario->dataFormatada('d/m/Y',$lista['lista']['Lista']['data']) ?><span>Hora do evento: <?php echo $this->Calendario->dataFormatada('H:m',$lista['lista']['Lista']['data']) ?></span> Local do evento: <span style="display: inline-block;" id="local"><a href="#map_evento_box" class="nyroModalMap"><?php echo $lista['lista']['Lista']['local'] ?></a></span></p>
    	<?php if($lista['lista']['Lista']['site'] != ""): ?>
			<?php 
				if(strpos("http://", $lista['lista']['Lista']['site'])){
					$url = $lista['lista']['Lista']['site'];
				}else{
					$url = "http://".$lista['lista']['Lista']['site'];
				}
			?>
			<p>Site do Evento: <a href="<?php echo $url; ?>" target="_blank"><?php echo $lista['lista']['Lista']['site']; ?></a></p>
		<?php endif; ?>
    </div>
    <!-- end txtb -->
    
    <!-- begin mapa evento -->
    <div id="map_evento_box" style="display: none; float: left;">
		
	</div>
    <!-- end mapa evento -->
    
    <!-- start present-navigation -->
    <div class="present-navigation">
    	<ul>
        	<li class="active"><a href="#" title="EDITAR">editar</a></li>
        </ul>
    </div>
    <!-- end present-navigation -->
    <!-- start content -->
    <div class="content create-list">
    	<!-- start leftcol -->
        <div class="leftcol">
        	<!-- start product-link -->
            <div class="product-link">
                <ul>
                    <li><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'minhas_listas')); ?>" title="MINHAS LISTAS">MINHAS LISTAS</a></li>
                    <li class="active"><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'adicionar_produtos', $this->params['pass'][0])); ?>" title="ADICIONAR PRODUTOS">adicionar produtos</a></li>
                  	<li><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'editar', $this->params['pass'][0])); ?>" title="PRODUTOS ADICIONADOS">PRODUTOS ADICIONADOS</a></li>
                    <li><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'lista_fotos', 'action' => 'adicionar', $this->params['pass'][0])); ?>" title="ADICIONAR FOTOS">adicionar fotos</a></li>
                    <li><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'lista_videos', 'action' => 'adicionar', $this->params['pass'][0])); ?>" title="ADICIONAR VÍDEOS">adicionar vídeos</a></li>
                    <li><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'criar', $this->params['pass'][0])); ?>" title="ALTERAR DADOS">alterar dados</a></li>
                    <li><a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'apagar_lista', $this->params['pass'][0])); ?>" onclick="return confirm('Deseja realmente remover esta lista?')" title="APAGAR LISTA">apagar lista</a></li>
                </ul>
            </div>
            <!-- end product-link -->
        </div>
        <!-- end leftcol -->
        <!-- start rightcol -->
        <div class="rightcol">
        	<!-- start top -->
			<div class="top">
<?php if (isset($this->Paginator) && $this->Paginator->counter(array('format' => '%pages%')) > 1) : ?>
			<!-- start pagination-->
			<div class="pagination pagination2 pagination3">
				<ul>
					<li>
<?php echo $this->Paginator->prev('Anterior',array('class' => 'prev'),null, array('class' => 'prev')) ?>
					</li>
<?php echo $this->Paginator->numbers(array('tag' => 'li','separator' => null)) ?>
					<li>
<?php echo $this->Paginator->next('Próximo',array('class' => 'next'),null, array('class' => 'next')) ?>
					</li>
				</ul>
			</div>
			<!-- end pagination-->
<?php endif ?>
				<!-- start prise button -->
				<div class="price-button btn-print">
				<a href="<?php echo $this->Html->url(array('controller' => 'listas','action' => 'imprimir',$lista['lista']['Lista']['id'],(isset($usuario_id) ? $usuario_id : null))) ?>" target="_blank" title="Imprimir Lista">imprimir lista</a>
				</div>
				<!-- end price button -->
			</div>
			<!-- end top -->
			<div class="clear"></div>
            <?php echo $content_to_element; ?>
        </div>
        <!-- end rightcol -->
    </div>
    <!-- end content -->
</div>
<!-- end present -->
<div class="clear"></div>

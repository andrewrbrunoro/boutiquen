<?php $img = ( isset($produto[0]['imagem']) && file_exists($produto[0]['imagem']) ) ? $produto[0]['imagem'] : "uploads/produto_imagem/filename/sem_imagem.jpg"; ?>
<h4><a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['Produto']['grade_id']); ?>.html" title="<?php echo $produto['Produto']['nome'] ?>"><?php echo $produto['Produto']['nome'] ?></a></h4>
<div class="product-view">
    <a class="imagem" href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['ListaProduto']['grade_id']); ?>.html" title="<?php echo $produto['Produto']['nome']; ?>">
        <?php echo $image->resize($img, 159, 156, true, array('alt' => $produto['Produto']['nome'])); ?>
    </a>
</div>
<?php
$preco = ($produto['Produto']['preco_promocao'] > 0) ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
$parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento);
?>
<a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['ListaProduto']['grade_id']); ?>.html" title="<?php echo $produto['Produto']['nome'] ?>">
    <p class="details">
        <?php if ($this->String->bcoToMoeda($produto['Produto']['preco_promocao']) > 0): ?>
            De: <s>R$ <?php echo $this->String->bcoToMoeda($produto['Produto']['preco']); ?></s>
        <span>Por: R$ <?php echo $this->String->bcoToMoeda($produto['Produto']['preco_promocao']) ?></span>
    <?php else: ?>
        <span>R$ <?php echo $this->String->bcoToMoeda($produto['Produto']['preco']) ?></span>
    <?php endif; ?>
    <?php if ($parcela_valida['parcela'] > 1): ?>
        <span>ou <?php echo $parcela_valida['parcela']; ?>x de R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor']) ?></span>
        <span><?php echo ($parcela_valida['juros'] == true) ? 'com' : 'sem'; ?> juros</span>
    <?php endif; ?>
    </p>
</a>
<?php  if ($produto['Produto']['quantidade_disponivel'] > 0 && $produto['Produto']['status'] != 2): ?>
    <a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['ListaProduto']['grade_id']); ?>.html" title="ver detalhes" class="button3">ver detalhes</a>
<?php else: ?>
    <a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['ListaProduto']['grade_id']); ?>.html" title="produto esgotado" class="button3">esgotado</a>
<?php endIf; ?>
<!-- start product-box -->
<div class="product-box">
<!-- start top -->
<div class="top">
	<div class="style-select style2">
<?php
$ordenacao = array(
	'sort:Grade.preco/direction:asc' => 'Menor preço',
	'sort:Grade.preco/direction:desc' => 'Maior preço',
	'sort:Produto.nome/direction:asc' => 'Ordenar A-Z',
	'sort:Produto.nome/direction:desc' => 'Ordenar Z-A',
	'sort:Produto.quantidade_acessos/direction:desc' => 'Mais acessados',
	'sort:Produto.quantidade_vendidos/direction:desc' => 'Mais vendidos',
);
?>
<?php
echo $this->Form->input('order', array(
	'label' => false,
	'div' => false,
	'class' => 'select',
	'id' => 'ordenacao',
	'rel' => $this->Html->url(),
	'value' => (isset($this->params['named']['sort']) && isset($this->params['named']['direction'])) ? 'sort:' . $this->params['named']['sort'] . '/direction:' . $this->params['named']['direction'] : false,
	'options' => array('' => 'Ordenar por') + $ordenacao)
);
?>
	</div>
	<!-- start prise button -->
	<div class="price-button">
		<a href="<?php echo $this->Html->url(array('controller' => 'listas','action' => 'imprimir',$lista['lista']['Lista']['id'],(isset($usuario_id) ? $usuario_id : null))) ?>" target="_blank" title="Imprimir Lista">imprimir lista</a>
	</div>
	<!-- end price button -->
<?php if ($this->Paginator->counter(array('format' => '%pages%')) > 1) : ?>
	<!-- start pagination-->
	<div class="pagination pagination2">
		<ul>
			<li>
<?php echo $this->Paginator->prev('Anterior',array('class' => 'prev'),null, array('class' => 'prev')) ?>
			</li>
<?php echo $this->Paginator->numbers(array('tag' => 'li','separator' => null)) ?>
			<li>
<?php echo $this->Paginator->next('Próximo',array('class' => 'next'),null, array('class' => 'next')) ?>
			</li>
		</ul>
	</div>
	<!-- end pagination-->
<?php endif ?>
	</div>
	<!-- end top -->
	<div class="clear"></div>
	<!-- start product list -->
	<ul class="product-list">
<?php if(count($lista['produtos']) > 0): ?>
<?php foreach ($lista['produtos'] as $produto)  : ?>
		<li>
			<!-- start left -->
			<div class="left">
<?php
$img = null;
if (!empty($produto['GradeImagem'])) {
	if (!empty($produto['GradeImagem'][0]['dir']) && !empty($produto['GradeImagem'][0]['filename'])) {
		$img = $produto['GradeImagem'][0]['dir'].'/'.$produto['GradeImagem'][0]['filename'];
	}
}
?>
<div class="lista_produtos box_imagens">
<?php echo $this->Image->resize($img,140,150,true,array('class' => 'img')) ?>
</div>
				<!-- start interest -->
				<div class="interest">
				<p class="heading"><?php echo $produto['Produto']['nome'] ?></p>
<?php $preco = $produto['Grade']['preco'] ?>
<?php if ($produto['Grade']['preco_promocao'] > 0) : ?>
					<p class="txt">De: R$ <?php echo $this->String->bcoToMoeda($produto['Grade']['preco']) ?></p>
<?php $preco = $produto['Grade']['preco_promocao'] ?>
<?php endif ?>
					<p class="txt2">Por: <span>R$ <?php echo $this->String->bcoToMoeda($preco) ?></span> ou</p>
<?php $parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento); ?>
<?php if ($parcela_valida['parcela'] > 1): ?>
					<p class="txt2"><?php echo $parcela_valida['parcela']; ?>x de <span>R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor']) ?></span>  <?php echo ($parcela_valida['juros'] == true) ? 'com' : 'sem'; ?> juros</p>
<?php endIf; ?>
				</div>
				<!-- end interest -->
			</div>
			<!-- end left -->
			<!-- start right -->
<?php echo $this->Form->create(null,array('url' => array('plugin' => 'carrinho','controller' => 'carrinho','action' => 'add'))) ?>
<?php echo $this->Form->input('Compra.produto_id',array('value' => $produto['Produto']['id'],'type' => 'hidden')) ?>
<?php echo $this->Form->input('Compra.grade_id',array('value' => $produto['Grade']['id'],'type' => 'hidden')) ?>
<?php echo $this->Form->input('Compra.lista_presente',array('value' => $lista['lista']['Lista']['id'],'type' => 'hidden')) ?>
			<div class="right">
			  <p class="heading">Quantidade</p>
				<div class="clear"></div>
				<p class="quantity quantidade"><?php echo $produto['ListaProduto']['quantidade'] ?></p>
				<!-- button start -->
				<div class="button">
					<input type="submit" title="COMPRAR" value="COMPRAR"/>
				</div>
			</div>
			<!-- button end -->
			<!-- end right -->
			<!-- start right -->
			<div class="right quantity-padding">
			  <p class="heading">Comprados</p>
			  <div class="clear"></div>
				<p class="quantity quantidade-vendidos"><?php echo $produto['ListaProduto']['quantidade_vendidos'] ?></p>
			</div>
			<!-- end right -->
			<!-- start right -->
			<div class="right quantity-padding">
				<p class="heading">Solicitados</p>
				<div class="clear"></div>
<?php echo $this->Form->input('Compra.quantidade',array('type' => 'text','div' => false, 'label' => false, 'class' => 'input')) ?>
				<a href="#" title="link" class="up">+</a><br /><br />
				<a href="#" title="link" class="down">-</a>
			</div>
			<!-- end right -->
<?php echo $this->Form->end() ?>
			<div class="clear"></div>
		</li>
<?php endforeach ?>
<?php else: ?>
	<li><p style='margin-left: 15px;'>Nenhum produto disponível.</p></li>
<?php endIf; ?>
	 </ul>
	<!-- end product list -->
</div>
<!-- end product box -->

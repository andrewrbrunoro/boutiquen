<!-- start element_lista_cabecalho -->
<div class="element_lista_cabecalho">
	<!-- start present -->
	<div class="present">
	    <h2>Edição da lista de presentes</h2>
	    <!-- start imgb -->
	    <div class="imgb">
<?php
	$img = null;
	if (!empty($lista['fotos'])) {
		if (
			!empty($lista['fotos'][0]['ListaFoto']['dir']) &&
			!empty($lista['fotos'][0]['ListaFoto']['filename'])
		) {
			$img = $lista['fotos'][0]['ListaFoto']['dir'] .'/'. $lista['fotos'][0]['ListaFoto']['filename'];
		}
	}
?>
<?php echo $this->Image->resize($img,115,115) ?>
	    </div>
	    <!-- start imgb -->
	    <!-- start txtb -->
	    <div class="txtb">
	        <h3>
<?php echo $lista['lista']['ListaTipo']['nome'] ?>: <?php echo $lista['lista']['Lista']['nome'] ?>
<?php if (!empty($lista['lista']['Lista']['nome_secundario'])) : ?>
			e <?php echo $lista['lista']['Lista']['nome_secundario'] ?>
<?php endif ?>
			</h3>
			<p>Data do Evento: <?php echo $this->Calendario->dataFormatada('d/m/Y',$lista['lista']['Lista']['data']) ?><span>Hora do evento: <?php echo $this->Calendario->dataFormatada('H:m',$lista['lista']['Lista']['data']) ?></span> Local do evento: <?php echo $lista['lista']['Lista']['local'] ?></p>
	    </div>
	    <!-- end txtb -->
	    <a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'editar', $lista['lista']['Lista']['id'])); ?>" class="button1">ENCERRAR EDIÇÃO</a>
	</div>
	<!-- end present -->
</div>
<!-- end element_lista_cabecalho -->
<!-- start entry -->
<div class="gallery">
	<ul>
		<?php if(isset($lista['videos']) && is_array($lista['videos']) && count($lista['videos']) > 0): ?>
			<?php foreach($lista['videos'] as $video): ?>
			<li>
				<div class="video">
					<a href="<?php echo $video['ListaVideo']['url']; ?>" class="nyroModalVideo">
						<?php $img = ( isset($video['ListaVideo']['thumb_filename']) && file_exists($video['ListaVideo']['thumb_dir'].'/'.$video['ListaVideo']['thumb_filename']) ) ? $video['ListaVideo']['thumb_dir'].'/'.$video['ListaVideo']['thumb_filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg"; ?>
						<?php echo $image->resize($img, 220, 153); ?>
					</a>
					<p>Clique no vídeo para visualizar.</p>
				</div>
			</li>
			<?php endforeach; ?>
		<?php else: ?>
			<li>Nenhum vídeo encontrado.</li>
		<?php endif; ?>
	</ul>
</div>
<!-- end gallery -->

<?php $javascript->link('common/swfobject.js', false); ?>
<?php $javascript->link('common/jquery.uploadify.v2.1.4.min', false); ?>
<?php $javascript->link('/lista_de_presente/js/site/lista_fotos/adicionar.js', false); ?>
<?php ob_start(); ?>
<div class="lista-fotos-content">
	<h2>Adicionar fotos</h2>
    <input id="lista-id" type="hidden" value="<?php echo $this->params['pass'][0]; ?>" />
    <input id="edit-produto" name="file_upload" type="file" />
    <div class="lista-fotos">
        <div class="container-edit">
            <?php 
                if(isset($imgs)){
                    echo $imgs;
                }
            ?>
        </div>
    </div>
</div>
<?php $content = ob_get_clean(); ?>
<?php echo $this->Element('/site/camada_edicao', array('content_to_element' => $content)); ?>
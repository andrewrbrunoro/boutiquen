<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/admin/lista/crud.js', false); ?>
<?php echo $this->Html->css('admin/style'); ?>
<div class="index">
    <?php echo $this->Form->create('Lista', array('url' => array('plugin' => 'lista_de_presente', 'admin' => true, 'controller' => 'listas', 'action' => 'adicionar_compra', $this->data['ListaProduto']['lista_id'], $this->data['ListaProduto']['id']))); ?>
    
    <!-- Begin Ecommerce -->
    <?php 
    	$pedido_ecommerce = array();
		if(isset($item['ListaProduto']['pedidos_metadata']['ecommerce'])){
    		$pedido_ecommerce = $item['ListaProduto']['pedidos_metadata']['ecommerce'];
		}
    ?>
    
    <table>
    	<tr>
    		<th colspan="4">Vendas: Ecommerce</th>
    	</tr>
    	<tr>
    		<th>Pedido ID</th>
    		<th>Quantidade</th>
    		<th>Ação</th>
    	</tr>
    	<?php if(isset($pedido_ecommerce) && count($pedido_ecommerce) > 0): ?>
	    	<?php foreach($pedido_ecommerce as $k => $pe): ?>
	    	<tr>
	    		<td align="center"><a href="<?php echo $this->Html->Url(array('plugin' => false, 'admin' => true, 'controller' => 'pedidos', 'action' => 'view', $pedido_ecommerce[$k]['pedido_id'])); ?>" target="_blank"><?php echo $pedido_ecommerce[$k]['pedido_id']; ?></a></td>
	    		<td align="center"><?php echo $pedido_ecommerce[$k]['quantidade']; ?></td>
	    		<td align="center"><a href="javascript:void(0);" class="rm rm-compra" data-tipo="ecommerce" data-lista-id="<?php echo $this->data['ListaProduto']['lista_id']; ?>" data-pedido-id="<?php echo $pedido_ecommerce[$k]['pedido_id']; ?>">Remover compra</a></td>
	    	</tr>
	    	<?php endforeach; ?>
	    <?php else: ?>
	    	<tr>
	    		<td colspan="3" align="center">Nenhum pedido realizado.</td>
	    	</tr>
		<?php endif; ?>
    </table>
    <!-- End Ecommerce -->
    
    <div class="clear"></div>
    <br />
    <div class="clear"></div>
    
    <!-- Begin Loja -->
    <?php 
    	$pedido_loja = array();
		if(isset($item['ListaProduto']['pedidos_metadata']['loja'])){
    		$pedido_loja = $item['ListaProduto']['pedidos_metadata']['loja'];
		} 
	?>
    
    <table>
    	<tr>
    		<th colspan="4">Vendas: Loja</th>
    	</tr>
    	<tr>
    		<th>Pedido ID</th>
    		<th>Quantidade</th>
    		<th>Ação</th>
    	</tr>
    	<?php if(isset($pedido_loja) && count($pedido_loja) > 0): ?>
	    	<?php foreach($pedido_loja as $k => $pe): ?>
	    	<tr>
	    		<td align="center"><?php echo $pedido_loja[$k]['pedido_id']; ?></td>
	    		<td align="center"><?php echo $pedido_loja[$k]['quantidade']; ?></td>
	    		<td align="center"><a href="javascript:void(0);" class="rm rm-compra" data-tipo="loja" data-lista-id="<?php echo $this->data['ListaProduto']['lista_id']; ?>" data-pedido-id="<?php echo $pedido_loja[$k]['pedido_id']; ?>">Remover compra</a></td>
	    	</tr>
	    	<?php endforeach; ?>
	    <?php else: ?>
	    	<tr>
	    		<td colspan="3" align="center">Nenhum pedido realizado.</td>
	    	</tr>
		<?php endif; ?>
    </table>
	<!-- End Loja -->
	
	<div class="clear"></div>
    <br />
    <div class="clear"></div>
    
    <fieldset>
        <legend><?php __('Adicionar Venda na Loja'); ?></legend>
        <p><?php echo $this->Session->flash(); ?></p>
        <div class="left clear">
        	<?php echo $this->Form->hidden("ListaProduto.id"); ?>
        	<?php echo $this->Form->hidden("ListaProduto.grade_id"); ?>
        	<?php echo $this->Form->hidden("ListaProduto.lista_id"); ?>
        	<?php echo $this->Form->input("ListaProduto.quantidade", array('label' => 'Quantidade', 'class' => 'inputs w147 number', 'value' => 1)); ?>
		</div>
		<?php echo $this->Form->end(__('Salvar', true)); ?>
	</fieldset>
	<?php echo $this->Form->end(); ?>
</div>
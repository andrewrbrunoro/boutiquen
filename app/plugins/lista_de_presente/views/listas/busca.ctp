<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/index.js', false); ?>
<?php ob_start(); ?>
<div class="column-right margin_none lista-busca">
	<!-- start find-form -->
	<div class="find-form">
<?php echo $this->element('site/form_busca') ?>
	</div>
	<h4>Busca</h4>
	<ul class="detail">
<?php if(isset($listas) && count($listas) > 0): ?>
<?php foreach($listas as $lista): ?>
		<li>
<?php echo $this->element('site/busca', array('lista' => $lista, 'url_botao' => array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'detalhe','slug' => strtolower(Inflector::slug($lista['ListaTipo']['nome'].'-'.$lista['Lista']['nome'].(!empty($lista['Lista']['nome_secundario']) ? '-'.$lista['Lista']['nome_secundario'] : ''),'-')) , 'id' => $lista['Lista']['id']))); ?>
		</li>
<?php endforeach; ?>
<?php else: ?>
		<li>Nenhuma lista encontrada</li>
<?php endif; ?>
	</ul>
</div>
<div class="clear"></div>
<?php $content = ob_get_clean(); ?>
<?php echo $this->Element('site/camada_default', array('content_to_element' => $content)); ?>

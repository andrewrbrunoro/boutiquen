<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/criar.js', false); ?>
<?php ob_start(); ?>
<div class="column-right">
	<!-- start create-list-form -->
	<div class="create-list-form">
		<h3>Crie sua Lista</h3>
		<?php echo $this->Form->create('Lista', array('url' => array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'criar'), 'type' => 'file')); ?>
			<!-- start row-1 -->
			<div class="row-1 row-1-custom-select">
				<div class="style-select style3">
					<?php echo $this->Form->input('id'); ?>
					<?php echo $this->Form->input('lista_tipo_id', array('div' => false, 'class' => 'select', 'label' => 'Tipo de lista *', 'options' => array('' => 'Selecione...')+$listaTipos)); ?>
				</div>
			</div>
			<!-- end row-1 -->
			<!-- start row-1 -->
			<div class="row-1">
				<?php echo $this->Form->input('nome', array('label' => 'Nome do Organizador Principal *')); ?>
			</div>
			<!-- end row-1 -->
			<!-- start row-1 -->
			<div class="row-1">
				<?php echo $this->Form->input('nome_secundario', array('label' => 'Nome do Noivo ou Noiva')); ?>
			</div>
			<!-- end row-1 -->
			<!-- start row-2 -->
			<div class="row-2">
				<?php
					$data = null;
					$hora = null;
					if(!empty($this->data['Lista']['data']) && empty($this->data['Lista']['hora'])){
						$data_tmp = $this->data['Lista']['data'];
						$data = $this->Calendario->dataFormatada("d/m/Y", $data_tmp);
						$hora = $this->Calendario->dataFormatada("H:m", $data_tmp);
					}
				?>
				<div class="box-1">
					<?php echo $this->Form->input('data', array('label' => 'Data do Evento *', 'type' => 'text', 'class' => 'date', 'value' => $data)); ?>
				</div>
				<div class="box-1">
					<?php echo $this->Form->input('hora', array('label' => 'Horário do Evento', 'type' => 'text', 'class' => 'hour', 'value' => $hora)); ?>
				</div>
			</div>
			<!-- end row-2 -->
			
			<?php if($this->data['Lista']['id'] == ""): ?>
			<!-- start row-1 -->
			<div class="row-1">
				<?php
	            	echo $this->Form->input('ListaFoto.filename', array('type' => 'file', 'label' => 'Fotos dos Organizadores', 'class' => 'file-original'));
	            	echo $this->Form->input('ListaFoto.dir', array('type' => 'hidden'));
	            	echo $this->Form->input('ListaFoto.mimetype', array('type' => 'hidden'));
	            	echo $this->Form->input('ListaFoto.filesize', array('type' => 'hidden'));
					echo $this->Form->input('ListaFoto.capa', array('type' => 'hidden', 'value' => 1));
				?>
				<ul class="div-input-falso">
					<li><input name="file-falso" type="text" class="file-falso" value="" /></li>
					
				</ul>
			</div>
			<!-- end row-1 -->
			<?php endif; ?>
			<!-- start row-1 -->
			<div class="row-1" style="position: relative;">
				<?php echo $this->Form->input('local', array('label' => 'Local')); ?>
				<script src="https://maps.googleapis.com/maps/api/js?sensor=false" rel="forceload" rev="shown"></script>
				<div id="view" style="height: 200px; position: absolute; float: left; width: 240px; right: -10px;"></div>
			</div>
			<!-- end row-1 -->
			<div class="clear"></div>
			<!-- start row-1 -->
			<div class="row-1">
				<?php echo $this->Form->input('site', array('label' => 'Site do evento')); ?>
			</div>
			<!-- end row-1 -->
			<div class="clear"></div>
			<!-- start row-1 -->
			<div class="row-1">
				<?php echo $this->Form->input('descricao', array('label' => 'Descrição')); ?>
			</div>
			<!-- end row-1 -->
			<div class="clear"></div>
			<!-- start row-3 -->
			<div class="row-3">
				<p>Deseja receber email de confirmação por compra?</p>
				<?php echo $this->Form->input('email_notificacao', array('default' => true, 'legend' => false, 'type' => 'radio', 'class' => 'radio', 'options' => array(true => 'Sim', false => 'Não'))); ?>
			</div>
			<!-- end row-3 -->
			<div class="clear"></div>
			<!-- start row-3 -->
			<div class="row-3">
				<p>Endereço de entrega</p>
				<?php echo $this->Form->input('endereco_proprio', array('default' => false, 
																		'legend' => false, 
																		'type' => 'radio', 
																		'class' => 'radio', 
																		'options' => array(1 => 'Meu Endereço', 0 => 'Endereço do convidado', 2 => 'Retirar na loja')
																		)
											); ?>
				<?php echo $this->Form->hidden('usuario_endereco_id'); ?>
				<?php echo $this->Form->hidden('loja_id'); ?>
			</div>
			<!-- end row-3 -->
			<!-- start row3 -->
			<div class="row-3 box-enderecos-lista">
				<h3>Meus Endereços</h3>
				<?php echo $this->Html->image('/img/site/zoomloader.gif', array('id' => 'loading', 'class' => 'loading', 'style' => 'visibility:hidden;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
				<div class="box-enderecos-lista-content"></div>
				<?php echo $form->error('usuario_endereco_id', 'Selecione um endereço de entrega'); ?>
			</div>
			<!-- end row3 -->
			<!-- start row3 -->
			<div class="row-3 box-lojas">
				<h3>Lojas</h3>
				<?php echo $this->Html->image('/img/site/zoomloader.gif', array('id' => 'loading', 'class' => 'loading', 'style' => 'visibility:hidden;', 'alt' => 'Carregando', 'title' => 'Carregando')); ?>
				<div class="box-lojas-content"></div>
				<?php echo $form->error('loja_id', 'Selecione uma das lojas'); ?>
			</div>
			<!-- end row3 -->
			<input type="submit" name="submit" value="PUBLICAR" class="button1" />
			<div class="clear"></div>
			<p style="float: right; margin-top: -10px;">* Campos de preenchimento obrigatório.</p>
		<?php echo $this->Form->end() ?>
	</div>
	<!-- end create-list-form -->
</div>
<?php $content = ob_get_clean(); ?>
<?php
if(!empty($this->data['Lista']['id'])){
	$elemento = "site/camada_edicao";
}else{
	$elemento = "site/camada_default";
}
?>
<?php echo $this->Element($elemento, array("content_to_element" => $content)); ?>

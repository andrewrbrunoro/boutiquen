<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/detalhe.js', false) ?>
<div class="present">
	<h2>Lista de Presentes</h2>
	<!-- start imgb -->
	<div class="imgb">
<?php
	$img = null;
	if (!empty($lista['fotos'])) {
		if (
			!empty($lista['fotos'][0]['ListaFoto']['dir']) &&
			!empty($lista['fotos'][0]['ListaFoto']['filename'])
		) {
			$img = $lista['fotos'][0]['ListaFoto']['dir'] .'/'. $lista['fotos'][0]['ListaFoto']['filename'];
		}
	}
?>
<?php echo $this->Image->resize($img,220,153) ?>
	</div>
	<!-- start imgb -->
	<!-- start txtb -->
	<div class="txtb">
		<h3>
			<?php echo $lista['lista']['ListaTipo']['nome'] ?>: <?php echo $lista['lista']['Lista']['nome'] ?>
<?php if (!empty($lista['lista']['Lista']['nome_secundario'])) : ?>
			e <?php echo $lista['lista']['Lista']['nome_secundario'] ?>
<?php endif ?>
		</h3>
		<p>Data do Evento: <?php echo $this->Calendario->dataFormatada('d/m/Y',$lista['lista']['Lista']['data']) ?><span>Hora do evento: <?php echo $this->Calendario->dataFormatada('H:m',$lista['lista']['Lista']['data']) ?></span> Local do evento: <a href="#map_evento_box" class="nyroModalMap"><?php echo $lista['lista']['Lista']['local'] ?></a></p>
		<?php if($lista['lista']['Lista']['site'] != ""): ?>
			<?php 
				if(strpos("http://", $lista['lista']['Lista']['site'])){
					$url = $lista['lista']['Lista']['site'];
				}else{
					$url = "http://".$lista['lista']['Lista']['site'];
				}
			?>
			<p>Site do Evento: <a href="<?php echo $url; ?>" target="_blank"><?php echo $lista['lista']['Lista']['site']; ?></a></p>
		<?php endif; ?>
		<!-- begin mapa evento -->
	    <script src="https://maps.googleapis.com/maps/api/js?sensor=false" rel="forceload" rev="shown"></script>
		<div id="map_evento_box" style="display: none; float: left;">
			
		</div>
	    <!-- end mapa evento -->
	</div>
	<!-- end txtb -->
	<!-- start present-navigation -->
	<div class="present-navigation">
		<ul>
			<li class="active"><a href="#" data-id="produtos" title="LISTA">lista</a></li>
			<li><a href="#" data-id="sobre"  title="SOBRE">sobre</a></li>
			<?php if(isset($lista['fotos']) && is_array($lista['fotos']) && count($lista['fotos']) > 0): ?>
				<li><a href="#" data-id="fotos" title="FOTOS">fotos</a></li>
			<?php endif; ?>
			<?php if(isset($lista['videos']) && is_array($lista['videos']) && count($lista['videos']) > 0): ?>
				<li><a href="#" data-id="videos" title="VIDEOS">vídeos</a></li>
			<?php endif; ?>
		</ul>
	</div>
	<!-- end present-navigation -->
	<div class="lista_box box_active" id="produtos">
<?php echo $this->element('site/produtos_box') ?>
	</div>
	<div class="lista_box" id="sobre">
<?php echo $this->element('site/sobre') ?>
	</div>
	<div class="lista_box" id="fotos">
<?php echo $this->element('site/fotos') ?>
	</div>
	<div class="lista_box" id="videos">
<?php echo $this->element('site/videos') ?>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>

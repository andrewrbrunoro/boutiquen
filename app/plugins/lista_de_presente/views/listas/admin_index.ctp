<div class="index ">
    <h2><?php __('Lista de Presentes'); ?></h2>
	<div class="btAddProduto">
    	<?php echo $this->Html->link(__('[+] Adicionar Lista de Presente', true), array('action' => 'add')); ?>
    </div>    
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('lista_tipo_id'); ?></th>
            <th><?php echo $this->Paginator->sort('nome'); ?></th>           
			<th><?php echo $this->Paginator->sort('usuario_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Criação', 'created'); ?></th>
            <th><?php echo $this->Paginator->sort('Data Modificação', 'modified'); ?></th>

            <th class="actions"><?php __('Ações'); ?></th>
        </tr>
        <?php
        $i = 0;
        foreach ($listas as $lista):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td align="center"><?php echo $lista['Lista']['id']; ?>&nbsp;</td>
				<td><?php echo $lista['ListaTipo']['nome']; ?>&nbsp;</td>
                <td><?php echo $lista['Lista']['nome']; ?>&nbsp;</td>                
				<td><a href="<?php echo $this->Html->Url(array('plugin' => false, 'admin' => true, 'controller' => 'usuarios', 'action' => 'edit', $lista['Usuario']['id'])); ?>"><?php echo $lista['Usuario']['nome']; ?></a>&nbsp;</td>
				<td>
					<?php if($lista['Lista']['status'] == -1): ?>
						Removido
					<?php elseif($lista['Lista']['status'] == 0): ?>
						Inativo
					<?php elseif($lista['Lista']['status'] == 1): ?>
						Ativo
					<?php endif; ?>
					&nbsp;
				</td>
				<td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $lista['Lista']['created']); ?>&nbsp;</td>
                <td align="center"><?php echo $this->Calendario->dataFormatada('d-m-Y H:i:s', $lista['Lista']['modified']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('Visualizar/Editar', true), array('action' => 'edit', $lista['Lista']['id'])); ?>
					<?php echo $this->Html->link(__('Deletar', true), array('action' => 'delete', $lista['Lista']['id']), null, sprintf(__('Deseja mesmo remover o registro # %s?', true), $lista['Lista']['id'])); ?>
				</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
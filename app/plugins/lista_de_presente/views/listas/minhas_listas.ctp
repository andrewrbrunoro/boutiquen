<?php ob_start(); ?>
<div class="column-right margin_none">
	<h4>Minhas Listas de Presentes</h4>
	<ul class="detail">
		<?php if(isset($listas) && count($listas) > 0): ?>
			<?php foreach($listas as $lista): ?>
	            <li>
	<?php echo $this->element('site/busca', array('lista' => $lista, 'url_botao' => array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'editar', $lista['Lista']['id']))); ?>
	            </li>
	        <?php endforeach; ?>
	    <?php else: ?>
	    	<li>
	    		Nenhuma lista encontrada
	    		<br />
	    		<a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'criar')); ?>" class="button3" style="margin-top: 10px;" title="Criar Lista">Criar Lista</a>
	    	</li>
	    <?php endif; ?>
	</ul>
</div>
<div class="clear"></div>
<?php $content = ob_get_clean(); ?>
<?php echo $this->Element('site/camada_default', array('content_to_element' => $content)); ?>
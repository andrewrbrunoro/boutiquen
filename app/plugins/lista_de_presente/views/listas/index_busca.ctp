<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/index.js', false); ?>
<!-- start find-list -->
<div class="banner">
	<!-- start column-left -->
	<div class="column-left">
		<h2>Lista de Presentes</h2>
<?php echo $this->element('site/duvidas_frequentes') ?>
	</div>
	<!-- end column-left -->
	<!-- start column-right -->
	<div class="column-right">
		<!-- start find-form -->
		<div class="find-form">
<?php echo $this->element('site/form_busca') ?>
			<!-- start row2 -->
			<div class="row2">
				<div class="col left">
					<h3>Crie a sua lista</h3>
					<a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'criar')); ?>" title="Criar agora">Criar agora</a>
				</div>
				<div class="col right">
					<h3>Acessar suas listas</h3>
					<a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'minhas_listas')); ?>" title="Visualizar">Visualizar</a>
				</div>
			</div>
			<!-- end row2 -->
		</div>
		<!-- end find-form -->
	</div>
	<!-- end column-right -->
</div>
<!-- end find-list-->
<div class="clear"></div>

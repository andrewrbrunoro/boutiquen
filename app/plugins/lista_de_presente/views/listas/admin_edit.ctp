<?php echo $javascript->link('common/jqueryCookie.js',false); ?>
<?php echo $javascript->link('common/jquery.meio_mask.js', false); ?>
<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/admin/lista/crud.js', false); ?>
<div class="index">
	<fieldset>
		<legend><?php printf(__('Editar/Visualizar %s', true), __('Lista de Presente', true)); ?></legend>
		 
	 	<div class="listaabas">
	        <div class="clicaaba active" rel="0">GERAL</div>
	        <div class="clicaaba" rel="1">FOTOS</div>
	        <div class="clicaaba" rel="2">VÍDEOS</div>
	        <div class="clicaaba" rel="3">PRODUTOS</div>
	    </div>
	    <br class="clear" />
	    <div class="abas" rel="0">
	    	
		    <?php echo $this->Form->create('Lista'); ?>
		    
	        <div class="left clear">
	        	<?php echo $this->Form->input('id'); ?>
	        	<?php echo $this->Form->input('status', array('type' => 'radio', 'options' => array(true => 'Ativo', false => 'Inativo'))); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('lista_tipo_id', array('class'=>'w312', 'options' => array('' => 'Selecione')+$ListaTipo)); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('nome', array('class'=>'w312')); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('nome_secundario', array('class'=>'w312')); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('descricao', array('class'=>'w312', 'label' => 'Descrição')); ?>
			</div>
			<?php
				$data = null; 
				$hora = null;
				if(!empty($this->data['Lista']['data']) && empty($this->data['Lista']['hora'])){
					$data_tmp = $this->data['Lista']['data'];
					$data = $this->Calendario->dataFormatada("d/m/Y", $data_tmp);
					$hora = $this->Calendario->dataFormatada("H:m", $data_tmp);
				}
			?>
			<div class="left clear">
				<?php echo $this->Form->input('data', array('label' => 'Data do Evento', 'type' => 'text', 'class' => 'date', 'value' => $data)); ?>							
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('hora', array('label' => 'Horário do Evento', 'type' => 'text', 'class' => 'hour', 'value' => $hora)); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('site', array('class'=>'w312')); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('email_notificacao', array('class'=>'w312', 'type' => 'radio', 'options' => array(true => 'Sim', false => 'Não'))); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->input('usuario', array('class'=>'w312', 'value' => $this->data['Usuario']['nome'], 'disabled' => 'disabled')); ?>
			</div>
			<?php 
			$UsuarioEndereco = array('' => 'Selecione');
			foreach($UsuarioEnderecos as $UsuarioEnderecoTmp){
				$UsuarioEndereco[$UsuarioEnderecoTmp['UsuarioEndereco']['id']] = $UsuarioEnderecoTmp['UsuarioEndereco']['nome'].": ".$UsuarioEnderecoTmp['UsuarioEndereco']['rua'].", ".$UsuarioEnderecoTmp['UsuarioEndereco']['numero']." - ".$UsuarioEnderecoTmp['UsuarioEndereco']['cidade']."/".$UsuarioEnderecoTmp['UsuarioEndereco']['uf']." - ".$UsuarioEnderecoTmp['UsuarioEndereco']['cep'];
			}  
			?>
			<div class="left clear">
				<?php echo $this->Form->input('usuario_endereco_id', array('class'=>'w312', 'options' => $UsuarioEndereco)); ?>
			</div>
			<?php 
			$Loja = array('' => 'Selecione');
			foreach($Lojas as $LojaTmp){
				$Loja[$LojaTmp['Loja']['id']] = $LojaTmp['Loja']['nome'].": ".$LojaTmp['Loja']['rua'].", ".$LojaTmp['Loja']['numero']." - ".$LojaTmp['Loja']['cidade']."/".$LojaTmp['Loja']['uf']." - ".$LojaTmp['Loja']['cep'];
			}  
			?>
			<div class="left clear">
				<?php echo $this->Form->input('loja_id', array('class'=>'w312', 'options' => $Loja)); ?>
			</div>
			<div class="left clear">
				<?php echo $this->Form->end(__('Salvar', true)); ?>
			</div>
	
		</div>
		<div class="abas" rel="1">
			<table class="lista-table-fotos">
		    	<tr>
		    		<th>Fotos Cadastradas:</th>
		    	</tr>
		    	<tr>
		    		<td>
	    			<?php if(count($lista['fotos']) > 0): ?>
		    			<ul>
		    				<?php 
		    					$cont = 1;
		    					foreach($lista['fotos'] as $lf):  ?>
		    					<li>
		    						<?php $img = str_replace('\\', '/',$lf['ListaFoto']['dir']) .'/'. $lf['ListaFoto']['filename']; ?>
									<a href="<?php echo $this->Html->Url('/', true).$img; ?>" class="nyroModalFoto">	
										<?php echo $this->Image->resize($img,150,150); ?>
									</a>
									<div class="clear"></div>
									
									<span class="box-capa" style="float: left;width: 58px; margin-top: 5px;">
										<?php 
											$checked = "";
								            if ($lf['ListaFoto']['capa'] == 1) {
								                $checked = "checked='checked'";
								            }
										?>
										<label>
											Capa 
											<input type="checkbox" class="foto_capa" 
												value="<?php echo $lf['ListaFoto']['capa']; ?>" 
												data-item-id="<?php echo $lf['ListaFoto']['id']; ?>" 
												data-lista-id="<?php echo $lf['ListaFoto']['lista_id']; ?>" 
												name="data[<?php echo $lf['ListaFoto']['id']; ?>][ListaFoto][capa]" <?php echo $checked; ?>  />
										</label>
									</span>
									<span class="box-remover-foto" style="float: right; margin-top: 3px;">
										<a href="javascript:void(0);" class="rm-img" data-id="<?php echo $lf['ListaFoto']['id']; ?>">Remover foto</a>
									</div>
								</li>
								<?php if($cont%5 == 0): ?>
									</ul><div class="clear"></div><ul>
								<?php endif; ?>
		    				<?php 
		    					$cont++;
		    					endforeach; ?>
		    			</ul>
		    		<?php else: ?>
		    			<p>Nenhum registro encontrado.</p>
	    			<?php endIf; ?>
		    		</td>
		    	</tr>
		    </table>
		</div>
	
		<div class="clear"></div>
		
		<div class="abas" rel="2">
			<table class="lista-table-fotos">
		    	<tr>
		    		<th>Vídeo Cadastrados:</th>
		    	</tr>
		    	<tr>
		    		<td>
	    			<?php if(count($lista['videos']) > 0): ?>
		    			<ul>
		    				<?php 
		    					$cont = 1;
		    					foreach($lista['videos'] as $lf):  ?>
		    					<li>
		    						<?php $img = str_replace('\\', '/',$lf['ListaVideo']['thumb_dir']) .'/'. $lf['ListaVideo']['thumb_filename']; ?>
									<a href="<?php echo $lf['ListaVideo']['url']; ?>" class="nyroModalVideo">	
										<?php echo $this->Image->resize($img,150,115); ?>
									</a>
									<div class="clear"></div>
									<a href="javascript:void(0);" class="rm-video" data-lista-id="<?php echo $lf['ListaVideo']['lista_id']; ?>" data-video-id="<?php echo $lf['ListaVideo']['id']; ?>">Remover vídeo</a>
								</li>
								<?php if($cont%5 == 0): ?>
									</ul><div class="clear"></div><ul>
								<?php endif; ?>
		    				<?php 
		    					$cont++;
		    					endforeach; ?>
		    			</ul>
		    		<?php else: ?>
		    			<p>Nenhum registro encontrado.</p>
	    			<?php endIf; ?>
		    		</td>
		    	</tr>
		    </table>
		</div>
	
		<div class="clear"></div>
		
		<div class="abas" rel="3">
			<table class="lista-table-fotos">
		    	<tr>
		    		<th colspan="6">Vídeo Cadastrados:</th>
		    	</tr>
		    	<tr>
		    		<th colspan="2" align="center">Produto</th>
		    		<th align="center">Grade</th>
		    		<th align="center">Quantidade</th>
		    		<th align="center">Comprados</th>
		    		<th align="center">Ações</th>
		    	</tr>
		    	<?php if(count($lista['produtos']) > 0): ?>
			    	<?php foreach($lista['produtos'] as $prd):  ?>
			    	<?php if($prd['ListaProduto']['status'] == 0) continue; ?>
		    		<tr>
		    			<td>
							<?php 
								if(isset($prd['GradeImagem'])){
									$img = str_replace('\\', '/',$prd['GradeImagem'][0]['dir']) .'/'. $prd['GradeImagem'][0]['filename'];	
								}else{
									$img = "img/site/sem_imagem.jpg";		
								}
								echo $this->Image->resize($img,150,115);
							?>
						</td>
		    			<td>
		    				<a href="<?php echo $this->Html->Url(array('plugin' => false, 'admin' => true, 'controller' => 'produtos', 'action' => 'edit', $prd['Produto']['id'])); ?>">
		    					<?php echo $prd['Produto']['nome']; ?>
		    				</a>
		    				<?php if(isset($prd['Variacoes']) && is_array($prd['Variacoes']) && count($prd['Variacoes']) > 0 && $prd['Variacoes'][0]['VariacaoTipo']['nome'] != "Padrão"): ?>
		    				(
		    					<?php foreach($prd['Variacoes'] as $k => $vr): ?>
		    						<?php 
		    							echo $vr['VariacaoTipo']['nome'].': '.$vr['Variacao']['valor'];
			    						if(isset($prd['Variacoes'][$k+1])){
			    							echo ' | ';
			    						}
									 ?>
		    					<?php endforeach; ?>
		    				)
		    				<?php endif; ?>
		    			</td>
		    			<td align="center"><?php echo $prd['Grade']['id']; ?></td>
		    			<td align="center"><?php echo $prd['ListaProduto']['quantidade']; ?></td>
		    			<td align="center"><?php echo $prd['ListaProduto']['quantidade_vendidos']; ?></td>
		    			<td>
		    				<a href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'admin' => true, 'controller' => 'listas', 'action' => 'remover_item', $prd['ListaProduto']['lista_id'], $prd['ListaProduto']['id'])); ?>" title="REMOVER" class="button rm-item" style="margin-top: 6px">REMOVER</a>
		    				<em style="float: left; width: 10px;">&nbsp;</em>
		    				<a href="<?php echo $this->Html->Url(array(
		    													'plugin' => 'lista_de_presente', 
		    													'admin' => true, 
		    													'controller' => 'listas', 
		    													'action' => 'adicionar_compra', 
		    													$prd['ListaProduto']['lista_id'], 
		    													$prd['ListaProduto']['id'])); ?>" 
		    											title="VENDAS" class="button nyroModalCompra" style="margin-top: 6px" target="_blank">VENDAS</a>
		    			</td>
		    		</tr>
		    		<?php endforeach; ?>
		    	<?php else: ?>
		    	<tr>
		    		<td colspan="6" align="center">Nenhum produto encontrado.</td>
		    	</tr>
		    	<?php endIf; ?>		
		    </table>
		</div>
    </fieldset>
</div>
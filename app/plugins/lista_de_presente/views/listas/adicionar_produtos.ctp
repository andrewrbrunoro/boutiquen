<?php ob_start(); ?>
<h2>Adicionar produtos</h2>
    <!-- start search-->
    <div class="header-form">
        <div class="search margin-bottom">
            <?php echo $this->Form->create('Busca', array('class' => 'header_form', 'id' => 'header_form', 'url' => '/busca', 'type' => 'get')); ?>
                <?php echo $this->Form->input('busca', array('rel' => 'Encontre no site', 'label' => false, 'div' => false, 'class' => 'input green useDefault BuscaBusca', 'value' => (isset($this->data['Busca']['busca']) && !empty($this->data['Busca']['busca'])) ? $this->data['Busca']['busca'] : 'Encontre no site')); ?>
                <input type="submit" value=" " name="BUSCAR" class="button" />
            <?php echo $this->Form->end(); ?>            
        </div>
    </div>
    <!-- end search-->
    <!-- start listbox -->
    <div class="listbox">
    <!-- start list -->
<?php 
if(isset($menu) && count($menu) > 0):
	foreach ($menu as $valor): 
?>
	<div class="list">
        <a href="<?php echo $this->Html->Url('/categorias/') . $valor['Categoria']['seo_url']; ?>" title="<?php echo $this->String->title_case($valor['Categoria']['nome']); ?>">
           <h4><?php echo $this->String->title_case($valor['Categoria']['nome']); ?></h4>
        </a>
            <ul>
            	<?php foreach ($valor['SubCategory'] as $sub): ?>
            		<li>
                        <a href="<?php echo $this->Html->Url('/categorias/') . $sub['seo_url']; ?>" title="<?php echo $this->String->title_case($sub['nome']); ?>">
                            <span><?php echo $this->String->title_case($sub['nome']); ?></span>
                        </a>
                    </li>
            	<?php endforeach; ?>
            </ul>
    </div>
<?php 
	endforeach; 
endif;
?>
	</div>
	<!-- end listbox -->
<?php $content = ob_get_clean(); ?>
<?php echo $this->Element('/site/camada_edicao', array('content_to_element' => $content)); ?>
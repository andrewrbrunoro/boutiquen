<style>
	table {
		border-collapse: collapse;
	}
	td,th {
		padding: 2px 5px;
		border: 1px solid #000;
	}

	thead tr {
		background-color: #999;
		font-size: 12px;
	}

	tbody tr:nth-child(even) {
		background-color: #ccc;
	}
	tbody {
		font-size: 11px;
	}

	.w100 {
		width: 80px;
	}
	.w200 {
		width: 250px;
	}

	.center {
		text-align: center;
	}
</style>
<script type="text/javascript">
	window.print();
</script>
<h1>
	<?php echo $lista['lista']['ListaTipo']['nome'] ?>: <?php echo $lista['lista']['Lista']['nome'] ?>
<?php if (!empty($lista['lista']['Lista']['nome_secundario'])) : ?>
	e <?php echo $lista['lista']['Lista']['nome_secundario'] ?>
<?php endif ?>
</h1>
<?php
	$img = null;
	if (!empty($lista['fotos'])) {
		if (
			!empty($lista['fotos'][0]['ListaFoto']['dir']) &&
			!empty($lista['fotos'][0]['ListaFoto']['filename'])
		) {
			$img = $lista['fotos'][0]['ListaFoto']['dir'] .'/'. $lista['fotos'][0]['ListaFoto']['filename'];
		}
	}
?>
<?php echo $this->Image->resize($img,220,153) ?>
<p>Data do Evento: <?php echo $this->Calendario->dataFormatada('d/m/Y',$lista['lista']['Lista']['data']) ?></p>
<p>Hora do evento: <?php echo $this->Calendario->dataFormatada('H:m',$lista['lista']['Lista']['data']) ?></p>
<p>Local do evento: <?php echo $lista['lista']['Lista']['local'] ?></p>
<table>
	<thead>
		<tr>
			<th colspan="2">Produto</th>
			<th>Valor</th>
			<th class="w100">Quantidade</th>
			<th class="w100">Quantidade Comprada</th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($produtos as $produto) : ?>
		<tr>
<?php
$img = null;
if (!empty($produto['GradeImagem'])) {
	if (!empty($produto['GradeImagem'][0]['dir']) && !empty($produto['GradeImagem'][0]['filename'])) {
		$img = $produto['GradeImagem'][0]['dir'].'/'.$produto['GradeImagem'][0]['filename'];
	}
}
?>
			<td style="text-align: center;">
<?php echo $this->Image->resize($img,100,100,true,array('class' => 'img')) ?>
			</td>
			<td class="w200"><?php echo $produto['Produto']['nome'] ?></td>
			<td>
<?php $preco = $produto['Grade']['preco'] ?>
<?php if ($produto['Grade']['preco_promocao'] > 0) : ?>
					<p class="txt">De: R$ <?php echo $this->String->bcoToMoeda($produto['Grade']['preco']) ?></p>
<?php $preco = $produto['Grade']['preco_promocao'] ?>
<?php endif ?>
					<p class="txt2">Por: <span>R$ <?php echo $this->String->bcoToMoeda($preco) ?></span> ou</p>
<?php $parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento); ?>
<?php if ($parcela_valida['parcela'] > 1): ?>
					<p class="txt2"><?php echo $parcela_valida['parcela']; ?>x de <span>R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor']) ?></span>  <?php echo ($parcela_valida['juros'] == true) ? 'com' : 'sem'; ?> juros</p>
<?php endIf; ?>
			</td>
			<td class="center"><?php echo $produto['ListaProduto']['quantidade'] ?></td>
			<td class="center"><?php echo $produto['ListaProduto']['quantidade_vendidos'] ?></td>
		</tr>
<?php endforeach ?>
	</tbody>
</table>

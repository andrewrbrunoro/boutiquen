<?php ob_start(); ?>
<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista/editar.js', false); ?>
<!-- start product-box -->
<div class="product-box meus-produtos">
	<!-- start product list -->
	<ul class="product-list">
<?php
	if(is_array($lista['produtos']) && count($lista['produtos']) > 0):
		foreach ($lista['produtos'] as $produto)  : ?>
		<li>
			<!-- start left -->
			<div class="left">
<?php
$img = null;

if (!empty($produto['GradeImagem'])) {
	if (!empty($produto['GradeImagem'][0]['dir']) && !empty($produto['GradeImagem'][0]['filename'])) {
		$img = $produto['GradeImagem'][0]['dir'].'/'.$produto['GradeImagem'][0]['filename'];
	}
}
?>
<div class="lista-editar box-img">
	<a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['ListaProduto']['grade_id']); ?>.html" title="<?php echo $produto['Produto']['nome'] ?>">
		<?php echo $this->Image->resize($img,120,140,true,array('class' => 'img')) ?>
	</a>
</div>
				<!-- start interest -->
				<div class="interest">
				<p class="heading">
					<a href="<?php echo $this->Html->Url("/" . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-' . 'prod-' . $produto['ListaProduto']['grade_id']); ?>.html" title="<?php echo $produto['Produto']['nome'] ?>">
						<?php echo $produto['Produto']['nome'] ?>
					</a>
				</p>
<?php $preco = $produto['Grade']['preco'] ?>
<?php if ($produto['Grade']['preco_promocao'] > 0) : ?>
					<p class="txt">De: R$ <?php echo $this->String->bcoToMoeda($produto['Grade']['preco']) ?></p>
<?php $preco = $produto['Grade']['preco_promocao'] ?>
<?php endif ?>
<?php $parcela_valida = $this->Parcelamento->parcelaValida($preco, $parcelamento); ?>
					<p class="txt2">Por: <span>R$ <?php echo $this->String->bcoToMoeda($preco) ?></span> ou</p>
<?php if ($parcela_valida['parcela'] > 1): ?>
					<p class="txt2"><?php echo $parcela_valida['parcela']; ?>x de <span>R$ <?php echo $this->String->bcoToMoeda($parcela_valida['valor']) ?></span>  <?php echo ($parcela_valida['juros'] == true) ? 'com' : 'sem'; ?> juros</p>
<?php endIf; ?>
				</div>
				<!-- end interest -->
			</div>
			<!-- end left -->
			<!-- start right -->
<?php echo $this->Form->create(null,array('url' => array('plugin' => 'carrinho','controller' => 'carrinho','action' => 'add'),'class' => 'form-editar','data-lista' => $lista['lista']['Lista']['id'])) ?>
			<input type="hidden" name="data[produto_id]" value="<?php echo $produto['Produto']['id'] ?>" />
			<input type="hidden" name="data[grade_id]" value="<?php echo $produto['Grade']['id'] ?>" />
			<input type="hidden" name="data[id]" value="<?php echo $produto['ListaProduto']['id'] ?>" />
			<div class="right">
				<p class="heading">Quantidade</p>
				<div class="clear"></div>
				<?php if($produto['Grade']['quantidade_disponivel'] > 0): ?>
					<input type="text" class="input" name="data[quantidade]" value="<?php echo $produto['ListaProduto']['quantidade'] ?>" />
					<!-- button start -->
					<div class="button">
						<input type="submit"  title="EDITAR" value="EDITAR"/>
					</div>
				<?php else: ?>
					<p class="quantity quantidade-vendidos"><?php echo $produto['ListaProduto']['quantidade'] ?></p>
					<div class="button" style="line-height: 46px;color: #e42c3e; cursor: default;">
					<p>Sem estoque</p>
					</div>
				<?php endIf; ?>
				<div class="button">
					<a class="remover" title="REMOVER" href="<?php echo $this->Html->Url(array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'remover_item',$lista['lista']['Lista']['id'], $produto['ListaProduto']['id'])) ?>">REMOVER</a>
				</div>
			</div>
			<!-- button end -->
			<!-- end right -->
			<!-- start right -->
			<div class="right quantity-padding">
			  <p class="heading">Comprados</p>
			  <div class="clear"></div>
				<p class="quantity quantidade-vendidos"><?php echo $produto['ListaProduto']['quantidade_vendidos'] ?></p>
			</div>
			<!-- end right -->
<?php echo $this->Form->end() ?>
			<div class="clear"></div>
		</li>
<?php
		endforeach;
	else:
?>
	<li>Nenhum produto encontrado.</li>
<?php
	endif;
?>
	 </ul>
	<!-- end product list -->
</div>
<!-- end product box -->

<?php $content = ob_get_clean(); ?>
<?php echo $this->Element('/site/camada_edicao', array('content_to_element' => $content)); ?>

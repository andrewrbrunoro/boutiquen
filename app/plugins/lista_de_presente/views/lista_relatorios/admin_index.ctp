<div class="index ">
    <h2><?php __('Lista de Presentes: Relatório Geral'); ?></h2>
    <?php echo $form->create('ListaRelatorio', array('class' => 'formBusca', 'action' => '/index')); ?>
	    <fieldset>
	        <div class="left">
	            <?php echo $form->input('Filter.filtro', array('div' => false, 'label' => 'Pruduto / SKU', 'class' => 'produtosFiltro')); ?>
	        </div>
	        <div class="submit">
	            <input name="submit" type="submit" class="button1" value="Busca" />
	        </div>
	        <div class="submit">
	            <input name="submit" type="submit" class="button1" value="Exportar" />
	        </div>
	        <div class="submit">
	            <a href="<?php e($this->Html->Url('index/filter:clear')) ?>" title="Limpar Filtro" class="button" >Limpar Filtro</a>
	        </div>
	    </fieldset>	
	<?php echo $form->end(); ?>
	<table cellpadding="0" cellspacing="0">
        <tr>
            <th>Nome do Produto</th>
            <th>Id Grade</th>
			<th>Variação</th>           
			<th>Total: Listas</th>
			<th>Total: Quantidades</th>
			<th>Total: Adquiridos</th>
        </tr>
        <?php
        $i = 0;
        foreach ($grades as $grade):
            $class = null;
            if ($i++ % 2 == 0) {
                $class = ' class="altrow"';
            }
            ?>
            <tr<?php echo $class; ?>>
                <td><a href="<?php echo $this->Html->Url(array('plugin' => false, 'admin' => true, 'controller' => 'produtos', 'action' => 'edit', $grade['Produto']['id'])); ?>"><?php echo $grade['Produto']['nome']; ?></a>&nbsp;</td>
                <td align="center"><?php echo $grade['Grade']['id']; ?>&nbsp;</td>
				<td><?php echo $grade['Variacao']['valor']; ?>&nbsp;</td>                
				<td align="center"><?php echo $grade[0]['itens_listas']; ?> &nbsp;</td>
				<td align="center"><?php echo $grade[0]['itens_totais']; ?> &nbsp;</td>
				<td align="center"><?php echo $grade[0]['itens_comprados']; ?> &nbsp;</td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Página %page% de %pages%, mostrando %current% registros de %count%, começando no registro %start% e terminando no %end%', true)
        ));
        ?>	</p>

    <div class="paging">
        <?php echo $this->Paginator->prev('<< ' . __('anterior', true), array(), null, array('class' => 'disabled')); ?>
	 | 	<?php echo $this->Paginator->numbers(); ?>
        |
        <?php echo $this->Paginator->next(__('próxima', true) . ' >>', array(), null, array('class' => 'disabled')); ?>
    </div>
    
</div>
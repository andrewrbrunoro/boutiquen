<?php ob_start(); ?>
<?php echo $this->Html->css('common/nyro_modal/nyroModal.css'); ?>
<?php echo $javascript->link('common/nyro_modal/jquery.nyroModal.custom.min', false); ?>
<?php echo $javascript->link('/lista_de_presente/js/site/lista_videos/adicionar.js', false); ?>
<!-- start column-right -->
<div class="column-right">
	<!-- start create-list-form -->
	<div class="create-list-form create-form-video">
		<h2>Adicionar novo vídeo</h2>
		<?php 
		echo $this->Form->create('ListaVideo', array('url' => array('plugin' => 'lista_de_presente', 'controller' => 'lista_videos', 'action' => 'adicionar', $this->params['pass'][0]), 'type' => 'file')); ?>
			<!-- start row-1 -->
			<div class="row-1">
				<?php echo $this->Form->input('url', array('label' => 'URL do vídeo')); ?>
			</div>
			<!-- end row-1 -->
			<input type="submit" name="submit" value="SALVAR" class="button1 button11" />
			<div class="clear"></div>
		<?php echo $this->Form->end(); ?>
	</div>
	<!-- end create-list-form -->
	<!-- start list-videos-->
	<div class="list-videos">
		<h2>Vídeos cadastrados</h2>
		<ul>
			<?php if(isset($lista['videos']) && is_array($lista['videos']) && count($lista['videos']) > 0): ?>
				<?php foreach($lista['videos'] as $video): ?>
				<li>
					<a href="<?php echo $video['ListaVideo']['url']; ?>" class="nyroModal">
						<?php $img = ( isset($video['ListaVideo']['thumb_filename']) && file_exists($video['ListaVideo']['thumb_dir'].'/'.$video['ListaVideo']['thumb_filename']) ) ? $video['ListaVideo']['thumb_dir'].'/'.$video['ListaVideo']['thumb_filename'] : "uploads/produto_imagem/filename/sem_imagem.jpg"; ?>
						<?php echo $image->resize($img, 135, 102); ?>
					</a>
					<a href="javascript:void(0);" class="btn-remover-video" data-lista-id="<?php echo $video['ListaVideo']['lista_id']; ?>" data-video-id="<?php echo $video['ListaVideo']['id']; ?>">Remover vídeo</a>
				</li>
				<?php endforeach; ?>
			<?php else: ?>
				<li>Nenhum vídeo encontrado.</li>
			<?php endif; ?>
		</ul>
	</div>
	<!-- end list-videos-->
</div>
<!-- end column-right -->

<?php $content = ob_get_clean(); ?>
<?php echo $this->Element('/site/camada_edicao', array('content_to_element' => $content)); ?>
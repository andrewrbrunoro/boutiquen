<?php
/**
 * Controladora de Vídeos das Listas
 *
 * @uses AppController
 * @category app
 * @package plugins
 * @subpackage lista_de_presente/controllers
 */
class ListaFotosController extends ListaDePresenteAppController {

	/**
	 * Helpers
	 *
	 * @var string
	 * @access public
	 */
	public $helpers = array('Image','String');

	public function formatbytes($file, $type = "MB") {
        switch ($type) {
            case "KB":
                $filesize = filesize($file) * .0009765625; // bytes to KB
                break;
            case "MB":
                $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
                break;
            case "GB":
                $filesize = ((filesize($file) * .0009765625) * .0009765625) * .0009765625; // bytes to GB
                break;
        }
        if ($filesize <= 0) {
            return $filesize = 'unknown file size';
        } else {
            return round($filesize, 2) . ' ' . $type;
        }
    }

    public function FlashImages($imgs) {

        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        App::import("helper", "Form");
        $this->Form = new FormHelper();
        App::import("helper", "Image");
        $this->Image = new ImageHelper();
		$model = "ListaFoto";
        $string = '';
        foreach ($imgs as $img):

            if (isset($img['tmp_file'])) {
                $imagem = '../../../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 100, 70, array(), null, true);
            } else {
                $imagem = '../../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 100, 70, array(), null, true);
            }

            if (isset($img['tmp_file'])) {
                $id = $img['tmp_file'];
            } else {
                $id = $img['id'];
            }
            if (isset($img['tmp_file'])) {
                $name = $img['tmp_file'];
            } else {
                $name = $img['filename'];
            }

            $string .= '
			<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">
				<div class="cancel">
					' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
				</div>';
		 	$checked = "";
            if ($img['capa'] == 1) {
                $checked = "checked='checked'";
            }
            $string .= '<span class="box-capa"><label>Capa <input type="checkbox" class="foto_capa" value="' . $img['capa'] . '" data-item-id="' . $id . '" data-lista-id="'.$img['lista_id'].'" name="data['.$model.'][capa]" '. $checked . ' /></label></span>';
			$string .= '<span class="fileName">
			<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
			<span class="percentage"> - 100%</span>
			</div>
			</div>';
        endForeach;

        return $string;
    }

    public function img_edit($lista_id) {
    	
		App::import("helper", "ListaDePresente.Uploadify");
        $this->Uploadify = new UploadifyHelper();
        $this->loadModel('ListaDePresente.Lista');
        $this->loadModel('ListaDePresente.ListaFoto');
		$path = "uploads/tmp/";
        if (!is_dir($path)) {
            mkdir($path);
        }

        $valid_formats = array("jpg", "png", "gif");
        if (!empty($_FILES)) {
            $name = $_FILES['Filedata']['name'];
            if (strlen($name)) {
                if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
                    $ext = $flag[2];
                    if (in_array(low($ext), $valid_formats)) {
                        $size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
                        if ($size < 100) {
                            $actual_image_name = md5(time() . $name) . "." . $ext;
                            $tmp = $_FILES['Filedata']['tmp_name'];

                            if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
                                $_FILES['Filedata']['tmp_file'] = $actual_image_name;
                                $_FILES['Filedata']['ordem'] = '';

                                $this->ListaFoto->create();
                                $this->data['ListaFoto']['lista_id'] = $lista_id;
                                $this->data['ListaFoto']['id'] = '';
                                $this->data['ListaFoto']['dir'] = '';
                                $this->data['ListaFoto']['mimetype'] = '';
                                $this->data['ListaFoto']['filesize'] = '';
                                //$this->data['ListaFoto']['ordem'] = 2;
                                $this->data['ListaFoto']['status'] = 1;
                                $this->data['ListaFoto']['filename']['type'] = 'image/jpeg';
                                $this->data['ListaFoto']['filename']['name'] = $_FILES['Filedata']['tmp_file'];
                                $this->data['ListaFoto']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file'];
                                $this->data['ListaFoto']['filename']['error'] = $_FILES['Filedata']['error'];
                                $this->data['ListaFoto']['filename']['size'] = $_FILES['Filedata']['size'];
                                $this->data['ListaFoto']['filename']['created'] = time();
                                $this->data['ListaFoto']['filename']['modified'] = time();
                                $this->ListaFoto->save($this->data);
                                unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file']);
                                $lista = $this->ListaFoto->find('all', array('recursive' => -1, 'conditions' => array('ListaFoto.lista_id' => $lista_id)));
                                $imgs = array();
                                foreach ($lista as $img) {
                                    $imgs[] = $img['ListaFoto'];
                                }
                                echo $this->Uploadify->build($imgs, 'ListaFoto', false);
                                exit;
                            } else {
                                echo "Falha ao mover arquivo";
                                exit;
                            }
                        } else {
                            echo "Arquivo muito grande";
                            exit;
                        }
                    } else {
                        echo "Formato de arquivo inválido.";
                        exit;
                    }
                } else {
                    echo "Formato de arquivo inválido.";
                    exit;
                }
            } else {
                echo "por favor selecione uma imagem.!";
                exit;
            }
        } else {
            echo "Falha ao enviar arquivo tente novamente.!";
            exit;
        }
    }

    public function rm_img($id) {
    	$this->_setUser();
		
		if (!$id) {
            die(json_encode(false));
        }
        if ($id != "") {
            if (!$this->ListaFoto->delete($id)) {
                die(json_encode(false));
            } else {
                die(json_encode(true));
            }
        } else {
            die(json_encode(false));
        }
    }
	
	/**
     * Ação que remove a foto em questão
     *
     * @param int $id Identificador da foto
     * @access public
     * @return null
     */
	public function admin_remover($id) {
    	$this->ListaFoto->setAdmin(true);
		
		if (!$id) {
            die(json_encode(false));
        }
        if ($id != "") {
            if (!$this->ListaFoto->delete($id)) {
                die(json_encode(false));
            } else {
                die(json_encode(true));
            }
        } else {
            die(json_encode(false));
        }
    }
	
	/**
     * Adição/Alteração de fotos das listas
     *
     * @param mixed $id
     * @access public
     * @return null
     */
	public function adicionar($id){
		$this->_setUser();

		// obtenção dos dados da lista
		App::import('Model','ListaDePresente.Lista');
		$modelLista = new Lista();

		$modelLista->setUsuarioId($this->ListaFoto->getUsuarioId());
		$lista = $modelLista->getListaDetalhada($id, array('videos','produtos'));

		// erro ao obter a lista?
		if (!$lista) {
			$this->cakeError('error404');
		}

		//seto as imagens da lista atual
		if (is_array($lista['fotos']) && count($lista['fotos']) > 0) {
            foreach ($lista['fotos'] as $img) {
                $imgs[] = $img['ListaFoto'];
            }
            $this->set('imgs', $this->FlashImages($imgs));
        }

		//set lista
		$this->set(compact('lista'));

		//breadcrumb
		$this->_addBreadcrumb('Vídeos');
	}
	
	/**
     * Acao que define se a foto eh capa ou não
     *
     * @param mixed $id
     * @access public
     * @return null
     */
     public function define_capa(){
     	$this->_setUser();
		
		//retorno
		$retorno = array('status' => $this->ListaFoto->define_capa($this->data['lista_id'],$this->data['item_id'], $this->data['capa']));
		$retorno['mensagens'] = $this->ListaFoto->getMensagens();

		//config
		Configure::write('debug',0);
		$this->autoRender = false;
		$this->layout = 'ajax';
        $this->header('Content-type: application/json');

		//return
		return json_encode($retorno);
     }
	 
	 /**
	 * Define o usuário que está acessando a área de lista
	 *
	 * @access protected
	 * @param bool $persiste_sessao Deve persistir o identificador do usuario na sessão?
	 * @return ListaVideosController Próprio Objeto para encadeamento
	 */
	protected function _setUser($persiste_sessao = false) {
		$usuario = $this->Auth->user('id');
		$this->ListaFoto->setUsuarioId($usuario);
		if ($persiste_sessao) {
			$this->Session->write('Lista.usuario_id',$usuario);
		}
		$this->set('usuario_id',$usuario);
		return $this;
	}

	// sobrescrita de gancho para antes da renderização da view
	public function beforeRender() {
		// envia a migalha para a visualização
		$this->set('breadcrumb',$this->_breadcrumb);
	}
}

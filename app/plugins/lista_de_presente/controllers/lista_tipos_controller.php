<?php
/**
 * Controladora de Tipos de Listas
 *
 * @uses AppController
 * @category app
 * @package plugins
 * @subpackage lista_de_presente/controllers
 */
class ListaTiposController extends AppController {
	
	/**
     * Helpers
     *
     * @var string
     * @access public
     */
    public $helpers = array('Calendario');
	
	/**
     * Listagem de tipos de listas no admin
     *
     * @access public
     * @return null
     */
	public function admin_index(){
		$this->set('tipos_lista', $this->paginate());
	}
	
	 /**
     * Edição/Alteração de tipos de listas
     *
     * @param mixed $id
     * @access public
     * @return null
     */
	public function admin_edit($id = null){
		if (!is_null($id)) {
            $data = $this->ListaTipo->read(null,$id);
        } else {
            $data = array();
        }

        if (!empty($this->data)) {
            if (!empty($data)) {
                $this->ListaTipo->id = $id;
            }
            if ($this->ListaTipo->save($this->data)) {
                $this->Session->setFlash('O registro foi salvo com sucesso','flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos abaixo','flash/error');
            }
        } else {
            $this->data = $data;
        }
	}
	
	/**
     * Remoção de tipos de listas
     *
     * @param mixed $id
     * @access public
     * @return null
     */
	public function admin_delete($id){
		if ($this->ListaTipo->delete($id)) {
            $this->Session->setFlash('O registro foi removido com sucesso','flash/success');
        } else {
            $this->Session->setFlash('Ocorreu um erro ao remover este registro','flash/error');
        }
        $this->redirect(array('action' => 'index'));
	}	
}
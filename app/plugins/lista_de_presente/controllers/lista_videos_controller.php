<?php
/**
 * Controladora de Vídeos das Listas
 *
 * @uses AppController
 * @category app
 * @package plugins
 * @subpackage lista_de_presente/controllers
 */
class ListaVideosController extends ListaDePresenteAppController {

	/**
	 * Helpers
	 *
	 * @var string
	 * @access public
	 */
	public $helpers = array('Image','String');

	 /**
     * Adição/Alteração de vídeos das listas
     *
     * @param mixed $id
     * @access public
     * @return null
     */
	public function adicionar($id){
		$this->_setUser();

		// obtenção dos dados da lista
		App::import('Model','ListaDePresente.Lista');
		$modelLista = new Lista();

		$modelLista->setUsuarioId($this->ListaVideo->getUsuarioId());
		$lista = $modelLista->getListaDetalhada($id, array('produtos'));

		// erro ao obter a lista?
		if (!$lista) {
			$this->cakeError('error404');
		}

		if (!empty($this->data)) {
			$this->data['ListaVideo']['lista_id'] = $id;
            if ($this->ListaVideo->save($this->data)) {
                $this->Session->setFlash('O registro foi salvo com sucesso','flash/success');
                $this->redirect(array('action' => 'adicionar', $id));
            } else {
                $this->Session->setFlash('Verifique os campos abaixo','flash/error');
            }
        }

		//set lista
		$this->set(compact('lista'));

		//breadcrumb
		$this->_addBreadcrumb('Vídeos');
	}

	/**
	 * Ação que remove um vídeo de uma lista
	 *
	 * @param int $id Identificador da lista
	 * @param int $item_id Identificador do vídeo
	 * @access public
	 * @return null
	 */
	public function remover($id, $video_id){
		$this->_setUser();

		//retorno
		$retorno = array('status' => $this->ListaVideo->remover($id,$video_id));
		$retorno['mensagens'] = $this->ListaVideo->getMensagens();

		//config
		Configure::write('debug',0);
		$this->autoRender = false;
		$this->layout = 'ajax';
        $this->header('Content-type: application/json');

		//return
		return json_encode($retorno);
	}
	
	/**
	 * Ação que remove um vídeo em questão
	 *
	 * @param int $id Identificador da lista
	 * @param int $item_id Identificador do vídeo
	 * @access public
	 * @return null
	 */
	public function admin_remover($id, $video_id){
		$this->ListaVideo->setAdmin(true);

		//retorno
		$retorno = array('status' => $this->ListaVideo->remover($id,$video_id));
		$retorno['mensagens'] = $this->ListaVideo->getMensagens();

		//config
		Configure::write('debug',0);
		$this->autoRender = false;
		$this->layout = 'ajax';
        $this->header('Content-type: application/json');

		//return
		return json_encode($retorno);
	}

	/**
	 * Define o usuário que está acessando a área de lista
	 *
	 * @access protected
	 * @param bool $persiste_sessao Deve persistir o identificador do usuario na sessão?
	 * @return ListaVideosController Próprio Objeto para encadeamento
	 */
	protected function _setUser($persiste_sessao = false) {
		$usuario = $this->Auth->user('id');
		$this->ListaVideo->setUsuarioId($usuario);
		if ($persiste_sessao) {
			$this->Session->write('Lista.usuario_id',$usuario);
		}
		$this->set('usuario_id',$usuario);
		return $this;
	}

	// sobrescrita de gancho para antes da renderização da view
	public function beforeRender() {
		// envia a migalha para a visualização
		$this->set('breadcrumb',$this->_breadcrumb);
	}
}

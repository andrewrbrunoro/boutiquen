<?php
/**
 * Controladora de Relatórios das Listas
 *
 * @uses AppController
 * @category app
 * @package plugins
 * @subpackage lista_de_presente/controllers
 */
class ListaRelatoriosController extends ListaDePresenteAppController {
	
	public $uses = array();
	
	var $components = array('Session','Filter');

	/**
     * obtem os produtos adicionados na lista
     *
     * @param mixed $id
     * @access public
     * @return null
     */
	public function admin_index(){
		
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Produto.nome LIKE '%{%value%}%' OR Grade.sku LIKE '%{%value%}%' OR Produto.sku LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		App::import('Model','Grade');
		$this->Grade = new Grade();
		
		$joins = array(
                    array(
                        'table' => 'variacoes_produtos',
                        'alias' => 'VariacaoProduto',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.grade_id = Grade.id')
                    ),
                    array(
                        'table' => 'produtos',
                        'alias' => 'Produto',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.produto_id = Produto.id')
                    ),
                    array(
						'table' => 'variacoes',
                        'alias' => 'Variacao',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.variacao_id = Variacao.id')
					)
				);
		$this->paginate = array(
			'fields' => array(
							'Grade.id',
							'VariacaoProduto.*',
							'Variacao.id','Variacao.valor',
							'Produto.id','Produto.nome',
							'(SELECT COUNT(id) FROM lista_produtos AS ListaProduto WHERE ListaProduto.grade_id = Grade.id) as itens_listas',
							'(SELECT IFNULL(SUM(quantidade),0) FROM lista_produtos AS ListaProduto WHERE ListaProduto.grade_id = Grade.id) as itens_totais',
							'(SELECT COUNT(PedidoItens.id) FROM pedido_itens AS PedidoItens INNER JOIN pedidos as Pedido ON PedidoItens.pedido_id = Pedido.id WHERE PedidoItens.grade_id = Grade.id AND Pedido.lista_id <> "" AND Pedido.pedido_status_id = 1) as itens_comprados'
					),
			'recursive' => 0,
			'joins' => $joins,
			'order' => 'itens_listas DESC'
		);
		
		$grades = $this->paginate("Grade", $conditions);
		
		$this->set(compact('grades'));		
	}

	public function admin_exportar($conditions){
		
		App::import('Model','Grade');
		$this->Grade = new Grade();
		
		$joins = array(
                    array(
                        'table' => 'variacoes_produtos',
                        'alias' => 'VariacaoProduto',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.grade_id = Grade.id')
                    ),
                    array(
                        'table' => 'produtos',
                        'alias' => 'Produto',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.produto_id = Produto.id')
                    ),
                    array(
						'table' => 'variacoes',
                        'alias' => 'Variacao',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.variacao_id = Variacao.id')
					)
				);
		
		$rows = $this->Grade->find('all',array(
											'recursive' => 0,
											'conditions' => $conditions,
											'fields' => array(
															'Grade.id',
															'VariacaoProduto.*',
															'Variacao.id','Variacao.valor',
															'Produto.id','Produto.nome',
																'(SELECT COUNT(id) FROM lista_produtos AS ListaProduto WHERE ListaProduto.grade_id = Grade.id) as itens_listas',
															'(SELECT IFNULL(SUM(quantidade),0) FROM lista_produtos AS ListaProduto WHERE ListaProduto.grade_id = Grade.id) as itens_totais',
															'(SELECT COUNT(PedidoItens.id) FROM pedido_itens AS PedidoItens INNER JOIN pedidos as Pedido ON PedidoItens.pedido_id = Pedido.id WHERE PedidoItens.grade_id = Grade.id AND Pedido.lista_id <> "" AND Pedido.pedido_status_id = 1) as itens_comprados'
													),
											
											'joins' => $joins,
											'order' => 'itens_listas DESC'
											)
								);
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Produto</strong></td>
					<td><strong>Grade ID</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Variação")."</strong></td>
					<td><strong>Total: Listas</strong></td>
					<td><strong>Total: Quantidades</strong></td>
					<td><strong>Total: Adquiridos</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$table .= "
				<tr>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Produto']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Grade']['id'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Variacao']['valor'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row[0]['itens_listas'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row[0]['itens_totais'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row[0]['itens_comprados'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "relatorio_lista_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
}

<?php
class ListaDePresenteAppController extends AppController {
	
	/**
	 * Migalha
	 *
	 * @var array
	 * @access protected
	 */
	protected $_breadcrumb = array();
	
	/**
	 * Persiste mensagens de sucesso e de erro
	 *
	 * @param array $mensagens Mensagens
	 * @access protected
	 * @return ListaController Próprio objeto para encadeamento
	 */
	protected function _setMensagens($mensagens) {
		foreach ($mensagens as $mensagem) {
			switch ($mensagem['tipo']) {
				case Lista::MENSAGEM_ALERTA:
				case Lista::MENSAGEM_ERRO:
					$tipo = 'flash/error';
					break;
				case Lista::MENSAGEM_SUCESSO:
					$tipo = 'flash/success';
					break;
			}
			$this->Session->setFlash($mensagem['mensagem'],$tipo);
		}
		return $this;
	}
	
	/**
	 * Adição de migalha
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param string $nome Nome da migalha
	 * @param mixed $url   Url de acesso (opcional). Deve seguir o padrão do cake para url
	 * @access protected
	 * @return ListaDePresenteAppController Próprio Objeto para encadeamento
	 */
	protected function _addBreadcrumb($nome,$url = null) {
		if (empty($url)) {
			$url = 'javascript:void(0);';
		}
		$this->_breadcrumb[] = array('nome' => $nome, 'url' => $url);
		return $this;
	}
	
	// sobrescrita de gancho para antes da execução das filtragens
	public function beforeFilter() {
		// adiciona a migalha para a página principal das listas
		$this->_addBreadcrumb(
			'Lista de Presentes',
			array('plugin' => 'lista_de_presente', 'controller' => 'listas', 'action' => 'index')
		);
		return parent::beforeFilter();
	}
}
?>
<?php
class ListaDePresenteAppModel extends AppModel {
	
	/**
	 * Mensagem de erro
	 */
	const MENSAGEM_ERRO = 'erro';

	/**
	 * Mensagem de alerta
	 */
	const MENSAGEM_ALERTA = 'alerta';

	/**
	 * Mensagem de sucesso
	 */
	const MENSAGEM_SUCESSO = 'sucesso';
	
	/**
	 * Identificador do usuário
	 * @var int
	 */
	protected $_usuarioId = null;

	/**
	 * Objeto de camada de controle
	 *
	 * @var mixed
	 * @access protected
	 */
	protected $_controller = null;

	/**
	 * Mensagens de operações
	 *
	 * @var array
	 * @access protected
	 */
	protected $_mensagens = array();
	
	/**
	 * Flag que indica operacoes via admin
	 *
	 * @var boolean
	 * @access protected
	 */
	protected $_admin = null;
	
	/**
	 * Define o usuário
	 *
	 * @param int $id Identificador do usuário
	 * @return Lista Próprio Objeto para encadeamento
	 */
	public function setUsuarioId($id) {
		$this->_usuarioId = $id;
		return $this;
	}

	/**
	 * Retorna o usuário
	 *
	 * @return int $id Identificador do usuário
	 */
	public function getUsuarioId() {
		return $this->_usuarioId;
	}
	
	/**
	 * Verifica se o usuário atual é dono da lista em questão
	 *
	 * @param int $id Identificador da lista
	 * @return boolean True se o usuário for dono da lista, e FALSE se não for
	 */
	public function isUsuarioLista($id) {
		if($this->getAdmin()){
			return true;
		}
		
		App::import('Model','ListaDePresente.Lista');
		$model = new Lista();
		$existe = $model->find('count',array(
			'conditions' => array(
				'usuario_id' => $this->getUsuarioId(),
				'Lista.id' => $id,
			)
		));
		return $existe > 0;
	}

	/**
	 * Define o objeto de camada de controle
	 *
	 * @param Controller $controller Controller em questão
	 * @return ListaDePresenteAppModel Próprio Objeto para encadeamento
	 */
	public function setController(Controller $controller) {
		$this->_controller = $controller;
		return $this;
	}

	/**
	 * Retorna a objeto da camada de controle
	 *
	 * @return ListaDePresenteAppModel Próprio Objeto para encadeamento
	 */
	public function getController() {
		if (is_null($this->_controller)) {
			throw new Exception('Uma controladora deve ser informada!');
		}
		return $this->_controller;
	}
	
	/**
	 * Adiciona uma mensagem
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @param string $mensagem Texto da mensagem
	 * @param string $tipo     Tipo da mensagem
	 * @access protected
	 * @return ListaDePresenteAppModel Próprio Objeto para encadeamento
	 */
	protected function _addMensagem($mensagem,$tipo) {
		switch ($tipo) {
			case self::MENSAGEM_ERRO:
			case self::MENSAGEM_ALERTA:
			case self::MENSAGEM_SUCESSO:
				break;
			default:
				throw new Exception('Por favor informe um tipo de mensagem válido');
		}

		$this->_mensagens[] = array('mensagem' => $mensagem,'tipo' => $tipo);
		return $this;
	}

	/**
	 * Obtenção de mensagens
	 *
	 * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
	 *
	 * @access public
	 * @return array
	 */
	public function getMensagens() {
		return $this->_mensagens;
	}
	
	/**
	 * Defini se as operacoes estao sendo feitas ou não no admin
	 *
	 * @access public
	 * @return ListaDePresenteAppModel Próprio Objeto para encadeamento
	 */
	public function setAdmin( $flag ){
		$this->_admin = (bool) $flag;
		return $this;
	}
	
	/**
	 * Obtem se as operacoes estao sendo feitas ou não no admin
	 *
	 * @access public
	 * @return ListaDePresenteAppModel Próprio Objeto para encadeamento
	 */
	public function getAdmin(){
		return $this->_admin;
	}
	
}
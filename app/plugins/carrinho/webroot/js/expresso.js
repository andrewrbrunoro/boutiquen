$(document).ready(function() {

    $.ajaxSetup({
        cache: false
    });
    $('.quantidade').mask('9999999');
    $('.mask-cep').mask('99.999-999');
    $('.mask-telefone').mask('(99) 9999-99999');
    $('.mask-cpf').mask('999.999.999-99');
    $('.mask-cnpj').mask('99.999.999.9999/99');
    $('.mask-data').mask('99/99/9999');
    $('.mask-numerico').mask('0#');

    //
    //$(".loading").ajaxStart(function() {
    //    $(this).css("visibility", "visible");
    //    $(this).css("display", "block");
    //});
    //
    //$(".loading").ajaxComplete(function(event, request, settings) {
    //    $(this).css("visibility", "hidden");
    //    $(this).css("display", "none");
    //});

    if ($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J') {
        //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
        $("#UsuarioSexo").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioDataNascimento").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioRg").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioCpf").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioCnpj").removeAttr("disabled").parents('.row').show();
        $("#UsuarioNomeFantasia").removeAttr("disabled").parents('.row').show();
        $("#UsuarioRazaoSocial").removeAttr("disabled").parents('.row').show();
        $("#UsuarioInscricaoEstadual").removeAttr("disabled").parents('.row').show();
    } else {
        //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
        $("#UsuarioSexo").removeAttr("disabled").parents('.row').show();
        $("#UsuarioDataNascimento").removeAttr("disabled").parents('.row').show();
        $("#UsuarioRg").removeAttr("disabled").parents('.row').show();
        $("#UsuarioCpf").removeAttr("disabled").parents('.row').show();
        $("#UsuarioCnpj").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioNomeFantasia").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioRazaoSocial").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parents('.row').hide();
    }

    $("input[name='data[Usuario][tipo_pessoa]']").change(function() {
        if ($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J') {
            //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
            $("#UsuarioSexo").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioRg").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioCnpj").removeAttr("disabled").parents('.row').show();
            $("#UsuarioNomeFantasia").removeAttr("disabled").parents('.row').show();
            $("#UsuarioRazaoSocial").removeAttr("disabled").parents('.row').show();
            $("#UsuarioInscricaoEstadual").removeAttr("disabled").parents('.row').show();
        } else {
            //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
            $("#UsuarioSexo").removeAttr("disabled").parents('.row').show();
            $("#UsuarioDataNascimento").removeAttr("disabled").parents('.row').show();
            $("#UsuarioRg").removeAttr("disabled").parents('.row').show();
            $("#UsuarioCpf").removeAttr("disabled").parents('.row').show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parents('.row').hide();
        }
    });

    $("input[name='data[Usuario][email]']").blur(function() {
        if (typeof email !== "undefined" && email == $('#UsuarioEmail').val()) {
            return false;
        } else {
            email = $("input[name='data[Usuario][email]']")[2].value;
            if (email.length > 0) {
                $.post(PATH.basename + "/usuarios/check_email/" + email, {}, function(data) {
                if (data.success) {
                        $(".error-message").remove();
                        $("input[name='data[Usuario][email]']")[2].style.border = '2px solid green';
                    } else {
                        alert(data.error);
                        $("input[name='data[Usuario][email]']")[2].style.border = '2px solid red';
                    }
                }, "json");
            }
        }
    });

    $("input[name='data[Usuario][cpf]']").blur(function() {
        if (typeof cpf !== "undefined" && cpf == $('#UsuarioCpf').val()) {
            return false;
        } else {
            cpf = $('#UsuarioCpf').val();
            if (cpf.length > 0) {
                $.post(PATH.basename + "/usuarios/check_cpf/" + cpf, {}, function(data) {
                if (data.success) {
                        $(".error-message").remove();
                        $('#UsuarioCpf').css({ 'border': '2px solid green'});
                    } else {
                        alert(data.error);
                        $('#UsuarioCpf').css({ 'border': '2px solid red'});
                    }
                }, "json");
            }
        }
    });

    //    if($("input[name='data[UsuarioEndereco][entrega][]']").is(':checked')){
    //        $('.enderecos-entrega').hide();
    //    }else{
    //        $('.enderecos-entrega').show();
    //    }
    $("input[name='data[UsuarioEndereco][entrega][]']").change(function() {
        //showLoading();
        if($(this).is(':checked')){
            $('.enderecos-entrega').hide();
            $('.lista-enderecos').find("input[name='data[UsuarioEndereco][id]']").first().attr('checked','checked');
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + $('.lista-enderecos').find("input[name='data[UsuarioEndereco][id]']").first().attr('rel'));
            $('.form-endereco-entrega input').each(function(i){
                $(this).attr("disabled", "disabled");
            });
            hiddenLoading();
        }else{
            $('.enderecos-entrega').show();
            $('.form-endereco-entrega').hide();
            $('.form-endereco-entrega input').each(function(i){
               $(this).attr("disabled", "disabled");
            });
            $(".btn-cadastrar-novo-endereco").show();
            hiddenLoading();
        }
    });

    if($("input[name='data[UsuarioEndereco][id]']").is(':checked')){        
        $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + $('.lista-enderecos').find("input[name='data[UsuarioEndereco][id]']").first().attr('rel'));
    }

    if($('.form-endereco-entrega').find('.error-message').length > 0 || $('.btn-cadastrar-novo-endereco').attr('display') == "none" || ($("#UsuarioEnderecoCep").val() != "" && $("#UsuarioEnderecoCep").attr("disabled") != "disabled")){
        $(".btn-cadastrar-novo-endereco").hide();
        $('.enderecos-entrega').show();
        $('.form-endereco-entrega').show();
        $('.form-endereco-entrega input').each(function(i){
            $(this).removeAttr("disabled");
        });
    }else{
        $('.form-endereco-entrega').hide();
        $('.form-endereco-entrega input').each(function(i){
            $(this).attr("disabled", "disabled");
        });
        $(".btn-cadastrar-novo-endereco").show();
    }

    $(".btn-cadastrar-novo-endereco").on('click', function(){
         $(this).hide();
         $(".form-endereco-entrega").fadeIn();
         $('.form-endereco-entrega input').each(function(i){
            $(this).removeAttr("disabled");
         });
         $('.endereco-entrega').attr('checked', false);
    });


    $('.endereco-entrega').click(function() {
        var cep = $(".endereco-entrega:checked").attr('rel');
        $(".form-endereco-entrega").fadeOut();
        $('.form-endereco-entrega input').each(function(i){
            $(this).attr("disabled", "disabled");
        });
        $(".btn-cadastrar-novo-endereco").fadeIn();
        if (cep) {
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
            $('.fretes-disponiveis-msg').empty();
        } else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
        return true;
    });

    $('.fretes-disponiveis .tipo-entrega').on('click', function() {
        //showLoading();
		var tipo_cartao = get_tipo_cartao_credito($("input[name='data[Pedido][numero_cartao]']").val());
		if (tipo_cartao != "" || tipo_cartao != null) {
			setTimeout(function(){
				//$('.lista-cartao-credito li').find("#"+tipo_cartao).parent('label').find('.tipo_pagamento').attr('checked',true);
				$('.lista-cartao-credito li').find("#"+tipo_cartao).parent('label').find('.tipo_pagamento').trigger('click');
			}, 700);			
		}

        if($(".endereco-entrega:checked").length){
            var cep = $(".endereco-entrega:checked").attr('rel');
        }else if($("#UsuarioEnderecoCep").length){
            var cep = $("#UsuarioEnderecoCep").val();
        }

        $('#cupom').val("");
        if($('.cupom-lbl')){
            $('.cupom-lbl').html("");
        }

        if (cep) {
            $.post(PATH.basename + "/carrinho/carrinho/set_frete/", {tipo_entrega: $('.tipo-entrega:checked').val(), cep: cep}, function(response) {
                if (response.dados) {
                    $('#frete').html(response.dados.frete);
                    $('#total').html(response.dados.total);
                    $('#sub_total').html(response.dados.sub_total);
                    //escondo/exibo enderecos da loja ou do usuario
                    if(typeof(response.retirar_na_loja) != "undefined" && response.retirar_na_loja !== null){
                        if(response.retirar_na_loja == true){
                            $("#loja_entrega_box").show();
                            $("#endereco_entrega_box").hide();
                        }else{
                            $("#loja_entrega_box").hide();
                            $("#endereco_entrega_box").show();
                        }
                    }
                }
                try {
                    if (response.Cupom.status == true) {

                        $('.cupom-lbl').html(null);
                        $('.produto-totalizador,#total').attr('style', null);
                        $('.cupom-msg').html(response.Cupom.msg);
                        if (response.Cupom.tipo == "FRETE") {
                            $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                            $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                            $('#frete').css('text-decoration', 'line-through');
                        } else {
                            $.each(response.Carrinho.itens, function(i, v) {
                                $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                                $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                            })
                            $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                        }
                        $('#total').css('text-decoration', 'line-through');
                    } else {
                        $('.cupom-msg').html(response.Cupom.msg[0]);
                    }
                } catch (e) {
                }
                hiddenLoading();
            }, "json");
        } else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
            hiddenLoading();
        }
    });

    if($("input[name='data[Pedido][numero_cartao]']").val() != ""){
        var tipo_cartao = get_tipo_cartao_credito($("input[name='data[Pedido][numero_cartao]']").val());
        if ($("#"+tipo_cartao).length) {
            $('.lista-cartao-credito li img').removeClass('enabled').addClass('disabled');
            $('.lista-cartao-credito li').find("#" + tipo_cartao).removeClass('disabled').addClass('enabled');
            if (tipo_cartao == 'AMEX') {
                $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
            } else {
                $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
            }
        }
    } else {
    	$('.lista-cartao-credito li').find('.tipo_pagamento').attr('checked',false);
    }

    $("input[name='data[Pedido][numero_cartao]']").keyup(function() {
        var tipo_cartao = get_tipo_cartao_credito($(this).val());
        if($("#"+tipo_cartao).length){
            $('.lista-cartao-credito li img').removeClass('enabled').addClass('disabled');
            $('.lista-cartao-credito li').find("#" + tipo_cartao).removeClass('disabled').addClass('enabled');
            if (tipo_cartao == 'AMEX') {
                $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
            } else {
                $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
            }
        }
    });

    $("input[name='data[Pedido][numero_cartao]']").blur(function() {
        //showLoading();
        var tipo_cartao = get_tipo_cartao_credito($(this).val());
        var radio = $('.lista-cartao-credito li').find("#" + tipo_cartao).parent('label').find('.tipo_pagamento');
        if($("#"+tipo_cartao).length){
            $(".PedidoParcelas").html('<option value"">Carregando...</option>');
            radio.attr('checked','checked');
            $.post(PATH.basename + "/carrinho/carrinho/ajax_forma_pagamento/" + radio.val(), {}, function(data) {
                $(".PedidoParcelas").empty();
                $(".PedidoParcelas").append(data);
                hiddenLoading();
            }, "json");
        }else{
            hiddenLoading();
        }
    });

    $('.tipo-de-pagamento').click(function() {
        $(".tipo_pagamento").attr('checked', false);
        $('.forms-pagamentos').hide();
        $('.'+$(this).val()).css('display', 'block');
        var rel = $(this).attr('rel');
        $('.tipo_pagamento').attr('checked',false);
        switch (rel) {
            case 'PAGSEGURO':
                $('.tipo_pagamento').attr('checked',false);
                $('.PAGSEGURO .tipo_pagamento').attr('checked',true);
                break;
            case 'BOLETO':
                $('.tipo_pagamento').attr('checked',false);
                $('.BOLETO .tipo_pagamento').attr('checked','checked');
                break;
            case 'CARTAO':
                $('.tipo_pagamento').attr('checked',false);
                $('.CARTAO .tipo_pagamento').attr('checked','checked');
                break;
        }
    });

    if ($('.tipo-de-pagamento:checked').val()) {
        $('.forms-pagamentos').hide();
        $('.' + $('.tipo-de-pagamento:checked').val()).show();
        var rel = $('.tipo-de-pagamento:checked').attr('rel');
        switch (rel) {
            case 'PAGSEGURO':
                $('.tipo_pagamento').attr('checked',false);
                $('.PAGSEGURO .tipo_pagamento').attr('checked',true);
                break;
            case 'BOLETO':
                $('.tipo_pagamento').attr('checked',false);
                $('.BOLETO .tipo_pagamento').attr('checked',true);
                break;
            case 'CARTAO':
                $('.tipo_pagamento').attr('checked',false);
                $('.CARTAO .tipo_pagamento').attr('checked',true);
                break;
        }
    }

    $('.tipo_pagamento').click(function() {
        //showLoading();
        $('.lista-cartao-credito li img').removeClass('enabled').addClass('disabled');
        $(this).parent("label").find("img").removeClass('disabled').addClass('enabled');
        $(".PedidoParcelas").html('<option value"">Carregando...</option>');
        $.post(PATH.basename + "/carrinho/carrinho/ajax_forma_pagamento/" + $(this).val(), {}, function(data) {
            $(".PedidoParcelas").empty();
            $(".PedidoParcelas").append(data);
            hiddenLoading();
        }, "json");
    });

    $('.btn-finalizar-compra').click(function() {
        $(this).hide();
        $('.gif_carregando').show();
    });

    $('.carrinho-expresso-endereco-cobranca').hide();
    $('#UsuarioEnderecoCep').on('change',function() {
        //showLoading();
        cep = $(this).val();
        if (cep) {
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
        }else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
        $('.carrinho-expresso-endereco-cobranca').fadeIn();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep, function(values) {
            if (values) {
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
            hiddenLoading();
        }, 'json');
    });

    if ($('#UsuarioEnderecoCep').val() != ""){
        cep = $('#UsuarioEnderecoCep').val();
        $('.carrinho-expresso-endereco-cobranca').fadeIn();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep, function(values) {
            if (values) {
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        }, 'json');
    }

    function converteMoedaFloat(valor){
        if(valor === ""){
            valor =  0;
        }else{
            valor = valor.replace(".","");
            valor = valor.replace(",",".");
            valor = parseFloat(valor);
        }
        return valor;
    }

    function recallPagseguro (amount) {
        amount = converteMoedaFloat(amount);
        $("#amount").val(amount);
        PagSeguroDirectPayment.getInstallments({
            amount: amount,
            brand: '',
            maxInstallmentNoInterest: 2,
            success: function (result) {
                insta = result.installments;
                var $si = $('select[name="card_installment"]');
                $si.html('');
                $.each(result, function (key, object) {
                    if (object) {
                        var $string = object.interestFree ? 'com/juros' : 'sem/juros';
                        if (object.quantity == 1) {
                            $si.append($('<option>', {
                                value: object.quantity + '#' + object.installmentAmount + '#' + object.totalAmount + '#' + object.interestFree,
                                text: valueToCoin(object.installmentAmount) + ' à vista'
                            }));
                        } else {
                            $si.append($('<option>', {
                                value: object.quantity + '#' + object.installmentAmount + '#' + object.totalAmount + '#' + object.interestFree,
                                text: 'Parcelar em ' + object.quantity + 'x de ' + valueToCoin(object.installmentAmount) + ' ' + $string
                            }));
                        }
                    }
                });
            }
        });
    }

    $(document).on('click', 'input[name="Frete"]', function(){
        var $value = $(this).data('value');
        $('#amount').val(parseFloat($value) + parseFloat($('#amount').val()));
    });

    $(document).on('click', 'input[name="pagseguro_payment_type"]', function (e) {
        if ($('.tipo-entrega:checked').val()) {
            $.each($('input[name="Frete"]'), function(){
                if ($(this).attr('id') == 'Frete_') {
                    $(this).remove();
                }
            });
            return true;
        } else {
            alert("Selecione uma Entrega");
            e.preventDefault();
            return false;
        }
    });

    if ($('#cupom').length > 0 && $('#cupom').val().length > 0) {
        var cupom = $('#cupom').val();
        $.post(PATH.basename + "/carrinho/carrinho/set_cupom", {cupom: cupom}, function(response) {
            $('.cupom-lbl').html(null);
            $('.produto-totalizador,#total').attr('style', null);
            if (response.Cupom.status == true) {
                recallPagseguro(response.Carrinho.dados.total_com_desconto);
                $('.cupom-msg').html(response.Cupom.msg);
                if (response.Cupom.tipo == "FRETE") {
                    $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                    $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                    $('#frete').css('text-decoration', 'line-through');
                } else {
                    $.each(response.Carrinho.itens, function(i, v) {
                        $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                        $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                    });
                    $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                }
                $('#total').css('text-decoration', 'line-through').css('color', '#999');
                if (response.Carrinho.dados) {
                    $('#frete').html(response.Carrinho.dados.frete);
                    $('#totais-total').html(response.Carrinho.dados.total);
                    $('#totais-subtotal').html(response.Carrinho.dados.sub_total);
                }
            } else {
                $('.cupom-msg').html(response.Cupom.msg[0]);
            }
            hiddenLoading();
        }, "json");
        return false;
    }



    $('#calcular-cupom').on('click', function() {
        var $this = $('#cupom');
        if ($this.val().length > 0) {
            var cupom = $this.val();
            var $cep = $('input[name="data[UsuarioEndereco][id]"]:checked').attr("rel");
            $.post(PATH.basename + "/carrinho/carrinho/set_cupom", {cupom: cupom, cep: $cep}, function(response) {
                console.log(response);
                $('.cupom-lbl').html(null);
                $('.produto-totalizador,#total').attr('style', null);
                if (response.Cupom.status == true) {
                    recallPagseguro(response.Carrinho.dados.total_com_desconto);
                    $('.cupom-msg').html(response.Cupom.msg);
                    if (response.Cupom.tipo == "FRETE") {
                        $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                        $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                        $('#frete').css('text-decoration', 'line-through');
                    } else {
                        $.each(response.Carrinho.itens, function(i, v) {
                            $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                            $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                        });
                        $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                    }
                    $('#total').css('text-decoration', 'line-through').css('color', '#999');
                    if (response.Carrinho.dados) {
                        $('#frete').html(response.Carrinho.dados.frete);
                        $('#totais-total').html(response.Carrinho.dados.total);
                        $('#totais-subtotal').html(response.Carrinho.dados.sub_total);
                    }
                } else {
                    $('.cupom-msg').html(response.Cupom.msg[0]);
                }
                hiddenLoading();
            }, "json");
            return false;
        }else{
            hiddenLoading();
        }
    });

    //efeito que remove o flash error dos campos no evento onclick
    $('input').on('blur', function() {
        if($(this).val() != ""){
            $(this).parent('div').find('.error-message').eq(0).fadeOut();
        }
        return true;
    });
    
    
    /* load */
    if($("input[name='data[Pedido][numero_cartao]']").val() != ""){
        var tipo_cartao = get_tipo_cartao_credito($("input[name='data[Pedido][numero_cartao]']").val());
        setTimeout(function(){
        	//$('.lista-cartao-credito li').find("#"+tipo_cartao).parent('label').find('.tipo_pagamento').attr('checked',true);
        	$('.lista-cartao-credito li').find("#"+tipo_cartao).parent('label').find('.tipo_pagamento').trigger('click');
        }, 2000);    	
    } else {
    	$('.lista-cartao-credito li').find('.tipo_pagamento').attr('checked',false);
    }
    
});

function get_tipo_cartao_credito(numero_cartao){
    var result="false";
    if(/^5[1-5][0-9]{14}$/.test(numero_cartao)){
        result = "MASTERCARD";
    }else if(/^4[0-9]{12}(?:[0-9]{3})?$/.test(numero_cartao)){
        result="VISA";
    }else if(/^3[47][0-9]{13}$/.test(numero_cartao)){
        result="AMEX";
    }else if(/^(?:2131|1800|35\d{3})\d{11}$/.test(numero_cartao)){
        result="JCB";
    }else if(/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/.test(numero_cartao)){
        result="DINERS";
    }else if(/^6(?:011|5[0-9]{2})[0-9]{12}$/.test(numero_cartao)){
        result="DISCOVER";
    }else if(/^6[0-9]{15}$/.test(numero_cartao)){
        result="ELO";
    }
    return result;
}
function showLoading(){
    $(".loading").css("visibility", "visible");
    $(".loading").css("display", "block");
}

function hiddenLoading(){
    $(".loading").css("visibility", "hidden");
    $(".loading").css("display", "none");
}
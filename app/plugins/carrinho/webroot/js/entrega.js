/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(function() {

    $('.mask-cep').setMask({mask: '99.999-999'});
    $('.mask-telefone').setMask({mask: '(99) 9999-99999'});
    $('.mask-cpf').setMask({mask: '999.999.999-99'});
    $('.mask-cnpj').setMask({mask: '99.999.999.9999/99'});
    $('.mask-data').setMask({mask: '99/99/9999'});

    $('.mask-telefone-8-9').keypress(function() {
        mascara_8_9_dig(this, mtel);
    });

    if ($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J') {
        $("#UsuarioSexo").attr("disabled", "disabled").parent().hide();
        $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
        $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
        $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
        $("#UsuarioCnpj").attr("disabled", "").parent().show();
        $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
        $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
        $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
        //$("#UsuarioTipoPessoa").val('J');
    } else {
        $("#UsuarioSexo").attr("disabled", "").parent().show();
        $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
        $("#UsuarioRg").attr("disabled", "").parent().show();
        $("#UsuarioCpf").attr("disabled", "").parent().show();
        $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
        $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
        $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
        $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
        //$("#UsuarioTipoPessoa").val('F');
    }

    $("input[name='data[Usuario][tipo_pessoa]']").change(function() {
        if ($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J') {
            $("#UsuarioSexo").attr("disabled", "disabled").parent().hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRg").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parent().hide();
            $("#UsuarioCnpj").attr("disabled", "").parent().show();
            $("#UsuarioNomeFantasia").attr("disabled", "").parent().show();
            $("#UsuarioRazaoSocial").attr("disabled", "").parent().show();
            $("#UsuarioInscricaoEstadual").attr("disabled", "").parent().show();
            //$("#UsuarioTipoPessoa").val('J');
        } else {
            $("#UsuarioSexo").attr("disabled", "").parent().show();
            $("#UsuarioDataNascimento").attr("disabled", "").parent().show();
            $("#UsuarioRg").attr("disabled", "").parent().show();
            $("#UsuarioCpf").attr("disabled", "").parent().show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parent().hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parent().hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parent().hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parent().hide();
        }

    });

    $("#loading").ajaxStart(function() {
        $(this).css("visibility", "visible");
    });

    $("#loading").ajaxComplete(function(event, request, settings) {
        $(this).css("visibility", "hidden");
    });

    $("#UsuarioEnderecoCep").change(function() {
        cep = $(this).val();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep, function(values) {
            if (values) {
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        }, 'json');
    });

    if ($('#UsuarioEnderecoCep').val() != "") {
        cep = $('#UsuarioEnderecoCep').val();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep, function(values) {
            if (values) {
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        }, 'json');
    }
    ;


    $(".useDefault").addDefaultText();

    if ($('.flash_error').length) {
        $('.form-endereco-content-entrega').fadeIn(500);
    }

    $('a.editar').click(function() {
        $('.form-endereco-content-entrega').fadeIn(500);
        $.post(PATH.basename + "/usuarios/ajax_endereco_usuario/" + $(this).attr('rel'), function(values) {
            if (values) {
                $("input[name='data[UsuarioEndereco][cobranca]']").each(function() {
                    if ($(this).val() == values.UsuarioEndereco.cobranca) {
                        $(this).attr('checked', 'checked');
                    }
                });
                $('#UsuarioEnderecoId').val(values.UsuarioEndereco.id);
                $('#UsuarioEnderecoRua').val(values.UsuarioEndereco.rua);
                $('#UsuarioEnderecoCidade').val(values.UsuarioEndereco.cidade);
                $('#UsuarioEnderecoBairro').val(values.UsuarioEndereco.bairro);
                $('#UsuarioEnderecoUf').val(values.UsuarioEndereco.uf);
                $('#UsuarioEnderecoNome').val(values.UsuarioEndereco.nome);
                $('#UsuarioEnderecoNumero').val(values.UsuarioEndereco.numero);
                $('#UsuarioEnderecoComplemento').val(values.UsuarioEndereco.complemento);
                $('#UsuarioEnderecoCep').val(values.UsuarioEndereco.cep).setMask({mask: '99.999-999'});
            }
        }, 'json');
        $('.form-endereco-content-entrega .row').each(function(){
            $('.error-message').remove();
        });
        $("html, body").animate({scrollTop: $('.form-endereco-content-entrega').offset().top}, 'slow');
        return false;
    })

    /*##  begin tela de enderecos de entrega ##*/
    $("ul.my-addresses li a").click(function() {
        $("ul.my-addresses li a").removeClass('active');
        $(".box-dados-endereco").hide();
        $("#endereco-" + $(this).attr('rel')).show();
        $(this).addClass('active');

    });

    $("#btn-cadastrar-novo-endereco").click(
            function() {
                $('.form-endereco-content-entrega').fadeOut();
                $("input[name='data[UsuarioEndereco][cobranca]']").each(function() {
                    if ($(this).val() == 0) {
                        $(this).attr('checked', 'checked');
                    }
                });
                $('#UsuarioEnderecoId').val('');
                $('#UsuarioEnderecoRua').val('');
                $('#UsuarioEnderecoCidade').val('');
                $('#UsuarioEnderecoBairro').val('');
                $('#UsuarioEnderecoUf').val('');
                $('#UsuarioEnderecoNome').val('');
                $('#UsuarioEnderecoNumero').val('');
                $('#UsuarioEnderecoComplemento').val('');
                $('#UsuarioEnderecoCep').val('').setMask({mask: '99.999-999'});
                $('.form-endereco-content-entrega .row').each(function(){
                    $('.error-message').remove();
                });
                $('.form-endereco-content-entrega').fadeIn(500);
                $('html, body').animate({
                    scrollTop: $(".form-endereco-content-entrega").offset().top
                }, 500);
            });
});
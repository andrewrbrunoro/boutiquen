$(document).ready(function() {
    $('a.pedido').click(function() {
        $('#pedido-detalhe-' + $(this).attr('rel')).toggle();
        if ($('#pedido-detalhe-' + $(this).attr('rel')).css("display") == 'none') {
            $(this).find('img').show();
            $(this).find('span').hide();
        } else {
            $(this).find('img').hide();
            $(this).find('span').show();
        }
        return false;
    });

    //scroll for top
    $("#btn-subir").click(function() {
        $('html, body').animate({
            scrollTop: $("#header").offset().top
        }, 500);
    });

    //back history
    $("#btn-voltar").click(function() {
        window.history.back()
    });
});
$(document).ready(function() {

    $.ajaxSetup({
        cache: false
    });
	
	$.datepicker.regional['pt-BR'] = {
            closeText: 'Fechar',
            prevText: '&#x3c;Anterior',
            nextText: 'Pr&oacute;ximo&#x3e;',
            currentText: 'Hoje',
            monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
            'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
            'Jul','Ago','Set','Out','Nov','Dez'],
            dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sabado'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
            dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 0,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['pt-BR']);
    $('.datePicker').datepicker();
    
    $(".useDefault").addDefaultText();
    $('.mask-cep').setMask({mask: '99.999-999',autoTab: false});
    $('.quantidade').setMask({mask: '9999999'});
    $('.mask-cep').setMask({mask: '99.999-999'});
    $('.mask-telefone').setMask({mask: '(99) 9999-99999'});
    $('.mask-cpf').setMask({mask: '999.999.999-99'});
    $('.mask-cnpj').setMask({mask: '99.999.999.9999/99'});
    $('.mask-data').setMask({mask: '99/99/9999'});
    $('.mask-numerico').setMask({mask: '9', type: 'repeat'});
    $('#PedidoCodigoSegurancaCartao').setMask({
        mask: '999'
    });
    $('#PedidoNumeroCartao').setMask({
        mask: '9999999999999999'
    });
    $('.tipo_pagamento').click(function() {
        if ($(this).attr('rel') == 'AMEX') {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
        } else {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
        }
    })
    if ($('.tipo_pagamento:checked').val()) {
        if ($('.tipo_pagamento:checked').attr('rel') == 'AMEX') {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
        } else {
            $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
        }
    }
	
    $(".loading").ajaxStart(function() {
        $(this).css("visibility", "visible");
        $(this).css("display", "block");
    });
	
    $(".loading").ajaxComplete(function(event, request, settings) {
        $(this).css("visibility", "hidden");
        $(this).css("display", "none");
    });    
    
    if ($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J') {
        //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
        $("#UsuarioSexo").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioDataNascimento").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioRg").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioCpf").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioCnpj").removeAttr("disabled").parents('.row').show();
        $("#UsuarioNomeFantasia").removeAttr("disabled").parents('.row').show();
        $("#UsuarioRazaoSocial").removeAttr("disabled").parents('.row').show();
        $("#UsuarioInscricaoEstadual").removeAttr("disabled").parents('.row').show();
    } else {
        //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
        $("#UsuarioSexo").removeAttr("disabled").parents('.row').show();
        $("#UsuarioDataNascimento").removeAttr("disabled").parents('.row').show();
        $("#UsuarioRg").removeAttr("disabled").parents('.row').show();
        $("#UsuarioCpf").removeAttr("disabled").parents('.row').show();
        $("#UsuarioCnpj").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioNomeFantasia").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioRazaoSocial").attr("disabled", "disabled").parents('.row').hide();
        $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parents('.row').hide();
    }

    $("input[name='data[Usuario][tipo_pessoa]']").change(function() {
        if ($("input[name='data[Usuario][tipo_pessoa]']:checked").val() == 'J') {
            //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
            $("#UsuarioSexo").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioDataNascimento").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioRg").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioCpf").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioCnpj").removeAttr("disabled").parents('.row').show();
            $("#UsuarioNomeFantasia").removeAttr("disabled").parents('.row').show();
            $("#UsuarioRazaoSocial").removeAttr("disabled").parents('.row').show();
            $("#UsuarioInscricaoEstadual").removeAttr("disabled").parents('.row').show();
        } else {
            //substituir 'parents('.row')' por .parent para voltar ao padrão de layout
            $("#UsuarioSexo").removeAttr("disabled").parents('.row').show();
            $("#UsuarioDataNascimento").removeAttr("disabled").parents('.row').show();
            $("#UsuarioRg").removeAttr("disabled").parents('.row').show();
            $("#UsuarioCpf").removeAttr("disabled").parents('.row').show();
            $("#UsuarioCnpj").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioNomeFantasia").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioRazaoSocial").attr("disabled", "disabled").parents('.row').hide();
            $("#UsuarioInscricaoEstadual").attr("disabled", "disabled").parents('.row').hide();
        }
    });
    
    //    if($("input[name='data[UsuarioEndereco][entrega][]']").is(':checked')){
    //        $('.enderecos-entrega').hide();
    //    }else{
    //        $('.enderecos-entrega').show();
    //    }
    $("input[name='data[UsuarioEndereco][entrega][]']").change(function() {
        if($(this).is(':checked')){
            $('.enderecos-entrega').hide();
            $('.lista-enderecos').find("input[name='data[UsuarioEndereco][id]']").first().attr('checked','checked');
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + $('.lista-enderecos').find("input[name='data[UsuarioEndereco][id]']").first().attr('rel'));
            $('.form-endereco-entrega input').each(function(i){
                $(this).attr("disabled", "disabled");
            });
        }else{
            $('.enderecos-entrega').show();
            $('.form-endereco-entrega').hide();
            $('.form-endereco-entrega input').each(function(i){
               $(this).attr("disabled", "disabled");
            });
            $(".btn-cadastrar-novo-endereco").show();
        }
    });
    
    if($('.form-endereco-entrega').find('.error-message').length > 0 || $('.btn-cadastrar-novo-endereco').attr('display') == "none" || ($("#UsuarioEnderecoCep").val() != "" && $("#UsuarioEnderecoCep").attr("disabled") != "disabled")){
        $(".btn-cadastrar-novo-endereco").hide();
        $('.enderecos-entrega').show();
        $('.form-endereco-entrega').show();
        $('.form-endereco-entrega input').each(function(i){
            $(this).removeAttr("disabled");
        });
    }else{
        $('.form-endereco-entrega').hide();
        $('.form-endereco-entrega input').each(function(i){
            $(this).attr("disabled", "disabled");
        });
        $(".btn-cadastrar-novo-endereco").show();
    }
    
    $(".btn-cadastrar-novo-endereco").live('click', function(){
         $(this).hide();
         $(".form-endereco-entrega").fadeIn();
         $('.form-endereco-entrega input').each(function(i){
            $(this).removeAttr("disabled");
         });
         $('.endereco-entrega').attr('checked', false);
    });
    
    
    $('.endereco-entrega').click(function() {
        var cep = $(".endereco-entrega:checked").attr('rel');
        $(".form-endereco-entrega").fadeOut();
        $('.form-endereco-entrega input').each(function(i){
            $(this).attr("disabled", "disabled");
        });
        $(".btn-cadastrar-novo-endereco").fadeIn();
        if (cep) {
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
            $('.fretes-disponiveis-msg').empty();
        } else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
        return true;
    });
    
    $('.fretes-disponiveis .tipo-entrega').live('click', function() {
        
        if($(".endereco-entrega:checked").length){
            var cep = $(".endereco-entrega:checked").attr('rel');
        }else if($("#UsuarioEnderecoCep").length){
            var cep = $("#UsuarioEnderecoCep").val();
        }
        
        if (cep) {
            $.post(PATH.basename + "/carrinho/carrinho/set_frete/", {tipo_entrega: $('.tipo-entrega:checked').val(), cep: cep}, function(response) {
                if (response.dados) {
                    $('#frete').html(response.dados.frete);
                    $('#total').html(response.dados.total);
                    $('#sub_total').html(response.dados.sub_total);
                }
                try {
                    if (response.Cupom.status == true) {

                        $('.cupom-lbl').html(null);
                        $('.produto-totalizador,#total').attr('style', null);
                        $('.cupom-msg').html(response.Cupom.msg);
                        if (response.Cupom.tipo == "FRETE") {
                            $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                            $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                            $('#frete').css('text-decoration', 'line-through');
                        } else {
                            $.each(response.Carrinho.itens, function(i, v) {
                                $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                                $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                            })
                            $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                        }
                        $('#total').css('text-decoration', 'line-through');
                    } else {
                        $('.cupom-msg').html(response.Cupom.msg[0]);
                    }
                } catch (e) {
                }

            }, "json");
        } else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
    });
    
    $("input[name='data[Pedido][numero_cartao]']").keyup(function() {
        var tipo_cartao = get_tipo_cartao_credito($(this).val());
        if($("#"+tipo_cartao).length){
            $('.lista-cartao-credito li img').removeClass('enabled').addClass('disabled');
            $('.lista-cartao-credito li').find("#" + tipo_cartao).removeClass('disabled').addClass('enabled');
            if (tipo_cartao == 'AMEX') {
                $('#PedidoCodigoSegurancaCartao').setMask({mask: '9999'});
            } else {
                $('#PedidoCodigoSegurancaCartao').setMask({mask: '999'});
            }
        }
    });
    
    $("input[name='data[Pedido][numero_cartao]']").blur(function() {
        var tipo_cartao = get_tipo_cartao_credito($(this).val());
        var radio = $('.lista-cartao-credito li').find("#" + tipo_cartao).parent('label').find('.tipo_pagamento');
        if($("#"+tipo_cartao).length){
            $(".PedidoParcelas").html('<option value"">Carregando...</option>');
            radio.attr('checked','checked');
            $.post(PATH.basename + "/carrinho/carrinho/ajax_forma_pagamento/" + radio.val(), {}, function(data) {
                $(".PedidoParcelas").empty();
                $(".PedidoParcelas").append(data);
            }, "json");
        }
    });
    
    
    $('.tipo-de-pagamento').click(function() {
        $(".tipo_pagamento").attr('checked', false);
        $('.forms-pagamentos').hide();
        $('.'+$(this).val()).css('display', 'block');
        var rel = $(this).attr('rel');
        $('.tipo_pagamento').attr('checked',false);
        switch (rel) {
            case 'PAGSEGURO':                
                $('.tipo_pagamento').attr('checked',false);
                $('.PAGSEGURO .tipo_pagamento').attr('checked',true);
                break;
            case 'BOLETO':
                $('.tipo_pagamento').attr('checked',false);
                $('.BOLETO .tipo_pagamento').attr('checked','checked');
                break;
        }
    })
    
    if ($('.tipo-de-pagamento:checked').val()) {
        $('.forms-pagamentos').hide();
        $('.' + $('.tipo-de-pagamento:checked').val()).show();
		var rel = $('.tipo-de-pagamento:checked').attr('rel');
        switch (rel) {
            case 'PAGSEGURO':
                $('.tipo_pagamento').attr('checked',false);
                $('.PAGSEGURO .tipo_pagamento').attr('checked',true);
                break;
            case 'BOLETO':
				$('.tipo_pagamento').attr('checked',false);
				$('.BOLETO .tipo_pagamento').attr('checked',true);
				break;
        }
    }
    
    $('.tipo_pagamento').click(function() {
        $('.lista-cartao-credito li img').removeClass('enabled').addClass('disabled');
        $(this).parent("label").find("img").removeClass('disabled').addClass('enabled');
        $(".PedidoParcelas").html('<option value"">Carregando...</option>');
        $.post(PATH.basename + "/carrinho/carrinho/ajax_forma_pagamento/" + $(this).val(), {}, function(data) {
            $(".PedidoParcelas").empty();
            $(".PedidoParcelas").append(data);
        }, "json");
    })
    
    $('.btn-finalizar-compra').click(function() {
        $(this).hide();
        $('.gif_carregando').show();
    });
    
    $('.carrinho-expresso-endereco-cobranca').hide();
    $('#UsuarioEnderecoCep').live('change',function() {
        cep = $(this).val();
        if (cep) {
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
        }else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
        $('.carrinho-expresso-endereco-cobranca').fadeIn();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep, function(values) {
            if (values) {
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        }, 'json');
    });

    if ($('#UsuarioEnderecoCep').val() != ""){
        cep = $('#UsuarioEnderecoCep').val();
        $('.carrinho-expresso-endereco-cobranca').fadeIn();
        $.post(PATH.basename + "/usuario_enderecos/ajax_correios_endereco/" + cep, function(values) {
            if (values) {
                $('#UsuarioEnderecoRua').val(values.rua);
                $('#UsuarioEnderecoCidade').val(values.cidade);
                $('#UsuarioEnderecoBairro').val(values.bairro);
                $('#UsuarioEnderecoUf').val(values.estado);
            }
        }, 'json');
    }
    
    if ($('#cupom').length > 0 && $('#cupom').val().length > 0) {
        var cupom = $('#cupom').val();
        $.post(PATH.basename + "/carrinho/carrinho/set_cupom", {cupom: cupom}, function(response) {
            $('.cupom-lbl').html(null);
            $('.produto-totalizador,#total').attr('style', null);
            if (response.Cupom.status == true) {
                $('.cupom-msg').html(response.Cupom.msg);
                if (response.Cupom.tipo == "FRETE") {
                    $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                    $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                    $('#frete').css('text-decoration', 'line-through');
                } else {
                    $.each(response.Carrinho.itens, function(i, v) {
                        $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                        $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                    })
                    $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                }
                $('#total').css('text-decoration', 'line-through').css('color', '#999');
                if (response.Carrinho.dados) {
                    $('#frete').html(response.Carrinho.dados.frete);
                    $('#totais-total').html(response.Carrinho.dados.total);
                    $('#totais-subtotal').html(response.Carrinho.dados.sub_total);
                }
            } else {
                $('.cupom-msg').html(response.Cupom.msg[0]);
            }
        }, "json");
        return false;
    }

    $('#calcular-cupom').live('click', function() {
        var $this = $('#cupom');
        if ($this.val().length > 0) {
            var cupom = $this.val();
            $.post(PATH.basename + "/carrinho/carrinho/set_cupom", {cupom: cupom}, function(response) {
                $('.cupom-lbl').html(null);
                $('.produto-totalizador,#total').attr('style', null);
                if (response.Cupom.status == true) {
                    $('.cupom-msg').html(response.Cupom.msg);
                    if (response.Cupom.tipo == "FRETE") {
                        $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                        $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                        $('#frete').css('text-decoration', 'line-through');
                    } else {
                        $.each(response.Carrinho.itens, function(i, v) {
                            console.log(v);
                            $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                            $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                        })
                        $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                    }
                    $('#total').css('text-decoration', 'line-through').css('color', '#999');
                    if (response.Carrinho.dados) {
                        $('#frete').html(response.Carrinho.dados.frete);
                        $('#totais-total').html(response.Carrinho.dados.total);
                        $('#totais-subtotal').html(response.Carrinho.dados.sub_total);
                    }
                } else {
                    $('.cupom-msg').html(response.Cupom.msg[0]);
                }
            }, "json");
            return false;
        }
    });
    
    //efeito que remove o flash error dos campos no evento onclick
    $('input').live('blur', function() {
        if($(this).val() != ""){
            $(this).parent('div').find('.error-message').eq(0).fadeOut();
        }
        return true;
    });
});

function get_tipo_cartao_credito(numero_cartao){
    var result="false";
    if(/^5[1-5][0-9]{14}$/.test(numero_cartao)){
        result = "MASTERCARD";
    }else if(/^4[0-9]{12}(?:[0-9]{3})?$/.test(numero_cartao)){
        result="VISA";
    }else if(/^3[47][0-9]{13}$/.test(numero_cartao)){
        result="AMEX";
    }else if(/^(?:2131|1800|35\d{3})\d{11}$/.test(numero_cartao)){
        result="JCB";
    }else if(/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/.test(numero_cartao)){
        result="DINERS";
    }else if(/^6(?:011|5[0-9]{2})[0-9]{12}$/.test(numero_cartao)){
        result="DISCOVER";
    }else if(/^6[0-9]{15}$/.test(numero_cartao)){
        result="ELO";
    }
    return result;
}
$(document).ready(function() {

    $.ajaxSetup({
        cache: false
    });

    $('.mask-cep').mask('99.999-999');
    $('.quantidade').mask('9999999');
    $("#loading").ajaxStart(function() {
        $(this).css("visibility", "visible");
        $(this).css("display", "block");
        //$(this).css("vertical-align", "middle");
        //$(this).css("margin-right", "15px");
    });
    $("#loading").ajaxComplete(function(event, request, settings) {
        $(this).css("visibility", "hidden");
        $(this).css("display", "none");
        //$(this).css("vertical-align", "middle");
        //$(this).css("margin-right", "15px");
    });

    $('.quantidade').change(function() {

        var gradeid = $(this).attr('rel');
        var quantidade = $(this).val();
        var data = new Date();

        $('div.fretes-disponiveis').empty();
        $('.cupom-lbl').empty();
        $('.produto-totalizador,#total,#frete').attr('style', null);
        $('.cupom-msg').empty();

        $.post(PATH.basename + "/carrinho/carrinho/quantidade/" + gradeid + "/" + quantidade + "/" + data.getTime(), {},
                function(response) {
                    if (response.total) {
                        $('#produto-total-' + gradeid).html(response.produto_total);
                        $('#frete').html(response.frete);
                        $('#total').html(response.total);
                        $('#sub_total').html(response.sub_total);
                        $('#quantidade-erro-' + gradeid).hide();
                        try {
                            if (response.Cupom.status == true) {
                                if (response.Cupom.tipo == "FRETE") {
                                    $('#frete-com-desconto').html('R$' + response.Carrinho.dados.frete_com_desconto);
                                    $('#total-com-desconto').html('<strong>R$ </strong>' + response.Carrinho.dados.total_com_desconto);
                                    $('#frete').css('text-decoration', 'line-through');
                                } else {
                                    $.each(response.Carrinho.itens, function(i, v) {
                                        $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                                        $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                                    })
                                    $('#total-com-desconto').html('<strong>R$ </strong>' + response.Carrinho.dados.total_com_desconto);
                                }
                                $('#total').css('text-decoration', 'line-through');
                            } else {
                                $('.cupom-msg').html(response.Cupom.msg[0]);
                            }
                        } catch (e) {
                        }
                    } else {
                        $('#quantidade-erro-' + gradeid).show();
                    }
                    var cep = $("#cep").val();
                    if (cep) {
                        $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
                    }
                }, "json");
    });
    
    $("#btn-alterar-quantidade").click(function(){
        var gradeid = $(this).parents('.td2').find('.quantidade').attr('rel');
        var quantidade = $(this).parents('.td2').find('.quantidade').val();
        var data = new Date();

        $('div.fretes-disponiveis').empty();
        $('.cupom-lbl').empty();
        $('.produto-totalizador,#total,#frete').attr('style', null);
        $('.cupom-msg').empty();

        $.post(PATH.basename + "/carrinho/carrinho/quantidade/" + gradeid + "/" + quantidade + "/" + data.getTime(), {},
            function(response) {
                if (response.total) {
                    $('#produto-total-' + gradeid).html(response.produto_total);
                    $('#frete').html(response.frete);
                    $('#total').html(response.total);
                    $('#sub_total').html(response.sub_total);
                    $('#quantidade-erro-' + gradeid).hide();
                    try {
                        if (response.Cupom.status == true) {
                            if (response.Cupom.tipo == "FRETE") {
                                $('#frete-com-desconto').html('R$' + response.Carrinho.dados.frete_com_desconto);
                                $('#total-com-desconto').html('<strong>R$ </strong>' + response.Carrinho.dados.total_com_desconto);
                                $('#frete').css('text-decoration', 'line-through');
                            } else {
                                $.each(response.Carrinho.itens, function(i, v) {
                                    $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                                    $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                                })
                                $('#total-com-desconto').html('<strong>R$ </strong>' + response.Carrinho.dados.total_com_desconto);
                            }
                            $('#total').css('text-decoration', 'line-through');
                        } else {
                            $('.cupom-msg').html(response.Cupom.msg[0]);
                        }
                    } catch (e) {
                    }
                } else {
                    $('#quantidade-erro-' + gradeid).show();
                }
                var cep = $("#cep").val();
                if (cep) {
                    $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
                }
            }, "json");
    });

    $('#consultar-frete').click(function() {
        var cep = $("#cep").val();
        if (cep) {
            $('.fretes-disponiveis').load(PATH.basename + "/carrinho/carrinho/get_frete/" + cep);
        } else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
        return false;
    });

    $('.fretes-disponiveis .tipo-entrega').on('click', function() {
        var cep = $("#cep").val();
        if (cep) {
            $.post(PATH.basename + "/carrinho/carrinho/set_frete/", {tipo_entrega: $('.tipo-entrega:checked').val(), cep: cep}, function(response) {
                if (response.dados) {
                    $('#frete').html(response.dados.frete);
                    $('#total').html(response.dados.total);
                    $('#sub_total').html(response.dados.sub_total);
                }
                try {
                    if (response.Cupom.status == true) {

                        $('.cupom-lbl').html(null);
                        $('.produto-totalizador,#total').attr('style', null);
                        $('.cupom-msg').html(response.Cupom.msg);
                        if (response.Cupom.tipo == "FRETE") {
                            $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                            $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                            $('#frete').css('text-decoration', 'line-through');
                        } else {
                            $.each(response.Carrinho.itens, function(i, v) {
                                $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                                $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                            })
                            $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                        }
                        $('#total').css('text-decoration', 'line-through').css('color','#999');
                    } else {
                        $('.cupom-msg').html(response.Cupom.msg[0]);
                    }
                } catch (e) {
                }

            }, "json");
        } else {
            $('.fretes-disponiveis-msg').html('Preencha o CEP');
        }
    });
    if ($('#cupom').length > 0 && $('#cupom').val().length > 0) {
        var cupom = $('#cupom').val();
        $.post(PATH.basename + "/carrinho/carrinho/set_cupom", {cupom: cupom}, function(response) {
            $('.cupom-lbl').html(null);
            $('.produto-totalizador,#total').attr('style', null);
            if (response.Cupom.status == true) {
                $('.cupom-msg').html(response.Cupom.msg);
                if (response.Cupom.tipo == "FRETE") {
                    $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                    $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                    $('#frete').css('text-decoration', 'line-through').css('color', '#999');
                } else {
                    $.each(response.Carrinho.itens, function(i, v) {
                        $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                        $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                    })
                    $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                }
                $('#total').css('text-decoration', 'line-through').css('color', '#999');
                if (response.Carrinho.dados) {
                    $('#frete').html(response.Carrinho.dados.frete);
                    $('#totais-total').html(response.Carrinho.dados.total);
                    $('#totais-subtotal').html(response.Carrinho.dados.sub_total);
                }
            } else {
                $('.cupom-msg').html(response.Cupom.msg[0]);
            }
        }, "json");
        return false;
    }

    $('#calcular-cupom').on('click', 'load', function() {
        var $this = $('#cupom');
        if ($this.val().length > 0) {
            var cupom = $this.val();
            $.post(PATH.basename + "/carrinho/carrinho/set_cupom", {cupom: cupom}, function(response) {
                $('.cupom-lbl').html(null);
                $('.produto-totalizador,#total').attr('style', null);
                if (response.Cupom.status == true) {
                    $('.cupom-msg').html(response.Cupom.msg);
                    if (response.Cupom.tipo == "FRETE") {
                        $('#frete-com-desconto').html('R$ ' + response.Carrinho.dados.frete_com_desconto);
                        $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                        $('#frete').css('text-decoration', 'line-through').css('color', '#999');
                    } else {
                        $.each(response.Carrinho.itens, function(i, v) {
                            $('#produto-total-desconto-' + v.id).html('R$ ' + v.preco_com_desconto);
                            $('#produto-total-' + v.id).css('text-decoration', 'line-through');
                        });
                        $('#total-com-desconto').html('<strong>R$ </strong> ' + response.Carrinho.dados.total_com_desconto);
                    }
                    $('#total').css('text-decoration', 'line-through').css('color', '#999');
                    if (response.Carrinho.dados) {
                        $('#frete').html(response.Carrinho.dados.frete);
                        $('#totais-total').html(response.Carrinho.dados.total);
                        $('#totais-subtotal').html(response.Carrinho.dados.sub_total);
                    }
                } else {
                    $('.cupom-msg').html(response.Cupom.msg[0]);
                }
            }, "json");
            return false;
        }
    });
});

//metodo que retorno para pagina anterior
function historyBack()
{
    window.history.back();
}
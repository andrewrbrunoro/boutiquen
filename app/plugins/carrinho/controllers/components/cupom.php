<?php

class CupomComponent extends Object {

    public function __construct() {
        App::import("component", "Session");
        $this->Session = new SessionComponent();
        App::import("model", "Cupom");
        $this->Cupom = new Cupom();
        App::import("helper", "String");
        $this->string = new StringHelper();
        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
    }

    public function getCupom() {
        return $this->Session->read("Cupom");
    }

    public function removeCupom() {
        foreach ($this->Session->read("Carrinho.itens") as $key => $item) {

            $this->Session->delete("Carrinho.itens.{$key}.preco_com_desconto");
        }
        $this->Session->delete("Carrinho.dados.frete_com_desconto");
        return $this->Session->delete("Cupom");
    }

    private function getValorTotalCarrinho($params = false) {
        App::import("model", "ProdutoCategoria");
        $this->ProdutoCategoria = new ProdutoCategoria();


        if (isset($params['Cupom']['id'])) {
            $total = 0;
            Foreach ($this->Session->read("Carrinho.itens") as $valor):
                $categorias_produtos = array();
                $cats = $this->ProdutoCategoria->find('all', array('recursive' => -1, 'conditions' => array('ProdutoCategoria.produto_id' => $valor['id'])));
                foreach ($cats as $cat) {
                    $categorias_produtos[$cat['ProdutoCategoria']['categoria_id']] = $cat['ProdutoCategoria']['categoria_id'];
                }

                $filhos = $this->ProdutoCategoria->Categoria->children($params['Cupom']['categoria_id']);
                $categorias_validas_cupom = am($params['Cupom']['categoria_id'], set::extract('/Categoria/id', $filhos));
                foreach ($categorias_validas_cupom as $categoria) {
                    if (in_array($categoria, $categorias_produtos)) {
                        $total += ((($valor['preco_promocao'] > 0) ? $valor['preco_promocao'] : $valor['preco']) * $valor['quantidade']);
                    }
                }
            endForeach;
        } else {
            $total = 0;
            Foreach ($this->Session->read("Carrinho.itens") as $valor):
                $total += ((($valor['preco_promocao'] > 0) ? $valor['preco_promocao'] : $valor['preco']) * $valor['quantidade']);
            endForeach;
        }
        return $total;
    }

    public function setCupom($cupom, $cep) {
        $this->removeCupom();
        App::import('Component', 'Carrinho.Carrinho');
        $this->Carrinho = new CarrinhoComponent();
        $this->Carrinho->setCep($cep);
        $cupom = $this->Cupom->findByCupomAndStatus($cupom,1);

        if ($cupom) {
            $this->Cupom->set($cupom);

            if ($this->Cupom->validates($cupom)) {

                switch (up($cupom['CupomTipo']['nome'])) {
                    case 'CARRINHO':

                        App::import("model", "ProdutoCategoria");
                        $this->ProdutoCategoria = new ProdutoCategoria();

                        $total = $this->getValorTotalCarrinho(false);
                        $total_categoria = $this->getValorTotalCarrinho($cupom);

                        $desconto = 0;
// Caso o valor total seja menor que o desconto
                        if ($cupom["Cupom"]['pct_desconto'] > 0) {
                            if ($cupom["Cupom"]['categoria_id'] != null) {
                            	$desconto = ($cupom["Cupom"]['pct_desconto'] / 100) * $total_categoria;	
                            } else {
                            	$desconto = ($cupom["Cupom"]['pct_desconto'] / 100) * $total;
                            }
                        } else {
                            $desconto = $cupom["Cupom"]['vlr_desconto'];
                        }

                        $produtos_validos = array();
                        $valor_desconto_sobre_produto = null;

                        foreach ($this->Session->read("Carrinho.itens") as $key => $item) {
                            $categorias_produto = array();
//se possuir filtro de categoria no cupom
                            if ($cupom['Cupom']['categoria_id']) {
                                $cats = $this->ProdutoCategoria->find('all', array('recursive' => -1, 'conditions' => array('ProdutoCategoria.produto_id' => $item['id'])));
                                foreach ($cats as $cat) {
                                    $categorias_produto[$cat['ProdutoCategoria']['categoria_id']] = $cat['ProdutoCategoria']['categoria_id'];
                                }


                                $filhos = $this->ProdutoCategoria->Categoria->children($cupom['Cupom']['categoria_id']);
                                $categorias_validas_cupom = am($cupom['Cupom']['categoria_id'], set::extract('/Categoria/id', $filhos));

                                $return = false;
                                foreach ($categorias_validas_cupom as $categoria) {
                                    if (in_array($categoria, $categorias_produto)) {
                                        if ($total_categoria == 0) {
                                            $x = 0;
                                        } else {
                                            $x = ($item['quantidade'] * round((($item['preco_promocao'] > 0) ? $item['preco_promocao'] : $item['preco']), 2) / round($total_categoria, 2));
                                        }
                                        $valor_produto = ($item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco']);
                                        $valor_desconto_sobre_produto = (($desconto * $x) / $item['quantidade']);
                                        if ($valor_produto - $valor_desconto_sobre_produto < 0) {
                                            $valor_produto_com_desconto = (double) 0.00;
                                        } else {
                                            $valor_produto_com_desconto = ($valor_produto - $valor_desconto_sobre_produto) * $item['quantidade'];
                                        }

                                        $produtos_validos[] = $item['grade_id'];
                                        $this->Session->write("Carrinho.itens.{$item['grade_id']}.preco_com_desconto", $valor_produto_com_desconto);
                                    }
                                }
                            } else {
//se nao possuir filtro de categoria no cupom
                                if ($total == 0) {
                                    $x = 0;
                                } else {
                                    $x = ($item['quantidade'] * round((($item['preco_promocao'] > 0) ? $item['preco_promocao'] : $item['preco']), 2) / round($total, 2));
                                }
                                $valor_produto = ($item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco']);
                                $valor_desconto_sobre_produto = (($desconto * $x) / $item['quantidade']);
                                if ($valor_produto - $valor_desconto_sobre_produto < 0) {
                                    $valor_produto_com_desconto = (double) 0.00;
                                } else {
                                    $valor_produto_com_desconto = ($valor_produto - $valor_desconto_sobre_produto) * $item['quantidade'];
                                }
                                $produtos_validos[] = $item['grade_id'];
                                $this->Session->write("Carrinho.itens.{$item['grade_id']}.preco_com_desconto", $valor_produto_com_desconto);
                            }
                        }

                        $retorno["Cupom"]["tipo"] = "CARRINHO";
                        $retorno["Cupom"]["msg"] = $this->setMensagemCupom($cupom);
                        $retorno["Cupom"]["cupom"] = $cupom['Cupom']['cupom'];
                        $retorno["Cupom"]["id"] = $cupom['Cupom']['id'];
                        $this->Session->write("Cupom.dados", $cupom["Cupom"]);
//se o valor total for negativo seta o valor do frete
                        if ($total - $desconto < 0) {
                            $total_com_desconto = $this->Session->read("Carrinho.dados.frete");
                            $sub_total_com_desconto = $this->Session->read("Carrinho.dados.frete");
                            $desconto = $this->Session->read("Carrinho.dados.frete");
                        } else {
                            $total_com_desconto = ($total - $desconto) + $this->Session->read("Carrinho.dados.frete");
                            $sub_total_com_desconto = ($total - $desconto);
                        }
                        $this->Session->write("Cupom.total_desconto", $desconto);
                        $this->Session->write("Carrinho.dados.total_com_desconto", $total_com_desconto);
                        $this->Session->write("Carrinho.dados.sub_total_com_desconto", $sub_total_com_desconto);
                        $this->Session->write("Carrinho.dados.cupom_total_desconto", $desconto);
                        foreach ($this->Session->read("Carrinho.itens") as $item):
                            if (in_array($item['grade_id'], $produtos_validos)) {
                                $retorno["Carrinho"]["itens"][] = array('id' => $item['grade_id'], 'preco_com_desconto' => $this->string->bcotoMoeda($item['preco_com_desconto']));
                            }
                        endForeach;
                        break;

                    case 'FRETE':
                        $frete_gratis = false;
                        $total = $this->getValorTotalCarrinho();

                        $valor_frete = $this->Session->read("Carrinho.dados.frete");
                        if ($cupom["Cupom"]['pct_desconto'] > 0) { //desconto dado por porcentagem
                            $valor_desconto = $valor_frete * ($cupom["Cupom"]['pct_desconto'] / 100);
                        } else {
                            $valor_desconto = $cupom["Cupom"]['vlr_desconto'];
                        }
//se o valor do desconto for maior que o valor do frete
                        if (($valor_frete - $valor_desconto) < 0) {
                            $frete_gratis = true;
                        }
                        $retorno["Cupom"]["tipo"] = "FRETE";
                        $retorno["Cupom"]["msg"] = $this->setMensagemCupom($cupom);
                        $retorno["Cupom"]["cupom"] = $cupom['Cupom']['cupom'];
                        $retorno["Cupom"]["id"] = $cupom['Cupom']['id'];
                        $this->Session->write("Cupom.dados", $cupom["Cupom"]);
                        if ($frete_gratis == false) {
                            $this->Session->write("Carrinho.dados.sub_total_com_desconto", $total);
                            $this->Session->write("Carrinho.dados.total_com_desconto", $total + ($valor_frete - $valor_desconto));
                            $this->Session->write("Carrinho.dados.frete_com_desconto", $valor_frete - $valor_desconto);
                            $this->Session->write("Cupom.total_desconto", $valor_desconto);
                            $this->Session->write("Carrinho.dados.cupom_total_desconto", $valor_desconto);
                        } else {
                            $this->Session->write("Carrinho.dados.sub_total_com_desconto", $total);
                            $this->Session->write("Carrinho.dados.total_com_desconto", $total);
                            $this->Session->write("Carrinho.dados.frete_com_desconto", (double) 0.00);
                            $this->Session->write("Carrinho.dados.cupom_total_desconto", $valor_desconto);
                            $this->Session->write("Cupom.total_desconto", $valor_frete);
                        }
                        break;

                    case 'PRODUTO':
                        $total = 0;
                        $total_carrinho = $this->getValorTotalCarrinho(false);
                        $desconto = 0;


                        $valor_desconto_sobre_produto = null;
                        foreach ($this->Session->read("Carrinho.itens") as $key => $item) {
                            
                            if ($cupom["Cupom"]['grade_id'] != $item['grade_id'])
                                continue;
                            
                            $total = (($item['preco_promocao'] > 0) ? $item['preco_promocao'] : $item['preco']) * $item['quantidade'];
                            
                            // Caso o valor total seja menor que o desconto
                            if ($cupom["Cupom"]['pct_desconto'] > 0) {
                                $desconto = ($cupom["Cupom"]['pct_desconto'] / 100) * $total;
                            } else {
                                $desconto = $cupom["Cupom"]['vlr_desconto'];
                            }                           
                            
                            if ($total == 0) {
                                    $x = 0;
                            }else{
                                if ($cupom["Cupom"]['pct_desconto'] > 0) {
                                    $x = ($item['quantidade'] * round((($item['preco_promocao'] > 0) ? $item['preco_promocao'] : $item['preco']), 2) / round($total, 2));
                                }else{
                                    $x = 1;
                                }
                           }
                            
                            $valor_produto = ($item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco']);                            
                            $valor_desconto_sobre_produto = (($desconto * $x) / $item['quantidade']);
                            if ($valor_produto - $valor_desconto_sobre_produto < 0) {
                                $valor_produto_com_desconto = (double) 0.00;
                            } else {
                                if ($cupom["Cupom"]['pct_desconto'] > 0) {
                                    $valor_produto_com_desconto = ($valor_produto - $valor_desconto_sobre_produto) * $item['quantidade'];
                                }else{
                                    $valor_produto_com_desconto = ($valor_produto - $valor_desconto_sobre_produto);
                                }                                
                            }

                            $this->Session->write("Carrinho.itens.{$item['grade_id']}.preco_com_desconto", $valor_produto_com_desconto);
                        }

                        $retorno["Cupom"]["tipo"] = "PRODUTO";
                        $retorno["Cupom"]["msg"] = $this->setMensagemCupom($cupom);
                        $retorno["Cupom"]["cupom"] = $cupom['Cupom']['cupom'];
                        $retorno["Cupom"]["id"] = $cupom['Cupom']['id'];
                        $this->Session->write("Cupom.dados", $cupom["Cupom"]);

						//se o valor total for negativo seta o valor do frete
                        if ($total_carrinho - $desconto < 0) {
                            $total_com_desconto = $this->Session->read("Carrinho.dados.frete");
                            $sub_total_com_desconto = $this->Session->read("Carrinho.dados.frete");
                            $desconto = $this->Session->read("Carrinho.dados.frete");
                        } else {
                            $total_com_desconto = ($total_carrinho - $desconto) + $this->Session->read("Carrinho.dados.frete");
                            $sub_total_com_desconto = ($total_carrinho - $desconto);
                        }
                        
                        $this->Session->write("Cupom.total_desconto", $desconto);
                        $this->Session->write("Carrinho.dados.total_com_desconto", $total_com_desconto);
                        $this->Session->write("Carrinho.dados.sub_total_com_desconto", $sub_total_com_desconto);
                        $this->Session->write("Carrinho.dados.cupom_total_desconto", $desconto);
                        foreach ($this->Session->read("Carrinho.itens") as $key => $item):
                            if ($cupom["Cupom"]['grade_id'] != $item['grade_id'])
                                continue;
                            $retorno["Carrinho"]["itens"][] = array('id' => $item['grade_id'], 'preco_com_desconto' => $this->string->bcotoMoeda($item['preco_com_desconto']));
                        endForeach;
						
					/* end tipo produto */	
					break;
                }
                $retorno["Cupom"]["status"] = true;
                /**
                 * retorna os valores para exibição
                 * * */
                $dados = $this->Session->read("Carrinho.dados");
                $totais['frete'] = $this->string->bcotoMoeda($dados['frete']);
                $totais['total'] = $this->string->bcotoMoeda($dados['total']);
                $totais['sub_total'] = $this->string->bcotoMoeda($dados['subtotal']);
                $totais['prazo'] = $this->Session->read("Carrinho.dados.prazo_entrega");
                $retorno["Cupom"]["cupom"] = $this->Session->read("Cupom.dados.cupom");

				//se for tipo frete
                if ($retorno["Cupom"]["tipo"] == 'FRETE' && isset($dados['frete_com_desconto'])) {
                    if ($dados['frete_com_desconto'] <= 0) {
                        $totais['frete_com_desconto'] = $this->string->bcotoMoeda($dados['frete_com_desconto']);
                    } else {
                        $totais['frete_com_desconto'] = $this->string->bcotoMoeda($dados['frete_com_desconto']);
                    }
                }

                $totais['total_com_desconto'] = $this->string->bcotoMoeda($dados['total_com_desconto']);
                $totais['sub_total_com_desconto'] = $this->string->bcotoMoeda($dados['sub_total_com_desconto']);
                $totais['cupom_total_desconto'] = $this->string->bcotoMoeda($this->Session->read("Cupom.total_desconto"));
                $retorno["Carrinho"]["dados"] = $totais;

                return $retorno;
            } else {
                $retorno["Cupom"]["status"] = false;
                foreach ($this->Cupom->invalidFields() as $chave => $valor) {
                    if ($valor) {
                        $retorno["Cupom"]["msg"][] = htmlentities($valor, null, 'utf-8');
                    }
                }
                return $retorno;
            }
        } else {
            $retorno["Cupom"]["status"] = false;
            $retorno["Cupom"]["msg"][] = htmlentities("Cupom inválido", null, 'utf-8');
            return $retorno;
        }
    }

    private function setMensagemCupom($cupom) {
//verifica se existe ainda o cupom setado
        if ($cupom["Cupom"] && $this->Cupom->findByCupomAndStatus($cupom["Cupom"]["cupom"], 1)) {
            if (up($cupom['CupomTipo']['nome']) == 'CARRINHO') {
                if ($cupom["Cupom"]["pct_desconto"] > 0) {
                    $retorno = $this->string->decimalFormat("human", $cupom["Cupom"]["pct_desconto"]) . '% de desconto nos produtos do carrinho.<a href="' . $this->Html->url("/") . 'carrinho/remove_cupom" id="clear-cupom"> Clique aqui para remover este cupom</a> <br/> (Obs.: O valor descontado é distribuído entre os itens por questões fiscais)';
                } else {
                    $retorno = "R$ " . $this->string->decimalFormat("human", $cupom["Cupom"]["vlr_desconto"]) . ' de desconto nos produtos do carrinho.<a href="' . $this->Html->url("/") . 'carrinho/remove_cupom" id="clear-cupom"> Clique aqui para remover este cupom</a> <br/> (Obs.:O valor descontado é distribuído entre os itens por questões fiscais)';
                }
            } elseif (up($cupom['CupomTipo']['nome']) == 'FRETE') {
                if ($cupom["Cupom"]["pct_desconto"] > 0) {
                    $retorno = $this->string->decimalFormat("human", $cupom["Cupom"]["pct_desconto"]) . '% de desconto no valor do frete.<a href="' . $this->Html->url("/") . 'carrinho/remove_cupom" id="clear-cupom"> Clique aqui para remover este cupom</a>';
                } else {
                    $retorno = "R$ " . $this->string->decimalFormat("human", $cupom["Cupom"]["vlr_desconto"]) . ' de desconto no valor do frete.<a href="' . $this->Html->url("/") . 'carrinho/remove_cupom" id="clear-cupom"> Clique aqui para remover este cupom</a>';
                }
            } elseif (up($cupom['CupomTipo']['nome']) == 'PRODUTO') {
                if ($cupom["Cupom"]["pct_desconto"] > 0) {
                    $retorno = $this->string->decimalFormat("human", $cupom["Cupom"]["pct_desconto"]) . '% de desconto para um produto.<a href="' . $this->Html->url("/") . 'carrinho/remove_cupom" id="clear-cupom"> Clique aqui para remover este cupom</a>';
                } else {
                    $retorno = "R$ " . $this->string->decimalFormat("human", $cupom["Cupom"]["vlr_desconto"]) . ' de desconto para um produto.<a href="' . $this->Html->url("/") . 'carrinho/remove_cupom" id="clear-cupom"> Clique aqui para remover este cupom</a>';
                }
            }
            return $retorno;
        }
    }

}
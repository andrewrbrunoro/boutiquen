<?php

    class CarrinhoComponent extends Object
    {


        /**
         */
        function __construct()
        {
            App::import("component", "Session");
            $this->Session = new SessionComponent();

            App::import("component", "RequestHandler");
            $this->RequestHandler = new RequestHandlerComponent();
            App::import("component", "Frete");
            $this->Frete = new FreteComponent();

            //App::import("component", "CarrinhosAbandonados");
            //$this->CarrinhosAbandonados = new CarrinhosAbandonadosComponent();

            App::import("helper", "Html");
            $this->Html = new HtmlHelper();

            App::import("model", "Produto");
            $this->Produto = new Produto();

            App::import("model", "Grade");
            $this->Grade = new Grade();
        }

        function initialize(&$Controller)
        {
            $this->Controller = $Controller;
        }

        private function loadModel($model)
        {
            App::import('Model', $model);
            $this->$model = new $model;
        }

        public function Adicionar($produto_id, $grade_id, $quantidade)
        {
            $items = $this->Session->read('Carrinho.itens');

            if ($this->existeItemNoCarrinho($grade_id))
                $quantidade = $items[$grade_id]['Produto']['quantidade'] + $quantidade;

            $this->loadModel('VariacaoProduto');
            $variacoes_produto = $this->VariacaoProduto->find('all',
                array(
                    'fields'     => array(
                        'Variacao.valor'
                    ),
                    'conditions' => array(
                        'VariacaoProduto.produto_id' => $produto_id,
                        'VariacaoProduto.grade_id'   => $grade_id
                    )
                )
            );

            /**
             *
             * fazer um finde na grade
             */
            if ($this->verificaQuantidade($grade_id, $quantidade) == true && $variacoes_produto) {
                $produto = $this->Produto->find('first',
                    array(
                        'recursive'  => -1,
                        'fields'     => array(
                            'Produto.id', 'Produto.nome', 'Produto.descricao', 'Produto.tag',
                            '(SELECT concat("uploads/produto_imagem/filename/",GradeImagem.filename) FROM grade_imagens GradeImagem WHERE GradeImagem.grade_id = "' . $grade_id . '" ORDER BY GradeImagem.ordem ASC LIMIT 1) as GradeImagem'
                        ),
                        'conditions' => array(
                            'Produto.id' => $produto_id
                        )
                    )
                );

                $gradeFields = array(
                    'Grade.id', 'Grade.sku', 'Grade.preco', 'Grade.preco_promocao', 'Grade.quantidade',
                    'Grade.quantidade_disponivel', 'Grade.peso', 'Grade.largura', 'Grade.profundidade', 'Grade.altura',
                    'Grade.status'
                );
                $grade = $this->Grade->find('first', array('recursive' => -1, 'fields' => $gradeFields, 'conditions' => array('Grade.id' => $grade_id)));

                if (isset($grade['Grade']) && count($grade['Grade'])) {
                    $produto['Produto'] = array(
                        'quantidade'     => $quantidade,
                        'grade_id'       => $grade_id,
                        'grade'          => $grade['Grade'],
                        'nome'           => $produto['Produto']['nome'],
                        'sku'            => $grade['Grade']['sku'],
                        'preco'          => $grade['Grade']['preco'],
                        'preco_promocao' => $grade['Grade']['preco_promocao'],
                        'peso'           => $grade['Grade']['peso'],
                        'largura'        => $grade['Grade']['largura'],
                        'altura'         => $grade['Grade']['altura'],
                        'imagem'         => $produto[0]['GradeImagem'],
                        'url'            => $this->Html->Url('/' . low(Inflector::slug($produto['Produto']['nome'], '-')) . '-prod-' . $grade_id . '.html', true)
                    );

                    /*BoutiqueNÔ*/
                    if (Configure::read('Loja.desconto_geral') != "") {
                        $produto['Produto']['preco_promocao'] = $produto['grade']['preco'] - ($produto['grade']['preco'] * (int)Configure::read('Loja.desconto_geral')) / 100;
                    }
                    unset($produto['Grade'], $produto[0]);
                    $items[$grade_id] = $produto;
                    $this->Session->write('Carrinho.itens', $items);
                    return true;
                } else {
                    $this->Session->setFlash('Ops, não foi possível salvar o produto no seu carrinho, tente novamente.', 'flash/error');
                    return false;
                }
            } else {
                $this->Session->setFlash('Quantidade indisponível para este produto. Cadastre seu e-mail no formulário de avise-me.', 'flash/error');
                return false;
            }

        }


        public function Remover($grade_id)
        {

            //verifica se a requisição é AJAX
            if ($this->RequestHandler->isAjax()) {
                $grade_id = $this->params['form']['grade_id'];
            }

            App::import('Model', 'CarrinhoAbandonado');
            $this->CarrinhoAbandonado = new CarrinhoAbandonado();
            $this->CarrinhoAbandonado->remover($grade_id, $this->Session->read("Carrinho.carrinho_abandonado"));
            $this->Session->delete("Carrinho.itens.{$grade_id}");
            return !$this->Session->check("Carrinho.itens.{$grade_id}");
        }

        public function RemoverAjax($grade_id)
        {

            App::import('Model', 'CarrinhoAbandonado');
            $this->CarrinhoAbandonado = new CarrinhoAbandonado();
            $this->CarrinhoAbandonado->remover($grade_id, $this->Session->read("Carrinho.carrinho_abandonado"));
            $this->Session->delete("Carrinho.itens.{$grade_id}");
            return !$this->Session->check("Carrinho.itens.{$grade_id}");
        }

        /**
         * Retira todos os itens do carrinho
         * e refaz os dados
         *
         * @author Luan Garcia
         */
        public function Esvaziar()
        {
            $this->Session->write("Carrinho.dados", array(
                "total"                  => 0,
                "subtotal"               => 0,
                "frete"                  => 0,
                "cupom_total_desconto"   => 0,
                "sub_total_com_desconto" => 0,
                "total_com_desconto"     => 0,
                "frete_com_desconto"     => 0,
                "prazo_entrega"          => 0,
                "tipo_entrega"           => null,
                "cep"                    => null,
                "endereco_entrega"       => null,
                "step"                   => null
            ));
            $this->Session->write("Carrinho.itens", array());
            $this->Session->write("Carrinho.pedido_id", null);
            $this->Session->write("Carrinho.carrinho_abandonado", null);
            $this->Session->write("Pedido", array());
        }

        /**
         * @param int $grade_id Verifica se existe um item no carrinho
         * @return boolean Existe?
         */
        public function existeItemNoCarrinho($grade_id)
        {
            $itens = $this->Session->read("Carrinho.itens");
            if (isset($itens) && is_array($itens)) {
                if (array_key_exists($grade_id, $itens)) {
                    return true;
                }
            }
            return false;
        }

        /**
         * Retorna o valor total
         * @return <Array> array com os valores do pedido e frete
         */
        function getCarrinhoTotal($return = false)
        {

            $total = 0;
            $frete = $this->Session->read("Carrinho.dados.frete");
            $sub_total = 0;
            foreach ($this->getItens() as $produto):
                $valor = $produto['Produto']['preco_promocao'] > 0 ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
                $sub_total += $valor * $produto['Produto']['quantidade'];
            endForeach;
            $dados['itens'] = $this->getItens();
            $dados['total'] = ($sub_total + $frete);
            $dados['sub_total'] = $sub_total;
            return $return ? $dados[$return] : $dados;
        }

        /**
         * Troca a quantidade atual, pela quantidade
         * fornecida do item com o id passado no parametro 1
         *
         * @param int $grade_id
         * @param int $quantidade
         * @return boolean Alterou?
         */
        public function Quantidade($grade_id, $quantidade)
        {
            App::import('Helper', 'String');
            $this->String = new StringHelper();
            if (!$this->Session->read("Carrinho.itens.{$grade_id}") || $quantidade <= 0)
                return false;
            if ($this->verificaQuantidade($grade_id, $quantidade)) {
                $this->Session->write("Carrinho.itens.{$grade_id}.Produto.quantidade", $quantidade);
                if ($quantidade == $this->Session->read("Carrinho.itens.{$grade_id}.Produto.quantidade")) {
                    $retorno = $this->recalcularCarrinho();
                    $this->Session->write('Carrinho.dados.total', $retorno);
                    /*$produto_id = $this->Session->read("Carrinho.itens.{$grade_id}.Produto.id");*/
                    /*$this->setCarrinhoAbandonado($produto_id, $grade_id, $quantidade, $preco);*/
                    return true;
                }
            }
            return false;
        }

        /**
         * Retorna um item que esta no carrinho
         *
         * @param Int $produto_id
         * @return array
         */
        public function getItem($grade_id)
        {
            return $this->Session->read("Carrinho.itens.{$grade_id}");
        }

        /**
         * Retorna os itens que estão no carrinho
         *
         * @return array
         */
        public function getItens()
        {
            return $this->Session->read("Carrinho.itens");
        }

        /**
         * Retorna o carrinho de compras do cliente
         *
         * @return array
         */
        public function getCarrinho()
        {
            return $this->Session->read("Carrinho");
        }

        /**
         * Retorna o valor total de um item
         * @param Int $produto_id
         * @return array
         */
        public function getTotalItem($grade_id)
        {
            $item = $this->getItem($grade_id);

            $valor = empty($item['preco_promocao']) ? $item['preco'] : $item['preco_promocao'];
            $valor_total = $item['quantidade'] * $valor;
            return $valor_total;
        }

        /**
         * Verifica se a quantidade pretentida está disponivel em estoque
         * @param Int $id         produto_id
         * @param Int $quantidade quantidade pretendida
         * @return Boolean
         */
        public function verificaQuantidade($grade_id, $quantidade)
        {
            $this->Grade->recursive = -1;
            $grade = $this->Grade->findById($grade_id);
            if ($grade['Grade']['quantidade_disponivel'] > Configure::read('Loja.quantidade_minima')) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Realiza o recalculo total dos valores carrinho, utilizado após alguma alteração de quantidade ou frete
         * @return Array
         */
        public function recalcularCarrinho()
        {

            App::import("helper", "String");
            $this->String = new StringHelper();

            $total = 0;
            $frete = $this->Session->read("Carrinho.dados.frete");
            $sub_total = 0;
            /*         * ************************
             * ***
             * *** IMPORTANTE OS DESCONTOS NÃO PODEM ACUMULAR, EX: CUPOM DE DESCONTO MAIS TABELA DE PREÇO
             * ***
             * **** */
            /* adiciona o desconto por tabela de preço se não tiver cupom de desconto */

            // foreach ($this->getItens() as $produto):
            // $this->calculaTabelaPreco($produto['grade_id'], $produto['quantidade']);
            // endForeach;

            foreach ($this->getItens() as $produto):
                $valor = $produto['Produto']['preco_promocao'] > 0 ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
                $sub_total += $valor * $produto['Produto']['quantidade'];
            endForeach;

            /*Frete adicionado manualmente*/
            $totais['frete'] = ($sub_total > 200) ? '0,00' : $this->String->bcoToMoeda($frete);
            $totais['total'] = $this->String->bcotoMoeda($sub_total + $frete);
            $totais['sub_total'] = $this->String->bcotoMoeda($sub_total);

            $this->Session->write("Carrinho.dados.total", $sub_total + $frete);
            $this->Session->write("Carrinho.dados.subtotal", $sub_total);
            $this->Session->write("Carrinho.dados.frete", $frete);

            return $totais;
        }

        /*     * *
         * recalcula a tabela de preço e sobreescre o carrinho
         * */

        public function calculaTabelaPreco($produto_id, $quantidade_pretendida)
        {

            $produto = $this->Produto->find("first", array(
                "fields"     => "Produto.id as produto_id, Produto.sku, Produto.referencia, Produto.codigo, Produto.codigo_externo, Produto.nome, Produto.descricao,Produto.custo, Produto.preco, Produto.preco_promocao, Produto.quantidade, Produto.peso, Produto.peso_ramos, Produto.garantia, Produto.largura, Produto.altura, Produto.profundidade, Produto.fabricante_id, Produto.observacao, Produto.unidade_venda, Produto.quantidade_disponivel, Produto.quantidade_alocada, Produto.preco_promocao_inicio, Produto.preco_promocao_fim, Produto.preco_promocao_novo, Produto.preco_promocao_velho, Produto.tempo_producao, Produto.marca, Produto.correios, Produto.portoalegre, Produto.debitar_estoque,
			(SELECT concat('uploads/produto_imagem/filename/',ProdutoImagem.filename) FROM produto_imagens ProdutoImagem WHERE ProdutoImagem.produto_id = " . $produto_id . " LIMIT 1) as imagem",
                "contain"    => array(
                    "Categoria",
                    "VariacaoProduto" => array(
                        "Grade"    => array("GradeImagem"),
                        "Variacao" => array("VariacaoTipo")
                    ),
                    "ProdutoPreco"    => array("conditions" => array("ProdutoPreco.status" => true)),
                    "AtributoProduto" => array("Atributo" => array("AtributoTipo"))
                ),
                "conditions" => array("Produto.id" => $produto_id)
            ));

            $itens = $this->Session->read("Carrinho.itens");
            if ($produto['ProdutoPreco']) {
                $tabela_menor = null;
                $first = true;

                foreach ($produto['ProdutoPreco'] as $tabela) {

                    if (($quantidade_pretendida >= $tabela['quantidade'] && ($tabela['preco'] < $tabela_menor['preco'] || $first == true))) {
                        $first = false;
                        $tabela_menor = $tabela;
                    }
                }
                //se tiver o menor preco relativo a parcela passada
                if ($tabela_menor) {
                    $produto['Produto']['preco'] = $tabela_menor['preco'];
                    $produto['Produto']['preco_promocao'] = 0;
                }
            }
            $produto_old = $itens[$produto_id];
            $produto['Produto']['quantidade'] = $produto_old['quantidade'];

            if (isset($produto_old['grade_id']) && $produto_old['grade_id'] > 0) {
                $grade_id = $produto_old['grade_id'];
                if ($grade_id != null) {
                    $produto['Produto']['grade_id'] = $grade_id;
                    $nomes_variacoes = '';
                    foreach ($produto['VariacaoProduto'] as $key => $variacoes) {
                        if ($variacoes['grade_id'] == $grade_id) {

                            $nomes_variacoes .= ' - ' . $produto['VariacaoProduto'][$key]['Variacao']['valor'];
                            $produto[0]['imagem'] = "uploads/produto_imagem/filename/" . $produto['VariacaoProduto'][$key]['Grade']['GradeImagem'][0]['filename'];
                            $produto['Produto']['preco'] = $produto['VariacaoProduto'][$key]['Grade']['preco'];
                            $produto['Produto']['preco_promocao'] = $produto['VariacaoProduto'][$key]['Grade']['preco_promocao'];
                        }
                    }
                    $produto['Produto']['nome'] .= $nomes_variacoes;
                }
            }

            $item = $produto['Produto'];
            $item['imagem'] = $produto[0]['imagem'];
            //$item['agrupador'] = $produto[0]['agrupador'];

            if (isset($produto['ProdutoPreco'][0])) {
                $item['ProdutoPreco'] = $produto['ProdutoPreco'][0];
            }

            foreach ($produto['AtributoProduto'] as $atributos) {
                if ($atributos['Atributo']['AtributoTipo']['nome'] != "") {
                    $item['ProdutoAtributos'][$atributos['Atributo']['AtributoTipo']['nome']] = $atributos['Atributo']['valor'];
                }
            }

            foreach ($produto['VariacaoProduto'] as $variacoes) {
                if ($variacoes['Variacao']['valor']) {
                    $item['VariacaoProduto'][$variacoes['Variacao']['VariacaoTipo']['nome']] = $variacoes['Variacao']['valor'];
                }
            }

            foreach ($produto['Categoria'] as $categoria) {
                $item['Categoria'][$categoria['id']] = $categoria['seo_url'];
            }
            $itens[$produto_id] = $item;
            //adiciona item na sessao os itens

            $this->Session->write("Carrinho.itens", $itens);
        }

        /**
         * Consulta o prazo e valor de entrega e salva na sessão.
         * @param String CEP $cep de destino
         * @param String TIPO  $tipo tipo de entrega
         */
        public function setFrete($cep, $tipo)
        {
            App::import("helper", "String");
            $this->String = new StringHelper();
            $cep = preg_replace('/[^0-9]/', '', $cep);
            //pega os fretes consultados na sessão;
            $frete = set::extract("/Frete[id={$tipo}]", $this->Session->read("Carrinho.fretes"));

            if ($frete) {
                $prazo = $frete[0]['Frete']['prazo'];
                $this->Session->write("Carrinho.dados.frete_antigo", isset($frete[0]['Frete']['preco_antigo']) ? $frete[0]['Frete']['preco_antigo'] : 0);
                $this->Session->write("Carrinho.dados.frete", $frete[0]['Frete']['preco']);
                $this->Session->write("Carrinho.dados.prazo_entrega", $prazo);
                $this->Session->write("Carrinho.dados.tipo_entrega", $frete[0]['Frete']['id']);
                $this->Session->write("Carrinho.dados.cep", $cep);
                $retorno['dados'] = $this->recalcularCarrinho(false);
                return $retorno;
            }

            return false;
        }

        /**
         * Limpar frete
         */
        public function limparFrete()
        {
            $this->Session->write("Carrinho.dados.tipo_entrega", null);
            $this->Session->write("Carrinho.dados.prazo_entrega", 0);
            $this->Session->write("Carrinho.dados.frete", 0);
        }

        /**
         * Consulta os meios de entrega disponiveis para o cep e peso passado.
         * @param String CEP $cep de destino
         */
        public function consultarFretesDisponiveis($cep, $produto = null)
        {
            $cep = preg_replace('/[^0-9]/', '', $cep);
            if ($produto == null)
                $fretes = $this->Frete->consultarFretesDisponiveis($cep, $this->getCarrinhoTotal()); else
                $fretes = $this->Frete->consultarFretesDisponiveis($cep, $produto);
            $this->Session->write("Carrinho.fretes", $fretes);
            return $fretes;
        }

        public function consultaCep($cep)
        {
            $cep = preg_replace('/[^0-9]/', '', $cep);
            return $this->Frete->consultaEndereco($cep);
        }

        public function getCep()
        {
            return $this->Session->read("Carrinho.dados.cep");
        }

        public function getPrazo()
        {
            return $this->Session->read("Carrinho.dados.prazo_entrega");
        }

        /**
         * Seta o cep a ser utilizado para entrega
         * @param String $cep
         * @return Boolean
         */
        public function setCep($cep)
        {
            return $this->Session->write("Carrinho.dados.cep", $cep);
        }

        /**
         * Seta o id do pedido finalizado para busca dos dados na finalizacao do pedido
         * @param Int $id id do pedido
         * @return Boolean
         */
        public function setPedido($id)
        {
            return $this->Session->write("Carrinho.pedido_id", $id);
        }

        /**
         * Retorna o id do pedido finalizado para busca dos dados na finalizacao do pedido
         * @return Int
         */
        public function getPedido()
        {
            return $this->Session->read("Carrinho.pedido_id");
        }

        public function getFrete()
        {
            return $this->Session->read("Carrinho.dados.tipo_entrega");
        }

        public function getStep()
        {
            return $this->Session->read("Carrinho.dados.step");
        }

        /**
         * Seta o passo que o usuário está no fluxo da compra.
         * @return Int passo atual do usuário usado no fluxo da compra.
         */
        public function setStep($step)
        {
            return $this->Session->write("Carrinho.dados.step", $step);
        }

        /**
         * Seta o passo que o usuário está no fluxo da compra.
         */
        public function setEnderecoEntrega($id)
        {
            return $this->Session->write("Carrinho.dados.endereco_entrega", $id);
        }

        /**
         * Retorna o endereço de entrega selecionado pelo cliente
         */
        public function getEnderecoEntrega()
        {
            return $this->Session->read("Carrinho.dados.endereco_entrega");
        }

        private function setCarrinhoAbandonado($produto_id, $grade_id, $quantidade_pretendida, $preco = null)
        {

            //CarrinhoAbandonado
            $produto['produto_id'] = $produto_id;
            $produto['grade_id'] = $grade_id;
            $produto['quantidade'] = $quantidade_pretendida;
            $produto['preco'] = $preco;

            //$valor_produto = $this->Produto->find('all',array('conditions'=>array('VariacaoProduto.produto_id'=>$id)));
            //model CarrinhoAbandonado
            App::import('Model', 'CarrinhoAbandonado');
            $this->CarrinhoAbandonado = new CarrinhoAbandonado();

            //dados banco
            if ($this->Session->read("Carrinho.carrinho_abandonado") > 0) {
                $produtos_carrinho = $this->CarrinhoAbandonado->find('first', array(
                    'recursive'  => -1,
                    'fields'     => 'CarrinhoAbandonado.produtos',
                    'conditions' => array('CarrinhoAbandonado.id' => $this->Session->read("Carrinho.carrinho_abandonado"))
                ));

                $produtos = array();
                if ($produtos_carrinho['CarrinhoAbandonado']['produtos'] != null) {
                    $produtos = json_decode($produtos_carrinho['CarrinhoAbandonado']['produtos'], true);
                }

                $data['produtos'][] = $produto;

                foreach ($produtos as $prod) {
                    if ($prod['grade_id'] != $produto['grade_id']) {
                        $data['produtos'][] = $prod;
                    }
                }
            } else {
                //dados produtos
                $data['produtos'][] = $produto;
            }

            //dados usuarios
            $u = $this->Controller->Auth->User();
            $data['usuario_id'] = $u['Usuario']['id'];
            //atualizo os registros do banco, cada vez que adiciono produto no carrinho
            if ($this->Session->read("Carrinho.carrinho_abandonado") > 0) {
                $this->CarrinhoAbandonado->salvar($data, $this->Session->read("Carrinho.carrinho_abandonado"));
            } else {
                $this->CarrinhoAbandonado->salvar($data);
            }
            //debug($data);  die;
        }

        public function getCarrinhoAbandonado()
        {

            //model CarrinhoAbandonado
            App::import('Model', 'CarrinhoAbandonado');
            $this->CarrinhoAbandonado = new CarrinhoAbandonado();

            //dados banco
            if (!$this->Session->read("Carrinho.carrinho_abandonado") > 0) {
                if ($this->Session->read("Carrinho.itens") != null) {
                    $carrinho_itens = $this->Session->read("Carrinho.itens");
                    if (is_array($carrinho_itens) && count($carrinho_itens) > 0) {
                        foreach ($carrinho_itens as $itens) {
                            $produto = array();
                            $produto['produto_id'] = $itens['produto_id'];
                            $produto['grade_id'] = $itens['grade_id'];
                            $produto['quantidade'] = $itens['quantidade'];
                            $produto['preco'] = $itens['preco'];
                            $data['produtos'][] = $produto;
                        }
                    }
                    //dados usuarios
                    $u = $this->Controller->Auth->User();
                    $data['usuario_id'] = $u['Usuario']['id'];
                    $this->CarrinhoAbandonado->salvar($data);
                }
            }
        }

        /**
         * Define a lista de presentes do carrinho
         *
         * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
         *
         * @param mixed $lista Identificador da lista (falso para identificar compra sem lista)
         * @access public
         * @return Carrinho Próprio objeto para encadeamento
         */
        public function setLista($lista)
        {
            $this->Session->write('Carrinho.dados.lista', $lista);
            return $this;
        }

        /**
         * Obtenção da lista de presentes corrente
         *
         * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
         *
         * @access public
         * @return mixed Valor nulo para valor inicial | Falso para carrinho sem lista | Identificador da lista
         */
        public function getLista()
        {
            return $this->Session->read('Carrinho.dados.lista');
        }

        /**
         * Verifica se o carrinho possui itens
         *
         * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
         *
         * @access public
         * @return bool
         */
        public function isEmpty()
        {
            $itens = $this->Session->read('Carrinho.itens');
            return empty($itens);
        }

        /**
         * Obtenção do identificador do endereço da lista
         *
         * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
         *
         * @access public
         * @return mixed Identificador | Valor nulo em caso de entrega do endereço do comprador
         */
        public function getListaEndereco()
        {
            $result = null;
            $listaId = $this->getLista();
            if ($listaId) {
                App::import('Model', 'ListaDePresente.Lista');
                $model = new Lista();
                $model->id = $listaId;
                $result = $model->field('usuario_endereco_id');
            }
            return $result;
        }

        /**
         * Verifica se deve ser utilizado o endereço da lista de presentes
         *
         * em caso positivo o endereço é setado no carrinho
         *
         * @author Luiz Felipe Cunha <felipe.silvacunha@gmail.com>
         *
         * @access public
         * @return bool
         */
        public function usarEnderecoLista()
        {
            $endereco = $this->getListaEndereco();
            $result = false;
            if ($endereco) {
                $this->setEnderecoEntrega($endereco);
                $this->recalcularCarrinho();
                $result = true;
            }
            return $result;
        }

        /**
         * Obtenção do identificador da loja da lista
         *
         *
         * @access public
         * @return mixed Identificador | Valor nulo em caso de entrega do endereço do comprador
         */
        public function getListaLoja()
        {
            $result = null;
            $listaId = $this->getLista();
            if ($listaId) {
                App::import('Model', 'ListaDePresente.Lista');
                $model = new Lista();
                $model->id = $listaId;
                $result = $model->field('loja_id');
            }
            return $result;
        }

        /**
         * Verifica e seta se os itens da lista deverão ser entregues na loja
         *
         * em caso positivo o endereço é setado no carrinho
         *
         * @access public
         * @return bool
         */
        public function retirarLojaLista()
        {
            $loja = $this->getListaLoja();
            $result = false;
            if ($loja) {
                $this->setEnderecoEntrega($this->getFrete());
                $this->setRetirarLoja(1);
                $this->recalcularCarrinho();
                $result = true;
            }
            return $result;
        }

        /**
         * Seta flag se a compra será tirar na loja ou não
         */
        public function setRetirarLoja($status)
        {
            if ($status == 0) {
                $this->recalcularCarrinho();
            }
            return $this->Session->write("Carrinho.dados.retirar_loja", $status);
        }

        /**
         * Retorna flag se a compra será tirar na loja ou não
         */
        public function getRetirarLoja()
        {
            return $this->Session->read("Carrinho.dados.retirar_loja");
        }

        /**
         * Retorna os dados de endereco de entrega do pedido
         */
        public function getEnderecoEntregaDados()
        {

            $retorno = false;
            App::import('Model', 'UsuarioEndereco');
            $this->UsuarioEndereco = new UsuarioEndereco();

            // a compra não vai ser entregue na loja?
            if ($this->getRetirarLoja() || $this->getEnderecoEntrega() != "") {
                $retorno = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $this->getEnderecoEntrega())));
            } else {
                //usuario logado
                $u = $this->Controller->Auth->User();
                //obtem o endereco de cobranca do usuario logado
                $retorno = $this->UsuarioEndereco->find('first', array(
                    'conditions' => array(
                        'UsuarioEndereco.usuario_id' => $u['Usuario']['id']
                    ),
                    'order'      => array('UsuarioEndereco.cobranca DESC')
                ));
            }
            return $retorno;

        }

        public function changeGridQuantity()
        {
            $this->loadModel('Produto');
            $this->loadModel('Grade');
            foreach($this->getItens() as $item) {
                $this->Grade->alteraEstoque($item['Produto']['grade']['id'], $item['Produto']['quantidade']);
            }
        }

    }

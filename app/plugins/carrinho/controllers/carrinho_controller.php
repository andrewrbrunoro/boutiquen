<?php

    class CarrinhoController extends CarrinhoAppController
    {

        public $uses = array(
            'Pedido',
            'PedidoItem',
            'PedidoHistoricoStatus',
            'FreteTipo',
            'UsuarioEndereco'
        );

        public $components = array(
            'Pagseguro',
            'Carrinho.Carrinho'
        );


        public function beforeFilter()
        {
            parent::beforeFilter();

            App::import('Helper', 'Crypt');
            $this->Crypt = new CryptHelper();

            App::import('Component', 'Pagseguro');
            $this->Pagseguro = new PagseguroComponent();

            App::import('Helper', 'String');
            $this->String = new StringHelper();

            $this->Auth->allow(array('callPayment', 'saveFreights', 'remover_item'));
            $this->loadFreights();
        }

        private function json($error = true, array $data, $msg = '')
        {
            die(json_encode(array('error' => $error, 'data' => $data, 'msg' => $msg)));
        }

        private function loadFreights()
        {
            $this->set('freights', $this->FreteTipo->getFreights());
        }

        public function meus_pedidos()
        {
            $orders = $this->Pedido->find('all',
                array(
                    'contain'    => array(
                        'PedidoItem',
                        'PedidoStatus'
                    ),
                    'conditions' => array(
                        'Pedido.usuario_id' => $this->Auth->user('id')
                    ),
                    'order'      => array('Pedido.id DESC')
                )
            );
            $this->set('data', $orders);
        }

        public function index()
        {
            $cart = $this->Carrinho->getItens() ? $this->Carrinho->getItens() : 0;
            $this->set('cart', $cart);
        }

        public function add()
        {
            if (isset($this->params['data']) && isset($this->params['data']['Compra'])) {
                $data = $this->params['data']['Compra'];
                if (!empty($data['produto_id']) && !empty($data['grade_id'])) {
                    if ($this->Carrinho->Adicionar($data['produto_id'], $data['grade_id'], 1)) {
                        $this->Session->setFlash('O produto foi adicionado no carrinho.', 'flash/success');
                    } else {
                        $this->redirect($this->referer());
                    }
                }
            } else {
                $this->Session->setFlash('Ops, não foi possível adicionar o produto ao seu carrinho. Tente novamente', 'flash/error');
            }
            $this->redirect($this->referer());
        }

        public function alterar_quantidade()
        {
            $this->layout = false;
            $this->render(false);
            if ($this->RequestHandler->isAjax()) {
                if (isset($this->params['form']['sent'])) {
                    $decode = $this->Crypt->decJson($this->params['form']['sent']);
                    if (isset($decode['grade_id']) && isset($decode['quantidade'])) {
                        if ($this->Carrinho->Quantidade($decode['grade_id'], $decode['quantidade'])) {
                            $this->json(false, $this->Session->read('Carrinho.dados.total'), 'Quantidade alterada com sucesso, obrigado.');
                        } else {
                            $this->json(true, array(), 'A Quantidade não está disponível.');
                        }
                    }
                }
            }
        }

        public function forma_pagamento()
        {
            if (!$this->Carrinho->getItens() || count($this->Carrinho->getItens()) == 0) {
                $this->redirect('/carrinho');
            }
            $cart = $this->Carrinho->getItens() ? $this->Carrinho->getItens() : 0;
            $this->set(
                array(
                    'cart'         => $cart,
                    'usuario'      => $this->Usuario->getUsuarioEndereco($this->Auth->user('id'), 1),
                    'pagseguroJS'  => $this->Pagseguro->getUrlJS(),
                    'sessionToken' => $this->Pagseguro->getSessionToken()
                )
            );
        }

        public function saveFreights()
        {
            $this->layout = false;
            $this->render(false);
            if ($this->RequestHandler->isAjax()) {
                if (isset($this->params['form']['sent'])) {
                    $sent = $this->Crypt->decJson($this->params['form']['sent']);
                    $data = $this->Crypt->decJson($sent['data']);
                    if (isset($data['Correios']['codigo']) && isset($data['Correios']['valor'])) {
                        $this->Session->write('Pedido', $data);
                        $this->Session->write('Pedido.total_com_frete', $this->Session->read('Pedido.Correios.valor') + $this->Carrinho->getCarrinhoTotal('total'));
                        die($this->Crypt->encJson(json_encode(
                            array(
                                'error' => false,
                                'data'  => array(
                                    'total'       => $this->Session->read('Pedido.total_com_frete'),
                                    'total_front' => $this->String->bcoToMoeda($this->Session->read('Pedido.total_com_frete')),
                                    'freight'     => $this->String->bcoToMoeda($this->Session->read('Pedido.Correios.valor'))
                                ),
                                'msg'   => 'Frete encontrado com sucesso.'
                            )
                        )));
                    }
                }
            }
        }

        private function getInstallments($installments)
        {
            $installments = explode('#', $installments);
            return array(
                'quantidade' => $installments[0],
                'valor'      => number_format($installments[1], 2, '.', ''),
                'juros'      => $installments[2]
            );
        }

        public function callPayment()
        {
            if (!$this->Session->read('Pedido.Correios.codigo') || !$this->Session->read('Pedido.UsuarioEndereco.id')) {
                $this->Session->setFlash('Por favor, selecione um frete de acordo com o endereço de entrega.', 'flash/error');
                $this->redirect($this->referer());
            } else if (!isset($this->params['form']['pagseguro_payment_type']) || empty($this->params['form']['pagseguro_payment_type'])) {
                $this->Session->setFlash('Houve um erro inesperado, tente novamente.', 'flash/error');
                $this->redirect($this->referer());
            }
            $validate = $this->Pagseguro->validate($this->params['form'], $this->params['form']['pagseguro_payment_type']);
            if ($validate === true) {
                $user = $this->Usuario->getRealTimeUser($this->Auth->user('id'));
                $endereco = $this->Session->read('Pedido.UsuarioEndereco');
                $form = $this->params['form'];
                $installments = array(
                    'quantidade' => 1,
                    'valor'      => 0.00,
                    'juros'      => 1
                );
                if (isset($form['card_installment'])) {
                    $installments = $this->getInstallments($form['card_installment']);
                }
                $pedidoSave['Pedido'] = array(
                    'parcelas'                => $installments['quantidade'],
                    'parcelas_valor'          => $installments['valor'],
                    'interestFree'            => $installments['juros'],
                    'valor_frete'             => $this->Session->read('Pedido.Correios.valor'),
                    'valor_pedido'            => $this->Carrinho->getCarrinhoTotal('total'),
                    'valor_desconto_cupom'    => '',
                    'entrega_tipo'            => $this->Session->read('Pedido.Tipo'),
                    'entrega_prazo'           => $this->Session->read('Pedido.Correios.prazo') + $this->Session->read('Pedido.Frete.dias_adicionais'),
                    'endereco_id'             => $endereco['id'],
                    'endereco_cep'            => $endereco['cep'],
                    'endereco_rua'            => $endereco['rua'],
                    'endereco_numero'         => $endereco['numero'],
                    'endereco_complemento'    => $endereco['complemento'],
                    'endereco_bairro'         => $endereco['bairro'],
                    'endereco_cidade'         => $endereco['cidade'],
                    'endereco_estado'         => $endereco['uf'],
                    'frete_id'                => $this->Session->read('Pedido.Frete.id'),
                    'pagamento_condicao_id'   => '1', //pagseguro padrão
                    'pedido_status_id'        => '1', //pedido sempre inica aguardando pagamento
                    'usuario_id'              => $this->Auth->user('id'),
                    'finalizado'              => '0', //pedido efetuado deve iniciar com 0 de não finalizado, após passar o request recebe 1 = finalizado
                    'usuario_ip'              => $_SERVER['REMOTE_ADDR'],
                    'exportado'               => '',
                    'usuario_nome'            => $user['Usuario']['nome'],
                    'usuario_email'           => $user['Usuario']['email'],
                    'usuario_telefone'        => $user['Usuario']['telefone'],
                    'usuario_cpf'             => $user['Usuario']['cpf'],
                    'usuario_sexo'            => 'F',
                    'usuario_data_nascimento' => $user['Usuario']['data_nascimento'],
                    'cupom'                   => '',
                    'carrinho_abandonado_id'  => '',
                    'campanha'                => '',
                    'referer'                 => ''
                );
                preg_match('/([0-9]+)\)\ ([0-9-]+)/', $user['Usuario']['telefone'], $phone);
                if ($this->Pedido->save($pedidoSave)) {
                    $this->Pagseguro->setPagseguro($this->Pedido->getLastInsertId(), $extraAmount = '0.00');
                    if ($this->params['form']['pagseguro_payment_type'] == 'BOLETO') {
                        $this->Pagseguro->boleto();
                        $boleto = true;
                    } else if ($this->params['form']['pagseguro_payment_type'] == 'CREDIT_CARD') {
                        $this->Pagseguro->credit(
                            array(
                                'card_token'         => $form['card_token'],
                                'quantity'           => $pedidoSave['Pedido']['parcelas'],
                                'installment_amount' => $pedidoSave['Pedido']['parcelas_valor'],
                                'card_name'          => $form['card_name'],
                                'cpf'                => preg_replace("/[^0-9]/", "", $form['card_cpf']),
                                'birth_date'         => $form['card_nasc'],
                                'ddd'                => $phone[1],
                                'phone'              => preg_replace('/[^0-9]/', '', $phone[2])
                            )
                        );
                    } else if ($this->params['form']['pagseguro_payment_type'] == 'ONLINE_DEBIT') {
                        $this->Pagseguro->debit($form['pagseguro_bank']);
                        $debito = true;
                    }
                    $this->Pagseguro->setSender(
                        array(
                            'full_name'      => (isset($form['card_name'])) ? $form['card_name'] : $this->Auth->user('nome'),
                            'cpf'            => preg_replace("/[^0-9]/", "", $pedidoSave['Pedido']['usuario_cpf']),
                            'ddd'            => $phone[1],
                            'phone'          => preg_replace('/[^0-9]/', '', $phone[2]),
                            'senderEmail'    => 'developer@sandbox.com.br',
                            'pagseguro_hash' => $form['hash']
                        )
                    );
                    $this->Pagseguro->setShipping(array(
                        'street'     => $pedidoSave['Pedido']['endereco_rua'],
                        'number'     => $pedidoSave['Pedido']['endereco_numero'],
                        'complement' => $pedidoSave['Pedido']['endereco_complemento'],
                        'district'   => $pedidoSave['Pedido']['endereco_bairro'],
                        'zip'        => $pedidoSave['Pedido']['endereco_cep'],
                        'city'       => $pedidoSave['Pedido']['endereco_cidade'],
                        'state'      => $pedidoSave['Pedido']['endereco_estado'],
                        'country'    => 'BR'
                    ), $pedidoSave['Pedido']['valor_frete']);
                    $this->Pagseguro->setBillingAddress(
                        array(
                            'street'     => $pedidoSave['Pedido']['endereco_rua'],
                            'number'     => $pedidoSave['Pedido']['endereco_numero'],
                            'complement' => $pedidoSave['Pedido']['endereco_complemento'],
                            'district'   => $pedidoSave['Pedido']['endereco_bairro'],
                            'zip'        => $pedidoSave['Pedido']['endereco_cep'],
                            'city'       => $pedidoSave['Pedido']['endereco_cidade'],
                            'state'      => $pedidoSave['Pedido']['endereco_estado'],
                            'country'    => 'BR'
                        )
                    );
                    $sendRequest = $this->Pagseguro->sendRequest($this->Session->read('Carrinho.itens'));
                    if (isset($sendRequest->code) && !empty($sendRequest->code)) {
                        $this->PedidoItem->saveItems($sendRequest->items, $this->Pedido->getLastInsertId());
                        $this->PedidoHistoricoStatus->save(array('PedidoHistoricoStatus' => array('pedido_id' => $this->Pedido->getLastInsertId(), 'pedido_status_id' => 1)));
                        $savePedido = $this->Pedido->save(
                            array(
                                'Pedido' => array(
                                    'id'                     => $this->Pedido->getLastInsertId(),
                                    'finalizado'             => true,
                                    'boleto'                 => isset($boleto) ? true : false,
                                    'debito'                 => isset($debito) ? true : false,
                                    'pagseguro_payment_link' => (string)$sendRequest->paymentLink,
                                    'pagseguro_code'         => (string)$sendRequest->code,
                                    'pagseguro_method_type'  => (string)$sendRequest->paymentMethod->type,
                                    'pagseguro_method_code'  => (string)$sendRequest->paymentMethod->code,
                                    'transaction_json'       => json_encode($sendRequest)
                                )
                            )
                        );
                        if ($savePedido) {
                            $this->Session->write('pedido_id', $this->Pedido->getLastInsertId());
                            $this->Carrinho->changeGridQuantity();
                            $this->Carrinho->Esvaziar();
                            $this->redirect('/carrinho/compra-finalizada');
                        } else {
                            $this->Session->setFlash('Não foi possível finalizar a compra, tente novamente.', 'flash/error');
                            $this->redirect($this->referer());
                        }
                    } else {
                        $this->Pedido->save(
                            array(
                                'Pedido' => array(
                                    'id'                => $this->Pedido->getLastInsertId(),
                                    'transaction_error' => json_encode($sendRequest)
                                )
                            )
                        );
                        $this->Session->setFlash('Ops, verifique se todos os campos foram preenchidos corretamente.', 'flash/error');
                        $this->redirect($this->referer());
                    }
                } else {
                    $this->Session->setFlash('Um erro inesperado aconteceu, tente novamente.', 'flash/error');
                    $this->redirect($this->referer());
                }
            } else {
                $this->Session->setFlash($validate, 'flash/error');
                $this->redirect($this->referer());
            }
        }

        public function finalizacao()
        {
            if (!$this->Session->read('pedido_id')) {
                $this->redirect('/');
            }
            $pedido = $this->Pedido->find('first',
                array(
                    'conditions' => array(
                        'Pedido.usuario_id' => $this->Auth->user('id'),
                        'Pedido.id'         => $this->Session->read('pedido_id')
                    )
                )
            );
            $this->Session->delete('pedido_id');
            if (!$pedido) {
                $this->Session->setFlash('Sentimos muito, não foi encontrado seu pedido. Entre em contato conosco.', 'flash/error');
                $this->redirect('/');
            }
            $this->set(compact('pedido'));
        }

        public function remover_item($grade_id)
        {
            $cart = $this->Session->read('Carrinho.itens');
            if (isset($cart[$grade_id])) {
                $this->Session->delete('Carrinho.itens.' . $grade_id);
                $this->Session->setFlash('O item foi removido do carrinho com sucesso.', 'flash/success');
            }
            $this->redirect($this->referer());
        }

    }

<div class="container">
	<header class="page-header">
        <h1 class="page-title">
			<img src="https://cdn3.iconfinder.com/data/icons/ballicons-reloaded-free/512/icon-68-48.png"/>
			PEDIDOS
		</h1>
		<span class="text-info">
			Aqui você pode ver como está o andamento do pedido ou consultar o histórico de pedidos.
		</span>
    </header>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 table-shopping-cart">
			<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Nº</th>
							<th>Data</th>
							<th class="text-center">Quantidade</th>
							<th>Total</th>
							<th>Status</th>
<!--							<th>&nbsp;</th>-->
						</tr>
					</thead>

					<tbody>
						<?php $total_items = 0; ?>
						<?php $total = 0.00; ?>
						<?php if (isset($data) && count($data)) { ?>
							<?php foreach($data as $order) { ?>
								<?php $quantity = 0; ?>
								<?php if (isset($order['PedidoItem']) && count($order['PedidoItem'])) { ?>
									<?php foreach($order['PedidoItem'] as $item) { ?>
										<?php $quantity = $quantity + $item['quantidade']; ?>
									<?php } ?>
								<?php } ?>
								<tr>
									<td>#<?php echo $order['Pedido']['id'] ?></td>
									<td><?php echo $order['Pedido']['id'] ?></td>
									<td class="text-center"><?php echo $quantity; ?></td>
									<td>R$ <?php echo $this->String->bcoToMoeda($order['Pedido']['valor_frete'] + $order['Pedido']['valor_pedido'] - $order['Pedido']['valor_desconto_cupom']) ?></td>
									<td><?php echo $order['PedidoStatus']['nome'] ?></td>
<!--									<td class="text-right">-->
<!--										<a class="btn btn-mini"-->
<!--										   href="#">-->
<!--											View &nbsp;-->
<!--											<i class="fa fa-angle-right"></i>-->
<!--										</a>-->
<!--									</td>-->
								</tr>
								<?php $total = $total + ($order['Pedido']['valor_frete'] + $order['Pedido']['valor_pedido'] - $order['Pedido']['valor_desconto_cupom']) ?>
								<?php $total_items = $total_items + $quantity; ?>
							<?php } ?>
                        <?php } else { ?>
                            <tr>
								<td colspan="6">Nenhum pedido efetuado no momento.</td>
							</tr>
                        <?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<article class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<div class="block-order-total box-border wow fadeInRight animated"
                 data-wow-duration="1s"
                 style="visibility: visible;-webkit-animation-duration: 1s; -moz-animation-duration: 1s; animation-duration: 1s;">
				<h3>R$ Total</h3>
				<hr>
				<ul class="list-unstyled">
					<li>Cliente desde: <strong><?php echo $auth['Usuario']['created'] ?></strong></li>
					<li>Total de pedidos: <strong><?php echo count($data); ?></strong></li>
					<li>Total de produtos: <strong><?php echo $total_items; ?></strong></li>
					<li class="color-active">Total gasto: <strong>R$ <?php echo $this->String->bcoToMoeda($total) ?></strong></li>
				</ul>

			</div>
		</article>
	</div>
</div>
<?php echo $this->Form->create('Pedido', array('class' => 'form', 'url' => '/service/Payment')) ?>
<div class="container">
    <header class="page-header">
        <h1 class="page-title">Pagamento</h1>
    </header>
    <p class="checkout-login-text text-info">
        <small>Para efetuar o pagamento corretamente, certifique-se que todos os campos obrigatórios estão preenchidos.</small>
    </p>
    <div class="row row-col-gap"
         data-gutter="60">
        <div class="col-md-4">
            <h3 class="widget-title">Informações do pedido</h3>
            <div class="box">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Produto</th>
                            <th class="text-center">Qtd.</th>
                            <th>Preço/Un.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $subtotal = 0.00;
                            $total = 0.00;
                            $free_shipping = false;
                        ?>
                        <?php foreach ($cart as $key => $item) { ?>
                            <?php $cor = explode('-', $item['Produto']['sku']) ?>
                            <tr>
                                <td><?php echo $item['Produto']['nome'] ?></td>
                                <td class="text-center"><?php echo $item['Produto']['quantidade'] ?></td>
                                <td>R$ <?php echo $this->String->bcoToMoeda($item['Produto']['preco_promocao'] ? $item['Produto']['preco_promocao'] : $item['Produto']['preco']) ?></td>
                            </tr>
                            <?php
                            $subtotal = $subtotal + $item['Produto']['preco'] * $item['Produto']['quantidade'];
                            $total = $total + ($item['Produto']['preco_promocao'] ? $item['Produto']['preco_promocao'] : $item['Produto']['preco']) * $item['Produto']['quantidade'];
                            ?>
                        <?php } ?>
                        <?php if ($this->Session->read('Pedido.Correios.codigo')) { ?>
                            <tr id="freight-value">
                                <td>Frete</td>
                                <td></td>
                                <td><?php echo ($this->Session->read('Pedido.Correios.valor') && $this->Session->read('Pedido.Correios.valor') > 0) ? 'R$ ' . $this->String->bcoToMoeda($this->Session->read('Pedido.Correios.valor')) : 'Grátis' ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td id="total-front">R$ <?php echo $this->String->bcoToMoeda($total  + ($this->Session->read('Pedido.Correios.valor') ? $this->Session->read('Pedido.Correios.valor') : 0.00)); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-4">
            <h3 class="widget-title">Endereço de entrega</h3>
            <div class="widget-box">
                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <div class="tickets-container">
                            <ul class="tickets-list">
                                <?php if (isset($usuario['UsuarioEndereco']) && count($usuario['UsuarioEndereco'])) { ?>
                                    <?php foreach ($usuario['UsuarioEndereco'] as $endereco) { ?>
                                        <li class="ticket-item">
                                            <div class="row">
                                                <div class="ticket-user col-md-10 col-sm-12">
                                                    <img src="https://cdn2.iconfinder.com/data/icons/dellipack/48/home.png"
                                                         class="user-avatar">
                                                    <span class="user-name"><?php echo $endereco['nome'] ?></span>
                                                    <span class="user-at">Nº</span>
                                                    <span class="user-company"><?php echo $endereco['numero'] ?></span>
                                                    <span class="user-name"><?php echo $endereco['bairro'] . ', ' . $endereco['cidade'] . ' - ' . $endereco['uf'] ?></span>
                                                </div>
                                                <div class="ticket-state <?php echo ($endereco['status']) ? 'bg-palegreen' : 'bg-yellow' ?>">
                                                    <a href="<?php echo $this->Html->Url('/usuarios/ativaEndereco/' . $endereco['id'] . '/' . $endereco['status']); ?>">
                                                        <?php if ($endereco['status']) { ?>
                                                            <i class="fa fa-check"
                                                               title="Ativo"></i>
                                                        <?php } else { ?>
                                                            <i class="fa fa-times"
                                                               title="Inativado"></i>
                                                        <?php } ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                <?php } else { ?>
                                    <li class="ticket-item">
                                        <div class="row">
                                            <div class="ticket-user col-md-6 col-sm-12">
                                                <img src="https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Desert-48.png"
                                                     class="user-avatar">
                                                <span class="user-name">Nenhum endereço cadastrado no momento</span>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <input type="hidden"
                       id="amount"
                       value="<?php echo $total + ($this->Session->read('Pedido.Correios.valor') ? $this->Session->read('Pedido.Correios.valor') : 0.00); ?>"/>
            </div>
            <?php if (isset($usuario['UsuarioEndereco']) && count($usuario['UsuarioEndereco'])) { ?>
                <?php foreach ($usuario['UsuarioEndereco'] as $endereco) { ?>
                    <?php if ($endereco['status'] == true) { ?>
                        <h4><?php echo $endereco['nome'] ?></h4>
                        <table class="table box">
                            <thead>
                                <tr>
                                    <td width="20%">Selecione</td>
                                    <td>Frete</td>
                                    <td>Dias</td>
                                    <td>Valor</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($freights) && count($freights)) { ?>
                                    <?php if ($total < 200) { ?>
                                        <?php foreach ($freights as $freight) { ?>
                                            <?php foreach ($freight['Frete'] as $frete) { ?>
                                                <?php $current = $this->String->getFrete($frete['nome'], $endereco['cep']) ?>
                                                <?php if ($current) { ?>
                                                    <?php
                                                    $set = array(
                                                        'Correios'        => $current,
                                                        'UsuarioEndereco' => $endereco,
                                                        'Frete'           => $frete
                                                    );
                                                    ?>
                                                    <tr>
                                                    <td class="text-center">
                                                        <label>
                                                            <input name="data[Pedido][frete]"
                                                                   class="i-check pedidoFrete"
                                                                   type="radio"
                                                                   <?php echo ($this->Session->read('Pedido.UsuarioEndereco.id') && $this->Session->read('Pedido.UsuarioEndereco.id') == $endereco['id']) ? 'checked' : '' ?>
                                                                   value='<?php echo $this->Crypt->encJson($set) ?>'/>
                                                        </label>
                                                    </td>
                                                    <td><?php echo $frete['descricao'] ?></td>
                                                    <td><?php echo $current['prazo'] + $frete['dias_adicionais']; ?></td>
                                                    <td>R$ <?php echo $this->String->bcoToMoeda($current['valor']) ?></td>
                                                </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php $current = $this->String->getFrete('pac', $endereco['cep']) ?>
                                        <?php
                                        $set = array(
                                            'Correios'        => array(
                                                'codigo' => 'Gratis',
                                                'valor'  => 0.00,
                                                'prazo'  => $current['prazo']
                                            ),
                                            'UsuarioEndereco' => $endereco,
                                            'Frete'           => array(
                                                'id'              => 'Gratis',
                                                'dias_adicionais' => 7
                                            )
                                        );
                                        ?>
                                        <tr>
                                            <td class="text-center">
                                                <label>
                                                    <input name="data[Pedido][frete]"
                                                           class="i-check"
                                                           type="radio"
                                                           <?php echo ($this->Session->read('Pedido.UsuarioEndereco.id') && $this->Session->read('Pedido.UsuarioEndereco.id') == $endereco['id']) ? 'checked' : '' ?>
                                                           value='<?php echo $this->Crypt->encJson($set) ?>'/>
                                                </label>
                                            </td>
                                            <td>Grátis</td>
                                            <td><?php echo Configure::read('FreteGratis.prazo_entrega') ?></td>
                                            <td>R$ 0,00</td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>

        <div class="col-md-4">
            <h3 class="widget-title">Método pagamento</h3>
            <div id="pagseguro"></div>
            <button type="submit"
                    class="btn btn-success"
                    id="pagseguro-finish">
                Efetuar pagamento
            </button>
        </div>

    </div>
</div>
<?php echo $this->Form->end() ?>


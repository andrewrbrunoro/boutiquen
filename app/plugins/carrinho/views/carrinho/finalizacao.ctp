<div class="gap"></div>

<div class="container">
    <div class="payment-success-icon fa fa-check-circle-o"></div>
    <div class="payment-success-title-area">
        <h1>Pagamento concluído <?php echo $pedido['Usuario']['nome'] ?></h1>
        <p class="lead">
            O seu pedido foi enviado para <strong><?php echo $pedido['Usuario']['email'] ?></strong> <br />
            Status do pedido: <strong style="color: #<?php echo $pedido['PedidoStatus']['cor'] ?>"><?php echo $pedido['PedidoStatus']['nome'] ?></strong>
        </p>
    </div>
    <div class="gap gap-small"></div>
    <div class="row row-col-gap">
        <div class="col-md-4">
            <h3 class="widget-title">Pedido</h3>
            <div class="box">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Qtd.</th>
                            <th>Preço/Un.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($pedido['PedidoItem'] as $item) { ?>
                            <tr>
                                <td><?php echo $item['nome'] ?></td>
                                <td><?php echo $item['quantidade'] ?></td>
                                <td>R$ <?php echo $this->String->bcoToMoeda($item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco']) ?></td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td>Sub total</td>
                            <td></td>
                            <td>R$ <?php echo $this->String->bcoToMoeda($pedido['Pedido']['valor_pedido']) ?></td>
                        </tr>
                        <tr>
                            <td>Cupom</td>
                            <td></td>
                            <td>R$ <?php echo $this->String->bcoToMoeda($pedido['Pedido']['valor_desconto_cupom']) ?></td>
                        </tr>
                        <tr>
                            <td>Frete</td>
                            <td></td>
                            <td><?php echo $pedido['Pedido']['valor_frete'] > 0 ? 'R$ ' . $this->String->bcoToMoeda($pedido['Pedido']['valor_frete']) : 'Grátis' ?></td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td></td>
                            <td>R$ <?php echo $this->String->bcoToMoeda(($pedido['Pedido']['valor_frete'] + $pedido['Pedido']['valor_pedido']) - $pedido['Pedido']['valor_desconto_cupom']) ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-5">
                <h3 class="widget-title">PAGAMENTO</h3>
            <?php if ($pedido['Pedido']['boleto'] == 1) { ?>
                <div class="box">
                    <p class="text-center text-info">

                    </p>
                    <a target="_blank" href="<?php echo $pedido['Pedido']['pagseguro_payment_link'] ?>">
                        <img src="https://cdn0.iconfinder.com/data/icons/50-payment-system-icons-2/120/Boleto.png" class="img-responsive center-block">
                        <p class="text-info text-center">
                            Imprimir boleto
                        </p>
                    </a>
                </div>
            <?php } else if ($pedido['Pedido']['debito'] == 1) { ?>
                <div class="box">
                    <p class="text-center text-info">

                    </p>
                    <a target="_blank" href="<?php echo $pedido['Pedido']['pagseguro_payment_link'] ?>">
                        <img src="<?php echo $pedido['PagseguroCode']['image'] ?>" class="img-responsive center-block">
                        <p class="text-info text-center">
                            Débito online
                        </p>
                    </a>
                </div>
            <?php } else { ?>
                <div class="box">
                    <p class="text-center text-info">
                        
                    </p>
                    <img src="<?php echo $pedido['PagseguroCode']['image'] ?>"
                         class="img-responsive center-block"
                         alt="">
                    <p class="text-info text-center">
                        <?php echo $pedido['PagseguroCode']['nome'] ?>
                    </p>
                    <p class="text-center text-info">
                        <?php echo $pedido['Pedido']['parcelas'] == 1 ? 'À vista' : $pedido['Pedido']['parcelas'].'x de ' ?>  <strong class="text-success">R$ <?php echo $this->String->bcoToMoeda($pedido['Pedido']['parcelas_valor']) ?></strong>
                    </p>
                </div>
            <?php } ?>
            <h3 class="widget-title">Endereço de entrega</h3>
            <div class="box">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Endereço</th>
                            <th>Dias</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php echo $pedido['Pedido']['endereco_rua'] ?>, <?php echo $pedido['Pedido']['endereco_numero'] ?>
                                /<?php echo $pedido['Pedido']['endereco_cidade'] ?> - <?php echo $pedido['Pedido']['endereco_estado'] ?>
                            </td>
                            <td>
                                <?php echo $pedido['Pedido']['entrega_prazo'] ?>
                            </td>
                            <td>
                                <?php echo $pedido['Pedido']['valor_frete'] == 0 ? 'Grátis' : 'R$ ' . $this->String->bcoToMoeda($pedido['Pedido']['valor_frete']) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-3">
            <h3 class="widget-title">Curte nossa página</h3>
            <div class="box">
                <ul class="payment-success-share-list">
                    <li>
                        <a class="fa fa-facebook"
                           href="#"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="gap gap-small"></div>
</div>
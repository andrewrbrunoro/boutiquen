<div class="container">

	<?php if ($cart) { ?>
        <header class="page-header">
            <h1 class="page-title">Meu carrinho de compras</h1>
        </header>
        <div class="row">
        <div class="col-md-10">
            <table class="table table table-shopping-cart">
                <thead>
                    <tr>
                        <th>Produto</th>
                        <th>Nome</th>
                        <th>Cor/Tamanho</th>
                        <th>Preço/Un.</th>
                        <th>Quantidade</th>
                        <th>Remover</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $subtotal = 0.00;
                        $total = 0.00;
                        $free_shipping = false;
                    ?>
                    <?php foreach ($cart as $key => $item) { ?>
                        <?php $cor = explode('-', $item['Produto']['grade']['sku']) ?>
                        <tr>
                            <td class="table-shopping-cart-img">
                                <a href="<?php echo $item['Produto']['url'] ?>">
                                    <?php
                                        echo $this->Html->image('http://boutiqueno.com.br/' . $item['Produto']['imagem'], array(
                                                'class' => 'img-responsive',
                                                'alt'   => $item['Produto']['nome'],
                                                'title' => $item['Produto']['nome']
                                            )
                                        )
                                    ?>
                                </a>
                            </td>
                            <td class="table-shopping-cart-title">
                                <a href="<?php echo $this->Html->Url(DS . $item['Produto']['url']) ?>">
                                    <?php echo $item['Produto']['nome'] ?>
                                </a>
                            </td>
                            <td>
                                <?php echo $cor[1] . '/' . $cor[2] ?>
                            </td>
                            <td>R$ <?php echo $this->String->bcoToMoeda($item['Produto']['preco_promocao'] ? $item['Produto']['preco_promocao'] : $item['Produto']['preco']) ?></td>
                            <td>
                                <?php echo $this->Form->input('Carrinho.quantidade', array(
                                        'value'     => $item['Produto']['quantidade'],
                                        'data-grid' => $item['Produto']['grade_id'],
                                        'label'     => false,
                                        'class'     => 'form-control table-shopping-qty'
                                    )
                                ) ?>
                            </td>
                            <td>
                                <a class="fa fa-close table-shopping-remove"
                                   title="Remover item do carrinho"
                                   href="<?php echo $this->Html->Url('/carrinho/remover_item/' . $key) ?>"></a>
                            </td>
                        </tr>
                        <?php
                        $subtotal = $subtotal + $item['Produto']['preco'] * $item['Produto']['quantidade'];
                        $total = $total + ($item['Produto']['preco_promocao'] ? $item['Produto']['preco_promocao'] : $item['Produto']['preco']) * $item['Produto']['quantidade'];
                        ?>
                    <?php } ?>
                </tbody>
            </table>
            <div class="gap gap-small"></div>
        </div>
            <div class="col-md-2">
                <ul class="shopping-cart-total-list">
                    <li>
                        <span>Sub total</span>
                        <span id="sub_total">R$ <?php echo $this->String->bcoToMoeda($subtotal) ?></span>
                    </li>
                    <li>
                        <span>Frete</span>
                        <span id="frete"><?php echo ($free_shipping || $subtotal > 200) ? 'Grátis' : 'R$ 0,00' ?></span>
                    </li>
                    <li>
                        <span>Total</span>
                        <span id="total">R$ <?php echo $this->String->bcoToMoeda($total); ?></span>
                    </li>
                </ul>
                <a class="btn btn-success"
                   href="<?php echo $this->Html->Url('/carrinho/pagamento') ?>">
                    Finalizar Compra
                </a>
            </div>
        </div>
        <ul class="list-inline">
            <li>
                <a class="btn btn-default"
                   href="<?php echo $this->String->Url('/') ?>">
                    Continuar Comprando
                </a>
            </li>
        </ul>

    <?php } else { ?>
        <div class="text-center"><i class="fa fa-cart-arrow-down empty-cart-icon"></i>
			<p class="lead">Seu carrinho está vazio.</p>
			<a class="btn btn-success btn-lg"
               href="<?php echo $this->Html->Url('/') ?>">
				Comece a comprar <i class="fa fa-long-arrow-right"></i>
			</a>
		</div>
    <?php } ?>
    <div class="gap"></div>
</div>
<?php

    /**
     * AclFilter Component
     *
     * PHP version 5
     *
     * @category Component
     * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
     */
    class AclFilterComponent extends Object
    {

        /**
         * @param object $controller controller
         * @param array  $settings   settings
         */
        function initialize(&$controller, $settings = array())
        {
            $this->controller = &$controller;
        }

        /**
         * acl and auth
         *
         * @return void
         */
        function auth()
        {

            $this->controller->Auth->userModel = 'Usuario';
            $this->controller->Auth->flashElement = 'flash/error';
            $this->controller->Auth->fields = array('username' => 'email', 'password' => 'senha');
            $this->controller->Auth->autoRedirect = true;
            $this->controller->Auth->loginError = "Login e/ou senha inválidos";
            $this->controller->Auth->actionPath = 'controllers/';

            //Configure AuthComponent
            $this->controller->Auth->authorize = 'actions';

            if (isset($this->controller->params['admin']) && $this->controller->params['admin'] == true) {
                $this->controller->Auth->loginAction = array('controller' => 'usuarios', 'action' => 'login', 'admin' => true, 'plugin' => null);
                $this->controller->Auth->loginRedirect = array('controller' => 'home', 'action' => 'index', 'admin' => true, 'plugin' => null);
                $this->controller->Auth->logoutRedirect = array('controller' => 'usuarios', 'action' => 'login', 'admin' => true, 'plugin' => null);
                $this->controller->Auth->authError = ' ';
            } else {
                $this->controller->Auth->loginAction = array('controller' => 'usuarios', 'action' => 'login', 'admin' => false, 'plugin' => null);
                $this->controller->Auth->loginRedirect = array('controller' => 'home', 'action' => 'index', 'admin' => false, 'plugin' => null);
                $this->controller->Auth->logoutRedirect = array('controller' => 'home', 'action' => 'index', 'admin' => false, 'plugin' => null);
                $this->controller->Auth->authError = 'Efetue o login para prosseguir';
            }

            $this->controller->Auth->userScope = array('Usuario.status' => 1);
            $this->controller->Auth->actionPath = 'controllers/';

            if ($this->controller->Auth->user() && $this->controller->Auth->user('grupo_id') == 1) {
                // Role: Admin
                $this->controller->Auth->allowedActions = array('*');
            } else {
                if ($this->controller->Auth->user()) {
                    $roleId = $this->controller->Auth->user('grupo_id');
                } else {
                    $roleId = 3; // Role: Public
                }

                $thisControllerNode = $this->controller->Acl->Aco->node($this->controller->Auth->actionPath . $this->controller->name);
                if ($thisControllerNode) {
                    $thisControllerNode = $thisControllerNode['0'];
                    $thisControllerActions = $this->controller->Acl->Aco->find('list', array(
                        'conditions' => array(
                            'Aco.parent_id' => $thisControllerNode['Aco']['id'],
                        ),
                        'fields'     => array(
                            'Aco.id',
                            'Aco.alias',
                        ),
                        'recursive'  => '-1',
                    ));
                    $thisControllerActionsIds = array_keys($thisControllerActions);
                    $allowedActions = $this->controller->Acl->Aco->Permission->find('list', array(
                        'conditions' => array(
                            'Permission.aro_id'  => $roleId,
                            'Permission.aco_id'  => $thisControllerActionsIds,
                            'Permission._create' => 1,
                            'Permission._read'   => 1,
                            'Permission._update' => 1,
                            'Permission._delete' => 1,
                        ),
                        'fields'     => array(
                            'id',
                            'aco_id',
                        ),
                        'recursive'  => '-1',
                    ));
                    $allowedActionsIds = array_values($allowedActions);
                }


                $allow = array();
                if (isset($allowedActionsIds) &&
                    is_array($allowedActionsIds) &&
                    count($allowedActionsIds) > 0
                ) {
                    foreach ($allowedActionsIds AS $i => $aId) {
                        $allow[] = $thisControllerActions[$aId];
                    }
                }
                //excessão para dúvidas frequentes pois não liberou pelo ACL
                if ($this->controller->params['controller'] == 'duvida_frequentes' && substr($this->controller->action, 0, 5) != 'admin') {
                    $this->controller->Auth->allowedActions = array('*');
                } else {
                    $this->controller->Auth->allowedActions = $allow;
                }
            }

        }

    }
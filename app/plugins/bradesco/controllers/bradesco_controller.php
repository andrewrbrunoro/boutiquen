<?php

class BradescoController extends BradescoAppController {

    var $uses = array('Pedido');
    var $components = array('session', 'requesthandler');
    var $param = array('boleto_vencimento' => 3, 'razao_social' => 'Confeccoes For Yetts IMP e EXP LTDA');

    /**
     * adiciona o produto no carrinho
     */
    public function link() {

        $this->layout = false;
        $this->render(false);

        $tipo_tran = $_POST['transId'];
        $codigo_pedido = $_POST['numOrder'];	

        $pedido = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => (int) $codigo_pedido)));
        App::import("helper", "String");
        $this->String = new StringHelper();

        $BradescoDiasdeVencimento = $this->param['boleto_vencimento'];  // total dias após a emissão para o vencimento
        // veja que esse if trata 3 formas de pagamento, no caso aqui o retorno é o getBoleto
        if ($pedido && (($tipo_tran == "getOrder") || ($tipo_tran == "getBoleto") || ($tipo_tran == "getTransfer"))) {

            $valortotal = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
            $taxa_envio = $pedido['Pedido']['valor_frete'];

            $sacado = iconv("UTF-8", "ISO-8859-1", $pedido['Usuario']['nome']);
            if($pedido['Usuario']['cpf'] != ""){
                $cgccpf = $pedido['Usuario']['cpf'];
            }else{
                $cgccnpj = $pedido['Usuario']['cnpj'];
            }
            $complemento = iconv("UTF-8", "ISO-8859-1", $pedido['Pedido']['endereco_complemento']);
            $rua = iconv("UTF-8", "ISO-8859-1", $pedido['Pedido']['endereco_rua'] . ',' . $pedido['Pedido']['endereco_numero']);
            $cidade = iconv("UTF-8", "ISO-8859-1", $pedido['Pedido']['endereco_cidade']);
            $estado = $pedido['Pedido']['endereco_estado'];
            $cep = $pedido['Pedido']['endereco_cep'];

            $BradescoShopFacil = "0";  // se o lojista fizer parte do shopping bradesco deve setar pra 1 senão fica em 0
            $BradescoRazaoSocial = $this->param['razao_social'];   // razao social da loja
            $BradescoAgencia = "001";  // para embiente de testes deve fica em 001
            $BradescoCodigoCedente = "1234567"; // para embiente de testes BOLETO  deve fica em 1234567
            /* $BradescoAgencia = "0001";  // para embiente de testes deve fica em 001
              if($tipo_tran == "getBoleto"){
              $BradescoCodigoCedente = "1234567"; // para embiente de testes BOLETO  deve fica em 1234567
              }else{
              $BradescoCodigoCedente = "0000001"; // para embiente de testes TRANSFERENCIA deve fica em 1234567
              } */
            $BradescoAgencia = "001";  // para embiente de testes deve fica em 001
            $BradescoCodigoCedente = "1234567"; // para embiente de testes BOLETO  deve fica em 1234567
            // as assinaturas devem ser copiadas do pdf e não podem ter qualquer alteração.
            $BradescoAssinaturaTransfer = "233542AD8CA027BA56B63C2E5A530029F68AACD5E152234BFA1446836220CAA53BD3EA92B296CA94A313E4E438AD64C1E4CF2CBAD6C67DAA00DE7AC2C907A99979A5AB53BFEF1FD6DD3D3A24B278536929F7F747907F7F922C6C0F3553F8C6E29D68E1F6E0CA2566C46C63A2DD65AFF7DF4802FBF4811CA58619B33989B8DDF8";
            //$BradescoAssinaturaBoleto = "24AF74AE050C0D11D0DE83893A6B340EC4257C80E21E893880606E930465DA5E6192B97201B0DB9D41683E36FF3FBD1D7FE574900E93CD472338A0AB2C38092FEC2D22882975D016DCF44D861B181D4240C667FE22E809CA7DFBD57F9DE188B8783DC1291A0320FC4DC21A806186D6333227384C03D2213FF0E9EF6187214C70";
            $BradescoAssinaturaBoleto = "233542AD8CA027BA56B63C2E5A530029F68AACD5E152234BFA1446836220CAA53BD3EA92B296CA94A313E4E438AD64C1E4CF2CBAD6C67DAA00DE7AC2C907A99979A5AB53BFEF1FD6DD3D3A24B278536929F7F747907F7F922C6C0F3553F8C6E29D68E1F6E0CA2566C46C63A2DD65AFF7DF4802FBF4811CA58619B33989B8DDF8";
            $boleto_emitido = 0;

            // aqui inicia o processo da montagem dos dados sei la parece XML

            $Resposta = "<BEGIN_ORDER_DESCRIPTION><orderid>=(" . $codigo_pedido . ")";

            foreach ($pedido['PedidoItem'] as $carrinho) {

                $quantidade = str_replace(",", ".", $carrinho['quantidade']);
                $preco_unitario = $carrinho['preco_promocao'] > 0 ? $carrinho['preco_promocao'] : $carrinho['preco'];
                $total_aux = $quantidade * $preco_unitario;
                $total_aux = str_replace(",", "", number_format($total_aux, 2, ",", "."));
                $total_aux = str_replace(".", "", $total_aux);
                $descricao = str_replace(")", "", $carrinho['nome']);
                $descricao = str_replace("(", "", $descricao);

                $Resposta .= chr(13) . chr(10) . "<descritivo>=(" . $descricao . ")" . chr(13) . chr(10);
                $Resposta .= "<quantidade>=(" . $quantidade . ")" . chr(13) . chr(10);
                $Resposta .= "<unidade>=(un)" . chr(13) . chr(10);
                $Resposta .= "<valor>=(" . $total_aux . ")";
            }

            $taxa_envio = str_replace(",", ".", $taxa_envio);
            $total = str_replace(",", "", number_format($taxa_envio, 2, ",", "."));
            $total = str_replace(".", "", $total);
            if ($taxa_envio > 0) {
                $Resposta .= chr(13) . chr(10) . "<adicional>=(Taxa de envio)" . chr(13) . chr(10);
                $Resposta .= "<valorAdicional>=(" . $total . ")";
            }

            $Resposta .= "<END_ORDER_DESCRIPTION>" . chr(13) . chr(10);

            echo $Resposta;

            if ($tipo_tran == "getTransfer") {
                $Resposta = "";
                $Resposta .= chr(13) . chr(10);
                $Resposta .= "<BEGIN_TRANSFER_DESCRIPTION><NUMEROAGENCIA>=(" . $BradescoAgencia . ")" . chr(13) . chr(10);
                $Resposta .= "<NUMEROCONTA>=(" . $BradescoCodigoCedente . ")" . chr(13) . chr(10);
                $Resposta .= "<ASSINATURA>=(" . $BradescoAssinaturaTransfer . ")<END_TRANSFER_DESCRIPTION>" . chr(13) . chr(10);
                echo $Resposta;
            }

            if ($tipo_tran == "getBoleto") {
                $Resposta = $cpfCnpjCorrigido = "";
                
                if(isset($cgccpf) && $cgccpf != ""){
                    for ($i = 0; $i <= strlen($cgccpf); $i++) {
                        if (is_numeric(substr($cgccpf, $i, 1))) {
                            $cpfCnpjCorrigido .= substr($cgccpf, $i, 1);
                        }
                    }
                }
                
                if(isset($cgccnpj) && $cgccnpj != ""){
                    for ($i = 0; $i <= strlen($cgccnpj); $i++) {
                        if (is_numeric(substr($cgccnpj, $i, 1))) {
                            $cpfCnpjCorrigido .= substr($cgccnpj, $i, 1);
                        }
                    }
                }
                $hoje = date("d/m/Y");
                $total = 'R$ ' . $valortotal;
                $total = str_replace(" ", "", $total);
                $vencimento = "86400" * $BradescoDiasdeVencimento + mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                $vencimento = date("d/m/Y", $vencimento);
                $Resposta .= chr(13) . chr(10);
                $Resposta .= "<BEGIN_BOLETO_DESCRIPTION><CEDENTE>=(" . $BradescoRazaoSocial . ")" . chr(13) . chr(10);
                $Resposta .= "<BANCO>=(237)" . chr(13) . chr(10);
                $Resposta .= "<NUMEROAGENCIA>=(" . $BradescoAgencia . ")" . chr(13) . chr(10);
                $Resposta .= "<NUMEROCONTA>=(" . $BradescoCodigoCedente . ")" . chr(13) . chr(10);
                $Resposta .= "<ASSINATURA>=(" . $BradescoAssinaturaBoleto . ")" . chr(13) . chr(10);
                $Resposta .= "<DATAEMISSAO>=(" . $hoje . ")" . chr(13) . chr(10);
                $Resposta .= "<DATAPROCESSAMENTO>=(" . $hoje . ")" . chr(13) . chr(10);
                $Resposta .= "<DATAVENCIMENTO>=(" . $vencimento . ")" . chr(13) . chr(10);
                $sacado = str_replace(")", "", $sacado);
                $sacado = str_replace("(", "", $sacado);
                $Resposta .= "<NOMESACADO>=(" . $sacado . ")" . chr(13) . chr(10);
                $rua = str_replace(")", "", $rua);
                $rua = str_replace("(", "", $rua);
                $complemento = str_replace($complemento, ")", "");
                $complemento = str_replace($complemento, "(", "");
                $Resposta .= "<ENDERECOSACADO>=(" . $rua . ", " . $complemento . ")" . chr(13) . chr(10);
                $cidade = str_replace(")", "", $cidade);
                $cidade = str_replace("(", "", $cidade);
                $Resposta .= "<CIDADESACADO>=(" . $cidade . ")" . chr(13) . chr(10);
                $Resposta .= "<UFSACADO>=(" . $estado . ")" . chr(13) . chr(10);
                $Resposta .= "<CEPSACADO>=(" . $cep . ")" . chr(13) . chr(10);
                $Resposta .= "<CPFSACADO>=(" . $cpfCnpjCorrigido . ")" . chr(13) . chr(10);
                $Resposta .= "<NUMEROPEDIDO>=(" . $codigo_pedido . ")" . chr(13) . chr(10);
                $Resposta .= "<VALORDOCUMENTOFORMATADO>=(" . $total . ")" . chr(13) . chr(10);
                $Resposta .= "<SHOPPINGID>=(0)<END_BOLETO_DESCRIPTION>" . chr(13) . chr(10);


                echo $Resposta;  // envia os dados para o bradesco.
            }
        }
    }

}

?>
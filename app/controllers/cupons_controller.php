<?php

class CuponsController extends AppController {

    var $name = 'Cupons';
    var $uses = array('Cupom');
    var $components = array('Session');
    var $helpers = array('Image', 'Calendario', 'String', 'Javascript');

    function beforeFilter() {
        parent::beforeFilter();
        //retira a validação do cupom para o site;
        $this->Cupom->validaCupomSite = false;
    }

    function admin_index() {
        $this->Cupom->recursive = 0;
        $this->set('cupons', $this->paginate());
    }

    function admin_add() {

        if (!empty($this->data)) {
            $this->Cupom->set($this->data);
            if ($this->Cupom->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $this->loadModel('Categoria');
        $this->loadModel('Produto');
        $this->Categoria->displayField = 'nome';
        $categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, null, null, '----');
        if (isset($this->data['Cupom']['grade_id'])) {



            $allp = $this->Produto->find('all', array('fields' => array('Grade.sku,Produto.id', 'Produto.nome', 'Variacao.valor', 'VariacaoTipo.nome', 'VariacaoProduto.grade_id'),
                'joins' => array(
                    array(
                        'table' => 'variacoes_produtos',
                        'alias' => 'VariacaoProduto',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.produto_id = Produto.id')
                    ),
                    array(
                        'table' => 'variacoes',
                        'alias' => 'Variacao',
                        'type' => 'INNER',
                        'conditions' => array('Variacao.id = VariacaoProduto.variacao_id')
                    ),
                    array(
                        'table' => 'variacao_tipos',
                        'alias' => 'VariacaoTipo',
                        'type' => 'INNER',
                        'conditions' => array('Variacao.variacao_tipo_id = VariacaoTipo.id')
                    ),
                    array(
                        'table' => 'grades',
                        'alias' => 'Grade',
                        'type' => 'INNER',
                        'conditions' => array('Grade.id = VariacaoProduto.grade_id')
                    )
                ), 'recursive' => -1, 'limit' => '10000', 'conditions' => array('Grade.id' => $this->data['Cupom']['grade_id'])));



            $final = array();
            if ($allp) {


                $nome = '';
                $grade_anterior = null;
                foreach ($allp as $allprodutos) {
                    if ($allprodutos['VariacaoProduto']['grade_id'] != $grade_anterior) {
                        $grade_anterior = $allprodutos['VariacaoProduto']['grade_id'];
                        $nome = $allprodutos['Grade']['sku'] . ' ' . $allprodutos['Produto']['nome'] . ' #';
                    } else {
                        $nome = '';
                    }
                    
                    if(isset($final[$allprodutos['VariacaoProduto']['grade_id']])){
                        $final[$allprodutos['VariacaoProduto']['grade_id']] .= $nome . ' ' . $allprodutos['VariacaoTipo']['nome'] . ':' . $allprodutos['Variacao']['valor'] . ' ';
                    }else{
                        $final[$allprodutos['VariacaoProduto']['grade_id']] = $nome . ' ' . $allprodutos['VariacaoTipo']['nome'] . ':' . $allprodutos['Variacao']['valor'] . ' ';
                    }
                }
            }



            $grades = array('' => 'Selecione') + $final;
        } else {
            $grades = array('' => 'Selecione');
        }
        $cupomTipos = array('' => 'Selecione') + $this->Cupom->CupomTipo->find('list', array('fields' => array('id', 'nome')));
        $this->set(compact('categorias', 'grades', 'cupomTipos'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->Cupom->set($this->data);
            if ($this->Cupom->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Cupom->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->loadModel('Categoria');
        $this->loadModel('Produto');
        $this->Categoria->displayField = 'nome';
        $categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, null, null, '----');

        if (isset($this->data['Cupom']['grade_id'])) {

            $allp = $this->Produto->find('all', array('fields' => array('Grade.sku,Produto.id', 'Produto.nome', 'Variacao.valor', 'VariacaoTipo.nome', 'VariacaoProduto.grade_id'),
                'joins' => array(
                    array(
                        'table' => 'variacoes_produtos',
                        'alias' => 'VariacaoProduto',
                        'type' => 'INNER',
                        'conditions' => array('VariacaoProduto.produto_id = Produto.id')
                    ),
                    array(
                        'table' => 'variacoes',
                        'alias' => 'Variacao',
                        'type' => 'INNER',
                        'conditions' => array('Variacao.id = VariacaoProduto.variacao_id')
                    ),
                    array(
                        'table' => 'variacao_tipos',
                        'alias' => 'VariacaoTipo',
                        'type' => 'INNER',
                        'conditions' => array('Variacao.variacao_tipo_id = VariacaoTipo.id')
                    ),
                    array(
                        'table' => 'grades',
                        'alias' => 'Grade',
                        'type' => 'INNER',
                        'conditions' => array('Grade.id = VariacaoProduto.grade_id')
                    )
                ), 'recursive' => -1, 'limit' => '10000', 'conditions' => array('Grade.id' => $this->data['Cupom']['grade_id'])));

            $final = array();
            if ($allp) {


                $nome = '';
                $grade_anterior = null;
                foreach ($allp as $allprodutos) {
                    if ($allprodutos['VariacaoProduto']['grade_id'] != $grade_anterior) {
                        $grade_anterior = $allprodutos['VariacaoProduto']['grade_id'];
                        $nome = $allprodutos['Grade']['sku'] . ' ' . $allprodutos['Produto']['nome'] . ' #';
                    } else {
                        $nome = '';
                    }
                    
                    if(isset($final[$allprodutos['VariacaoProduto']['grade_id']])){
                        $final[$allprodutos['VariacaoProduto']['grade_id']] .= $nome . ' ' . $allprodutos['VariacaoTipo']['nome'] . ':' . $allprodutos['Variacao']['valor'] . ' ';
                    }else{
                        $final[$allprodutos['VariacaoProduto']['grade_id']] = $nome . ' ' . $allprodutos['VariacaoTipo']['nome'] . ':' . $allprodutos['Variacao']['valor'] . ' ';
                    }
                }
            }



            $grades = array('' => 'Selecione') + $final;
        } else {
            $grades = array('' => 'Selecione');
        }
        $cupomTipos = array('' => 'Selecione') + $this->Cupom->CupomTipo->find('list', array('fields' => array('id', 'nome')));
        $this->set(compact('categorias', 'grades', 'cupomTipos'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Cupom->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_view($idCampanha=null){
        App::import('Model','CupomCampanha');
        $this->CupomCampanha=new CupomCampanha();
        $this->set('cupom_campanha',$this->CupomCampanha->read($idCampanha));

        $this->recursive = 0;
        $this->paginate=array('conditions'=>array('cupom_campanha_id'=>$idCampanha));
        $this->set('cupons',$this->paginate());
    }


}

?>
<?php

class ImagensController extends AppController {

    var $uses = array('Imagem');
    var $components = array('Session');
    var $helpers = array('Calendario', 'String', 'Image', 'Javascript');

    function admin_add() {
        $this->layout = 'modal';

        if (!empty($this->data)) {
            $this->Imagem->create();
            if ($this->Imagem->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('admin' => true, 'action' => 'add'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $this->ProdutoImagem->recursive = 0;
        $this->set('imagens', $this->paginate());
    }

    function admin_index() {

        $this->layout = 'modal';
        $this->Imagem->recursive = 0;
        $conditions = array();
        $this->set('imagens', $this->paginate($conditions));
    }
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('admin' => true, 'action' => 'add'));
        }
        if ($this->Imagem->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('admin' => true, 'action' => 'add'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('admin' => true, 'action' => 'add'));
	}
}

?>
<?php

class UsuarioEnderecosController extends AppController {

    var $components = array('Session', 'Frete', 'Carrinho.Carrinho');
    var $helpers = array('Calendario', 'String', 'Image', 'Javascript', 'Estados');
    var $name = 'UsuarioEnderecos';

    function admin_index() {
        //se não exisir o id do cliente redireciona para a tela de usuarios.
        if (!isset($this->params['named']['user_id'])) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
        }

        $this->UsuarioEndereco->recursive = 0;
        $conditions = array('usuario_id' => $this->params['named']['user_id']);
        $this->set('usuarioEnderecos', $this->paginate($conditions));
    }

    function admin_add() {
        //se não exisir o id do cliente redireciona para a tela de usuarios.
        if (!isset($this->params['named']['user_id'])) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
        }

        if (!empty($this->data)) {
            $this->UsuarioEndereco->create();
            $this->data['UsuarioEndereco']['usuario_id'] = $this->params['named']['user_id'];
            if ($this->UsuarioEndereco->save($this->data)) {
                if ($this->data['UsuarioEndereco']['cobranca'] == true) {
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $this->data['UsuarioEndereco']['usuario_id'], 'UsuarioEndereco.id !=' => $this->UsuarioEndereco->id));
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index', 'user_id:' . $this->params['named']['user_id']));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $usuarios = $this->UsuarioEndereco->Usuario->find('list');
        $this->set(compact('usuarios'));
    }

    function admin_edit($id = null) {
        if (!isset($this->params['named']['user_id'])) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
        }
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index', 'user_id:' . $this->params['named']['user_id']));
        }
        if (!empty($this->data)) {

            $this->data['UsuarioEndereco']['usuario_id'] = $this->params['named']['user_id'];

            if ($this->UsuarioEndereco->save($this->data)) {
                if ($this->data['UsuarioEndereco']['cobranca'] == true) {
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $this->data['UsuarioEndereco']['usuario_id'], 'UsuarioEndereco.id !=' => $this->data['UsuarioEndereco']['id']));
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index', 'user_id:' . $this->params['named']['user_id']));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->UsuarioEndereco->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
        $usuarios = $this->UsuarioEndereco->Usuario->find('list');
        $this->set(compact('usuarios'));
    }

    function admin_delete($id = null) {
        if (!isset($this->params['named']['user_id'])) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('controller' => 'usuarios', 'action' => 'index'));
        }
        if (!$id) {
            $this->Session->setFlash('Paramentros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index', 'user_id:' . $this->params['named']['user_id']));
        }
        if ($this->UsuarioEndereco->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index', 'user_id:' . $this->params['named']['user_id']));
        }
        $this->Session->setFlash('Registro não foi deletado', 'flash/error');
        $this->redirect(array('action' => 'index', 'user_id:' . $this->params['named']['user_id']));
    }

    function jsonRemoveUnicodeSequences($struct) {
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($struct));
    }

    function admin_ajax_correios_endereco($cep) {
        $cep = preg_replace('/[^0-9]+/', '', $cep);
        $return = $this->jsonRemoveUnicodeSequences($this->Frete->consultaEndereco($cep));
        die($return);
    }

    function ajax_correios_endereco($cep) {
        $this->admin_ajax_correios_endereco($cep);
    }

    function add() {

        if (!$this->Auth->User()) {
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
        }
        $usuario = $this->Auth->User();

        if (!empty($this->data)) {
            $this->UsuarioEndereco->create();
            $this->data['UsuarioEndereco']['usuario_id'] = $usuario['Usuario']['id'];
            if ($this->UsuarioEndereco->save($this->data)) {
                if ($this->data['UsuarioEndereco']['cobranca'] == true) {
                    //se o endereco passado for setado como cobranca altera todos os outros para nao cobranca
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id !=' => $this->UsuarioEndereco->id));
                }else{
                    $count = $this->UsuarioEndereco->find('count',array('conditions' => array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'])));
                    if($count == 1){
                        $this->UsuarioEndereco->updateAll(array('cobranca' => '1'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id' => $this->UsuarioEndereco->id));
                    }
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        //start breadcrumb
        $breadcrumb = "";
		$breadcrumb[] = array('url' => array('controller' => 'usuarios', 'action' => 'edit'), 'nome' => "Meus Dados");
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Cadastrar Endereço");
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    function edit($id = null) {

        if (!$this->Auth->User()) {
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
        }
        $usuario = $this->Auth->User();

        $endereco_valido = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $id, 'usuario_id' => $usuario['Usuario']['id'])));

        if (!$endereco_valido)
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));

        if (!empty($this->data)) {
            $this->data['UsuarioEndereco']['usuario_id'] = $usuario['Usuario']['id'];
            $this->data['UsuarioEndereco']['id'] = $id;

            if ($this->UsuarioEndereco->save($this->data)) {
                if ($this->data['UsuarioEndereco']['cobranca'] == true) {
                    $this->UsuarioEndereco->updateAll(array('cobranca' => '0'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id !=' => $this->data['UsuarioEndereco']['id']));
                }else{
                    $count = $this->UsuarioEndereco->find('count',array('conditions' => array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.cobranca' => 1)));
                    if($count == 0){
                        $this->UsuarioEndereco->updateAll(array('cobranca' => '1'), array('UsuarioEndereco.usuario_id' => $usuario['Usuario']['id'], 'UsuarioEndereco.id' => $this->UsuarioEndereco->id));
                    }
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->UsuarioEndereco->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
            }
        }
        // $this->set('breadcrumbs',array(array('nome'=>'Endereços','link'=>'/usuarios/edit'),array('nome'=>'Editar Endereço','link'=>'/usuario_enderecos/edit/'.$id)));
        //start breadcrumb
        $breadcrumb = "";
		$breadcrumb[] = array('url' => array('controller' => 'usuarios', 'action' => 'edit'), 'nome' => "Meus Dados");
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Editar Endereço");
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    function delete($id = null) {
        $this->render(false);
        $this->layout = false;

        if (!$this->Auth->User()) {
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
        }
        $usuario = $this->Auth->User();
        $endereco_valido = $this->UsuarioEndereco->find('first', array('conditions' => array('UsuarioEndereco.id' => $id, 'usuario_id' => $usuario['Usuario']['id'])));

        if (!$endereco_valido)
            $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));

        if ($this->UsuarioEndereco->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            if ($this->Carrinho->getStep() >= 1 && $this->referer() != "/usuarios/edit") {
                $this->redirect(array('plugin' => 'carrinho', 'controller' => 'carrinho', 'action' => 'entrega'));
            } else {
                $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
            }
        }
        $this->Session->setFlash('Registro não foi deletado', 'flash/error');
        $this->redirect(array('controller' => 'usuarios', 'action' => 'edit'));
    }

	/**
     * Obtem os enderecos do usuario via ajax
     *
     * @access public
     * @return json
     */
	public function get_endereco_usuario(){
		//usuario id atual
		$usuario_id = $this->Auth->user('id');
		
		//obtem os enderecos do usuario
		$enderecos = $this->UsuarioEndereco->getEnderecosUsuario($usuario_id);
		
		//config
		Configure::write('debug',0);
		$this->autoRender = false;
		$this->layout = 'ajax';
        $this->header('Content-type: application/json');
		
		//return
		return json_encode($enderecos);
	}
}

?>
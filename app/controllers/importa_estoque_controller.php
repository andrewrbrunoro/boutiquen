<?php

    class ImportaEstoqueController extends AppController
    {
        var $uses = array();

        public function importacao()
        {
            App::import('Component', 'Importacao');
            new ImportacaoComponent('full');
        }

        public function estoque()
        {
            App::import('Component', 'Importacao');
            new ImportacaoComponent('stock', false);
        }

    }
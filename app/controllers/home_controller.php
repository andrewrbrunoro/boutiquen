<?php

    class HomeController extends AppController
    {

        public $uses = array(
            'Vitrine',
            'Fabricante',
            'Produto',
            'Banner'
        );
        public $components = array('Email');

        function index()
        {
            $this->set('banners_home_top', $this->Banner->getBanners());
            $this->set('showcases', $this->Vitrine->getAllShowcases());
            $this->set('manufacturers', $this->Fabricante->getManufacturers());
        }


        function newsletter()
        {
            //import model
            App::import("Model", "Newsletter");
            $this->Newsletter = new Newsletter();
            //define debug como 0 para retornar o json
            Configure::write('debug', 0);
            if ($this->Newsletter->save($this->data)) {

                //importamos o helper html para o redirecionamento
                App::import('Helper', 'Html');
                $html = new HtmlHelper();

                if ($this->data['Vitrine']['barra'] == 1) {
                    $this->loadModel('Cupom');
                    $novoCupom = array();
                    $novoCupom['Cupom']['id'] = null;
                    $novoCupom['Cupom']['status'] = 1;
                    $novoCupom['Cupom']['usuario_unico'] = 1;
                    $novoCupom['Cupom']['nome'] = $this->data['Newsletter']['nome'];
                    $novoCupom['Cupom']['cupom'] = time() . '-' . rand(1, 99);
                    $novoCupom['Cupom']['pct_desconto'] = 10;
                    //$novoCupom['Cupom']['vlr_desconto']='50,00';
                    $novoCupom['Cupom']['data_fim'] = '31/01/2050';
                    $novoCupom['Cupom']['max_usos'] = 1;
                    $novoCupom['Cupom']['cupom_tipo_id'] = 2;
                    $novoCupom['Cupom']['compra_minima'] = '0,00';
                    $novoCupom['Cupom']['cupom_campanha_id'] = 1;
                    $novoCupom['Cupom']['newsletters_id'] = $this->Newsletter->id;
                    if ($this->Cupom->save($novoCupom, false)) {
                        $this->data['Cupom']['cupom_do_usuario'] = $novoCupom['Cupom']['cupom'];
                    }
                    $this->enviaEmailNewsletterBarraRodape($this->data);

                    $this->Session->write('newsletter', '1');
                }

                //redirecionamos o usuario quando ocorrer sucesso.
                die(json_encode(array('success' => $html->url("/", true))));
            } else {
                //retorna os campos inv�lidos
                die(json_encode(array('error' => $this->Newsletter->invalidFields())));
            }
            $this->autoRender = false;
            exit();
        }

        private function enviaEmailNewsletterBarraRodape($dados)
        {
            Configure::write('debug', 0);

            if (Configure::read('Loja.smtp_host') != 'localhost') {
                $this->Email->smtpOptions = array(
                    'port'     => (int)Configure::read('Loja.smtp_port'),
                    'host'     => Configure::read('Loja.smtp_host'),
                    'username' => Configure::read('Loja.smtp_user'),
                    'password' => Configure::read('Loja.smtp_password')
                );
                $this->Email->delivery = 'smtp';
            }

            $this->Email->delivery = 'smtp';
            $this->Email->lineLength = 120;
            $this->Email->sendAs = 'html';
            //$this->Email->replyTo  = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
            $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
            $this->Email->to = "{$dados['Newsletter']['nome']} <{$dados['Newsletter']['email']}>";
            $this->Email->subject = "Cupom de desconto - " . Configure::read('Loja.nome');

            $email = str_replace(array(
                '{CODIGO}'
            ), array(
                $dados['Cupom']['cupom_do_usuario']
            ), Configure::read('LojaTemplate.newsletter_barra'));

            /*
            if (Configure::read('Reweb.nome_bcc')) {
                $bccs = array();
                $assunto = Configure::read('Reweb.nome_bcc');
                $emails = Configure::read('Reweb.email_bcc');
                foreach (explode(';', $emails) as $bcc) {
                    if($bcc != ""){
                        $bccs[] = "{$assunto} <{$bcc}>";
                    }
                }
                $this->Email->bcc = $bccs;
            }*/

            if ($this->Email->send($email)) {
                return true;
            } else {
                $this->log('DEBUG\r\nErro no envio do email do cadastro de news do rodape\r\n', LOG_DEBUG);
                return false;
            }
        }
    }
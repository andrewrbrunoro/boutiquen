<?php

    class UsuariosController extends AppController
    {

        public $uses = array(
            'Usuario',
            'UsuarioEndereco'
        );

        var $components = array(
            'RequestHandler'
        );

        public function beforeFilter()
        {
            parent::beforeFilter();
            $this->Auth->allow(array('ativaEndereco'));
        }

        function login()
        {
            if ($this->Auth->user())
                $this->redirect('/');
        }

        public function logout()
        {
            if ($this->Auth->logout()) {
                $this->Session->setFlash('Agracemos sua visita, volte sempre!', 'flash/success');
            } else {
                $this->Session->setFlash('Ops, não consigo desconectar você. Tente novamente!', 'flash/error');
            }
            $this->redirect('/');
        }

        public function add()
        {
            if (!$this->Auth->user()) {
                if (strstr($this->referer(), '/usuarios/add')) {
                    /*Dados customizados para inserção do usuário*/
                    $this->data['Usuario']['grupo_id'] = 2;
                    $this->data['Usuario']['status'] = true;
                    $this->data['Usuario']['data_nascimento'] = $this->data['Usuario']['ano'] . '-' . $this->data['Usuario']['mes'] . '-' . $this->data['Usuario']['dia'];
                    $this->data['Usuario']['activation_key'] = md5(uniqid());
                    $this->data['Usuario']['senha'] = Security::hash($this->data['Usuario']['senha_nova'], null, true);
                    /*Dados customizados para inserção do usuário*/
                    if ($this->Usuario->save(array('Usuario' => $this->data['Usuario']))) {
                        /*Dados customizados para inserção do endereço*/
                        $this->data['UsuarioEndereco']['usuario_id'] = $this->Usuario->getLastInsertId();
                        $this->data['UsuarioEndereco']['nome'] = $this->data['UsuarioEndereco']['rua'];
                        /*Dados customizados para inserção do endereço*/
                        if ($this->UsuarioEndereco->save(array('UsuarioEndereco' => $this->data['UsuarioEndereco']))) {
                            $this->Session->setFlash('Obrigado, seu registro foi efetuado com sucesso.', 'flash/success');
                            $this->Auth->login(array('Usuario' => array('email' => $this->data['Usuario']['email'], 'senha' => $this->data['Usuario']['senha'])));
                            $this->redirect('/usuarios/edit');
                        } else {
                            $this->Usuario->delete($this->Usuario->getLastInsertId());
                        }
                    } else {
                        $this->Session->setFlash('Ops, verifique se todos os dados foram preenchidos corretamente.', 'flash/error');
                    }
                }
            } else {
                $this->redirect('/usuarios/edit');
            }
        }


        public function edit()
        {
            if ($this->Auth->user()) {
                if ($this->RequestHandler->isPut()) {
                    $data = $this->data;
                    if (empty($data['Usuario']['senha_nova'])) {
                        unset($data['Usuario']['senha_nova'], $data['Usuario']['confirm']);
                    } else {
                        $data['Usuario']['senha'] = Security::hash($this->data['Usuario']['senha_nova'], null, true);
                    }
                    $data['Usuario']['data_nascimento'] = $this->data['Usuario']['ano'] . '-' . $this->data['Usuario']['mes'] . '-' . $this->data['Usuario']['dia'];
                    $data['Usuario']['id'] = $this->Auth->user('id');
                    $rollback = $this->Auth->user();
                    if ($this->Usuario->save(array('Usuario' => $data['Usuario']))) {
                        if (!empty($data['UsuarioEndereco']['cep'])) {
                            $data['UsuarioEndereco']['usuario_id'] = $this->Auth->user('id');
                            $data['UsuarioEndereco']['nome'] = $this->data['UsuarioEndereco']['rua'];
                            if ($this->UsuarioEndereco->save(array('UsuarioEndereco' => $data['UsuarioEndereco']))) {
                                $this->Session->setFlash('As alterações foram efetuadas com sucesso.', 'flash/success');
                                $this->redirect('/usuarios/edit');
                            } else {
                                $this->Usuario->save($rollback);
                            }
                        } else {
                            $this->Session->setFlash('As alterações foram efetuadas com sucesso.', 'flash/success');
                            $this->redirect('/usuarios/edit');
                        }
                    } else {
                        $this->Session->setFlash('Ops, verifique se todos os dados foram preenchidos corretamente.', 'flash/error');
                    }
                }
                $this->data = $this->Usuario->getUsuarioEndereco($this->Auth->user('id'));
            } else {
                $this->redirect($this->referer());
            }
        }

        public function ativaEndereco($id, $status)
        {
            if ($this->UsuarioEndereco->isMyAddress($id, $this->Auth->user('id'))) {
                $endereco['UsuarioEndereco'] = array(
                    'id'         => $id,
                    'status'     => (int)!$status
                );
                if ($this->UsuarioEndereco->save($endereco)) {
                    $return = (!$status) ? 'ativado' : 'inativado';
                    $this->Session->setFlash('O endereço foi '.$return.' com sucesso.', 'flash/success');
                } else {
                    $this->Session->setFlash('Ops, falha ao altear os status do endereço, tente novamente.', 'flash/error');
                }
            } else {
                $this->Session->setFlash('Ops, houve um erro inesperado. Tente novamente.', 'flash/error');
            }
            $this->redirect($this->referer());
        }

    }


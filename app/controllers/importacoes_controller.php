<?php

class ImportacoesController extends AppController {
	
	
	function index(){
		App::import('Model', 'Produto');
		$this->Produto = new Produto();
		
		App::import('Model', 'Grade');
		$this->Grade = new Grade();
		
		$this->layout = false;
		$this->render(false);
		ini_set('display_errors', 'On');
		error_reporting(E_ALL);
		ini_set('max_execution_time', 0);
		ini_set('default_socket_timeout', 3600);
		//ini_set('memory_limit', '1024M');
		set_time_limit(0);
		$handle = fopen('/home/storage/9/c3/8b/coliseu2/Coliseu/est_coliseu_'.(date('Ymd')).'.txt', "r");
		//$handle = fopen('/home/luiz/est_coliseu_'.(date('Ymd')).'.txt', "r");
		//echo $_SERVER['DOCUMENT_ROOT'];
		//die;
		//$handle = fopen($_SERVER['DOCUMENT_ROOT'].'/Coliseu/est_coliseu_'.(date('Ymd')).'.txt', "r");
		$first = true;
		$success = 0;
		$error = 0	;
		while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
			if ($first) {
				$first = false;
				continue;
			}
			$produto['codigo'] = iconv('ISO-8859-1', 'UTF-8', $data[0]);
			$produto['preco'] = (float)(($data[1]));
			if(($produto['codigo'][0]=='O' && $produto['codigo'][1]=='C') || ($produto['codigo'][0]=='A' && $produto['codigo'][1]=='R')){
                if ($produto['codigo'][0]=='O' && $produto['codigo'][1]=='C') {
                    $product = $this->Produto->find('first',array(
                        'recursive' => -1,
                        'conditions' => array('codigo' => $produto['codigo']),
                        'fields' => array('codigo_externo'),
                    ));
                    if (strpos($product['Produto']['codigo_externo'], 'rb') === 0) {
                        $produto['preco'] *= 1.045;
                    } else {
                        $produto['preco'] *= 1.03;
                    }
                } elseif (($produto['codigo'][0]=='A' && $produto['codigo'][1]=='R')) {
                    $product = $this->Produto->find('first',array(
                        'recursive' => -1,
                        'conditions' => array('codigo' => $produto['codigo']),
                        'fields' => array('codigo_externo'),
                    ));
                    if (strpos($product['Produto']['codigo_externo'], 'rx') === 0) {
                        $produto['preco'] *= 1.04;
                    } else {
                        $produto['preco'] *= 1.025;
                    }
                }

				$produto['preco_promocao'] = (float)(0.0);
			}else{
				$produto['preco_promocao'] = (float)(($data[1])*0.9);
			}
			$produto['quantidade'] = (int)trim($data[2]);
			
			//Desconto agora ta sendo feito apenas a vista pelo sistema.
			$produto['preco_promocao'] = (float)(0.0);
			
			$grade_id = null;
			$produto['quantidade_disponivel'] = $produto['quantidade'];
			$produto_id = null;
			
			$sql = "SELECT id, quantidade_alocada FROM grades WHERE sku = '" . $produto['codigo'] . "'";
			$result = mysql_query($sql);
			if(mysql_num_rows($result) > 0){
				$prd = mysql_fetch_row($result);
				$grade_id = $prd[0];
				$produto['quantidade_disponivel'] = $produto['quantidade'] - ((int)$prd[1]);
			}
			
			$sql = "SELECT id FROM produtos WHERE sku = '" . $produto['codigo'] . "'";
			$result2 = mysql_query($sql);
			if(mysql_num_rows($result2) > 0){
				$prd = mysql_fetch_row($result2);
				$produto_id = $prd[0];
			}
			
			/**/
			$query = "UPDATE grades SET preco='".$produto['preco']."', preco_promocao='".$produto['preco_promocao']."', quantidade='".$produto['quantidade']."', quantidade_disponivel='".$produto['quantidade_disponivel']."' WHERE sku = '" . ($produto['codigo']) . "' ";
			/**/
			
			if ($this->Grade->query($query)) {
				$lastid = mysql_insert_id();
				if(!empty($produto_id) && $produto_id>0){
					$this->Produto->atualiza_grade_produto($produto_id);
				}
				$success++;
			} else {
				$error++;
			}
		}
		if ($success > 0 && $error <= 0) {
			echo 'Todos os registros ('.$success.' itens) foram importados com sucesso! ';
			$this->log('Todos os registros foram importados com sucesso!', LOG_DEBUG);
		} elseif ($success > 0 && $error > 0) {
			echo 'Alguns registros não foram importados ('.$error.' erros)!';
			$this->log('Alguns registros não foram importados ('.$error.' erros)!', LOG_DEBUG);
		}
	}
	

	function atualiza_grades_produtos(){
		App::import('Model','Produto');
		$this->Produto = new Produto();
		
		$produtos = $this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.id')));
		
		$cont = 1;
		foreach($produtos as $produto){
			$this->Produto->atualiza_grade_produto($produto['Produto']['id']);
			$cont++;
		}
		
		die('Produtos Atualizados: '.$cont);
		
	}

	function admin_index($action) {

		switch ($action) {
			case 'estoque':
				$this->estoque();
				break;
			
			case'produtos':
				$this->produtos();
				break;

			case'produtos_pai':
				$this->produtosPai();
				break;     
		}
	}

}
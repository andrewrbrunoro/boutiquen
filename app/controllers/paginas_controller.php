<?php

    class PaginasController extends AppController
    {

        function pagina_404()
        {

        }

        function view($alias = null)
        {
            $file = APP.'views'.DS.'paginas'.DS.$alias.'.ctp';
            if (file_exists($file)) {
                $this->set('view', $file);
            } else {
                $this->redirect('/intl/pagina_404');
            }
        }

    }
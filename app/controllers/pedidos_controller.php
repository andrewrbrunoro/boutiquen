<?php

    class PedidosController extends AppController
    {

        var $components = array(
            'BpagPagamento',
            'PagSeguro.PagSeguro',
            'RequestHandler',
            'Session',
            'Email',
            'Filter',
            'Cielo',
            'Clearsale'
        );
        var $name = 'Pedidos';
        var $helpers = array(
            'Calendario',
            'Javascript',
            'String',
            'Image',
            'Flash'
        );

        public function __construct()
        {
            parent::__construct();
            App::import('Component', 'OrderTxt');
        }

        function admin_index()
        {
            $this->Pedido->recursive = 0;
            //importantdo libs
            App::import('Helper', 'Calendario');
            $this->Calendario = new CalendarioHelper();
            //options status do pedido
            $pedido_status = $this->Pedido->PedidoStatus->find('list', array(
                'fields' => array(
                    'id',
                    'nome'
                )
            ));
            $condicoes = $this->Pedido->PagamentoCondicao->find('list', array(
                'fields'     => array(
                    'id',
                    'nome'
                ),
                'conditions' => array('PagamentoCondicao.status' => true)
            ));
            $this->set(compact('pedido_status', 'condicoes'));

            if (isset($this->params['named']['Pedido.produto_id'])) {
                $this->data['Filter']['produto_id'] = $this->params['named']['Pedido.produto_id'];
            }
            if (isset($this->params['named']['Pedido.sku'])) {
                $this->data['Filter']['sku'] = $this->params['named']['Pedido.sku'];
            }
            $filtros = array();
            //filtros
            if (isset($this->data["Filter"]["pedido_status_id"])) {
                $aStatus = $this->data["Filter"]["pedido_status_id"];
                if (is_array($this->data["Filter"]["pedido_status_id"])) {
                    $this->data["Filter"]["pedido_status_id"] = implode(',', $this->data["Filter"]["pedido_status_id"]);
                }
                $filtros['pedido_status_id'] = "Pedido.pedido_status_id IN({%value%})";
            }
            if (isset($this->data["Filter"]["id"])) {
                $filtros['id'] = "Pedido.id = '{%value%}' OR Usuario.nome LIKE '%{%value%}%' ";
            }
            if (isset($this->data["Filter"]["midia"])) {
                $filtros['midia'] = "Pedido.campanha LIKE '%{%value%}%' ";
            }
            if (isset($this->data["Filter"]["cupom"])) {
                $filtros['cupom'] = "cupom_queimados.cupom = '{%value%}' ";
            }
            if (isset($this->data["Filter"]["produto_id"])) {
                $filtros['produto_id'] = "PedidoItem.produto_id = '{%value%}'";
            }
            if (isset($this->data["Filter"]["sku"])) {
                $filtros['sku'] = "PedidoItem.sku = '{%value%}'";
            }
            if (isset($this->data["Filter"]["pagamento_condicao_id"])) {
                $aPagCond = $this->data["Filter"]["pagamento_condicao_id"];
                if (is_array($this->data["Filter"]["pagamento_condicao_id"])) {
                    $this->data["Filter"]["pagamento_condicao_id"] = implode(',', $this->data["Filter"]["pagamento_condicao_id"]);
                }
                $filtros['pagamento_condicao_id'] = "Pedido.pagamento_condicao_id IN({%value%})";
            }
            if (isset($this->data["Filter"]["pedido"])) {
                $filtros['pedido'] = "Pedido.id = '{%value%}' OR pag_seguro_id = '{%value%}'";
            }
            if (isset($this->data["Filter"]["nome"])) {
                $filtros['nome'] = "Pedido.usuario_nome like '%{%value%}%' OR usuario_razao_social LIKE '%{%value%}%'";
            }
            if (isset($this->data["Filter"]["email"])) {
                $filtros['email'] = "Pedido.usuario_email = '{%value%}'";
            }
            if (isset($this->data["Filter"]["cpf"])) {
                $filtros['cpf'] = "Pedido.usuario_cpf = '{%value%}'";
            }
            if (isset($this->data["Filter"]["cnpj"])) {
                $filtros['cnpj'] = "Pedido.usuario_cnpj = '{%value%}'";
            }
            if (isset($this->data["Filter"]["midia"])) {
                $filtros['midia'] = "Pedido.campanha like '%{%value%}%'";
            }
            if (isset($this->data["Filter"]["data_ini"]) && isset($this->data["Filter"]["data_fim"])) {
                if (($this->Calendario->DataDiff($this->data["Filter"]["data_ini"], $this->data["Filter"]["data_fim"]) / 86400) <= 90) {
                    $intervalo_valido = true;
                } else {
                    $intervalo_valido = false;
                    $this->Session->setFlash('O intervalo do filtro não pode ser superior a 3 meses.', 'flash/error');
                }
            }

            //se tiver o campo filtra
            if (isset($this->data["Filter"]["data_ini"]) && $intervalo_valido == true) {
                $filtros['data_ini'] = "DATE_FORMAT(Pedido.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", $this->data["Filter"]["data_ini"]) . "'";
            } else {
                $filtros['data_ini'] = "DATE_FORMAT(Pedido.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')))) . "'";
            }
            //se tiver o campo filtra
            if (isset($this->data["Filter"]["data_fim"]) && $intervalo_valido == true) {
                $filtros['data_fim'] = "DATE_FORMAT(Pedido.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", $this->data["Filter"]["data_fim"]) . "'";
            } else {
                $filtros['data_fim'] = "DATE_FORMAT(Pedido.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y', mktime(0, 0, 0, date('m'), date('d'), date('Y')))) . "'";
            }

            $join[] = array(
                'table'      => 'cupom_queimados',
                'type'       => 'LEFT',
                'alias'      => 'cupom_queimados',
                'conditions' => array('Pedido.id = cupom_queimados.pedido_id')
            );

            $this->Filter->setConditions($filtros);
            $this->Filter->check();
            $conditions = $this->Filter->getFilters();
            $this->Filter->setDataToView();

            if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
                $this->admin_exportar($conditions);
            }

            if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Relatório") {
                $this->admin_exportar_relatorio($conditions);
            }
            if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar Listagem de Pedidos") {
                $this->admin_exportar_listagem_pedidos();
            }

            $filtros_usados = $this->Filter->getStoredData();
            if (isset($filtros_usados['produto_id']) || isset($filtros_usados['sku'])) {
                $join[] = array(
                    'table'      => 'pedido_itens',
                    'type'       => 'LEFT',
                    'alias'      => 'PedidoItem',
                    'conditions' => array('Pedido.id = PedidoItem.pedido_id')
                );
            }
            // if(!$conditions){
            // $conditions = array('AND'=>array("DATE_FORMAT(Pedido.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y',mktime(0,0,0,date('m')-1,date('d'),date('Y')))) . "'","DATE_FORMAT(Pedido.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y',mktime(0,0,0,date('m'),date('d'),date('Y')))) . "'"));
            // }
            $this->set('pedidostotais', $this->Pedido->find('all', array(
                'fields'     => array('distinct Pedido.id,Pedido.*'),
                'joins'      => $join,
                'conditions' => $conditions
            )));
            $this->set('pedidostatus', $this->Pedido->PedidoStatus->find('all', array('recursive' => -1)));
            //paginacao
            $this->paginate = array(
                'joins'   => $join,
                'contain' => array(
                    'Usuario',
                    'PedidoStatus',
                    'PagamentoCondicao'
                ),
                'limit'   => 60,
                'order'   => array('Pedido.id' => 'DESC')
            );
            $this->set('pedidos', $this->paginate('Pedido', $conditions));
        }

        public function admin_downloadOrder($id)
        {
            $this->layout = false;
            $this->render(false);
            $order_txt = new OrderTxtComponent($id);
            set_time_limit(0);
            header('Content-type: text/plain');
            header('Content-Disposition: attachment; filename=' . $order_txt->filename);
            header('Pragma: no-cache');
            header('Expires: 0');
            die($order_txt->string);
        }

        function admin_exportar_listagem_pedidos()
        {

            $this->layout = false;
            $this->render(false);
            set_time_limit(0);
            header('Content-type: application/x-msexcel');
            $filename = "pedidos_rel" . date("d_m_Y_H_i_s");
            header('Content-Disposition: attachment; filename=' . $filename . '.xls');
            header('Pragma: no-cache');
            header('Expires: 0');

            $sql = "SELECT
                    ped.id, ped.created,
                    ped.valor_frete, ped.valor_pedido, ped.entrega_tipo, ped.entrega_prazo,
                    ped.parcelas, ped.endereco_cidade, ped.endereco_estado,
                    sta.nome AS situacao, pedit.codigo, pedit.quantidade,
                    pedit.preco, ped.usuario_id, usuarios.nome,
                    prod.nome
                FROM
                    pedidos AS ped,
                    pedido_status AS sta,
                    pedido_itens AS pedit,
                    usuarios,
                    produtos AS prod
                WHERE
                    sta.id = ped.pedido_status_id
                    AND pedit.pedido_id = ped.id
                    AND ped.usuario_id = usuarios.id
                    AND prod.id = pedit.produto_id
                ORDER BY ped.id DESC, pedit.codigo DESC";
            $rows = $this->Pedido->query($sql);

            $table = "<table>";
            $table .= "
                <tr bgcolor=\"#CECECE\">
                    <td><strong>Pedido Id</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Data Criação") . "</strong></td>
                    <td><strong>Valor do Frete</strong></td>
                    <td><strong>Valor do Pedido</strong></td>
                    <td><strong>Tipo de Entrega</strong></td>
                    <td><strong>Prazo de Entrega</strong></td>
                    <td><strong>Parcelas</strong></td>
                    <td><strong>Cidade</strong></td>
                    <td><strong>Estado</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Situação") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Código do Produto") . "</strong></td>
                    <td><strong>Quantidade do Produto</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Preço do Produto") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Usuário") . "</strong></td>
                    <td><strong>Produto</strong></td>
                </tr>";
            foreach ($rows as $row) {
                $table .= "
                <tr>
                    <td>" . $row['ped']['id'] . "</td>
                    <td>" . $row['ped']['created'] . "</td>
                    <td>" . $row['ped']['valor_frete'] . "</td>
                    <td>" . $row['ped']['valor_pedido'] . "</td>
                    <td>" . $row['ped']['entrega_tipo'] . "</td>
                    <td>" . $row['ped']['entrega_prazo'] . "</td>
                    <td>" . $row['ped']['parcelas'] . "</td>
                    <td>" . $row['ped']['endereco_cidade'] . "</td>
                    <td>" . $row['ped']['endereco_estado'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['sta']['situacao']) . "</td>
                    <td>" . $row['pedit']['codigo'] . "</td>
                    <td>" . $row['pedit']['quantidade'] . "</td>
                    <td>" . $row['pedit']['preco'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['usuarios']['nome']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['prod']['nome']) . "</td>
                </tr>";
            }
            $table .= "</table>";

            die($table);
        }

        function admin_exportar($conditions)
        {

            $this->layout = false;
            $this->render(false);
            set_time_limit(0);
            header('Content-type: application/x-msexcel');
            $filename = "pedidos_" . date("d_m_Y_H_i_s");
            header('Content-Disposition: attachment; filename=' . $filename . '.xls');
            header('Pragma: no-cache');
            header('Expires: 0');

            $join[] = array(
                'table'      => 'cupom_queimados',
                'type'       => 'LEFT',
                'alias'      => 'cupom_queimados',
                'conditions' => array('Pedido.id = cupom_queimados.pedido_id')
            );
            $join[] = array(
                'table'      => 'pedido_itens',
                'type'       => 'LEFT',
                'alias'      => 'PedidoItem',
                'conditions' => array('Pedido.id = PedidoItem.pedido_id')
            );

            $rows = $this->Pedido->find("all", array(
                'conditions' => $conditions,
                'joins'      => $join,
                'order'      => array('Pedido.id' => 'DESC'),
                'contain'    => array(
                    'Usuario',
                    'PedidoStatus',
                    'PagamentoCondicao',
                    'PedidoItem'
                )
            ));

            $table = "<table>";
            foreach ($rows as $row) {
                $table .= "<tr bgcolor=\"#CECECE\">
                    <td><strong>Pedido</strong></td>
                    <td><strong>Valor Frete</strong></td>
                    <td><strong>Valor Pedido</strong></td>
                    <td><strong>Valor Desconto Cupom</strong></td>
                    <td><strong>Valor Desconto Meio Pagamento</strong></td>
                    <td><strong>Valor Juros Meio Pagamento</strong></td>
                    <td><strong>Tipo de Entrega</strong></td>
                    <td><strong>Prazo de Entrega</strong></td>
                    <td><strong>Parcelas</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço CEP") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço Rua") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço Número") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço Complemento") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço Bairro") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço Cidade") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Endereço Estado") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Data Criação") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Condição de Pagamento") . "</strong></td>
                    <td><strong>Status do Pedido</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", "Usuário") . "</strong></td>
                </tr>
                <tr>
                    <td>" . $row['Pedido']['id'] . "</td>
                    <td>" . $row['Pedido']['valor_frete'] . "</td>
                    <td>" . $row['Pedido']['valor_pedido'] . "</td>
                    <td>" . $row['Pedido']['valor_desconto_cupom'] . "</td>
                    <td>" . $row['Pedido']['valor_desconto_meio_pagamento'] . "</td>
                    <td>" . $row['Pedido']['valor_juros_meio_pagamento'] . "</td>
                    <td>" . $row['Pedido']['entrega_tipo'] . "</td>
                    <td>" . $row['Pedido']['entrega_prazo'] . "</td>
                    <td>" . $row['Pedido']['parcelas'] . "</td>
                    <td>" . $row['Pedido']['endereco_cep'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['Pedido']['endereco_rua']) . "</td>
                    <td>" . $row['Pedido']['endereco_numero'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['Pedido']['endereco_complemento']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['Pedido']['endereco_bairro']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['Pedido']['endereco_cidade']) . "</td>
                    <td>" . $row['Pedido']['endereco_estado'] . "</td>
                    <td>" . $row['Pedido']['created'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['PagamentoCondicao']['nome']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['PedidoStatus']['nome']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['Usuario']['nome']) . "</td>
                </tr>";
                $table .= iconv("UTF-8", "ISO-8859-1//TRANSLIT", "
            <tr bgcolor=\"#EAE8E3\">
                <td><strong>Item</strong></td>
                <td><strong>Nome</strong></td>
                <td><strong>Preço</strong></td>
                <td><strong>Grade</strong></td>
            </tr>");
                foreach ($row['PedidoItem'] as $row) {
                    $table .= "<tr>";
                    $table .= "<td>" . $row['id'] . "</td>";
                    $table .= "<td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $row['nome']) . "</td>";
                    $preco = ($row['preco_promocao'] > 0 || $row['gratis'] === true) ? $row['preco_promocao'] : $row['preco'];
                    $table .= "<td>" . $preco . "</td>";
                    $variacoes = json_decode($row['variacoes'], true);
                    $grade = '';
                    foreach ($variacoes as $key => $variacao)
                        $grade .= "{$key}: {$variacao} | ";
                    $table .= "<td>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", trim($grade, '| ')) . "</td>";
                    $table .= "</tr>";
                }
                $table .= "<tr><td></td></tr>";
            }
            $table .= "</table>";
            die($table);
        }

        function admin_exportar_relatorio($conditions)
        {

            $this->layout = false;
            $this->render(false);
            set_time_limit(0);
            header('Content-type: application/x-msexcel');
            $filename = "pedidos_" . date("d_m_Y_H_i_s");
            header('Content-Disposition: attachment; filename=' . $filename . '.xls');
            header('Pragma: no-cache');
            header('Expires: 0');

            $join[] = array(
                'table'      => 'cupom_queimados',
                'type'       => 'LEFT',
                'alias'      => 'cupom_queimados',
                'conditions' => array('Pedido.id = cupom_queimados.pedido_id')
            );
            $join[] = array(
                'table'      => 'pedido_itens',
                'type'       => 'LEFT',
                'alias'      => 'PedidoItem',
                'conditions' => array('Pedido.id = PedidoItem.pedido_id')
            );

            $rows = $this->Pedido->find("all", array(
                'conditions' => $conditions,
                'joins'      => $join,
                'order'      => array('Pedido.id' => 'DESC'),
                'group'      => array('Pedido.id'),
                'contain'    => array(
                    'Usuario',
                    'PedidoStatus',
                    'PagamentoCondicao',
                    'PedidoItem'
                )
            ));

            $table = "<table>";
            $table .= "<tr bgcolor=\"#CECECE\">
                    <td><strong>Pedido</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Data") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Cliente") . "</strong></td>
                    <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Email") . "</strong></td>
                    <td><strong>Valor Pedido</strong></td>
                    <td><strong>Valor Frete Cobrado</strong></td>
                    <td><strong>Valor Frete Previsto</strong></td>
                    <td><strong>Status</strong></td>
                    <td><strong>Midia</strong></td>
                </tr>";
            foreach ($rows as $row) {
                $campanha = json_decode($row['Pedido']['campanha'], true);
                $row['Pedido']['campanha'] = !empty($campanha['midia']) ? $campanha['midia'] : '---';
                $table .= "<tr>
                    <td>" . $row['Pedido']['id'] . "</td>
                    <td>" . $row['Pedido']['created'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Usuario']['nome']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Usuario']['email']) . "</td>
                    <td>" . $row['Pedido']['valor_pedido'] . "</td>
                    <td>" . $row['Pedido']['valor_frete'] . "</td>
                    <td>" . $row['Pedido']['frete_antigo'] . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['PedidoStatus']['nome']) . "</td>
                    <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Pedido']['campanha']) . "</td>
                </tr>";
            }
            $table .= "</table>";

            die($table);
        }

        function admin_view($id = null)
        {
            if (!$id) {
                $this->Session->setFlash('Parametros inválidos', 'flash/error');
                $this->redirect(array('action' => 'index'));
            }

            $dados = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => $id)));
            if ($dados['CupomQueimado']['cupom_id']) {
                $this->loadModel('Cupom');
                $cupom = $this->Cupom->find('first', array('conditions' => array('Cupom.id' => $dados['CupomQueimado']['cupom_id'])));
                $dados['CupomQueimado']['Cupom'] = $cupom['Cupom'];
                $dados['CupomQueimado']['CupomTipo'] = $cupom['CupomTipo'];
            }
            $this->set('pedido', $dados);

            $pedido_status = $this->Pedido->PedidoStatus->find('list', array(
                'fields' => array(
                    'id',
                    'nome'
                )
            ));
            $this->set(compact('pedido_status'));
            $this->data = $dados;

            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
            $this->set('clearsale', $this->Clearsale->run($dados));
        }

        function admin_delete($id = null)
        {
            if (!$id) {
                $this->Session->setFlash('Parametros inválidos', 'flash/error');
                $this->redirect(array('action' => 'index'));
            }
            if ($this->Pedido->delete($id)) {
                $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
                $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        public function admin_edit($pedido_id)
        {
            if (!empty($this->data)) {
                $this->layout = false;
                $this->render(false);
                //só altera o status quando o mesmo ainda não foi finalizado
                if (isset($this->data['Pedido']['pedido_status_id'])) {
                    //load do pedido
                    $this->Pedido->id = $pedido_id;
                    $this->Pedido->saveField('pedido_status_id', $this->data['Pedido']['pedido_status_id']);
                    $this->data = $this->Pedido->read(null, $pedido_id);
                    $status = $this->Pedido->PedidoStatus->find('first', array('conditions' => array('PedidoStatus.id' => $this->data['Pedido']['pedido_status_id'])));

                    //log
                    $save['PedidoHistoricoStatus']['id'] = null;
                    $save['PedidoHistoricoStatus']['pedido_status_id'] = $this->data['Pedido']['pedido_status_id'];
                    $save['PedidoHistoricoStatus']['pedido_id'] = $pedido_id;
                    $save['PedidoHistoricoStatus']['status'] = $status['PedidoStatus']['nome'];
                    $this->Pedido->PedidoHistoricoStatus->save($save);

                    //se o pedido já finalizou
                    App::import('model', 'Produto');
                    $this->Produto = new Produto();
                    App::import('model', 'PedidoItem');
                    $this->PedidoItem = new PedidoItem();
                    App::import('Model', 'Grade');
                    $this->Grade = new Grade();

                    //só desaloca quando o pedido não foi finalizado
                    if ($this->data['PedidoStatus']['desalocar_produtos'] && !$this->data['Pedido']['finalizado']) {
                        //seta pedido como finalizado
                        $this->Pedido->saveField('finalizado', true);
                        if ($this->data['PagamentoCondicao']['pagamento_tipo_id'] == '1') {
                            $this->Cielo->cancelar($this->data);
                        }
                    }
                    //só debita quando o pedido não foi finalizado
                    if ($this->data['PedidoStatus']['debitar_produtos'] && !$this->data['Pedido']['finalizado']) {

                        foreach ($this->data['PedidoItem'] as $valor) {
                            if (!isset($valor['debitar_estoque'])) {
                                $valor['debitar_estoque'] = true;
                            }
                            if ($valor['debitar_estoque'] == true) {
                                $grade = $this->Grade->find('first', array('conditions' => array('Grade.id' => $valor['grade_id'])));
                                //quantidade a ser debitada do estoque
                                $desalocar = $grade['Grade']['quantidade'] - $valor['quantidade'];
                                $this->Grade->id = $valor['grade_id'];
                                $this->Grade->saveField('quantidade', $desalocar);
                            }
                        }
                        //seta o pedido como finalizado
                        $this->Pedido->saveField('finalizado', true);
                        if ($this->data['PagamentoCondicao']['pagamento_tipo_id'] == '1') {
                            $this->Cielo->capturar($this->data);
                        }
                    }

                    if ($this->data['PedidoStatus']['enviar_email']) {
                        $this->_enviarEmailStatusPedido($this->data);
                    }
                    $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                }
                //atualiza o estoque de disponivel e alocado.
                foreach ($this->data['PedidoItem'] as $valor) {
                    if (!isset($valor['debitar_estoque'])) {
                        $valor['debitar_estoque'] = true;
                    }
                    if ($valor['debitar_estoque'] == true) {
                        //estoque grade
                        $quantidade_grade = $this->PedidoItem->query("SELECT
                            (`Grade`.`quantidade` - ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE ((`Grade`.`id` = `pedido_itens`.`grade_id`) AND (`pedido_itens`.`pedido_id` = `pedidos`.`id`) AND (`pedidos`.`finalizado` = 0))),0)) AS disponivel,
                            (ifnull((SELECT SUM(`pedido_itens`.`quantidade`)  FROM (`pedido_itens` join `pedidos`) WHERE ((`Grade`.`id` = `pedido_itens`.`grade_id`) AND (`pedido_itens`.`pedido_id` = `pedidos`.`id`) AND (`pedidos`.`finalizado` = 0))),0)) AS alocado,
                            (ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE `Grade`.`id` = `pedido_itens`.`grade_id` AND `pedido_itens`.`pedido_id` = `pedidos`.`id` AND `pedidos`.`finalizado` = 1),0)) AS quantidade_vendido
                        FROM grades Grade WHERE Grade.id  = '" . (int)$valor['grade_id'] . "'");
                        $estoque = array(
                            'id'                    => $valor['grade_id'],
                            'quantidade_disponivel' => $quantidade_grade[0][0]['disponivel'],
                            'quantidade_alocada'    => $quantidade_grade[0][0]['alocado'],
                            'quantidade_vendidos'   => $quantidade_grade[0][0]['quantidade_vendido']
                        );
                        if ($quantidade_grade[0][0]['disponivel'] <= 0) {
                            $estoque['status'] = 2;
                        } else {
                            $estoque['status'] = 1;
                        }
                    } else {
                        //estoque grade
                        $quantidade_grade = $this->PedidoItem->query("SELECT
                            (ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE `Grade`.`id` = `pedido_itens`.`grade_id` AND `pedido_itens`.`pedido_id` = `pedidos`.`id` AND `pedidos`.`finalizado` = 1),0)) AS quantidade_vendido
                        FROM grades Grade WHERE Grade.id  = '" . (int)$valor['grade_id'] . "'");
                        $estoque = array(
                            'id'                  => $valor['grade_id'],
                            'quantidade_vendidos' => $quantidade_grade[0][0]['quantidade_vendido'],
                            'status'              => $valor['status']
                        );
                    }

                    $this->Grade->saveField('id', $estoque['id']);
                    $this->Grade->saveField('quantidade_disponivel', $estoque['quantidade_disponivel']);
                    $this->Grade->saveField('quantidade_alocada', $estoque['quantidade_alocada']);
                    $this->Grade->saveField('quantidade_vendidos', $estoque['quantidade_vendidos']);
                    $this->Grade->saveField('status', $estoque['status']);

                    ///$this->Grade->save($estoque, false);
                    $this->Produto->atualiza_grade_produto((int)$valor['produto_id']);
                }
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
            $this->redirect(array(
                'action' => 'view',
                $pedido_id
            ));
        }

        function admin_tracking($id = null)
        {
            $this->layout = 'modal';
            if (!$id && empty($this->data)) {
                $this->Session->setFlash('Paramentros inválidos', 'flash/error');
                $this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                if ($this->Pedido->PedidoHistoricoTracking->save($this->data, false)) {
                    $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                    $pedido = $this->Pedido->read(null, $id);
                    $this->data['Usuario'] = $pedido['Usuario'];
                    $this->data['Pedido'] = $pedido['Pedido'];
                    $this->_enviarEmailTrackingPedido($this->data);
                    $this->data = array();
                } else {
                    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                }
            }
        }

        function admin_google_aprovar($id = null)
        {
            $this->layout = 'modal';
            if (!$id && empty($this->data)) {
                $this->Session->setFlash('Paramentros inválidos', 'flash/error');
                $this->redirect(array('action' => 'index'));
            }

            $dados = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => $id)));
            $this->set('dados', $dados);
        }

        function admin_google_cancelar($id = null)
        {
            $this->layout = 'modal';
            if (!$id && empty($this->data)) {
                $this->Session->setFlash('Paramentros inválidos', 'flash/error');
                $this->redirect(array('action' => 'index'));
            }
            $dados = $this->Pedido->find('first', array('conditions' => array('Pedido.id' => $id)));
            $this->set('dados', $dados);
        }

        private function _enviarEmailTrackingPedido($dados)
        {
            App::import('Helper', 'Calendario');
            $this->Calendario = new CalendarioHelper();
            //seta o debug como zero para evitar aparecer bugs no email do cliente.
            Configure::write('debug', 0);
            if (Configure::read('Loja.smtp_host') != 'localhost') {
                $this->Email->smtpOptions = array(
                    'port'     => (int)Configure::read('Loja.smtp_port'),
                    'host'     => Configure::read('Loja.smtp_host'),
                    'username' => Configure::read('Loja.smtp_user'),
                    'password' => Configure::read('Loja.smtp_password')
                );
                $this->Email->delivery = 'smtp';
            }
            $this->Email->lineLength = 120;
            $this->Email->sendAs = 'html';
            $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.smtp_user') . ">";
            $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
            $this->Email->to = "{$this->data['Usuario']['nome']} <{$this->data['Usuario']['email']}>";
            $this->Email->subject = "Pedido #" . $this->data['Pedido']['id'] . ' Código de rastreamento  ' . Configure::read('Loja.nome');

            //$url_tracking = "http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=".$dados['PedidoHistoricoTracking']['tracking'];
            if (!strstr($dados['PedidoHistoricoTracking']['tracking'], 'http') && strstr($dados['Pedido']['entrega_tipo'], 'CORREIOS')) {
                $dados['PedidoHistoricoTracking']['tracking'] = "http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=" . $dados['PedidoHistoricoTracking']['tracking'];
            }

            $email = str_replace(array(
                '{USUARIO_NOME}',
                '{PEDIDO_ID}',
                '{PEDIDO_DATA}',
                '{TRACKING}',
                '{LOJA_NOME}'
            ), array(
                $dados['Usuario']['nome'],
                $dados['Pedido']['id'],
                $this->Calendario->DataFormatada("d/m/Y H:i:s", $dados["Pedido"]["created"]),
                $dados['PedidoHistoricoTracking']['tracking'],
                Configure::read('Loja.nome')
            ), Configure::read('LojaTemplate.pedido_tracking'));
            if (Configure::read('Reweb.nome_bcc')) {
                $bccs = array();
                $assunto = Configure::read('Reweb.nome_bcc');
                $emails = Configure::read('Reweb.email_bcc');
                foreach (explode(';', $emails) as $bcc) {
                    if ($bcc != "") {
                        $bccs[] = "{$assunto} <{$bcc}>";
                    }
                }
                $this->Email->bcc = $bccs;
            }
            if ($this->Email->send($email)) {
                return true;
            } else {
                return false;
            }
        }

        /* Campainha do PagSeguro */
        public function campainha()
        {
            $this->render(false);
            $export_dir = WWW_ROOT . "integracao" . DS . "pedido" . DS;
            if (!empty($_POST)) {
                $dados = $_POST;
                $numeroNotificacao = $dados['notificationCode'];
                $tipoNotificacao = $dados['notificationType'];

                $this->log('transaction start', 'transaction');
                $log = var_export($dados, true);
                $this->log($log, 'transaction');
                $this->log('transaction end', 'transaction');

                if ($tipoNotificacao == "transaction") {

                    $url = "https://ws.pagseguro.uol.com.br/v2/transactions/notifications/{$numeroNotificacao}?email={$this->PagSeguro->__init['pagseguro']['email']}&token={$this->PagSeguro->token}";

                    $transaction = $this->xmlstr_to_array(file_get_contents($url));

                    App::import('model', 'Produto');
                    $this->Produto = new Produto();
                    //SE FOI AMBIENTE DE TESTE SEMPRE É PAGO, PARA GERAR O XML.

                    // PAGSEGURO / FG
                    // 1    Aguardando pagamento: o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.  WAITING_PAYMENT
                    // 2    Em análise: o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.    IN_ANALYSIS
                    // 3    Paga: a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.    PAID
                    // 4    Disponível: a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.  AVAILABLE
                    // 5    Em disputa: o comprador, dentro do prazo de liberação da transação, abriu uma disputa.  IN_DISPUTE
                    // 6    Devolvida: o valor da transação foi devolvido para o comprador.     REFUNDED
                    // 7    Cancelada: a transação foi cancelada sem ter sido finalizada.   CANCELLED

                    $pedido_id = $transaction['reference'];
                    //ALTERA O STATUS PRIMEIRO

                    //Se for diferente de Disponivel salva as informações, cliente não necessita deste status, pois já consultou direto
                    if ($transaction['status'] != 4) {
                        $this->Pedido->id = $pedido_id;
                        $this->Pedido->saveField('pedido_status_id', $transaction['status']);
                        $this->Pedido->saveField('parcelas', $transaction['installmentCount']);
                        $this->Pedido->saveField('pag_seguro_xml', $transaction);
                        $this->data = $this->Pedido->find('first', array(
                            'contain'    => array(
                                'PedidoStatus',
                                'PedidoItem',
                                'Usuario'
                            ),
                            'conditions' => array('Pedido.id' => $pedido_id)
                        ));
                        //PEGA O STATUS PARA GUARDAR O HISTORICO
                        $status = $this->Pedido->PedidoStatus->find('first', array('conditions' => array('PedidoStatus.id' => $this->data['Pedido']['pedido_status_id'])));

                        //log

                        $this->log('transaction pedido start', 'transaction');
                        $log = var_export($transaction, true);
                        $this->log($log, 'transaction');
                        $log = var_export($this->data, true);
                        $this->log($log, 'transaction');
                        $this->log('transaction pedido end', 'transaction');

                        $save['PedidoHistoricoStatus']['id'] = null;
                        $save['PedidoHistoricoStatus']['pedido_status_id'] = $this->data['Pedido']['pedido_status_id'];
                        $save['PedidoHistoricoStatus']['pedido_id'] = $pedido_id;
                        $save['PedidoHistoricoStatus']['status'] = $status['PedidoStatus']['nome'];
                        $this->Pedido->PedidoHistoricoStatus->save($save);

                        App::import('Model', 'PedidoItem');
                        $this->PedidoItem = new PedidoItem();
                        App::import('Model', 'Grade');
                        $this->Grade = new Grade();
                        //só desaloca quando o pedido não foi finalizado
                        if ($this->data['PedidoStatus']['desalocar_produtos'] && !$this->data['Pedido']['finalizado']) {
                            foreach ($this->data['PedidoItem'] as $valor) {
                                //estoque grade
                                $quantidade_grade = $this->PedidoItem->query("SELECT quantidade, quantidade_disponivel, quantidade_alocada, quantidade_vendidos FROM grades WHERE id  = " . (int)$valor['grade_id']);
                                //debug($quantidade_grade);die;
                                $estoque = array(
                                    'id'                    => $valor['grade_id'],
                                    'quantidade_disponivel' => $quantidade_grade[0]['grades']['quantidade_disponivel'],
                                    'quantidade_alocada'    => $quantidade_grade[0]['grades']['quantidade_alocada'],
                                    'quantidade_vendidos'   => $quantidade_grade[0]['grades']['quantidade_vendidos']
                                );

                                $estoque['quantidade_alocada'] = $estoque['quantidade_alocada'] - $valor['quantidade'];
                                $estoque['quantidade_disponivel'] = $estoque['quantidade_disponivel'] + $valor['quantidade'];

                                if ($estoque['quantidade_disponivel'] <= 0) {
                                    $estoque['status'] = 2;
                                } else {
                                    $estoque['status'] = 1;
                                }
                                //debug($estoque);die;

                                $this->Grade->saveField('id', $estoque['id']);
                                $this->Grade->saveField('quantidade_disponivel', $estoque['quantidade_disponivel']);
                                $this->Grade->saveField('quantidade_alocada', $estoque['quantidade_alocada']);
                                $this->Grade->saveField('quantidade_vendidos', $estoque['quantidade_vendidos']);
                                if (isset($estoque['status'])) {
                                    $this->Grade->saveField('status', $estoque['status']);
                                    //die('status');
                                }
                                ////$this->Grade->save($estoque, false);
                                $this->Produto->atualiza_grade_produto((int)$valor['produto_id']);
                            }
                            //seta pedido como finalizado
                            $this->Pedido->saveField('finalizado', true);
                        }
                        //só debita quando o pedido não foi finalizado
                        if ($this->data['PedidoStatus']['debitar_produtos'] && !$this->data['Pedido']['finalizado']) {
                            foreach ($this->data['PedidoItem'] as $valor) {
                                //$produto = $this->Produto->find('first', array('conditions' => array('Produto.id' => $valor['produto_id'])));
                                //quantidade a ser debitada do estoque
                                //debug($produto);
                                //$desalocar = ($produto['Produto']['quantidade'] - $valor['quantidade']);
                                //die('ass' . $desalocar);
                                //$this->Produto->id = $valor['produto_id'];
                                //$this->Produto->saveField('quantidade', $desalocar);

                                //estoque grade
                                $quantidade_grade = $this->PedidoItem->query("SELECT quantidade, quantidade_disponivel, quantidade_alocada, quantidade_vendidos FROM grades WHERE id  = " . (int)$valor['grade_id']);
                                //debug($quantidade_grade);die;
                                $estoque = array(
                                    'id'                    => $valor['grade_id'],
                                    'quantidade'            => $quantidade_grade[0]['grades']['quantidade'],
                                    'quantidade_disponivel' => $quantidade_grade[0]['grades']['quantidade_disponivel'],
                                    'quantidade_alocada'    => $quantidade_grade[0]['grades']['quantidade_alocada'],
                                    'quantidade_vendidos'   => $quantidade_grade[0]['grades']['quantidade_vendidos']
                                );
                                $estoque['quantidade'] = $estoque['quantidade'] - $valor['quantidade'];
                                $estoque['quantidade_alocada'] = $estoque['quantidade_alocada'] - $valor['quantidade'];
                                $estoque['quantidade_vendidos'] = $estoque['quantidade_vendidos'] + $valor['quantidade'];

                                if ($quantidade_grade[0]['grades']['quantidade_disponivel'] <= 0) {
                                    $estoque['status'] = 2;
                                } else {
                                    $estoque['status'] = 1;
                                }
                                //debug($estoque);die;

                                $this->Grade->saveField('id', $estoque['id']);
                                $this->Grade->saveField('quantidade', $estoque['quantidade']);
                                $this->Grade->saveField('quantidade_disponivel', $estoque['quantidade_disponivel']);
                                $this->Grade->saveField('quantidade_alocada', $estoque['quantidade_alocada']);
                                $this->Grade->saveField('quantidade_vendidos', $estoque['quantidade_vendidos']);
                                if (isset($estoque['status'])) {
                                    $this->Grade->saveField('status', $estoque['status']);
                                }

                                ////$this->Grade->save($estoque, false);
                                $this->Produto->atualiza_grade_produto((int)$valor['produto_id']);
                            }
                            //seta o pedido como finalizado
                            $this->Pedido->saveField('finalizado', true);
                        }
                        //atualiza o estoque de disponivel e alocado.
                        /*foreach ($this->data['PedidoItem'] as $valor) {
                            $quantidades = $this->Pedido->query("select (`Produto`.`quantidade` - ifnull((select sum(`pedido_itens`.`quantidade`)  from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as disponivel,(ifnull((select sum(`pedido_itens`.`quantidade`)  from (`pedido_itens` join `pedidos`) where ((`Produto`.`id` = `pedido_itens`.`produto_id`) and (`pedido_itens`.`pedido_id` = `pedidos`.`id`) and (`pedidos`.`finalizado` = 0))),0)) as alocado from produtos Produto where Produto.id  = '" . (int) $valor['produto_id'] . "'");
                            //aloca o estoque e notifica quantos disponiveis
                            $this->Produto->saveAll(array('id' => $valor['produto_id'], 'quantidade_disponivel' => $quantidades[0][0]['disponivel'], 'quantidade_alocada' => $quantidades[0][0]['alocado']));
                        }*/
                        if ($this->data['PedidoStatus']['enviar_email']) {
                            $this->_enviarEmailStatusPedido($this->data);
                        }
                        //nao usar esta funcao cagada
                        //$this->atualiza_o_estoque($this->data['PedidoItem']);
                        //SE FOI PAGO GERA O XML DO PEDIDO
                        if ($transaction['status'] == '3'):
                            $meioEntrega = $this->data['Pedido']['entrega_tipo'];
                            $pedido = $this->data;

                            if ($pedido['Pedido']['usuario_tipo_pessoa'] == 'F') {
                                $identificacao = "cpf";
                                $cpf_cnpj = substr(trim($pedido['Pedido']['usuario_cpf']), 0, 14);
                            } else {
                                $identificacao = "cnpj";
                                $cpf_cnpj = substr(trim($pedido['Pedido']['usuario_cnpj']), 0, 14);
                            }
                            // Dados Pedido
                            $xml = "<?xml version='1.0' encoding='utf-8'?>
                            <pedido>
                                <numero>" . $transaction['reference'] . "</numero>
                                <transacao>" . $transaction['code'] . "</transacao>
                                <status>" . $transaction['status'] . "</status>
                                <tipo_metodo_pagamento>" . $transaction['paymentMethod']['type'] . "</tipo_metodo_pagamento>
                                <codigo_metodo_pagamento>" . $transaction['paymentMethod']['code'] . "</codigo_metodo_pagamento>
                                <taxa>" . $transaction['feeAmount'] . "</taxa>
                                <data>" . $pedido['Pedido']['created'] . "</data>
                                <observacao></observacao>
                                <transportadora>" . $meioEntrega . "</transportadora>
                                <valor_acrescimento_pedido>" . $transaction['extraAmount'] . "</valor_acrescimento_pedido>
                                <valor_desconto_pedido>" . $transaction['discountAmount'] . "</valor_desconto_pedido>
                                <valor_frete>" . $transaction['shipping']['cost'] . "</valor_frete>
                                <valor_pedido>" . $transaction['grossAmount'] . "</valor_pedido>";
                            // Limpa telefone
                            $arReplace = array(
                                '(',
                                ')',
                                '-',
                                ' ',
                                '.',
                                ',',
                                '/'
                            );
                            $strTelefone = substr(str_replace($arReplace, "", trim($pedido['Pedido']['usuario_telefone'])), 0, 20);
                            $strCelular = substr(str_replace($arReplace, "", trim($pedido['Pedido']['usuario_celular'])), 0, 20);

                            // Razão Social
                            $razao_social = $pedido['Pedido']['usuario_razao_social'];
                            $razao_social = substr($razao_social, 0, 100);

                            $xml .= "<cliente>
                                    <tipo_cliente>" . $pedido['Pedido']['usuario_tipo_pessoa'] . "</tipo_cliente>
                                    <" . $identificacao . ">" . $cpf_cnpj . "</" . $identificacao . ">
                                    <razao_social>" . $razao_social . "</razao_social>
                                    <fone>" . $strTelefone . "</fone>
                                    <celular>" . $strCelular . "</celular>
                                    <email>" . substr($pedido['Pedido']['usuario_email'], 0, 50) . "</email>
                                    <endereco>
                                        <tipo_endereco>E</tipo_endereco>
                                        <cep_entrega>" . substr($pedido['Pedido']['endereco_cep'], 0, 5) . '-' . substr($pedido['Pedido']['endereco_cep'], 5) . "</cep_entrega>
                                        <logradouro_entrega>" . substr($pedido['Pedido']['endereco_rua'], 0, 70) . "</logradouro_entrega>
                                        <numero_entrega>" . substr($pedido['Pedido']['endereco_numero'], 0, 10) . "</numero_entrega>
                                        <complemento_entrega>" . substr($pedido['Pedido']['endereco_complemento'], 0, 70) . "</complemento_entrega>
                                        <bairro_entrega>" . substr($pedido['Pedido']['endereco_bairro'], 0, 50) . "</bairro_entrega>
                                        <cidade_entrega>" . substr($pedido['Pedido']['endereco_cidade'], 0, 30) . "</cidade_entrega>
                                        <estado_entrega>" . substr($pedido['Pedido']['endereco_estado'], 0, 2) . "</estado_entrega>
                                        <pais_entrega>BR</pais_entrega>
                                    </endereco>
                                </cliente>";

                            // Dados dos Itens
                            $contador = 0;
                            foreach ($pedido['PedidoItem'] as $item) {
                                $contador++;
                                $preco = ($item['preco_promocao'] > 0 ? $item['preco_promocao'] : $item['preco']);
                                $xml .= "<item_pedido>
                                            <item>" . $contador . "</item>
                                            <sku>" . $item['sku'] . "</sku>
                                            <quantidade>" . $item['quantidade'] . "</quantidade>
                                            <valor_produto>" . $preco . "</valor_produto>
                                            <valor_acrescimento></valor_acrescimento>
                                            <valor_desconto></valor_desconto>
                                            <valor_item>" . ($preco * $item['quantidade']) . "</valor_item>
                                            <data_promessa>" . $item['created'] . "</data_promessa>
                                        </item_pedido>";
                            }
                            $xml .= "</pedido>";
                            $this->log($xml, 'transaction');
                            $fp = fopen($export_dir . 'pedido' . $transaction['reference'] . '.xml', 'w');
                            fwrite($fp, $xml);
                            fclose($fp);
                        endif;
                    } else {
                        //guarda o log do pedido com status Disponivel para descargo de consciência

                        $this->log('transaction pedido start', 'transaction_disponivel');
                        $log = var_export($transaction, true);
                        $this->log($log, 'transaction_disponivel');
                        $log = var_export($this->data, true);
                        $this->log($log, 'transaction_disponivel');
                        $this->log('transaction pedido end', 'transaction_disponivel');
                    }
                }
            }
            $this->redirect(array('controller' => 'carrinho', 'admin' => false, 'plugin' => 'carrinho', 'action' => 'meus_pedidos'));
        }

        /* Campainha do BPag */
        public function ws_campainha()
        {
            $this->layout = false;
            $this->render(false);

            if ($this->RequestHandler->isPost()) {

                $retorno = $_POST;
                $retorno_ws = $this->BpagPagamento->campainhaWS($retorno);
                $log = var_export($retorno_ws, true);
                $this->log($log, LOG_DEBUG);
                $this->Pedido->id = $retorno['merch_ref'];
                $pedido_atual = $this->Pedido->read(null, $this->Pedido->id);
                /**
                 * Se o pedido já está finalizado não faz nada
                 */
                if ($pedido_atual['Pedido']['finalizado']) {
                    $x = '<?xml version="1.0" encoding="UTF-8"?>
                        <bell>
                            <status>1</status>
                            <msg>Pedido já Finalizado</msg>
                        </bell>';
                    die($x);
                }

                /**
                 * STATUS DO SISTEMA DO BPAG
                 * 0  Pago - Pedido pago com sucesso.
                 * 1  Não Pago - Comprador finaliza pagamento sem sucesso e o pedido é dado como terminado pela Instituição Financeira.
                 * 2  Inválido - Este status acontece quando a transação não pode mais ser processada pelo BPag. Esta mudança pode ocorrer por regras da própria loja ou por ação manual via painel de control do BPag.
                 * 3  Cancelado - (Estornado)  Pagamento cancelado.
                 * 4  Não Efetivado - Este é o status inicial do pedido.
                 * 5  Saldo Insuficiente  - Ocorre quando o comprador não tem saldo suficiente em sua conta bancária.
                 * 6  Pendente de Liberação - Ocorre quando a compra precisa da aprovação de mais de uma pessoa na instituição financeira. Comum para débitos online em contas empresariais.
                 * 7  Pendente de Pagamento - Ocorre quando a instituição financeira está aguardando o pagamento. Ocorre para métodos de pagamento via boleto ou para débigo Real entre 0h e 7h.
                 * 8  Não Capturado - Este é um status intermediário, principalmente para cartões de crédito, que significa se o valor da compra foi reservada no limite do comprado. Mas ainda é necessário capturar a transação para que ele seja efetivamente cobrada.
                 * 10 Pago Parcialmente - Ocorre quando o comprador pagou valor inferior ao valor total do pedido. Geralmente isto ocorre quando o usuário tenta fraudar o pagamento de boletos ou quando o usuário erra, sem intenção, o valor dos centavos do boleto.
                 * 12 Em Análise  - Significa que o pedido está sendo analisado pelo módulo anti-fraude.
                 */
                //pedido cancelado, desalocar os produtos para nova venda
                if (in_array($retorno_ws['ProbeReturn']['OrderData']['Order']['BpagData']['status'], array(
                    "1",
                    "2",
                    "3",
                    "4",
                    "5"
                ))) {
                    if (!$pedido_atual['Pedido']['finalizado']) {
                        $this->Pedido->saveField('finalizado', true);
                        $this->Pedido->saveField('pedido_status_id', 4);
                    }
                } elseif (in_array($retorno_ws['ProbeReturn']['OrderData']['Order']['BpagData']['status'], array(
                    "6",
                    "7",
                    "8",
                    "10",
                    "12"
                ))) {
                    //pedido aguardando o pagamento
                    $this->Pedido->saveField('pedido_status_id', 1);
                } elseif (in_array($retorno_ws['ProbeReturn']['OrderData']['Order']['BpagData']['status'], array("0"))) {
                    //pedido aprovado, irá debitar os produtos do estoque
                    //se o pedido já finalizou
                    App::import('model', 'Grade');
                    $this->Grade = new Grade();
                    if (!$pedido_atual['Pedido']['finalizado']) {
                        foreach ($this->data['PedidoItem'] as $valor) {
                            $grade = $this->Grade->find('first', array('conditions' => array('Grade.id' => $valor['grade_id'])));
                            //quantidade a ser debitada do estoque
                            $desalocar = $grade['Grade']['quantidade'] - $valor['quantidade'];
                            $this->Grade->id = $valor['grade_id'];
                            $this->Grade->saveField('quantidade', $desalocar);
                        }
                        //seta o pedido como finalizado
                        $this->Pedido->saveField('finalizado', true);
                    }

                    $this->Pedido->saveField('pedido_status_id', 3);
                }

                if ($pedido_atual['PedidoStatus']['enviar_email']) {
                    $pedido_alterado = $this->Pedido->read(null, $this->Pedido->id);
                    $this->_enviarEmailStatusPedido($pedido_alterado);
                }
            }
            $x = '<?xml version="1.0" encoding="UTF-8"?>
                        <bell>
                            <status>1</status>
                            <msg>Notificacao recebida com sucesso</msg>
                        </bell>';
            die($x);
        }

        private function _enviarEmailStatusPedido($dados)
        {
            App::import('Helper', 'Calendario');
            $this->Calendario = new CalendarioHelper();
            //seta o debug como zero para evitar aparecer bugs no email do cliente.
            Configure::write('debug', 0);
            $this->Email->smtpOptions = array(
                'port'     => (int)Configure::read('Loja.smtp_port'),
                'host'     => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );

            $this->Email->delivery = 'smtp';
            $this->Email->lineLength = 120;
            $this->Email->sendAs = 'html';
            $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
            $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.smtp_user') . ">";
            $this->Email->to = "{$this->data['Usuario']['nome']} <{$this->data['Usuario']['email']}>";
            $this->Email->subject = "Pedido #" . $this->data['Pedido']['id'] . '  ' . Configure::read('Loja.nome');
            $email = str_replace(array(
                '{USUARIO_NOME}',
                '{PEDIDO_ID}',
                '{PEDIDO_DATA}',
                '{PEDIDO_STATUS}',
                '{LOJA_NOME}'
            ), array(
                $dados['Usuario']['nome'],
                $dados['Pedido']['id'],
                $this->Calendario->DataFormatada("d/m/Y H:i:s", $dados["Pedido"]["created"]),
                $dados['PedidoStatus']['nome'],
                Configure::read('Loja.nome')
            ), $dados['PedidoStatus']['template_email']);
            if (Configure::read('Reweb.nome_bcc')) {
                $bccs = array();
                $assunto = Configure::read('Reweb.nome_bcc');
                $emails = Configure::read('Reweb.email_bcc');
                foreach (explode(';', $emails) as $bcc) {
                    $bccs[] = "{$assunto} <{$bcc}>";
                }
                $this->Email->bcc = $bccs;
            }
            if ($this->Email->send($email)) {
                return true;
            } else {
                return false;
            }
        }

        function xmlstr_to_array($xmlstr)
        {
            $doc = new DOMDocument();
            $doc->loadXML($xmlstr);
            return $this->domnode_to_array($doc->documentElement);
        }

        function domnode_to_array($node)
        {
            $output = array();
            switch ($node->nodeType) {
                case XML_CDATA_SECTION_NODE:
                case XML_TEXT_NODE:
                    $output = trim($node->textContent);
                    break;
                case XML_ELEMENT_NODE:
                    for ($i = 0, $m = $node->childNodes->length; $i < $m; $i++) {
                        $child = $node->childNodes->item($i);
                        $v = $this->domnode_to_array($child);
                        if (isset($child->tagName)) {
                            $t = $child->tagName;
                            if (!isset($output[$t])) {
                                $output[$t] = array();
                            }
                            $output[$t][] = $v;
                        } elseif ($v) {
                            $output = (string)$v;
                        }
                    }
                    if (is_array($output)) {
                        if ($node->attributes->length) {
                            $a = array();
                            foreach ($node->attributes as $attrName => $attrNode) {
                                $a[$attrName] = (string)$attrNode->value;
                            }
                            $output['@attributes'] = $a;
                        }
                        foreach ($output as $t => $v) {
                            if (is_array($v) && count($v) == 1 && $t != '@attributes') {
                                $output[$t] = $v[0];
                            }
                        }
                    }
                    break;
            }
            return $output;
        }

        public function boleto($pedido_hash)
        {

            $dados = explode('|', base64_decode($pedido_hash));
            $pedido_id = $dados[0];
            $usuario_id = $dados[2];

            $this->layout = '';
            $dados = $this->Pedido->find('first', array(
                'conditions' => array(
                    'Pedido.usuario_id' => $usuario_id,
                    'Pedido.id'         => $pedido_id
                )
            ));
            if (!$dados)
                die('Parametros inválidos');
            $this->set('dados', $dados);
        }

        /**
         * envia email de boletos a vencer, pendentes de pagamento.
         */
        public function cron_check_email_boleto()
        {
            App::import('model', 'Pedido');
            $this->Pedido = new Pedido();
            App::import("helper", "String");
            $this->String = new StringHelper();
            App::import("helper", "Time");
            $this->Time = new TimeHelper();
            App::import("helper", "Calendario");
            $this->Calendario = new CalendarioHelper();

            $pedidos = $this->Pedido->find('all', array(
                'recursive'  => 1,
                'fields'     => array(
                    'Pedido.id',
                    'Pedido.usuario_id',
                    'Usuario.nome',
                    'Usuario.email',
                    'Pedido.url_boleto',
                    'Pedido.created',
                    'Pedido.pagamento_condicao_id',
                    'Pedido.pedido_status_id',
                    'Pedido.finalizado',
                    'DATE_ADD(Pedido.created, INTERVAL 6 DAY) AS vencimento_boleto'
                ),
                'contain'    => array(
                    'Usuario',
                    'PedidoItem'
                ),
                'conditions' => array(
                    'AND' => array(
                        'finalizado'            => false,
                        'pedido_status_id'      => 1,
                        'pagamento_condicao_id' => 5,
                        'aviso_vcto_boleto'     => false
                    )
                )
            ));

            $dataAtual = date_create(date('Y-m-d', time()));
            $enviado = 1;
            $vencido = 1;
            $count = count($pedidos);
            $num = 0;
            print '<pre>';
            print_r($count);
            print '</pre>';
            print '<pre>';
            print_r('**************');
            print '</pre>';

            foreach ($pedidos as $pedido) {
                $dataVencimento = date_create($this->Calendario->DataFormatada('Y-m-d', $pedido[0]['vencimento_boleto']));
                $interval = date_diff($dataAtual, $dataVencimento);

                print '<pre>';
                print_r('id = ' . $pedido['Pedido']['id']);
                print '</pre>';
                print '<pre>';
                print_r($dataVencimento);
                print '</pre>';
                print '<pre>';
                print_r('sinal = ' . $interval->invert);
                print '</pre>';
                print '<pre>';
                print_r('numero de dias = ' . $interval->days);
                print '</pre>';

                if ($interval->invert == 0 && $interval->days == 0) {
                    print '<pre>';
                    print_r('enviados = ' . $enviado);
                    print '</pre>';
                    /* boleto a vencer */
                    if ($this->send_email_boleto($pedido['Pedido'], $pedido['Usuario'])) {
                        $this->Pedido->updateAll(array('Pedido.aviso_vcto_boleto' => true), array('Pedido.id' => $pedido['Pedido']['id']));
                    }
                    $enviado++;
                } elseif ($interval->invert == 1) {
                    /* boleto vencido */
                    print '<pre>';
                    print_r('vencidos = ' . $vencido);
                    print '</pre>';
                    ///$this->Pedido->save(array('id'=> $pedido['Pedido']['id'], 'pedido_status_id' => 7, 'finalizado' => true), false);
                    $vencido++;
                }
                print '<pre>';
                print_r('##########');
                print '</pre>';
            }
            die('FINAL ROTINA');
        }

        public function send_email_boleto($aPedido, $aUsuario)
        {
            App::import('Helper', 'Calendario');
            $this->Calendario = new CalendarioHelper();
            App::import("helper", "String");
            $this->String = new StringHelper();

            $user_name = $aUsuario['nome'];
            $user_mail = $aUsuario['email'];
            $url_boleto = $aPedido['url_boleto'];
            $dataLimite = $this->Calendario->dataFormatada('d/m/Y', strtotime("+7 days"));
            $dataPedido = $this->Calendario->dataFormatada('d/m/Y', $aPedido['modified']);
            $pedido_id = $aPedido['id'];

            Configure::write('debug', 0);
            if (Configure::read('Loja.smtp_host') != 'localhost') {
                $this->Email->smtpOptions = array(
                    'port'     => (int)Configure::read('Loja.smtp_port'),
                    'host'     => Configure::read('Loja.smtp_host'),
                    'username' => Configure::read('Loja.smtp_user'),
                    'password' => Configure::read('Loja.smtp_password')
                );
                $this->Email->delivery = 'smtp';
            }
            $this->Email->lineLength = 120;
            $this->Email->sendAs = 'html';
            $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
            $this->Email->to = "{$user_name} <{$user_mail}>";
            $this->Email->subject = "Notificação de boleto a vencer.";

            $email = str_replace(array(
                '{NOME}',
                '{URL_BOLETO}',
                '{DATAVENCTO}',
                '{DATAPEDIDO}',
                '{PEDIDO_ID}'
            ), array(
                $user_name,
                $url_boleto,
                $dataLimite,
                $dataPedido,
                $pedido_id
            ), Configure::read('LojaTemplate.notificacao_boleto'));

            if (Configure::read('Reweb.nome_bcc')) {
                $bccs = array();
                $assunto = Configure::read('Reweb.nome_bcc');
                $emails = Configure::read('Reweb.email_bcc');
                foreach (explode(';', $emails) as $bcc) {
                    $bccs[] = "{$assunto} <{$bcc}>";
                }
                $this->Email->bcc = $bccs;
            }

            if ($this->Email->send($email)) {
                return true;
            } else {
                return false;
            }
        }

        public function cron_atualiza_status_pedidos()
        {
            $this->render(false);
            $this->layout = false;

            App::import('model', 'Produto');
            $this->Produto = new Produto();
            App::import('model', 'PedidoItem');
            $this->PedidoItem = new PedidoItem();

            $pedidos = $this->Pedido->query("SELECT * FROM pedidos WHERE pedido_status_id = 1 AND finalizado=0 AND DATE(created) < (CURDATE() - INTERVAL 8 DAY)");
            $cancelado = 7;

            $this->log('------INICIO-----', 'atualizacao_pedidos');

            foreach ($pedidos as $pedido) {

                $this->log('------O PEDIDO ' . $pedido['pedidos']['id'] . ' FOI CANCELADO-----', 'atualizacao_pedidos');

                $this->Pedido->id = $pedido['pedidos']['id'];
                $this->Pedido->saveField('pedido_status_id', $cancelado);
                //log
                $save['PedidoHistoricoStatus']['id'] = null;
                $save['PedidoHistoricoStatus']['pedido_status_id'] = $cancelado;
                $save['PedidoHistoricoStatus']['pedido_id'] = $pedido['pedidos']['id'];
                $save['PedidoHistoricoStatus']['status'] = 'Cancelada';
                $this->Pedido->PedidoHistoricoStatus->save($save);

                $pedidos_itens = $this->Pedido->query("SELECT * FROM pedido_itens WHERE pedido_id =" . $pedido['pedidos']['id'] . "");

                //atualiza o estoque de disponivel e alocado.
                foreach ($pedidos_itens as $valor) {
                    //begin estoque grade
                    App::import('Model', 'Grade');
                    $this->Grade = new Grade();
                    if (!isset($valor['debitar_estoque'])) {
                        $valor['debitar_estoque'] = true;
                    }
                    if ($valor['debitar_estoque'] == true) {
                        $quantidade_grade = $this->PedidoItem->query("SELECT
                                    (`Grade`.`quantidade` - ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE ((`Grade`.`id` = `pedido_itens`.`grade_id`) AND (`pedido_itens`.`pedido_id` = `pedidos`.`id`) AND (`pedidos`.`finalizado` = 0))),0)) AS disponivel,
                                    (ifnull((SELECT SUM(`pedido_itens`.`quantidade`)  FROM (`pedido_itens` join `pedidos`) WHERE ((`Grade`.`id` = `pedido_itens`.`grade_id`) AND (`pedido_itens`.`pedido_id` = `pedidos`.`id`) AND (`pedidos`.`finalizado` = 0))),0)) AS alocado,
                                    (ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE `Grade`.`id` = `pedido_itens`.`grade_id` AND `pedido_itens`.`pedido_id` = `pedidos`.`id` AND `pedidos`.`finalizado` = 1),0)) AS quantidade_vendido
                                FROM grades Grade WHERE Grade.id  = '" . (int)$valor['pedido_itens']['grade_id'] . "'");
                        $estoque = array(
                            'id'                    => $valor['pedido_itens']['grade_id'],
                            'quantidade_disponivel' => $quantidade_grade[0][0]['disponivel'],
                            'quantidade_alocada'    => $quantidade_grade[0][0]['alocado'],
                            'quantidade_vendidos'   => $quantidade_grade[0][0]['quantidade_vendido']
                        );

                        $this->Grade->saveField('id', $estoque['id']);
                        $this->Grade->saveField('id', $estoque['quantidade_disponivel']);
                        $this->Grade->saveField('id', $estoque['quantidade_alocada']);
                        $this->Grade->saveField('quantidade_vendidos', $estoque['quantidade_vendidos']);

                    } else {
                        $quantidade_grade = $this->PedidoItem->query("SELECT
                                    (ifnull((SELECT SUM(`pedido_itens`.`quantidade`) FROM (`pedido_itens` join `pedidos`) WHERE `Grade`.`id` = `pedido_itens`.`grade_id` AND `pedido_itens`.`pedido_id` = `pedidos`.`id` AND `pedidos`.`finalizado` = 1),0)) AS quantidade_vendido
                                FROM grades Grade WHERE Grade.id  = '" . (int)$valor['pedido_itens']['grade_id'] . "'");
                        $estoque = array(
                            'id'                  => $valor['pedido_itens']['grade_id'],
                            'quantidade_vendidos' => $quantidade_grade[0][0]['quantidade_vendido']
                        );

                        $this->Grade->saveField('id', $estoque['id']);
                        $this->Grade->saveField('quantidade_vendidos', $estoque['quantidade_vendidos']);
                    }

                    $this->Grade->saveField('status', $estoque['status']);

                    $this->Grade->save($estoque);
                    if ((int)$valor['pedido_itens']['produto_id'] > 0) {
                        $this->Produto->atualiza_grade_produto((int)$valor['pedido_itens']['produto_id']);
                    }
                    //envia o email da compra pro cliente.
                    ///$this->emailNotificacaoCompraCancelada();
                }
                $this->Pedido->saveField('finalizado', true);
            }
            $this->log('------FIM-----', 'atualizacao_pedidos');
        }

    }

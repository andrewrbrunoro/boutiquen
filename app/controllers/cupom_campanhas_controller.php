<?php

class CupomCampanhasController extends AppController {

    var $name = 'cupom_campanhas';
    var $uses = array('CupomCampanha');
    var $helpers = array('Calendario', 'String', 'Javascript', 'Image', 'Flash');

    function admin_index(){
        $this->CupomCampanha->recursive = 0;
        $this->set('cupom_campanha',$this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->CupomCampanha->create();
            if ($this->CupomCampanha->save($this->data)) {
                $this->Session->setFlash(__('Os registros foram adicionados com sucesso.', 'flash/success', array(), 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Os registros foram adicionados com sucesso.', true));
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Parametros inválidos', 'flash/error', array(), 'error'));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->CupomCampanha->save($this->data)) {
                App::import('model','Cupom');
                $this->Cupom=new Cupom();
                //$dadosCupons=$this->Cupom->find('all',array('fields'=>array('id'),'conditions'=>array('cupom_campanha_id'=>$this->data['CupomCampanha']['id'])));
               
                $this->Cupom->updateAll(array('status'=>$this->data['CupomCampanha']['status']),array('cupom_campanha_id'=>$this->data['CupomCampanha']['id']));
                
                $this->Session->setFlash(__('O registro foi alterado com sucesso.', 'flash/success', array(), 'success'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('O registro não pode ser alterado, tente novamente.', 'flash/error', array(), 'error'));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->CupomCampanha->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Parametros inválidos', 'flash/error', array(), 'error'));
            $this->redirect(array('action' => 'index'));
        }
        if ($this->CupomCampanha->delete($id)) {
            $this->Session->setFlash(__('Registro deletado com sucesso', 'flash/success', array(), 'success'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('O Registro não pode ser deletado, tente novamente.', 'flash/error', array(), 'error'));
        $this->redirect(array('action' => 'index'));
    }

    function admin_view($idCampanha=null){
        $this->set('cupom_campanha',$this->CupomCampanha->read($idCampanha));

        App::import('Model','Cupom');
        $this->Cupom=new Cupom();
        $this->Cupom->recursive = 0;
        $this->set('cupons',$this->Cupom->paginate(array('conditions'=>array('cupom_campanha_id'=>$idCampanha))));
    }

}

?>
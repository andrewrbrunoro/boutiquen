<?php
class DuvidaFrequenteTiposController extends AppController {

	var $name = 'DuvidaFrequenteTipo';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "DuvidaFrequenteTipo.nome LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->DuvidaFrequenteTipo->recursive = 0;
		$this->set('duvidas_frequentes_tipos', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->DuvidaFrequenteTipo->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['DuvidaFrequenteTipo']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['DuvidaFrequenteTipo']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['DuvidaFrequenteTipo']['nome'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['DuvidaFrequente']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['DuvidaFrequente']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "duvidas_frequentes_tipos_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		return false;
		if (!empty($this->data)) {
			$this->DuvidaFrequenteTipo->create();
            
			if ($this->DuvidaFrequenteTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['DuvidaFrequenteTipo']['id'] = $id;
			$this->DuvidaFrequenteTipo->id = $id;
                 
			if ($this->DuvidaFrequenteTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->DuvidaFrequenteTipo->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->DuvidaFrequenteTipo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>
<?php

class BannersController extends AppController {

    var $name = 'Banners';
    var $helpers = array('Calendario', 'String', 'Image', 'Javascript', 'Session', 'Form');
    var $uses = array('Banner', "Produto");

    function admin_index() {
        $this->Banner->recursive = 0;
        $this->set('banners', $this->paginate());
    }

    function admin_add() {
        
        if (!empty($this->data)) {
            $this->Banner->create();
            if ($this->Banner->saveAll($this->data)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $bannerTipos = array('Selecione') + $this->Banner->BannerTipo->find('list');

        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();
        $categorias = $this->Categoria->find('list', array('conditions' => array('Categoria.status' => true), 'fields' => array('id', 'seo_url')));


        $this->set(compact('bannerTipos', 'categorias'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Banner->saveAll($this->data)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Banner->read(null, $id);
        }
        $bannerTipos = array('Selecione') + $this->Banner->BannerTipo->find('list');


        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();
        $categorias = $this->Categoria->find('list', array('conditions' => array('Categoria.status' => true), 'fields' => array('id', 'seo_url')));

        $this->set(compact('bannerTipos', 'categorias'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Banner->delete($id)) {
            //limpo o cache
            $this->limparCache();
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    private function limparCache() {
        Cache::write('banners', false);
        Cache::write('categorias_routes', false);
    }

}

?>
<?php

class VitrinesController extends AppController {

    var $name = 'Vitrines';
    var $helpers = array('Calendario', 'Parcelamento', 'String', 'Image', 'Javascript', 'Tree', 'Session', 'Form');
    var $uses = array('Categoria', 'Produto', 'Vitrine', 'ProdutoVitrine', 'PagamentoCondicao');

    function index($vitrine_id = null) {
		
        $this->loadModel('ProdutoVitrine');
        
        $produtos_vitrines = $this->ProdutoVitrine->find('all', array('contain' => array('Vitrine' => array('fields' => array('nome'))), 'recursive' => -2, 'fields' => array('produto_id'), 'conditions' => array('ProdutoVitrine.vitrine_id' => (int) $vitrine_id)));
 
        if (!$produtos_vitrines) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect('/');
        }
        
        $vitrine = $this->Vitrine->find('first', array('recursive' => -1, 'conditions' => array('Vitrine.id' => $vitrine_id)));

        $produtos = set::extract('/ProdutoVitrine/produto_id', $produtos_vitrines);
        $joins =
                array(
                    array(
                        'table' => 'produtos_categorias',
                        'alias' => 'Categorias',
                        'type' => 'left',
                        'conditions' =>
                        array(
                            'Categorias.produto_id = Produto.id'
                        )
                    ), array(
                        'table' => 'categorias',
                        'alias' => 'Categoria',
                        'type' => 'left',
                        'conditions' =>
                        array(
                            'Categorias.categoria_id = Categoria.id'
                        )
                    ), array(
                        'table' => 'produtos_vitrines',
                        'alias' => 'ProdutoVitrine',
                        'type' => 'left',
                        'conditions' =>
                        array(
                            'ProdutoVitrine.produto_id = Produto.id'
                        )
                    )
        );
        $this->paginate = array(
            'group' => 'Produto.id',
			'conditions' => array('Produto.status >' => 0),
            'limit'=>$vitrine['Vitrine']['quantidade_exibida']>0?$vitrine['Vitrine']['quantidade_exibida']:48,
            'order' => "FIELD(Produto.id,". implode(", ", array_reverse($produtos,true)).")",
            'recursive' => -1,
            'joins' => $joins,
            'fields' => array('(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",Categorias.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",Categorias.produto_id,"\"%") OR FreteGratis.grade_id LIKE concat("%\"",Produto.grade_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de ) LIMIT 1) as frete_gratis,Produto.id,Produto.nome,Produto.desconto_meio_pagamento,Produto.preco,Produto.preco_promocao,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho,Produto.marca,Produto.grade_id,Categoria.*,(SELECT concat("uploads/produto_imagem/filename/",GradeImagem.filename) FROM grade_imagens as GradeImagem WHERE GradeImagem.grade_id = Produto.grade_id ORDER BY GradeImagem.ordem ASC LIMIT 1) as imagem,(MATCH (Produto.nome,Produto.descricao,Produto.tag) AGAINST ("' . addslashes($this->params['pass'][0]) . '" IN BOOLEAN MODE)) AS rating, (select group_concat(concat(concat(id, \'|\'),concat(concat(thumb_dir, \'/\'),thumb_filename)) separator \';\') as file from flags fla inner join produtos_flags pfla on pfla.flag_id=fla.id where pfla.produto_id=Produto.id ) as flags')
        );

        $this->set('vitrine_nome', $vitrine['Vitrine']['nome']);

        $this->set('produtos', $this->paginate('Produto', array('Produto.id' => $produtos)));

        $this->set('parcelamento', current($this->PagamentoCondicao->find('list', array('fields' => array('parcelas'), 'recursive' => -1, 'ORDER' => 'parcelas DESC'))));
        $this->set('seo_title', $vitrine['Vitrine']['nome'] . ' - ' . Configure::read('Loja.titulo'));

        //start breadcrumb
        $breadcrumb = "";
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $vitrine['Vitrine']['nome']);
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    function admin_index() {
        $this->Vitrine->recursive = -1;

        $this->set('vitrines', $this->paginate('Vitrine'));
    }

    function admin_add() {
        if (!empty($this->data)) {
            
            if (isset($this->data['Produto']['Produto'])) {
                $prod = array_reverse($this->data['Produto']['Produto'], true);
                unset($this->data['Produto']['Produto']);
            }
            
            $this->Vitrine->create();
            if ($this->Vitrine->saveAll($this->data)) {
                if(isset($prod) && is_array($prod) && count($prod) > 0){
                    foreach ($prod as $chave=>$valor) {
                        $this->ProdutoVitrine->primaryKey = '';
                        $this->ProdutoVitrine->saveAll(array('ordem'=>$chave+1,'produto_id'=>$valor,'vitrine_id' => $this->Vitrine->id));
                    }
                }
                
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
            
            if(isset($prod)){
                $order = "FIELD(id,". implode(", ", array_reverse($prod, true)).")";
                $produtos = $this->Produto->find('list', array('fields' => array('id', 'nome'), 'conditions' => array('Produto.id' => array_reverse($prod, true)), 'order' => $order));
                $this->set(compact('produtos'));
            }
        }
        
        //set categorias
        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();
        $categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, '{n}.Categoria.id', '{n}.Categoria.nome', '--');
        $this->set(compact('categorias'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->ProdutoVitrine->deleteAll(array('vitrine_id' => $id));
            if (isset($this->data['Produto']['Produto'])) {
                $prod = $this->data['Produto']['Produto'];
                unset($this->data['Produto']['Produto']);
            }
            if ($this->Vitrine->saveAll($this->data)) {
				//begin atualizo registro do link
				$this->loadModel('Link');
				$links = array();
				$links = $this->Link->find('all', array('recursive' => -1, 'fields' => array('Link.id','Link.link'), 'conditions' => array('Link.vitrine_id' => $id)));
				
				if ($this->data['Vitrine']['tipo'] == 'PROMOCOES') {
					$url = "/ofertas";
				} elseif ($this->data['Vitrine']['tipo'] == 'LANCAMENTOS') {
					$url = "/lancamentos";
				}elseif ($this->data['Vitrine']['tipo'] == 'MENU') {
					$url = "/vitrine/" . low(Inflector::slug($this->data['Vitrine']['nome'], "-"));
				}else{
					$url = "/categoria/" . low(Inflector::slug($this->data['Vitrine']['nome'], "-"));
				}
				if(count($links) > 0){
					foreach($links as $link){
						$dt = '';
						$dt['id'] 	= $link['Link']['id'];
						$dt['link'] = $url;
						$this->Link->save($dt, false);
					}
					Cache::write('menus_for_layout', false);
				}
				//end atualizo registro do link
                if(isset($prod) && is_array($prod) && count($prod) > 0){
                    foreach ($prod as $chave=>$valor) {
                        $this->ProdutoVitrine->primaryKey = '';
                        $this->ProdutoVitrine->saveAll(array('ordem'=>$chave+1,'produto_id'=>$valor,'vitrine_id' => $id));
                    }
                }
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        if (empty($this->data)) {
            $this->data = $this->Vitrine->find('first', array('contain' => array('Produto'), 'conditions' => array('id' => $id)));
            $this->data['Produto'] = array_reverse($this->data['Produto'],true);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
            
            foreach($this->data['Produto'] as $p){
                $produtos[$p['id']] = $p['sku'] .' - '.$p['nome'];
            }
        }

        //set categorias
        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();
        $categorias = array('' => 'Selecione') + $this->Categoria->generatetreelist(null, '{n}.Categoria.id', '{n}.Categoria.nome', '--');
        $this->set(compact('categorias'));
        $this->set(compact('categorias', 'produtos'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Vitrine->delete($id)) {
            //limpo o cache
            $this->limparCache();
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_ajax_produtos() {
        $query = trim($this->params['form']['query']);
        $categoria_id = $this->params['form']['categoria_id'];

        //produtos já selecionados
        $selecteds = $this->params['form']['selecteds'];

        if ($query) {
            $filhos = $this->Categoria->children($categoria_id);
            $ids = am(set::extract('/Categoria/id', $filhos), $categoria_id);
            $produtos = $this->Produto->find('all', array('fields' => array('Produto.id', 'Produto.nome', 'Produto.sku'),
                'recursive' => -1,
                'limit' => '100',
                'conditions' => array(
                    'AND' => array('NOT' => array('Produto.id' => $selecteds),
                        'OR' => array('Produto.tag LIKE' => '%' . $query . '%', 'Produto.sku LIKE' => '%' . $query . '%', 'Produto.descricao LIKE' => '%' . $query . '%', 'Produto.nome LIKE' => '%' . $query . '%'),
                        'AND' => array( 'Produto.parent_id' => 0, 'Produto.status > ' => 0))
                ),
                'joins' => array(
                    array(
                        'table' => 'produtos_categorias',
                        'alias' => 'ProdutosCategorias',
                        'type' => 'INNER',
                        'conditions' => array(
                            'ProdutosCategorias.categoria_id' => $ids,
                            'ProdutosCategorias.produto_id = Produto.id'
                        )
                    )
                )
                    )
            );

            $produtos_arr = array();
            foreach ($produtos as &$produto) {
                $produtos_arr[$produto['Produto']['id']] = $produto['Produto']['sku'] . ' - ' . $produto['Produto']['nome'];
            }
            die(json_encode($produtos_arr));
        } else {
            die(json_encode(false));
        }
    }

    function ajax_vitrines($nivel1_id = false) {
        $this->layout = false;
        $this->set('vitrines', $this->Vitrine->BuscaVitrines($nivel1_id));
    }

    private function limparCache() {
        Cache::write('vitrines_normal', false);
        Cache::write('vitrines_destaque', false);
        Cache::write('vitrines_routes', false);
		Cache::write('vitrines_promocao', false);
    }
}
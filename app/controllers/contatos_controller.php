<?php

class ContatosController extends AppController {

    public $uses = array("Contato");
    public $components = array("Session", "Email");
    public $helpers = array("Image", "Flash", "Html", "Estados", "Javascript");

    public function index() {

        if (!empty($this->data)) {
            $this->Contato->set($this->data);
            if ($this->Contato->save($this->data)) {
                if ($this->sendMail($this->data)) {
                    $this->Session->setFlash('Contato realizado com sucesso!', 'flash/success');
                    $this->redirect(array("controller" => "contato", "action" => "index"));
                } else {
                    $this->Session->setFlash('Ocorreu um erro inesperado, tente novamente!', 'flash/error');
                }
            } else {
                $this->Session->setFlash('Por favor corrija os campos em destaque e tente novamente', 'flash/error');
            }
        }
        $this->set('breadcrumbs', array(array('nome' => 'Contato', 'link' => '/contato')));
    }

    public function admin_index() {
        $this->Contato->recursive = 1;
        $contatos = $this->paginate('Contato');
        $this->set('contatos', $contatos);
    }

    public function admin_lido($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Contato->id = $id;
        if ($this->Contato->saveField('lido', true)) {
            $this->Session->setFlash('Contato Lido com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser alterado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    public function admin_nlido($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Contato->id = $id;
        if ($this->Contato->saveField('lido', false)) {
            $this->Session->setFlash('Contato Marcado como não Lido', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser alterado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    private function sendMail($dados) {
        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.smtp_user') . ">";
        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->subject = Configure::read('Loja.nome') ."Contato - " . Configure::read('loja_nome');

        $email = str_replace(array(
            '{CONTATO_NOME}',
            '{CONTATO_ESTADO}',
            '{CONTATO_CIDADE}',
            '{CONTATO_TELEFONE}',
            '{CONTATO_MENSAGEM}',
            '{CONTATO_DATA}',
            '{CONTATO_IP}'
                ), array(
            $dados['Contato']['nome'],
            $dados['Contato']['estado'],
            $dados['Contato']['cidade'],
            $dados['Contato']['telefone'],
            $dados['Contato']['mensagem'],
            date("d/m/Y H:i:s"),
            $_SERVER['REMOTE_ADDR']
                ), Configure::read('LojaTemplate.contato')
        );
        if (Configure::read('Reweb.nome_bcc')) {
            $bccs = array();
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
               if($bcc != ""){
                    $bccs[] = "{$assunto} <{$bcc}>";
                }
            }
            $this->Email->bcc = $bccs;
        }
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

}

?>
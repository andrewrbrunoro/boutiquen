<?php

/**
 * BakeSale shopping cart
 * Copyright (c)	2006-2009, Matti Putkonen
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright (c) 2006-2009, Matti Putkonen
 * @link			http://bakesalepro.com/
 * @package			BakeSale
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Manages product categories
 *
 */
class FlagsController extends AppController {

    public $uses = array('Flag', 'Produto');
    public $helpers = array('Image', 'Tree', 'String', 'Parcelamento');
    public $components = array('Filter', 'Session');
    private $conditions = array();

    
    public function admin_add() {
        if (!empty($this->data)) {
            unset($this->data['thumb_filename']);
            
            if ($this->Flag->saveAll($this->data)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $flags = $this->Flag->find("list", array('recursive' => -1, 'fields' => array('Flag.id', 'Flag.nome'), 'order' => 'Flag.nome'));
        $this->set('flags', $flags);
    }

    /**
     * Edits information on a category.
     *
     * @param id int
     * The ID field of the category to edit.
     */
    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {

            $this->data['Flag']['id'] = $id;

            if ($this->data['Flag']['thumb_remove'] == 1) {
                $this->data['Flag']['thumb_filename'] = "";
                $this->data['Flag']['thumb_dir'] = "";
                $this->data['Flag']['thumb_mimetype'] = "";
                $this->data['Flag']['thumb_filesize'] = "";
            }
            
            
            if ($this->Flag->saveAll($this->data)) {
                //limpo o cache
                $this->limparCache();

                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        
        $flags = $this->Flag->find("list", array('recursive' => -1, 'fields' => array('Flag.id', 'Flag.nome'), 'order' => 'Flag.nome'));
        $this->set('flags', $flags);

        if (empty($this->data)) {
            $this->data = $this->Flag->read(null, $id);
        }
    }

    /**
     * Displays all cateogries in the admin page.
     *
     * The categories are retrieved for display on screen. Makes use of the parent-child relationship of categories.
     *
     * @return The array as returned by the Model::findAllThreaded() method. Results are sorted by sort.
     */
    public function admin_menu() {

        return $this->Categoria->_menu('admin');
    }

    public function admin_index() {
        $this->Flag->recursive = 0;
        $this->set('flags', $this->paginate());
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!$this->Produto->find('first', array('conditions' => array('Produto.flag_id' => $id)))) {
            if ($this->Flag->delete($id)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash('O Registro não pode ser deletado, pois possui registros vinculados', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    /**
     * if using storefronts check that the root category is correct.
     */
    private function _validateCategory($id) {
        $valid = $this->Flag->isValidCategory((int) $id);
        if (!$valid) {
            $this->redirect('/');
        }
        return true;
    }


    private function limparCache() {
        Cache::write('menu_topo', false);

    }

    

}

?>

<?php

class XmlController extends AppController {

    var $name = 'Xml';
    var $components = array('Session');
    var $helpers = array('Calendario', 'String', 'Flash', 'Javascript');
    var $cron = false;

    function view($id) {
        return $this->admin_gera_xml($id, false);
    }

    function admin_index() {
        $this->Xml->recursive = 0;
        $this->paginate = array('limit' => 1000);
        $this->set('xmls', $this->paginate());
    }

    function admin_add() {

        if (!empty($this->data)) {

            $this->Xml->create();

            App::import('Model', 'Grade');
            $this->Grade = new Grade();

            if ($this->data['Xml']['all'] == true) {
                $grades = $this->Grade->find('all', array(
                                                            'recursive' => -1, 
                                                            'fields' => array('Grade.id'), 
                                                            'conditions' => array('Grade.status > ' => 0, 'Grade.preco >' => 0, 'Grade.quantidade_disponivel > ' => 0)
                                                        )
                                            );
                $this->data['Xml']['grades_ids'] = Set::extract('/Grade/id', $grades);
            }
            
//            $grades = $this->Grade->find('all', array(
//                                                    'recursive' => -1, 
//                                                    'fields' => array('Grade.*','VariacaoProduto.*','Produto.*', 'Categoria.*', '(SELECT concat("uploads/produto_imagem/filename/",GradeImagem.filename) FROM grade_imagens AS GradeImagem WHERE GradeImagem.grade_id = Grade.id ORDER BY GradeImagem.ordem LIMIT 1) AS imagem'), 
//                                                    'conditions' => array('Grade.status > ' => 0, 'Grade.preco >' => 0, 'Grade.quantidade_disponivel > ' => 0),
//                                                    'joins' => array(
//                                                        array(
//                                                            'table' => 'variacoes_produtos',
//                                                            'alias' => 'VariacaoProduto',
//                                                            'type' => 'LEFT',
//                                                            'conditions' => array('VariacaoProduto.grade_id = Grade.id')
//                                                        ),
//                                                        array(
//                                                            'table' => 'produtos',
//                                                            'alias' => 'Produto',
//                                                            'type' => 'LEFT',
//                                                            'conditions' => array('Produto.id = VariacaoProduto.produto_id')
//                                                        ),
//                                                        array(
//                                                            'table' => 'produtos_categorias',
//                                                            'alias' => 'ProdutoCategoria',
//                                                            'type' => 'LEFT',
//                                                            'conditions' => array('ProdutoCategoria.produto_id = Produto.id')
//                                                        ),
//                                                        array(
//                                                            'table' => 'categorias',
//                                                            'alias' => 'Categoria',
//                                                            'type' => 'LEFT',
//                                                            'conditions' => array('Categoria.id = ProdutoCategoria.categoria_id')
//                                                        ),
//                                                    )
//                                                )
//                                    );
//            
//            debug($grades);die;
            
            if ($this->Xml->save($this->data)) {
                $dados = $this->Xml->find('first', array('conditions' => array('Xml.id' => $this->Xml->id)));

                $this->admin_gera_xml($dados['Xml']['url']);
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                App::import('model', 'Grade');
                $this->Grade = new Grade();
               
                if (!empty($this->data['Xml']['grades_ids'])) {
                    $grades = $this->Grade->find('all', array(
                                                            'recursive' => -1, 
                                                            'fields' => array('Grade.id','Grade.sku','Produto.nome','Variacao.valor','VariacaoTipo.nome'), 
                                                            'conditions' => array('Grade.id' => $this->data['Xml']['grades_ids']),
                                                            'joins' => array(
                                                                array(
                                                                    'table' => 'variacoes_produtos',
                                                                    'alias' => 'VariacaoProduto',
                                                                    'type' => 'LEFT',
                                                                    'conditions' => array('VariacaoProduto.grade_id = Grade.id')
                                                                ),
                                                                array(
                                                                    'table' => 'variacoes',
                                                                    'alias' => 'Variacao',
                                                                    'type' => 'INNER',
                                                                    'conditions' => array('Variacao.id = VariacaoProduto.variacao_id')
                                                                ),
                                                                array(
                                                                    'table' => 'variacao_tipos',
                                                                    'alias' => 'VariacaoTipo',
                                                                    'type' => 'INNER',
                                                                    'conditions' => array('Variacao.variacao_tipo_id = VariacaoTipo.id')
                                                                ),
                                                                array(
                                                                    'table' => 'produtos',
                                                                    'alias' => 'Produto',
                                                                    'type' => 'LEFT',
                                                                    'conditions' => array('Produto.id = VariacaoProduto.produto_id')
                                                                )
                                                            )
                                                        )
                                            );
                }
                
                $final = array();
                if ($grades) {
                    $nome = '';
                    $grade_anterior = null;
                    foreach ($grades as $allgrades) {
                        if($allgrades['Grade']['id'] != $grade_anterior){
                            $grade_anterior = $allgrades['Grade']['id'];
                            $nome = $allgrades['Grade']['sku'] .' '. $allgrades['Produto']['nome'] . ' #';
                        }else{
                            $nome = '';
                        }

                        if (isset($final[$allgrades['Grade']['id']])) {
                            $final[$allgrades['Grade']['id']] .= ' # ' . $allgrades['VariacaoTipo']['nome'] . ': ' . $allgrades['Variacao']['valor'] . ' ';
                        } else {
                            $final[$allgrades['Grade']['id']] = $nome . ' ' . $allgrades['VariacaoTipo']['nome'] . ': ' . $allgrades['Variacao']['valor'] . ' ';
                        }
                    }
                }
                
                
                //                foreach ($grades as $allgrades) {
                //                    $final[$allgrades['Grade']['id']] = $allgrades['Grade']['sku'] . ' - ' . $allgrades['Produto']['nome'];
                //                }
                $this->data['Xml']['grades_ids'] = $final;

                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        //set XmlTipo
        App::import('Model', 'XmlTipo');
        $this->XmlTipo = new XmlTipo();
        $xml_tipos = array('' => 'Selecione') + $this->XmlTipo->find('list', array('conditions' => array('XmlTipo.status' => true), 'fields' => array('id', 'nome')));
        $this->set(compact('xml_tipos'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Xml']['id'] = $id;
            $this->data['Xml']['produtos_ids'] = json_encode($this->data['Xml']['produtos_ids']);
            $this->Xml->id = $id;

            if ($this->Xml->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Xml->read(null, $id);
        }
        //set XmlTipo
        App::import('Model', 'XmlTipo');
        $this->XmlTipo = new XmlTipo();
        $xml_tipos = array('' => 'Selecione') + $this->XmlTipo->find('list', array('conditions' => array('XmlTipo.status' => true), 'fields' => array('id', 'nome')));
        $this->set(compact('xml_tipos'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        } else {
            $xml_url_arquivo = $this->Xml->find('first', array('conditions' => array('Xml.id' => $id)));
        }
        if ($this->Xml->delete($id) && unlink(WWW_ROOT . 'xmls' . DS . $xml_url_arquivo['Xml']['url'] . '.xml')) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_gera_xml($url = null) {


        if($_SERVER['REMOTE_ADDR'] == '179.219.197.193'){
           $this->cron = true;
        }

        ///ajuste para pegar o caminho absoluto, pois quando roda a cron do host, não está pegando
        $absoluto = '';                
        if($this->cron){
          ///$absoluto = 'http://www.melbenett.com.br';
        }

        //Configure::write ('debug', 0);        
        //import helper
        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        App::import("helper", "String");
        $this->String = new StringHelper();

        //import Produto
        App::import('Model', 'Produto');
        $this->Produto = new Produto();
        
        App::import('Model', 'Grade');
        $this->Grade = new Grade();
        
        App::import('Model', 'Fabricante');
        $this->Fabricante = new Fabricante();

        App::import('Model', 'Xml');
        $this->Xml = new Xml();

        //import PagamentoCondicao
        App::import('Model', 'PagamentoCondicao');
        $this->PagamentoCondicao = new PagamentoCondicao();

        //import Categoria
        App::import('Model', 'Categoria');
        $this->Categoria = new Categoria();

        //Import core file
        App::import('Core', 'File');


        $xml = $this->Xml->find('first', array('conditions' => array('Xml.url' => $url)));

        $xml_content = "";
        $xml_content .= $xml['Xml']['template_cabecalho'];

        //replaces cabecalhos
        $xml_content = str_replace('{{cabecalho_title}}', Configure::read('Loja.nome'), $xml_content);
        $xml_content = str_replace('{{cabecalho_link}}', $absoluto.$this->Html->Url('/', true), $xml_content);
        $xml_content = str_replace('{{cabecalho_descricao}}', Configure::read('Loja.titulo'), $xml_content);

        //grades
        $grades = $this->Grade->find('all', array(
                                                            'recursive' => -1, 
                                                            'fields' => array('DISTINCT Grade.id','VariacaoProduto.*','Produto.*','Categoria.*','Variacao.valor','VariacaoTipo.nome','(SELECT concat("uploads/produto_imagem/filename/",GradeImagem.filename) FROM grade_imagens AS GradeImagem WHERE GradeImagem.grade_id = Grade.id ORDER BY GradeImagem.ordem LIMIT 1) AS imagem'), 
                                                            'conditions' => array('Grade.id' => json_decode($xml['Xml']['grades_ids']),'Categoria.id IS NOT NULL', 'Categoria.id !=' => '33','Produto.nome !=' => ''),
                                                            'group' => array('Grade.id'),
                                                            'joins' => array(
                                                                array(
                                                                    'table' => 'variacoes_produtos',
                                                                    'alias' => 'VariacaoProduto',
                                                                    'type' => 'LEFT',
                                                                    'conditions' => array('VariacaoProduto.grade_id = Grade.id')
                                                                ),
                                                                array(
                                                                    'table' => 'variacoes',
                                                                    'alias' => 'Variacao',
                                                                    'type' => 'INNER',
                                                                    'conditions' => array('Variacao.id = VariacaoProduto.variacao_id')
                                                                ),
                                                                array(
                                                                    'table' => 'variacao_tipos',
                                                                    'alias' => 'VariacaoTipo',
                                                                    'type' => 'INNER',
                                                                    'conditions' => array('Variacao.variacao_tipo_id = VariacaoTipo.id')
                                                                ),
                                                                array(
                                                                    'table' => 'produtos',
                                                                    'alias' => 'Produto',
                                                                    'type' => 'LEFT',
                                                                    'conditions' => array('Produto.id = VariacaoProduto.produto_id')
                                                                ),
                                                                array(
                                                                    'table' => 'produtos_categorias',
                                                                    'alias' => 'ProdutoCategoria',
                                                                    'type' => 'LEFT',
                                                                    'conditions' => array('ProdutoCategoria.produto_id = Produto.id')
                                                                ),
                                                                array(
                                                                    'table' => 'categorias',
                                                                    'alias' => 'Categoria',
                                                                    'type' => 'LEFT',
                                                                    'conditions' => array('Categoria.id = ProdutoCategoria.categoria_id')
                                                                ),
                                                            )
                                                        )
                                            );

        //contador
        $cont = 1;
        //coeficiente do juros
        $coef = array(4 => 1.02490, 5 => 1.03750, 6 => 1.05019, 7 => 1.06300, 8 => 1.07589, 9 => 1.08889, 10 => 1.10200, 11 => 1.11530, 12 => 1.12860);
        //parcelamento
        $parcelamento_condicao = $this->PagamentoCondicao->find('first', array('recursive' => -1, 'conditions' => array('PagamentoCondicao.status' => true)));
        //percorre produtos

       
        foreach ($grades as $grade):
 
            //            if (trim($grade['Produto']['descricao']) == '') {
            //                continue;
            //            }

            //produto imagem
            if (!empty($grade[0]['imagem'])) {
                $produto_imagem = '/'.$grade[0]['imagem'];
            } else {
                $produto_imagem = '/img/site/logo.png';
            }

            //produto categoria
            $produto_categoria = null;

            $seo_url = "";
            
//            foreach ($grade['Categoria'] as $categoria) {
//                $categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.id' => $categoria['parent_id'])));
//                if ($categoria_nome['Categoria']['nome'] != 'marcas' && $categoria_nome['Categoria']['nome'] != 'Comprar por') {
//                    $seo_url = $categoria['seo_url'];
//                    break;
//                }
//            }
            //$categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.id' => $grade['Categoria']['id'])));
            $seo_url = $grade['Categoria']['seo_url'];  

            $quebra = explode("/", $seo_url);
            $url_atual = "";
            $produto_categoria_pai ="";
            foreach ($quebra as $key => $cat_url) {
                if($key==0){
                    $produto_categoria_pai = $cat_url;
                }
                $url_atual .= $cat_url;

                $categoria_nome = $this->Produto->Categoria->find('first', array('recursive' => -1, 'fields' => 'Categoria.nome', 'conditions' => array('Categoria.seo_url' => $url_atual)));

                $categorias[] = array('url' => 'categorias/' . $url_atual, 'nome' => $categoria_nome['Categoria']['nome']);

                $url_atual = $url_atual . "/";
            } 


            $catega = array();
            foreach ($categorias as $categoriax) {
                $catega[] = $categoriax['nome'];
            }
            $categorias = array();
            $produto_categoria = implode(' - ', $catega);
          
            $catega = array();

            $produto_categoria = strip_tags(html_entity_decode($this->CoverteTexto($produto_categoria)));
            $produto['Produto']['marca'] = strip_tags(html_entity_decode($this->CoverteTexto($grade['Produto']['marca'])));
            if(empty($produto['Produto']['marca']) && $grade['Produto']['fabricante_id'] != ''){
                $fabricante = $this->Fabricante->find('first', array('recursive' => -1, 'fields' => array('Fabricante.nome'), 'conditions' => array('Fabricante.id' => $grade['Produto']['fabricante_id'])));
                $produto['Produto']['marca'] = $fabricante['Fabricante']['nome'];
            }

            //produto preco            
            $grade_preco = $grade['Produto']['preco'];
            if ($grade['Produto']['preco_promocao'] > 0) {
                $grade_preco = $grade['Produto']['preco_promocao'];
            }

            if (is_string($grade_preco)) {
                $valor = (str_replace(',', '.', $grade_preco));
            }
            $juros_apartir = $parcelamento_condicao['PagamentoCondicao']['juros_apartir'];
            $juros = false;

            for ($i = 1; $i <= (int) $parcelamento_condicao['PagamentoCondicao']['parcelas']; $i++):

                $parcela = $valor / $i;

                if ($i > $juros_apartir && up($parcelamento_condicao['PagamentoCondicao']['sigla']) == 'PAGSEGURO') {
                    $parcela = $valor * $coef[$i] / $i;
                    $juros = true;
                }
                if ($i == 1 || $this->String->moedaToBco($parcela) > $this->String->moedaToBco(Configure::read('Loja.valor_minimo_parcelamento'))):
                    $parcelado = $parcela;
                    $parcelas = $i;
                endIf;
            endFor;


            if ($juros) {
                $parcelamento_juros = " com juros";
            } else {
                $parcelamento_juros = " sem juros";
            }

            $valor_parcelado_br = number_format($parcelado, 2, ",", "");
            $valor_parcelado_us = number_format($parcelado, 2, ".", "");
            
            if(isset($grade_preco)){
                $grade_preco_br = number_format($grade_preco, 2, ",", "");
                $grade_preco_us = number_format($grade_preco, 2, ".", "");
            }

            $grade_preco_promocao_br = number_format($grade['Produto']['preco_promocao'], 2, ",", "");
            $grade_preco_promocao_us = number_format($grade['Produto']['preco_promocao'], 2, ".", "");

            //produto nome
          $produto_nome = strip_tags(html_entity_decode($this->CoverteTexto($grade['Produto']['nome'])));

            //produto descricao
            $produto_descricao = strip_tags(html_entity_decode($this->CoverteTexto($grade['Produto']['descricao'])));

            //produto descricao resumida
            $produto_descricao_resumida = strip_tags(html_entity_decode($this->CoverteTexto($grade['Produto']['descricao_resumida'])));

            //valida disponibidade
            if (!empty($grade['Produto']['quantidade']) && $grade['Produto']['quantidade'] > 1) {
                $availability = "in stock";
            } else {
                $availability = 'out of stock';
            }

            //replaces
            $xml_bloco = $xml['XmlTipo']['template_centro'];


            if ((isset($xml['Xml']['xml_tipo_id']) && $xml['Xml']['xml_tipo_id'] == 1) || (isset($tipo_id) && $tipo_id == 1)) {
                /* insert midia google */
                $xml_bloco = str_replace('{{produto_link}}',$absoluto.$this->Html->Url("/" . low(Inflector::slug($produto_nome, '-')) . '-prod-' . $grade['Grade']['id'],true).'.html'.'?utm_source=google-cpc&amp;utm_medium=google-cpc&amp;utm_campaign=google-cpc&amp;midia=google-cpc',$xml_bloco);

            }else{ 
                $xml_bloco = str_replace('{{produto_link}}', $absoluto.$this->Html->Url("/" . low(Inflector::slug($grade['Produto']['nome'], '-')) . '-prod-' . $grade['Grade']['id'], true) . '.html', $xml_bloco);
            }
          
            //google shoopping
            $xml_bloco = str_replace('{{content_g:image_link}}', $absoluto . $this->Html->Url((($produto_imagem)), true), $xml_bloco);
            $xml_bloco = str_replace('{{content_g:price_[00,00]}}', $grade_preco_br, $xml_bloco);
            $xml_bloco = str_replace('{{content_g:price_[00.00]}}', $grade_preco_us, $xml_bloco);
            $xml_bloco = str_replace('{{content_g:condition}}', "new", $xml_bloco);
            $xml_bloco = str_replace('{{content_g:id}}', $grade['Grade']['id'], $xml_bloco);          
            $xml_bloco = str_replace('{{content_g:brand}}', $produto['Produto']['marca'], $xml_bloco);
            $xml_bloco = str_replace('{{content_g:product_type}}', $produto_categoria, $xml_bloco);
            $xml_bloco = str_replace('{{content_g:google_product_category}}', $produto_categoria, $xml_bloco);
            $xml_bloco = str_replace('{{content_g:availability}}', $availability, $xml_bloco);
            $xml_bloco = str_replace('{{content_g:description}}', $produto_descricao, $xml_bloco);
            $xml_bloco = str_replace('{{content_g:mpn}}', $grade['Produto']['codigo_externo'], $xml_bloco);            

            //demais
            $xml_bloco = str_replace('{{content_contador}}', $cont, $xml_bloco);
            $xml_bloco = str_replace('{{produto_nome}}', $produto_nome, $xml_bloco);
            
            $xml_bloco = str_replace('{{produto_descricao}}',$produto_descricao, $xml_bloco);
            $xml_bloco = str_replace('{{produto_descricao_resumida}}', $produto_descricao_resumida, $xml_bloco);
            $xml_bloco = str_replace('{{produto_id}}', $grade['Grade']['id'], $xml_bloco);
            $xml_bloco = str_replace('{{produto_sku}}', $grade['Produto']['sku'], $xml_bloco);
            $xml_bloco = str_replace('{{produto_preco_[00,00]}}', $grade_preco_br, $xml_bloco);
            $xml_bloco = str_replace('{{produto_preco_[00.00]}}', $grade_preco_us, $xml_bloco);
            $xml_bloco = str_replace('{{produto_preco_promocao_[00,00]}}', $grade_preco_promocao_br, $xml_bloco);
            $xml_bloco = str_replace('{{produto_preco_promocao_[00.00]}}', $grade_preco_promocao_us, $xml_bloco);
            $xml_bloco = str_replace('{{produto_parcelamento_parcelas}}', $parcelas, $xml_bloco);
            $xml_bloco = str_replace('{{produto_parcelamento_juros}}', $parcelamento_juros, $xml_bloco);
            $xml_bloco = str_replace('{{produto_parcelamento_valor_[00.00]}}', $valor_parcelado_us, $xml_bloco);
            $xml_bloco = str_replace('{{produto_parcelamento_valor_[00,00]}}', $valor_parcelado_br, $xml_bloco);
            $xml_bloco = str_replace('{{produto_parcelamento_parcela}}', $parcelas, $xml_bloco);
            $xml_bloco = str_replace('{{produto_disponibilidade}}', ($grade['Produto']['quantidade_disponivel'] > 0 ) ? 'Entrega imediata' : 'Indisponível', $xml_bloco);
            $xml_bloco = str_replace('{{produto_imagem}}', $absoluto.$this->Html->Url((($produto_imagem)), true), $xml_bloco);
            $xml_bloco = str_replace('{{produto_categoria}}', $produto_categoria, $xml_bloco);
            ///$xml_bloco = str_replace('{{produto_marca}}',$produto['Produto']['marca'], $xml_bloco);
            ///a marca deve vir de categoria
            //$xml_bloco = str_replace('{{produto_marca}}',$produto_categoria, $xml_bloco);


            $xml_content .= $xml_bloco;

              


            $cont++;

            $produto_categoria = null;
            unset($produto_categoria);
        endforeach;

        $xml_content .= $xml['Xml']['template_rodape'];

        $xmlFile = new File("xmls/" . $xml['Xml']['url'] . ".xml");
        $xmlFile->write($xml_content, 'w');

        $xmlFile->close();
    }

    private function CoverteTexto($str) {

        setlocale(LC_ALL, 'pt_BR.UTF-8');

        $codigo_acentos = array('/Â/', '/>/', '/</', '/&/',
            '/&Agrave;/', '/&Aacute;/', '/&Acirc;/', '/&Atilde;/', '/&Auml;/', '/&Aring;/',
            '/&agrave;/', '/&aacute;/', '/&acirc;/', '/&atilde;/', '/&auml;/', '/&aring;/',
            '/&Ccedil;/',
            '/&ccedil;/',
            '/&Egrave;/', '/&Eacute;/', '/&Ecirc;/', '/&Euml;/',
            '/&egrave;/', '/&eacute;/', '/&ecirc;/', '/&euml;/',
            '/&Igrave;/', '/&Iacute;/', '/&Icirc;/', '/&Iuml;/',
            '/&igrave;/', '/&iacute;/', '/&icirc;/', '/&iuml;/',
            '/&Ntilde;/',
            '/&ntilde;/',
            '/&Ograve;/', '/&Oacute;/', '/&Ocirc;/', '/&Otilde;/', '/&Ouml;/',
            '/&ograve;/', '/&oacute;/', '/&ocirc;/', '/&otilde;/', '/&ouml;/',
            '/&Ugrave;/', '/&Uacute;/', '/&Ucirc;/', '/&Uuml;/',
            '/&ugrave;/', '/&uacute;/', '/&ucirc;/', '/&uuml;/',
            '/&Yacute;/',
            '/&yacute;/', '/&yuml;/',
            '/&ordf;/',
            '/&ordm;/',
            '/&quot;/');

        $acentos = array(' ', ' ', ' ', 'e',
            'À', 'Á', 'Â', 'Ã', 'Ä', 'Å',
            'à', 'á', 'â', 'ã', 'ä', 'å',
            'Ç',
            'ç',
            'È', 'É', 'Ê', 'Ë',
            'è', 'é', 'ê', 'ë',
            'Ì', 'Í', 'Î', 'Ï',
            'ì', 'í', 'î', 'ï',
            'Ñ',
            'ñ',
            'Ò', 'Ó', 'Ô', 'Õ', 'Ö',
            'ò', 'ó', 'ô', 'õ', 'ö',
            'Ù', 'Ú', 'Û', 'Ü',
            'ù', 'ú', 'û', 'ü',
            'Ý',
            'ý', 'ÿ',
            'ª',
            'º',
            "'");
        return utf8_encode(utf8_decode(preg_replace($codigo_acentos, $acentos, $str)));
    }


    function cron_xml_googleshopping() {


        App::import('Model', 'XmlTipo');
        $this->XmlTipo = new XmlTipo();
        $xml_tipos = $this->XmlTipo->find('all', array('conditions' => array('XmlTipo.id' => 1), 'fields' => array('id', 'nome','template_cabecalho','template_centro','template_rodape')));        

        App::import('Model', 'XmlVariavel');
        $this->XmlVariavel = new XmlVariavel();
        $xml_variaveis = $this->XmlVariavel->find('all', array('conditions' => array('XmlVariavel.xml_tipo_id' => 1), 'fields' => array('valor')));        

        $item_xml_variaveis = array();
        foreach($xml_variaveis as $value) {       
            array_push($item_xml_variaveis, $value['XmlVariavel']['valor']);                          
        }       
        $template_variaveis = implode(",", $item_xml_variaveis);
        $nome = "googleshopping";      

        //deleta da lista os xmls anteriores        
        $this->Xml->deleteAll(array('Xml.nome' => $nome), false);  

        $this->data['Xml']['xml_tipo_id']=$xml_tipos[0]['XmlTipo']['id'];
        $this->data['Xml']['nome'] = $nome;
        $this->data['Xml']['template_variaveis'] = $template_variaveis;
        $this->data['Xml']['template_cabecalho'] = $xml_tipos[0]['XmlTipo']['template_cabecalho'];
        $this->data['Xml']['template_centro'] = $xml_tipos[0]['XmlTipo']['template_centro'];
        $this->data['Xml']['template_rodape'] = $xml_tipos[0]['XmlTipo']['template_rodape'];
        $this->data['Xml']['all']=1;
        $this->data['Xml']['Buscar'] ='';
        $this->data['Xml']['Buscar_marca']='';
        $this->data['Xml']['Selecionar']='';
        $this->data['Xml']['grades_ids']='';

        if (!empty($this->data)) {            

            $this->Xml->create();

            App::import('Model', 'Grade');
            $this->Grade = new Grade();
          
            $grades = $this->Grade->find('all', array(
                                        'recursive' => -1, 
                                        'fields' => array('Grade.id'), 
                                        'conditions' => array('Grade.status > ' => 0, 'Grade.preco >' => 0, 'Grade.quantidade_disponivel > ' => 1)                                        
                                        )
                            );
            $this->data['Xml']['grades_ids'] = Set::extract('/Grade/id', $grades);         
           
            

            
            if ($this->Xml->save($this->data)) {
                $dados = $this->Xml->find('first', array('conditions' => array('Xml.id' => $this->Xml->id)));     

                $this->admin_gera_xml($dados['Xml']['url']);
                die('Finalizado');
                $this->log('Gerado em: '.PHP_EOL);
                ///$this->redirect(array('action' => 'admin_index'));
            } else {
                die('Erro ao gerar Xml');
                $this->log('Erro ao gerar Xml: '.PHP_EOL);
            }
        }
    }        

}

?>
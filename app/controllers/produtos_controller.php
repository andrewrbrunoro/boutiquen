<?php

    class ProdutosController extends AppController
    {
        public $uses = array(
            'Produto',
            'VariacaoProduto',
            'ProdutoCategoria',
            'GradeImagem'
        );

        public function beforeFilter()
        {
            parent::beforeFilter();
            $this->Auth->allow('*');
        }

        /**
         * @param $grade_id
         * @return mixedA
         *
         * Pablo, automatizar esta função.
         * Estou transformando a cor em uma flag, ou seja, caso a grade selecionada seja da cor Azul, eu pego todas as
         * grades com a cor Azul, trazendo todos os números dele
         *
         * Eu não lembro, mas pensando agora, imagino que qualquer produto vai ter uma variação padrão... Como o caso
         * da cor.
         */
        public function detalhe($grade_id)
        {
            if (!$grade_id) {
                $this->Session->setFlash('Desculpa, não encontramos este produto :(', 'flash/error');
                return $this->redirect($this->referer());
            }
            $product = $this->Produto->getDetail($grade_id);

            $colors = $this->Produto->getColors($product, $grade_id);

            $sizes = $this->Produto->getSizesByColor($product, $grade_id);

            $current_variation = $this->Produto->getCurrentVariation($grade_id);

            $images = $this->GradeImagem->getImages($grade_id);

            $this->set(compact('product', 'colors', 'sizes', 'images', 'current_variation', 'grade_id'));
        }

        public function changeDetail()
        {

        }

    }
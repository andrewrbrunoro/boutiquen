<?php

class ServicosController extends AppController {

    var $name = 'Servicos';
    var $components = array('Session');
    var $helpers = array('Image', 'Calendario', 'String', 'Javascript', 'Uploadify');
    
    function index($seo_url){
        $servico = $this->Servico->find('first', array('conditions' => array('Servico.seo_url' => $seo_url)));
        $this->set('servico', $servico);
        
        $this->set('servicos', $this->Servico->find('all', array(
                                                                'recursive' => -1, 
                                                                'fields' => array('Servico.nome','Servico.seo_url'),
                                                                'conditions' => array('Servico.id <>' => $servico['Servico']['id']),
                                                                //'order' => array('Servico.id ASC')
                                                                )
                                                    )
                 );
    }
    
    function admin_index() {
        $this->Servicos->recursive = 0;
        $this->set('servicos', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            if ($this->Servico->save($this->data)) {
                 if (is_array($this->Session->read("TMP.ServicoImagem"))) {
                    foreach ($this->Session->read("TMP.ServicoImagem") as $img):
                        $this->data['ServicoImagem']['servico_id'] = $this->Servico->id;
                        $this->data['ServicoImagem']['id'] = '';
                        $this->data['ServicoImagem']['dir'] = '';
                        $this->data['ServicoImagem']['mimetype'] = '';
                        $this->data['ServicoImagem']['filesize'] = '';
                        $this->data['ServicoImagem']['ordem'] = $img['ordem'];
                        $this->data['ServicoImagem']['status'] = 1;
                        $this->data['ServicoImagem']['filename']['type'] = 'image/jpeg';
                        $this->data['ServicoImagem']['filename']['name'] = $img['tmp_file'];
                        $this->data['ServicoImagem']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file'];
                        $this->data['ServicoImagem']['filename']['error'] = $img['error'];
                        $this->data['ServicoImagem']['filename']['size'] = $img['size'];
                        $this->data['ServicoImagem']['filename']['created'] = time();
                        $this->data['ServicoImagem']['filename']['modified'] = time();
                        $this->Servico->ServicoImagem->save($this->data);
                        unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $img['tmp_file']);
                    endForeach;
                    $this->Session->delete("TMP.ServicoImagem");
                }
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $imgs = (array) $this->Session->read('TMP.ServicoImagem');
        $this->set('imgs', $this->FlashImages($imgs));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Servico->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Servico->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
        
        if(isset($this->data['ServicoImagem']) && count($this->data['ServicoImagem']) > 0){
            $this->set('imgs', $this->FlashImages($this->data['ServicoImagem']));
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Servico->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }
    
    function formatbytes($file, $type = "MB") {
        switch ($type) {
            case "KB":
                $filesize = filesize($file) * .0009765625; // bytes to KB
                break;
            case "MB":
                $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
                break;
            case "GB":
                $filesize = ((filesize($file) * .0009765625) * .0009765625) * .0009765625; // bytes to GB
                break;
        }
        if ($filesize <= 0) {
            return $filesize = 'unknown file size';
        } else {
            return round($filesize, 2) . ' ' . $type;
        }
    }

    public function FlashImages($imgs) {

        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        App::import("helper", "Form");
        $this->Form = new FormHelper();
        App::import("helper", "Image");
        $this->Image = new ImageHelper();

        $string = '';
        foreach ($imgs as $img):

            if (isset($img['tmp_file'])) {
                $imagem = '../../../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
            } else if(isset($img['filename'])) {
                $imagem = '../../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
            }
            
//            3

            if (isset($img['tmp_file'])) {
                $id = $img['tmp_file'];
            } else if(isset($img['id'])) {
                $id = $img['id'];
            }
            if (isset($img['tmp_file'])) {
                $name = $img['tmp_file'];
            } else if(isset($img['filename'])) {
                $name = $img['filename'];
            }

            $string .= '
			<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">								
				<div class="cancel">
					' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
				</div>										
			<span class="fileName">
			<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[ServicoImagem][ordem]" /></label>
			<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
			<span class="percentage"> - 100%</span>											
			</div>
			</div>';
        endForeach;

        return $string;
    }

    public function admin_ajax_imagens() {

        App::import("helper", "Html");
        $this->Html = new HtmlHelper();
        App::import("helper", "Form");
        $this->Form = new FormHelper();
        App::import("helper", "Image");
        $this->Image = new ImageHelper();

        $string = '';
        foreach ($imgs as $img):

            if (isset($img['tmp_file'])) {
                $imagem = '../../' . $this->Image->resize('uploads/tmp/' . $img['tmp_file'], 40, 40, array(), null, true);
            } else {
                $imagem = '../../../' . $this->Image->resize($img['dir'] . DS . $img['filename'], 40, 40, array(), null, true);
            }

            if (isset($img['tmp_file'])) {
                $id = $img['tmp_file'];
            } else {
                $id = $img['id'];
            }
            if (isset($img['tmp_file'])) {
                $name = $img['tmp_file'];
            } else {
                $name = $img['filename'];
            }

            $string .= '
			<div class="uploadifyQueue" id="file_uploadQueue"><div class="uploadifyQueueItem">								
				<div class="cancel">
					' . (is_numeric($id) ? '<a href="javascript:;" rel="' . $id . '" class="rm rm-img-old">remover</a>' : '<a href="javascript:;" rel="' . $id . '" class="rm rm-img">remover</a>') . '
				</div>										
			<span class="fileName">
			<label>Ordem <input type="text" class="ordem" value="' . $img['ordem'] . '" rel="' . $id . '" name="data[ServicoImagem][ordem]" /></label>
			<img src="' . $imagem . '" alt="Foto" /> ' . $name . '</span>
			<span class="percentage"> - 100%</span>											
			</div>
			</div>';
        endForeach;

        return $string;
    }

    function admin_img_add() {
        App::import("helper", "Uploadify");
        $this->Uploadify = new UploadifyHelper();

        $path = "uploads/tmp/";
        if (!is_dir($path)) {
            mkdir($path);
        }

        $valid_formats = array("jpg", "png", "gif");
        if (!empty($_FILES)) {
            $name = $_FILES['Filedata']['name'];
            if (strlen($name)) {
                if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
                    $ext = $flag[2];
                    if (in_array(low($ext), $valid_formats)) {
                        $size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
                        if ($size < 100) {
                            $actual_image_name = md5(time() . $name) . "." . $ext;
                            $tmp = $_FILES['Filedata']['tmp_name'];

                            if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
                                $_FILES['Filedata']['tmp_file'] = $actual_image_name;
                                $_FILES['Filedata']['ordem'] = '';
                                //pega as imagens no tmp
                                $imgs = $this->Session->read('TMP.ServicoImagem');
                                if (isset($imgs)) {
                                    $imgs = am($imgs, array($_FILES['Filedata']));
                                } else {
                                    $imgs = array($_FILES['Filedata']);
                                }
                                $this->Session->write('TMP.ServicoImagem', $imgs);
                                echo $this->Uploadify->build($imgs, 'ServicoImagem', false, '../../');
                                exit;
                            } else {
                                echo "Falha ao mover arquivo";
                                exit;
                            }
                        } else {
                            echo "Arquivo muito grande";
                            exit;
                        }
                    } else {
                        echo "Formato de arquivo inválido.";
                        exit;
                    }
                } else {
                    echo "Formato de arquivo inválido.";
                    exit;
                }
            } else {
                echo "por favor selecione uma imagem.!";
                exit;
            }
        } else {
            echo "Falha ao enviar arquivo tente novamente.!";
            exit;
        }
    }

    function admin_img_edit($servico_id) {

        App::import("helper", "Uploadify");
        $this->Uploadify = new UploadifyHelper();
        $this->loadModel('Servico');
        $this->loadModel('ServicoImagem');
        $path = "uploads/tmp/";
        if (!is_dir($path)) {
            mkdir($path);
        }

        $valid_formats = array("jpg", "png", "gif");
        if (!empty($_FILES)) {
            $name = $_FILES['Filedata']['name'];
            if (strlen($name)) {
                if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
                    $ext = $flag[2];
                    if (in_array(low($ext), $valid_formats)) {
                        $size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
                        if ($size < 100) {
                            $actual_image_name = md5(time() . $name) . "." . $ext;
                            $tmp = $_FILES['Filedata']['tmp_name'];

                            if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
                                $_FILES['Filedata']['tmp_file'] = $actual_image_name;
                                $_FILES['Filedata']['ordem'] = '';

                                $this->ServicoImagem->create();
                                $this->data['ServicoImagem']['servico_id'] = $servico_id;
                                $this->data['ServicoImagem']['id'] = '';
                                $this->data['ServicoImagem']['dir'] = '';
                                $this->data['ServicoImagem']['mimetype'] = '';
                                $this->data['ServicoImagem']['filesize'] = '';
                                $this->data['ServicoImagem']['ordem'] = null;
                                $this->data['ServicoImagem']['status'] = 1;
                                $this->data['ServicoImagem']['filename']['type'] = 'image/jpeg';
                                $this->data['ServicoImagem']['filename']['name'] = $_FILES['Filedata']['tmp_file'];
                                $this->data['ServicoImagem']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file'];
                                $this->data['ServicoImagem']['filename']['error'] = $_FILES['Filedata']['error'];
                                $this->data['ServicoImagem']['filename']['size'] = $_FILES['Filedata']['size'];
                                $this->data['ServicoImagem']['filename']['created'] = time();
                                $this->data['ServicoImagem']['filename']['modified'] = time();
                                $this->ServicoImagem->save($this->data);
                                unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file']);
                                $servico = $this->ServicoImagem->find('all', array('recursive' => -1, 'conditions' => array('ServicoImagem.servico_id' => $servico_id)));
                                $imgs = array();
                                foreach ($servico as $img) {
                                    $imgs[] = $img['ServicoImagem'];
                                }
                                echo $this->Uploadify->build($imgs, 'ServicoImagem', false, '../../../../');
                                exit;
                            } else {
                                echo "Falha ao mover arquivo";
                                exit;
                            }
                        } else {
                            echo "Arquivo muito grande";
                            exit;
                        }
                    } else {
                        echo "Formato de arquivo inválido.";
                        exit;
                    }
                } else {
                    echo "Formato de arquivo inválido.";
                    exit;
                }
            } else {
                echo "por favor selecione uma imagem.!";
                exit;
            }
        } else {
            echo "Falha ao enviar arquivo tente novamente.!";
            exit;
        }
    }

    function admin_rm_img_tmp($remove) {

        $this->layout = false;
        $this->render(false);
        $path = "uploads/tmp/";
        //pega as imagens no tmp
        $olds = $this->Session->read('TMP.ServicoImagem');
        //se a imagem passada não está no array add a img no tmp
        foreach ($olds as $key => $img) {
            if ($img['tmp_file'] === $remove) {
                $this->Session->delete("TMP.ServicoImagem.{$key}");
                unlink($path . $img['tmp_file']);
            }
        }
        die(json_encode(true));
    }

    function admin_rm_img_old($id) {

        App::import('Model', 'ServicoImagem');
        $this->ServicoImagem = new ServicoImagem();
        $this->ServicoImagem->recursive = -1;

        if (!$id) {
            die(json_encode(false));
        }
        if ($id != "") {
            if (!$this->ServicoImagem->delete($id)) {
                die(json_encode(false));
            } else {
                die(json_encode(true));
            }
        } else {
            die(json_encode(false));
        }
    }

    function admin_img_ordem_old($id, $ordem) {
        $this->loadModel('ServicoImagem');
        if ($id && $ordem) {
            $this->ServicoImagem->id = $id;
            if ($this->ServicoImagem->saveField('ordem', $ordem)) {
                die(json_encode(true));
            }
        }
        die(json_encode(false));
    }

    function admin_img_ordem_tmp($id, $ordem) {
        $this->loadModel('ServicoImagem');
        if ($id && $ordem) {
            $tmp = $this->Session->read("TMP.ServicoImagem");
            //se a imagem passada não está no array add a img no tmp
            foreach ($tmp as $key => $img) {
                if ($img['tmp_file'] === $id) {
                    $this->Session->write("TMP.ServicoImagem.{$key}.ordem", $ordem);
                }
            }
        }
        die(json_encode(true));
    }
}

?>
<?php

class CarrinhoAbandonadoController extends AppController {

    var $name = 'CarrinhoAbandonado';
    var $components = array('Session', 'Filter', 'Email');
    var $helpers = array('String', 'Calendario', 'Image', 'Flash', 'Javascript');

    function admin_index() {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();
        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->loadModel('Produto');
        $this->Produto = new Produto();
        $this->loadModel('Grade');
        $this->Grade = new Grade();

        $filtros = array();
        if (isset($this->data['Filter']['ids_carrinho'])) {
            $aIdsToSendCupom = $this->data['Filter']['ids_carrinho'];
        }

        if ($this->data["Filter"]["cupom_enviado"]) {
            if ($this->data["Filter"]["cupom_enviado"] == 'Enviado') {
                $cupom_enviado = 1;
            } else {
                $cupom_enviado = null;
            }
            $filtros['cupom_enviado'] = "CarrinhoAbandonado.cupom_enviado = '$cupom_enviado'";
        }

        if (isset($this->data["Filter"]["finalizado"])) {
            $filtros['finalizado'] = "CarrinhoAbandonado.finalizado = '{%value%}'";
        }

        if ($this->data["Filter"]["nome"]) {
            $filtros['nome'] = "Usuario.nome LIKE '%{%value%}%'";
        }

        if ($this->data["Filter"]["email"]) {
            $filtros['email'] = "Usuario.email LIKE '%{%value%}%'";
        }

        if (isset($this->data["Filter"]["sku"])) {
            $produto = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.sku' => $this->data["Filter"]["sku"])));
            $filtros['sku'] = "CarrinhoAbandonado.produtos LIKE '%{%" . $produto['Produto']['id'] . "%}%'";
        }

        if ($this->data["Filter"]["preco"]) {
            $preco = $this->String->moedaToBco($this->data["Filter"]["preco"]);
            $filtros['preco'] = "CarrinhoAbandonado.valor_total = '$preco'";
        }

        if (isset($this->data["Filter"]["data_ini"]) && isset($this->data["Filter"]["data_fim"])) {
            if (($this->Calendario->DataDiff($this->data["Filter"]["data_ini"], $this->data["Filter"]["data_fim"]) / 86400) <= 90) {
                $intervalo_valido = true;
            } else {
                $intervalo_valido = false;
                $this->Session->setFlash('O intervalo do filtro não pode ser superior a 3 meses.', 'flash/error');
            }
        }

        if (isset($this->data["Filter"]["faixa_preco_de"]) && isset($this->data["Filter"]["faixa_preco_ate"])) {
            if ($this->data["Filter"]["faixa_preco_de"] < $this->data["Filter"]["faixa_preco_ate"] || (isset($this->data["Filter"]["faixa_preco_de"]) || isset($this->data["Filter"]["faixa_preco_ate"]))) {
                $intervalo_valido = true;
            } else {
                $intervalo_valido = false;
                $this->Session->setFlash('Faixa de preço inicial inferior a faixa de preço final.', 'flash/error');
            }
        }

        //se tiver o campo filtra
        if (isset($this->data["Filter"]["faixa_preco_de"]) && $intervalo_valido == true) {
            $faixa_preco_de = $this->String->moedaToBco($this->data["Filter"]["faixa_preco_de"]);
            $filtros['faixa_preco_de'] = "CarrinhoAbandonado.valor_total >= '$faixa_preco_de'";
        }

        //se tiver o campo filtra
        if (isset($this->data["Filter"]["faixa_preco_ate"]) && $intervalo_valido == true) {
            $faixa_preco_ate = $this->String->moedaToBco($this->data["Filter"]["faixa_preco_ate"]);
            $filtros['faixa_preco_ate'] = "CarrinhoAbandonado.valor_total <= '$faixa_preco_ate'";
        }

        //se tiver o campo filtra
        if (isset($this->data["Filter"]["data_ini"]) && $intervalo_valido == true) {
            $filtros['data_ini'] = "DATE_FORMAT(CarrinhoAbandonado.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", $this->data["Filter"]["data_ini"]) . "'";
        } else {
            $filtros['data_ini'] = "DATE_FORMAT(CarrinhoAbandonado.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')))) . "'";
        }
        //se tiver o campo filtra
        if (isset($this->data["Filter"]["data_fim"]) && $intervalo_valido == true) {
            $filtros['data_fim'] = "DATE_FORMAT(CarrinhoAbandonado.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", $this->data["Filter"]["data_fim"]) . "'";
        } else {
            $filtros['data_fim'] = "DATE_FORMAT(CarrinhoAbandonado.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y', mktime(0, 0, 0, date('m'), date('d'), date('Y')))) . "'";
        }

        $filtros['finalizado'] = "CarrinhoAbandonado.finalizado = '0'";
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        /* Relatório */
        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        /* Envio de cupons */
        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Enviar Cupom") {
            if (isset($aIdsToSendCupom)) {
                $this->admin_enviar_cupom($conditions, $aIdsToSendCupom, $this->data['carrinho_abandonado']['cupom']);
                $this->Session->setFlash('Cupons enviados com sucesso.', 'flash/success');
                $aIdsToSendCupom = array();
            } else {
                $this->Session->setFlash('Selecione os carrinhos abandonados que deseja enviar cupom.', 'flash/error');
            }
        }

        $this->CarrinhoAbandonado->recursive = 0;
        $this->paginate = array('contain' => array('Usuario'), 'limit' => 48, 'conditions' => $conditions, 'order' => 'CarrinhoAbandonado.id DESC');

        $carrinhos = $this->paginate('CarrinhoAbandonado', array('CarrinhoAbandonado.usuario_id !=' => null,'CarrinhoAbandonado.finalizado'=>0));
        foreach ($carrinhos as $chave => &$carrinho) {
            $produtos = json_decode($carrinho['CarrinhoAbandonado']['produtos'], true);
            $itens = array();
            if ($produtos) {
                foreach ($produtos as $produto) {
                    $prod = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $produto['produto_id'])));
                    $itens[$produto['quantidade']]['Produto'] = $prod['Produto'];
                    $grade = $this->Grade->find('first', array('recursive' => -1, 'conditions' => array('Grade.id' => $produto['grade_id'])));
                    $itens[$produto['quantidade']]['Grade'] = $grade['Grade'];
                }
                $carrinho['CarrinhoAbandonado']['produtos'] = $itens;
            }
            if (isset($aIdsToSendCupom)) {
                if (in_array($carrinho['CarrinhoAbandonado']['id'], $aIdsToSendCupom)) {
                    $carrinho['CarrinhoAbandonado']['checked'] = 'checked';
                } else {
                    $carrinho['CarrinhoAbandonado']['checked'] = "";
                }
            } else {
                $carrinho['CarrinhoAbandonado']['checked'] = "";
            }
        }

        if (isset($this->data['Filter']['ids_carrinho'])) {
            $carrinhos['ids_carrinhos'] += $this->data['Filter']['ids_carrinho'];
        }

        App::import('Model', 'Cupom');
        $this->Cupom = new Cupom();
        /* CUPONS TIPO CARRINHO TO LIST */
        $cupons = $this->Cupom->find('list', array('conditions'=>array('Cupom.status'=>true, 'Cupom.cupom_tipo_id =' => 2), 'fields'=>array('id','cupom')));
        $this->set('cupons', $cupons);

        $this->set('carrinhos_abandonados', $carrinhos);
    }

    function admin_updateFieldTotalDB() {
        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();
        App::import("helper", "String");
        $this->String = new StringHelper();

        $this->loadModel('Produto');
        $this->loadModel('CarrinhoAbandonado');
        $conditions = null;

        $carrinhos = $this->CarrinhoAbandonado->find('all');

        foreach ($carrinhos as $chave => &$carrinho) {
            $produtos = json_decode($carrinho['CarrinhoAbandonado']['produtos'], true);
            $itens = array();
            if ($produtos) {
                foreach ($produtos as $produto) {
                    $itens[$produto['quantidade']] = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $produto['produto_id'])));
                }
                $carrinho['CarrinhoAbandonado']['produtos'] = $itens;
            }
        }

        foreach ($carrinhos as $pedido_abandonado) {
            if ($pedido_abandonado['CarrinhoAbandonado']['produtos']) {
                $total = 0;
                foreach ($pedido_abandonado['CarrinhoAbandonado']['produtos'] as $chave => $produto) {
                    $preco = $produto['Produto']['preco_promocao'] > 0 ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
                    $total += $preco * $chave;
                }
            } else {
                $total = 0;
            }
            $this->CarrinhoAbandonado->save(array('valor_total' => $total, 'id' => $pedido_abandonado['CarrinhoAbandonado']['id']), false);
        }
        print '<pre>';
        print_r('ENCERRADA ATUALIZACAO VALOR TOTAL CARRINHO');
        print '</pre>';
        die();
    }

    function admin_exportar($conditions) {
        array_push($conditions, "Usuario.id = CarrinhoAbandonado.usuario_id");
        array_push($conditions, "CarrinhoAbandonado.usuario_id IS NOT NULL");

        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "pedidos_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');
        $this->loadModel('Produto');

        $aCarrinhosAbandonados = $this->CarrinhoAbandonado->find('all', array(
            'recursive' => 1,
            'fields' => array('CarrinhoAbandonado.id', 'CarrinhoAbandonado.usuario_id', 'CarrinhoAbandonado.produtos', 'CarrinhoAbandonado.modified', 'Usuario.nome', 'Usuario.email'),
            'contains' => array('Usuario'),
            'conditions' => $conditions,
            'group' => array('CarrinhoAbandonado.id')
            ));



        $table = "<table>";
        foreach ($aCarrinhosAbandonados as $chave => &$carrinho) {
            $produtos = json_decode($carrinho['CarrinhoAbandonado']['produtos'], true);
            $itens = array();
            if ($produtos) {
                foreach ($produtos as $produto) {
                    $prod = $this->Produto->find('first', array('recursive' => -1, 'conditions' => array('Produto.id' => $produto['produto_id'])));
                    $itens[$produto['quantidade']]['Produto'] = $prod['Produto'];
                    $grade = $this->Grade->find('first', array('recursive' => -1, 'conditions' => array('Grade.id' => $produto['grade_id'])));
                    $itens[$produto['quantidade']]['Grade'] = $grade['Grade'];
                }
                $carrinho['CarrinhoAbandonado']['produtos'] = $itens;
            }
        }

        $table .= "<tr bgcolor=\"#CECECE\">
        <td><strong>Pedido</strong></td>
        <td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Usuário") . "</strong></td>
        <td><strong>Email</strong></td>
        <td><strong>SKU</strong></td>
        <td><strong>Carrinho</strong></td>
        <td><strong>Total</strong></td>
        <td><strong>Data</strong></td>
        </tr>";
        foreach ($aCarrinhosAbandonados as $row) {
            if (is_array($row)) {
                if ($row['CarrinhoAbandonado']['produtos']) {
                    $total = 0;
                    $carrinho = '';
                    $sku = '';
                    foreach ($row['CarrinhoAbandonado']['produtos'] as $chave => $produto) {
                        $preco = $produto['Produto']['preco_promocao'] > 0 ? $produto['Produto']['preco_promocao'] : $produto['Produto']['preco'];
                        $total += $preco * $chave;
                        $sku .= Inflector::slug($produto['Produto']['sku'], " ") . '<br/>';
                        $carrinho .= Inflector::slug($produto['Produto']['nome'], " ") . '<br/>';

                        $variacoes_no_nome = '';
                        $variacoes = json_decode($produto['Produto']['grades'], true);
                        if(is_array($variacoes) && count($variacoes) > 0){
                            foreach($variacoes as $k => $var){
                                if($var['Grade']['id'] == $produto['Grade']['id']){
                                    foreach($variacoes[$k]['Variacoes'] as $vr){
                                        $variacoes_no_nome .= ' - '.iconv("UTF-8", "ISO-8859-1//IGNORE", $vr['VariacaoTipo']['nome']) .': '. iconv("UTF-8", "ISO-8859-1//IGNORE", $vr['Variacao']['valor']);
                                    }
                                    break;
                                }
                            }
                            $carrinho .= $variacoes_no_nome;
                        }
                    }
                } else {
                    $carrinho = 'Carrinho Vazio';
                    $total = 0;
                }
                $table .= "<tr>
                <td>" . $row['CarrinhoAbandonado']['id'] . "</td>
                <td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", Inflector::slug($row['Usuario']['nome'], " ")) . "</td>
                <td>" . $row['Usuario']['email'] . "</td>
                <td>" . $sku . "</td>
                <td>" . $carrinho . "</td>
                <td>" . number_format($total, 2, ",", ".") . "</td>
                <td>" . $row['CarrinhoAbandonado']['modified'] . "</td>
                </tr>";
            }
        }
        $table .= "</table>";
        die($table);
    }

    public function admin_delete($id){
        $this->CarrinhoAbandonado->recursive=-1;
        if($this->CarrinhoAbandonado->delete($id,false)){
            $this->Session->setFlash('Carrinho Abandonado excluido com sucesso.', 'flash/success');
            $this->redirect('/admin/carrinho_abandonado');
        }
    }

    function admin_enviar_cupom($conditions, $aIdsToSendCupom, $cupom_id) {
//model CarrinhoAbandonado
        App::import('Model', 'CarrinhoAbandonado');
        $this->CarrinhoAbandonado = new CarrinhoAbandonado();

        $aIdsToSendCupom2 = array();
        foreach ($aIdsToSendCupom as $key => $value) {
            if($value != ''){
                $aIdsToSendCupom2[] = $value;
            }
        }
        $aIds = implode(',', $aIdsToSendCupom2);

        array_push($conditions, "Usuario.id = CarrinhoAbandonado.usuario_id");
        array_push($conditions, "CarrinhoAbandonado.usuario_id IS NOT NULL");
        array_push($conditions, "CarrinhoAbandonado.id in ($aIds)");

//$this->layout = false;
//$this->render(false);

        $this->loadModel('Produto');
        $aCarrinhosAbandonados = $this->CarrinhoAbandonado->find('all',array(
            'recursive' => 1,
            'fields' => array('CarrinhoAbandonado.usuario_id', 'CarrinhoAbandonado.id', 'CarrinhoAbandonado.produtos', 'CarrinhoAbandonado.modified', 'Usuario.nome', 'Usuario.email', 'CarrinhoAbandonado.valor_total'),
            'contains' => array('Usuario'),
            'conditions' => $conditions,
            'order' => array('CarrinhoAbandonado.id DESC'),
            'group' => array('CarrinhoAbandonado.usuario_id')));

        foreach($aCarrinhosAbandonados as $chave => &$carrinho) {
            $produtos = json_decode($carrinho['CarrinhoAbandonado']['produtos'],true);
            $itens = array();
            if($produtos){
                foreach($produtos as $produto){
                    $itens[$produto['quantidade']] = $this->Produto->find('first', array('recursive' => -1, 'conditions'=>array('Produto.id'=>$produto['produto_id'])));
                }
                $carrinho['CarrinhoAbandonado']['produtos'] = $itens;
            }
        }

        App::import('Model', 'Cupom');
        $this->Cupom = new Cupom();
        $aCupom = $this->Cupom->find('first',array('conditions'=>array('Cupom.id' => $cupom_id)));
        $ids_to_notify = array();
        foreach ($aCarrinhosAbandonados as $aCarrinhosAbandonado){
            /* send cupom */
            if ($aCarrinhosAbandonado['CarrinhoAbandonado']['valor_total'] >= $aCupom['Cupom']['compra_minima']) {
                $this->email_cupom($aCupom, $aCarrinhosAbandonado['Usuario']['nome'], $aCarrinhosAbandonado['CarrinhoAbandonado']['usuario_id'], $aCarrinhosAbandonado['Usuario']['email'], $aCarrinhosAbandonado['CarrinhoAbandonado']['id']);
                /* update para carrinho abandonado - cupom enviado */
                $this->CarrinhoAbandonado->save(array('id' => $aCarrinhosAbandonado['CarrinhoAbandonado']['id'], 'cupom_enviado' => true, 'cupom_id' => $aCupom['Cupom']['id'], 'cupom' => $aCupom['Cupom']['cupom'], 'data_envio_cupom' => date("Y-m-d H:i:s")), false);
            } else {
                $ids_to_notify[] = $aCarrinhosAbandonado['CarrinhoAbandonado']['id'];
            }
        }

        if (count($ids_to_notify) > 0) {
            $mensagem = 'Os seguintes carrinhos abandonados não contemplam a regra do cupom selecionado: <br />['.implode(", ", $ids_to_notify).']';
//$this->Session->setFlash($mensagem, 'flash/warning');
            $this->Session->setFlash($mensagem, 'flash/error');
            $this->redirect($this->referer());
        }
    }

    public function email_cupom($cupom, $user_name, $user_id, $user_mail, $carrinho_abandonado_id) {
        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();
        App::import("helper", "String");
        $this->String = new StringHelper();

        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
                );
            $this->Email->delivery = 'smtp';
        }
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_compra') . ">";
        $this->Email->to = "{$user_name} <{$user_mail}>";
        $this->Email->subject = "Voce ganhou um cupom de desconto da Melbenett. Aproveite!";

        $dataLimite = $this->Calendario->dataFormatada('d/m/Y', $cupom['Cupom']['data_fim']);
        $valorMinimo = $this->String->bcoToMoeda($cupom['Cupom']['compra_minima']);
        $cupom_to_use = $cupom['Cupom']['cupom'];

        if ((int)$cupom['Cupom']['pct_desconto'] > 0) {
            $desconto = (int)$cupom['Cupom']['pct_desconto'].'% de desconto';
        } else {
            $desconto = 'R$'.$this->String->bcoToMoeda($cupom['Cupom']['vlr_desconto']).' de desconto';
        }

        $link = Router::url("/carrinho/carrinho/refazer_pedido_carrinho_abandonado?carrinho_id=$carrinho_abandonado_id&cupom=$cupom_to_use&user_id=$user_id&utm_source=carrinho-abandonado&utm_medium=carrinho-abandonado&utm_campaign=carrinho-abandonado", true);

        $email = str_replace(
            array('{CUPOM}', '{DATALIMITE}', '{VALORMINIMO}', '{DESCONTO}', '{LINK}', '{NOME}'),
            array($cupom_to_use, $dataLimite, $valorMinimo, $desconto, $link, $user_name),
            Configure::read('LojaTemplate.cupom_carrinho_abandonado'));

        if (Configure::read('Reweb.nome_bcc')) {
            $bccs = array();
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                $bccs[] = "{$assunto} <{$bcc}>";
            }
            $this->Email->bcc = $bccs;
        }

        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }
}
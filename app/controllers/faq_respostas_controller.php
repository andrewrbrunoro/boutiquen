<?php
class FaqRespostasController extends AppController {

	var $name = 'FaqRespostas';
	var $helpers = array('Calendario','String', 'Javascript');
	function admin_index() {
		$this->FaqResposta->recursive = 0;
		$this->set('faqRespostas', $this->paginate());
	}

	function admin_add() {
		if (!empty($this->data)) {
			$this->FaqResposta->create();
			if ($this->FaqResposta->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		$faqPergunta = $this->FaqResposta->FaqPergunta->find('list',array('fields'=>'texto'));
		$this->set(compact('faqPergunta'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parametros inválidos', 'flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->FaqResposta->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->FaqResposta->read(null, $id);
		}
		$faqPergunta = $this->FaqResposta->FaqPergunta->find('list',array('fields'=>'texto'));
		$this->set(compact('faqPergunta'));
	}

	
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->FaqResposta->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
}
?>
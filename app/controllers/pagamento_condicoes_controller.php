<?php

class PagamentoCondicoesController extends AppController {

    var $name = 'PagamentoCondicoes';
    var $components = array('Session');
    var $helpers = array('Calendario', 'String', 'Image', 'Javascript');

    function admin_index() {
        $this->PagamentoCondicao->recursive = 1;
        $this->set('pagamentoCondicoes', $this->paginate());
    }

    function admin_add() {

        if (!empty($this->data)) {
            //somente faz a  validacao
            if ($this->PagamentoCondicao->saveAll($this->data, array('validate' => 'only'))) {
                //salva condicao
                $this->PagamentoCondicao->create();
                $this->PagamentoCondicao->save($this->data);
				Cache::write('geral_parcelamento', false);

                //salva imagem
                App::import("model", "PagamentoCondicaoImagem");
                $this->PagamentoCondicaoImagem = new PagamentoCondicaoImagem();
                $this->data['PagamentoCondicaoImagem']['pagamento_condicao_id'] = $this->PagamentoCondicao->id;
                $this->PagamentoCondicaoImagem->save($this->data);
                //flash
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        $pagamentoTipos = array('' => 'Selecione') + $this->PagamentoCondicao->PagamentoTipo->find('list', array('conditions' => array('PagamentoTipo.status' => true)));
        $this->set(compact('pagamentoTipos'));
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            App::import("model", "PagamentoCondicaoImagem");
            $this->PagamentoCondicaoImagem = new PagamentoCondicaoImagem();
            if ($this->PagamentoCondicao->save($this->data, array('validate' => 'only'))) {
                //salva condicao
                $this->PagamentoCondicao->save($this->data);
				Cache::write('geral_parcelamento', false);
                
                if(isset($this->data['PagamentoCondicaoImagem']['filename']['tmp_name']) && $this->data['PagamentoCondicaoImagem']['filename']['tmp_name'] != ""){
                    $this->PagamentoCondicaoImagem->deleteAll(array('pagamento_condicao_id' => $this->data['PagamentoCondicaoImagem']['pagamento_condicao_id']), false);
                    $this->PagamentoCondicaoImagem->create();
                    $this->PagamentoCondicaoImagem->save($this->data);
                }

                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->PagamentoCondicao->read(null, $id);
            if (!$this->data) {
                $this->redirect(array('action' => 'index'));
            }
        }
        $pagamentoTipos = array('' => 'Selecione') + $this->PagamentoCondicao->PagamentoTipo->find('list');
        $this->set(compact('pagamentoTipos'));
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->PagamentoCondicao->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

}

?>
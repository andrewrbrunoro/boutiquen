<?php

class NoticiasController extends AppController {

    var $name = 'Noticias';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript');

    function detalhe($id = false) {

        //id
        if (!$id) {
            $id = $this->params['id'];
        }

        //find
        $noticia = $this->Noticia->find("first", array("conditions" => array("Noticia.id" => $id)));

        //redirect
        if (!$noticia) {
            $this->redirect("/");
        }

        //set
        $this->set('noticia', $noticia);
        
        $this->set('pagina_atual_tipo', "Institucional");
        $this->set('pagina_atual_url', "noticias");

        //breadcrumb
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => __("Noticias", true));
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $noticia['Noticia']['titulo']);
        $this->set('breadcrumb', $breadcrumb);

        //noticias
        $mais_noticias = $this->Noticia->find("all", array(
															"fields" => array("Noticia.id", "Noticia.titulo", "Noticia.descricao", "Noticia.created"),
															"order" => array("Noticia.created DESC"),
															"limit" => 4,
															"conditions" => array(
																"Noticia.id <>" => $id,
																"Noticia.status" => true
															)
														)
											);
        $this->set('mais_noticias', $mais_noticias);

        if ($noticia['Noticia']['titulo']) {
            $this->set('seo_title', $noticia['Noticia']['titulo']);
        } else {
            $this->set('title_for_layout_produto', 'Notícia - ' . Configure::read('Loja.titulo'));
        }
        if ($noticia['Noticia']['descricao']) {
            $this->set('seo_meta_description', substr($noticia['Noticia']['descricao'], 0, 255));
        } else {
            $this->set('seo_meta_description', Configure::read('Loja.seo_meta_description'));
        }
    }

    function admin_index() {
        //filters
        $filtros = array();

        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Noticia.titulo LIKE '%{%value%}%' OR Noticia.conteudo LIKE '%{%value%}%'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->Noticia->recursive = 0;
        $this->set('noticias', $this->paginate($conditions));
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $rows = $this->Noticia->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Titulo</strong></td>
					<td><strong>Descricao</strong></td>
					<td><strong>Conteudo</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['Noticia']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['Noticia']['id'] . "</td>
					<td>" . strip_tags(iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Noticia']['titulo'])) . "</td>
					<td>" . strip_tags(iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Noticia']['descricao'])) . "</td>
					<td>" . strip_tags(iconv("UTF-8", "ISO-8859-1//IGNORE", $row['Noticia']['conteudo'])) . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Noticia']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['Noticia']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "noticias_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

    function admin_add() {
        if (!empty($this->data)) {

            $this->Noticia->create();

            if ($this->Noticia->save($this->data)) {
                Cache::write('ultimas_noticias', false);
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Noticia']['id'] = $id;
            $this->Noticia->id = $id;

            if (isset($this->data['Noticia']['thumb_remove']) && $this->data['Noticia']['thumb_remove'] == 1) {
                $this->Noticia->remover_thumb($this->data['Noticia']['id']);
            }

            if ($this->Noticia->save($this->data)) {
                Cache::write('ultimas_noticias', false);
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Noticia->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Noticia->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

}

?>
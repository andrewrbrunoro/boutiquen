<?php

class FaqsController extends AppController {

    var $name = 'Faqs';
    var $helpers = array('Calendario', 'String', 'Javascript', 'Image', 'Flash');

    function index($id=null) {
        if($id){
            $this->params['id'] = $id;
        }

        if (isset($this->params['id'])) {
            $conditions = array('Faq.status' => true, 'Faq.id' => $this->params['id']);
        } else {
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
            $conditions = array();
        }
        $perguntas = $this->Faq->find('first', array('conditions' => $conditions));
        if (!$perguntas) {
            $this->redirect(array('controller' => 'home', 'action' => 'index'));
        }
        $this->set('faqs', $perguntas);
        $this->set('perguntas', $this->Faq->FaqPergunta->find('all', array('conditions' => array('Faq.status' => true, 'FaqPergunta.status' => true, 'Faq.id' => $this->params['id']))));		
		$this->set('title_for_layout',$perguntas['Faq']['texto']);
		$this->set('seo_title',$perguntas['Faq']['seo_title']);
		$this->set('seo_meta_keywords',$perguntas['Faq']['seo_meta_keywords']);
		$this->set('seo_meta_description',$perguntas['Faq']['seo_meta_description']);
		$this->set('seo_institucional',$perguntas['Faq']['seo_institucional']);		
		$this->set('breadcrumbs',array(array('nome'=>$perguntas['Faq']['texto'],'link'=>'/'.low(Inflector::slug($perguntas['Faq']['texto'],'-')))));
    }

    function admin_index() {
        $this->Faq->recursive = 0;
        $this->set('faqs', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Faq->create();
            if(empty($this->data['Faq']['url'])){
                $this->data['Faq']['url'] = low(Inflector::slug($this->data['Faq']['texto'],'-'));
            }
            if ($this->Faq->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
            } else {
			    $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if(empty($this->data['Faq']['url'])){
                $this->data['Faq']['url'] = low(Inflector::slug($this->data['Faq']['texto'],'-'));
            }

            if ($this->Faq->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                 $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Faq->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Faq->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

}

?>
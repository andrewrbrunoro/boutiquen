<?php
class LojasController extends AppController {

	var $name = 'Lojas';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas','Estados');
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Loja.nome LIKE '%{%value%}%' OR Loja.rua LIKE '%{%value%}%' OR Loja.cep LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->Loja->recursive = 0;
		$this->set('lojas', $this->paginate($conditions));
	}
	
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->Loja->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Rua</strong></td>
					<td><strong>Bairro</strong></td>
					<td><strong>Cidade</strong></td>
					<td><strong>UF</strong></td>
					<td><strong>Cep</strong></td>
					<td><strong>".iconv("UTF-8", "ISO-8859-1//IGNORE","Observações")."</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['Loja']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['Loja']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['rua'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['bairro'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['cidade'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['uf'])."</td>
					<td>".$row['Loja']['cep']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['observacoes'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Loja']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['Loja']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "lojas_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
			$this->Loja->create();
			if ($this->Loja->save($this->data)) {
				$this->limparCache();
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['Loja']['id'] = $id;
			$this->Loja->id = $id;
			if ($this->Loja->save($this->data)) {
				$this->limparCache();
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Loja->read(null, $id);
		}
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Loja->delete($id)) {
			$this->limparCache();
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	public function get_enderecos_lojas(){
		
		//obtem os enderecos das lojas
		$lojas = $this->Loja->find("all", array("conditions" => array("Loja.status" => true)));
		
		//config
		Configure::write('debug',0);
		$this->autoRender = false;
		$this->layout = 'ajax';
        $this->header('Content-type: application/json');
		
		//return
		return json_encode($lojas);
		
	}
	private function limparCache() {
        Cache::write('lojas', false);
    }
}
?>
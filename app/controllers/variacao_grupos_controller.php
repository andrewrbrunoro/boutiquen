<?php

class VariacaoGruposController extends AppController {

    var $name = 'VariacaoGrupos';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript');

    function admin_index() {
        //filters
        $filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['nome'] = "VariacaoGrupo.nome LIKE '%{%value%}%'";
        }
        if (isset($this->data["Filter"]["status"])) {
            $filtros['status'] = "VariacaoGrupo.status = '{%value%}'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        $this->VariacaoGrupo->recursive = 0;
        $this->set('variacao_grupos', $this->paginate($conditions));
    }

    function admin_add() {
        if (!empty($this->data)) {

            $this->VariacaoGrupo->create();

            if ($this->VariacaoGrupo->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['VariacaoGrupo']['id'] = $id;
            $this->VariacaoGrupo->id = $id;

            if ($this->VariacaoGrupo->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->VariacaoGrupo->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->VariacaoGrupo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();
        
        App::import('Model','VariacaoTipo');
        $this->VariacaoTipo = new VariacaoTipo();

        $rows = $this->VariacaoTipo->find('all', array('conditions' => $conditions));

        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $status = ( $row['VariacaoGrupo']['status'] ) ? "Ativo" : "Inativo";
            $table .= "
				<tr>
					<td>" . $row['VariacaoGrupo']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['VariacaoGrupo']['nome']) . "</td>
					<td>" . $status . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['VariacaoGrupo']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['VariacaoGrupo']['modified']) . "</td>
				</tr>";
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "variacoes_grupos_" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

}

?>
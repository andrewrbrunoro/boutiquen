<?php

    class CategoriasController extends AppController
    {

        public $uses = array(
            'Produto',
            'ProdutoCategoria',
            'Categoria',
            'Fabricante',
            'CategoriaAtributo'
        );


        public function __construct()
        {
            parent::__construct();
            App::import('Helper', 'String');
            $this->String = new StringHelper();
            App::import('Helper', 'Paginator');
            $this->Paginator = new PaginatorHelper();
        }

        public function index()
        {
            if (isset($this->params['seo-url'])) {
                $this->loadModel('Categoria');
                $url = $this->params['seo-url'];
                $category = $this->Categoria->find('first', array('recursive' => -1, 'conditions' => array('Categoria.seo_url' => $url)));

                $this->paginate['limit'] = 20;
                $this->paginate['page'] = isset($this->params['page']) ? $this->params['page'] : 1;
                $this->paginate['recursive'] = -1;

                $this->paginate['conditions'] = array(
                    'ProdutoCategoria.categoria_id' => $category['Categoria']['id']
                );

                $products = $this->paginate('ProdutoCategoria');

                if ($category) {
                    $min = $this->Produto->getMinValue();
                    $max = $this->Produto->getMaxValue();
                    $products = $this->String->getProducts(array_column(array_column($products, 'ProdutoCategoria'), 'produto_id'));

                    $this->set('manufacturers', $this->Fabricante->getManufacturers());
                    $this->set(compact('category', 'products', 'min', 'max', 'url'));
                } else {
                    $this->Session->setFlash('Ops, categoria não foi encontrada.', 'flash/error');
                    $this->redirect('/');
                }
            } else {
                $this->Session->setFlash('Ops, categoria não foi encontrada.', 'flash/error');
                $this->redirect('/');
            }
        }

    }
<?php

class GeranewsController extends AppController {

    var $name = 'Geranews';
    var $components = array('Session', 'Email');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript', 'Estados', 'Marcas');


    function index($id = null) {


        if (!$id) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        $pagina = $this->Geranew->read(null, $id);
        
        
        
        //$content_pagina = $pagina['Geranew']['texto'];
        //mando o tipo da categoria pra view.
        //debug($pagina);
        $this->set('pagina_atual_tipo', $pagina['Geranew']['categoria']);
        $this->set('pagina_atual_url', $pagina['Geranew']['url']);
        $this->set('pagina_nome', $pagina['Geranew']['nome']);

        //start breadcrumb
        $breadcrumb = "";
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $pagina['Geranew']['categoria']);
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $pagina['Geranew']['nome']);
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb

        if ($pagina['Geranew']['dinamico']) {
            switch ($pagina['Geranew']['element']) {
                case "onde_estamos":
                    App::import('Model', 'Loja');
                    $this->Loja = new Loja();
                    $pagina_element_content['lojas'] = $this->Loja->find('all', array('fields' => array('Loja.id', 'Loja.nome', 'Loja.rua', 'Loja.numero', 'Loja.bairro','Loja.uf', 'Loja.telefone', 'Loja.observacoes', 'Loja.coordenadas', 'Loja.thumb_filename'), 'recursive' => -1, 'conditions' => array('Loja.status' => true), 'order' => 'Loja.ordem ASC'));
                    $pagina_element_content['url_form'] = $pagina['Geranew']['url'];
                    $pagina_element_content['tipo_form'] = "1";
                    $this->set('pagina_element_content', $pagina_element_content);
                    break;
					
               case "duvidas-frequentes":
                    App::import('Model', 'DuvidaFrequente');
                    $this->DuvidaFrequente = new DuvidaFrequente();
                    $duvidas_frequentes = $this->DuvidaFrequente->find('all', array('fields' => array('DuvidaFrequente.id', 'DuvidaFrequente.pergunta', 'DuvidaFrequente.resposta'), 'recursive' => -1, 'conditions' => array('DuvidaFrequente.status' => true), 'order' => array('DuvidaFrequente.id ASC')));
                    $this->set('pagina_element_content', $duvidas_frequentes);
                    break;
                
               case "lojas":
                    App::import('Model', 'Loja');
                    $this->Loja = new Loja();
                    $lojas = $this->Loja->find('all', array(
                    										'recursive' => -1, 
                    										'conditions' => array('Loja.status' => true)
														)
												);
                    $this->set('pagina_element_content', $lojas);
                    break;

                case "form":
                    switch ($pagina['Geranew']['url']) {
                        case 'fale-conosco':
                            $pagina_element_content['tipo_form'] = "8";
                            break;
                    }
                    $this->set('pagina_element_content', $pagina_element_content);
                    break;
            }
        }

        //start seo
        $this->set('title_for_layout', $pagina['Geranew']['nome']);
        $this->set('seo_title', $pagina['Geranew']['seo_title']);
        $this->set('seo_meta_keywords', $pagina['Geranew']['seo_meta_keywords']);
        $this->set('seo_meta_description', $pagina['Geranew']['seo_meta_description']);
        $this->set('seo_institucional', $pagina['Geranew']['seo_institucional']);
        //end seo

        $this->set('pagina', $pagina);
    }

    function admin_index() {
        $this->Geranew->recursive = 0;
        $this->set('geranews', $this->paginate());
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Geranew->create();
			$this->Geranew->set($this->data);
            if ($this->Geranew->save($this->data)) {
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
    }

    function admin_edit($id = null) {
        App::import("helper", "Uploadify");
        $this->Uploadify = new UploadifyHelper();
		
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->data['Geranew']['id'] = $id;
            $this->Geranew->id = $id;
            if ($this->Geranew->save($this->data)) {
				
				App::import('Model','GeranewsImagem');
				$this->GeranewsImagem = new GeranewsImagem();
				
				$imagenstodas = $this->data['GeranewsImagem'];
				
				foreach($imagenstodas AS $ind => $item){
					$idam=null;
					$idam['GeranewsImagem']=$item;
					$idam['GeranewsImagem']['id']=$ind;
					
					$this->GeranewsImagem->save($idam);
				}
				
				
                //limpo o cache
                $this->limparCache();
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Geranew->read(null, $id);
        }
		
		
		App::import('Model','GeranewsImagem');
		$this->GeranewsImagem = new GeranewsImagem();
		$grades_imagens = $this->GeranewsImagem->find('all', array('recursive' => -1, 'conditions' => array('GeranewsImagem.geranew_id' => $id), 'order' => array('GeranewsImagem.ordem ASC')));
		
		$images = array();
		
		foreach($grades_imagens as $k => $img){
			$images[$k] = $img['GeranewsImagem'];
		}
		
		$this->set('imagens_news', $this->Uploadify->imagens_news($images, false, true));
    }

    function admin_gera($id = null) {
		
        $this->layout = false;
        //$this->render(false);
		
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
		
		$dadosnews = $this->Geranew->read(null, $id);
		
		App::import('Model','GeranewsImagem');
		$this->GeranewsImagem = new GeranewsImagem();
		$news_imagens = $this->GeranewsImagem->find('all', array('recursive' => -1, 'conditions' => array('GeranewsImagem.geranew_id' => $dadosnews['Geranew']['id']),'order' => array('GeranewsImagem.ordem ASC')));
		
		$this->set('dadosnews', $dadosnews);
		$this->set('news_imagens', $news_imagens);
		
		
		App::import('Model', 'Link');
		$this->Link = new Link();
		App::import('Model', 'Menu');
		$this->Menu = new Menu();
		
		$lista_menu_newsletter = $this->Link->find('all', array(
					'conditions' => array(
						'menu_id' => 5,
					),
					'recursive' => -1,
				));
		
		
        $this->set('lista_menu_newsletter', $lista_menu_newsletter);
		
    }

    function admin_ver($id = null) {
		
        $this->layout = false;
        //$this->render(false);
		
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
		
		$dadosnews = $this->Geranew->read(null, $id);
		
		App::import('Model','GeranewsImagem');
		$this->GeranewsImagem = new GeranewsImagem();
		$news_imagens = $this->GeranewsImagem->find('all', array('recursive' => -1, 'conditions' => array('GeranewsImagem.geranew_id' => $dadosnews['Geranew']['id']),'order' => array('GeranewsImagem.ordem ASC')));
		
		$this->set('dadosnews', $dadosnews);
		$this->set('news_imagens', $news_imagens);
		
		
		App::import('Model', 'Link');
		$this->Link = new Link();
		App::import('Model', 'Menu');
		$this->Menu = new Menu();
		
		$lista_menu_newsletter = $this->Link->find('all', array(
					'conditions' => array(
						'menu_id' => 5,
					),
					'recursive' => -1,
				));
		
		
        $this->set('lista_menu_newsletter', $lista_menu_newsletter);
		
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Geranew->delete($id)) {
            //limpo o cache
            $this->limparCache();
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    private function sendMail($dados) {

        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
        
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        
		//seto o tipo de contato
        App::import('Model', 'SacTipo');
        $this->SacTipo = new SacTipo();
        $sac_tipo = $this->SacTipo->find('first', array('fields' => array('SacTipo.nome', 'SacTipo.email_contato'), 'conditions' => array('SacTipo.id' => $dados['Sac']['sac_tipo_id'])));
        
        if(!$sac_tipo){
            return false;
        }
        
        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . $sac_tipo['SacTipo']['email_contato'] . ">";
        $this->Email->subject = Configure::read('Loja.nome') . " - Contato - " . $sac_tipo['SacTipo']['nome'];

        $email = str_replace(
                array(
            '{SAC_TIPO}',
            '{SAC_NOME}',
            '{SAC_EMAIL}',
            '{SAC_ESTADO}',
            '{SAC_CIDADE}',
            '{SAC_TELEFONE}',
            '{SAC_CNPJ}',
            '{SAC_MENSAGEM}',
            '{SAC_DATA}',
            '{SAC_IP}'
                ), array(
            $sac_tipo_nome,
            $dados['Sac']['nome'],
            $dados['Sac']['email'],
            (isset($dados['Sac']['estado'])) ? $dados['Sac']['estado'] : "",
            (isset($dados['Sac']['cidade'])) ? $dados['Sac']['cidade'] : "",
            (isset($dados['Sac']['telefone'])) ? $dados['Sac']['telefone'] : "",
            (isset($dados['Sac']['cnpj'])) ? $dados['Sac']['cnpj'] : "",
            $dados['Sac']['mensagem'],
            date("d/m/Y H:i:s"),
            $_SERVER['REMOTE_ADDR']
                ), Configure::read('LojaTemplate.sac')
        );

        $bccs = array();
        //COPIAS REWEB
        if (Configure::read('Reweb.nome_bcc')) {
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                if ($bcc != "") {
                    $bccs[] = "{$assunto} <{$bcc}>";
                }
            }
        }

        //bcc
        $this->Email->bcc = $bccs;

        //dispara email
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

    private function limparCache() {
        Cache::write('geranews', false);
        Cache::write('geranews_routes', false);
    }
	
    function admin_img_edit($geranew_id) {

        App::import("helper", "Uploadify");
        $this->Uploadify = new UploadifyHelper();
        $this->loadModel('Geranew');
        $this->loadModel('GeranewsImagem');
        $path = "uploads/tmp/";
        if (!is_dir($path)) {
            mkdir($path);
        }

        $valid_formats = array("jpg", "png", "gif");
        if (!empty($_FILES)) {
            $name = $_FILES['Filedata']['name'];
            if (strlen($name)) {
                if (preg_match('/(.*)\.([a-z]{3,4})$/i', $name, $flag)) {
                    $ext = $flag[2];
                    if (in_array(low($ext), $valid_formats)) {
                        $size = $this->formatbytes($_FILES['Filedata']['tmp_name']);
                        if ($size < 100) {
                            $actual_image_name = md5(time() . $name) . "." . $ext;
                            $tmp = $_FILES['Filedata']['tmp_name'];

                            if (move_uploaded_file($tmp, $path . '/' . $actual_image_name)) {
                                $_FILES['Filedata']['tmp_file'] = $actual_image_name;
                                $_FILES['Filedata']['ordem'] = '';

                                $this->GeranewsImagem->create();
                                $this->data['GeranewsImagem']['geranew_id'] = $geranew_id;
                                $this->data['GeranewsImagem']['id'] = '';
                                $this->data['GeranewsImagem']['dir'] = '';
                                $this->data['GeranewsImagem']['mimetype'] = '';
                                $this->data['GeranewsImagem']['filesize'] = '';
                                $this->data['GeranewsImagem']['ordem'] = 2;
                                $this->data['GeranewsImagem']['status'] = 1;
                                $this->data['GeranewsImagem']['filename']['type'] = 'image/jpeg';
                                $this->data['GeranewsImagem']['filename']['name'] = $_FILES['Filedata']['tmp_file'];
                                $this->data['GeranewsImagem']['filename']['tmp_name'] = WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file'];
                                $this->data['GeranewsImagem']['filename']['error'] = $_FILES['Filedata']['error'];
                                $this->data['GeranewsImagem']['filename']['size'] = $_FILES['Filedata']['size'];
                                $this->data['GeranewsImagem']['filename']['created'] = time();
                                $this->data['GeranewsImagem']['filename']['modified'] = time();
                                $this->GeranewsImagem->save($this->data);
                                unlink(WWW_ROOT . 'uploads' . DS . 'tmp' . DS . $_FILES['Filedata']['tmp_file']);
                                $listaima = $this->GeranewsImagem->find('all', array('recursive' => -1, 'conditions' => array('GeranewsImagem.geranew_id' => $geranew_id)));
                                $imgs = array();
                                foreach ($listaima as $img) {
                                    $imgs[] = $img['GeranewsImagem'];
                                }
                                echo $this->Uploadify->build($imgs, 'GeranewsImagem', false);
                                $this->limparCache();
                                exit;
                            } else {
                                echo "Falha ao mover arquivo";
                                exit;
                            }
                        } else {
                            echo "Arquivo muito grande";
                            exit;
                        }
                    } else {
                        echo "Formato de arquivo inválido.";
                        exit;
                    }
                } else {
                    echo "Formato de arquivo inválido.";
                    exit;
                }
            } else {
                echo "por favor selecione uma imagem.!";
                exit;
            }
        } else {
            echo "Falha ao enviar arquivo tente novamente.!";
            exit;
        }
    }

	function admin_rm_img_tmp($remove){

		$this->layout = false;
		$this->render(false);
		$path = "uploads/tmp/";
		//pega as imagens no tmp
		$olds = $this->Session->read('TMP.GeranewsImagem');
		//se a imagem passada não está no array add a img no tmp
		foreach($olds as $key=>$img){
			if($img['tmp_file'] === $remove){
				$this->Session->delete("TMP.GeranewsImagem.{$key}");
				unlink($path.$img['tmp_file']);
			}
		}
		die(json_encode(true));
	}
	
	function admin_rm_img_old($id){

		if (!$id) {
            die(json_encode(false));
        }
		
		App::import('Model','GeranewsImagem');
		$this->GeranewsImagem = new GeranewsImagem();
		
        if ($this->GeranewsImagem->delete($id)) {
            die(json_encode(true));
        }else{
			die(json_encode(false));
		}
	}

    function formatbytes($file, $type = "MB") {
        switch ($type) {
            case "KB":
                $filesize = filesize($file) * .0009765625; // bytes to KB
                break;
            case "MB":
                $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB
                break;
            case "GB":
                $filesize = ((filesize($file) * .0009765625) * .0009765625) * .0009765625; // bytes to GB
                break;
        }
        if ($filesize <= 0) {
            return $filesize = 'unknown file size';
        } else {
            return round($filesize, 2) . ' ' . $type;
        }
    }
}

?>
<?php

class NewslettersController extends AppController {

    var $name = 'Newsletters';
    var $helpers = array('Calendario', 'String', 'Javascript');
     var $components = array('Filter','Session');

    function admin_index() {
        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        $filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['nome'] = "Newsletter.nome LIKE '%{%value%}%'";
        }

        if (isset($this->data["Filter"]["email"])) {
            $filtros['email'] = "Newsletter.email LIKE '%{%value%}%'";
        }

        //se tiver o campo filtra
        if (isset($this->data["Filter"]["data_ini"])) {
            $filtros['data_ini'] = "DATE_FORMAT(Newsletter.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", $this->data["Filter"]["data_ini"]) . "'";
        } else {
            $filtros['data_ini'] = "DATE_FORMAT(Newsletter.created,'%Y-%m-%d') >= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y', mktime(0, 0, 0, date('m') - 1, date('d'), date('Y')))) . "'";
        }

        //se tiver o campo filtra
        if (isset($this->data["Filter"]["data_fim"])) {
            $filtros['data_fim'] = "DATE_FORMAT(Newsletter.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", $this->data["Filter"]["data_fim"]) . "'";
        } else {
            $filtros['data_fim'] = "DATE_FORMAT(Newsletter.created,'%Y-%m-%d') <= '" . $this->Calendario->DataFormatada("Y-m-d", date('d/m/Y', mktime(0, 0, 0, date('m'), date('d'), date('Y')))) . "'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();


        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_export($conditions);
        }

        $this->Newsletter->recursive = 0;
        $this->set('newsletters', $this->paginate($conditions));
    }


    function admin_export($conditions) {
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        $this->recursive = -1;
        $rows = $this->Newsletter->find('all', array('conditions' => $conditions));
        header("Content-Type: application/octet-stream");
        $file = "newsletter_" . date("y-m-d-H-i-s") . ".csv";
        header("Content-disposition:attachment; filename=$file");
        echo "Nome;Email" . "\n";
        foreach ($rows as $row) {
            echo "{$row['Newsletter']['nome']}" . ";";
            echo "{$row['Newsletter']['email']}" . "\n";
        }
        exit;
    }


    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inv�lidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->Newsletter->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }

        $this->Session->setFlash('O Registro n�o pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

}

?>
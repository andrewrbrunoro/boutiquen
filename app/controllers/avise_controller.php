<?php

class AviseController extends AppController {

    var $name = 'Avise';
    var $uses = array('Avise');
    var $components = array('Email', 'Filter');
    var $helpers = array('Calendario');

    function admin_index() {

        $filtros = array();
        if ($this->data["Filter"]["filtro"]) {
            $filtros['filtro'] = "Avise.nome LIKE '%{%value%}%' OR Avise.email = '{%value%}' ";
        }

        if (isset($this->params['named']['produto_id']) && $this->params['named']['produto_id'] != ""){
            $this->data["Filter"]["produto_id"] = $this->params['named']['produto_id'];
            $filtros['produto_id'] = "Avise.produto_id = '{%value%}' ";
        }
        if (isset($this->data["Filter"]["produto_nome"])) {
            $filtros['produto_nome'] = "Produto.nome LIKE '%{%value%}%'";
        }
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        $conditions[] = array('Avise.status' => true);

        $this->paginate = array('recursive' => 1, 'limit' => 24);
        $avise = $this->paginate('Avise', $conditions);
        $this->set('avises', $avise);
    }

    function admin_enviar() {
        //$this->layout = false;
        //$this->render(false);
        $avise = $this->Avise->find('all', array('conditions' => array('Avise.status' => true)));
        $count_sim = false;

        App::import('Model', 'Produto');
        $this->Produto = new Produto();
        
        App::import('Model', 'Grade');
        $this->Grade = new Grade();
        
        App::import('Model', 'GradeImagem');
        $this->GradeImagem = new GradeImagem();

        foreach ($avise as $valor) {
            $produto = array();
            $grade = $this->Grade->find('first', array('recursive' => -1, 'contain' => 'GradeImagem', 'conditions' => array('AND' => array('Grade.status !=' => 2, 'Grade.quantidade_disponivel >' => 0, 'Grade.id' => $valor['Avise']['grade_id']))));
            if($grade){
                
                $produto_grade = $this->Produto->VariacaoProduto->find("all", array(
                    "conditions" => array("AND" => array("VariacaoProduto.grade_id" => $valor['Avise']['grade_id'], "VariacaoProduto.produto_id" => $valor['Avise']['produto_id'])),
                    "recursive" => -1,
                    "fields" => array('VariacaoProduto.*', 'Variacao.id', 'Variacao.valor', 'VariacaoTipo.id', 'VariacaoTipo.nome'),
                    "joins" => array(
                        array(
                            "table" => 'variacoes',
                            "alias" => 'Variacao',
                            "type" => 'LEFT',
                            "conditions" => array("Variacao.id = VariacaoProduto.variacao_id")
                        ),
                        array(
                            "table" => 'variacao_tipos',
                            "alias" => 'VariacaoTipo',
                            "type" => 'LEFT',
                            "conditions" => array("Variacao.variacao_tipo_id = VariacaoTipo.id")
                        )
                    ),
                    "order" => array('VariacaoTipo.ordem ASC'),
                        )
                );

                $variacoes = '<table border=0>';
                foreach($produto_grade as $pg){
                    $variacoes .= '<tr>';
                    $variacoes .= '<td>'.$pg['VariacaoTipo']['nome'].'</td>';
                    $variacoes .= '<td>'.$pg['Variacao']['valor'].'</td>';
                    $variacoes .= '</tr>';
                }
                $variacoes .= '</table>';
                
                 if(isset($produto_grade[0]['VariacaoProduto']['produto_id'])){
                    $produto = $this->Produto->find('first', array('recursive' => 1, 'contain' => 'ProdutoImagem', 'conditions' => array('AND' => array('Produto.quantidade_disponivel >' => 0, 'Produto.status' => 1, 'Produto.id' => $produto_grade[0]['VariacaoProduto']['produto_id'])))); 
                 }
                 
                if($produto){
                    $produto['Produto']['preco']           = $grade['Grade']['preco'];
                    $produto['Produto']['preco_promocao']  = $grade['Grade']['preco_promocao'];
                    $produto['Produto']['variacoes']       = $variacoes;
                    $produto['Produto']['grade_id']        = $valor['Avise']['grade_id'];

                    $grade_imagem = $this->GradeImagem->find('first', array('recursive' => -1, 'conditions' => array("GradeImagem.grade_id" => $valor['Avise']['grade_id']), "order" => array("GradeImagem.ordem ASC")));
                    $produto['ProdutoImagem']              = $grade_imagem['GradeImagem'];
                }
            }
            
            if ($produto) {
                $count_sim = true;
                //atualiza status somente se o produto e a grade estão disponiveis e com estoque
                $this->Avise->id = $valor['Avise']['id'];
                $this->Avise->saveField('status', 0);
               	$this->disponivel($valor, $produto);
            }
        }
        if ($count_sim) {
            $this->Session->setFlash('Rotina rodada com sucesso. Os clientes serão notificados por email.', 'flash/success');
        } else {
            $this->Session->setFlash('Você não possui produtos para enviar notificações.', 'flash/error');
        }
        $this->redirect(array('action' => 'index'));
    }

    public function disponivel($usuario, $data) {

        App::import("helper", "Html");
        $this->Html = new HtmlHelper();

        App::import("helper", "String");
        $this->String = new StringHelper();

        Configure::write('debug', 0);
        if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
        }

        $this->Email->delivery = 'smtp';
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
		$this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.email_sac') . ">";
        $this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.smtp_user') . ">";
        $this->Email->to = "{$usuario['Avise']['nome']} <{$usuario['Avise']['email']}>";
        $this->Email->subject = Configure::read('Loja.nome') . " Produto disponível";

        if (Configure::read('Reweb.nome_bcc')) {
            $bccs = array();
            $assunto = Configure::read('Reweb.nome_bcc');
            $emails = Configure::read('Reweb.email_bcc');
            foreach (explode(';', $emails) as $bcc) {
                //$bccs[] = "{$assunto} <{$bcc}>";
                if($bcc != ""){
                    $bccs[] = "{$assunto} <{$bcc}>";
                }
            }
            $this->Email->bcc = $bccs;
        }

        $produto_preco = $data['Produto']['preco'];
        if ($data['Produto']['preco_promocao'] > 0) {
            $produto_preco = $data['Produto']['preco_promocao'];
        }

        $produto_imagem = str_replace('uploads/produto_imagem/filename/', '', $data['ProdutoImagem']['filename']);
        $produto_imagem = $this->Html->image($this->Html->Url("/resizer/view/110/290/false/true/" . $produto_imagem, true), array("alt" => $data['Produto']['nome']));
        $produto_url = Router::url("/" . low(Inflector::slug($data['Produto']['nome'], '-')) . '-' . 'prod-' . $data['Produto']['grade_id'], true) . ".html";
        $email = str_replace(array('{USUARIO_NOME}', '{PRODUTO_NOME}', '{PRODUTO_VARIACOES}', '{PRODUTO_IMAGEM}', '{PRODUTO_URL}', '{PRODUTO_PRECO}', '{LOJA_NOME}'), array(
            $usuario['Avise']['nome'],
            $data['Produto']['nome'],
            $data['Produto']['variacoes'],
            $produto_imagem,
            $produto_url,
            'R$ ' . $this->String->bcoToMoeda($produto_preco),
            Configure::read('Loja.smtp_assinatura_email')
                ), Configure::read('LojaTemplate.aviseme')
        );

        //setando os dados para o email
        if ($this->Email->send($email)) {
            return true;
        } else {
            return false;
        }
    }

}
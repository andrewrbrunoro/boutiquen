<?php
/**
 * Tipo de Pagamento

    Código Descrição
    1 Cartão de Crédito
    2 Bloqueto Bancário
    3 Débito Bancário
    4 Débito Bancário – Dinheiro
    5 Débito Bancário – Cheque
    6 Transferência Bancária
    7 Sedex a Cobrar
    8 Cheque
    9 Dinheiro
    10 Financiamento
    11 Fatura
    12 Cupom
    13 Multicheque
    14 Outros


    Bandeira Cartão

    Código Descrição
    1 Diners
    2 MasterCard
    3 Visa
    4 Outros
    5 American Express
    6 HiperCard
    7 Aura

 *
 */
class ClearsaleComponent extends Object {
    private $url = "http://www.clearsale.com.br/start/Entrada/EnviarPedido.aspx";
    private $campos = array(
        "pedido" => array(
            "CodigoIntegracao"      => "eb2bc434-cf8f-4357-bd89-f2350d77d390",
            "PedidoID"      => "",
            "Data"          => "", //(dd-mm-yyyy hh:mm:ss)
            //"IP"            => "", //obrigatorio = false
            "Total"         => "", //obrigatorio = true
            "TipoPagamento" => "1", //obrigatorio = true
            //"TipoCartao"    => "", //obrigatorio = false
            "Parcelas"      => "", //obrigatorio = true
        ),
        "endereco_cobranca" => array(
            "Cobranca_Nome"                     => "", //nome
            "Cobranca_Email"                    => "", //email
            "Cobranca_Documento"                => "", //CPF ou CNPJ
            "Cobranca_Nascimento"                => "", //CPF ou CNPJ
            "Cobranca_Logradouro"               => "", //endereco
            "Cobranca_Logradouro_Numero"        => "", //endereco numero
            "Cobranca_Logradouro_Complemento"   => "", //endereco complemento
            "Cobranca_Bairro"                   => "", //endereco complemento
            "Cobranca_Cidade"                   => "", //endereco complemento
            "Cobranca_Estado"                   => "", //endereco complemento
            "Cobranca_CEP"                      => "", //endereco complemento
            "Cobranca_Pais"                     => "", //endereco complemento
            "Cobranca_DDD_Telefone_1"             => "", //endereco complemento
            "Cobranca_Telefone_1"                 => "", //endereco complemento
            //"Cobranca_DDD_Celular"              => "", //endereco complemento
            //"Cobranca_Celular"                  => "", //endereco complemento
        ),
        "endereco_entrega" => array(
            "Entrega_Nome"                      => "", //nome
            "Entrega_Email"                     => "", //email
            "Entrega_Documento"                 => "", //CPF ou CNPJ
            "Entrega_Nascimento"                 => "", //CPF ou CNPJ
            "Entrega_Logradouro"                => "", //endereco
            "Entrega_Logradouro_Numero"         => "", //endereco numero
            "Entrega_Logradouro_Complemento"    => "", //endereco complemento
            "Entrega_Bairro"                    => "", //endereco complemento
            "Entrega_Cidade"                    => "", //endereco complemento
            "Entrega_Estado"                    => "", //endereco complemento
            "Entrega_CEP"                       => "", //endereco complemento
            "Entrega_Pais"                      => "", //endereco complemento
            "Entrega_DDD_Telefone_1"              => "", //endereco complemento
            "Entrega_Telefone_1"                  => "", //endereco complemento
           // "Entrega_DDD_Celular"               => "", //endereco complemento
           // "Entrega_Celular"                   => "", //endereco complemento
        ),
        "pedido_itens"=>array()
        
    );

    public function run($pedido) {

        App::import("helper", "Calendario");
        $this->Calendario = new CalendarioHelper();

        App::import("helper", "String");
        $this->String = new StringHelper();

        $this->campos['pedido']['PedidoID']         = $pedido['Pedido']['id'];
        $this->campos['pedido']['Data']             = $this->Calendario->DataFormatada('d/m/Y H:i:s',$pedido['Pedido']['created']);
        $this->campos['pedido']['Total']            = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
        $this->campos['pedido']['Total']            = str_replace(',','.',str_replace('.','',$this->campos['pedido']['Total']));
        $this->campos['pedido']['TipoPagamento']    = 1;
        $this->campos['pedido']['Parcelas']         = $pedido['Pedido']['parcelas'];

        $this->campos['endereco_cobranca']['Cobranca_Nome']                     = $pedido['Usuario']['nome'];
        $this->campos['endereco_cobranca']['Cobranca_Nascimento']               = $this->Calendario->DataFormatada('d/m/Y H:i:s',$pedido['Usuario']['data_nascimento']);
        
        $this->campos['endereco_cobranca']['Cobranca_Email']                    = $pedido['Usuario']['email'];
        $this->campos['endereco_cobranca']['Cobranca_Documento']                = $pedido['Usuario']['tipo_pessoa']=="F"?$pedido['Usuario']['cpf']:$pedido['Usuario']['cnpj'];
        $this->campos['endereco_cobranca']['Cobranca_Logradouro']               = $pedido['Pedido']['endereco_rua'];
        $this->campos['endereco_cobranca']['Cobranca_Logradouro_Numero']        = $pedido['Pedido']['endereco_numero'];
        $this->campos['endereco_cobranca']['Cobranca_Logradouro_Complemento']   = $pedido['Pedido']['endereco_complemento'].'casa';
        $this->campos['endereco_cobranca']['Cobranca_Bairro']                   = $pedido['Pedido']['endereco_bairro'];
        $this->campos['endereco_cobranca']['Cobranca_Cidade']                   = $pedido['Pedido']['endereco_cidade'];
        $this->campos['endereco_cobranca']['Cobranca_Estado']                   = $pedido['Pedido']['endereco_estado'];
        $this->campos['endereco_cobranca']['Cobranca_CEP']                      = $pedido['Pedido']['endereco_cep'];
        $this->campos['endereco_cobranca']['Cobranca_Pais']                     = 'Bra';
        $this->campos['endereco_cobranca']['Cobranca_DDD_Telefone_1']             = substr($pedido['Usuario']['telefone'],0,2);
        $this->campos['endereco_cobranca']['Cobranca_Telefone_1']                 = substr($pedido['Usuario']['telefone'],2);

        $this->campos['endereco_entrega']['Entrega_Nome']                       = $pedido['Usuario']['nome'];
        $this->campos['endereco_entrega']['Entrega_Nascimento']               = $this->Calendario->DataFormatada('d/m/Y H:i:s',$pedido['Usuario']['data_nascimento']);
        $this->campos['endereco_entrega']['Entrega_Email']                      = $pedido['Usuario']['email'];
        $this->campos['endereco_entrega']['Entrega_Documento']                  = $pedido['Usuario']['tipo_pessoa']=="F"?$pedido['Usuario']['cpf']:$pedido['Usuario']['cnpj'];
        $this->campos['endereco_entrega']['Entrega_Logradouro']                 = $pedido['Pedido']['endereco_rua'];
        $this->campos['endereco_entrega']['Entrega_Logradouro_Numero']          = $pedido['Pedido']['endereco_numero'];
        $this->campos['endereco_entrega']['Entrega_Logradouro_Complemento']     = $pedido['Pedido']['endereco_complemento'];
        $this->campos['endereco_entrega']['Entrega_Bairro']                     = $pedido['Pedido']['endereco_bairro'];
        $this->campos['endereco_entrega']['Entrega_Cidade']                     = $pedido['Pedido']['endereco_cidade'];
        $this->campos['endereco_entrega']['Entrega_Estado']                     = $pedido['Pedido']['endereco_estado'];
        $this->campos['endereco_entrega']['Entrega_CEP']                        = $pedido['Pedido']['endereco_cep'];
        $this->campos['endereco_entrega']['Entrega_Pais']                       = 'Bra';
        $this->campos['endereco_entrega']['Entrega_DDD_Telefone_1']               = substr($pedido['Usuario']['telefone'],0,2);
        $this->campos['endereco_entrega']['Entrega_Telefone_1']                   = substr($pedido['Usuario']['telefone'],2);

            $count = 1;
         foreach($pedido['PedidoItem'] as $chave=>$item){
            $this->campos['pedido_itens']["Item_ID_{$count}"]          = $item['id'];
            $this->campos['pedido_itens']["Item_Nome_{$count}"]        = $item['nome'];
            $this->campos['pedido_itens']["Item_Qtd_{$count}"]         = $item['quantidade'];
            $this->campos['pedido_itens']["Item_Valor_{$count}"]       = $item['preco_promocao']>0?$item['preco_promocao']:$item['preco'];
            $this->campos['pedido_itens']["Item_Categoria_{$count}"]   = 'teste';
            $count++;
         }

         $args[] = $this->campos['pedido'];
         $args[] = $this->campos['endereco_cobranca'];
         $args[] = $this->campos['endereco_entrega'];
         $args[] = $this->campos['pedido_itens'];
         $retorno = $this->enviarFormulario($args);
         
        return $retorno;
    }
     /**
     * Metodo responsavel somente por submeter os dados
     */
    private function enviarFormulario($args) {
        //Form
        $form = '<iframe border="0" frameborder="0" scrolling="0" noresize="noresize" name="iFrameStart" id="iFrameStart" src="http://www.clearsale.com.br/start/Entrada/EnviarPedido.aspx?codigointegracao='.$this->campos['pedido']['CodigoIntegracao'].'&PedidoID='.$this->campos['pedido']['PedidoID'].'">
                    <p>Seu Browser não suporta iframes</p>
                </iframe>
            
                <form method="POST" name="formPagamento" id="formPagamento" target="iFrameStart" action="http://www.clearsale.com.br/start/Entrada/EnviarPedido.aspx">';
                        foreach ($args as $arg) {
                            foreach ($arg as $c => $v) {
                                $form .= '<input type="hidden" name="' . $c . '" value="' . $v . '" />';
                            }
                        }
                        $form .= '
                </form>
                <script language="JavaScript">
                    document.getElementById(\'formPagamento\').submit();
                </script>
            ';
        return $form;
    }
}
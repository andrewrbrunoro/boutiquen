<?php

class CorreiosComponent extends Object {

    private $valor_max = 10000;  // máximo valor declarado, em reais
    private $altura_max = 105;   // todas as medidas em cm
    private $largura_max = 105;
    private $comprimento_max = 105;
    private $altura_min = 2;
    private $largura_min = 11;
    private $comprimento_min = 16;
    private $soma_dim_max = 200;  // medida máxima das somas da altura, largura, comprimento
    private $peso_max = 30;   // em kg
    private $peso_min = 0.300;
    private $nCdServico = array();
    private $url = '';
    private $quote_data = array();
    private $cep_destino;
    private $cep_origem;
    private $contrato_codigo = '';
    private $contrato_senha = '';
    private $mensagem_erro = array();
    public $correios = array(
        'SEDEX' => '40010',
        '40010' => 'SEDEX',
        'SEDEX a Cobrar' => '40045',
        '40045' => 'SEDEX a Cobrar',
        'SEDEX a Cobrar - contrato' => '40126',
        '40126' => 'SEDEX a Cobrar - contrato',
        'SEDEX 10' => '40215',
        '40215' => 'SEDEX 10',
        'SEDEX Hoje' => '40290',
        '40290' => 'SEDEX Hoje',
        'SEDEX - contrato 1' => '40096',
        '40096' => 'SEDEX - contrato 1',
        'SEDEX - contrato 2' => '40436',
        '40436' => 'SEDEX - contrato 2',
        'SEDEX - contrato 3' => '40444',
        '40444' => 'SEDEX - contrato 3',
        'SEDEX - contrato 4' => '40568',
        '40568' => 'SEDEX - contrato 4',
        'SEDEX - contrato 5' => '40606',
        '40606' => 'SEDEX - contrato 5',
        'PAC' => '41106',
        '41106' => 'PAC',
        'PAC - contrato' => '41068',
        '41068' => 'PAC - contrato',
        'e-SEDEX' => '81019',
        '81019' => 'e-SEDEX',
        'e-SEDEX Prioritario' => '81027',
        '81027' => 'e-SEDEX Prioritario',
        'e-SEDEX Express' => '81035',
        '81035' => 'e-SEDEX Express',
        'e-SEDEX grupo 1' => '81868',
        '81868' => 'e-SEDEX grupo 1',
        'e-SEDEX grupo 2' => '81833',
        '81833' => 'e-SEDEX grupo 2',
        'e-SEDEX grupo 3' => '81850',
        '81850' => 'e-SEDEX grupo 3'
    );

    // função responsável pelo retorno à loja dos valores finais dos valores dos fretes
    public function Calcular($cep, $carrinho) {

        $method_data = array();

        if (true) {

            $produtos = $carrinho;

            // obtém só a parte numérica do CEP
            $this->cep_origem = preg_replace("/[^0-9]/", '', Configure::read('Correios.cep_origem'));
            $this->cep_destino = preg_replace("/[^0-9]/", '', $cep);

            // serviços sem contrato
            if (Configure::read('Correios.correios_' . $this->correios['PAC'])) {
                $this->nCdServico[] = $this->correios['PAC'];
            }
            if (Configure::read('Correios.correios_' . $this->correios['SEDEX'])) {
                $this->nCdServico[] = $this->correios['SEDEX'];
            }
            if (Configure::read('Correios.correios_' . $this->correios['SEDEX a Cobrar'])) {
                $this->nCdServico[] = $this->correios['SEDEX a Cobrar'];
            }
            if (Configure::read('Correios.correios_' . $this->correios['SEDEX 10'])) {
                $this->nCdServico[] = $this->correios['SEDEX 10'];
            }
            if (Configure::read('Correios.correios_' . $this->correios['SEDEX Hoje'])) {
                $this->nCdServico[] = $this->correios['SEDEX Hoje'];
            }
            // serviços com contrato			
            if (trim(Configure::read('Correios.contrato_codigo')) != "" && trim(Configure::read('Correios.contrato_senha')) != "") {
                $this->contrato_codigo = Configure::read('Correios.contrato_codigo');
                $this->contrato_senha = Configure::read('Correios.contrato_senha');

                if (Configure::read('Correios.correios_' . $this->correios['SEDEX a Cobrar - contrato'])) {
                    $this->nCdServico[] = $this->correios['SEDEX a Cobrar - contrato'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['SEDEX - contrato 1'])) {
                    $this->nCdServico[] = $this->correios['SEDEX - contrato 1'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['SEDEX - contrato 2'])) {
                    $this->nCdServico[] = $this->correios['SEDEX - contrato 2'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['SEDEX - contrato 3'])) {
                    $this->nCdServico[] = $this->correios['SEDEX - contrato 3'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['SEDEX - contrato 4'])) {
                    $this->nCdServico[] = $this->correios['SEDEX - contrato 4'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['SEDEX - contrato 5'])) {
                    $this->nCdServico[] = $this->correios['SEDEX - contrato 5'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['PAC - contrato'])) {
                    $this->nCdServico[] = $this->correios['PAC - contrato'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['e-SEDEX'])) {
                    $this->nCdServico[] = $this->correios['e-SEDEX'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['e-SEDEX Prioritario'])) {
                    $this->nCdServico[] = $this->correios['e-SEDEX Prioritario'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['e-SEDEX Express'])) {
                    $this->nCdServico[] = $this->correios['e-SEDEX Express'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['e-SEDEX grupo 1'])) {
                    $this->nCdServico[] = $this->correios['e-SEDEX grupo 1'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['e-SEDEX grupo 2'])) {
                    $this->nCdServico[] = $this->correios['e-SEDEX grupo 2'];
                }
                if (Configure::read('Correios.correios_' . $this->correios['e-SEDEX grupo 3'])) {
                    $this->nCdServico[] = $this->correios['e-SEDEX grupo 3'];
                }
            }

            // 'empacotando' o carrinho em caixas

            $caixas = $this->organizarEmCaixas($produtos);

            // file_put_contents('filename.txt', print_r($caixas, true));
            // obtém o frete de cada caixa
            foreach ($caixas as $caixa) {
                $this->setQuoteData($caixa);
            }
            // ajustes finais
            if ($this->quote_data) {

                $valor_adicional = (is_numeric(Configure::read('Correios.valor_adicional'))) ? Configure::read('Correios.valor_adicional') : 0;

                foreach ($this->quote_data as $codigo => $data) {

                    // soma o valor adicional ao valor final do frete - não aplicado ao Sedex a Cobrar
                    if ($codigo != $this->correios['SEDEX a Cobrar'] || $codigo != $this->correios['SEDEX a Cobrar - contrato']) {
                        $new_cost = $this->quote_data[$codigo]['preco'] + ($this->quote_data[$codigo]['preco'] * ($valor_adicional / 100));
                        // novo custo
                        $this->quote_data[$codigo]['preco'] = $new_cost;
                    } else {
                        // zera o valor do frete do Sedex a Cobrar para evitar de ser adiconado ao valor do carrinho
                        $this->quote_data[$codigo]['preco'] = 0;
                    }
                }
                $method_data = array(
                    'code' => 'correios',
                    'quote' => $this->quote_data,
                    'error' => false
                );
            } else if (!empty($this->mensagem_erro)) {
                $method_data = array(
                    'code' => 'correios',
                    'quote' => $this->quote_data,
                    'error' => implode('<br />', $this->mensagem_erro)
                );
            }
        }
    	return $method_data;
    }

    // obtém os dados dos fretes para os produtos da caixa
    private function setQuoteData($caixa) {

        // obtém o valor total da caixa
        $total_caixa = $this->getTotalCaixa($caixa['produtos']);
        $total_caixa = ($total_caixa > $this->valor_max) ? $this->valor_max : $total_caixa;

        list($peso, $altura, $largura, $profundidade) = $this->ajustarDimensoes($caixa);

        // fazendo a chamada ao site dos Correios e obtendo os dados
        $servicos = $this->getServicos($peso, $total_caixa, $profundidade, $largura, $altura);
        foreach ($servicos as $servico) {

            // o site dos Correios retornou os dados sem erros.
            $valor_frete_sem_adicionais = $servico['Valor'] - $servico['ValorAvisoRecebimento'] - $servico['ValorMaoPropria'] - $servico['ValorValorDeclarado'];
            if ($servico['Erro'] == 0 && $valor_frete_sem_adicionais > 0) {

                // subtrai do valor do frete as opções desabilitadas nas configurações do módulo - 'declarar valor' é obrigatório para sedex a cobrar
                $cost = (Configure::read('Correios.declarar_valor') == false && ($servico['Codigo'] != $this->correios['SEDEX a Cobrar'] || $servico['Codigo'] != $this->correios['SEDEX a Cobrar - contrato'])) ? ($servico['Valor'] - $servico['ValorValorDeclarado']) : $servico['Valor'];
                $cost = (Configure::read('Correios.aviso_recebimento') == false) ? ($cost - $servico['ValorAvisoRecebimento']) : $cost;
                $cost = (Configure::read('Correios.mao_propria') == false) ? ($cost - $servico['ValorMaoPropria']) : $cost;

                // o valor do frete para a caixa atual é somado ao valor total já calculado para outras caixas 
                if (isset($this->quote_data[$servico['Codigo']])) {
                    $cost += $this->quote_data[$servico['Codigo']]['cost'];
                }
                // texto a ser exibido para Sedex a Cobrar
                if ($servico['Codigo'] == $this->correios['SEDEX a Cobrar'] || $servico['Codigo'] == $this->correios['SEDEX a Cobrar - contrato']) {
                    $title = 'prazo entrega ' . $servico['PrazoEntrega'] . ' - ' . $cost;
                    $text = $cost;
                } else {
                    $title = 'prazo entrega ' . $servico['PrazoEntrega'];
                    $text = $cost;
                }
                $this->quote_data[$servico['Codigo']] = array(
                    'codigo' => $servico['Codigo'],
                    'prazo' => $servico['PrazoEntrega'],
                    'preco' => $cost,
                    'nome' => $this->correios[$servico['Codigo']],
                );
            }
            // grava no log de erros do OpenCart a mensagem de erro retornado pelos Correios
            else {
                $this->mensagem_erro[] = $this->correios[$servico['Codigo']] . ': ' . $servico['MsgErro'];
                $this->log($this->correios[$servico['Codigo']] . ': ' . $servico['MsgErro'], 'log_correios');
            }
        }
    }

    // prepara a url de chamada ao site dos Correios
    //ok
    private function setUrl($peso, $valor, $comp, $larg, $alt) {

        $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
        #$url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx/CalcPrecoPrazo?"; // url alternativa disponibilizada pelos Correios.
        $url .= "nCdEmpresa=" . $this->contrato_codigo;
        $url .= "&sDsSenha=" . $this->contrato_senha;
        $url .= "&sCepOrigem=%s";
        $url .= "&sCepDestino=%s";
        $url .= "&nVlPeso=%s";
        $url .= "&nCdFormato=1";
        $url .= "&nVlComprimento=%s";
        $url .= "&nVlLargura=%s";
        $url .= "&nVlAltura=%s";
        $url .= "&sCdMaoPropria=s";
        $url .= "&nVlValorDeclarado=%s";
        $url .= "&sCdAvisoRecebimento=s";
        $url .= "&nCdServico=" . implode(',', $this->nCdServico);
        $url .= "&nVlDiametro=0";
        $url .= "&StrRetorno=xml";

        $this->url = sprintf($url, $this->cep_origem, $this->cep_destino, $peso, $comp, $larg, $alt, $valor);
    }

    // conecta ao sites dos Correios e obtém o arquivo XML com os dados do frete
    //ok
    private function getXML($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($ch);

        if (!$result) {
            $this->log(curl_error($ch), 'log_correios');
            $this->log('erro conexao', 'log_correios');
            $result = curl_exec($ch);

            if ($result) {
                $this->log('sucesso conexao', 'log_correios');
            } else {
                $this->log(curl_error($ch), 'log_correios');
                $this->log('erro reconexao', 'log_correios');
            }
        }

        curl_close($ch);

        $result = str_replace('&amp;lt;sup&amp;gt;&amp;amp;reg;&amp;lt;/sup&amp;gt;', '', $result);
        $result = str_replace('&amp;lt;sup&amp;gt;&amp;amp;trade;&amp;lt;/sup&amp;gt;', '', $result);
        $result = str_replace('**', '', $result);
        $result = str_replace("\r\n", '', $result);
        $result = str_replace('\"', '"', $result);

        return $result;
    }

    // faz a chamada e lê os dados no arquivo XML retornado pelos Correios 
    //ok
    public function getServicos($peso, $valor, $comp, $larg, $alt) {

        $dados = array();

        // troca o separador decimal de ponto para vírgula nos dados a serem enviados para os Correios
        $peso = str_replace('.', ',', $peso);

        $valor = str_replace('.', ',', $valor);
        $valor = number_format((float) $valor, 2, ',', '.');

        $comp = str_replace('.', ',', $comp);
        $larg = str_replace('.', ',', $larg);
        $alt = str_replace('.', ',', $alt);

        // ajusta a url de chamada
        $this->setUrl($peso, $valor, $comp, $larg, $alt);

        // faz a chamada e retorna o xml com os dados
        $xml = $this->getXML($this->url);

        // lendo o xml
        if ($xml) {
            $dom = new DOMDocument('1.0', 'ISO-8859-1');
            $dom->loadXml($xml);

            $servicos = $dom->getElementsByTagName('cServico');

            if ($servicos) {

                // obtendo o prazo adicional a ser somado com o dos Correios
                $prazo_adicional = (is_numeric(Configure::read('Correios.prazo_adicional'))) ? Configure::read('Correios.prazo_adicional') : 0;

                foreach ($servicos as $servico) {
                    $codigo = $servico->getElementsByTagName('Codigo')->item(0)->nodeValue;
                    // Sedex 10 e Sedex Hoje não tem prazo adicional
                    $prazo = ($codigo == $this->correios['SEDEX 10'] || $codigo == $this->correios['SEDEX Hoje']) ? 0 : $prazo_adicional;

                    $dados[$codigo] = array(
                        "Codigo" => $codigo,
                        "Valor" => str_replace(',', '.', $servico->getElementsByTagName('Valor')->item(0)->nodeValue),
                        "PrazoEntrega" => ($servico->getElementsByTagName('PrazoEntrega')->item(0)->nodeValue + $prazo),
                        "Erro" => $servico->getElementsByTagName('Erro')->item(0)->nodeValue,
                        "MsgErro" => $servico->getElementsByTagName('MsgErro')->item(0)->nodeValue,
                        "ValorMaoPropria" => (isset($servico->getElementsByTagName('ValorMaoPropria')->item(0)->nodeValue)) ? str_replace(',', '.', $servico->getElementsByTagName('ValorMaoPropria')->item(0)->nodeValue) : 0,
                        "ValorAvisoRecebimento" => (isset($servico->getElementsByTagName('ValorAvisoRecebimento')->item(0)->nodeValue)) ? str_replace(',', '.', $servico->getElementsByTagName('ValorAvisoRecebimento')->item(0)->nodeValue) : 0,
                        "ValorValorDeclarado" => (isset($servico->getElementsByTagName('ValorValorDeclarado')->item(0)->nodeValue)) ? str_replace(',', '.', $servico->getElementsByTagName('ValorValorDeclarado')->item(0)->nodeValue) : 0
                    );
                }
            }
        }
        return $dados;
    }

    // retorna a dimensão em centímetros
    //ok
    private function getDimensaoEmCm($unidade = 'cm', $dimensao) {

        if ($unidade == 'mm') {
            return $dimensao / 10;
        }
        return $dimensao;
    }

    // retorna o peso em quilogramas
    //ok
    private function getPesoEmKg($unidade = 'kg', $peso) {

        if (is_numeric($peso)) {
            if ($unidade == 'g') {
                return ($peso / 1000);
            }
        }
        return $peso;
    }

    // pré-validação das dimensões e peso do produto 
    //ok
    private function validar($produto) {

        if (!is_numeric($produto['altura']) || !is_numeric($produto['largura']) || !is_numeric($produto['profundidade']) || !is_numeric($produto['peso'])) {
            $this->log(sprintf('erro dimensoes', $produto['nome']));
            return false;
        }

        $altura = $produto['altura'];
        $largura = $produto['largura'];
        $comprimento = $produto['profundidade'];
        $peso = $produto['peso'];

        if ($altura > $this->altura_max || $largura > $this->largura_max || $comprimento > $this->comprimento_max) {
            $this->log(sprintf('error_dim_limite', $this->comprimento_max, $this->largura_max, $this->altura_max, $produto['nome'], $comprimento, $largura, $altura));
            return false;
        }

        $soma_dim = $altura + $largura + $comprimento;
        if ($soma_dim > $this->soma_dim_max) {
            $this->log(sprintf('error_dim_soma', $this->soma_dim_max, $produto['nome'], $soma_dim));
            return false;
        }

        if ($peso > $this->peso_max) {
            $this->log(sprintf('error_peso', $this->peso_max, $produto['nome'], $peso));
            return false;
        }

        return true;
    }

    // 'empacota' os produtos do carrinho em caixas com dimensões e peso dentro dos limites definidos pelos Correios
    // algoritmo desenvolvido por: Thalles Cardoso <thallescard@gmail.com>
    //ok
    private function organizarEmCaixas($produtos) {

        $caixas = array();

        foreach ($produtos as $chave => $valor) {
            $produtos[$chave]['peso'] = $valor['peso'] * $valor['quantidade'];
        }
        foreach ($produtos as $prod) {

            $prod_copy = $prod;

            // muda-se a quantidade do produto para incrementá-la em cada caixa
            $prod_copy['quantidade'] = 1;

            // todas as dimensões da caixa serão em cm e kg
            $prod_copy['largura'] = $this->getDimensaoEmCm('cm', $prod_copy['largura']);
            $prod_copy['altura'] = $this->getDimensaoEmCm('cm', $prod_copy['altura']);
            $prod_copy['profundidade'] = $this->getDimensaoEmCm('cm', $prod_copy['profundidade']);

            // O peso do produto não é unitário como a dimensão. É multiplicado pela quantidade. Se quisermos o peso unitário, teremos que dividir pela quantidade.
            $prod_copy['peso'] = $this->getPesoEmKg('kg', $prod_copy['peso']) / $prod['quantidade'];


            $cx_num = 0;

            for ($i = 1; $i <= $prod['quantidade']; $i++) {
                // valida as dimensões do produto com as dos Correios
                if ($this->validar($prod_copy)) {

                    // cria-se a caixa caso ela não exista.
                    isset($caixas[$cx_num]['peso']) ? true : $caixas[$cx_num]['peso'] = 0;
                    isset($caixas[$cx_num]['altura']) ? true : $caixas[$cx_num]['altura'] = 0;
                    isset($caixas[$cx_num]['largura']) ? true : $caixas[$cx_num]['largura'] = 0;
                    isset($caixas[$cx_num]['profundidade']) ? true : $caixas[$cx_num]['profundidade'] = 0;

                    $new_largura = $caixas[$cx_num]['largura'] + $prod_copy['largura'];
                    $new_altura = $caixas[$cx_num]['altura'] + $prod_copy['altura'];
                    $new_profundidade = $caixas[$cx_num]['profundidade'] + $prod_copy['profundidade'];
                    $new_peso = $caixas[$cx_num]['peso'] + $prod_copy['peso'];

                    $cabe_do_lado = ($new_largura <= $this->largura_max) && $this->somaDimDentroLimite($caixas, $prod_copy, $cx_num, 'lado');

                    $cabe_no_fundo = ($new_profundidade <= $this->comprimento_max) && $this->somaDimDentroLimite($caixas, $prod_copy, $cx_num, 'fundo');

                    $cabe_em_cima = ($new_altura <= $this->altura_max) && $this->somaDimDentroLimite($caixas, $prod_copy, $cx_num, 'cima');

                    $peso_dentro_limite = ($new_peso <= $this->peso_max);

                    // o produto cabe na caixa
                    if (($cabe_do_lado || $cabe_no_fundo || $cabe_em_cima) && $peso_dentro_limite) {

                        // já existe o mesmo produto na caixa, assim incrementa-se a sua quantidade
                        if (isset($caixas[$cx_num]['produtos'][$prod_copy['grade_id']])) {
                            $caixas[$cx_num]['produtos'][$prod_copy['grade_id']]['quantidade'] ++;
                        }
                        // adiciona o novo produto
                        else {
                            $caixas[$cx_num]['produtos'][$prod_copy['grade_id']] = $prod_copy;
                        }

                        // aumenta-se o peso da caixa
                        $caixas[$cx_num]['peso'] += $prod_copy['peso'];

                        // ajusta-se as dimensões da nova caixa 
                        if ($cabe_do_lado) {
                            $caixas[$cx_num]['largura'] += $prod_copy['largura'];

                            // a caixa vai ficar com a altura do maior produto que estiver nela
                            $caixas[$cx_num]['altura'] = max($caixas[$cx_num]['altura'], $prod_copy['altura']);

                            // a caixa vai ficar com o comprimento do maior produto que estiver nela
                            $caixas[$cx_num]['profundidade'] = max($caixas[$cx_num]['profundidade'], $prod_copy['profundidade']);
                        } else if ($cabe_no_fundo) {
                            $caixas[$cx_num]['profundidade'] += $prod_copy['profundidade'];

                            // a caixa vai ficar com a altura do maior produto que estiver nela
                            $caixas[$cx_num]['altura'] = max($caixas[$cx_num]['altura'], $prod_copy['altura']);

                            // a caixa vai ficar com a largura do maior produto que estiver nela
                            $caixas[$cx_num]['largura'] = max($caixas[$cx_num]['largura'], $prod_copy['largura']);
                        } else if ($cabe_em_cima) {
                            $caixas[$cx_num]['altura'] += $prod_copy['altura'];

                            //a caixa vai ficar com a altura do maior produto que estiver nela
                            $caixas[$cx_num]['largura'] = max($caixas[$cx_num]['largura'], $prod_copy['largura']);

                            //a caixa vai ficar com a largura do maior produto que estiver nela
                            $caixas[$cx_num]['profundidade'] = max($caixas[$cx_num]['profundidade'], $prod_copy['profundidade']);
                        }
                    }
                    // tenta adicionar o produto que não coube em uma nova caixa
                    else {
                        $cx_num++;
                        $i--;
                    }
                }
                // produto não tem as dimensões/peso válidos então abandona sem calcular o frete. 
                else {
                    $caixas = array();
                    break 2;  // sai dos dois foreach
                }
            }
        }
        return $caixas;
    }

    // retorna o valor total dos prodtos na caixa
    private function getTotalCaixa($products) {
        $total = 0;

        foreach ($products as $product) {
            $preco = $product['preco_promocao'] > 0 ? $product['preco_promocao'] : $product['preco'];
            $total += $preco;
        }
        return $total;
    }

    //ok
    private function ajustarDimensoes($caixa) {

        // a altura não pode ser maior que o comprimento, assim inverte-se as dimensões
        $altura = $caixa['altura'];
        $largura = $caixa['largura'];
        $profundidade = $caixa['profundidade'];
        $peso = $caixa['peso'];

        // se dimensões menores que a permitida, ajusta para o padrão
        if ($altura < $this->altura_min) {
            $altura = $this->altura_min;
        }
        if ($largura < $this->largura_min) {
            $largura = $this->largura_min;
        }
        if ($profundidade < $this->comprimento_min) {
            $profundidade = $this->comprimento_min;
        }
        if ($peso < $this->peso_min) {
            $peso = $this->peso_min;
        }
        if ($altura > $profundidade) {
            $temp = $altura;
            $altura = $profundidade;
            $profundidade = $temp;
        }

        return array($peso, $altura, $largura, $profundidade);
    }

    //ok
    private function somaDimDentroLimite($caixas, $prod_copy, $cx_num, $orientacao) {

        if ($orientacao == 'lado') {
            $largura = $caixas[$cx_num]['largura'] + $prod_copy['largura'];
            $altura = max($caixas[$cx_num]['altura'], $prod_copy['altura']);
            $profundidade = max($caixas[$cx_num]['profundidade'], $prod_copy['profundidade']);
        } elseif ($orientacao == 'fundo') {
            $profundidade = $caixas[$cx_num]['profundidade'] + $prod_copy['profundidade'];
            $altura = max($caixas[$cx_num]['altura'], $prod_copy['altura']);
            $largura = max($caixas[$cx_num]['largura'], $prod_copy['largura']);
        } elseif ($orientacao == 'cima') {
            $altura = $caixas[$cx_num]['altura'] + $prod_copy['altura'];
            $largura = max($caixas[$cx_num]['largura'], $prod_copy['largura']);
            $profundidade = max($caixas[$cx_num]['profundidade'], $prod_copy['profundidade']);
        } else {
            $largura = $caixas[$cx_num]['largura'];
            $altura = $caixas[$cx_num]['altura'];
            $profundidade = $caixas[$cx_num]['profundidade'];
        }
        $dentroLimite = ($largura + $altura + $profundidade) <= $this->soma_dim_max;

        return $dentroLimite;
    }

}

?>
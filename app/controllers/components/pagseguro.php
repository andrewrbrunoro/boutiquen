<?php

    class PagseguroComponent extends Object
    {

        public $serverStatus = 'sandbox';

        private $paymentSend = array();

        private $required = array(
            'CREDIT_CARD'  => array(
                'card_token'       => array(
                    'message' => 'Ops, houve uma falha ao tentar finalizar o pagamento. Tente novamente.'
                ),
                'hash'             => array(
                    'message' => 'Ops, houve uma falha ao tentar finalizar o pagamento. Tente novamente.'
                ),
                'card_name'        => array(
                    'message' => 'O nome impresso no cartão é obrigatório.'
                ),
                'card_number'      => array(
                    'message' => 'O número do cartão é obrigatório.'
                ),
                'card_month'       => array(
                    'message' => 'O mês do cartão é obrigatório.'
                ),
                'card_year'        => array(
                    'message' => 'O ano do cartão é obrigatório.'
                ),
                'card_cvv'         => array(
                    'message' => 'O número de segurança é obrigatório.'
                ),
                'card_installment' => array(
                    'message' => 'Selecione a quantidade de parcelas.'
                ),
            ),
            'BOLETO'       => array(
                'hash' => array(
                    'message' => 'Ops, houve uma falha ao tentar finalizar o pagamento. Tente novamente.'
                )
            ),
            'ONLINE_DEBIT' => array(
                'hash'           => array(
                    'message' => 'Ops, houve uma falha ao tentar finalizar o pagamento. Tente novamente.'
                ),
                'pagseguro_bank' => array(
                    'message' => 'Ops, houve uma falha ao tentar finalizar o pagamento. Tente novamente.'
                )
            )
        );

        public $credentials = array(
            'production' => array(
                'email' => 'alex@boutiqueno.com.br',
                'token' => '209386F75F1C4EE3B41AB831949B08FD'
            ),
            'sandbox'    => array(
                'email' => 'alex@boutiqueno.com.br',
                'token' => '8295A29B6D104904B0D50CEC751A30B3'
            )
        );

        public $urls = array(
            'production' => array(
                'https://pagseguro.uol.com.br',
                'https://ws.pagseguro.uol.com.br',
                'https://stc.pagseguro.uol.com.br'
            ),
            'sandbox'    => array(
                'https://sandbox.pagseguro.uol.com.br',
                'https://ws.sandbox.pagseguro.uol.com.br',
                'https://stc.sandbox.pagseguro.uol.com.br'
            )
        );

        public function __construct()
        {
            $this->loadDependencyByStatus();
        }

        private function loadDependencyByStatus()
        {
            $this->urls = $this->urls[$this->serverStatus];
            $this->credentials = $this->credentials[$this->serverStatus];
        }

        public function getUrlJS()
        {
            return $this->urls[2] . '/pagseguro/api/v2/checkout/pagseguro.directpayment.js';
        }

        public function getSessionToken()
        {
            $code = '';
            $ch = curl_init($this->urls[1] . '/v2/sessions');
            curl_setopt($ch, CURLOPT_POST, count($this->credentials));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->credentials));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec($ch);
            curl_close($ch);
            $xml = simplexml_load_string($result);
            $json = json_encode($xml);
            $array = json_decode($json, true);
            if ($array) {
                $code = $array['id'];
            }
            return $code;
        }

        /**
         * @param        $reference   = ID DO PEDIDO OU REFERENCIA DO PEDIDO
         * @param string $extraAmount = Caso exista desconto, passar o valor total - o desonto
         * @param string $campainha
         * @param string $email
         * @param string $mode
         */
        public function setPagseguro($reference, $extraAmount = '', $campainha = '', $email = '', $mode = 'default')
        {
            $this->paymentSend += array(
                    'paymentMode'     => $mode,
                    'receiverEmail'   => $this->serverStatus == 'sandbox' ? 'alex@boutiqueno.com.br' : $email,
                    'currency'        => 'BRL',
                    'extraAmount'     => '-' . number_format($extraAmount, 2, '.', ''),
                    'notificationURL' => $campainha,
                    'reference'       => $reference
                ) + $this->credentials;
        }

        /**
         * @param array $data
         *
         * Dados do usuário
         * ddd = 99
         * senderHash = Token gerada na forma de pagamento
         */
        public function setSender(array $data)
        {
            $this->paymentSend += array(
                'senderName'     => $data['full_name'],
                'senderCPF'      => $data['cpf'],
                'senderAreaCode' => $data['ddd'],
                'senderPhone'    => $data['phone'],
                'senderEmail'    => $this->serverStatus == 'sandbox' ? 'c65602210211839113425@sandbox.pagseguro.com.br' : $data['email'],
                'senderHash'     => $data['pagseguro_hash']
            );
        }

        /**
         * @param array  $data
         * @param string $shippingCost
         * @param int    $shippingType
         */
        public function setShipping(array $data, $shippingCost = '0.00', $shippingType = 3)
        {
            $this->paymentSend += array(
                'shippingAddressStreet'     => $data['street'],
                'shippingAddressNumber'     => $data['number'],
                'shippingAddressComplement' => $data['complement'],
                'shippingAddressDistrict'   => $data['district'],
                'shippingAddressPostalCode' => $data['zip'],
                'shippingAddressCity'       => $data['city'],
                'shippingAddressState'      => $data['state'],
                'shippingAddressCountry'    => $data['country'],
                'shippingType'              => $shippingType,
                'shippingCost'              => number_format($shippingCost, 2, '.', '')
            );
        }

        public function setBillingAddress(array $data)
        {
            $this->paymentSend += array(
                'billingAddressStreet'     => $data['street'],
                'billingAddressNumber'     => $data['number'],
                'billingAddressComplement' => $data['complement'],
                'billingAddressDistrict'   => $data['district'],
                'billingAddressPostalCode' => $data['zip'],
                'billingAddressCity'       => $data['city'],
                'billingAddressState'      => $data['state'],
                'billingAddressCountry'    => $data['country']
            );
        }

        /**
         * @param array $data
         * @param int   $interestFree
         *    Parcelas quebrar explode ('#'), exemplo de valor passado
         *    quantidade   valor da parcela   Total parcelado    Com juros ou sem juros (true or false)
         *    3       #       104.51     #      313.53     #  false
         */
        public function credit(array $data, $interestFree = 2)
        {
            $this->paymentSend += array(
                'creditCardToken'           => $data['card_token'],
                'paymentMethod'             => 'creditCard',
                'installmentQuantity'       => $data['quantity'],
                'installmentValue'          => $data['installment_amount'],
                //'noInterestInstallmentQuantity' => $interestFree,
                'creditCardHolderName'      => $data['card_name'],
                'creditCardHolderCPF'       => $data['cpf'],
                'creditCardHolderBirthDate' => $data['birth_date'],
                'creditCardHolderAreaCode'  => $data['ddd'],
                'creditCardHolderPhone'     => $data['phone']
            );
        }

        public function debit($bank)
        {
            $this->paymentSend += array(
                'bankName'      => $bank,
                'paymentMethod' => 'eft',
            );
        }

        public function boleto()
        {
            $this->paymentSend += array(
                'paymentMethod' => 'boleto',
            );
        }

        /**
         * @param array $cart_items
         * @return array
         *
         * FUN��O PADR�O OU CRIAR UMA FUN��O ALTERNATIVA E ALTERAR NO SEND REQUEST ()
         *
         * UTILIZAR O MESMO RETORNO
         */
        private function getItems(array $cart_items)
        {
            $items = array();
            if ($cart_items) {
                $i = 1;
                foreach ($cart_items as $item) {
                    $items['itemId' . $i] = $item['Produto']['grade']['id'];
                    $items['itemDescription' . $i] = $item['Produto']['nome'];
                    $items['itemAmount' . $i] = $item['Produto']['preco'];
                    $items['itemQuantity' . $i] = isset($item['Produto']['quantidade']) ? $item['Produto']['quantidade'] : 1;
                    $i++;
                }
            }
            return $items;
        }

        public function sendRequest(array $items)
        {
            $ch = curl_init($this->urls[1] . '/v2/transactions');
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->paymentSend + $this->getItems($items)));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec($ch);
            curl_close($ch);
            return simplexml_load_string($result);
        }

        public function getSent()
        {
            return $this->paymentSend;
        }

        public function validate($data, $type = 'CREDIT_CARD')
        {
            foreach ($this->required[$type] as $key => $message) {
                if (!isset($data[$key]) || empty($data[$key])) {
                    return $message['message'];
                }
            }
            return true;
        }

    }
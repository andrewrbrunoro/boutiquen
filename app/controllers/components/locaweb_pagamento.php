<?php

class LocawebPagamentoComponent extends Object {
	var $filiacao = '';
    var $__init = array(
        'Url' => 'https://comercio.locaweb.com.br/comercio.comp',
        'Cartao' => array(
          
            
        ),
        'Boleto' => array(
            "identificacao" => "6684311",
            "modulo" => "BOLETOLOCAWEB",
            "ambiente" => "PRODUCAO",
            "valor" => NULL,
            "numdoc" => NULL,
            "datadoc" =>NULL,
            "vencto" => NULL,
            "sacado" => NULL,
            "cgccpfsac" => NULL,
            "enderecosac" => NULL,
            "numeroendsac" => NULL,
            "complementosac" => NULL,
            "bairrosac" => NULL,
            "cepsac" => NULL,
            "cidadesac" => NULL,
            "ufsac" => NULL,
            "instr1" => NULL,
            "instr2" => NULL,
            "instr3" => NULL,
            "instr4" => NULL,
            "instr5" => NULL,
        )
    );

    public function pagar($pedido) {

        switch ($pedido['PagamentoCondicao']['sigla']) {
            case 'VISA':
            case 'MASTERCARD':
            case 'DINERS':
                return $this->pagamentoRedeCard($pedido);
                break;
			case 'BOLETO':
                return $this->pagamentoBoleto($pedido);
                break;
            default:
                break;
        }
    }

	public function pagamentoRedeCard($pedido) {
        App::import("helper", "String");
        $this->String = new StringHelper();

		App::import("helper", "Html");
        $this->Html = new HtmlHelper();
		$numparcelas = $pedido['Pedido']['parcelas'] <= 1 ? 1 : $pedido['Pedido']['parcelas'];	
		if (strlen($numparcelas) == 1){
		   $parcelas = "0" . $numparcelas;
		} else {
		   $parcelas = $numparcelas;
		}

		if ($parcelas == "01" || $parcelas == "00" || $numparcelas == ""){
			$parcelas = "00";				  
		} 
						
		$total = preg_replace('/[^0-9]/', '', $this->String->bcoToMoeda($this->String->moedaTobco($pedido['Pedido']['valor_pedido'])+$this->String->moedaTobco($pedido['Pedido']['valor_frete'])));
		$total = substr($total, 0, (strlen($total)-2)) . "." . substr($total,-2);
		
       
        $this->__init['Cartao']['FILIACAO'] 	= '38816911';
        $this->__init['Cartao']['URLBACK'] 		= $this->Html->Url('/',true).'carrinho/carrinho/finalizacao';
        $this->__init['Cartao']['URLCIMA'] 		= $this->Html->Url('/',true).'img/site/redecard_header.png';
        $this->__init['Cartao']['TOTAL'] 		= $total;
        $this->__init['Cartao']['NUMPEDIDO'] 	= $pedido['Pedido']['id'];
        $this->__init['Cartao']['BANDEIRA'] 	= up($pedido['PagamentoCondicao']['sigla']);
        $this->__init['Cartao']['PARCELAS'] 	= $parcelas;
        
		if($parcelas<=1){
			$this->__init['Cartao']['TRANSACAO']	= '73';
		}else{
			$this->__init['Cartao']['TRANSACAO']	= '08';
		}	
		
		$this->__init['Cartao']['INIFRAME ']	= 'S';		
		
		$params = http_build_query($this->__init['Cartao']);
		$log = var_export($params, true);
		$this->log($log, LOG_DEBUG);
		
		return $this->__init['Cartao'];		
	}

    /**
     * Metodo responsavel somente por submeter os dados
     */
    private function pagamentoBoleto($pedido) {
		App::import("helper", "String");
        $this->String = new StringHelper();
		$valor_compra = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
		
		$dias_boleto = Configure::read('Loja.dias_boleto');
		if($dias_boleto <= 0 || $dias_boleto > 15){
			$dias_boleto =5;
		}
		
		$this->__init['Boleto']['modulo'] 			= 'BOLETOLOCAWEB';
		$this->__init['Boleto']['valor']			= $valor_compra;
		$this->__init['Boleto']['numdoc']			= $pedido['Pedido']['id'];
		$this->__init['Boleto']['datadoc']			= date('d/m/Y');
		$this->__init['Boleto']['vencto']			= date('d/m/Y', mktime(0, 0, 0, date('m'), date('d') + $dias_boleto, date('Y')));
		$this->__init['Boleto']['sacado']			= iconv('UTF-8', 'ISO-8859-1',$pedido['Usuario']['nome']);
		$this->__init['Boleto']['cgccpfsac']		= $pedido['Usuario']['tipo_pessoa']=='J'?$pedido['Usuario']['cnpj']:$pedido['Usuario']['cpf'];
		$this->__init['Boleto']['enderecosac']		= iconv('UTF-8', 'ISO-8859-1',$pedido['Pedido']['endereco_rua']);
		$this->__init['Boleto']['numeroendsac']		= $pedido['Pedido']['endereco_numero'];
		$this->__init['Boleto']['complementosac']	= iconv('UTF-8', 'ISO-8859-1',$pedido['Pedido']['endereco_complemento']);
		$this->__init['Boleto']['bairrosac']		= iconv('UTF-8', 'ISO-8859-1',$pedido['Pedido']['endereco_bairro']);
		$this->__init['Boleto']['cepsac']			= $pedido['Pedido']['endereco_cep'];
		$this->__init['Boleto']['cidadesac']		= iconv('UTF-8', 'ISO-8859-1',$pedido['Pedido']['endereco_cidade']);
		$this->__init['Boleto']['ufsac']			= iconv('UTF-8', 'ISO-8859-1',$pedido['Pedido']['endereco_estado']);
		$this->__init['Boleto']['instr1']			= iconv('UTF-8', 'ISO-8859-1','Não receber após o vencimento.Se o pagamento for em cheque a mercadoria só será liberada após a compensação.');
		$this->__init['Boleto']['instr2']			= '';
		$this->__init['Boleto']['instr3']			= '';
		$this->__init['Boleto']['instr4']			= '';
		$this->__init['Boleto']['instr5']			= '';


		$params = http_build_query($this->__init['Boleto']);
		$url = $this->__init['Url'].'?'.$params;	
		return $url;
	
    }
}
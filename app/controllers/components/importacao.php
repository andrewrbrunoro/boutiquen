<?php

    class ImportacaoComponent extends Object
    {
        public $Import = '';
        public $Email = '';
        protected $manager = 'Oryon';
        protected $file = array(
            'file'      => 'estoque',
            'extension' => '.csv'
        );
        private $log_full = '';
        private $finish = '';
        private $alert = array(
            'errors'   => array(),
            'success'  => array(),
            'warnings' => array(),
            'ativos'   => array(),
            'inativos' => array()
        );
        private $send = true;

        public function __construct($import, $send = true)
        {
            $this->Import = $this->m('Importacao');
            $this->send = $send;
            ini_set('max_execution_time', 0);
            ini_set('default_socket_timeout', 3600);
            ini_set('memory_limit', '3096M');
            set_time_limit(0);
            $this->log_full = WWW_ROOT . 'importacao' . DS . $this->file['file'] . $this->file['extension'];
            $this->finish = WWW_ROOT . 'importacao' . DS . 'importados' . DS . date('d-m-Y_H-i-s');
            if ($this->requireBeforeStart() != false) {
                $this->$import();
            }
            #Termina a importação
            $this->requireDestruct();
            #Termina a importação
            die('end');
        }

        private function configureEmail()
        {
            App::import('Component', 'Email');
            $this->Email = new EmailComponent();

            $this->layout = false;
            $this->render = false;
            $this->Email->smtpOptions = array(
                'port'     => '465',
                'host'     => 'ssl://smtp.gmail.com',
                'username' => 'naoresponda@boutiqueno.com.br',
                'password' => 'boutiqueno2015'
            );
            $this->Email->delivery = 'smtp';
            $this->Email->lineLength = 120;
            $this->Email->sendAs = 'html';
            $this->Email->replyTo = 'Boutique Nô <naoresponda@boutiqueno.com.br>';
            $this->Email->from = 'Boutique Nô <naoresponda@boutiqueno.com.br>';
            $this->Email->to = "andrewrbrunoro@gmail.com";
            $this->Email->cc = "alessandra@prodv.com.br";
            $this->Email->subject = 'Boutique Nô - Status Importação';
            $this->readFilesToSendEmail();
        }

        private function readFilesToSendEmail()
        {
            $dir = opendir($this->finish);
            $dot = array(
                '.',
                '..'
            );
            while (false !== ($file = readdir($dir))) {
                if (!in_array($file, $dot)) {
                    $this->Email->attachments[] = $this->finish . DS . $file;
                }
            }
        }

        private function emailNotification($email = null)
        {
            if (!is_null($email)) {
                $this->configureEmail();
                $this->Email->send($email);
            }
        }

        private function push($alertName, $msg)
        {
            array_push($this->alert[$alertName], $msg);
        }

        private function requireBeforeStart()
        {
            if (!file_exists($this->log_full)) {
                $this->push('errors', $this->file['file'] . ', ainda não foi gerado pela ' . $this->manager);
                return false;
            }
            if (!file_exists(WWW_ROOT . 'importacao' . DS . 'importados')) {
                mkdir(WWW_ROOT . 'importacao' . DS . 'importados');
            }
            if (!file_exists($this->finish)) {
                if (mkdir($this->finish)) {
                    $this->push('success', 'A importação foi iniciada com sucesso.');
                } else {
                    $this->push('errors', 'Erro, não foi possível dar continuidade na importação, houve uma falha a criar o Diretório ' . $this->finish);
                    return false;
                }
            }
            return true;
        }

        private function constructLOGS()
        {
            foreach ($this->alert as $type => $text) {
                if (count($this->alert[$type]) > 0) {
                    if (!file_exists($this->finish . DS . $type)) {
                        $open = fopen($this->finish . DS . $type . '.txt', 'w');
                        foreach ($this->alert[$type] as $msg) {
                            fwrite($open, $msg . PHP_EOL);
                        }
                        fclose($open);
                    }
                }
            }
        }

        private function m($model)
        {
            App::import('Model', $model);
            return new $model();
        }

        function utf8_converter($array)
        {
            array_walk_recursive($array, function (&$item, $key) {
                if (!mb_detect_encoding($item, 'utf-8', true)) {
                    $item = utf8_encode($item);
                }
            });
            return $array;
        }

        private function requireDestruct()
        {
            $this->constructLOGS();
            if ($this->send == true) {
                $this->emailNotification('Os arquivos estão em anexo para validação.');
            }
            $this->Import->save(array(
                'Importacao' => array(
                    'id'      => null,
                    'agenda'  => DboSource::expression('NOW()'),
                    'arquivo' => $this->log_full,
                    'log'     => $this->finish
                )
            ));
            rename($this->log_full, $this->finish . '/' . $this->file['file'] . $this->file['extension']);
        }
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////
        private function full()
        {
            $handle = fopen($this->log_full, 'r');
            if ($handle) {
                $i = 0;
                while (($row = fgetcsv($handle, 0, ';')) !== false) {
                    if ($i > 0) {
                        $data[$i] = $this->utf8_converter($this->__format_data($row));
                        $data[$i]['Categoria'] = $this->categoria($data[$i]['Categoria']);
                        $fabricante = $this->fabricante($data[$i]['Fabricante']);
                        $data[$i]['Fabricante']['id'] = $fabricante['fabricante_id'];
                        $data[$i]['Produto']['marca'] = $fabricante['marca'];
                        $data[$i]['Produto']['grupo_variacao_id'] = $this->grupo_variacao($data[$i]['Produto']);
                        $data[$i]['VariacaoTipo'] = $this->tipo_variacao($data[$i]);
                        $data[$i]['Variacao'] = $this->variacoes($data[$i]);
                        $data[$i]['AtributoTipo'] = $this->tipo_atributo();
                        $data[$i]['Atributo'] = $this->atributos($data[$i]);
                        $this->categoria_atributo($data[$i]);
                        $this->produtos($data[$i]);
                    }
                    $i++;
                }
                return true;
            } else {
                $this->push('errors', 'Não foi possível fazer a leitura do arquivo');
                return false;
            }
        }

        private function stock()
        {
            $handle = fopen($this->log_full, 'r');
            if ($handle) {
                $i = 0;
                while (($row = fgetcsv($handle, 0, ';')) !== false) {
                    if ($i > 0) {
                        $data[$i] = $this->utf8_converter($this->__format_data($row));
                        $this->produto_estoque($data[$i]);
                    }
                    $i++;
                }
                return true;
            } else {
                $this->push('errors', 'Não foi possível fazer a leitura do arquivo');
                return false;
            }
        }

        private function __format_data($data)
        {
            return array(
                'Produto'    => array(
                    'status'    => (isset($data[0]) && !empty($data[0]) && $data[0] == 'S') ? (int)1 : (int)0,
                    'codigo'    => (string)(isset($data['1'])) ? $data['1'] : '',
                    'codcor'    => (string)(isset($data['2'])) ? $data['2'] : '',
                    'cor'       => (string)(isset($data['3'])) ? $data['3'] : '',
                    'tamanho'   => (string)(isset($data['4'])) ? $data['4'] : '',
                    'barra'     => (string)(isset($data['5'])) ? $data['5'] : '',
                    'descricao' => (string)(isset($data['6'])) ? $data['6'] : '',
                    'resumo'    => (string)(isset($data['7'])) ? $data['7'] : '',
                    'preco'     => (string)(isset($data['9'])) ? $data['9'] : ''
                ),
                'Categoria'  => array(
                    'departamento' => (string)(isset($data['11'])) ? $data['11'] : '',
                    'categoria'    => (string)(isset($data['10'])) ? $data['10'] : ''
                ),
                'Fabricante' => array(
                    'nome'   => (string)(isset($data['14'])) ? $data['14'] : 'Outros',
                    'codigo' => (isset($data[17]) && !empty($data[17])) ? (string)$data[17] : 'Outros'
                ),
                'Estoque'    => array(
                    'malcon' => (int)(isset($data[25])) ? (int)$data[25] : 0,
                    'ramiro' => (int)(isset($data[27])) ? (int)$data[27] : 0,
                    'wallig' => (int)(isset($data[28])) ? (int)$data[28] : 0,
                    'total'  => (int)(isset($data[29])) ? (int)$data[29] : 0,
                    'barra'  => (int)(isset($data[30])) ? (int)$data[30] : 0
                )
            );
        }

        private function categoria($data)
        {
            $category = $this->m('Categoria');
            $departamento_id = '';
            $categoria_id = '';
            $departamento = $category->find('first', array(
                'fields'     => array('Categoria.id,Categoria.nome,Categoria.descricao,Categoria.status'),
                'recursive'  => '-1',
                'conditions' => array(
                    'Categoria.nome' => $data['departamento']
                )
            ));
            if (!$departamento) {
                $dataSave['Categoria'] = array(
                    'id'        => null,
                    'nome'      => $data['departamento'],
                    'descricao' => $data['departamento'],
                    'status'    => 1
                );
                $save = $category->save($dataSave);
                if ($save) {
                    $departamento_id = $category->id;
                    $this->push('success', 'Departamento ' . $data['departamento'] . ', foi criado com sucesso.');
                } else {
                    $this->push('errors', 'Erro ao tentar criar o Departamento ' . $data['departamento']);
                }
            } else {
                $departamento_id = $departamento['Categoria']['id'];
            }

            if (!empty($departamento_id)) {
                $categoria = $category->find('first', array(
                    'recursive'  => '-1',
                    'fields'     => array('Categoria.id,Categoria.nome,Categoria.descricao,Categoria.status'),
                    'conditions' => array(
                        'Categoria.nome'      => $data['categoria'],
                        'Categoria.parent_id' => $departamento_id
                    )
                ));

                if (!$categoria) {
                    $dataSave['Categoria'] = array(
                        'id'        => null,
                        'parent_id' => $departamento_id,
                        'nome'      => $data['categoria'],
                        'descricao' => $data['categoria'],
                        'status'    => 1
                    );
                    $save = $category->save($dataSave);
                    if ($save) {
                        $categoria_id = $category->id;
                        $this->push('success', 'Departamento ' . $data['departamento'] . ', foi criado com sucesso.');
                    } else {
                        $this->push('errors', 'Erro ao tentar criar o Departamento ' . $data['departamento']);
                    }
                } else {
                    $categoria_id = $categoria['Categoria']['id'];
                }
            }
            return (!empty($categoria_id)) ? $categoria_id : $departamento_id;
        }

        private function fabricante($data)
        {
            $manufacturer = $this->m('Fabricante');
            $fabricante_id = '';
            $fabricante = $manufacturer->find('first', array(
                'fields'     => array('Fabricante.id, Fabricante.nome'),
                'recursive'  => '-1',
                'conditions' => array(
                    'Fabricante.nome' => $data['nome']
                )
            ));
            if (!$fabricante) {
                $dataSave['Fabricante'] = array(
                    'nome'    => $data['nome'],
                    'status'  => 1,
                    'seo_url' => low(Inflector::slug($data['nome']))
                );
                if ($manufacturer->save($dataSave)) {
                    $fabricante_id = $manufacturer->id;
                    $this->push('success', 'Fabricante ' . $data['nome'] . ', foi criado com sucesso');
                } else {
                    $this->push('errors', 'Erro ao tentar criar o Fabricante ' . $data['nome']);
                }
            } else {
                $fabricante_id = $fabricante['Fabricante']['id'];
            }
            return array(
                'marca'         => $data['nome'],
                'fabricante_id' => $fabricante_id
            );
        }

        private function grupo_variacao($data)
        {
            $variationGroup = $this->m('VariacaoGrupo');
            $group_id = '';
            if ($data['cor'] == 'UNICO' && $data['tamanho'] == 'UNICO') {
                $grupoVariacaoTexto = 'Padrão';
            } else {
                $grupoVariacaoTexto = 'TamanhoCor';
            }
            $grupo_variacao = $variationGroup->find('first', array(
                'fields'     => array('VariacaoGrupo.nome, VariacaoGrupo.id'),
                'recursive'  => '-1',
                'conditions' => array(
                    'VariacaoGrupo.nome' => $grupoVariacaoTexto
                )
            ));
            if (!$grupo_variacao) {
                $dataSave = array(
                    'id'     => null,
                    'nome'   => $grupoVariacaoTexto,
                    'status' => 1
                );
                if ($variationGroup->save($dataSave)) {
                    $group_id = $variationGroup->id;
                    $this->push('success', 'Grupo Variação : ' . $grupoVariacaoTexto . ', foi criado com sucesso.');
                } else {
                    $this->push('errors', 'Erro ao criar o Grupo de variação ' . $grupoVariacaoTexto);
                }
            } else {
                $group_id = $grupo_variacao['VariacaoGrupo']['id'];
            }
            return $group_id;
        }

        private function tipo_variacao($data)
        {
            $typeVariation = $this->m('VariacaoTipo');
            $variacao_tipo_id = '';
            if ($data['Produto']['cor'] == 'UNICO' && $data['Produto']['tamanho'] == 'UNICO') {
                $typeVariationText = 'Padrão';
                $variacaoTipo = $typeVariation->find('first', array(
                    'fields'     => array('VariacaoTipo.nome, VariacaoTipo.variacao_grupo_id, VariacaoTipo.id'),
                    'recursive'  => '-1',
                    'conditions' => array(
                        'VariacaoTipo.nome'              => $typeVariationText,
                        'VariacaoTipo.variacao_grupo_id' => $data['Produto']['grupo_variacao_id']
                    )
                ));
                if (!$variacaoTipo) {
                    $dataSave['VariacaoTipo'] = array(
                        'id'                => null,
                        'variacao_grupo_id' => $data['Produto']['grupo_variacao_id'],
                        'status'            => 1,
                        'nome'              => $typeVariationText,
                        'codigo'            => low(Inflector::slug($typeVariationText, '-'))
                    );
                    if ($typeVariation->save($dataSave)) {
                        $variacao_tipo_id[] = $typeVariation->id;
                        $this->push('success', 'Variação Tipo : ' . $typeVariationText . ', foi criado com sucesso');
                    } else {
                        $this->push('errors', 'Erro ao criar a Variação Tipo ' . $typeVariationText);
                    }
                } else {
                    $variacao_tipo_id[] = $variacaoTipo['VariacaoTipo']['id'];
                }
            } else {
                $types = array(
                    'Tamanho',
                    'Cor'
                );
                foreach ($types as $typeVariationText) {
                    $variacaoTipo = $typeVariation->find('first', array(
                        'fields'     => array('VariacaoTipo.nome, VariacaoTipo.variacao_grupo_id, VariacaoTipo.id'),
                        'recursive'  => '-1',
                        'conditions' => array(
                            'VariacaoTipo.nome'              => $typeVariationText,
                            'VariacaoTipo.variacao_grupo_id' => $data['Produto']['grupo_variacao_id']
                        )
                    ));
                    if (!$variacaoTipo) {
                        $dataSave['VariacaoTipo'] = array(
                            'id'                => null,
                            'variacao_grupo_id' => $data['Produto']['grupo_variacao_id'],
                            'status'            => 1,
                            'nome'              => $typeVariationText,
                            'codigo'            => low(Inflector::slug($typeVariationText, '-'))
                        );
                        if ($typeVariation->save($dataSave)) {
                            $variacao_tipo_id[] = $typeVariation->id;
                            $this->push('success', 'Variação Tipo : ' . $typeVariationText . ', foi criado com sucesso');
                        } else {
                            $this->push('errors', 'Erro ao criar a Variação Tipo ' . $typeVariationText);
                        }
                    } else {
                        $variacao_tipo_id[] = $variacaoTipo['VariacaoTipo']['id'];
                    }
                }
            }

            return $variacao_tipo_id;
        }

        private function variacoes($data)
        {
            $variation = $this->m('Variacao');
            $variationType = $this->m('VariacaoTipo');
            $variacao_id = '';

            foreach ($data['VariacaoTipo'] as $id) {
                $variacoes = '';
                $variacoes = $variationType->find('first', array(
                    'recursive'  => '-1',
                    'conditions' => array(
                        'VariacaoTipo.id' => $id
                    )
                ));
                if ($variacoes) {
                    if ($data['Produto']['cor'] == 'UNICO' && $data['Produto']['tamanho'] == 'UNICO') {
                        $variacao = 'Padrão';
                    } else {
                        if ($variacoes['VariacaoTipo']['nome'] == 'Cor') {
                            $variacao = $data['Produto']['cor'];
                        } else {
                            $variacao = $data['Produto']['tamanho'];
                        }
                    }
                    $vfind = '';
                    $vfind = $variation->find('first', array(
                        'recursive'  => '-1',
                        'conditions' => array(
                            'Variacao.valor'            => $variacao,
                            'Variacao.variacao_tipo_id' => $variacoes['VariacaoTipo']['id']
                        )
                    ));
                    if (!$vfind) {
                        $dataSave = '';
                        $dataSave['Variacao'] = array(
                            'valor'            => $variacao,
                            'variacao_tipo_id' => $variacoes['VariacaoTipo']['id'],
                            'status'           => 1
                        );
                        if ($variation->save($dataSave)) {
                            $variacao_id[] = $variation->id;
                            $this->push('success', 'Variação :' . $dataSave['Variacao']['valor'] . ', criada com sucesso');
                        } else {
                            $this->push('errors', 'Erro ao criar a Variação ' . $dataSave['Variacao']['valor']);
                        }
                    } else {
                        $variacao_id[] = $vfind['Variacao']['id'];
                    }
                }
            }
            return $variacao_id;
        }

        private function tipo_atributo()
        {
            $attribute = $this->m('AtributoTipo');
            $atributo_tipo_id = '';
            $array = array(
                'Tamanho',
                'Fabricante'
            );
            foreach ($array as $val) {
                $find = '';
                $find = $attribute->find('first', array(
                    'recursive'  => '-1',
                    'conditions' => array(
                        'AtributoTipo.nome' => $val
                    )
                ));
                if (!$find) {
                    $dataSave = '';
                    $dataSave['AtributoTipo'] = array(
                        'nome'   => $val,
                        'status' => 1,
                        'codigo' => low(Inflector::slug($val, '-'))
                    );
                    if ($attribute->save($dataSave)) {
                        $atributo_tipo_id[$val] = $attribute->id;
                        $this->push('success', 'Atributo tipo : ' . $dataSave['AtributoTipo']['nome'] . ', criado com sucesso.');
                    } else {
                        $this->push('errors', 'Erro ao criar o Atributo tipo ' . $dataSave['AtributoTipo']['nome']);
                    }
                } else {
                    $atributo_tipo_id[$val] = $find['AtributoTipo']['id'];
                }
            }
            return $atributo_tipo_id;
        }

        private function categoria_atributo($data)
        {
            $attrCategory = $this->m('CategoriaAtributo');

            foreach ($data['Atributo'] as $id) {

                $categoria_atributo = $attrCategory->find('first', array(
                    'recursive'  => '-1',
                    'conditions' => array(
                        'CategoriaAtributo.categoria_id' => $data['Categoria'],
                        'CategoriaAtributo.atributo_id'  => $id
                    )
                ));

                if (!$categoria_atributo) {
                    $dataSave['CategoriaAtributo'] = array(
                        'categoria_id' => $data['Categoria'],
                        'atributo_id'  => $id
                    );
                    $query = 'INSERT INTO categorias_atributos (categoria_id, atributo_id) VALUES ("' . $data['Categoria'] . '","' . $id . '")';
                    $attrCategory->query($query);
                }

            }

        }

        private function atributos($data)
        {
            $attr = $this->m('Atributo');
            $varGroup = $this->m('VariacaoGrupo');
            $atributo_id = '';

            $variacao_grupo = $varGroup->find('first', array(
                'fields'     => array('VariacaoGrupo.id, VariacaoGrupo.nome'),
                'recursive'  => '-1',
                'conditions' => array(
                    'VariacaoGrupo.id' => $data['Produto']['grupo_variacao_id']
                )
            ));

            if ($variacao_grupo['VariacaoGrupo']['nome'] == 'Padrão') {
                $atributo = $attr->find('first', array(
                    'fields'     => array('Atributo.id'),
                    'recursive'  => '-1',
                    'conditions' => array(
                        'Atributo.valor'            => $data['Fabricante']['nome'],
                        'Atributo.atributo_tipo_id' => $data['AtributoTipo']['Fabricante']
                    )
                ));

                if (!$atributo) {
                    $dataSave['Atributo'] = array(
                        'id'               => null,
                        'valor'            => $data['Fabricante']['nome'],
                        'status'           => 1,
                        'atributo_tipo_id' => $data['AtributoTipo']['Fabricante'],
                        'filtro'           => 1,
                        'codigo'           => low(Inflector::slug($data['AtributoTipo']['Fabricante'], '-'))
                    );
                    $attr->save($dataSave);
                    $atributo_id[] = $attr->id;
                } else {
                    $atributo_id[] = $atributo['Atributo']['id'];
                }
            } else {
                foreach ($data['AtributoTipo'] as $type => $id) {

                    $attr_nome = $type_attr_id = '';
                    if ($type == 'Fabricante') {
                        $attr_nome = $data['Fabricante']['nome'];
                        $type_attr_id = $id;
                    }

                    if ($type == 'Tamanho') {
                        $attr_nome = $data['Produto']['tamanho'];
                        $type_attr_id = $id;
                    }

                    $atributo = $attr->find('first', array(
                        'recursive'  => '-1',
                        'conditions' => array(
                            'Atributo.valor'            => $attr_nome,
                            'Atributo.atributo_tipo_id' => $type_attr_id
                        )
                    ));

                    if (!$atributo) {
                        $dataSave['Atributo'] = array(
                            'id'               => null,
                            'valor'            => $attr_nome,
                            'status'           => 1,
                            'atributo_tipo_id' => $type_attr_id,
                            'filtro'           => 1,
                            'codigo'           => low(Inflector::slug($attr_nome, '-'))
                        );
                        $attr->save($dataSave);
                        $atributo_id[] = $attr->id;
                    } else {
                        $atributo_id[] = $atributo['Atributo']['id'];
                    }

                }
            }
            return $atributo_id;
        }

        private function produto_estoque($data)
        {
            $product = $this->m('Produto');
            $produto_id = '';
            $new = false;
            $produto = $product->find('first', array(
                'fields'     => array('Produto.id, Produto.codigo'),
                'recursive'  => '-1',
                'conditions' => array(
                    'Produto.codigo' => $data['Produto']['codigo']
                )
            ));

            if (!$produto) {
                $dataSave['Produto'] = array(
                    'id'             => null,
                    'nome'           => $data['Produto']['descricao'],
                    'referencia'     => $data['Produto']['descricao'],
                    'nome_loja'      => null,
                    'codigo'         => $data['Produto']['codigo'],
                    'codigo_externo' => $data['Fabricante']['codigo'],
                    'codigo_barras'  => $data['Produto']['barra'],
                    'status'         => $data['Produto']['status']
                );
                $new = true;
            } else {
                $dataSave['Produto'] = array(
                    'id'             => $produto['Produto']['id'],
                    'referencia'     => $data['Produto']['descricao'],
                    'codigo_externo' => $data['Fabricante']['codigo'],
                    'status'         => $data['Produto']['status']
                );
                $new = false;
            }
            if ($product->save($dataSave)) {
                if ($new) {
                    $produto_id = $product->id;
                    $this->push('success', 'Novo Produto : ' . $dataSave['Produto']['nome']);
                } else {
                    $produto_id = $produto['Produto']['id'];
                    $this->push('success', 'Produto : ' . $produto['Produto']['codigo'] . ', atualizado.');
                }
            } else {
                $this->push('errors', 'Erro ao tentar salvar o Produto : ' . $data['Produto']['descricao']);
                return false;
            }

            $this->grade_estoque($data, $produto_id);

            return true;
        }

        private function grade_estoque($data, $produto_id)
        {
            $product = $this->m('Produto');
            $grid = $this->m('Grade');
            $productVariation = $this->m('VariacaoProduto');
            $productCategory = $this->m('ProdutoCategoria');
            $attrProduct = $this->m('AtributoProduto');
            $grade_id = '';

            $ref = $data['Produto']['codigo'] . '|' . $data['Produto']['cor'] . '|' . $data['Produto']['tamanho'];
            $sku = $data['Produto']['codigo'] . '-' . $data['Produto']['cor'] . '-' . $data['Produto']['tamanho'];

            $estoque_total = 0;
            foreach ($data['Estoque'] as $k) {
                $estoque_total += $k;
            }

            $grade = $grid->find('first', array(
                'recursive'  => '-1',
                'conditions' => array(
                    'Grade.sku' => $sku
                )
            ));

            if (!$grade) {
                $dataSave['Grade']['id'] = null;
                $dataSave['Grade']['quantidade'] = $estoque_total;
                $dataSave['Grade']['quantidade_disponivel'] = $estoque_total;
                $dataSave['Grade']['quantidade_alocada'] = 0;
            } else {
                $dataSave['Grade']['id'] = $grade['Grade']['id'];
                $dataSave['Grade']['quantidade'] = $estoque_total;
                $dataSave['Grade']['quantidade_disponivel'] = $estoque_total - $grade['Grade']['quantidade_alocada'];
                $dataSave['Grade']['quantidade_alocada'] = $grade['Grade']['quantidade_alocada'];
            }
            $dataSave['Grade'] = array(
                    'referencia'   => $ref,
                    'sku'          => $sku,
                    'altura'       => 14,
                    'largura'      => 12,
                    'profundidade' => 34,
                    'peso'         => 1,
                    'preco'        => $data['Produto']['preco'],
                    'status'       => $data['Produto']['status']
                ) + $dataSave['Grade'];
            if ($grid->save($dataSave)) {
                $grade_id = $grid->id;
                $this->push('success', '/////////////////////');
                $this->push('success', 'Referência : ' . $data['Produto']['descricao']);
                $this->push('success', 'Status : ' . $data['Produto']['status']);
                $this->push('success', 'Quantidade em todas as lojas : ' . $estoque_total);
                $this->push('success', 'Quantidade disponível : ' . $dataSave['Grade']['quantidade_disponivel']);
                $this->push('success', 'Quantidade alocada : ' . $dataSave['Grade']['quantidade_alocada']);
                $this->push('success', '/////////////////////');

                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', '/////////////////////');
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Referência : ' . $data['Produto']['descricao']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Status : ' . $data['Produto']['status']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Quantidade em todas as lojas : ' . $estoque_total);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Quantidade disponível : ' . $dataSave['Grade']['quantidade_disponivel']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Quantidade alocada : ' . $dataSave['Grade']['quantidade_alocada']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', '/////////////////////');
            } else {
                $this->push('errors', 'Erro ao tentar atualizar a Referência : ' . $data['Produto']['descricao']);
            }
        }

        private function produtos($data)
        {
            $product = $this->m('Produto');
            $produto_id = '';
            $produto = $product->find('first', array(
                'fields'     => array('Produto.id, Produto.codigo'),
                'recursive'  => '-1',
                'conditions' => array(
                    'Produto.codigo' => $data['Produto']['codigo']
                )
            ));

            if (!$produto) {
                $dataSave['Produto'] = array(
                    'id'               => null,
                    'nome'             => $data['Produto']['descricao'],
                    'referencia'       => $data['Produto']['descricao'],
                    'nome_loja'        => null,
                    'codigo'           => $data['Produto']['codigo'],
                    'codigo_externo'   => $data['Fabricante']['codigo'],
                    'codigo_barras'    => $data['Produto']['barra'],
                    'variaca_grupo_id' => $data['Produto']['grupo_variacao_id'],
                    'fabricante_id'    => $data['Fabricante']['id'],
                    'status'           => $data['Produto']['status']
                );
                $new = true;
            } else {
                $dataSave['Produto'] = array(
                    'referencia'       => $data['Produto']['descricao'],
                    'variaca_grupo_id' => $data['Produto']['grupo_variacao_id'],
                    'codigo_externo'   => $data['Fabricante']['codigo'],
                    'fabricante_id'    => $data['Fabricante']['id'],
                    'status'           => $data['Produto']['status']
                );
                $new = false;
//                if ($produto['Produto']['id'] == 2230) {
//                    echo '<pre>';
//                    print_r($data);
//                    echo '//////////';
//                    print_r($dataSave);
//                    exit;
//                }

                $query = "
                UPDATE produtos SET referencia = '".$data['Produto']['descricao']."',
                variacao_grupo_id = '".$data['Produto']['grupo_variacao_id']."',
                codigo_externo = '".$data['Produto']['codigo_externo']."',
                fabricante_id = '".$data['Fabricante']['id']."',
                status = '".$data['Produto']['status']."'
                WHERE id = '".$produto['Produto']['id']."'
                ";

                $product->query($query);


            }
            if ($product->save($dataSave, false)) {
                if ($new) {
                    $produto_id = $product->id;
                    $this->push('success', 'Novo Produto : ' . $dataSave['Produto']['nome']);
                } else {
                    $produto_id = $produto['Produto']['id'];
                    $this->push('success', 'Produto : ' . $produto['Produto']['codigo'] . ', atualizado.');
                }
            } else {
                $this->push('errors', 'Erro ao tentar salvar o Produto : ' . $data['Produto']['descricao']);
                return false;
            }

            $this->produto_categorias($data, $produto_id);
            $this->grade($data, $produto_id);

            return true;
        }

        private function produto_categorias($data, $produto_id)
        {
            $productCategory = $this->m('ProdutoCategoria');

            $produto_categoria = $productCategory->find('first', array(
                'recursive'  => '-1',
                'conditions' => array(
                    'ProdutoCategoria.produto_id'   => $produto_id
                )
            ));

            if ($produto_categoria) {
                $delete = 'DELETE FROM produtos_categorias WHERE produto_id = '.$produto_id;
                $productCategory->query($delete);
            }

            $dataSave['ProdutoCategoria'] = array(
                'categoria_id' => $data['Categoria'],
                'produto_id'   => $produto_id
            );
            $query = 'INSERT INTO produtos_categorias (produto_id, categoria_id) VALUES ("' . $data['Categoria'] . '", "' . $produto_id . '")';
            $productCategory->query($query);


        }

        private function grade($data, $produto_id)
        {
            $product = $this->m('Produto');
            $grid = $this->m('Grade');
            $productVariation = $this->m('VariacaoProduto');
            $productCategory = $this->m('ProdutoCategoria');
            $attrProduct = $this->m('AtributoProduto');
            $grade_id = '';

            $ref = $data['Produto']['codigo'] . '|' . $data['Produto']['cor'] . '|' . $data['Produto']['tamanho'];
            $sku = $data['Produto']['codigo'] . '-' . $data['Produto']['cor'] . '-' . $data['Produto']['tamanho'];

            $estoque_total = 0;
            foreach ($data['Estoque'] as $k) {
                $estoque_total += $k;
            }

            $grade = $grid->find('first', array(
                'recursive'  => '-1',
                'conditions' => array(
                    'Grade.sku' => $sku
                )
            ));

            if (!$grade) {
                $dataSave['Grade']['id'] = null;
                $dataSave['Grade']['quantidade'] = $estoque_total;
                $dataSave['Grade']['quantidade_disponivel'] = $estoque_total;
                $dataSave['Grade']['quantidade_alocada'] = 0;
            } else {
                $dataSave['Grade']['id'] = $grade['Grade']['id'];
                $dataSave['Grade']['quantidade'] = $estoque_total;
                $dataSave['Grade']['quantidade_disponivel'] = $estoque_total - $grade['Grade']['quantidade_alocada'];
                $dataSave['Grade']['quantidade_alocada'] = $grade['Grade']['quantidade_alocada'];
            }
            $dataSave['Grade'] = array(
                    'referencia'   => $ref,
                    'sku'          => $sku,
                    'altura'       => 14,
                    'largura'      => 12,
                    'profundidade' => 34,
                    'peso'         => 1,
                    'preco'        => $data['Produto']['preco'],
                    'status'       => $data['Produto']['status']
                ) + $dataSave['Grade'];
            if ($grid->save($dataSave)) {
                $grade_id = $grid->id;
                $this->push('success', '/////////////////////');
                $this->push('success', 'Referência : ' . $data['Produto']['descricao']);
                $this->push('success', 'Status : ' . $data['Produto']['status']);
                $this->push('success', 'Quantidade em todas as lojas : ' . $estoque_total);
                $this->push('success', 'Quantidade disponível : ' . $dataSave['Grade']['quantidade_disponivel']);
                $this->push('success', 'Quantidade alocada : ' . $dataSave['Grade']['quantidade_alocada']);
                $this->push('success', '/////////////////////');

                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', '/////////////////////');
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Referência : ' . $data['Produto']['descricao']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Status : ' . $data['Produto']['status']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Quantidade em todas as lojas : ' . $estoque_total);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Quantidade disponível : ' . $dataSave['Grade']['quantidade_disponivel']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', 'Quantidade alocada : ' . $dataSave['Grade']['quantidade_alocada']);
                $this->push(($data['Produto']['status'] == 1) ? 'ativos' : 'inativos', '/////////////////////');
            } else {
                $this->push('errors', 'Erro ao tentar atualizar a Referência : ' . $data['Produto']['descricao']);
            }

            foreach ($data['Variacao'] as $var) {
                $variacaoProduto = $productVariation->find('first', array(
                    'recursive'  => '-1',
                    'conditions' => array(
                        'VariacaoProduto.variacao_id' => $var,
                        'VariacaoProduto.produto_id'  => $produto_id,
                        'VariacaoProduto.grade_id'    => $grade_id
                    )
                ));
                if (!$variacaoProduto) {
                    $query = '';
                    $query = '
					INSERT INTO variacoes_produtos
					(variacao_id,produto_id,grade_id,status)
					VALUES
					("' . $var . '","' . $produto_id . '","' . $grade_id . '",1)';
                    $productVariation->query($query);
                }
            }

            $produto_categoria = $productCategory->find('first', array(
                'recursive'  => '-1',
                'conditions' => array(
                    'ProdutoCategoria.produto_id' => $produto_id
                )
            ));

            if ($produto_categoria) {
                $productCategory->query('DELETE FROM produtos_categorias WHERE produto_id = ' . $produto_id);
            }

            $productCategory->query('INSERT INTO produtos_categorias (produto_id, categoria_id) VALUES ("' . $produto_id . '","' . $data['Categoria'] . '")');

            $product->atualiza_grade_produto($produto_id);
        }

    }
<?php

    /**
     * @author Andrew Rodrigues Brunoro
     *
     * Class KomerciComponent
     *
     * Adicionar os ip's que ir�o fazer a transa��o direto na userede.com.br
     * O ip da sua m�quina caso seja feita
     */
    class KomerciComponent extends Object
    {
        // Configura��o de acesso
        protected $config = array(
            'user'  => 'andrewrbrunoro',
            'senha' => 'Ojm454821'
        );
        // Par�metros para pegar autoriza��o
        protected $params = array(
            'Filiacao'     => '57252440',
            'Total'        => '',
            'Transacao'    => '',
            'Parcelas'     => '',
            'NumPedido'    => '',
            'Nrcartao'     => '',
            'CVC2'         => '',
            'Mes'          => '',
            'Ano'          => '',
            'Portador'     => '',
            'ConfTxn'      => '',
            'IATA'         => '',
            'Distribuidor' => '',
            'Concentrador' => '',
            'TaxaEmbarque' => '',
            'Entrada'      => '',
            'Pax1'         => '',
            'Pax2'         => '',
            'Pax3'         => '',
            'Pax4'         => '',
            'Numdoc1'      => '',
            'Numdoc2'      => '',
            'Numdoc3'      => '',
            'Numdoc4'      => '',
            'Add_Data'     => ''
        );

        /**
         * Nome de gera��o do log
         * @var string
         */
        private $logName = '';

        /**
         * Retorno da RedeCard
         * @var string
         */
        private $return = '';

        /**
         * @param array $params
         * Passa os par�metros obrigat�rios de acordo com o que for usado
         */
        public function __construct(array $params)
        {
            parent::__construct();
            //Seta os par�metros para uso da classe
            $this->setParamsValue($params);
            $this->logName = 'Komerci-pedido-' . $this->params['NumPedido'];
        }

        /**
         * Verifica quais o par�metros que est� sendo passado
         * Caso exista o indice do par�metro passado, adiciona o valor para vari�vel $params da classe
         * @param array $params
         */
        private function setParamsValue(array $params)
        {
            foreach ($this->params as $key => $value) {
                if (!empty($params[$key])) {
                    $this->params[$key] = $params[$key];
                }
            }
        }

        /**
         * Por seguran�a codifica os dados do cart�o nesta etapa
         * Gera o log de transa��o
         */
        private function logGenerator()
        {
            //Codifica os dados principais
            $this->params['Nrcartao'] = md5($this->params['Nrcartao']);
            $this->params['CVC2'] = md5($this->params['CVC2']);
            $this->params['Mes'] = md5($this->params['Mes']);
            $this->params['Ano'] = md5($this->params['Ano']);
            //Cria o log
            $this->log(var_export($this->params, true), $this->logName);
            $this->log(var_export($this->return, true), $this->logName);
        }

        /**
         * Atualiza o pedido na database e adiciona os par�metros de retorno
         */
        private function updateDatabase($NumPedido)
        {
            App::import('Model', 'Pedido');
            $pedido = new Pedido();

            $exists = $pedido->find('first', array(
                'recursive'  => '-1',
                'conditions' => array(
                    'Pedido.id' => $NumPedido
                )
            ));

            if (isset($exists['Pedido']['id']) && $exists) {
                $dataUpdate['Pedido'] = array(
                    'id'                   => $exists['Pedido']['id'],
                    //                    'rede_status'            => (string)$this->return['rede_status'],
                    //                    'rede_reason'            => (string)$this->return['rede_reason'],
                    //                    'rede_gateway_reference' => $this->return['rede_gateway_reference'],
                    //                    'rede_auth_code'         => $this->return['rede_auth_code']
                    'redecard_confirmacao' => $this->return['NUMAUTENT'],
                    'redecard_retorno'     => $this->return['rede_auth_code'],
                    'redecard_data'        => $this->return['DATA'],
                    'redecard_comprovante' => $this->return['rede_reason']
                );
                $pedido->save($dataUpdate, false);
            }
        }

        /**
         * Efetua a comunica��o com o servidor da RedeCard
         * @param $method
         * @return mixed
         */
        private function CallCurl($method)
        {
            $xml_data = implode("", array_map(create_function('$key, $value', 'return "<$key>$value</$key>";'), array_keys($this->params), array_values($this->params)));
            $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" . "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " . "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " . "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" . "<soap:Body>" . "<$method xmlns=\"http://ecommerce.redecard.com.br\">" . $xml_data . "</$method>" . "</soap:Body>" . "</soap:Envelope>";
            $headers = array(
                "Content-Type: text/xml; charset=utf-8",
                "SOAPAction: \"http://ecommerce.redecard.com.br/$method\"",
                "Content-length: " . strlen($xml)
            );
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, 'https://ecommerce.redecard.com.br/pos_virtual/wskomerci/cap.asmx');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            curl_close($curl);
            return $result;
        }

        /**
         * Fun��o para Autoriza��o de compra com cr�dito
         * Campos obrigat�rios
         *  'Filiacao'     => '57252440',
         *  'Total'        => '',
         *  'Transacao'    => '',
         *  'Parcelas'     => '',
         *  'NumPedido'    => '',
         *  'Nrcartao'     => '',
         *  'CVC2'         => '',
         *  'Mes'          => '',
         *  'Ano'          => '',
         *  'Portador'     => '',
         *  'ConfTxn'      => '',
         */
        public function GetAuthorized()
        {
            $return = $this->defaultOutput($this->CallCurl('GetAuthorized'));;
            $this->updateDatabase($this->params['NumPedido']);
            return $return;
        }

        /**
         * Sa�da padr�o para as mensagens da RedeCard
         * $path = N�s para a leitura do retorno
         * @param       $result
         * @param array $paths
         * @return string
         */
        private function defaultOutput($result, array $paths = array(
            'Body',
            'GetAuthorizedResponse',
            'GetAuthorizedResult',
            'AUTHORIZATION'
        ))
        {
            $clean_xml = str_ireplace(array(
                'SOAP-ENV:',
                'SOAP:'
            ), '', $result);
            $xml = simplexml_load_string($clean_xml);
            foreach ($paths as $path) {
                $xml = $xml->$path;
            }
            $xml->MSGRET = iconv("CP1252", "UTF-8", urldecode($xml->MSGRET));
            $this->return = $xml;
            $this->setDatabaseRedeCard($xml);
            return $this->return;
        }

        /**
         * 0 Pendente
         * 1 Confirmada
         * 2 N�o Aprovada
         * 3 Desfeita
         * 4 Estornada
         * @param $xml
         */
        private function setDatabaseRedeCard($xml)
        {
            $this->return['rede_status'] = (string)$xml->CODRET;
            $this->return['rede_reason'] = (string)$xml->MSGRET;
            if (in_array($xml->CODRED, array(
                '0',
                '1'
            ))) {
                $this->return['rede_gateway_reference'] = (string)$xml->NUMSQN;
                $this->return['rede_auth_code'] = (string)$xml->NUMAUTENT;
            }
        }

        /**
         * Finaliza a Classe gerando o log da transa��o
         */
        public function __destruct()
        {
            //Gera o log de transa��o
            $this->logGenerator();
        }

    }
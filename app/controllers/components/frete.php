<?php

    /**
     * Classe que retorna o endereço pelo CEP e o Frete Do cliente baseado na regras de frete do BCO
     * @Author Luan Garcia <luan.garcia@gmail.com>
     * */
    Class FreteComponent extends Object
    {
        public $jundiai_desconto = 15;
        private $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
        //public $tipos = array('PAC (Correios)' => 41106);
        //public $tipos = array('PAC (Correios)' => 41106, 'SEDEX (Correios)' => 40010, 'SEDEX 10' => 40215);
        public $tipos = array(
            'PAC (Correios)'   => 41068,
            'SEDEX (Correios)' => 40096
        );
        public $correios = array(
            'altura_padrao'      => 2,
            'largura_padrao'     => 11,
            'comprimento_padrao' => 16,
            'coeficiente_volume' => 6.000,
            'altura_max'         => 105,
            'altura_min'         => 2,
            'comprimento_max'    => 105,
            'comprimento_min'    => 16,
            'largura_max'        => 105,
            'largura_min'        => 11,
            'sum_max'            => 200,
            'check_dimensions'   => true
        );
        public $campos = array(
            "nCdEmpresa"          => '',
            //codigo da empresa se tiver afiliacao com os correios
            "sDsSenha"            => '',
            //senha da afiliacao,
            "nCdServico"          => null,
            //servico sedex,sedex10,pac
            "sCepOrigem"          => '91040000',
            //origem do frete
            "sCepDestin"          => null,
            //destino do pedido
            "nVlPeso"             => null,
            //peso do pedido 0.3 para 300 gramas max 30kg
            "nCdFormato"          => 1,
            //formato do pacote, caixa,envelope etc,
            "nVlComprimento"      => 0,
            //comprimento do pacote,
            "nVlAltura"           => 0,
            //altura do pacote,
            "nVlLargura"          => 0,
            //largura do pacote,
            "nVlDiametro"         => 0,
            //diametro do pacote,
            "sCdMaoPropria"       => "N",
            //entregar somente em mao propria
            "nVlValorDeclarado"   => 0,
            //declarar valor do pedido
            "sCdAvisoRecebimento" => "N",
            //aviso de recebimento,
            "StrRetorno"          => "XML"
        );

        /**
         * Retorna o valor e o prazo dos serviços dos correios (PAC,SEDEX e SEDEX10)
         * */
        public function correios($cep, $codigo, $carrinho)
        {

            App::import("Xml");
            $cep = preg_replace("/[^0-9]+/", "", $cep);
            //se exceder o peso colocar o peso max de 30 kg
            $pesoCubicoTotal = 0;
            $peso = 0;
            $altura = 0;
            $comprimento = 0;
            $largura = 0;

            foreach ($carrinho['itens'] as $_product) {

                unset($_item);

                $itemAltura = 0;
                $itemLargura = 0;
                $itemComprimento = 0;

                if ($_product['altura'] == '' || (int)$_product['altura'] == 0 || (int)$_product['altura'] < $this->correios['altura_padrao'])
                    $_item[] = $this->correios['altura_padrao']; else
                    $_item[] = $_product['altura'];

                if ($_product['largura'] == '' || (int)$_product['largura'] == 0 || (int)$_product['largura'] < $this->correios['largura_padrao'])
                    $_item[] = $this->correios['largura_padrao']; else
                    $_item[] = $_product['largura'];

                if ($_product['profundidade'] == '' || (int)$_product['profundidade'] == 0 || (int)$_product['profundidade'] < $this->correios['comprimento_padrao'])
                    $_item[] = $this->correios['comprimento_padrao']; else
                    $_item[] = $_product['profundidade'];
                sort($_item);
                $itemAltura = $_item[0];
                $itemLargura = $_item[1];
                $itemComprimento = $_item[2];
                //pega o ultimo produto maior
                $altura = $itemAltura > $altura ? $itemAltura : $altura;
                $largura = $itemLargura > $largura ? $itemLargura : $largura;
                $comprimento = $itemComprimento > $comprimento ? $itemComprimento : $comprimento;

                if ($this->correios['check_dimensions'] == true) {
                    if ($itemAltura > $this->correios['altura_max'] || $itemAltura < $this->correios['altura_min'] || $itemLargura > $this->correios['largura_max'] || $itemLargura < $this->correios['largura_min'] || $itemComprimento > $this->correios['comprimento_max'] || $itemComprimento < $this->correios['comprimento_min'] || ($itemAltura + $itemLargura + $itemComprimento) > $this->correios['sum_max']) {
                        return false;
                    }
                }

                if (count($carrinho) > 1 && $this->verificaFreteGratisProduto($cep, $_product)) {
                    //
                } else {
                    $pesoCubicoTotal += (($itemAltura * $itemLargura * $itemComprimento) * $_product['quantidade']) / $this->correios['coeficiente_volume'];
                    $peso += $_product['peso'] * $_product['quantidade'];
                }
            }
            $this->campos['nVlPeso'] = $peso;
            $this->campos['sCepDestino'] = $cep;
            $this->campos['nVlComprimento'] = ceil($comprimento);
            $this->campos['nVlAltura'] = ceil($altura);
            $this->campos['nVlLargura'] = ceil($largura);

            //$pesoCubicoTotal = (($altura*$largura*$comprimento)*1)/$this->correios['coeficiente_volume'];
            //$pesoCubicoTotal = number_format($pesoCubicoTotal, 2, '.', '');

            /*
             * Monta o valor conforme o tipo de frete
             */
            $this->campos['nCdServico'] = $codigo;
            $args = http_build_query($this->campos);

            $retorno = Set::reverse(new Xml(file_get_contents($this->url . $args)));

            return $retorno;
        }

        /**
         * Retorna o valor e o prazo dos serviços dos correios e transportadoras
         * @param String $cep  cep do destino
         * @param Int    $peso peso da compra
         * @return Array
         * */
        public function consultarFretesDisponiveis($cep, $carrinho)
        {
            //se não tem cep setado
            if (preg_replace("/[^0-9]+/", "", $cep) == "")
                return false;
            //se o carrinho está vazio
            if (count($carrinho['itens']) <= 0) {
                return false;
            }
            //retorno
            $consulta = array();
            $frete_gratis_carrinho = false;

            // Inicio Frete Grátis
            if (Configure::read('FreteGratis.status') == '1') {
                if ($this->verificaFreteGratis($cep, $carrinho)) {
                    $peso_carrinho = 0;

                    foreach ($carrinho['itens'] as $chave => $valor) {
                        $peso_carrinho += $valor['peso'] * $valor['quantidade'];
                    }

                    //consulta o prazo do pac, para setar no frete gratis
                    App::import('Component', 'Correios');
                    $this->Correios = new CorreiosComponent();
                    $frete_disponiveis = $this->Correios->Calcular($cep, $carrinho['itens']);
                    $prazo_entrega = 0;
                    if (isset($frete_disponiveis['quote'])) {
                        foreach ($frete_disponiveis['quote'] as $chave => $frete) {
                            if (strstr($frete['nome'], 'PAC')) {
                                $prazo_entrega = $frete['prazo'];
                                break;
                            }
                        }
                    }

                    if ($prazo_entrega == 0) {
                        $prazo_entrega = Configure::read('FreteGratis.prazo_entrega');
                    }

                    $consulta[] = array(
                        'FreteTipo' => array(
                            'id'        => 3,
                            'status'    => 1,
                            'nome'      => 'Grátis',
                            'descricao' => 'Frete Grátis',
                        ),
                        'Frete'     => array(
                            0 => array(
                                'id'            => 'gratis',
                                'status'        => '1',
                                'peso'          => $peso_carrinho,
                                'preco'         => 0.00,
                                'descricao'     => 'Frete Grátis',
                                'prazo'         => $prazo_entrega,
                                'frete_tipo_id' => 3,
                            )
                        )
                    );
                }
                $frete_gratis_carrinho = true;
            }
            // Fim Frete Grátis
            // Inicio Transportadora JAMEF
            if (Configure::read('TransportadoraJamef.status') == '1' && ($frete_gratis_carrinho == true && Configure::read('FreteGratis.sobrescrever_fretes') == '0' || Configure::read('FreteGratis.status') == '0')) {

                App::import('Component', 'Transportadoras');
                $this->Transportadoras = new TransportadorasComponent();
                $frete = $this->Transportadoras->calcularJamef($cep, $carrinho['itens']);

                if (isset($frete['FreteTipo'])) {
                    $consulta[] = $frete;
                }
            }
            // Fim Transportadora JAMEF
            // Inicio correios
            if (Configure::read('Correios.status') == '1' && ($frete_gratis_carrinho == true && Configure::read('FreteGratis.sobrescrever_fretes') == '0' || Configure::read('FreteGratis.status') == '0')) {
                App::import('Component', 'Correios');
                $this->Correios = new CorreiosComponent();
                $frete_disponiveis = $this->Correios->Calcular($cep, $carrinho['itens']);

                $peso_carrinho = 0;
                foreach ($carrinho['itens'] as $chave => $valor) {
                    $peso_carrinho += $valor['peso'] * $valor['quantidade'];
                }

                if (isset($frete_disponiveis['quote'])) {
                    foreach ($frete_disponiveis['quote'] as $chave => $frete) {
                        $consulta[] = array(
                            'FreteTipo' => array(
                                'id'        => 1,
                                'status'    => 1,
                                'nome'      => $frete['nome'],
                                'descricao' => 'Correios',
                            ),
                            'Frete'     => array(
                                0 => array(
                                    'id'            => $chave,
                                    'status'        => 1,
                                    'peso'          => $peso_carrinho,
                                    'preco'         => $frete['preco'],
                                    'descricao'     => 'Correios',
                                    'prazo'         => $frete['prazo'],
                                    'frete_tipo_id' => 1,
                                )
                            )
                        );
                    }
                }
            }
            // Fim correios
            // Inicio Retirar na Loja
            if (Configure::read('RetirarNaLoja.status') == '1') {

                $peso_carrinho = 0;

                foreach ($carrinho['itens'] as $chave => $valor) {
                    $peso_carrinho += $valor['peso'] * $valor['quantidade'];
                }

                $consulta[] = array(
                    'FreteTipo' => array(
                        'id'        => 4,
                        'status'    => 1,
                        'nome'      => 'Retirar na Loja',
                        'descricao' => 'Retirar na Loja',
                    ),
                    'Frete'     => array(
                        0 => array(
                            'id'            => 'retirar_na_loja',
                            'status'        => '1',
                            'peso'          => $peso_carrinho,
                            'preco'         => 0.00,
                            'descricao'     => 'Retirar na Loja',
                            'prazo'         => 1,
                            'frete_tipo_id' => 4,
                        )
                    )
                );
            }
            // Fim Retirar na Loja

            // Inicio TeleEntrega
            if (Configure::read('TeleEntrega.status') == '1') {

                App::import('Component', 'TeleEntrega');
                $this->TeleEntrega = new TeleEntregaComponent();
                $frete = $this->TeleEntrega->calcular($cep, $carrinho['itens']);

                if (isset($frete['FreteTipo'])) {
                    $consulta[] = $frete;
                }
            }
            // Fim Retirar na Loja

            //zera o tempo de producao adicional dos produtos
            $tempo_producao = 0;
            foreach ($carrinho['itens'] as $item) {
                //se um produto tiver um tempo de producao coloca o maior valor na variavel
                if (isset($item['tempo_producao']) && $item['tempo_producao'] > 0 && $item['tempo_producao'] > $tempo_producao) {
                    $tempo_producao = $item['tempo_producao'];
                }
            }
            // dias adicionais
            $tempo_add = Configure::read('Loja.frete_dias_adicionais');

            if (($tempo_producao > 0 || $tempo_add > 0)) {
                foreach ($consulta as &$frete1) {
                    $frete1['Frete'][0]['prazo'] += $tempo_producao;

                    if (!isset($frete1['Frete'][0]['dias_adicionais'])) {
                        $frete1['Frete'][0]['dias_adicionais'] = true;
                    }
                    if (!in_array($frete1['FreteTipo']['descricao'], array(
                        'Correios',
                        'Frete Grátis',
                        'Tele-entrega'
                    ))
                    ) {
                        if ($tempo_add > 0 && (isset($frete1['Frete'][0]['dias_adicionais']) && $frete1['Frete'][0]['dias_adicionais'] == true)) {
                            $frete1['Frete'][0]['prazo'] += $tempo_add;
                        }
                    }
                }
            }

            $fretes = array();
            foreach ($consulta as $c => $v) {
                $fretes[Inflector::slug(strtolower($v['FreteTipo']['descricao']), '_')][] = $v;
            }

            return array_reverse($fretes);
        }

        function calculaTransportadora($freteTipo, $pedido_valor_total, $peso)
        {
            App::import("model", "Frete");
            $this->Frete = new Frete();
            $valorPedido = $pedido_valor_total;

            $valorPeso = $peso;
            $pesoRamos = $peso;

            $linha = isset($freteTipo['Frete'][0]) ? $freteTipo['Frete'][0] : $freteTipo['Frete'];
            $rate['preco'] = isset($freteTipo['Frete'][0]['preco']) ? $freteTipo['Frete'][0]['preco'] : $freteTipo['Frete']['preco'];

            $valor = $linha["preco"];
            $gris = $linha["gris"];
            $tas = $linha["tas"];
            $ad_valorem = $linha["ad_valorem"];
            $valor_adicional = $linha["valor_adicional"];
            $trt_minimo = $linha["trt_minimo"];
            $trt = $linha["trt"];

            if (in_array($freteTipo['FreteTipo']['id'], array(4))) {

                //se for Jamef
                $valorParcial = $valor;

                //ADD GRIS
                $valorParcial += round(($gris / 100) * $valorPedido, 2);
                //ADD TAS
                $valorParcial += $tas;

                //AdValorem
                $valorAdValorem = $valorPedido * ($ad_valorem / 100);
                $valorParcial += $valorAdValorem;

                //peso
                if ($peso > 100) {
                    $peso_extra = $peso - 100;
                    $valor_adicional = $peso_extra * $valor_adicional;
                    $valorParcial += $valor_adicional;

                    //Pedagio
                    //$valorParcial+= $pedagio;
                }

                //begin TRT

                //begin primeira importacao
                $osasco_inicio = "06000001";
                $osasco_fim = "06299999";

                $diadema_inicio = "09900001";
                $diadema_fim = "09999999";

                $rj_inicio = "20000001";
                $rj_fim = "23799999";

                $sp1_inicio = "01000001";
                $sp1_fim = "05999999";

                $sp2_inicio = "08000000";
                $sp2_fim = "08499999";
                //end primeira importacao

                //begin segunda importacao
                $juiz_de_fora_inicio = "36000001";
                $juiz_de_fora_fim = "36099999";

                $montes_claros_inicio = "39400001";
                $montes_claros_fim = "39409999";

                $varginha_inicio = "37000001";
                $varginha_fim = "37109999";

                $bom_despacho = "35600000";
                $diamantina = "39100000";
                $pouso_alegre_1 = "35573000";
                $pouso_alegre_2 = "37550000";
                //end segunda importacao

                if ($freteTipo['Frete'][0]['cep_inicial'] >= $osasco_inicio && $freteTipo['Frete'][0]['cep_final'] <= $osasco_fim || $freteTipo['Frete'][0]['cep_inicial'] >= $diadema_inicio && $freteTipo['Frete'][0]['cep_final'] <= $diadema_fim || $freteTipo['Frete'][0]['cep_inicial'] >= $rj_inicio && $freteTipo['Frete'][0]['cep_final'] <= $rj_fim || $freteTipo['Frete'][0]['cep_inicial'] >= $sp1_inicio && $freteTipo['Frete'][0]['cep_final'] <= $sp1_fim || $freteTipo['Frete'][0]['cep_inicial'] >= $sp2_inicio && $freteTipo['Frete'][0]['cep_final'] <= $sp2_fim

                    || $freteTipo['Frete'][0]['cep_inicial'] >= $juiz_de_fora_inicio && $freteTipo['Frete'][0]['cep_final'] <= $juiz_de_fora_fim || $freteTipo['Frete'][0]['cep_inicial'] >= $montes_claros_inicio && $freteTipo['Frete'][0]['cep_final'] <= $montes_claros_fim || $freteTipo['Frete'][0]['cep_inicial'] >= $varginha_inicio && $freteTipo['Frete'][0]['cep_final'] <= $varginha_fim

                    || $freteTipo['Frete'][0]['cep_inicial'] == $bom_despacho || $freteTipo['Frete'][0]['cep_inicial'] == $diamantina || $freteTipo['Frete'][0]['cep_inicial'] == $pouso_alegre_1 || $freteTipo['Frete'][0]['cep_inicial'] == $pouso_alegre_2
                ) {
                    //TRT
                    $valorTrt = round(($trt / 100) * $valorParcial, 2);
                    if ($valorTrt < $trt_minimo) {
                        $valorTrt = $trt_minimo;
                    }

                    $valorParcial += $valorTrt;
                }
                //end TRT

                $Price = $valorParcial;
            }

            if (isset($freteTipo['Frete'][0])) {
                $freteTipo['Frete'][0]['preco'] = $Price;
            } else {
                $freteTipo['Frete']['preco'] = $Price;
            }

            return $freteTipo;
        }

        /**
         * verifica se o possui regra de frete grátis por produto,categoria ou carrinho.
         */
        function verificaFreteGratis($cep, $carrinho)
        {

            App::import("model", "FreteGratis");
            $this->FreteGratis = new FreteGratis();

            App::import("model", "ProdutoCategoria");
            $this->ProdutoCategoria = new ProdutoCategoria();

            $pedidoTotal = 0;

            foreach ($carrinho['itens'] as $k => $i) {
                $pedidoTotal += $i['quantidade'] * ($i['preco_promocao'] > 0 ? $i['preco_promocao'] : $i['preco']);
            }
            //verifica se tem frete gratis por carrinho
            if ($this->FreteGratis->find('first', array(
                'conditions' => array(
                    'FreteGratis.data_inicio <= now()',
                    'FreteGratis.data_fim >= now()',
                    'FreteGratis.cep_inicial <=' => $cep,
                    'FreteGratis.cep_final >='   => $cep,
                    'FreteGratis.codigo'         => 'CARRINHO',
                    'FreteGratis.status'         => true,
                    'FreteGratis.apartir_de <='  => $pedidoTotal
                )
            ))
            ) {
                return true;
            }

            //verifica se tem frete grátis por produto, se encontrar um produto que tenha a regra da o frete grátis no carrinho;
            foreach ($carrinho['itens'] as $k => $i) {
                if ($this->FreteGratis->find('first', array(
                    'conditions' => array(
                        'FreteGratis.data_inicio <= now()',
                        'FreteGratis.data_fim >= now()',
                        'FreteGratis.cep_inicial <=' => $cep,
                        'FreteGratis.cep_final >='   => $cep,
                        'FreteGratis.codigo'         => 'PRODUTO',
                        'FreteGratis.status'         => true,
                        'FreteGratis.produto_id LIKE concat("%\"",' . $i['id'] . ',"\"%") '
                    )
                ))
                ) {
                    return true;
                }
            }

            //verifica se tem frete grátis por grade, se encontrar uma grade que tenha a regra da o frete grátis no carrinho;
            foreach ($carrinho['itens'] as $k => $i) {
                if ($this->FreteGratis->find('first', array(
                    'conditions' => array(
                        'FreteGratis.data_inicio <= now()',
                        'FreteGratis.data_fim >= now()',
                        'FreteGratis.cep_inicial <=' => $cep,
                        'FreteGratis.cep_final >='   => $cep,
                        'FreteGratis.codigo'         => 'GRADE',
                        'FreteGratis.status'         => true,
                        'FreteGratis.grade_id LIKE concat("%\"",' . $i['grade_id'] . ',"\"%") '
                    )
                ))
                ) {
                    return true;
                }
            }

            //verifica se tem frete grátis por categoria, se encontrar um produto que tenha a categoria dá frete grátis no carrinho;
            $categorias = array();
            foreach ($carrinho['itens'] as $k => $i) {
                $cats = $this->ProdutoCategoria->find('all', array(
                    'recursive'  => -1,
                    'conditions' => array('ProdutoCategoria.produto_id' => $i['id'])
                ));
                foreach ($cats as $cat) {
                    $categorias[$cat['ProdutoCategoria']['categoria_id']] = ($i['quantidade'] * ($i['preco_promocao'] > 0 ? $i['preco_promocao'] : $i['preco']));
                }
            }

            foreach ($categorias as $k => $i) {
                if ($this->FreteGratis->find('first', array(
                    'conditions' => array(
                        'FreteGratis.data_inicio <= now()',
                        'FreteGratis.data_fim >= now()',
                        'FreteGratis.cep_inicial <=' => $cep,
                        'FreteGratis.cep_final >='   => $cep,
                        'FreteGratis.codigo'         => 'CATEGORIA',
                        'FreteGratis.status'         => true,
                        'FreteGratis.categoria_id LIKE concat("%\"",' . $k . ',"\"%") ',
                        'FreteGratis.apartir_de <='  => $i
                    )
                ))
                ) {
                    return true;
                }
            }
            return false;
        }

        function consultaEndereco($cep = false)
        {
            $return = false;
            $cep = preg_replace("/[^0-9]+/", "", $cep);
            if ($cep) {
                $url = "http://www.buscacep.correios.com.br/sistemas/buscacep/resultadoBuscaCepEndereco.cfm?relaxation";
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('relaxation' => $cep)));

                // faz a requisicao e retorna o conteudo do endereco
                $saida = curl_exec($ch);

                curl_close($ch);// encerra e retorna os dados
                preg_match_all("/\<[td].*\>(.*)\<\/[td>]+>/", $saida, $regs);
                if (isset($regs[1][0])) {
                    if (isset($regs[1][3])) {
                        $return['rua'] = utf8_encode($regs[1][0]);
                        $return['bairro'] = utf8_encode($regs[1][1]);
                        $return['cidade'] = utf8_encode($regs[1][2]);
                        $return['nome_cidade'] = utf8_encode($regs[1][2]);
                        $return['estado'] = utf8_encode(!isset($regs[1][3]) ? $regs[1][1] : $regs[1][3]);
                    } else {
                        $return['rua'] = '';
                        $return['bairro'] = '';
                        $return['cidade'] = utf8_encode($regs[1][0]);
                        $return['nome_cidade'] = utf8_encode($regs[1][2]);
                        $return['estado'] = utf8_encode(!isset($regs[1][3]) ? $regs[1][1] : $regs[1][3]);
                    }
                }
                if (isset($return)) {
                    foreach ($return as $key => $value) {
                        $return[$key] = preg_replace("/&#?[a-z0-9]+;/i", '', $value);
                    }
                }

            }
            return $return;
        }

        function verificaFreteGratisProduto($cep, $produto)
        {

            App::import("model", "FreteGratis");
            $this->FreteGratis = new FreteGratis();

            App::import("model", "ProdutoCategoria");
            $this->ProdutoCategoria = new ProdutoCategoria();

            $gratis_produto = false;
            //verifica se tem frete grátis por produto, se encontrar um produto que tenha a regra da o frete grátis no carrinho;

            if ($this->FreteGratis->find('first', array(
                'conditions' => array(
                    'FreteGratis.data_inicio <= now()',
                    'FreteGratis.data_fim >= now()',
                    'FreteGratis.cep_inicial <=' => $cep,
                    'FreteGratis.cep_final >='   => $cep,
                    'FreteGratis.codigo'         => 'PRODUTO',
                    'FreteGratis.status'         => true,
                    'FreteGratis.produto_id LIKE concat("%\"",' . $produto['id'] . ',"\"%") '
                )
            ))
            ) {
                return true;
            }

            $categorias = array();

            $cats = $this->ProdutoCategoria->find('all', array(
                'recursive'  => -1,
                'conditions' => array('ProdutoCategoria.produto_id' => $produto['id'])
            ));
            foreach ($cats as $cat) {
                $categorias[$cat['ProdutoCategoria']['categoria_id']] = ($produto['quantidade'] * ($produto['preco_promocao'] > 0 ? $produto['preco_promocao'] : $produto['preco']));
            }

            foreach ($categorias as $k => $i) {
                if ($this->FreteGratis->find('first', array(
                    'conditions' => array(
                        'FreteGratis.data_inicio <= now()',
                        'FreteGratis.data_fim >= now()',
                        'FreteGratis.cep_inicial <=' => $cep,
                        'FreteGratis.cep_final >='   => $cep,
                        'FreteGratis.codigo'         => 'CATEGORIA',
                        'FreteGratis.status'         => true,
                        'FreteGratis.categoria_id LIKE concat("%\"",' . $k . ',"\"%") ',
                        'FreteGratis.apartir_de <='  => $i
                    )
                ))
                ) {
                    return true;
                }
            }

            return false;
        }

        /**
         * realiza o post para a jadlog
         */
        public function consultarJadLog($freteTipo, $carrinho, $cep, $peso, $altura_max, $largura_max, $profundidade, $valor_declarado = 0)
        {

            // inserção de regra para que o somente o maior dos itens seja cálculado (FI-62)
            $items = $carrinho['itens'];
            if (!empty($items)) {
                $altura_max = 0;
                $largura_max = 0;
                $profundidade_max = 0;
                $volume = 0;
                foreach ($items as $item) {
                    $vol = $item['largura'] * $item['profundidade'] * $item['altura'];
                    if ($vol > $volume) {
                        $volume = $vol;
                        $altura_max = $item['altura'];
                        $largura_max = $item['largura'];
                        $profundidade_max = $item['altura'];
                    }
                }
            }

            App::import("Component", "Session");
            $this->Session = new SessionComponent();

            $params = array(
                'form_precifica'                        => 'form_precifica',
                'form_precifica:j_idt81'                => 'form_precifica:j_idt81',
                'form_precifica:selectModalidade_input' => 0,
                'javax.faces.ViewState'                 => '2231265782295594789%3A-3542411601807011746',
                'javax.faces.partial.ajax'              => true,
                'javax.faces.partial.execute'           => "@all",
                'javax.faces.partial.render'            => 'form_precifica:panel_resultado form_informacao:mensagemTmp',
                'javax.faces.source'                    => 'form_precifica:j_idt81',
                'form_precifica:selectEntrega_input'    => 'D',
                'form_precifica:selectFrete'            => 'N',
                'form_precifica:selectModalidade_input' => 5,
                'form_precifica:cepOrigem'              => "90220-160",
                'form_precifica:cepDestino'             => "91160-601",
                'form_precifica:peso'                   => ceil($peso),
                'form_precifica:valLargura'             => ceil($largura_max),
                'form_precifica:valAltura'              => ceil($altura_max),
                'form_precifica:valProfundidade'        => ceil($profundidade),
                'form_precifica:valMercadoria'          => ceil($valor_declarado),
                'form_precifica:valColeta'              => 0
            );

            $ch = curl_init('http://jadlog.com/sitejadlog/pages/simulador/dados.jad');

            $head = array(
                'Accept'           => 'application/xml, text/xml, */*; q=0.01',
                'Accept-Encoding'  => 'gzip, deflate',
                'Accept-Language'  => 'pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
                'Cache-Control'    => 'no-cache',
                'Connection'       => 'keep-alive',
                'Content-Length'   => '692',
                'Content-Type'     => 'application/x-www-form-urlencoded; charset=UTF-8',
                'Cookie'           => 'JSESSIONID=5d52ac3a69c3ea8261223a50e9a5.ajp_no_77; __utma=177721662.114533204.1380650430.1380656685.1380658701.3; __utmc=177721662; __utmz=177721662.1380650430.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmb=177721662.5.10.1380658701',
                'Faces-Requesth'   => 'partial/ajax',
                'Host'             => 'jadlog.com',
                'Pragma'           => 'no-cache',
                'User-Agent'       => 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0',
                'X-Requested-With' => 'XMLHttpRequest'
            );

            curl_setopt($ch, CURLOPT_URL, 'http://jadlog.com/sitejadlog/pages/simulador/dados.jad?' . http_build_query($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl_object, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
            $a = curl_exec($ch);
            curl_close($ch);

            die($a);
        }

        /**
         * Verifica se o cep passado está cai na regra de frete grátis.
         * se o valor da compra atingir o apartir_de e se o cep entrar o range
         * @return Boolean
         * * */
        function margemFrete($cep)
        {

            $cep = (int)preg_replace("/[^0-9]+/", "", $cep);
            $cep_inicial = 90000001;
            $cep_final = 91999999;
            if ($cep_inicial <= $cep && $cep_final >= $cep) {
                return true;
            } else {
                return false;
            }
        }
    }
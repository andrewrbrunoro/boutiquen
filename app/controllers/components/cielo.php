<?php
/**
 * Cielo CakePHP Component
 * Copyright (c) 2011 Luan Garcia
 * www.implementado.com
 *
 * @author      Luan Garcia <luan.garcia@gmail.com>
 * @version     1.0
 * @license     MIT
 *
 */

class CieloComponent extends Object {

    public $teste       = FALSE;    //por padrao é false.
    public $shopid      = '1050242987';     //Número de afiliação da loja com a Cielo..
    public $chave       = 'db6cb563a82caa31e42a84845317e14e174a78c7bc8dfe8a42fff97b9b0f3aba';     //Chave de acesso da loja atribuída pela Cielo.
    public $url_teste   = 'https://qasecommerce.cielo.com.br/servicos/ecommwsec.do';
    public $url         = 'https://ecommerce.cielo.com.br/servicos/ecommwsec.do';


    /**
     * Metodo realiza a criacao do pedido na visa
     * @return mixed
     */
    public function criarPedido($pedido) {
		
        App::import("helper", "String");
        $this->String = new StringHelper();

        $tid = $pedido['Pedido']['tid'];
        //se já existe uma transação com o tid cancela a nova transação
        //if($this->consultarTid($tid))return false;

        $produto = $pedido['Pedido']['parcelas'] > 1 ? 2 : 1;
        $bandeira = low($pedido['PagamentoCondicao']['sigla']);
        $parcelas = $pedido['Pedido']['parcelas'];
        $pedido_id = $pedido['Pedido']['id'];
        $data = date("Y-m-d\TH:i:s");
        //valor compra
        $valor_compra = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
        $valor = preg_replace('/[^0-9]/', '',$valor_compra);
        //cartao

        $cartao_numero = $pedido['Pedido']['numero_cartao'];
        list($mes, $ano) = explode('/', $pedido['Pedido']['validade_cartao']);
        $cartao_validade = $ano . $mes;
        $cartao_seguranca = $pedido['Pedido']['codigo_seguranca_cartao'];
        $cartao_indicador = '1';

        $post = '<?xml version="1.0" encoding="ISO-8859-1"?>
                <requisicao-autorizacao-portador id="7" versao="1.1.1">
                  <tid>'.$tid.'</tid>
                  <dados-ec>
                        <numero>' .$this->shopid. '</numero>
                        <chave>' .$this->chave. '</chave>
                  </dados-ec>
                  <dados-cartao>
                        <numero>'.$cartao_numero.'</numero>
                        <validade>'.$cartao_validade.'</validade>
                        <indicador>'.$cartao_indicador.'</indicador>
                        <codigo-seguranca>'.$cartao_seguranca.'</codigo-seguranca>
                  </dados-cartao>
                  <dados-pedido>
                        <numero>'.$pedido_id.'</numero>
                        <valor>'.$valor.'</valor>
                        <moeda>986</moeda>
                        <data-hora>'.$data.'</data-hora>
                        <idioma>PT</idioma>
                  </dados-pedido>
                  <forma-pagamento>
                        <bandeira>'.$bandeira.'</bandeira>
                        <produto>'.$produto.'</produto>
                        <parcelas>'.$parcelas.'</parcelas>
                  </forma-pagamento>
                  <capturar-automaticamente>false</capturar-automaticamente>
                </requisicao-autorizacao-portador>';
        App::import("Xml");
        $retorno = Set::reverse(new Xml($this->file_post_contents($post)));
		
        if(isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $post =  var_export($post, true);
            $this->log('ERRO - AO CRIAR PEDIDO TID\r\n'.$log.'\r\n', LOG_DEBUG);
            $this->log('ERRO - AO CRIAR PEDIDO TID\r\n'.$post.'\r\n', LOG_DEBUG);
		
        }else {
            $log =  var_export($retorno, true);
            $this->log('SUCESSO - AO CRIAR PEDIDO TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }

        return $retorno;
    }

     /**
     * Metodo realiza a consulta do pedido na visa
     * @return mixed
     */
    public function criarTid($pedido) {
        $produto = $pedido['Pedido']['parcelas'] > 1 ? 2 : 1;
        $bandeira = low($pedido['PagamentoCondicao']['sigla']);
        $parcelas = $pedido['Pedido']['parcelas'];

        $post = '<?xml version="1.0" encoding="UTF-8"?>
                    <requisicao-tid id="' . md5(date("YmdHisu")) . '" versao="1.1.0">
                        <dados-ec>
                            <numero>' .$this->shopid. '</numero>
                            <chave>' .$this->chave. '</chave>
                        </dados-ec>
                        <forma-pagamento>
                            <bandeira>' .$bandeira. '</bandeira>
                            <produto>' .$produto. '</produto>
                            <parcelas>' .$parcelas. '</parcelas>
                        </forma-pagamento>
                    </requisicao-tid>';
        App::import("Xml");
		
        $retorno = Set::reverse(new Xml($this->file_post_contents($post)));
		
        if(!isset($retorno['Retorno-tid']['tid'])) {
            $log =  var_export($retorno, true);
            $this->log('ERRO - AO CONSULTAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }elseif(isset($retorno['Retorno-tid']['tid'])) {
            $log =  var_export($retorno, true);
            $this->log('SUCESSO - AO CONSULTAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }
        if(!isset($retorno['Retorno-tid']['tid'])){
            return false;
        }
        return $retorno['Retorno-tid']['tid'];
    }

	
    /**
     * Metodo realiza a consulta do pedido na visa
     * @return mixed
     */
    public function consultarTid($tid) {
        $post = '<?xml version="1.0" encoding="UTF-8"?>
                    <requisicao-consulta id="5" versao="1.1.1">
                        <tid>'.$tid.'</tid>
                        <dados-ec>
                            <numero>' .$this->shopid. '</numero>
                            <chave>' .$this->chave. '</chave>
                        </dados-ec>
                    </requisicao-consulta>';
        App::import("Xml");
        $retorno = Set::reverse(new Xml($this->file_post_contents($post)));

        if(isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $this->log('ERRO - AO CONSULTAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }elseif(!isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $this->log('SUCESSO - AO CONSULTAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }
        if(isset($retorno['Erro'])){
            return false;
        }else{
            return true;
        }
    }
	/**
     * Capturar
     * @return mixed
     */
    public function capturar($pedido) {
        $post = '<?xml version="1.0" encoding="ISO-8859-1"?> 
					<requisicao-captura id="0374f305-0e23-4aad-82a2-055788c8cf4d" versao="1.2.0"> 
					  <tid>'.$pedido['Pedido']['tid'].'</tid> 
					  <dados-ec> 
						<numero>' .$this->shopid. '</numero> 
						<chave>' .$this->chave. ' 
						</chave> 
					  </dados-ec> 
					</requisicao-captura>';
        App::import("Xml");
        $retorno = Set::reverse(new Xml($this->file_post_contents($post)));

        if(isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $this->log('ERRO - AO CAPTURAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }elseif(!isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $this->log('SUCESSO - AO CAPTURAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }
        if(isset($retorno['Erro'])){
            return false;
        }else{
            return true;
        }
    }
	
	/**
     * Cancelar
     * @return mixed
     */
    public function cancelar($pedido) {
        $post = '<?xml version="1.0" encoding="ISO-8859-1"?> 
					<requisicao-cancelamento id="13368079-dedc-4cdf-9140-84473faf83d4" versao="1.2.0"> 
					  <tid>'.$pedido['Pedido']['tid'].'</tid> 
					  <dados-ec> 
						<numero>' .$this->shopid. '</numero> 
						<chave>' .$this->chave. ' 
						</chave> 
					  </dados-ec> 
					</requisicao-cancelamento>';
        App::import("Xml");
        $retorno = Set::reverse(new Xml($this->file_post_contents($post)));

        if(isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $this->log('ERRO - AO CAPTURAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }elseif(!isset($retorno['Erro'])) {
            $log =  var_export($retorno, true);
            $this->log('SUCESSO - AO CAPTURAR TID\r\n'.$log.'\r\n', LOG_DEBUG);
        }
        if(isset($retorno['Erro'])){
            return false;
        }else{
            return true;
        }
    }
   

    /**
     * Metodo realiza o post do xml local para a visa
     * @param String Xml com os dados do pedido
     * @return mixed
     */
     function file_post_contents($msg) {
        
        $url = 'https://ecommerce.cielo.com.br/servicos/ecommwsec.do';
  
            //setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
        // Following line is compulsary to add as it is:
            curl_setopt($ch, CURLOPT_POSTFIELDS,"mensagem=" . $msg);
        curl_setopt($ch, CURLOPT_SSLVERSION,4);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            $data = curl_exec($ch);
        curl_close($ch);
        return utf8_encode($data);
    }
}
?>

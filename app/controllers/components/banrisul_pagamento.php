<?php

class BanrisulPagamentoComponent extends Object {
	//teste 'Url' => 'https://ww4.banrisul.com.br/banricompras/link/asp/ambiente.asp?',
	//producao 'Url' => 'https://ww2.banrisul.com.br/banricompras/link/asp/ambiente.asp?',
    var $__init = array(
        'Url' => 'https://ww2.banrisul.com.br/banricompras/link/asp/ambiente.asp?',
        'PGTX' => array(
            'IdentPedido' => NULL,//ex: 1010abc187
            'CodRede' => '00410138000',//ex: 123456
            'CodEstab' => '000000000000576',//ex: 123456
            'VlrTotal' => NULL,//ex: Para R$22,50 (vinte  dois reais e cinqüenta centavos): 000000000002250 
            'FormaPagto' => NULL,//ex: PGTX
            'NumParcelas' => NULL,//ex: Para 6 (seis) parcelas: 06 
            'Opcao' => NULL,//ex: Data:D Prazo: P
            'Parcela' => NULL,//ex: Para pagamento em duas parcelas: 0102 
            'DataSugerida' => NULL,//ex: Somente se campo Opcao = D, DataSugerida = AAAAMMDD;
            'Prazo' => NULL,//ex: Somente se campo Opcao = P, Prazo = 015 (ex.: 15 dias) 
            'VlrParcela' => NULL,//ex: Para duas parcelas de R$22,50 (vinte dois reais e cinqüenta centavos), informar: 000000000002250 
          
        ),
		'BOLETO' => array(
            'IdentPedido' => NULL,
            'CodRede' => '00410138000',
            'CodEstab' => '000000000000576',
            'Vcto' => NULL,//A vista: 00000000 Data do Vencimento: 20012003 (DDMMAAAA) 
            'VlrTotal' => NULL,//Para R$22,50 (vinte dois reais e cinqüentacentavos): 0000002250 
            'FormaPagto' => 'PGBC',//PGBC
            'Sacado' => NULL,
            'End1' => NULL,//Rua,  dos Andradas,  1500. AP.15-A
            'Uf' => NULL,
            'Cidade' => NULL,
            'Cep' => NULL,//90018-900 
          
        ),
		'PGTA' => array(
            'IdentPedido' => NULL,
            'CodRede' => '00410138000',
            'CodEstab' => '000000000000576',
            'VlrTotal' => NULL,//Para R$22,50 (vinte dois reais e cinqüentacentavos): 0000002250 
            'FormaPagto' => 'PGTA',//PGTA
          )
    );

	// function run on init
	function startup(&$controller){
		$this->controller = $controller;
	}
    public function pagar($pedido) {

        switch ($pedido['PagamentoCondicao']['sigla']) {
            case 'BOLETO':
                return $this->pagamentoBoleto($pedido);
                break;
			case 'PGTA':
                return $this->pagamentoAvista($pedido);
                break;
			case 'PGTX':
                return $this->pagamentoParcelado($pedido);
                break;
        }
    }
	/**
     * seta os dados para compra parcelado PGTX
     */
    
	public function pagamentoParcelado($pedido) {
		App::import("helper", "String");
		$this->String = new StringHelper();
        $valor_total = preg_replace("/[^0-9]+/","",$this->String->bcoToMoeda(($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']))));
        $this->__init['PGTX']['IdentPedido']   = $pedido['Pedido']['id'];
        $this->__init['PGTX']['FormaPagto']    = "PGTX";
        $this->__init['PGTX']['Opcao']         = "P";
        $this->__init['PGTX']['VlrTotal']      = str_pad($valor_total, 15, "0", STR_PAD_LEFT);
        $this->__init['PGTX']['NumParcelas']   = str_pad($pedido['Pedido']['parcelas'],2,"0",STR_PAD_LEFT);
        /**
         * Divide o valor total pelo numero de parcelas
         */
        $rest = round($valor_total/$pedido['Pedido']['parcelas']);
        /**
         * pega a diferenca entre parcelas impares e soma na primeira parcela
         */
        $rest = $valor_total-($rest*$pedido['Pedido']['parcelas']);
        /**/
        for($i=1;$i<=$pedido['Pedido']['parcelas'];$i++) {
            $valor_parcela = str_pad(round($valor_total/$pedido['Pedido']['parcelas'])+($i==1?$rest:0), 15, "0", STR_PAD_LEFT);
            $this->__init['PGTX']['Parcela']        .= str_pad($i, 2, "0", STR_PAD_LEFT);
            $this->__init['PGTX']['Prazo']          .= str_pad($i*30, 3, "0", STR_PAD_LEFT);
            $this->__init['PGTX']['VlrParcela']     .= $valor_parcela;
            $this->__init['PGTX']['DataSugerida']   .= date('Ymd', strtotime("+{$i} months"));
        }
		$url = $this->__init['Url'].http_build_query($this->__init['PGTX']);             
		$this->log('BANRICOMPRA PGTX\r\n' . $url . '\r\n', LOG_DEBUG);        
		return $url;       
    }
    public function pagamentoBoleto($pedido) {

        App::import("helper", "String");
        $this->String = new StringHelper();
    
        $valor_compra = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
        $valor = str_pad(preg_replace('/[^0-9]/', '',$valor_compra), 10, "0", STR_PAD_LEFT);
     	$this->__init['BOLETO']['IdentPedido'] = $pedido['Pedido']['id'];
		$this->__init['BOLETO']['Vcto'] = date('dmY', mktime(0, 0, 0, date('m'), date('d') + 5, date('Y')));
		$this->__init['BOLETO']['VlrTotal'] = $valor;		
		$this->__init['BOLETO']['Sacado'] = urlencode($pedido['Usuario']['nome']);
		$this->__init['BOLETO']['End1'] = urlencode($pedido['Pedido']['endereco_rua']. ','.$pedido['Pedido']['endereco_numero'].($pedido['Pedido']['endereco_complemento']?' - '.$pedido['Pedido']['endereco_complemento']:''));
		$this->__init['BOLETO']['Uf'] = $pedido['Pedido']['endereco_estado'];
		$this->__init['BOLETO']['Cidade'] = urlencode($pedido['Pedido']['endereco_cidade']);
		$this->__init['BOLETO']['Cep'] = preg_replace("/([0-9]{2})\.([0-9]{3})-([0-9]{3})/", "$1$2-$3", $pedido['Pedido']['endereco_cep']);	
	
        $url = $this->__init['Url'].http_build_query($this->__init['BOLETO']);             
		$this->log('BANRICOMPRA BOLETO\r\n' . $url . '\r\n', LOG_DEBUG);   
		
        return $url;
    }
	public function pagamentoAvista($pedido) {

        App::import("helper", "String");
        $this->String = new StringHelper();    
        $valor_compra = $this->String->bcoToMoeda($this->String->moedaToBco($pedido['Pedido']['valor_pedido']) + $this->String->moedaToBco($pedido['Pedido']['valor_frete']));
        $valor = str_pad(preg_replace('/[^0-9]/', '',$valor_compra), 15, "0", STR_PAD_LEFT);
     	$this->__init['PGTA']['IdentPedido'] = $pedido['Pedido']['id'];		
		$this->__init['PGTA']['VlrTotal'] = $valor;
        $url = $this->__init['Url'].http_build_query($this->__init['PGTA']);             
		$this->log('BANRICOMPRA PGTA\r\n' . $url . '\r\n', LOG_DEBUG);   
		
        return $url;
    }
}
<?php

class TeleEntregaComponent extends Object {

    function calcular($cep, $carrinho) {
		
		$peso = 0;
        
		//total carrinho
        foreach ($carrinho as $chave => $valor) {
            $peso = $valor['peso'] * $valor['quantidade'];
        }

        App::import("model", "Frete");
        $this->Frete = new Frete();

        $dados = $this->Frete->find('first', array(
            'limit' => 1,
            'order' => "IF (peso - $peso < 0,99999,peso - $peso) ASC, $peso - peso ASC ",
            'conditions' => array(
                'AND' => array('Frete.nome' => 'TELEENTREGA'),
                'Frete.cep_inicial <=' => $cep, 'Frete.cep_final >=' => $cep
            )
                )
        );

       
        //Fim TRT
      	if($dados){
	        $retorno = array(
	            'FreteTipo' => array(
	                'id' => 5,
	                'status' => 1,
	                'nome' => 'Motoboy',
	                'descricao' => 'Tele-entrega',
	            ),
	            'Frete' => array(
	                0 => array(
	                    'id' => 'Tele-entrega',
	                    'status' => '1',
	                    'peso' => $peso,
	                    'preco' => $dados['Frete']['preco'],
	                    'descricao' => 'Tele-entrega',
	                    'prazo' => $dados['Frete']['prazo'],
	                    'frete_tipo_id' => 5,
	                )
	            )
	        );

	        return $retorno;
		}else{
			return false;
		}
    }

}
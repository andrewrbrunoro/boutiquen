<?php

    class OrderTxtComponent extends Object
    {

        private $order = array();
        private $itens = array();
        private $handler = array();
        public $string = '';
        public $filename = '';

        public function __construct($id)
        {
            App::import('Model', 'Pedido');
            $this->Pedido = new Pedido();

            App::import('Helper', 'String');
            $this->String = new StringHelper();

            $this->order = $this->Pedido->find('first', array(
                'conditions' => array(
                    'Pedido.id' => $id
                )
            ));
            $this->itens = $this->order['PedidoItem'];
            $this->order = $this->order['Pedido'];
            $this->filename = 'pedido-' . $id . '.txt';
            $this->handler = fopen(WWW_ROOT . 'integracao/pedido/' . $this->filename, 'a');
            $this->start();
            $this->end();
        }

        private function start()
        {
            $this->informacoes_usuario();
            $this->nova_linha();
            $this->cabecalho_pre_venda();
            $this->nova_linha();
            $this->itens_pre_venda();
        }

        private function itens_pre_venda()
        {
            $i = 3;
            foreach ($this->itens as $item) {
                $this->id('04');
                $this->sequencial('000' . $i);
                $this->string .= str_pad($this->order['id'], 6, "0", STR_PAD_LEFT);
                $this->string .= str_pad($item['codigo'], 20, " ", STR_PAD_RIGHT);
                $this->string .= str_pad($item['quantidade'].'000', 8, "0", STR_PAD_LEFT);
                $this->string .= str_pad($this->arrumaValor($item['preco']),12, "0", STR_PAD_LEFT);


                if ($item['preco_promocao'] > 0) {
                    $this->string .= str_pad($this->arrumaValor($item['preco'] - $item['preco_promocao']),12, "0", STR_PAD_LEFT);
                    $this->string .= str_pad($this->arrumaValor($item['preco_promocao'] * $item['quantidade']),12, "0", STR_PAD_LEFT);
                } else {
                    $this->string .= str_pad($this->arrumaValor($item['preco'] * $item['quantidade']),24, "0", STR_PAD_LEFT);
                }

//                $this->string .= str_pad($this->arrumaValor((!empty($item['preco_promocao'])) ? $item['preco_promocao'] : $item['preco']), 12, "0", STR_PAD_LEFT);
//                $this->string .= str_pad('', 12, "0", STR_PAD_LEFT);
//                $this->string .= str_pad($this->arrumaValor((!empty($item['preco_promocao'])) ? $item['preco_promocao'] : $item['preco']), 12, "0", STR_PAD_LEFT);
                $this->nova_linha();
                $i++;
            }
        }

        private function cabecalho_pre_venda()
        {
            $this->id('03');
            $this->sequencial('0002');
            $this->string .= str_pad($this->data_ou_hora($this->order['created']), 8, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->data_ou_hora($this->order['created'], 'His'), 6, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->order['id'], 6, "0", STR_PAD_LEFT);
            $this->string .= str_pad('', 10, "0", STR_PAD_LEFT);
            $this->string .= str_pad('', 18, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['usuario_cpf'], 14, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->arrumaValor($this->order['valor_pedido']), 12, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->arrumaValor($this->order['valor_desconto_cupom']), 12, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->arrumaValor($this->order['valor_frete']), 12, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->arrumaValor($this->order['valor_pedido'] + $this->order['valor_frete']), 12, "0", STR_PAD_LEFT);
            $this->string .= str_pad('', 100, " ", STR_PAD_RIGHT);
        }

        private function arrumaValor($valor)
        {
            $novovalor = (int)(100 * ((float)(str_replace(',', '.', $valor))));

            return str_replace(',','',$novovalor);
        }

        private function data_ou_hora($data, $retorno = 'dmY')
        {
            $date = new DateTime($data);
            return $date->format($retorno);
        }

        private function informacoes_usuario()
        {
            $this->id('01');
            $this->sequencial('0001');
            $this->codigo('', 10, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->order['usuario_nome'], 50, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_rua'], 40, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_numero'], 5, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_complemento'], 5, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_bairro'], 30, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_cidade'], 30, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_estado'], 2, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['endereco_cep'], 9, " ", STR_PAD_RIGHT);
            $this->string .= str_pad('Brasil', 30, " ", STR_PAD_RIGHT);
            $this->string .= str_pad('1', 1, " ", STR_PAD_RIGHT);
            $this->string .= $this->order['usuario_sexo'];
            $this->string .= str_pad('', 18, " ", STR_PAD_RIGHT);
            $this->string .= str_pad('', 18, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['usuario_cpf'], 14, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['usuario_rg'], 14, " ", STR_PAD_RIGHT);
            $this->string .= str_pad('', 8, "0", STR_PAD_LEFT);
            $this->string .= str_pad('', 8, "0", STR_PAD_LEFT);
            $this->string .= str_pad($this->order['usuario_telefone'], 50, " ", STR_PAD_RIGHT);
            $this->string .= str_pad('', 30, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['usuario_celular'], 30, " ", STR_PAD_RIGHT);
            $this->string .= str_pad($this->order['usuario_email'], 50, " ", STR_PAD_RIGHT);
        }

        private function nova_linha()
        {
            $this->string .= PHP_EOL;
        }

        private function id($id)
        {
            $this->string .= $id;
        }

        private function sequencial($sequencial, $length = 4, $pad_string = '0')
        {
            $this->string .= str_pad($sequencial, $length, $pad_string);
        }

        private function codigo($codigo, $length = 10, $pad_string = '0')
        {
            $this->string .= str_pad($codigo, $length, $pad_string);
        }

        private function end()
        {
            fwrite($this->handler, $this->string);
            fclose($this->handler);
        }

    }
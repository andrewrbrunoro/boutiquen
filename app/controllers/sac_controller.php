<?php

class SacController extends AppController {

    public $uses = array("Sac");
    public $components = array("Session","Email","Filter");
    public $helpers = array("Image","Flash","Html","Estados","Javascript","Calendario");

    public function index() {
        if (!empty($this->data)) {
            $this->Sac->set($this->data);
			if ($this->Sac->save($this->data)) {
                if ($this->sendMail($this->data)) {
                    $this->Session->setFlash('Contato realizado com sucesso!','flash/success');
                    $this->data = array();
                } else {
                    $this->Session->setFlash('Ocorreu um erro inesperado, tente novamente!','flash/error');
                }
            } else {
                $this->Session->setFlash('Por favor corrija os campos em destaque e tente novamente','flash/error');
            }
        }
        $this->set('breadcrumbs',array(array('nome'=>'Contato','link'=>'/contato')));
		
		$this->loadModel('SacTipo');
		$sacTipos = $this->SacTipo->find('list', array('fields' => array('SacTipo.id','SacTipo.nome'), 'conditions' => array('SacTipo.status' => true)));
		$this->set(compact('sacTipos'));
		
		$this->set('pagina_atual_tipo', "Institucional");
        $this->set('pagina_atual_url', '/contato');
		$this->set('seo_title', 'Contato');
		
		//start breadcrumb
        $breadcrumb = "";
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => 'Institucional');
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => 'Contato');
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }
	public function admin_index() {
			
		//listo os tipos de sac
        App::import('Model', 'SacTipo');
        $this->SacTipo = new SacTipo();
        $sac_tipos = $this->SacTipo->find('list',array('fields' => array('id', 'nome'), 'conditions' => array('SacTipo.status' => true)));
        $this->set('sac_tipos', $sac_tipos);
		
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "Sac.nome LIKE '%{%value%}%' OR Sac.email LIKE '%{%value%}%' OR Sac.mensagem LIKE '%{%value%}%'";
        }
		
		if (isset($this->data["Filter"]["sac_tipo_id"])) {
            $filtros['sac_tipo_id'] = "Sac.sac_tipo_id = '{%value%}'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		//sac
        $this->Sac->recursive = 1;
		$sac = $this->paginate('Sac', $conditions);
        $this->set('sac', $sac);
	}
	public function admin_exportar($conditions){
	
		$rows = $this->Sac->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>E-mail</strong></td>
					<td><strong>Telefone</strong></td>
					<td><strong>Estado</strong></td>
					<td><strong>Cidade</strong></td>
					<td><strong>Mensagem</strong></td>
					<td><strong>Tipo Sac</strong></td>
					<td><strong>Nome Vendedor</strong></td>
					<td><strong>Loja</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$table .= "
				<tr>
					<td>".$row['Sac']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Sac']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Sac']['email'])."</td>
					<td>".$row['Sac']['telefone']."</td>
					<td>".$row['Sac']['estado']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Sac']['cidade'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Sac']['mensagem'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['SacTipo']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Vendedor']['nome'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['Loja']['cidade'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "sac_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
    public function admin_view($id = null) {
        $sac = $this->Sac->read(null, $id);
        $this->set('sac', $sac);
    }
	public function admin_lido($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
		$this->Sac->id = $id;
        if ($this->Sac->saveField('lido',true)) {
            $this->Session->setFlash('Contato Lido com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser alterado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	public function admin_nlido($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
		$this->Sac->id = $id;
        if ($this->Sac->saveField('lido',false)) {
            $this->Session->setFlash('Contato Marcado como não Lido', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser alterado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	private function sendMail($dados) {
        Configure::write('debug', 0);
		if (Configure::read('Loja.smtp_host') != 'localhost') {
            $this->Email->smtpOptions = array(
                'port' => (int) Configure::read('Loja.smtp_port'),
                'host' => Configure::read('Loja.smtp_host'),
                'username' => Configure::read('Loja.smtp_user'),
                'password' => Configure::read('Loja.smtp_password')
            );
            $this->Email->delivery = 'smtp';
        }
		
		//dados do tipo do sac
		$this->loadModel('SacTipo');
		$sacTipo = $this->SacTipo->find('first', array('conditions' => array('SacTipo.id' => $dados['Sac']['sac_tipo_id'])));
		if($sacTipo){
			$sac_tipo = $sacTipo['SacTipo']['nome'];
			$email_to = $sacTipo['SacTipo']['email_contato'];
		}else{
			$sac_tipo = "Comntato";
			$email_to = Configure::read('Loja.email_sac');
		}
		
        $this->Email->lineLength = 120;
        $this->Email->sendAs = 'html';
        $this->Email->replyTo = Configure::read('Loja.smtp_remetente_email') . " <" . $email_to . ">";
		$this->Email->from = Configure::read('Loja.smtp_remetente_email') . " <" . Configure::read('Loja.smtp_user') . ">";
        $this->Email->to = Configure::read('Loja.smtp_remetente_email') . " <" . $email_to . ">";
		$this->Email->subject = Configure::read('Loja.nome') . " - Contato realizado pelo site";
         
		$email = str_replace(array(
		'{SAC_TIPO}',
		'{SAC_NOME}',
		'{SAC_EMAIL}',
		'{SAC_ESTADO}',
		'{SAC_CIDADE}',
		'{SAC_TELEFONE}',
		'{SAC_CNPJ}',
		'{SAC_MENSAGEM}',
		'{SAC_DATA}',
		'{SAC_IP}'
		),array(
		$sac_tipo,
		$dados['Sac']['nome'],
		$dados['Sac']['email'],
		(isset($dados['Sac']['estado'])) ? $dados['Sac']['estado'] : "",
		(isset($dados['Sac']['cidade'])) ? $dados['Sac']['cidade'] : "",
		(isset($dados['Sac']['telefone'])) ? $dados['Sac']['telefone'] : "",
		(isset($dados['Sac']['cnpj'])) ? $dados['Sac']['cnpj'] : "",
		$dados['Sac']['mensagem'],
		date("d/m/Y H:i:s"),
		$_SERVER['REMOTE_ADDR']
		),Configure::read('LojaTemplate.sac')
		);
		
			$bccs = array();
			$assunto = Configure::read('Reweb.nome_bcc');
		if(Configure::read('Reweb.nome_bcc')){
			$emails = Configure::read('Reweb.email_bcc');
			foreach(explode(';',$emails) as $bcc){
                            if($bcc != ""){
                                $bccs[] = "{$assunto} <{$bcc}>";
                            }
			}
		}
		
		if(Configure::read('Loja.email_sac') != ""){
			$mail_sac = Configure::read('Loja.email_sac');
			$bccs[] = "{$assunto} <{$mail_sac}>";
		}
		
		if(is_array($bccs) && count($bccs) > 0){
			$this->Email->bcc = $bccs;
		}
		
		if ($this->Email->send($email)) {
		    return true;
        } else {
		    return false;
        }
		
    }
}

?>
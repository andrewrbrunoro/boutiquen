<?php

class BuscaController extends AppController {

    public $uses = array('Busca','Produto');
    var $helpers = array('Parcelamento', 'String', 'Image', 'Javascript','Calendario');
    var $components = array('Filter');

    /**
     * Realiza as buscas dos produtos pelo form de busca do site
     */
    function index() {

        App::import("Model", "Busca");
        $this->Busca = new Busca();
        App::import("Model", "Produto");
        $this->Produto = new Produto();
        App::import('Component', 'Metaphone');
        $this->Metaphone = new MetaphoneComponent();
        if (strpos($this->params['url']['url'], "marca/index") !== false) {
            $is_marca = true;
        } else {
            $is_marca = false;
        }

        $this->params['url']['busca'] = $this->params['pass'][0];

        if(isset($this->params['pass'][1]) && is_numeric($this->params['pass'][0])){
            $this->params['url']['busca'] = $this->params['pass'][1];
            $is_marca = true;
        }

        $conditions = array('Produto.status >' => 0, 'Produto.grade_id <> ' => null);
        $conditions_fabricante = array();
        //verifica se a busca é pelo form de busca = variaval setada no routes

        if ($is_marca) {
            if (isset($this->params['pass'][0]) && isset($this->params['pass'][1])) {
                $conditions = array_merge($conditions, array("AND" => array("Produto.fabricante_id" => $this->params['pass'][0])));
            }
            $this->data['Busca']['busca'] = $this->params['pass'][1];
        } elseif ($this->params['url']['busca'] != "") {
            //se possuir o parametro todos retira o conditions e adiciona um conditions para todos os produtos
            if (isset($this->params['url']['busca']) && !empty($this->params['url']['busca']) && $this->params['url']['busca'] != "todos") {
                $busca_metaphonica = $this->Metaphone->portuguese_metaphone($this->params['url']['busca']);
                if (preg_match("/[^a-z]+/i", $this->params['url']['busca'])) {
                    $conditions = array_merge($conditions, array("OR" => array("MATCH(Produto.nome,Produto.tag) AGAINST('" . addslashes($this->params['url']['busca']) . "' IN BOOLEAN MODE)", "Produto.nome LIKE '%" . addslashes($this->params['url']['busca']) . "%'  OR Produto.sku LIKE '%" . addslashes($this->params['url']['busca']) . "%' OR Produto.marca LIKE '%" . addslashes($this->params['url']['busca']) . "%' OR Produto.referencia LIKE '%" . addslashes($this->params['url']['busca']) . "%' ")));
                } else {
                    $conditions = array_merge($conditions, array("OR" => array("Produto.metaphone_nome LIKE '%" . addslashes($busca_metaphonica) . "%'", "Produto.metaphone_tag LIKE '%" . addslashes($busca_metaphonica) . "%'", "MATCH(Produto.nome,Produto.tag) AGAINST('" . addslashes($this->params['url']['busca']) . "' IN BOOLEAN MODE)", "Produto.nome LIKE '%" . addslashes($this->params['url']['busca']) . "%'  OR Produto.sku LIKE '%" . addslashes($this->params['url']['busca']) . "%' OR Produto.marca LIKE '%" . addslashes($this->params['url']['busca']) . "%' OR Produto.referencia LIKE '%" . addslashes($this->params['url']['busca']) . "%' ")));
                }
            } else if (isset($this->params['url']['busca']) && !empty($this->params['url']['busca']) && $this->params['url']['busca'] == "todos") {
                $conditions = array_merge($conditions, array("OR" => array("Produto.nome LIKE" => '%%')));
            }
            //die(var_dump($conditions));
            if (isset($this->params['url']['busca']) && $this->params['url']['busca'] != 'todos') {
                $this->params['slug'] = $this->params['url']['busca'];
                $this->data['Busca']['busca'] = $this->params['url']['busca'];
            } elseif (isset($this->params['url']['busca']) && $this->params['url']['busca'] == 'todos') {
                $this->params['slug'] = $this->params['url']['busca'];
            }
        } else {
            $this->redirect("/");
        }

        //se nao tiver paramentros entro direto, redirecionado para home
        if (count($conditions) <= 0)
            $this->redirect("/");
        $joins =
                array(
                    array(
                        'table' => 'produtos_categorias',
                        'alias' => 'Categorias',
                        'type' => 'left',
                        'conditions' =>
                        array(
                            'Categorias.produto_id = Produto.id'
                        )
                    ), array(
                        'table' => 'categorias',
                        'alias' => 'Categoria',
                        'type' => 'left',
                        'conditions' =>
                        array(
                            'Categorias.categoria_id = Categoria.id'
                        )
                    )
        );

        $produtos_ids = $this->Produto->find("all", array('fields' => 'Produto.id', 'recursive' => -1, 'conditions' => $conditions));
        $produtos_ids = Set::extract('{n}./Produto/id', $produtos_ids);
        $produtos_ids_com_atributo = $this->carregaFiltrosBusca($produtos_ids);

        //se há filtro de atributos, acrescento a condition com ids dos produtos
        if (isset($this->params['named']['atributos']) && !empty($this->params['named']['atributos'])) {
            $conditions['Produto.id'] = explode(',', $produtos_ids_com_atributo);
            App::import('Model', 'Atributo');
            $this->Atributo = new Atributo();
            $atributo_tamanho = false;
            $filtros_ids = json_decode($this->params['named']['atributos']);
            foreach ($filtros_ids as $filtros_id) {
                $atributo = $this->Atributo->find('first', array('recursive' => 1, 'contain' => 'AtributoTipo', 'conditions' => array('Atributo.id' => $filtros_id)));
                if ($atributo['AtributoTipo']['nome'] == 'Tamanho') {
                    $atributo_tamanho = true;
                }
            }
            if ($atributo_tamanho != true) {
                $conditions = array_merge(
                        array('Produto.parent_id' => 0), $conditions
                );
            }
        } else {
            $conditions = array_merge(
                    array('Produto.parent_id' => 0), $conditions
            );
        }

        $this->paginate = array(
            'limit' => 24,
            'group' => 'Produto.id',
            'order' => array('rating' => 'DESC', 'quantidade_disponivel' => 'DESC', 'quantidade_acessos' => 'DESC'),
            'recursive' => -1,
            'joins' => $joins,
            'fields' => array('(SELECT IF(FreteGratis.label != "",FreteGratis.label,"") FROM frete_gratis FreteGratis WHERE (FreteGratis.categoria_id LIKE concat("%\"",Categorias.categoria_id,"\"%") OR FreteGratis.produto_id LIKE concat("%\"",Categorias.produto_id,"\"%") OR FreteGratis.grade_id LIKE concat("%\"",Produto.grade_id,"\"%") ) AND ( FreteGratis.status = true AND IF(Produto.preco_promocao > 0,Produto.preco_promocao,Produto.preco) >= FreteGratis.apartir_de AND FreteGratis.data_inicio <= now() AND FreteGratis.data_fim >= now() ) LIMIT 1) as frete_gratis,Produto.id,Produto.nome,Produto.preco,Produto.preco_promocao,Produto.grades,Produto.variacoes,Produto.quantidade_disponivel,Produto.status,Produto.preco_promocao_inicio,Produto.preco_promocao_fim,Produto.preco_promocao_novo,Produto.preco_promocao_velho, Produto.status, Produto.grade_id, Produto.marca, Produto.fabricante_id, Produto.codigo, Produto.unidade_venda, Categoria.nome,Categoria.seo_url ,(SELECT concat("uploads/produto_imagem/filename/",GradeImagem.filename) FROM grade_imagens as GradeImagem WHERE GradeImagem.grade_id = Produto.grade_id ORDER BY GradeImagem.ordem ASC LIMIT 1) as imagem,(MATCH (Produto.nome,Produto.tag) AGAINST ("' . addslashes($this->params['url']['busca']) . '" IN BOOLEAN MODE)) AS rating, (select group_concat(concat(concat(id, \'|\'),concat(concat(thumb_dir, \'/\'),thumb_filename)) separator \';\') as file from flags fla inner join produtos_flags pfla on pfla.flag_id=fla.id where pfla.produto_id=Produto.id ) as flags')
        );
        $produtos = $this->paginate('Produto', $conditions);
        $this->set('produtos', $produtos);

        /* Procura na tabela "busca" se nao tem a ocorrencia da pesquisa associada a palavra relacionada */
        if (empty($produtos) && null != $this->params['url']['busca']) {
            $conditions = array('Busca.quantidade_itens =' => 0, 'Busca.quantidade_busca >=' => 0);
            $aBuscas = $this->Busca->find('all', array('conditions' => $conditions));
            foreach ($aBuscas as $aBusca) {
                if ($aBusca['Busca']['palavra_chave'] == $this->params['url']['busca']) {
                    if (isset($aBusca['Busca']['palavra_relacionada'])) {
                        $sInputBusca = $aBusca['Busca']['palavra_relacionada'];
                        $this->params['pass'][0] = $sInputBusca;
                        self::index();
                    }
                }
            }
        }

        $valido = false;
        foreach ($produtos as $produto) {
            if (preg_match("/{$this->params['pass'][0]}/", $produto['Produto']['nome'])) {
                $valido = true;
            }
        }

        //begin salvo o registro da busca no banco
        if (!$is_marca && $valido == true) {
            $dados_busca = array();
            $dados_busca['id'] = '';
            $dados_busca['quantidade_itens'] = $this->params['paging']['Produto']['count'];
            $dados_busca['palavra_chave'] = trim($this->params['pass'][0]);
            $dados_busca['metaphone'] = $this->Metaphone->portuguese_metaphone($dados_busca['palavra_chave']);
            $this->Busca->salvar($dados_busca);
        } else {
            /* salva registro de buscas que nao retornam nada - dic. hiroshima */
            $dados_busca = array();
            $dados_busca['id'] = '';
            $dados_busca['quantidade_itens'] = $this->params['paging']['Produto']['count'];
            if(isset($this->params['pass'][1])){
                $dados_busca['palavra_chave'] = trim($this->params['pass'][1]);
            }else{
                $dados_busca['palavra_chave'] = trim($this->params['pass'][0]);
            }
            $dados_busca['metaphone'] = $this->Metaphone->portuguese_metaphone($dados_busca['palavra_chave']);
            $this->Busca->salvar($dados_busca);
            $this->set('busca_google', $dados_busca);
        }
        //end salvo o registro da busca no banco

        if ($this->params['url']['busca'] == "todos" && $cat['Categoria']['nome'] != "") {
            $this->set('seo_title', addslashes($cat['Categoria']['nome']) . ' na ' . Configure::read('Loja.nome') . '.com.br');
        } else {
            if (!$is_marca) {
                $this->set('seo_title', addslashes($this->params['url']['busca']) . ' na ' . Configure::read('Loja.nome') . '.com.br');
            } else {
                $this->set('seo_title', addslashes($this->data['Busca']['busca']) . ' na ' . Configure::read('Loja.nome') . '.com.br');
            }
        }
        //start breadcrumb
        $breadcrumb = "";
        if (!$is_marca) {
            $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Resultado de Busca");
        } else {
            $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => "Marca");
        }
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => $this->data['Busca']['busca']);
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
    }

    private function carregaFiltrosBusca($produtos_id) {

        //START: isso vai sair daki
        //menu de categorias do topo
        /* if(Cache::read('menu_topo') === false){
          if(!isset($menu_topo)){
          $menu_topo = $this->Categoria->find('all',array('contain'=>array('SubCategory'),'conditions'=>array('Categoria.parent_id' => 0)));
          }

          Cache::write('menu_topo', $menu_topo);
          }
          else{
          $menu_topo = Cache::read('menu_topo');
          }

          $this->set('menu_topo', $menu_topo);
          $this->set('menu', array()); */
        //END: isso vai sair daki
        //se passar os produtos resultante da busca, faz o tratamento
        if (!empty($produtos_id)) {
            //se nao tem filtro na url, busca todos os atributos de todos os produtos resultantes da busca
            if (!isset($this->params['named']['atributos']) && empty($this->params['named']['atributos'])) {
                $query = "
						SELECT
							Atributo.valor,
							AtributoTipo.nome,
							AtributoTipo.codigo,
							AtributoTipo.id,
							Atributo.id,
                            Atributo.thumb_filename, 
                            Atributo.thumb_dir
						FROM
							atributo_tipos AS AtributoTipo
								LEFT JOIN atributos AS Atributo ON (Atributo.atributo_tipo_id = AtributoTipo.id) 
								LEFT JOIN atributos_produtos AS ProdutoAtributo ON (Atributo.id = ProdutoAtributo.atributo_id)
								LEFT JOIN produtos AS Produto ON (Produto.id = ProdutoAtributo.produto_id)
						WHERE
							Atributo.filtro = TRUE
							AND Produto.status > 0
							AND
							(Produto.parent_id in (" . implode(",", $produtos_id) . ") OR Produto.id in (" . implode(",", $produtos_id) . "))
						GROUP BY
							Atributo.id
						ORDER BY
							AtributoTipo.nome,
							Atributo.valor
						";
                $atributos = $this->Categoria->Query($query);

                ///inicio - não estava trazendo orndenado o tipo de salto
                $saltos = array();
                foreach ($atributos as $key => $value) {
                    
                    if($value['AtributoTipo']['nome'] == 'Salto'){                       
                        array_push($saltos, $value);
                        unset($atributos[$key]);                        
                    }
                }

                if($saltos){
                    asort($saltos);

                    foreach ($saltos as $key => $value) {
                       array_push($atributos, $value);
                    }                
                }
                ///fim - não estava trazendo orndenado o tipo de salto         


            } else {

                //se tiver parametro na url, faco a busca dos atributos considerandos os produtos e atributos
                //capturo os filtros da url
                $filtros_ids = json_decode($this->params['named']['atributos']);
                $produtos_ids_com_atributo = "";
                //percorro os filtros
                foreach ($filtros_ids as $k => $v):

                    //no primeiro atributo do filtro, considero os produtos resultante da busca
                    if ($k == 0) {
                        $produtos_id_cond = implode(',', $produtos_id);
                    } else {
                        //a partir do primeiro, considero os produtos já filtrados pelo primeiro atributo
                        $produtos_id_cond = $produtos_ids_com_atributo;
                    }

                    $query = "
								SELECT group_concat(AtributoProduto.produto_id) as produtos_ids
								FROM
									atributos_produtos AS AtributoProduto
								WHERE
									AtributoProduto.atributo_id = " . $v . " AND
									AtributoProduto.produto_id IN ( " . $produtos_id_cond . " )
								";

                    $produtos_in = $this->Categoria->query($query);
                    $produtos_ids = $produtos_in[0][0]['produtos_ids'];
                    $produtos_ids_com_atributo = $produtos_ids;

                endForeach;

				if($produtos_ids_com_atributo != ""){					
                //monto a query dos atributos
                $query = "
							SELECT
								*
							FROM
									atributo_tipos AS AtributoTipo,
									atributos AS Atributo
										INNER JOIN
									atributos_produtos AS AtributoProduto
										ON Atributo.id = AtributoProduto.atributo_id
							WHERE
									Atributo.filtro = true AND
									AtributoTipo.id = Atributo.atributo_tipo_id AND
									AtributoProduto.produto_id IN ( " . $produtos_ids_com_atributo . " )
							GROUP BY
									Atributo.id
							ORDER BY
									AtributoTipo.nome, Atributo.valor ASC
						";

                //print('<hr />Query Atributos = '.$query);

                $atributos = $this->Categoria->Query($query);


                ///inicio - não estava trazendo orndenado o tipo de salto
                $saltos = array();
                foreach ($atributos as $key => $value) {
                    
                    if($value['AtributoTipo']['nome'] == 'Salto'){                       
                        array_push($saltos, $value);
                        unset($atributos[$key]);                        
                    }
                }

                if($saltos){
                    asort($saltos);

                    foreach ($saltos as $key => $value) {
                       array_push($atributos, $value);
                    }                
                }
                ///fim - não estava trazendo orndenado o tipo de salto                      

                //removo o filtro atual do array de atributos
                //alimento o array de filtros
                $filtros = array();
                foreach (json_decode($this->params['named']['atributos']) as $v) {
                    foreach ($atributos as $key => $atributo) {
                        $k = array_search($v, $atributo['Atributo']);
                        if ($k == 'id') {
                            $filtros[$key] = am($filtros, $atributos[$key]);
                            unset($atributos[$key]);
                        }
                    }
                }

                //monto o menu de filtros selecionados
                $filtros_menu = array();
                foreach ($filtros as $linha) {

                    $filtros_menu[$linha['AtributoTipo']['id']]['nome_filtro'] = $linha['AtributoTipo']['nome'];
                    $filtros_menu[$linha['AtributoTipo']['id']]['codigo_filtro'] = $linha['AtributoTipo']['codigo'];
                    $filtros_menu[$linha['AtributoTipo']['id']]['id_filtro'] = $linha['Atributo']['id'];
                    $filtros_menu[$linha['AtributoTipo']['id']]['valor_filtro'] = $linha['Atributo']['valor'];
                }
                //set
                $this->set('filtros_selecionados', $filtros_menu);
				}
            }

            //monto o menu de atributo
            $atributos_menu = array();
			if(isset($atributos)){
            foreach ($atributos as $linha) {
                if(isset($filtros_menu[$linha['AtributoTipo']['id']])) continue;
                $atributos_menu[$linha['AtributoTipo']['id']]['nome_filtro'] = $linha['AtributoTipo']['nome'];
                $atributos_menu[$linha['AtributoTipo']['id']]['codigo_filtro'] = $linha['AtributoTipo']['codigo'];
                $atributos_menu[$linha['AtributoTipo']['id']]['valor_filtro'][] = $linha['Atributo'];
            }
            $this->set('menu_filtro', $atributos_menu);
			}

            //retorno os produtos resultantes do filtro
            if (isset($produtos_ids_com_atributo))
                return $produtos_ids_com_atributo;
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Busca->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_index() {

        $this->helpers[] = 'Layout';
        $filtros = array();

        if ($this->data["Filter"]["palavra_chave"]) {
            $filtros['palavra_chave'] = "Busca.palavra_chave LIKE '%{%value%}%'";
        }

        if ($this->data["Filter"]["palavra_relacionada"]) {
            $filtros['palavra_relacionada'] = "Busca.palavra_relacionada LIKE '%{%value%}%'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $conditions += array('Busca.quantidade_itens =' => 0, 'Busca.quantidade_busca >=' => 0);
        $this->Filter->setDataToView();



        $this->paginate = array(
            'limit' => 25,
            'order' => array('Busca.quantidade_itens' => 'ASC', 'Busca.quantidade_busca' => 'DESC'),
            'recursive' => 0
        );

        $buscas = $this->paginate('Busca', $conditions);
        $this->set('buscas', $buscas);
    }

    function admin_add() {

        if (!empty($this->data)) {
            $this->Busca->create();
            if ($this->Busca->save($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        } else {
            $this->helpers[] = 'Layout';
        }
    }

    function admin_edit($id = null) {

        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }

        if (!empty($this->data)) {
            if ($this->Busca->save($this->data, false)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }

        if (empty($this->data)) {
            $this->helpers[] = 'Layout';
            $this->data = $this->Busca->read(null, $id);
        }
    }

}

?>
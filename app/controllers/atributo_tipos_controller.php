<?php

/**
 * BakeSale shopping cart
 * Copyright (c)	2006-2009, Matti Putkonen
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright		Copyright (c) 2006-2009, Matti Putkonen
 * @link			http://bakesalepro.com/
 * @package			BakeSale
 * @license			http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Manages product categories
 *
 */
class AtributoTiposController extends AppController {

    public $uses = array('AtributoTipo');
    var $helpers = array('Javascript','String','Calendario');
	var $components = array('Filter');

	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["nome"])) {
            $filtros['nome'] = "AtributoTipo.nome LIKE '%{%value%}%'";
        }	
		if (isset($this->data["Filter"]["status"])) {
            $filtros['status'] = "AtributoTipo.status = '{%value%}'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->AtributoTipo->recursive = 0;
		$this->set('atributo_tipos', $this->paginate($conditions));
	}
	
    public function admin_add() {
        if (!empty($this->data)) {

            if ($this->AtributoTipo->saveAll($this->data)) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
            }
        }
	}
	
    public function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['AtributoTipo']['id'] = $id;
			$this->AtributoTipo->id = $id;
                 
			if ($this->AtributoTipo->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->AtributoTipo->read(null, $id);
		}
    }

	public function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->AtributoTipo->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
	public function admin_exportar($conditions){
	
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->AtributoTipo->find('all',array('recursive' => 0, 'conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Nome</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['AtributoTipo']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['AtributoTipo']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['AtributoTipo']['nome'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['AtributoTipo']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['AtributoTipo']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "atributos_tipos_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
    
	public function ajax_get_atributo_tipos() {
		$this->layout = 'ajax';
		$busca_atributo_tipos = $this->AtributoTipo->find("all", array(
										'recursive'=>-1,
										'fields' => array('AtributoTipo.id','AtributoTipo.nome'),
										'conditions' => "AtributoTipo.nome LIKE '%".$this->params['url']['term']."%'"
										)
							);
		$json = array();
		foreach($busca_atributo_tipos as $atributo_tipos){
			$json[] = array(
				'id' => $atributo_tipos['AtributoTipo']['id'],
				'nome' => $atributo_tipos['AtributoTipo']['nome']
			);
		}
		die(json_encode($json));
    }	
}

?>

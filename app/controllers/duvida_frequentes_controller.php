<?php
class DuvidaFrequentesController extends AppController {

	var $name = 'DuvidaFrequente';
	var $components = array('Session','Filter');
	var $helpers = array('Calendario','String','Image','Flash','Javascript','Marcas','Estados');

    public function index() {
    	
        $this->set('duvidas_frequentes', $this->DuvidaFrequente->buscaDuvidas());
        $this->set('pagina_atual_tipo', "Institucional");
        $this->set('pagina_atual_url', 'duvidas-frequentes');
        //start breadcrumb
        $breadcrumb = "";
        $breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => 'Institucional');
        ///$breadcrumb[] = array('url' => 'javascript:void(0);', 'nome' => 'Dúvidas Frequentes');
        $this->set('breadcrumb', $breadcrumb);
        //end breadcrumb
	}			
	
	function admin_index() {
		//filters
		$filtros = array();
        if (isset($this->data["Filter"]["filtro"])) {
            $filtros['filtro'] = "DuvidaFrequente.pergunta LIKE '%{%value%}%' OR DuvidaFrequente.resposta LIKE '%{%value%}%'";
        }
		
        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();
		
		if(isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar"){
			$this->admin_exportar($conditions);
		}
		
		$this->DuvidaFrequente->recursive = 0;
		$this->set('duvidas_frequentes', $this->paginate($conditions));
	}
	public function admin_exportar($conditions){
		
		App::import('Helper', 'Calendario');
		$this->Calendario = new CalendarioHelper();
		
		$rows = $this->DuvidaFrequente->find('all',array('conditions' => $conditions));
		
		$table = "<table>";
		$table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>Pergunta</strong></td>
					<td><strong>Resposta</strong></td>
					<td><strong>Status</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Modificado</strong></td>
				</tr>";
		foreach ($rows as $row) {
			$status = ( $row['DuvidaFrequente']['status'] ) ? "Ativo" : "Inativo";
			$table .= "
				<tr>
					<td>".$row['DuvidaFrequente']['id']."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['DuvidaFrequente']['pergunta'])."</td>
					<td>".iconv("UTF-8", "ISO-8859-1//IGNORE",$row['DuvidaFrequente']['resposta'])."</td>
					<td>".$status."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['DuvidaFrequente']['created'])."</td>
					<td>".$this->Calendario->DataFormatada("d-m-Y",  $row['DuvidaFrequente']['modified'])."</td>
				</tr>";
		}
		$table .= "</table>";
		
		App::import("helper", "String");
		$this->String = new StringHelper();
		$this->layout = false;
		$this->render(false);
		set_time_limit(0);		
		header('Content-type: application/x-msexcel');
		$filename = "duvidas_frequentes_" . date("d_m_Y_H_i_s");
		header('Content-Disposition: attachment; filename='.$filename.'.xls');
		header('Pragma: no-cache');
		header('Expires: 0');
		
		die($table);
	}
	function admin_add() {
		if (!empty($this->data)) {
			$this->DuvidaFrequente->create();
			if ($this->DuvidaFrequente->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}

		$duvidaFrequenteTipos = $this->DuvidaFrequente->DuvidaFrequenteTipo->find(
																				'list', array(
																					'fields' 		=> 'nome', 
																					'conditions' 	=> array('status' => true)
																				)
																			);
		$this->set(compact('duvidaFrequenteTipos'));
	}

	function admin_edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Parâmetro inválidos','flash/error');
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->data['DuvidaFrequente']['id'] = $id;
			$this->DuvidaFrequente->id = $id;
                 
			if ($this->DuvidaFrequente->save($this->data)) {
				$this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
			}
		}
		if (empty($this->data)) {
			$this->data = $this->DuvidaFrequente->read(null, $id);
		}
		
		$duvidaFrequenteTipos = $this->DuvidaFrequente->DuvidaFrequenteTipo->find(
																				'list', array(
																					'fields' 		=> 'nome', 
																					'conditions' 	=> array('status' => true)
																				)
																			);
		$this->set(compact('duvidaFrequenteTipos'));
	}
	function admin_delete($id = null) {
		if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->DuvidaFrequente->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
	}
	
}
?>
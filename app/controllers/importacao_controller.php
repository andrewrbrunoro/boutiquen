<?php
class ImportacaoController extends AppController {
	
	var $uses = array( 'Importacao' );
	
	public $components = array( 'Filter' , 'Session');
	
	/**
	 * @param Atributo Constante caminho do arquivo
	 */
	const PATH_IMPORTACAO = WWW_ROOT;
	
	/**
	 * @param Atributo que receberá o Objeto ZipArchive
	 */
	private $zip = FALSE;
	
	/**
	 * @param Atributo arquivo
	 */
	private $arquivo = FALSE;
	
	/**
	 * @param Atributo arquivo zip
	 */
	private $arquivozip = FALSE;
	
	/**
	 * @param Atributo pasta de imagens
	 */
	private $imgpath = FALSE;
	
	/**
	 * @param Atributo data de agendamento
	 */
	private $agendado = FALSE;
	
	/**
	 * @param Atributo Save
	 */
	private $save = array( 'Importacao' => array(  ) );
	
	/**
	 * @param Atributo Dir para o objeto Interator
	 */
	private $dir = FALSE;
	
	/**
	 * @param Atributo extensões permitidas
	 */
	private $extensions = array( 'jpg' , 'png' , 'gif' , 'jpeg' );
	
	/**
	 * @param Atributo nome log de importacao
	 */
	public $lognome = '';
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 */
	public function admin_index(){
		
		$importacoes = $this->Importacao->find('all');
		$this->set('importacoes',$importacoes);
		
		if($_SERVER['REQUEST_METHOD'] == 'POST'){
			
			$bool = $this->store($this->data['Importacao']);
			
			if($bool == TRUE && empty($this->agendado)){
				
				if(!empty($this->arquivozip)){
					
					if($this->unzip($this->arquivozip)){
						
						$this->manipularImagens($this->imgpath);
						
					}else{
						
						$this->log('Erro ao tentar extrair a pasta do arquivo .zip '.$this->arquivozip.PHP_EOL,'importacao_log-'.date('d-m-Y'));
						
					}			
					
				}
				
				$this->Session->setFlash('Importação efetuada com sucesso', 'flash/success');
				$this->redirect('./');
				
			}
			if($bool == TRUE && !empty($this->agendado)){
				
				$this->Session->setFlash('Importação agendada para '.$this->agendado, 'flash/success');
				$this->redirect('./');
				
			}
			if($bool == FALSE){
				
				$this->Session->setFlash('Não foi possível inserir', 'flash/error');
				$this->redirect('./');
				
			}
		}
	}
	// -------------------------------------------------------------------
	//  http://www.cjdinfo.com.br/solucao-php-excluir-diretorio-com-sub-diretorios-e-arquivos
	//  Exclui o Diretório dado com todos seus sub-diretórios e arquivos:
	//  Obs.:   - Função recursiva;
	//          - Montada para Linux (Separador "/").
	// -------------------------------------------------------------------
	private function ExcluiDir($Dir){
    
	    if ($dd = opendir($Dir)) {
	        while (false !== ($Arq = readdir($dd))) {
	            if($Arq != "." && $Arq != ".."){
	                $Path = "$Dir/$Arq";
	                if(is_dir($Path)){
	                    $this->ExcluiDir($Path);
	                }elseif(is_file($Path)){
	                    unlink($Path);
	                }
	            }
	        }
	        closedir($dd);
	    }
	    rmdir($Dir);
	}
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 */
	public function admin_importa(){
		
		if($_SERVER['REQUEST_METHOD'] == "POST"){
			
			$importacao = $this->Importacao->find("first",
				array(
					'conditions' => array( 'Importacao.id' => $this->data['Importacao']['id'] )
				)
			);
			
			if(empty($importacao['Importacao']['log'])){
			
				if($importacao){
	
					App::import('Component','Importacao');
					$this->ImportacaoComponent = new ImportacaoComponent($importacao['Importacao']['arquivo']);
					
					$this->lognome = $this->ImportacaoComponent->importacaoDados();
	
					$this->save = 'UPDATE importacoes SET log = "'.$this->lognome.'" WHERE id = '.$this->data['Importacao']['id'];
						
					if($this->Importacao->query($this->save)){
						
						rename(
							ImportacaoController::PATH_IMPORTACAO.'importacao/'.$importacao['Importacao']['arquivo'], 
							ImportacaoController::PATH_IMPORTACAO.'importacao/importados/'.date('d-m-Y').'-'.$importacao['Importacao']['arquivo']
						);
						
						rename(
							ImportacaoController::PATH_IMPORTACAO.'importacao/'.$importacao['Importacao']['zip'], 
							ImportacaoController::PATH_IMPORTACAO.'importacao/importados/'.date('d-m-Y').'-'.$importacao['Importacao']['zip']
						);
						
						$this->imgpath = explode('.',$importacao['Importacao']['zip']);
						$this->imgpath = $this->imgpath[0];
						
						$this->ExcluiDir(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$this->imgpath);
						
					}else{
						$this->log('Erro ao atualizar o log da importacao '.$this->lognome,'debug');
					}
						
				}
				$this->Session->setFlash('Importação efetuada , baixe o log para ver passo-a-passo da importação.','flash/success');
				$this->redirect('../importacao');
			
			}else{
				
				$this->Session->setFlash('Essa importação já foi efetuada, nome do log - '.$this->lognome,'flash/error');
				$this->redirect('../importacao');
				
			}
		}
		
	}
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 * @param String
	 */
	private function manipularImagens($string = ''){
		
		if(!empty($string)){
			
			$this->dir = new DirectoryIterator(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$string);
						
			foreach($this->dir as $file){
				
				if($file->isFile()){
					
					if(in_array($file->getExtension(), $this->extensions)){
						
						if(!file_exists(ImportacaoController::PATH_IMPORTACAO.'uploads/produto_imagem/filename/'.$file->getFilename())){
							
							copy(
								ImportacaoController::PATH_IMPORTACAO.'importacao/'.$this->imgpath.'/'.$file->getFilename(),
								ImportacaoController::PATH_IMPORTACAO.'uploads/produto_imagem/filename/'.$file->getFilename()
							);
						
						}else{
							
							$this->log('Imagem já existe '.$file->getFilename().PHP_EOL,'importacao_log-'.date('d-m-Y'));
							
						}
						
					}else{
						
						$this->log('Imagem não permitida, apenas JPG , PNG , GIF e JPEG'.PHP_EOL,'importacao_log-'.date('d-m-Y'));
						
					}
					
				}
				
			}
			
			//rmdir(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$this->imgpath);
			
		}
		
	}
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 * @param String
	 * @return Bool
	 */
	private function unzip($string = ''){
		
		if(!empty($string)){
			
			$this->zip = new ZipArchive();
			$open = '';
			$open = $this->zip->open(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$string);
			
			if($open){
				
				$string = explode('.',$string);
				$string = $string[0];
				
				$this->zip->extractTo(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$string);	
				$this->zip->close();
				
				$this->imgpath = $string;
				
				return TRUE;
				
			}else{
				
				return FALSE;
				
			}
			
		}
		
	}
	
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 * @param Array
	 * @return True|False
	 */
	public function store($data = array()){
		
		if($data){
			
			$this->arquivo = $this->validaArquivo($data['arquivo']['name']);
			$this->save['Importacao']['arquivo'] = $this->arquivo;
			
			if(!$this->moveArquivo($data['arquivo']['tmp_name'],$this->arquivo)){
				$this->Session->setFlash('Erro ao enviar o arquivo <strong> '.$this->arquivo.' </strong> , tente novamente!' , 'flash/error');
				$this->redirect('./');
			}
			
			if(!empty($data['zip']['name'])){
				
				$this->arquivozip = $this->validaArquivo($data['zip']['name']);
				$this->save['Importacao']['zip'] = $this->arquivozip;
				
				if(!$this->moveArquivo($data['zip']['tmp_name'],$this->arquivozip)){
						
					unlink(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$this->arquivo);
					$this->Session->setFlash('Erro ao enviar o arquivo <strong>'.$this->arquivozip.'</strong> , tente novamente!' , 'flash/error');
					$this->redirect('./');
					
				}
				
			}
			
			$this->Importacao->create($this->save);
			
			if($this->Importacao->save()){

				return TRUE;
				
			}else{
				
				unlink(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$this->arquivo);
				unlink(ImportacaoController::PATH_IMPORTACAO.'importacao/'.$this->arquivozip);

				return FALSE;
				
			}
			
		}
		
	}
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 * @param String
	 * @param String
	 * @return TRUE|FALSE
	 */
	private function moveArquivo($tmp = '',$string = ''){
		if(!empty($tmp) && !empty($string)){
			
			if(move_uploaded_file($tmp, ImportacaoController::PATH_IMPORTACAO.'importacao/'.$string)){
				return TRUE;
			}else{
				return FALSE;
			}
			
		}
	}
	
	/**
	 * @author Andrew Rodrigues Brunoro
	 * @param String
	 * @return String
	 */
	private function validaArquivo($string = ''){
		if(!empty($string)){
			
			$string = explode('.',$string);
			$filename = $string[0];
			$ext = '.'.$string[1];
			$filename = preg_replace('/[^a-zA-Z.]/','',$filename);
			
			$newfile = '';
			$newname = '';
			
			$i = 0;
			do{
				if($i){
					$newname = $filename.'_'.$i;
					$newfile = ImportacaoController::PATH_IMPORTACAO.'importacao/'.$newname.$ext;
					$newname = $newname.$ext;
				}else{
					$newname = $filename.$ext;
					$newfile = ImportacaoController::PATH_IMPORTACAO.'importacao/'.$filename.$ext;
				}
				$i++;
			}while(file_exists($newfile));
			
			echo $newfile.'<br />';

			return $newname;
			
		}
	}
	
	
	
	
	
	
}

?>
<?php

class VariacoesController extends AppController {

    var $name = 'Variacoes';
    var $components = array('Session', 'Filter');
    var $helpers = array('Calendario', 'String', 'Image', 'Flash', 'Javascript');

    function admin_index() {

        //filters
        $filtros = array();
        if (isset($this->data["Filter"]["variacao_grupo_id"])) {
            $filtros['variacao_grupo_id'] = "VariacaoGrupo.id = '{%value%}'";
        }

        $this->Filter->setConditions($filtros);
        $this->Filter->check();
        $conditions = $this->Filter->getFilters();
        $this->Filter->setDataToView();

        if (isset($this->params['form']['submit']) && $this->params['form']['submit'] == "Exportar") {
            $this->admin_exportar($conditions);
        }

        App::import('Model', 'VariacaoGrupo');
        $this->VariacaoGrupo = new VariacaoGrupo();
        $this->paginate = array(
            'contain' => array("VariacaoTipo" => array("Variacao")),
            'conditions' => $conditions
        );
        $variacoes = $this->paginate($this->VariacaoGrupo);
        $this->set('variacoes', $variacoes);
//        $this->Variacao->recursive = 1;
//        $this->paginate['Variacao'] = array(
//                                         'contain' => array("VariacaoTipo" => array("VariacaoGrupo"))
//                                        );
//        $this->set('variacoes', $this->paginate($conditions));
        //carrego as VariacaoTipos para os selects
        App::import('Model', 'VariacaoGrupo');
        $this->VariacaoGrupo = new VariacaoGrupo();
        $VariacaoGrupo = array('' => 'Selecione') + $this->VariacaoGrupo->find('list', array('fields' => array('VariacaoGrupo.id', 'VariacaoGrupo.nome'), 'order' => array('VariacaoGrupo.nome')));
        $this->set('variacao_grupos', $VariacaoGrupo);
    }

    function admin_add() {

        App::import('Model', 'VariacaoGrupo');
        $this->VariacaoGrupo = new VariacaoGrupo();
        App::import('Model', 'VariacaoTipo');
        $this->VariacaoTipo = new VariacaoTipo();
        
        $validationErrors = array();
        if (!empty($this->data)) {
            foreach ($this->data['VariacaoTipo'] as $k => $vt) {
                $this->data['VariacaoTipo'][$k]['variacao_grupo_id'] = $this->data['VariacaoGrupo']['id'];
                $this->VariacaoTipo->set($this->data['VariacaoTipo'][$k]);
                if ($this->VariacaoTipo->validates()) {
                    if ($this->data['Variacao'][$k]) {
                        foreach ($this->data['Variacao'][$k] as $b => $vp) {
                            $this->Variacao->set($this->data['Variacao'][$k][$b]);
                            if ($this->Variacao->validates()) {
                                if ($this->VariacaoTipo->save($this->data['VariacaoTipo'][$k])) {
                                    $this->data['VariacaoTipo'][$k]['id'] = $this->VariacaoTipo->id;
                                    $this->data['Variacao'][$k][$b]['variacao_tipo_id'] = $this->data['VariacaoTipo'][$k]['id'];
                                    
                                    //begin ajustes no meio upload
                                    if(isset($this->data['Variacao'][$k][$b]['name']['thumb_filename'])){
                                        $this->data['Variacao'][$k][$b]['thumb_filename']['name']       = $this->data['Variacao'][$k][$b]['name']['thumb_filename'];
                                        unset($this->data['Variacao'][$k][$b]['name']);
                                    }
                                    if($this->data['Variacao'][$k][$b]['type']['thumb_filename']){
                                        $this->data['Variacao'][$k][$b]['thumb_filename']['type']       = $this->data['Variacao'][$k][$b]['type']['thumb_filename'];
                                        unset($this->data['Variacao'][$k][$b]['type']);
                                    }
                                    if($this->data['Variacao'][$k][$b]['tmp_name']['thumb_filename']){
                                        $this->data['Variacao'][$k][$b]['thumb_filename']['tmp_name']   = $this->data['Variacao'][$k][$b]['tmp_name']['thumb_filename'];
                                        unset($this->data['Variacao'][$k][$b]['tmp_name']);
                                    }
                                    if($this->data['Variacao'][$k][$b]['error']['thumb_filename']){
                                        $this->data['Variacao'][$k][$b]['thumb_filename']['error']      = $this->data['Variacao'][$k][$b]['error']['thumb_filename'];
                                        unset($this->data['Variacao'][$k][$b]['error']);
                                    }
                                    if($this->data['Variacao'][$k][$b]['size']['thumb_filename']){
                                        $this->data['Variacao'][$k][$b]['thumb_filename']['size']       = $this->data['Variacao'][$k][$b]['size']['thumb_filename'];
                                        unset($this->data['Variacao'][$k][$b]['size']);
                                    }
                                    //end ajustes no meio upload
                                    
                                    if ($this->Variacao->save($this->data['Variacao'][$k][$b])) {
                                        $this->data['Variacao'][$k][$b]['id'] = $this->Variacao->id;
                                    }
                                }
                            } else {
                                foreach ($this->Variacao->invalidFields() as $field => $error) {
                                    $validationErrors['Variacao'][$k][$b][$field] = $error;
                                }
                            }
                        }
                    }
                } else {
                    foreach ($this->VariacaoTipo->invalidFields() as $field => $error) {
                        if ($field != "variacao_grupo_id")
                            $validationErrors['VariacaoTipo'][$k][$field] = $error;
                        else
                            $validationErrors['VariacaoGrupo']['id'] = $error;
                    }
                }
            }
            if (count($validationErrors) == 0) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                $this->set(compact('validationErrors'));
            }
        }

        //begin VariacaoGrupos
        $VariacaoGrupos = array('' => 'Selecione') + $this->VariacaoGrupo->find('list', array(
                    'fields' => array('VariacaoGrupo.id', 'VariacaoGrupo.nome'),
                    'order' => array('VariacaoGrupo.nome'),
                    'conditions' => array("(select count(*) from variacao_tipos as VariacaoTipo where VariacaoTipo.variacao_grupo_id = VariacaoGrupo.id) <= 0")
//outer nao funcionou :/
//                                                                                            "joins" => array(
//                                                                                                    array(
//                                                                                                        "table" => 'variacao_tipos',
//                                                                                                        "alias" => 'VariacaoTipo',
//                                                                                                        "type" => 'LEFT OUTER',
//                                                                                                        "conditions" => array("VariacaoGrupo.id = VariacaoTipo.variacao_grupo_id")
//                                                                                                    )
//                                                                                                ),
                        )
        );
        $this->set('variacao_grupos', $VariacaoGrupos);
        //end VariacaoGrupos
    }

    function admin_edit($id = null) {
        
        //header('Content-Type: text/html; charset=utf-8');
        
        /* force UTF-8 */
		//iconv_set_encoding("internal_encoding", "UTF-8");
		//iconv_set_encoding("output_encoding", "UTF-8");
		//iconv_set_encoding("input_encoding", "UTF-8"); 
        
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Parâmetro inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        $validationErrors = array();
        if (!empty($this->data)) {

            App::import('Model', 'VariacaoTipo');
            $this->VariacaoTipo = new VariacaoTipo();

            App::import('Model', 'VariacaoProduto');
            $this->VariacaoProduto = new VariacaoProduto();

            App::import('Model', 'Grade');
            $this->Grade = new Grade();

            App::import('Model', 'GradeImagem');
            $this->GradeImagem = new GradeImagem();
            
            $tipo_variacao_novo = false;
            
            
            
            //Configure::write('debug',2);
            //debug($this->data['Variacao']);
            //die;
            
            $tam = count($this->data['Variacao']);
            for ($i = 0; $i < $tam; $i++) {
                $tam_i = count($this->data['Variacao'][$i]);
                for ($j = 0; $j < $tam_i; $j++) {
                    //$this->data['Variacao'][$i][$j]['valor'] = mb_convert_encoding($this->data['Variacao'][$i][$j]['valor'], "UTF-8");
                    
                    $valor = $this->data['Variacao'][$i][$j]['valor'];
                    
                    //$valor_utf8 = iconv(mb_detect_encoding($valor, mb_detect_order(), true), "UTF-8", $valor);
		    //$valor_utf8 = iconv("UTF-8", "ISO-8859-1", $valor);

		    $valor_utf8 = iconv("ISO-8859-1", "UTF-8", $valor);

                    
                    $this->data['Variacao'][$i][$j]['valor'] =  $valor_utf8;

		    //debug(mb_detect_order());

		    //debug($valor_utf8);

                }
            }
            //debug($this->data['Variacao']);
            //Configure::write('debug',0);
            //die();
            
                    
            foreach ($this->data['VariacaoTipo'] as $k => $vt) {
                $this->data['VariacaoTipo'][$k]['variacao_grupo_id'] = $this->data['VariacaoGrupo']['id'];
                $this->VariacaoTipo->set($this->data['VariacaoTipo'][$k]);
                if ($this->VariacaoTipo->validates()) {
                    if ($this->data['Variacao'][$k]) {
                        foreach ($this->data['Variacao'][$k] as $b => $vp) {
                            //$this->Variacao->Behaviors->detach('MeioUpload');
                            $this->Variacao->set($this->data['Variacao'][$k][$b]);
                            if ($this->Variacao->validates()) {
                                if(!isset($this->data['VariacaoTipo'][$k]['id']) || $this->data['VariacaoTipo'][$k]['id'] == ""){
                                    $tipo_variacao_novo = true;
                                }
                                if ($this->VariacaoTipo->save($this->data['VariacaoTipo'][$k])) {
                                    $this->data['VariacaoTipo'][$k]['id'] = $this->VariacaoTipo->id;
                                    $this->data['Variacao'][$k][$b]['variacao_tipo_id'] = $this->data['VariacaoTipo'][$k]['id'];
                                    if ($this->data['Variacao'][$k][$b]['remover'] != 1) {
                                        
                                        //begin ajustes no meio upload
                                        if(isset($this->data['Variacao'][$k][$b]['name']['thumb_filename'])){
                                            $this->data['Variacao'][$k][$b]['thumb_filename']['name']       = $this->data['Variacao'][$k][$b]['name']['thumb_filename'];
                                            unset($this->data['Variacao'][$k][$b]['name']);
                                        }
                                        if($this->data['Variacao'][$k][$b]['type']['thumb_filename']){
                                            $this->data['Variacao'][$k][$b]['thumb_filename']['type']       = $this->data['Variacao'][$k][$b]['type']['thumb_filename'];
                                            unset($this->data['Variacao'][$k][$b]['type']);
                                        }
                                        if($this->data['Variacao'][$k][$b]['tmp_name']['thumb_filename']){
                                            $this->data['Variacao'][$k][$b]['thumb_filename']['tmp_name']   = $this->data['Variacao'][$k][$b]['tmp_name']['thumb_filename'];
                                            unset($this->data['Variacao'][$k][$b]['tmp_name']);
                                        }
                                        if($this->data['Variacao'][$k][$b]['error']['thumb_filename']){
                                            $this->data['Variacao'][$k][$b]['thumb_filename']['error']      = $this->data['Variacao'][$k][$b]['error']['thumb_filename'];
                                            unset($this->data['Variacao'][$k][$b]['error']);
                                        }
                                        if($this->data['Variacao'][$k][$b]['size']['thumb_filename']){
                                            $this->data['Variacao'][$k][$b]['thumb_filename']['size']       = $this->data['Variacao'][$k][$b]['size']['thumb_filename'];
                                            unset($this->data['Variacao'][$k][$b]['size']);
                                        }
                                        //end ajustes no meio upload
                                        
                                        //save data
                                        if ($this->Variacao->save($this->data['Variacao'][$k][$b])) {
                                            $this->data['Variacao'][$k][$b]['id'] = $this->Variacao->id;
                                        }
                                        //remove thumb
                                        if(isset($this->data['Variacao'][$k][$b]['thumb_remove']) && $this->data['Variacao'][$k][$b]['thumb_remove'] == 1){
                                            $this->Variacao->remover_thumb($this->data['Variacao'][$k][$b]['id']);
                                        }
                                    } else {
                                        $grades = $this->VariacaoProduto->find("list", array("recursive" => -1, "fields" => array("VariacaoProduto.grade_id", "VariacaoProduto.grade_id"), "group" => array("VariacaoProduto.grade_id"), "conditions" => array("VariacaoProduto.variacao_id" => $this->data['Variacao'][$k][$b]['id'])));
                                        if(count($grades) > 0){
                                            foreach ($grades as $gr) {
                                                $this->GradeImagem->deleteAll(array('grade_id' => $gr), false);
                                                $this->Grade->delete(array('id' => $gr), false);
                                                $this->VariacaoProduto->deleteAll(array('grade_id' => $gr), false);
                                            }
                                        }
                                        $this->Variacao->delete(array('id' => $this->data['Variacao'][$k][$b]['id']));
                                    }
                                }
                            } else {
                                foreach ($this->Variacao->invalidFields() as $field => $error) {
                                    $validationErrors['Variacao'][$k][$b][$field] = $error;
                                }
                            }
                        }
                    }

                    if ($this->data['VariacaoTipo'][$k]['remover'] == 1) {
                        $variacoes_vinculadas = $this->Variacao->find("list", array("recursive" => -1, "conditions" => array("Variacao.variacao_tipo_id" => $this->data['VariacaoTipo'][$k]['id'])));
                        $grades_vinculadas = $this->VariacaoProduto->find("list", array("recursive" => -1, "fields" => array("VariacaoProduto.grade_id", "VariacaoProduto.grade_id"), "group" => array("VariacaoProduto.grade_id"), "conditions" => array("VariacaoProduto.variacao_id" => $variacoes_vinculadas)));
                        if(count($grades_vinculadas) > 0){
                            foreach ($grades_vinculadas as $gr) {
                                $this->GradeImagem->deleteAll(array('grade_id' => $gr), false);
                                $this->Grade->delete(array('id' => $gr), false);
                                $this->VariacaoProduto->deleteAll(array('grade_id' => $gr), false);
                            }                            
                            $this->Variacao->deleteAll(array('variacao_tipo_id' => $this->data['VariacaoTipo'][$k]['id']), false);
                        }
                        $this->VariacaoTipo->delete(array('id' => $this->data['VariacaoTipo'][$k]['id']), false);
                    }
                } else {
                    foreach ($this->VariacaoTipo->invalidFields() as $field => $error) {
                        if ($field != "variacao_grupo_id")
                            $validationErrors['VariacaoTipo'][$k][$field] = $error;
                        else
                            $validationErrors['VariacaoGrupo']['id'] = $error;
                    }
                }
            }
            
            
            //begin validacao se o grupo de variacao tem grade vinculada
             if (count($validationErrors) == 0) {
                $variacoes_tipos = $this->VariacaoTipo->find("list", array("fields" => array("VariacaoTipo.id"), "conditions" => array("VariacaoTipo.variacao_grupo_id" => $this->data['VariacaoGrupo']['id'])));
                if(count($variacoes_tipos) > 0){
                    $variacoes = $this->Variacao->find("list", array("recursive" => -1, "conditions" => array("Variacao.variacao_tipo_id" => $variacoes_tipos)));
                    if(count($variacoes) > 0){
                        $data = $this->VariacaoProduto->find('all', array('recursive' => -1, 'conditions' => array("VariacaoProduto.variacao_id" => $variacoes)));
                        if ($data && $tipo_variacao_novo == true) {
                            $tem_variacao = true;
                        } else {
                            $tem_variacao = false;
                        }
                    }else{
                        $tem_variacao = false;
                    }
                }else{
                    $tem_variacao = false;
                }
                
                if($tem_variacao == true){
                     $produtos_ids = Set::extract($data, '{n}/VariacaoProduto/produto_id');
                     $produtos_ids = array_unique($produtos_ids);
                     App::import('Model','Produto');
                     $this->Produto = new Produto();
                     foreach($produtos_ids as $produto){
                         $this->Produto->save(array('status' => 0, 'id' => $produto), false);
                         $produtos_editados_ids[] = $this->Produto->id;
                     }
                     $produtos_editados = $this->Produto->find('all', array('recursive' => -1, 'fields' => array('Produto.id','Produto.nome'), 'conditions' => array('Produto.id' => $produtos_ids)));
                }
             }
            //end validacao se o grupo de variacao tem grade vinculada
            
            
            if (count($validationErrors) == 0) {
                $this->Session->setFlash('Os registros foram salvos com sucesso.', 'flash/success');
                if($tem_variacao && count($produtos_editados) > 0){
                    $this->set(compact('produtos_editados'));
                    $id = $this->data['VariacaoGrupo']['id'];
                    $this->set('grupo_id', $id);
                }else{
                    $this->redirect(array('action' => 'index'));
                }
            } else {
                $this->Session->setFlash('Verifique os campos em destaque e tente novamente.', 'flash/error');
                $this->set(compact('validationErrors'));
            }
        }
        
        App::import('Model', 'VariacaoGrupo');
        $this->VariacaoGrupo = new VariacaoGrupo();
            
        if (empty($this->data)) {
           
            $data = $this->VariacaoGrupo->find("all", array(
                "conditions" => array("VariacaoGrupo.id" => $id),
                "contain" => array("VariacaoTipo" => array("Variacao"))
                    )
            );
            
            //ordenacao
            foreach($data as $kk => $dt){
                foreach($dt['VariacaoTipo'] as $mm => $vt){
                    $data[$kk]['VariacaoTipo'][$mm]['Variacao'] = $this->Variacao->sort_variacoes($data[$kk]['VariacaoTipo'][$mm]['Variacao'], 'valor', SORT_ASC);
                }
            }

            $this->data['VariacaoGrupo']['id'] = $data[0]['VariacaoGrupo']['id'];
            foreach ($data[0]['VariacaoTipo'] as $k => $vt) {
                $this->data["VariacaoTipo"][$k]["id"] = $vt['id'];
                $this->data["VariacaoTipo"][$k]["nome"] = $vt['nome'];
                $this->data["VariacaoTipo"][$k]["ordem"] = $vt['ordem'];
                foreach ($data[0]["VariacaoTipo"][$k]['Variacao'] as $y => $v) {
                    $this->data["Variacao"][$k][$y]["id"]               = $v['id'];
                    $this->data["Variacao"][$k][$y]["valor"]            = $v['valor'];
					$this->data["Variacao"][$k][$y]["ordem"]            = $v['ordem'];
                    $this->data["Variacao"][$k][$y]["thumb"]            = $v['thumb'];
                    $this->data["Variacao"][$k][$y]["thumb_filename"]   = $v['thumb_filename'];
                    $this->data["Variacao"][$k][$y]["thumb_filesize"]   = $v['thumb_filesize'];
                    $this->data["Variacao"][$k][$y]["thumb_dir"]        = $v['thumb_dir'];
                    $this->data["Variacao"][$k][$y]["thumb_mimetype"]   = $v['thumb_mimetype'];
                    
                }
            }
        }

        //begin VariacaoGrupos
        $VariacaoGrupos = array('' => 'Selecione') + $this->VariacaoGrupo->find('list', array(
                    'fields' => array('VariacaoGrupo.id', 'VariacaoGrupo.nome'),
                    'order' => array('VariacaoGrupo.nome'),
                    'conditions' => array(
                        "OR" =>
                        array(
                            array("(select count(*) from variacao_tipos as VariacaoTipo where VariacaoTipo.variacao_grupo_id = VariacaoGrupo.id) <= 0"),
                            array("VariacaoGrupo.id" => $id))
                    )
//outer nao funcionou :/
//                                                                                            "joins" => array(
//                                                                                                    array(
//                                                                                                        "table" => 'variacao_tipos',
//                                                                                                        "alias" => 'VariacaoTipo',
//                                                                                                        "type" => 'LEFT OUTER',
//                                                                                                        "conditions" => array("VariacaoGrupo.id = VariacaoTipo.variacao_grupo_id")
//                                                                                                    )
//                                                                                                ),
                        )
        );
        $this->set('variacao_grupos', $VariacaoGrupos);
        //end VariacaoGrupos
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Parametros inválidos', 'flash/error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Variacao->delete($id)) {
            $this->Session->setFlash('Registro deletado com sucesso', 'flash/success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('O Registro não pode ser deletado, tente novamente.', 'flash/error');
        $this->redirect(array('action' => 'index'));
    }

    public function admin_exportar($conditions) {

        App::import('Helper', 'Calendario');
        $this->Calendario = new CalendarioHelper();

        App::import('Model','VariacaoGrupo');
        $this->VariacaoGrupo = new VariacaoGrupo();
        $rows =  $this->VariacaoGrupo->find('all', array(
                                            'contain' => array("VariacaoTipo" => array("Variacao")),
                                            'conditions' => $conditions
                                        )
                                    );
        
        $table = "<table>";
        $table .= "
				<tr bgcolor=\"#CECECE\">
					<td><strong>Id</strong></td>
					<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Variação") . "</strong></td>
					<td><strong>" . iconv("UTF-8", "ISO-8859-1//IGNORE", "Valor") . "</strong></td>
					<td><strong>Criado</strong></td>
					<td><strong>Editado</strong></td>
				</tr>";
        foreach ($rows as $row) {
            $table .= "
				<tr>
					<td>" . $row['VariacaoGrupo']['id'] . "</td>
					<td>" . iconv("UTF-8", "ISO-8859-1//IGNORE", $row['VariacaoGrupo']['nome']) . "</td>
                                        <td>" . $this->Calendario->DataFormatada("d-m-Y", $row['VariacaoGrupo']['created']) . "</td>
					<td>" . $this->Calendario->DataFormatada("d-m-Y", $row['VariacaoGrupo']['modified']) . "</td>
				</tr>
                                <tr>";
                                                foreach($row['VariacaoTipo'] as $vt){ 
                                                $table .= "<td>";
                                                $table .= iconv("UTF-8", "ISO-8859-1//IGNORE", $vt['nome']); 
                                                $table .= "</td>";
                                                $first = true;
                                                    foreach($vt['Variacao'] as $v){ 
                                                        if($first){
                                                            $first = false;
                                                        }else{
                                                            $table .= "</tr><tr><td>&nbsp;</td>";
                                                        }
                                                        $table .= "<td>";
                                                        $table .= iconv("UTF-8", "ISO-8859-1//IGNORE", $v['valor']);
                                                        $table .= "</td></tr>";
                                                    }
                                                }
                               $table .= "</tr>";
                                            
        }
        $table .= "</table>";

        App::import("helper", "String");
        $this->String = new StringHelper();
        $this->layout = false;
        $this->render(false);
        set_time_limit(0);
        header('Content-type: application/x-msexcel');
        $filename = "Variacoes" . date("d_m_Y_H_i_s");
        header('Content-Disposition: attachment; filename=' . $filename . '.xls');
        header('Pragma: no-cache');
        header('Expires: 0');

        die($table);
    }

    function admin_ajax_get_grade_variacao_valor($id) {
        $this->layout = false;
        $this->render(false);

        App::import('Model', 'VariacaoProduto');
        $this->VariacaoProduto = new VariacaoProduto();

        $data = $this->VariacaoProduto->find('first', array('recursive' => -1, 'conditions' => array("VariacaoProduto.variacao_id" => $id)));
        if ($data) {
            die(json_encode(true));
        } else {
            die(json_encode(false));
        }
    }
    
    function admin_ajax_get_grade_variacao_grupo($id) {
        $this->layout = false;
        $this->render(false);
        
        App::import('Model', 'VariacaoTipo');
        $this->VariacaoTipo = new VariacaoTipo();
        $variacoes_tipos = $this->VariacaoTipo->find("list", array("fields" => array("VariacaoTipo.id"), "conditions" => array("VariacaoTipo.variacao_grupo_id" => $id)));
       
        if(count($variacoes_tipos) > 0){
            App::import('Model', 'Variacao');
            $this->Variacao = new Variacao();
            $variacoes = $this->Variacao->find("list", array("recursive" => -1, "conditions" => array("Variacao.variacao_tipo_id" => $variacoes_tipos)));
            if(count($variacoes) > 0){
                App::import('Model', 'VariacaoProduto');
                $this->VariacaoProduto = new VariacaoProduto();

                $data = $this->VariacaoProduto->find('first', array('recursive' => -1, 'conditions' => array("VariacaoProduto.variacao_id" => $variacoes)));
                if ($data) {
                    die(json_encode(true));
                } else {
                    die(json_encode(false));
                }
            }else{
                die(json_encode(false));
            }
        }else{
            die(json_encode(false));
        }
    }

    function admin_ajax_get_grade_variacao_tipo($id) {
        $this->layout = false;
        $this->render(false);

        App::import('Model', 'Variacao');
        $this->Variacao = new Variacao();
        $variacoes = $this->Variacao->find("list", array("recursive" => -1, "conditions" => array("Variacao.variacao_tipo_id" => array($id))));

        App::import('Model', 'VariacaoProduto');
        $this->VariacaoProduto = new VariacaoProduto();

        $data = $this->VariacaoProduto->find('first', array('recursive' => -1, 'conditions' => array("VariacaoProduto.variacao_id" => $variacoes)));
        if ($data) {
            die(json_encode(true));
        } else {
            die(json_encode(false));
        }
    }

    public function admin_get_ajax_variacoes() {
        $this->layout = 'ajax';
        $variacoes = $this->Variacao->find("all", array(
            'recursive' => -1,
            'fields' => array('Variacao.id', 'Variacao.valor'),
            'order' => array('Variacao.valor ASC'),
            'conditions' => array('Variacao.variacao_tipo_id' => $this->params['form']['variacao_id'])
                )
        );
        $final = array();
        if ($variacoes) {
            foreach ($variacoes as $variacoes) {
                $final[$variacoes['Variacao']['id']] = $variacoes['Variacao']['valor'];
            }
        }
        die(json_encode($final));
    }

    public function ajax_get_cor_fabricante($fabricante_id, $ajax = true) {

        App::import("Model", "VariacaoProduto");
        $this->VariacaoProduto = new VariacaoProduto();

        App::import("Model", "Variacao");
        $this->Variacao = new Variacao();

        App::import("Model", "Produto");
        $this->Produto = new Produto();

        App::import('Component', 'Session');
        $this->Session = new SessionComponent();
        //$this->load('Component','Session');

        $conditions = array(
            "Produto.id" => $this->Session->read("ProdutosCategoria"),
            "Produto.fabricante_id" => $fabricante_id,
                //"Produto.status >" => 0
        );
        $produtos_ids = $this->Produto->find("list", array("recursive" => -1, "fields" => "Produto.id", "conditions" => $conditions));

        $variacoes_cores = $this->Variacao->find("list", array(
            'recursive' => 1,
            'contain' => array('VariacaoTipo'),
            'joins' => array(
                array(
                    'table' => 'variacoes_produtos',
                    'alias' => 'VariacaoProduto',
                    'type' => 'LEFT',
                    'conditions' => array('VariacaoProduto.variacao_id = Variacao.id')
                )
            ),
            'fields' => array('Variacao.id', 'Variacao.valor'),
            'conditions' => array('VariacaoProduto.produto_id' => $produtos_ids, 'VariacaoTipo.codigo' => 'cor')
                )
        );
        if ($ajax) {
            $this->layout = 'ajax';
            die(json_encode($variacoes_cores));
        } else {
            return $variacoes_cores;
        }
    }

    public function ajax_get_tamanho_cor($fabricante_id, $cor_id, $ajax = true) {
        $this->layout = 'ajax';

        App::import("Model", "VariacaoProduto");
        $this->VariacaoProduto = new VariacaoProduto();

        App::import("Model", "Produto");
        $this->Produto = new Produto();

        $conditions = array(
            "Produto.id" => $this->Session->read("ProdutosCategoria"),
            "Produto.status >" => 0,
            "Produto.fabricante_id" => $fabricante_id,
            "VariacaoProduto.variacao_id" => $cor_id
        );
        $produtos_ids = $this->Produto->find("list", array(
            "recursive" => 1,
            'joins' => array(
                array(
                    'table' => 'variacoes_produtos',
                    'alias' => 'VariacaoProduto',
                    'type' => 'LEFT',
                    'conditions' => array('VariacaoProduto.produto_id = Produto.id')
                )
            ),
            "fields" => "Produto.id",
            "conditions" => $conditions)
        );

        //$filhos = $this->Produto->children($produtos_ids);
        $conditions2 = array(
            "Produto.parent_id" => $produtos_ids,
            "Produto.status >" => 0
        );
        $filhos_ids = $this->Produto->find("list", array(
            "recursive" => 1,
            "fields" => "Produto.id",
            "conditions" => $conditions2)
        );
        //debug($filhos_ids);
        $variacoes_tamanho = $this->Variacao->find("list", array(
            'recursive' => 1,
            'contain' => array('VariacaoTipo'),
            'joins' => array(
                array(
                    'table' => 'variacoes_produtos',
                    'alias' => 'VariacaoProduto',
                    'type' => 'LEFT',
                    'conditions' => array('VariacaoProduto.variacao_id = Variacao.id')
                )
            ),
            'fields' => array('Variacao.id', 'Variacao.valor'),
            'conditions' => array('VariacaoProduto.produto_id' => am($produtos_ids, $filhos_ids), 'VariacaoTipo.codigo' => 'tamanho'),
            'group' => 'Variacao.valor'
                )
        );

        if ($ajax) {
            $this->layout = 'ajax';
            die(json_encode($variacoes_tamanho));
        } else {
            return $variacoes_tamanho;
        }
    }

}

?>
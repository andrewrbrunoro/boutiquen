<?php
    $routes = new Router();

    $routes->connect('/404', array('controller' => 'paginas', 'action' => 'pagina_404'));

    $routes->connect('/', array('controller' => 'home', 'action' => 'index'));

    $routes->connect('/login', array('controller' => 'usuarios', 'action' => 'login'));
    $routes->connect('/logout', array('controller' => 'usuarios', 'action' => 'logout'));


    $routes->connect('/busca/index/', array('controller' => 'busca', 'action' => 'index'));
    $routes->connect('/busca/index/*', array('controller' => 'busca', 'action' => 'index'));

    $routes->connect('/marca/index/', array('controller' => 'busca', 'action' => 'index'));
    $routes->connect('/marca/index/*', array('controller' => 'busca', 'action' => 'index'));

    $routes->connect("/intl/*", array('controller' => 'paginas', 'action' => 'view'), array());


    $routes->connect('/:slug-cod-:id.html', array('controller' => 'produtos', 'action' => 'detalhe'), array('pass' => array('id'), 'id' => '[0-9]+'));

    /*Carrinho*/
    $routes->connect('/carrinho',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'index'
        )
    );
    $routes->connect('/carrinho/pagamento',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'forma_pagamento'
        )
    );
    $routes->connect('/carrinho/compra-finalizada',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'finalizacao'
        )
    );
    $routes->connect('/carrinho/remover_item/:id',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'remover_item'
        ), array(
            'pass' => array('id'),
            'id'   => '[0-9]+'
        )
    );
    $routes->connect('/service/Payment',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'callPayment'
        )
    );

    $routes->connect('/service/saveFreights',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'saveFreights'
        )
    );

    $routes->connect('/carrinho/alteraQuantidade',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'alterar_quantidade'
        )
    );

    $routes->connect('/meus-pedidos',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'meus_pedidos'
        )
    );

    $routes->connect('/carrinho/add/*',
        array(
            'plugin'     => 'carrinho',
            'controller' => 'carrinho',
            'action'     => 'add'
        )
    );
    /*CARRINHO*/
    $routes->connect('/categoria/:seo-url/filtros/:page',
        array(
            'plugin'     => false,
            'controller' => 'categorias',
            'action'     => 'index'
        ),
        array(
            'pass'    => array('seo-url', 'page'),
            'seo-url' => '[a-zA-Z0-9-/]+',
            'page'    => '[0-9]+'
        )
    );
    $routes->connect('/categoria/:seo-url/filtros/:page/:price-range',
        array(
            'plugin'     => false,
            'controller' => 'categorias',
            'action'     => 'index'
        ),
        array(
            'pass'        => array('seo-url', 'page', 'price-range'),
            'seo-url'     => '[a-zA-Z0-9-/]+',
            'page'        => '[0-9]+',
            'price-range' => '[0-9-.]+'
        )
    );
    $routes->connect('/categoria/:seo-url/filtros/:page/:price-range/:color',
        array(
            'plugin'     => false,
            'controller' => 'categorias',
            'action'     => 'index'
        ),
        array(
            'pass'        => array('seo-url', 'page', 'price-range', 'color'),
            'seo-url'     => '[a-zA-Z0-9-/]+',
            'page'        => '[0-9]+',
            'price-range' => '[0-9-.]+',
            'color'       => '[a-zA-Z-]+'
        )
    );
    $routes->connect('/categoria/:seo-url/filtros/:page/:price-range/:color/:size',
        array(
            'plugin'     => false,
            'controller' => 'categorias',
            'action'     => 'index'
        ),
        array(
            'pass'        => array('seo-url', 'page', 'price-range', 'color', 'size'),
            'seo-url'     => '[a-zA-Z0-9-/]+',
            'page'        => '[0-9]+',
            'price-range' => '[0-9-.]+',
            'color'       => '[a-zA-Z-]+',
            'size'        => '[0-9]+'
        )
    );
    $routes->connect('/categoria/:seo-url',
        array(
            'plugin'     => false,
            'controller' => 'categorias',
            'action'     => 'index'
        ),
        array(
            'pass'    => array('seo-url'),
            'seo-url' => '[a-zA-Z0-9-/]+'
        )
    );


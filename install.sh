#!/bin/bash

DIRS="app/tmp
app/webroot/imagecache
app/webroot/uploads
app/webroot/integracao"

for DIR in $DIRS
do
    echo "Changing MODE to 0777 into $DIR"
    chmod -R 0777 "$DIR"
done
